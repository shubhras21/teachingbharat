﻿using BLL.BusinessModels;
using DAL.Entities;
using DAL.MasterEntity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL
{
    public static class ReviewManagers
    {
        public static List<RatingReviews> GetAllReviewsByTeachers()
        {
            var bannermodel = new List<ReviewModel>();

            using (var ctx = new MyDbContext())
            {
                var bannerslides = (ctx.RatingReviews.Where(o=>o.CreatedBy == "Teacher").OrderByDescending(p => p.RatingReviewsID)).ToList();

                Mapper.MappRatingsListToModel(bannerslides, ref bannermodel);

                return bannerslides;
            }
        }
        public static List<RatingReviews> GetAllReviewsByStudents()
        {
            var bannermodel = new List<ReviewModel>();

            using (var ctx = new MyDbContext())
            {
                var bannerslides = (ctx.RatingReviews.Where(o => o.CreatedBy == "Student").OrderByDescending(p => p.RatingReviewsID)).ToList();

                Mapper.MappRatingsListToModel(bannerslides, ref bannermodel);

                return bannerslides;
            }
        }
        public static List<RatingReviews> GetAllReviews()
        {
            var bannermodel = new List<ReviewModel>();

            using (var ctx = new MyDbContext())
            {
                var bannerslides = (ctx.RatingReviews.OrderByDescending(p => p.RatingReviewsID)).ToList();

                Mapper.MappRatingsListToModel(bannerslides, ref bannermodel);

                return bannerslides;
            }
        }
    }
}
