﻿using BLL.BusinessManagers.AdminManager;
using BLL.BusinessManagers.StudentManager;
using BLL.BusinessManagers.TeacherManager;
using BLL.BusinessManagers.UserManager;
using BLL.BusinessModels;
using BLL.BusinessModels.AdminModels;
using BLL.BusinessModels.NotificationModel;
using BLL.BusinessModels.StudentModels;
using BLL.BusinessModels.TeacherModels;
using DAL.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL
{
    public static class Mapper
    {
        internal static void MappUsersListToModel(List<Users> dbusersList, ref List<UserViewModel> usersListViewModel)
        {
            usersListViewModel = (from dbuser in dbusersList
                                  select new UserViewModel
                                  {
                                      UserID = dbuser.UserID,
                                      LoginEmail = dbuser.LoginEmail,
                                      Password = dbuser.Password,
                                      ExternalLoginID = dbuser.ExternalLoginID,
                                      UserType = dbuser.UserType,
                                      UserStatus = dbuser.UserStatus,
                                      IsVerified = dbuser.IsVerified,
                                      IsActive = dbuser.IsActive,
                                      IsOnline = dbuser.IsOnline,
                                      UsersProfileViewModel = dbuser.UsersProfile != null ? MappUserProfileToModel(dbuser.UsersProfile) : null

                                  }).ToList();
        }

        internal static void MappBannerSlidesListToModel(List<BannerSlides> slides , ref List<BannerModel> modelList)
        {
            modelList = (from dbBanner in slides
                         select new BannerModel
                         {
                             ID = dbBanner.BannerSlidesID,
                             Link = dbBanner.Link,
                             Status = dbBanner.Status,
                             Title = dbBanner.Title
                         }).ToList();
        }
        internal static void MappBannerNotificationsListToModel(List<NotificationTable> slides, ref List<NotificationModel> modelList)
        {
            modelList = (from dbBanner in slides
                         select new NotificationModel
                         {
                             NotificationID = dbBanner.NotificationID,
                             UserID = dbBanner.UserID,
                             Comments = dbBanner.Comments,
                             ReviewID = dbBanner.ReviewID,
                             IsSeen = dbBanner.IsSeen,
                             NotificationDateTime = dbBanner.NotificationDateTime
                         }).ToList();
        }
        internal static void MappRatingsListToModel(List<RatingReviews> slides, ref List<ReviewModel> modelList)
        {
            modelList = (from dbBanner in slides
                         select new ReviewModel
                         {
                             ratingID = Convert.ToInt32( dbBanner.RatingReviewsID),
                             studentID = Convert.ToInt32(dbBanner.FKStudentID),
                             techerID = Convert.ToInt32(dbBanner.FKTeacherID),
                             Comments = dbBanner.Comment,
                             createdDate = dbBanner.CreatedDate,
                             ratings = dbBanner.Rating,
                             IsActive = dbBanner.IsActive,
                             CreatedBy = dbBanner.CreatedBy,
                             StudentName = dbBanner.StudentName,
                             TeacherName = dbBanner.TeacherName,
                             IsApproved=dbBanner.IsApproved
                             
                         }).ToList();
        }
        internal static void MappUserActivitiesListToModel(List<UserActivities> activitiesList, ref List<UserActivitiesModel> activitiesListModel )
        {
            AdminManager adminMgr = new AdminManager();

            activitiesListModel = (from dbActivity in activitiesList
                                   select new UserActivitiesModel
                                   {
                                       ActivityID = dbActivity.ActivityID,
                                       ActionPerformed = dbActivity.ActionPerformed,
                                       FKUserID = dbActivity.FKUserID,
                                       IsActive = dbActivity.IsActive,
                                       Time = dbActivity.Time,
                                       ActivitySource = dbActivity.ActivitySource,
                                       userModel = dbActivity.FKUserID != 0 ? adminMgr.GetAdminByUserID(dbActivity.FKUserID) : null

                                   }).ToList();
        }
        internal static void MappCourseHeaderListToModel(List<CoursesHeader> coursesHeader, ref List<TeacherCourseHeaderModel> coursesHeaderListModel)
        {
            CurriculumsManagers currMgr = new CurriculumsManagers();
            StudentManager studMgr = new StudentManager();

            coursesHeaderListModel = (from dbCourseHeader in coursesHeader
                                      select new TeacherCourseHeaderModel
                                      {
                                          CoursesHeaderID = dbCourseHeader.CoursesHeaderID,
                                          CourseTitle = dbCourseHeader.CourseTitle,
                                          CourseDescription = dbCourseHeader.CourseDescription,
                                          CourseFee = dbCourseHeader.CourseFee,
                                          TotalHours = dbCourseHeader.TotalHours,
                                          TotalSeats = dbCourseHeader.TotalSeats,
                                          TrialVideoURL = dbCourseHeader.TrialVideoURL,
                                          OfferedDate = dbCourseHeader.OfferedDate,
                                          TimeSlot = dbCourseHeader.TimeSlot,
                                          StartDate = dbCourseHeader.StartDate,
                                          EndDate = dbCourseHeader.EndDate,
                                          FKCurriculumID = dbCourseHeader.FKCurriculumID,
                                          FKGradeID = dbCourseHeader.FKGradeID,
                                          FKSubjectID = dbCourseHeader.FKSubjectID,
                                          FKOfferedBY = dbCourseHeader.FKOfferedBY,
                                          IsActive = dbCourseHeader.IsActive,
                                          curriculumViewModel = dbCourseHeader.FKCurriculumID != 0 ? currMgr.GetCurriculumByCurriculumID(dbCourseHeader.FKCurriculumID) : null,
                                          gradeViewModel = dbCourseHeader.FKGradeID != 0 ? studMgr.GetGradeByGradeID(dbCourseHeader.FKGradeID) : null,
                                          subjectModel = dbCourseHeader.FKSubjectID != 0 ? studMgr.GetSubjectBySubjectID(dbCourseHeader.FKSubjectID) : null

                                      }).ToList();
        }

        internal static void MappTeacherAvailableHoursListToModel(List<TeacherAvailableHours> teacherHours,ref List<TeacherAvailableHoursModel> teachersAvailableHoursModel)
        {
            TeacherManager teacherMgr = new TeacherManager();

            teachersAvailableHoursModel = (from DbAvailableHours in teacherHours
                                           select new TeacherAvailableHoursModel
                                           {
                                               FKUserID = DbAvailableHours.FKUserID,
                                               FKTimeID = DbAvailableHours.FKTimeID,
                                               FKDayID =  DbAvailableHours.FKDayID,
                                               IsActive = DbAvailableHours.IsActive,
                                               timingModel = DbAvailableHours.FKTimeID != 0 ? teacherMgr.GetTeacherTimingsByTimeID(DbAvailableHours.FKTimeID) : null,
                                               dayModel = DbAvailableHours.FKDayID != 0 ? teacherMgr.GetDayByDayID(DbAvailableHours.FKDayID) : null

                                           }).ToList();
        }

        internal static void MappTeacherSubjectsListToModel(List<TeacherSubjects> DbTeacherSubjectsList , ref List<TeacherSubjectsViewModel> teacherSubjectsModel)
        {
            CurriculumsManagers currMgr = new CurriculumsManagers();
            StudentManager studMgr = new StudentManager(); 

            teacherSubjectsModel = (from dbTeacherSubject in DbTeacherSubjectsList
                                    select new TeacherSubjectsViewModel
                                    {
                                        TeacherSubjectsID = dbTeacherSubject.TeacherSubjectsID,
                                        FKUserID = dbTeacherSubject.FKUserID,
                                        FKCurriculumID = dbTeacherSubject.FKCurriculumID,
                                        FKSubjectID = dbTeacherSubject.FKSubjectID,
                                        FKGradeID = dbTeacherSubject.FKGradeID,
                                        Type = dbTeacherSubject.Type,
                                        IsActive = dbTeacherSubject.IsActive,
                                        curriculumModel = dbTeacherSubject.FKCurriculumID != 0 ? currMgr.GetCurriculumByCurriculumID(dbTeacherSubject.FKCurriculumID) : null,
                                        gradeModel = dbTeacherSubject.FKGradeID != 0 ? studMgr.GetGradeByGradeID(dbTeacherSubject.FKGradeID) : null,
                                        subjectModel = dbTeacherSubject.FKSubjectID != 0 ? studMgr.GetSubjectBySubjectID(dbTeacherSubject.FKSubjectID) : null


                                    }).ToList();
        }

        internal static void MappSessionListToModel(List<Sessions> dbSessionList, ref List<SessionViewModel> sessionsModelList)
        {
            CurriculumsManagers currMgr = new CurriculumsManagers();
            StudentManager studMgr = new StudentManager();
            TeacherManager teacherMgr = new TeacherManager();
           

            sessionsModelList = (from dbSession in dbSessionList
                                 select new SessionViewModel
                                 {
                                     TrialSessionID = dbSession.TrialSessionID,
                                     SessionName = dbSession.SessionName,
                                     TopicName = dbSession.TopicName,
                                     StartDate = dbSession.StartDate,
                                     EndDate = dbSession.EndDate,
                                     RequestDate = dbSession.RequestDate,
                                     TrialHappenDate = dbSession.TrialHappenDate,
                                     FKCurriculumID = dbSession.FKCurriculumID,
                                     FKGradeID = dbSession.FKGradeID,
                                     FKSubjectID = dbSession.FKSubjectID,
                                     FKRequestedBy = dbSession.FKRequestedBy,
                                     FKRequestedTo = dbSession.FKRequestedTo,
                                     SessionType = dbSession.SessionType,
                                     RequestStatus = dbSession.RequestStatus,
                                     SessionLink = dbSession.SessionLink,
                                     SessionCost = dbSession.SessionCost,
                                     CurrentStatus = dbSession.CurrentStatus,
                                     CurriculumModel = dbSession.FKCurriculumID != 0 ? currMgr.GetCurriculumByCurriculumID(dbSession.FKCurriculumID) : null,
                                     GradeModel = dbSession.FKGradeID != 0 ? studMgr.GetGradeByGradeID(dbSession.FKGradeID) : null,
                                     SubjectModel = dbSession.FKSubjectID != 0 ?  studMgr.GetSubjectBySubjectID(dbSession.FKSubjectID) : null,
                                     userModel = dbSession.FKRequestedTo != null ? teacherMgr.GetTeacherByUserID(dbSession.FKRequestedTo) : null

                                 }).ToList();
        }
        

        internal static void MappPermissionsListToModel(List<Permission> DbPermissionsList, ref List<PermissionViewModel> PermissionsList)
        {
            PermissionsList = (from dbPermission in DbPermissionsList
                               select new PermissionViewModel
                               {
                                   PermissionID = dbPermission.PermissionID,
                                   PermissionName = dbPermission.PermissionName,
                                   IsActive = dbPermission.IsActive,

                               }).ToList();
        }

        internal static void MappGradesListToModel(List<Grades> dbGrades, ref List<GradeViewModel> gradesModelList)
        {
            gradesModelList = (from dbGrade in dbGrades
                               select new GradeViewModel
                               {
                                   GradeViewModelID = dbGrade.GradeID,
                                   GradeName = dbGrade.GradeName,
                                   IsActive = dbGrade.IsActive,
                                   FKCurriculumID = dbGrade.FKCurriculumID
                               }).ToList();
        }

        internal static void MappCurriculumsListToModel(List<Curriculums> DbCurriculumsList, ref List<CurriculumsModel> curriculumsModelList)
        {
            curriculumsModelList = (from dbCurriculum in DbCurriculumsList
                                    select new CurriculumsModel
                                    {
                                        CurriculumID = dbCurriculum.CurriculumID,
                                        CurriculumName = dbCurriculum.CurriculumName,
                                        IsActive = dbCurriculum.IsActive,

                                    }).ToList();
        }
        internal static void MappStatesListToModel(List<States> DbStatesList, ref List<StateViewModel> statesListViewModel)
        {
            statesListViewModel = (from dbstate in DbStatesList
                                   select new StateViewModel
                                   {
                                       StateId = dbstate.StateID,
                                       StateName = dbstate.StateName,
                                       IsActive = dbstate.IsActive,
                                       FKCountryId = dbstate.FKCountryID

                                   }).ToList();
        }

        internal static void MappUserViewModelToUserEntity(UserViewModel userViewModel, ref Users DbUser)
        {
            DbUser = new Users
            {
                UserID = userViewModel.UserID,
                LoginEmail = userViewModel.LoginEmail,
                Password = userViewModel.Password,
                UserType = userViewModel.UserType,
                UserStatus = userViewModel.UserStatus,
                IsVerified = userViewModel.IsVerified,
                IsActive = userViewModel.IsActive,
                IsOnline = userViewModel.IsOnline,

                UsersProfile = MappUserProfileToDB(userViewModel.UsersProfileViewModel),

            };
        }

        internal static void MappEmailTemplateToDB(EmailTemplatesModel templateModel , ref DAL.Entities.EmailTemplate DBemailTemplate)
        {
            DBemailTemplate = new DAL.Entities.EmailTemplate
            {
                EmailTemplateID = templateModel.EmailTemplatesModelID,
                Body = templateModel.Body,
                Type = templateModel.Type
            };
        }

        internal static void  MappBannerModelToDB(BannerModel model , ref BannerSlides bannerslide)
        {
            bannerslide = new BannerSlides
            {
                BannerSlidesID = model.ID,
                Link = model.Link,
                Status = true,
                Title = model.Title
            };
        }
        internal static void MappBannerModelUpdateToDB(BannerModel model, ref BannerSlides bannerslide)
        {
            bannerslide = new BannerSlides
            {
                BannerSlidesID = model.ID,
                Link = model.Link,
                Status = model.Status,
                Title = model.Title
            };
        }
        internal static void MappSettingsModelUpdateToDB(SettingsModel model, ref MetaSetting bannerslide)
        {
            bannerslide = new MetaSetting
            {
                MetaID = model.MetaID,
                Description = model.Description,
                IsEnabled = model.IsEnabled,
                KeyWords = model.KeyWords,
                Title = model.Title
            };
        }

        internal static void MappEmailTemplateToModel(DAL.Entities.EmailTemplate emailTemplate , ref EmailTemplatesModel templateModel)
        {
            templateModel = new EmailTemplatesModel
            {
                EmailTemplatesModelID = emailTemplate.EmailTemplateID,
                Body  = emailTemplate.Body,
                Type = emailTemplate.Type,
            };
        }

        internal static void MappBannerSlideToModel(BannerSlides bannerslide , ref BannerModel model)
        {
            model = new BannerModel
            {
                ID = bannerslide.BannerSlidesID,
                Link = bannerslide.Link,
                Status = bannerslide.Status,
                Title = bannerslide.Title
            };
        }




        internal static UsersProfile MappUserProfileToDB(UsersProfileViewModel userProfileViewModel)
        {
            return new UsersProfile
            {
                UsersProfileID = userProfileViewModel.UsersProfileID,
                FirstName = userProfileViewModel.FirstName,
                LastName = userProfileViewModel.LastName,
                ContactNumber = userProfileViewModel.ContactNumber,
                AlternateNumber = userProfileViewModel.AlternateNumber,
                ProfilePic = userProfileViewModel.ProfilePic,
                HeardAboutRudra = userProfileViewModel.HeardAboutRudra,
                Resume = userProfileViewModel.Resume,
                AboutMe = userProfileViewModel.AboutMe,
                DateOfBirth = userProfileViewModel.DateOfBirth,
                CreateDate = userProfileViewModel.CreateDate,
                UpdatedDate = userProfileViewModel.UpdatedDate,
                WeekHours = userProfileViewModel.WeekHours,
                WeekendHours = userProfileViewModel.WeekendHours,
                CityName = userProfileViewModel.CityName,
                IsPen = userProfileViewModel.IsPen,
                IsWebcam = userProfileViewModel.IsWebcam,
                IsInternetSpeed = userProfileViewModel.IsInternetSpeed,
                FKStateID = userProfileViewModel.FKStateId,
                FKUserID = userProfileViewModel.FKUserId,
                FKOccupationID = userProfileViewModel.FKOccupationId,
                FKGradeID = userProfileViewModel.GradeID
            };
        }

        internal static void MappUserToUserViewModel(Users DbUser, ref UserViewModel UserViewModel)
        {
            UserViewModel = new UserViewModel
            {
                UserID = DbUser.UserID,
                LoginEmail = DbUser.LoginEmail,
                Password = DbUser.Password,
                UserType = DbUser.UserType,
                UserStatus = DbUser.UserStatus,
                IsVerified = DbUser.IsVerified,
                IsActive = DbUser.IsActive,
                IsOnline = DbUser.IsOnline,

                UsersProfileViewModel = DbUser.UsersProfile != null ? MappUserProfileToModel(DbUser.UsersProfile) : null,

            };
        }

        internal static void MappUserActivityToModel(UserActivities userActivity, ref UserActivitiesModel userActivityModel)
        {
            userActivityModel = new UserActivitiesModel
            {
                ActivityID = userActivity.ActivityID,
                ActionPerformed = userActivity.ActionPerformed,
                FKUserID = userActivity.FKUserID,
                IsActive = userActivity.IsActive,
                Time = userActivity.Time,
               
            };
        }

        internal static void MappTimingToModel(Timings timing, ref TimingModel timingModel)
        {
            timingModel = new TimingModel
            {
                TimingId = timing.TimeID,
                FromTime = timing.FromTime,
                ToTime = timing.ToTime,
                TimeSlot = timing.TimeSlot
            };
        }

        internal static void MappDayToModel(Days day , ref DaysModel dayModel)
        {
            dayModel = new DaysModel
            {
                DayOfWeek = day.DayOfWeek,
                FullName = day.FullDayName,
            };
        }

        internal static void MappTeacherQualificationToModel(TeacherQualification TeacherQualification, ref TeacherQualificationModel model)
        {
            model = new TeacherQualificationModel
            {
                TeacherQualificationID = TeacherQualification.TeacherQualificationID,
                QualificationName = TeacherQualification.QualificationName,
                UniversityName = TeacherQualification.UniversityName,
                EndDate = TeacherQualification.EndDate,
                FKUserID = TeacherQualification.FKUserID,
                IsActive = TeacherQualification.IsActive,
                StartDate = TeacherQualification.StartDate
            };
        }

        internal static void MappStudentGradeToModel(StudentGrades studentGrade, ref StudentGradeViewModel stdntGradeModel)
        {
            stdntGradeModel = new StudentGradeViewModel
            {
                StudentGradeID = studentGrade.StudentGradeID,
                GradeName = studentGrade.GradeName,
                FKUserID = studentGrade.FKUserID
            };
        }
        internal static void MappCurriculumToModel(Curriculums DbCurriculum, ref CurriculumsModel curriculumModel)
        {
            curriculumModel = new CurriculumsModel
            {
                CurriculumID = DbCurriculum.CurriculumID,
                CurriculumName = DbCurriculum.CurriculumName,
                IsActive = DbCurriculum.IsActive
            };
        }
        internal static void MappSubjectToModel(Subjects DbSubject , ref SubjectModel model)
        {
            model = new SubjectModel
            {
                SubjectId = DbSubject.SubjectID,
                SubjectName = DbSubject.SubjectName
            
            };
        }

        internal static void MappGradeToModel(Grades DbGrade, ref GradeViewModel gradeModel)
        {
            gradeModel = new GradeViewModel
            {
                GradeViewModelID = DbGrade.GradeID,
                GradeName = DbGrade.GradeName,
                FKCurriculumID = DbGrade.FKCurriculumID,
                IsActive = DbGrade.IsActive
            };
        }

        internal static void MappStateToStateModel(States DbState, ref StateViewModel stateModel)
        {
            stateModel = new StateViewModel
            {
                StateId = DbState.StateID,
                StateName = DbState.StateName,
                IsActive = DbState.IsActive,
                FKCountryId = DbState.FKCountryID
            };
        }

        internal static UsersProfileViewModel MappUserProfileToModel(UsersProfile userProfile)
        {
            return new UsersProfileViewModel
            {
                UsersProfileID = userProfile.UsersProfileID,
                FirstName = userProfile.FirstName,
                LastName = userProfile.LastName,
                ContactNumber = userProfile.ContactNumber,
                AlternateNumber = userProfile.AlternateNumber,
                ProfilePic = userProfile.ProfilePic,
                HeardAboutRudra = userProfile.HeardAboutRudra,
                Resume = userProfile.Resume,
                AboutMe = userProfile.AboutMe,
                DateOfBirth = userProfile.DateOfBirth,
                CreateDate = userProfile.CreateDate,
                UpdatedDate = userProfile.UpdatedDate,
                WeekHours = userProfile.WeekHours,
                WeekendHours = userProfile.WeekendHours,
                CityName = userProfile.CityName,
                IsPen = userProfile.IsPen,
                IsWebcam = userProfile.IsWebcam,
                IsInternetSpeed = userProfile.IsInternetSpeed,
                ExperienceName = userProfile.ExperienceName,
                FKUserId = userProfile.FKUserID,
                FKStateId = userProfile.FKStateID,
                GradeID = (int)userProfile.FKGradeID
            };
        }

        //internal static void MappTeacherEmailTemplateToEmailTemplateModel(TeacherEmailTemplateModel teacherTemplate , ref EmailTemplatesModel emailTemplateModel)
        //{
        //    emailTemplateModel = new EmailTemplatesModel
        //    {
        //        EmailTemplatesModelID = teacherTemplate.TeacherID,
        //        Body = teacherTemplate.bo

        //    };
        //}
    }
}