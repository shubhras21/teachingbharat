﻿using DAL.MasterEntity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Web.Mvc;

namespace BLL.Helpers
{
    public static class HelperMethods
    {
        //----------------------- Generic Methods written here -------------------------//

       public static MyDbContext context  = new MyDbContext();

        #region [GenericLists]

        public static List<T> ListofEntity<T>() where T : class
        {
            try
            {
                return context.Set<T>().ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }




        #endregion



        //DateTime datetimepicker_dark
        //string hourSeleted = datetimepicker_dark.ToString("HH");
        //string AmPm= datetimepicker_dark.ToString("tt", CultureInfo.InvariantCulture);
        //DateTime tomorrow = datetimepicker_dark.AddHours(1);
        //string hourSeleted1 = tomorrow.ToString("HH");
        //string AmPm1 = tomorrow.ToString("tt", CultureInfo.InvariantCulture);  
        // return View();

        public static void SendEmailRegardingBlock(string toAddress , string username, string reason)
        {
            try
            {
                SmtpClient client = new SmtpClient
                {
                   // Host = "smtpout.secureserver.net",
                    Host = "relay-hosting.secureserver.net",
                    Port = 25,
                    UseDefaultCredentials = false,
                    EnableSsl = false

                };
                NetworkCredential credentials = new NetworkCredential("support@rudraa.org", "123456");
                client.UseDefaultCredentials = false;
                client.Credentials = credentials;
                string fromAddress = "support@rudraa.org";
                string subject = "Support Request  : ";
                string body = "<!DOCTYPE html><html lang='en'>" +
                              "<head><meta charset='utf-8'>" +
                              "<title>Rudra</title>" +
                              "</head>" +
                              "<body>" +
                              "<div class='outerdiv'>" +
                              "<div style='padding-left: 15px; padding-right: 15px;background:white; margin-right: auto; margin-left: auto;'>" +
                              "<div class='row'><div tyle='margin-left: 144px;'></div>" +
                              "</div></div></div><br />" +
                              "<div class='container jus'> " +
                              "<div>" +
                              "<div class='col-md-12'>Hey " + username + "! " +
                              "<br/>" +
                              "<br/>" +
                              "<p style='margin-left:2%'> Your account has been blocked by the system admin for the Reason" + "[" + reason + "]" + "mentioned from admin end. You are currently not able to perform any activity from your account. </p>" +
                              "</div>" +
                              "</div>" +
                              "</div>" +
                              "<br/>" +
                              "<br/>" +
                              "<br/>" +
                             
                              "If you have any question, please feel free to contact us on +91-1111-1111 or email us at <br/>" +
                              "support@rudra.com." +
                              "<br/>" +
                              "<br/>" +
                              "Your Sincerely,<br/><br/>" +
                              "Team Rudra" +
                              "<br/>" +
                              "<br/>" +
                              "<div style='background-color: #FF7F00; padding: 20px 0;' id='footer-bottom'>" +
                              "<div class='container' style='margin-right: auto; margin-left: auto; padding-left: 15px;padding-right: 15px;'> " +
                              "<div class='col-md-3'> <div class='ft-side'></div>" +
                              "<div class='clearfix'></div></div>" +
                              "<div class='bottom clearfix'>" +
                              "<div class='container'>" +
                              "<div class='copyright'>©Copyrights " + DateTime.Now.Year + ", Rudra" +
                              "</div>" +
                              "</div>" +
                              "</div></div></div></div>" +
                              "</body>" +
                              "</html>";
                var msg = new MailMessage(fromAddress, toAddress, subject, body) { IsBodyHtml = true };
                msg.To.Add("support@rudraa.org");
                client.Send(msg);
            }
            catch (Exception ex)
            {

            }

        }


        public static bool isSiteDisable()
        {
            var result = context.MetaSettings.OrderByDescending(p => p.MetaID).FirstOrDefault();
            return result.IsEnabled;
        }

    }
}
