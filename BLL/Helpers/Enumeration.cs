﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL.Helpers
{

    public static class Enumerations
    {
        public enum ResponsHelpers
        {
            Created = 1,
            Updated=2,
            Deleted=3,
            Alreadyexsists=4,
            Doesnotexsists=5,
            EmailSentFail=6,
            Error=7,
            Block=8
        }

        public enum UserType
        {
            Super=1,
            Admin=2,
            Teacher=3,
            Student=4,
            Parent=5
        }

        public enum UserStatus
        {
            IsDeleted=1,
            IsBlocked=2,
            IsActive=3,
            IsOnline=4,
            NotApproved=5,
            Approved=6,
            Pending=7
        }
        public enum VerificationType
        {
            NotVerified=0,
            EmailVerfied=1,
            PhoneVerified=2,
            VerifiedByAdmin = 3,
        }
        public enum PermissionTo
        {
            Teacher = 1,
            Student = 2
        }
        public enum SubjectType
        {
            Primary=1,
            Secondary=2
        }
        public enum TrialRequest
        {
            Request = 1,
            Accepted = 2,
            Rejected = 3
        }
        public enum SessionsState
        {
            InProgress = 1,
            Cancelled = 2,
            Complete = 3
        }

        public enum SessionType
        {
           One=1,
           Many=2,
           Trial=3
        }

        public enum AccessoryStatus
        {
            No = 1,
            Yes = 2
        }

        public enum RequestStatus
        {
            Pending = 1,
            Accepted = 2,
            Rejected = 3
        }

        public enum QuestionLevel
        {
            Easy=1,
            Medium=2,
            Hard=3
        }
        public enum Answer
        {
            A = 1,
            B = 2,
            C = 3,
            D=4
        }
        public enum TestLevel
        {
            Easy = 1,
            Medium = 2,
            Hard = 3
        }
        public enum TestStatus
        {
            Pending=1,
            New=2,
            Completed=3,
            NotTaken=4
        }

        public enum StudentAnswerStatus
        {
            Right=1,
            Wrong=2, 
            Skipped=3

        }
        public enum TestResultStatus
        {
            Pass=1,
            Fail=2

        }

        public enum SessionStatus
        {
            Pending = 1,
            Completed = 2,
            NotTaken = 3,
            Delay =4
        }

        public enum OneOrMany
        {
            Session=0,
            Course=1,
            Monthly=2,
            Test=3
        }
        public enum Occupation
        {
            FullTimeTeacher = 1,
            Freelancer = 2,
            WorkingProfessional = 3,
            Student = 4,
            NotWorking = 5
        }

        public enum Permissions
        {
            PermissiontoAddSubAdmin = 1, 
            PermissiontoUpdateSubAdmin = 2,
            PermissiontoDeleteSubAdmin = 3,
            PermissiontoAddnewStudent = 4,
            PermissiontoUpdateStudent = 5,
            PermissiontoDeleteStudent = 6,
            PermissiontoBlockStudent = 7,
            PermissiontoApproveStudent = 8,
            PermissiontoViewStudentDetails = 9,
            PermissiontoDeleteTeacher = 10,
            PermissiontoViewDetailsofTeacher = 11,
            PermissiontoApproveTeacher = 12,
            PermissiontoBlockTeacher = 13,
        }

        public enum ActivityActions
        {
            [Display(Name = "Student has been Created")]
            StudentCreated = 1,

            [Display(Name = "Student has been Updated")]
            StudentUpdated = 2,

            [Display(Name = "Student has been Deleted")]
            StudentDeleted = 3,

            [Display(Name = "Student has been Blocked")]
            StudentBlocked
        }

        public enum ActivitySource
        {
            BacksideActivity = 1,
            FrontsideActivity = 2,
           
        }

        public enum EmailTemplates
        {
            EmailTemplateForTeacherRegistration = 1,
            EmailTemplateForStudentRegistration = 2,
            EmailTemplateForCoursePurchase = 3,
        }

        public enum SendEmailTo
        {
            AllAdmins = 1,
            AllTeachers = 2,
            AllStudents = 3,
            SelectedUsers = 4
        }

        public enum Actions
        {
            Registeration = 1,
            Purchase = 2,
            ForgetPassword = 3,
        }
        public enum TransactionStatus
        {
            pending = 1,
            success = 2,
            failure = 3,
        }

        public enum ISSkipAble
        {
            Yes = 1,
            No = 0
        }
    }
}
