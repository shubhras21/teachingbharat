﻿using DAL.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;

namespace BLL.Helpers
{
  public static  class Config
    {


        public static Int64 CurrentUser
        {
            get
            {
                if (System.Web.HttpContext.Current.Session["UserID"] == null)
                    return 0;
                else
                    return Convert.ToInt64(System.Web.HttpContext.Current.Session["UserID"]);
            }
            set
            {
                System.Web.HttpContext.Current.Session["UserID"] = value;
            }
        }

        public static Users User
        {
            get
            {
                if (System.Web.HttpContext.Current.Session["User"] == null)
                    return null;
                else
                    return (Users)System.Web.HttpContext.Current.Session["User"];
            }
            set
            {
                System.Web.HttpContext.Current.Session["User"] = value;
            }
        }

        public static UsersProfile UsersProfile
        {
            get
            {
                if (System.Web.HttpContext.Current.Session["UsersProfile"] == null)
                    return null;
                else
                    return (UsersProfile)System.Web.HttpContext.Current.Session["UsersProfile"];
            }
            set
            {
                System.Web.HttpContext.Current.Session["UsersProfile"] = value;
            }
        }

        public static string WebsiteURL
        {
            get
            {
                return Convert.ToString(ConfigurationManager.AppSettings["WebsiteURL"]);
            }
        }

        public static Int64 CBSEID
        {
            get
            {
                return Convert.ToInt64(ConfigurationManager.AppSettings["CBSE"]);
            }
        }
        public static Int64 ICSEID
        {
            get
            {
                return Convert.ToInt64(ConfigurationManager.AppSettings["ICSE"]);
            }
        }
        public static Int64 JEEID
        {
            get
            {
                return Convert.ToInt64(ConfigurationManager.AppSettings["JEE"]);
            }
        }


        // Session left hours for notification

        public static string TwentyFourHourMessage
        {
            get
            {
                return "24 Hours Left for your session.";
            }
        }

        public static string TwelveHourMessage
        {
            get
            {
                return "12 Hours Left for your session.";
            }
        }

        public static string SixHourMessage
        {
            get
            {
                return "6 Hours Left for your session.";
            }
        }



        public static string ThreeHourMessage
        {
            get
            {
                return "3 Hours Left for your session.";
            }
        }


        public static string OneHourMessage
        {
            get
            {
                return "1 Hour Left for your session.";
            }
        }



    }
}
