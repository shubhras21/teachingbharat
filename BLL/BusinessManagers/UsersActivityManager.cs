﻿using DAL.MasterEntity;
using DAL.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BLL.Helpers;
using BLL.BusinessModels.AdminModels;

namespace BLL.BusinessManagers
{
    public class UsersActivityManager
    {

        MyDbContext context = new MyDbContext();
        CustomTimeZone czone = new CustomTimeZone();

        public Int64 AddActivity(UserActivities obj)
        {
            try
            {

                obj.Time = czone.DateTimeNow();
                obj.IsActive = true;
                context.UserActivity.Add(obj);
                context.SaveChanges();
                return obj.ActivityID;
            }
            catch (Exception ex)
            {

                throw;
            }

        }

        public List<UserActivities> ListOfUserActivities(Int64 UserID)
        {
            try
            {

                List<UserActivities> list = new List<UserActivities>();
                list = context.UserActivity.Where(x => x.FKUserID == UserID && x.IsActive == true).ToList();
                if (list != null && list.Count > 0)
                {
                    list = list.OrderByDescending(x => x.Time).ToList();
                    return list;
                }
                else
                {
                    return null;
                }

            }
            catch (Exception)
            {

                throw;
            }
        }


        public List<UserActivity> LastFiveActivites(Int64 UserID)
        {
            try
            {

                List<UserActivity> list = new List<UserActivity>();
                list = context.UsersActivity.Where(x => x.UserID == UserID && x.IsSeen == false).ToList();
                if (list != null && list.Count > 0)
                {
                    list = list.OrderByDescending(x => x.ActivityDateTime).Take(5).ToList();
                    return list;
                }
                else
                {
                    return null;
                }

            }
            catch (Exception)
            {

                throw;
            }
        }

        public List<UserActivitiesModel> GetAllActivities()
        {
            var activitiesModel = new List<UserActivitiesModel>();
            using (var ctx = new MyDbContext())
            {
                var activities = (from UA in ctx.UserActivity
                                  where UA.IsActive == true
                                  orderby UA.Time descending
                                  select UA).ToList();
                if (activities.Count > 0)
                {
                    Mapper.MappUserActivitiesListToModel(activities, ref activitiesModel);

                }
            }
            return activitiesModel;
        }

        public List<UserActivitiesModel> GetFrontendActivities()
        {
            var activitiesModel = new List<UserActivitiesModel>();
            using (var ctx = new MyDbContext())
            {
                var activities = (from UA in ctx.UserActivity
                                  where UA.IsActive == true
                                  && UA.ActivitySource == (int)Enumerations.ActivitySource.FrontsideActivity
                                  orderby UA.Time descending
                                  select UA).ToList();
                if (activities.Count > 0)
                {
                    Mapper.MappUserActivitiesListToModel(activities, ref activitiesModel);

                }
            }
            return activitiesModel;
        }
        

             public List<UserActivitiesModel> GetBackendActivities()
        {
            var activitiesModel = new List<UserActivitiesModel>();
            using (var ctx = new MyDbContext())
            {
                var activities = (from UA in ctx.UserActivity
                                  where UA.IsActive == true
                                  && UA.ActivitySource == (int)Enumerations.ActivitySource.BacksideActivity
                                  orderby UA.Time descending
                                  select UA).ToList();
                if (activities.Count > 0)
                {
                    Mapper.MappUserActivitiesListToModel(activities, ref activitiesModel);

                }
            }
            return activitiesModel;
        }

        public UserActivitiesModel GetLastLoginActivity(int userID)
        {
            var Activities = new List<UserActivities>();
            var activityModel = new UserActivitiesModel();

            using (var ctx = new MyDbContext())
            {
                var lastLoginActivity = (from UA in ctx.UserActivity
                                  where UA.ActionPerformed.Contains("Login request from") &&
                                  UA.FKUserID == userID &&
                                  UA.IsActive == true
                                  orderby UA.ActivityID descending
                                  select UA).FirstOrDefault();

                if(lastLoginActivity != null)
                {
                    Mapper.MappUserActivityToModel(lastLoginActivity, ref activityModel);
                }
            }

            return activityModel;
        }

        public UserActivitiesModel GetLastLogoutActivity(int userID)
        {
            var Activities = new List<UserActivities>();
            var activityModel = new UserActivitiesModel();

            using (var ctx = new MyDbContext())
            {
                var lastLogoutActivity = (from UA in ctx.UserActivity
                                          where UA.ActionPerformed.Contains("Logout request from") &&
                                          UA.FKUserID == userID &&
                                          UA.IsActive == true
                                          orderby UA.ActivityID descending
                                          select UA).FirstOrDefault();

                if (lastLogoutActivity != null)
                {
                    Mapper.MappUserActivityToModel(lastLogoutActivity, ref activityModel);
                }
                

            }

            return activityModel;
        }



    }
}
