﻿using BLL.BusinessModels;
using DAL.Entities;
using DAL.MasterEntity;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL.BusinessManagers
{
    public static class SettingsManager
    {
        public static SettingsModel UpdateSettings(SettingsModel model)
        {
            var bannerSlide = new MetaSetting();

            using (var ctx = new MyDbContext())
            {

                Mapper.MappSettingsModelUpdateToDB(model, ref bannerSlide);
                ctx.MetaSettings.Add(bannerSlide);
                ctx.SaveChanges();

                model.MetaID = bannerSlide.MetaID;
                return model;
            }
        }
    }
}
