﻿using BLL.BusinessManagers.SessionsManager;
using BLL.BusinessModels.AdminModels;
using BLL.BusinessModels.StudentModels;
using BLL.BusinessModels.TeacherModels;
using BLL.Helpers;
using DAL.Entities;
using DAL.MasterEntity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace BLL.BusinessManagers.AdminManager
{
    public class CurriculumsManagers
    {
        MyDbContext context = new MyDbContext();

        public Int64 AddCurriculum(CurriculumsViewModel model)
        {
            try
            {
                var obj = new Curriculums
                {
                    CurriculumName = model.CurriculumName,
                    IsActive = true
                };
                context.Curriculums.Add(obj);
                context.SaveChanges();
                Int64 CurriculumID = obj.CurriculumID;
                return CurriculumID;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
       
        public List<Curriculums> ListOfCurriculums()
        {
            try
            {
                List<Curriculums> list = context.Curriculums.Where(x => x.IsActive == true).ToList();
                return list;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        
        public IEnumerable<SelectListItem> dListOfCurriculums()
        {
            try
            {
                IEnumerable<SelectListItem> Curriculums = context.Curriculums.Where(x => x.IsActive == true).ToList().Select(x => new SelectListItem()
                {
                    Text = x.CurriculumName,
                    Value = x.CurriculumID.ToString()
                });
                return Curriculums;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        public Int64 AddGrade(GradesViewModel model)
        {
            try
            {
                var obj = new Grades
                {
                    FKCurriculumID = model.CurriculumID,
                    GradeName = model.GradeName,
                    IsActive = true
                };
                context.Grades.Add(obj);
                context.SaveChanges();
                Int64 GradeID = obj.GradeID;
                return GradeID;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<Grades> ListOfGrades()
        {
            try
            {
                List<Grades> list = context.Grades.Where(x => x.IsActive == true).ToList();
                list.ForEach(x => x.CurriculumName = context.Curriculums.Where(y => y.CurriculumID == x.FKCurriculumID && y.IsActive == true).Select(y => y.CurriculumName).FirstOrDefault().ToString());
                return list;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<Subjects> ListOfSubjects()
        {
            try
            {
                List<Subjects> list = context.Subjects.Where(x => x.IsActive == true).ToList();
                list.ForEach(x => x.CurriculumName = context.Curriculums.Where(y => y.CurriculumID == x.FKCurriculumID && y.IsActive == true).Select(y => y.CurriculumName).FirstOrDefault().ToString());
                list.ForEach(x => x.GradeName = context.Grades.Where(y => y.GradeID == x.FKGradeID && y.IsActive == true).Select(y => y.GradeName).FirstOrDefault().ToString());
                return list;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public Int64 AddSubject(SubjectViewModel model)
        {
            try
            {
                var obj = new Subjects
                {
                    FKCurriculumID = model.CurriculumID,
                    FKGradeID = model.GradeID,
                    SubjectName = model.SubjectName,
                    IsActive = true
                };
                context.Subjects.Add(obj);
                context.SaveChanges();
                Int64 GradeID = obj.SubjectID;
                return GradeID;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<Topics> ListOfTopics(Int64 SubjectID)
        {
            try
            {
                List<Topics> list = context.Topics.Where(x => x.IsActive == true && x.FKSubjectID==SubjectID).ToList();
                list.ForEach(x => x.CurriculumName = context.Curriculums.Where(y => y.CurriculumID == x.FKCurriculumID && y.IsActive == true).Select(y => y.CurriculumName).FirstOrDefault().ToString());
                list.ForEach(x => x.GradeName = context.Grades.Where(y => y.GradeID == x.FKGradeID && y.IsActive == true).Select(y => y.GradeName).FirstOrDefault().ToString());
                list.ForEach(x => x.SubjectName = context.Subjects.Where(y => y.SubjectID == x.FKSubjectID && y.IsActive == true).Select(y => y.SubjectName).FirstOrDefault().ToString());
                return list;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public Subjects GetSubject(Int64 SubjectID)
        {
            Subjects obj = context.Subjects.Where(x => x.SubjectID == SubjectID && x.IsActive == true).FirstOrDefault();
            return obj;
        }

        public Int64 AddTopic(TopicsViewModel model)
        {
            try
            {
                var obj = new Topics
                {
                    FKCurriculumID = model.CurriculumID,
                    FKGradeID = model.GradeID,
                    FKSubjectID = model.SubjectID,
                    TopicName = model.TopicName,
                    TopicDescription = model.TopicDescription,
                    IsActive = true
                };
                context.Topics.Add(obj);
                context.SaveChanges();
                Int64 TopicID = obj.TopicID;
                return TopicID;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        public Int64 AddQuestionsBank(QuestionsBankViewModel model)
        {
            try
            {
                var obj = new QuestionsBank
                {
                     Question =model.Question,
                     FirstOption=model.FirstOption,
                     SecondOption =model.SecondOption,
                     ThirdOption=model.ThirdOption,
                     FourthOption=model.FourthOption,
                     AnswerKey=model.AnswerKey,
                     QuestionLevel=model.QuestionLevel,
                     FKCreatedBy=Config.CurrentUser,
                     FKTopicID =model.TopicID,
                     QuestionMarks=4,
                     IsActive = true
                };
                context.QuestionsBank.Add(obj);
                context.SaveChanges();
                Int64 TopicID = obj.QuestionBankID;
                return TopicID;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        public List<SubjectFAQS> ListOfSubjectFAQ(Int64 SubjectID)
        {
            try
            {
                List<SubjectFAQS> list = context.SubjectFAQS.Where(x => x.IsActive == true && x.FKSubjectID==SubjectID).ToList();               
                list.ForEach(x => x.SubjectName = context.Subjects.Where(y => y.SubjectID == x.FKSubjectID && y.IsActive == true).Select(y => y.SubjectName).FirstOrDefault().ToString());
                return list;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public Int64 AddSubjectFAQ(SubjectFAQViewModel model)
        {
            try
            {
                var obj = new SubjectFAQS
                {
                    Question=model.Question,
                    Answer=model.Answer,
                    QuestionBy=model.QuestionBy,
                    FKSubjectID=model.SubjectID,
                    IsActive = true
                };
                context.SubjectFAQS.Add(obj);
                context.SaveChanges();
                Int64 GradeID = obj.SubjectFAQSID;
                return GradeID;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public IEnumerable<SelectListItem> dListOfGrades(Int64 CurriculumID)
        {
            try
            {
                IEnumerable<SelectListItem> Grades = context.Grades.Where(x => x.IsActive == true && x.FKCurriculumID == CurriculumID).ToList().Select(x => new SelectListItem()
                {
                    Text = x.GradeName,
                    Value = x.GradeID.ToString()
                });
                return Grades;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public IEnumerable<SelectListItem> dListOfGrades()
        {
            try
            {
                IEnumerable<SelectListItem> Grades = context.Grades.Where(x => x.IsActive == true).ToList().Select(x => new SelectListItem()
                {
                    Text = context.Curriculums.Where(y => y.CurriculumID == x.FKCurriculumID && y.IsActive == true).Select(y => y.CurriculumName).FirstOrDefault()+ " - " + x.GradeName,
                    Value = x.GradeID.ToString()
                });
                return Grades;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public IEnumerable<SelectListItem> dListOfTeacherGrades(Int64 UserID)
        {
            try
            {
                IEnumerable<SelectListItem> Grades = context.TeacherSubjects.Where(x => x.IsActive == true && x.FKUserID==UserID).GroupBy(x => x.FKGradeID).Select(x => x.FirstOrDefault()).ToList().Select(x => new SelectListItem()
                {
                    Text = context.Curriculums.Where(y=>y.CurriculumID==x.FKCurriculumID && y.IsActive==true).Select(y=>y.CurriculumName).FirstOrDefault().ToString() +" - " + 
                           context.Grades.Where(y=>y.GradeID==x.FKGradeID && y.IsActive==true).Select(y=>y.GradeName).FirstOrDefault().ToString(),
                    Value = x.FKGradeID.ToString()
                });
                return Grades;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public IEnumerable<SelectListItem> dListOfTeacherSubjects(Int64 GradeID,Int64 UserID)
        {
            try
            {
                IEnumerable<SelectListItem> Grades = context.TeacherSubjects.Where(x => x.IsActive == true && x.FKUserID == UserID && x.FKGradeID==GradeID).ToList().Select(x => new SelectListItem()
                {
                    Text = context.Subjects.Where(y => y.SubjectID == x.FKSubjectID&& y.IsActive == true).Select(y => y.SubjectName).FirstOrDefault().ToString(), 
                    Value = x.FKSubjectID.ToString()

                });
                return Grades;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        public IEnumerable<SelectListItem> dListOfTeacherSubjects(Int64 UserID)
        {
            try
            {
                IEnumerable<SelectListItem> Subjects = context.TeacherSubjects.Where(x => x.IsActive == true && x.FKUserID == UserID).Distinct().ToList().Select(x => new SelectListItem()
                {
                    Text = GetSubjectDetail(x.FKSubjectID).CompleteSubjectName.ToString(),
                    Value = x.FKSubjectID.ToString()
                });
                return Subjects;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public Subjects GetSubjectDetail(Int64 SubjectID)
        {
            try
            {
                Subjects obj = new Subjects();
                obj = context.Subjects.Where(x => x.SubjectID == SubjectID && x.IsActive == true).FirstOrDefault();
                if(obj!=null)
                {
                    obj.CurriculumName = Convert.ToString(context.Curriculums.Where(x => x.CurriculumID == obj.FKCurriculumID).Select(x => x.CurriculumName).FirstOrDefault());
                    obj.GradeName = Convert.ToString(context.Grades.Where(x => x.GradeID == obj.FKGradeID).Select(x => x.GradeName).FirstOrDefault());

                    obj.CompleteSubjectName = Convert.ToString(context.Grades.Where(x => x.IsActive == true && x.GradeID == obj.FKGradeID).Select(x => x.GradeName).FirstOrDefault()) + " - " +obj.SubjectName;
                    return obj;
                }
                else
                {
                    return null;
                }

            }
            catch (Exception)
            {

                throw;
            }
        }

        public List<TimingModel> ListOfAvailableTimes()
        {
            try
            {
                List<TimingModel> list = new List<TimingModel>();
                list = context.Timings.Select(g => new TimingModel
                {
                    TimingId = g.TimeID,
                    FromTime = g.FromTime,
                    ToTime = g.ToTime,
                    TimeSlot=g.TimeSlot

                }).ToList();
                return list;
            }
            catch (Exception)
            {

                throw;
            }
        }

       
        public List<DaysModel> ListOfDays()
        {
            try
            {
                List<DaysModel> list = new List<DaysModel>();
                list = context.Days.Select(g => new DaysModel
                {
                    DayOfWeek= g.DayOfWeek,
                   FullName= g.FullDayName,
                    HalfName = g.DayName

                }).ToList();
                return list;
            }
            catch (Exception)
            {

                throw;
            }
        }

        public IEnumerable<SelectListItem> dListOfSubjectTopics(Int64 SubjectID)
        {
            try
            {
                IEnumerable<SelectListItem> Topics = context.Topics.Where(x => x.IsActive == true && x.FKSubjectID==SubjectID).ToList().Select(x => new SelectListItem()
                {
                    Text = x.TopicName.ToString(),
                    Value = x.TopicID.ToString()

                });
                return Topics;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public IEnumerable<SelectListItem> dListOfSelectedSubjectTopics(Int64 SubjectID,Int64[] FKTopicID)
        {
            try
            {
                IEnumerable<SelectListItem> Topics = context.Topics.Where(x => x.IsActive == true && x.FKSubjectID == SubjectID).ToList().Select(x => new SelectListItem()
                {
                    Text = x.TopicName.ToString(),
                    Value = x.TopicID.ToString(),
                    Selected=FKTopicID.ToList().Contains(x.TopicID)? true :false

                });
                return Topics;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public IEnumerable<SelectListItem> dListOfSubjects(Int64 GradeID)
        {
            try
            {
                IEnumerable<SelectListItem> Subjects = context.Subjects.Where(x => x.IsActive == true && x.FKGradeID == GradeID).ToList().Select(x => new SelectListItem()
                {
                    Text = x.SubjectName,
                    Value = x.SubjectID.ToString()
                });
                return Subjects;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public IEnumerable<SelectListItem> dListOfSubjects( )
        {
            try
            {
               var list = context.Subjects.Where(x => x.IsActive == true).ToList();
                list.ForEach(x => x.CurriculumName = context.Curriculums.Where(y => y.CurriculumID == x.FKCurriculumID && y.IsActive == true).Select(y => y.CurriculumName).FirstOrDefault().ToString());
                list.ForEach(x => x.GradeName = context.Grades.Where(y => y.GradeID == x.FKGradeID && y.IsActive == true).Select(y => y.GradeName).FirstOrDefault().ToString());


                IEnumerable<SelectListItem> Subjects = list.Select(x => new SelectListItem()
                {
                    Text = x.CurriculumName+"-"+ x.SubjectName +"-"+ x.GradeName,
                    Value = x.SubjectID.ToString()
                });
             

                return Subjects;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public IEnumerable<SelectListItem> dListOfTopics(Int64 SubjectID)
        {
            try
            {
                IEnumerable<SelectListItem> Topics = context.Topics.Where(x => x.IsActive == true && x.FKSubjectID == SubjectID).ToList().Select(x => new SelectListItem()
                {
                    Text = x.TopicName,
                    Value = x.TopicID.ToString()
                });
                return Topics;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public IEnumerable<SelectListItem> ListOfAnswers()
        {
            try
            {
                IEnumerable<SelectListItem> Answers = from Enumerations.Answer n in Enum.GetValues(typeof(Enumerations.Answer))
                                                           select new SelectListItem()
                                                           {
                                                               Text = n.ToString(),
                                                               Value = Convert.ToString((int)n)
                                                           };
                return Answers;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public IEnumerable<SelectListItem> ListOfQuestionsLevel()
        {
            try
            {
                IEnumerable<SelectListItem> QuestionsLevel = from Enumerations.QuestionLevel n in Enum.GetValues(typeof(Enumerations.QuestionLevel))
                                                      select new SelectListItem()
                                                      {
                                                          Text = n.ToString(),
                                                          Value = Convert.ToString((int)n)
                                                      };
                return QuestionsLevel;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<CurriculumsModel> GetCurriculumsList()
        {
            var curriculumsList = new List<CurriculumsModel>();
            using (var ctx = new MyDbContext())
            {
                var DbCurriculumsList = (from c in ctx.Curriculums select c).ToList();
                if (DbCurriculumsList.Count > 0)
                {
                    Mapper.MappCurriculumsListToModel(DbCurriculumsList, ref curriculumsList);
                }
            }
            return curriculumsList;
        }

        public CurriculumsModel GetCurriculumByCurriculumID(long curriculumID)
        {
            var curriculumModel = new CurriculumsModel();
            using (var ctx = new MyDbContext())
            {
                var curriculumData = (from c in ctx.Curriculums where c.CurriculumID == curriculumID select c).FirstOrDefault();
                if (curriculumData.CurriculumID > 0)
                {
                    Mapper.MappCurriculumToModel(curriculumData, ref curriculumModel);
                }
            }
            return curriculumModel;
        }

        #region [Test]

        public List<QuestionsBank> ListOfQuestionsBank()
        {
            try
            {
                List<QuestionsBank> list = context.QuestionsBank.Where(x => x.IsActive == true && x.FKTopicID > 0).ToList();
                list.ForEach(x => x.TopicName = context.Topics.Where(y => y.TopicID == x.FKTopicID && y.IsActive == true ).Select(y => y.TopicName).FirstOrDefault().ToString());
                list.ForEach(x => x.Answer = Enum.GetName(typeof(Enumerations.Answer), x.AnswerKey));
                list.ForEach(x => x.QuestionLevelName = Enum.GetName(typeof(Enumerations.QuestionLevel), x.QuestionLevel));
                return list;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        public Int64 AddTest(CreateTestViewModel model)
        {
            try {
                CustomTimeZone zone = new CustomTimeZone();
            var Test = new Test
            {
                FKGradeID = model.FKGradeID,
                FKSubjectID = model.FKSubjectID,
                FKCreatedBy = Config.CurrentUser,
                EasyCount = model.EasyCount,
                MediumCount = model.MediumCount,
                HardCount = model.HardCount,
                TestLevel = model.TestLevel,
                TotalQuestions = 25,
                TotalScores = 100,
                TestDuration = 60,
                TestName = model.TestName,
                FKCreatedDate=zone.DateTimeNow(),

                IsActive = true
            };
            context.Test.Add(Test);
            context.SaveChanges();
            Int64 statusID = Test.TestID;
            if(statusID>0)
            {
                foreach(var item in model.FKTopicID)
                {
                   MyDbContext context = new MyDbContext();
                   TestCoveredTopics topic = new TestCoveredTopics();
                    topic.FKTestID = statusID;
                    topic.FKTopicID = item;
                    topic.IsActive = true;
                    context.TestCoveredTopics.Add(topic);
                    context.SaveChanges();

                }

                    // Logic to generate questions from rudra bank 

                    var listOfEasyQuestions = context.QuestionsBank.Where(x => x.IsActive == true).ToList();//context.QuestionsBank.Where(x => x.IsActive == true && x.QuestionLevel == (int)Enumerations.QuestionLevel.Easy).Take(model.EasyCount);
                    var resultEasy = listOfEasyQuestions.Where(p => model.FKTopicID.Any(p2 => p2 == p.FKTopicID) && p.QuestionLevel == (int)Enumerations.QuestionLevel.Easy).Take(model.EasyCount);
                   // var listOfMediumQuestions = context.QuestionsBank.Where(x => x.IsActive == true && x.QuestionLevel == (int)Enumerations.QuestionLevel.Medium).Take(model.MediumCount);
                    var resultMedium = listOfEasyQuestions.Where(p => model.FKTopicID.Any(p2 => p2 == p.FKTopicID) &&  p.QuestionLevel == (int)Enumerations.QuestionLevel.Medium).Take(model.MediumCount);
                   // var listOfHardQuestions = context.QuestionsBank.Where(x => x.IsActive == true && x.QuestionLevel == (int)Enumerations.QuestionLevel.Hard).Take(model.HardCount);
                    var resultHard = listOfEasyQuestions.Where(p => model.FKTopicID.Any(p2 => p2 == p.FKTopicID) && p.QuestionLevel == (int)Enumerations.QuestionLevel.Hard).Take(model.HardCount);


                    // Pick questions from Easy questions
                    foreach (var item in resultEasy)
                    {
                        MyDbContext context = new MyDbContext();
                        TestQuestions obj = new TestQuestions();
                        obj.FKTestID = statusID;
                        obj.FKQuestionID = item.QuestionBankID;
                        obj.IsActive = true;
                        context.TestQuestions.Add(obj);
                        context.SaveChanges();
                    }

                    // Pick questions from Medium questions
                    foreach (var item in resultMedium)
                    {
                        MyDbContext context = new MyDbContext();
                        TestQuestions obj = new TestQuestions();
                        obj.FKTestID = statusID;
                        obj.FKQuestionID = item.QuestionBankID;
                        obj.IsActive = true;
                        context.TestQuestions.Add(obj);
                        context.SaveChanges();
                    }

                    // Pick questions from Hard questions
                    foreach (var item in resultHard)
                    {
                        MyDbContext context = new MyDbContext();
                        TestQuestions obj = new TestQuestions();
                        obj.FKTestID = statusID;
                        obj.FKQuestionID = item.QuestionBankID;
                        obj.IsActive = true;
                        context.TestQuestions.Add(obj);
                        context.SaveChanges();
                    }

                    return statusID;

            }
            else
            {
                return 0;
            }
            }
            catch(Exception ex)
            {
                throw ex;
            }

        }


        public int DeleteTest(Int64 TestID)
        {
            try
            {
                var isAssigned = context.TestAssigned.Any(x => x.FKTestID == TestID&& x.IsActive==true);
                if(!isAssigned)
                {
                    MyDbContext cxtz = new MyDbContext();
                    var oldObj = cxtz.Test.Where(x => x.TestID == TestID && x.IsActive == true).FirstOrDefault();
                    cxtz.Test.Attach(oldObj);
                    oldObj.IsActive = false;
                    cxtz.SaveChanges();
                    return 1;
                }
                else
                {
                    return 2;
                }
             
            }
            catch (Exception)
            {

                throw;
            }
            return 0;
        }

        public int EditTest(CreateTestViewModel model)
        {
            try
            {
                CustomTimeZone zone = new CustomTimeZone();

                // Remove previous data and add new 
                bool status = UpdateTestRecord(model.TestID);
                if(status==true)
                {

                    MyDbContext cxtz = new MyDbContext();
                    var oldObj = cxtz.Test.Where(x => x.TestID == model.TestID && x.IsActive == true).FirstOrDefault();
                    cxtz.Test.Attach(oldObj);

                    // Update Record
                    oldObj.FKGradeID = model.FKGradeID;
                    oldObj.FKSubjectID = model.FKSubjectID;
                    oldObj.FKCreatedBy = Config.CurrentUser;
                    oldObj.EasyCount = model.EasyCount;
                    oldObj.MediumCount = model.MediumCount;
                    oldObj.HardCount = model.HardCount;
                    oldObj.TestLevel = model.TestLevel;
                    oldObj.TotalQuestions = 25;
                    oldObj.TotalScores = 100;
                    oldObj.TestDuration = 60;
                    oldObj.TestName = model.TestName;
                    oldObj.FKCreatedDate = zone.DateTimeNow();
                    oldObj.IsActive = true;
                    
                    bool statusID = (cxtz.SaveChanges() > 0) ? true : false;
                    
                    if (statusID)
                    {
                        foreach (var item in model.FKTopicID)
                        {
                            MyDbContext context = new MyDbContext();
                            TestCoveredTopics topic = new TestCoveredTopics();
                            topic.FKTestID = model.TestID;
                            topic.FKTopicID = item;
                            topic.IsActive = true;
                            context.TestCoveredTopics.Add(topic);
                            context.SaveChanges();

                        }

                        // Logic to generate questions from rudra bank 

                        var listOfEasyQuestions = context.QuestionsBank.Where(x => x.IsActive == true).ToList();//context.QuestionsBank.Where(x => x.IsActive == true && x.QuestionLevel == (int)Enumerations.QuestionLevel.Easy).Take(model.EasyCount);
                        var resultEasy = listOfEasyQuestions.Where(p => model.FKTopicID.Any(p2 => p2 == p.FKTopicID) && p.QuestionLevel == (int)Enumerations.QuestionLevel.Easy).Take(model.EasyCount);
                        // var listOfMediumQuestions = context.QuestionsBank.Where(x => x.IsActive == true && x.QuestionLevel == (int)Enumerations.QuestionLevel.Medium).Take(model.MediumCount);
                        var resultMedium = listOfEasyQuestions.Where(p => model.FKTopicID.Any(p2 => p2 == p.FKTopicID) && p.QuestionLevel == (int)Enumerations.QuestionLevel.Medium).Take(model.MediumCount);
                        // var listOfHardQuestions = context.QuestionsBank.Where(x => x.IsActive == true && x.QuestionLevel == (int)Enumerations.QuestionLevel.Hard).Take(model.HardCount);
                        var resultHard = listOfEasyQuestions.Where(p => model.FKTopicID.Any(p2 => p2 == p.FKTopicID) && p.QuestionLevel == (int)Enumerations.QuestionLevel.Hard).Take(model.HardCount);


                        // Pick questions from Easy questions
                        foreach (var item in resultEasy)
                        {
                            MyDbContext context = new MyDbContext();
                            TestQuestions obj = new TestQuestions();
                            obj.FKTestID = model.TestID;
                            obj.FKQuestionID = item.QuestionBankID;
                            obj.IsActive = true;
                            context.TestQuestions.Add(obj);
                            context.SaveChanges();
                        }

                        // Pick questions from Medium questions
                        foreach (var item in resultMedium)
                        {
                            MyDbContext context = new MyDbContext();
                            TestQuestions obj = new TestQuestions();
                            obj.FKTestID = model.TestID;
                            obj.FKQuestionID = item.QuestionBankID;
                            obj.IsActive = true;
                            context.TestQuestions.Add(obj);
                            context.SaveChanges();
                        }

                        // Pick questions from Hard questions
                        foreach (var item in resultHard)
                        {
                            MyDbContext context = new MyDbContext();
                            TestQuestions obj = new TestQuestions();
                            obj.FKTestID = model.TestID;
                            obj.FKQuestionID = item.QuestionBankID;
                            obj.IsActive = true;
                            context.TestQuestions.Add(obj);
                            context.SaveChanges();
                        }

                        return 1;

                    }
                    else
                    {
                        return 0;
                    }
                }
                else
                {
                    return 2;
                }


              
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }


        public bool UpdateTestRecord(Int64 TestID)
        {
            try
            {
                if (TestID > 0)
                {
                    var isExistinAssigned = context.TestAssigned.Any(x => x.FKTestID == TestID && x.TestStatus == (int)Enumerations.TestStatus.Completed);
                    if (!isExistinAssigned)
                    {
                        MyDbContext cxt = new MyDbContext();
                        cxt.TestCoveredTopics.RemoveRange(cxt.TestCoveredTopics.Where(x => x.FKTestID == TestID));
                        cxt.SaveChanges();
                        cxt.Dispose();

                        MyDbContext cxt1 = new MyDbContext();
                        cxt1.TestQuestions.RemoveRange(cxt1.TestQuestions.Where(x => x.FKTestID == TestID));
                        cxt1.SaveChanges();
                        cxt1.Dispose();
                        return true;
                    }
                    else
                    {
                        return false;
                    }

                }
            }
            catch (Exception)
            {

                throw;
            }

            return false;
        }

        public QuestionPaperViewModel TestQuestionPaper(Int64 TestID)
        {
            QuestionPaperViewModel obj = new QuestionPaperViewModel();

            var test = context.Test.Where(x => x.TestID == TestID && x.IsActive == true).FirstOrDefault();

            if (test != null)
            {
                obj.UserID = test.FKCreatedBy;
                obj.TestID = TestID;
                obj.TestName = test.TestName;
                obj.GradeSubjectName = context.Grades.Where(x => x.GradeID == test.FKGradeID && x.IsActive == true).Select(x => new {
                    GradeName = context.Curriculums.Where(y => y.CurriculumID == x.FKCurriculumID).Select(y => y.CurriculumName).FirstOrDefault().ToString() + "  " + x.GradeName
                }).FirstOrDefault().GradeName.ToString() + "   " + context.Subjects.Where(x => x.SubjectID == test.FKSubjectID && x.IsActive == true).Select(y=>y.SubjectName).FirstOrDefault().ToString();

                obj.TestDuration = test.TestDuration.ToString();
                obj.TotalQuestions = test.TotalQuestions.ToString();
                obj.TotalMarks = test.TotalScores.ToString();
                obj.TestLevel = Enum.GetName(typeof(Enumerations.TestLevel), test.TestLevel);

                obj.listOfTopics = context.TestCoveredTopics.Where(x => x.FKTestID == TestID && x.IsActive == true).ToList();
                if(obj.listOfTopics!=null && obj.listOfTopics.Count>0)
                {
                    obj.listOfTopics.ForEach(x => x.TopicName = context.Topics.Where(y => y.TopicID == x.FKTopicID && x.IsActive == true).Select(y => y.TopicName).FirstOrDefault().ToString());
                }

                obj.listOfQuestion = context.TestQuestions.Where(x => x.FKTestID == TestID && x.IsActive == true).ToList();
                if (obj.listOfQuestion != null && obj.listOfQuestion.Count > 0)
                {
                    obj.listOfQuestion.ForEach(x => x.listOfQuestions= context.QuestionsBank.Where(y => y.QuestionBankID == x.FKQuestionID && x.IsActive == true).FirstOrDefault());
                }

                return obj;

            }
            else
            {
                return null;
            }
            return obj;

        }


        public Test GetTestDetails(Int64 TestID)
        {
            try
            {         
                Test test = new Test();
                test = context.Test.Where(x => x.TestID == TestID && x.IsActive == true).FirstOrDefault();
                if(test!=null)
                {
                    test.GradeSubjectName = context.Grades.Where(y => y.GradeID == test.FKGradeID && y.IsActive == true).Select(y => new
                    {
                        GradeName = context.Curriculums.Where(z => z.CurriculumID == y.FKCurriculumID).Select(z => z.CurriculumName).FirstOrDefault().ToString() + "  " + y.GradeName + " /" +
                             context.Subjects.Where(z => z.SubjectID == test.FKSubjectID && z.IsActive == true).Select(z => z.SubjectName).FirstOrDefault().ToString()
                    }).FirstOrDefault().GradeName.ToString();
                    test.AssignedByName = context.UsersProfile.Where(x => x.FKUserID == test.FKCreatedBy).Select(x => new { Name = x.FirstName + " " + x.LastName }).FirstOrDefault().Name.ToString();
                    var list = context.TestCoveredTopics.Where(x => x.FKTestID == test.TestID).Select(x => x.FKTopicID).ToList();
                if (list != null && list.Count > 0)
                {
                    test.FKTopicID = list.ToArray();
                }
                else
                {
                    test.FKTopicID = null;
                }
                return test;
                }
                else
                {
                    return null;
                }
            }
            catch (Exception)
            {

                throw;
            }
      
        }

        public List<Test> TeacherTestList(Int64 UserID)
        {
            try
            {
                List<Test> list = new List<Test>();
                list = context.Test.Where(x => x.FKCreatedBy == UserID && x.IsActive == true).ToList();
                if(list!=null && list.Count>0)
                {
                    list.ForEach(x => x.TestLevelName = Enum.GetName(typeof(Enumerations.TestLevel), x.TestLevel));
                    list.ForEach(x => {
                        x.GradeSubjectName = context.Grades.Where(y => y.GradeID == x.FKGradeID && x.IsActive == true).Select(y => new
                        {
                            GradeName = context.Curriculums.Where(z => z.CurriculumID == y.FKCurriculumID).Select(z => z.CurriculumName).FirstOrDefault().ToString() + "  " + y.GradeName + " /" +
                            context.Subjects.Where(z => z.SubjectID == x.FKSubjectID && z.IsActive == true).Select(z => z.SubjectName).FirstOrDefault().ToString()
                        }).FirstOrDefault().GradeName.ToString();

                        var temp = context.TestAssigned.Where(y => y.FKTestID == x.TestID&&y.IsActive==true && y.FKAssignedBy == x.FKCreatedBy).ToList();
                        if (temp != null && temp.Count > 0)
                        {
                            x.TotalAssignedCount = temp.Select(y => y.FKAssignedTo).Distinct().Count();
                            x.LastAssignedDate = temp.Last().AssignedDate.ToString("dd/MM/yyyy hh:mm tt");
                        }
                        else
                        {
                            x.TotalAssignedCount = 0;
                            x.LastAssignedDate = "N/A";
                        }
                    });
                    return list;
                }
                else
                {
                    return null;
                }

            }
            catch(Exception ex)
            {

            }
            return null;
        }




        public TestAssignViewModel AssignTest(Int64 TestID)
        {
            try
            {
          
            TestAssignViewModel obj = new TestAssignViewModel();
            var test = context.Test.Where(x => x.TestID == TestID && x.IsActive == true).FirstOrDefault();

            if (test != null)
            {
                obj.UserID = test.FKCreatedBy;               
                obj.TestID = TestID;
                obj.TestName = test.TestName;
                obj.GradeSubjectName = context.Grades.Where(x => x.GradeID == test.FKGradeID && x.IsActive == true).Select(x => new {
                    GradeName = context.Curriculums.Where(y => y.CurriculumID == x.FKCurriculumID).Select(y => y.CurriculumName).FirstOrDefault().ToString() + "  " + x.GradeName
                }).FirstOrDefault().GradeName.ToString() + "   " + context.Subjects.Where(x => x.SubjectID == test.FKSubjectID && x.IsActive == true).Select(y => y.SubjectName).FirstOrDefault().ToString();

                obj.TestDuration = test.TestDuration.ToString();
                obj.TotalQuestions = test.TotalQuestions.ToString();
                obj.TotalMarks = test.TotalScores.ToString();
                obj.TestLevel = Enum.GetName(typeof(Enumerations.TestLevel), test.TestLevel);

                obj.listOfTopics = context.TestCoveredTopics.Where(x => x.FKTestID == TestID && x.IsActive == true).ToList();
                if (obj.listOfTopics != null && obj.listOfTopics.Count > 0)
                {
                    obj.listOfTopics.ForEach(x => x.TopicName = context.Topics.Where(y => y.TopicID == x.FKTopicID && x.IsActive == true).Select(y => y.TopicName).FirstOrDefault().ToString());
                }

                SessionManager manager = new SessionManager();

                obj.listOfStudents = manager.MyStudents(obj.UserID);
                if(obj.listOfStudents!=null && obj.listOfStudents.Count>0)
                {
                        obj.ListofMyStudents = obj.listOfStudents.Select(x => new SelectListItem {
                            Text = x.FirstName + " " + x.LastName + "  (" + Convert.ToString(context.StudentGrades.Where(y => y.FKUserID == x.FKUserID).Select(y => y.GradeName).FirstOrDefault() + " )"),
                            Value = x.FKUserID.ToString()
                        }).ToList();
                }





                    return obj;

            }
            else
            {
                return null;
            }
               
            }
            catch(Exception ex)
            {

            }
            return null;
        }

        public TestAssignViewModel ReAssignTest(Int64 TestID,Int64 UserID)
        {
            try
            {

                TestAssignViewModel obj = new TestAssignViewModel();

                var test = context.Test.Where(x => x.TestID == TestID && x.IsActive == true).FirstOrDefault();

                if (test != null)
                {
                    obj.UserID = test.FKCreatedBy;
                    obj.TestID = TestID;
                    obj.TestName = test.TestName;
                    obj.GradeSubjectName = context.Grades.Where(x => x.GradeID == test.FKGradeID && x.IsActive == true).Select(x => new {
                        GradeName = context.Curriculums.Where(y => y.CurriculumID == x.FKCurriculumID).Select(y => y.CurriculumName).FirstOrDefault().ToString() + "  " + x.GradeName
                    }).FirstOrDefault().GradeName.ToString() + "   " + context.Subjects.Where(x => x.SubjectID == test.FKSubjectID && x.IsActive == true).Select(y => y.SubjectName).FirstOrDefault().ToString();

                    obj.TestDuration = test.TestDuration.ToString();
                    obj.TotalQuestions = test.TotalQuestions.ToString();
                    obj.TotalMarks = test.TotalScores.ToString();
                    obj.TestLevel = Enum.GetName(typeof(Enumerations.TestLevel), test.TestLevel);

                    obj.listOfTopics = context.TestCoveredTopics.Where(x => x.FKTestID == TestID && x.IsActive == true).ToList();
                    if (obj.listOfTopics != null && obj.listOfTopics.Count > 0)
                    {
                        obj.listOfTopics.ForEach(x => x.TopicName = context.Topics.Where(y => y.TopicID == x.FKTopicID && x.IsActive == true).Select(y => y.TopicName).FirstOrDefault().ToString());
                    }

                    SessionManager manager = new SessionManager();

                    obj.listOfStudents = manager.MyStudents(obj.UserID);
                    if (obj.listOfStudents != null && obj.listOfStudents.Count > 0)
                    {
                        obj.ListofMyStudents = obj.listOfStudents.Select(x => new SelectListItem
                        {
                            Text = x.FirstName + " " + x.LastName + "  (" + Convert.ToString(context.StudentGrades.Where(y => y.FKUserID == x.FKUserID).Select(y => y.GradeName).FirstOrDefault() + " )"),
                            Value = x.FKUserID.ToString(),
                            Selected = (UserID != 0 && x.FKUserID == UserID) ? true : false
                        }).ToList();
                    }

                    return obj;

                }
                else
                {
                    return null;
                }

            }
            catch (Exception ex)
            {

            }
            return null;
        }

        public TestAssignViewModel GenerateResultReport(Int64 TestID)
        {
            try
            {

                TestAssignViewModel obj = new TestAssignViewModel();

                var test = context.Test.Where(x => x.TestID == TestID && x.IsActive == true).FirstOrDefault();

                if (test != null)
                {
                    obj.UserID = test.FKCreatedBy;
                    obj.TestID = TestID;
                    obj.TestName = test.TestName;
                    obj.GradeSubjectName = context.Grades.Where(x => x.GradeID == test.FKGradeID && x.IsActive == true).Select(x => new {
                        GradeName = context.Curriculums.Where(y => y.CurriculumID == x.FKCurriculumID).Select(y => y.CurriculumName).FirstOrDefault().ToString() + "  " + x.GradeName
                    }).FirstOrDefault().GradeName.ToString() + "   " + context.Subjects.Where(x => x.SubjectID == test.FKSubjectID && x.IsActive == true).Select(y => y.SubjectName).FirstOrDefault().ToString();

                    obj.TestDuration = test.TestDuration.ToString();
                    obj.TotalQuestions = test.TotalQuestions.ToString();
                    obj.TotalMarks = test.TotalScores.ToString();
                    obj.TestLevel = Enum.GetName(typeof(Enumerations.TestLevel), test.TestLevel);
                    obj.FKAssignedBy = test.FKCreatedBy;
                    obj.listOfTopics = context.TestCoveredTopics.Where(x => x.FKTestID == TestID && x.IsActive == true).ToList();
                    if (obj.listOfTopics != null && obj.listOfTopics.Count > 0)
                    {
                        obj.listOfTopics.ForEach(x => x.TopicName = context.Topics.Where(y => y.TopicID == x.FKTopicID && x.IsActive == true).Select(y => y.TopicName).FirstOrDefault().ToString());
                    }

                    var temp = context.TestAssigned.Where(y => y.FKTestID == TestID && y.IsActive == true && y.FKAssignedBy == obj.FKAssignedBy && y.TestStatus==(int)Enumerations.TestStatus.Completed).ToList();
                    if (temp != null && temp.Count > 0)
                    {
                        obj.listOfAssignedStudents = temp;
                        obj.listOfAssignedStudents.ForEach(x => x.AssignedToName = context.UsersProfile.Where(y => y.FKUserID == x.FKAssignedTo).Select(y => new { Name = y.FirstName + " " + y.LastName }).FirstOrDefault().Name.ToString());
                        obj.listOfAssignedStudents.ForEach(x => x.ListofStudentAnswers = context.StudentTestAnswers.Where(y => y.FKStudentID == x.FKAssignedTo && y.FKAssignedTestID == x.TestAssignedID && y.IsActive == true).ToList());
                        if (obj.listOfAssignedStudents != null && obj.listOfAssignedStudents.Count > 0)
                        {
                            int count = 0;
                            obj.RankViewModel = new List<CalculateRankViewModel>();
                            foreach (var item in obj.listOfAssignedStudents)
                            {
                                count++;
                                var last = item.ListofStudentAnswers.Last();
                                var First = item.ListofStudentAnswers.First();
                                var EndTime = last.QuestionSubmitTime;
                                var StartTime = First.QuestionStartTime;
                              
                                int pass = 0;
                                int fail = 0;         
                                foreach (var answers in item.ListofStudentAnswers)
                                {        
                                    
                                                               
                                    if (answers.IsSkipped != true)
                                    {
                                        int status = context.QuestionsBank.Where(x => x.QuestionBankID == answers.FKTestQuestionsID).Select(x => x.AnswerKey).FirstOrDefault();
                                        if (answers.FKStudentAnswer == status)
                                        {
                                            pass = pass + 1;
                                            answers.StudentAnswerStatus = (int)Enumerations.StudentAnswerStatus.Right;                                   
                                        }
                                        else
                                        {
                                            fail = fail + 1;
                                            answers.StudentAnswerStatus = (int)Enumerations.StudentAnswerStatus.Wrong;                                   
                                        }
                                    }
                                    else
                                    {
                                        answers.StudentAnswerStatus = (int)Enumerations.StudentAnswerStatus.Skipped;
                                    }  
                                    if(last.Equals(answers))
                                    {
                                        item.GainedScores = pass * 4;
                                        double span = (EndTime - StartTime).TotalMinutes;
                                        item.TotalSpendTime = span;
                                        item.TestStartTime = StartTime;
                                        if (pass > fail)
                                            item.TestStatusPassFail = "Pass";
                                        else
                                            item.TestStatusPassFail = "Fail";
                                    }                                                                                      
                                }

                                CalculateRankViewModel rankmodel = new CalculateRankViewModel();
                                rankmodel.Index = count;
                                rankmodel.ObtainedMarks = item.GainedScores;
                                rankmodel.UserID = item.FKAssignedTo;
                                obj.RankViewModel.Add(rankmodel);

                            }
                           
                            obj.TotalPass = obj.listOfAssignedStudents.Where(x => x.TestStatusPassFail == "Pass").Count();
                            obj.TotalFail = obj.listOfAssignedStudents.Where(x => x.TestStatusPassFail == "Fail").Count();
                            obj.TotalStudents = obj.listOfAssignedStudents.Count();

                        }
                        return obj;
                    }
                    else
                    {
                        obj.listOfAssignedStudents = null;
                    }

                    return obj;

                }
                else
                {
                    return null;
                }

            }
            catch (Exception ex)
            {

            }
            return null;
        }



        public TestAssignViewModel StudentsResultReport(Int64 UserID)
        {
            try
            {
                    TestAssignViewModel obj = new TestAssignViewModel();
                    CustomTimeZone czone = new CustomTimeZone();         
                    var temp = context.TestAssigned.Where(y =>  y.IsActive == true && y.FKAssignedBy==UserID).ToList();
                    if (temp != null && temp.Count > 0)
                    {
                    obj.listOfAssignedStudents = temp;
                    obj.listOfAssignedStudents.ForEach(x=> {
                        x.Test = GetTestDetails(x.FKTestID);
                    });
                        obj.listOfAssignedStudents.ForEach(x => x.AssignedToName = context.UsersProfile.Where(y => y.FKUserID == x.FKAssignedTo).Select(y => new { Name = y.FirstName + " " + y.LastName }).FirstOrDefault().Name.ToString());
                        obj.listOfAssignedStudents.ForEach(x => x.ListofStudentAnswers = context.StudentTestAnswers.Where(y => y.FKStudentID == x.FKAssignedTo && y.FKAssignedTestID == x.TestAssignedID && y.IsActive == true).ToList());
                        if (obj.listOfAssignedStudents != null && obj.listOfAssignedStudents.Count > 0)
                        {
                            foreach (var item in obj.listOfAssignedStudents)
                            {


                            if (item.ListofStudentAnswers != null && item.ListofStudentAnswers.Count > 0)
                            {
                                var last = item.ListofStudentAnswers.Last();
                                var First = item.ListofStudentAnswers.First();
                                var EndTime = last.QuestionSubmitTime;
                                var StartTime = First.QuestionStartTime;

                                int pass = 0;
                                int fail = 0;
                                foreach (var answers in item.ListofStudentAnswers)
                                {
                                    if (answers.IsSkipped != true)
                                    {
                                        int status = context.QuestionsBank.Where(x => x.QuestionBankID == answers.FKTestQuestionsID).Select(x => x.AnswerKey).FirstOrDefault();
                                        if (answers.FKStudentAnswer == status)
                                        {
                                            pass = pass + 1;
                                            answers.StudentAnswerStatus = (int)Enumerations.StudentAnswerStatus.Right;
                                        }
                                        else
                                        {
                                            fail = fail + 1;
                                            answers.StudentAnswerStatus = (int)Enumerations.StudentAnswerStatus.Wrong;
                                        }
                                    }
                                    else
                                    {
                                        answers.StudentAnswerStatus = (int)Enumerations.StudentAnswerStatus.Skipped;
                                    }
                                    if (last.Equals(answers))
                                    {
                                        item.GainedScores = pass * 4;
                                        double span = (EndTime - StartTime).TotalMinutes;
                                        item.TotalSpendTime = span;
                                        item.TestStartTime = StartTime;
                                        if (pass > fail)
                                            item.TestStatusPassFail = "Pass";
                                        else
                                            item.TestStatusPassFail = "Fail";
                                        item.TestStatusName = "Completed";
                                    }
                                }
                               }
                              else
                              {
                                item.ListofStudentAnswers = null;
                                item.TestStatusPassFail = "N/A";
                                item.GainedScores = 0;
                                var startTime = item.TestHappenedTime.AddHours(1);
                                var endTime = czone.DateTimeNow();

                                if (item.TestStatus == (int)Enumerations.TestStatus.Completed)
                                    item.TestStatusName = "Completed";
                                else if (item.TestStatus == (int)Enumerations.TestStatus.New && startTime > endTime)
                                    item.TestStatusName = "Pending";
                                else if (item.TestStatus == (int)Enumerations.TestStatus.New && startTime < endTime)
                                {
                                    item.TestStatusName = "Not Taken";
                                    item.TestStatus = (int)Enumerations.TestStatus.NotTaken;
                                }

                            }
                            }

                            obj.TotalPass = obj.listOfAssignedStudents.Where(x => x.TestStatusPassFail == "Pass").Count();
                            obj.TotalFail = obj.listOfAssignedStudents.Where(x => x.TestStatusPassFail == "Fail").Count();
                            obj.TotalPending = obj.listOfAssignedStudents.Where(x => x.TestStatus == (int)Enumerations.TestStatus.Pending || x.TestStatus == (int)Enumerations.TestStatus.New || x.TestStatus == (int)Enumerations.TestStatus.NotTaken).Count(); 
                            obj.TotalStudents = obj.listOfAssignedStudents.Count();

                        }
                        return obj;
                   
                    return obj;

                }
                else
                {
                    return null;
                }

            }
            catch (Exception ex)
            {

            }
            return null;
        }


        public AnswerSheetViewModel GetAnswerSheet(Int64 StudentID, Int64 TestAssignedID)
        {
            try
            {
                TestAssigned isTrue = context.TestAssigned.Where(x => x.FKAssignedTo == StudentID && x.TestAssignedID==TestAssignedID && x.IsActive == true && x.FKAssignedBy == Config.CurrentUser).FirstOrDefault();
                if(isTrue!=null)
                {
                    AnswerSheetViewModel obj = new AnswerSheetViewModel();
                    var test = context.Test.Where(x => x.TestID == isTrue.FKTestID && x.IsActive == true).FirstOrDefault();
                    if (test != null)
                    {
                        obj.StudentName = context.UsersProfile.Where(x=>x.FKUserID==isTrue.FKAssignedTo).Select(x=>new {Name=x.FirstName+ " " + x.LastName }).FirstOrDefault().Name.ToString();
                        obj.UserID = test.FKCreatedBy;
                        obj.TestID = test.TestID;
                        obj.TestName = test.TestName;
                        obj.GradeSubjectName = context.Grades.Where(x => x.GradeID == test.FKGradeID && x.IsActive == true).Select(x => new
                        {
                            GradeName = context.Curriculums.Where(y => y.CurriculumID == x.FKCurriculumID).Select(y => y.CurriculumName).FirstOrDefault().ToString() + "  " + x.GradeName
                        }).FirstOrDefault().GradeName.ToString() + "   " + context.Subjects.Where(x => x.SubjectID == test.FKSubjectID && x.IsActive == true).Select(y => y.SubjectName).FirstOrDefault().ToString();

                        obj.TestDuration = test.TestDuration.ToString();
                        obj.TotalQuestions = test.TotalQuestions.ToString();
                        obj.TotalMarks = test.TotalScores.ToString();
                        obj.TestLevel = Enum.GetName(typeof(Enumerations.TestLevel), test.TestLevel);
                    }

                    obj.listOfStudentTestAnswers = new List<StudentTestAnswers>();
                    obj.listOfStudentTestAnswers = context.StudentTestAnswers.Where(x => x.FKAssignedTestID == TestAssignedID && x.FKStudentID == StudentID && x.IsActive == true && x.IsSkipped==false).ToList();
                    if(obj.listOfStudentTestAnswers != null && obj.listOfStudentTestAnswers.Count>0)
                    {
                        var last = obj.listOfStudentTestAnswers.Last();
                        var First = obj.listOfStudentTestAnswers.First();
                        var EndTime = last.QuestionSubmitTime;
                        var StartTime = First.QuestionStartTime;

                        double span = (EndTime - StartTime).TotalMinutes;
                        obj.TotalSpendTime = span;

                        obj.listOfStudentTestAnswers.ForEach(x => {
                           
                            x.QuestionBank = context.QuestionsBank.Where(y => y.QuestionBankID == x.FKTestQuestionsID && y.IsActive == true).FirstOrDefault();
                            if(x.IsSkipped!=true)
                            { 
                            if (x.QuestionBank.AnswerKey==x.FKStudentAnswer)
                            {
                                obj.RightCount = obj.RightCount + 1;
                                x.TrueAnswer = true;
                            }
                            else 
                            {
                                obj.WrongCount = obj.WrongCount + 1;
                                x.TrueAnswer = false;
                            }
                            }
                            else
                            {
                               obj.SkippedCount = obj.SkippedCount + 1;
                            }
                        });

                        obj.MarksObtained=obj.RightCount*4;
                        obj.TestAssignDate = isTrue.AssignedDate;
                        obj.TestHappenDate = obj.listOfStudentTestAnswers.First().QuestionStartTime;
                        if(obj.MarksObtained>50)
                        {
                            obj.PassFailedStatus = "Pass";
                        }
                        else
                        {
                            obj.PassFailedStatus = "Fail";
                        }
                        return obj;
                    }
                }
                else
                {
                    return null;
                }


                return null;
            }
            catch (Exception ex)
            {
                 
                throw;
            }
        }


        public AnswerSheetViewModel GetTestResult(Int64 StudentID, Int64 TestAssignedID)
        {
            try
            {
                TestAssigned isTrue = context.TestAssigned.Where(x => x.FKAssignedTo == StudentID && x.TestAssignedID == TestAssignedID && x.IsActive == true ).FirstOrDefault();
                if (isTrue != null)
                {
                    AnswerSheetViewModel obj = new AnswerSheetViewModel();
                    var test = context.Test.Where(x => x.TestID == isTrue.FKTestID && x.IsActive == true).FirstOrDefault();
                    if (test != null)
                    {
                        obj.Test = GetTestDetails(test.TestID);
                        obj.UserProfile = context.UsersProfile.Where(x => x.FKUserID == StudentID).FirstOrDefault();
                        obj.StudentName = obj.UserProfile.FirstName + " " + obj.UserProfile.LastName;  //context.UsersProfile.Where(x => x.FKUserID == isTrue.FKAssignedTo).Select(x => new { Name = x.FirstName + " " + x.LastName }).FirstOrDefault().Name.ToString();
                        obj.UserID = test.FKCreatedBy;
                        obj.TestID = test.TestID;
                        obj.TestName = test.TestName;
                        obj.GradeSubjectName = context.Grades.Where(x => x.GradeID == test.FKGradeID && x.IsActive == true).Select(x => new
                        {
                            GradeName = context.Curriculums.Where(y => y.CurriculumID == x.FKCurriculumID).Select(y => y.CurriculumName).FirstOrDefault().ToString() + "  " + x.GradeName
                        }).FirstOrDefault().GradeName.ToString() + "   " + context.Subjects.Where(x => x.SubjectID == test.FKSubjectID && x.IsActive == true).Select(y => y.SubjectName).FirstOrDefault().ToString();

                        obj.TestDuration = test.TestDuration.ToString();
                        obj.TotalQuestions = test.TotalQuestions.ToString();
                        obj.TotalMarks = test.TotalScores.ToString();
                        obj.TestLevel = Enum.GetName(typeof(Enumerations.TestLevel), test.TestLevel);
                    }
                    obj.TeacherName=context.UsersProfile.Where(x=>x.FKUserID==isTrue.FKAssignedBy).Select(x => new { Name = x.FirstName + " " + x.LastName }).FirstOrDefault().Name.ToString();
                    obj.listOfStudentTestAnswers = new List<StudentTestAnswers>();
                    obj.listOfStudentTestAnswers = context.StudentTestAnswers.Where(x => x.FKAssignedTestID == TestAssignedID && x.FKStudentID == StudentID && x.IsActive == true && x.IsSkipped == false).ToList();
                    if (obj.listOfStudentTestAnswers != null && obj.listOfStudentTestAnswers.Count > 0)
                    {
                        var last = obj.listOfStudentTestAnswers.Last();
                        var First = obj.listOfStudentTestAnswers.First();
                        var EndTime = last.QuestionSubmitTime;
                        var StartTime = First.QuestionStartTime;

                        double span = (EndTime - StartTime).TotalMinutes;
                        obj.TotalSpendTime = span;
                        int slowCount = 0;
                        int fastCount = 0;
                       float FastCorrect = 0;
                        float SlowCorrect = 0;
                        float FastIncorrect = 0;
                        float SlowIncorrent = 0;

                        obj.listOfStudentTestAnswers.ForEach(x => {

                            x.QuestionBank = context.QuestionsBank.Where(y => y.QuestionBankID == x.FKTestQuestionsID && y.IsActive == true).FirstOrDefault();
                            if (x.IsSkipped != true)
                            {
                           
                                double spans = (x.QuestionSubmitTime - x.QuestionStartTime).TotalSeconds;

                                if (spans > 144)
                                {
                                    slowCount = slowCount + 1;
                                }
                                else
                                {
                                    fastCount = fastCount + 1;
                                }

                                if (x.QuestionBank.AnswerKey == x.FKStudentAnswer)
                                {
  
                                    obj.RightCount = obj.RightCount + 1;
                                    x.TrueAnswer = true;
                                    if (spans < 144)
                                    {
                                        FastCorrect = FastCorrect + 1;

                                    }
                                    else
                                    {
                                        SlowCorrect = SlowCorrect + 1;
                                    }

                                }
                                else
                                {
                                    obj.WrongCount = obj.WrongCount + 1;
                                    x.TrueAnswer = false;
                                    if (spans > 144)
                                    {
                                        SlowIncorrent = SlowIncorrent + 1;

                                    }
                                    else
                                    {
                                        FastIncorrect = FastIncorrect + 1;
                                    }
                                }
                            }
                            else
                            {
                                obj.SkippedCount = obj.SkippedCount + 1;
                            }
                        });


                        var totalQSolved = obj.listOfStudentTestAnswers.Count();
                        obj.FastCountPercentage = fastCount*100 / totalQSolved;
                        obj.SlowCountPercentage = slowCount *100 / totalQSolved;

                        obj.FastCorrect = FastCorrect * 100 / totalQSolved;
                        obj.FastIncorrect = FastIncorrect * 100 / totalQSolved;
                        obj.SlowCorrect = SlowCorrect * 100 / totalQSolved;
                        obj.SlowIncorrect = SlowIncorrent * 100 / totalQSolved;

                        obj.MarksObtained = obj.RightCount * 4;
                        obj.TestAssignDate = isTrue.AssignedDate;
                        obj.TestHappenDate = obj.listOfStudentTestAnswers.First().QuestionStartTime;
                       
                        if (obj.MarksObtained > 50)
                        {
                            obj.PassFailedStatus = "Pass";
                        }
                        else
                        {
                            obj.PassFailedStatus = "Fail";
                        }

                        // Get Test Assigned
                        TestAssignViewModel objs = GenerateResultReport(test.TestID);
                        List<CalculateRankViewModel> lsts = objs.RankViewModel.OrderByDescending(x=>x.ObtainedMarks).ToList();
                        int c = 0;
                        lsts.ForEach(x => {
                            c++;
                            x.Index = c;
                        });
                        CalculateRankViewModel md = lsts.Where(x => x.UserID == isTrue.FKAssignedTo).FirstOrDefault();
                        obj.IndividualRank = md.Index;
                        obj.TotalAssignedStudent = lsts.Count();
                        obj.TAID = isTrue.TestAssignedID;
                        obj.SID = isTrue.FKAssignedTo;
                        return obj;
                    }
                }
                else
                {
                    return null;
                }


                return null;
            }
            catch (Exception ex)
            {

                throw;
            }
        }

        public int AssignTest(TestAssignViewModel model)
        {
            try
            {
                model.FKAssignedBy = Config.CurrentUser;
                CustomTimeZone czone = new CustomTimeZone();
                if(model.FKStudentID!=null && model.FKStudentID.Count>0)
                {
                    foreach(var item in model.FKStudentID)
                    {
                        model.FKAssignedTo = 0;
                        model.FKAssignedTo = item;
                        var isExist = TestAssignedStatus(model);
                        if (isExist == true)
                        {
                            MyDbContext context = new MyDbContext();
                            var AssignTest = new TestAssigned
                            {
                                AssignedDate = czone.DateTimeNow(),
                                FKAssignedBy = Config.CurrentUser,
                                FKAssignedTo = item,
                                FKTestID = model.TestID,
                                TestHappenedTime = model.TestHappenDate,
                                TestEndTime=model.TestHappenDate.AddHours(1),
                                TestStatus = (int)Enumerations.TestStatus.New,
                                IsActive = true
                            };
                            context.TestAssigned.Add(AssignTest);
                            context.SaveChanges();
                        }                             
                    }



                    // Add session request status into notifications 
                    string teacherName = context.UsersProfile.Where(x => x.FKUserID == model.FKAssignedBy).Select(x => new { Name = x.FirstName + " " + x.LastName }).FirstOrDefault().Name.ToString();

                    Notifications nObj = new Notifications();
                    nObj.ActionPerformed = "You are been assigned for a new test by  " + teacherName;
                    nObj.FKSessionID = 0;
                    nObj.FKUserID = Convert.ToInt64(model.FKAssignedTo);
                    nObj.IsCourse = (int)Enumerations.OneOrMany.Test;

                    NotificationManager nManager = new NotificationManager();
                    Int64 ID = nManager.AddNotification(nObj);


                    // Add session request status into notifications 
                    string StudentName = context.UsersProfile.Where(x => x.FKUserID == model.FKAssignedTo).Select(x => new { Name = x.FirstName + " " + x.LastName }).FirstOrDefault().Name.ToString();

                    Notifications nObj1 = new Notifications();
                    nObj1.ActionPerformed = "You assigned new test to   " + StudentName;
                    nObj1.FKSessionID = 0;
                    nObj1.FKUserID = Convert.ToInt64(model.FKAssignedBy);
                    nObj1.IsCourse = (int)Enumerations.OneOrMany.Test;

                    Int64 IDs = nManager.AddNotification(nObj1);






                    return 1;
                }
                else
                {
                    return 0;
                }
               
            }
            catch(Exception ex)
            {

            }
            return 0;
        }

        public bool TestAssignedStatus(TestAssignViewModel model)
        {
            try
            {
                MyDbContext context = new MyDbContext();
                var isExist = context.TestAssigned.Where(x => x.FKAssignedBy == model.FKAssignedBy && x.FKAssignedTo == model.FKAssignedTo && x.FKTestID == model.TestID && x.IsActive == true).FirstOrDefault();
                if (isExist != null)
                {
                    MyDbContext cxt = new MyDbContext();
                    context.TestAssigned.Attach(isExist);
                    isExist.IsActive = false;
                    return (context.SaveChanges() > 0) ? true : false;
                }
                else
                {
                    return true;
                }
          
            }
            catch (Exception ex)
            {

            }
            return false;
        }


        public bool CancelTestAssigned(Int64 TestAssignedID)
        {
            try
            {
                MyDbContext context = new MyDbContext();
                var isExist = context.TestAssigned.Where(x => x.FKAssignedBy == Config.CurrentUser && x.TestAssignedID==TestAssignedID && x.IsActive == true).FirstOrDefault();
                if (isExist != null)
                {
                    MyDbContext cxt = new MyDbContext();
                    context.TestAssigned.Attach(isExist);
                    isExist.IsActive = false;
                    return (context.SaveChanges() > 0) ? true : false;
                }
                else
                {
                    return true;
                }

            }
            catch (Exception ex)
            {

            }
            return false;
        }

        #endregion


        #region [Students test related]

        public StudentsTestListViewModel StudentTestList(Int64 UserID)
        {
            try
            {
                StudentsTestListViewModel model = new StudentsTestListViewModel();
                CustomTimeZone czone = new CustomTimeZone();
                model.listOfAllTests = new List<TestAssigned>();
                model.listOfUpcomingTests = new List<TestAssigned>();
                model.listOfAllTests = context.TestAssigned.Where(x => x.FKAssignedTo == UserID && x.IsActive == true).ToList();
                if(model.listOfAllTests != null && model.listOfAllTests.Count>0)
                {
                    model.listOfAllTests.ForEach(x => x.Test = context.Test.Where(y => y.TestID == x.FKTestID && y.IsActive == true).FirstOrDefault());
                    model.listOfAllTests.ForEach(x => x.AssignedByName = context.UsersProfile.Where(y => y.FKUserID == x.FKAssignedBy).Select(y => new{ Name = y.FirstName + " " + y.LastName }).FirstOrDefault().Name.ToString());
                    model.listOfAllTests.ForEach(x => x.Test.GradeSubjectName = context.Subjects.Where(y => y.SubjectID == x.Test.FKSubjectID && y.IsActive == true).Select(y => y.SubjectName).FirstOrDefault().ToString());
                    model.listOfAllTests.ForEach(x => x.TesthappenedString = x.TestHappenedTime.ToString("dd MMMM, yyyy hh:mm tt"));

                    model.listOfAllTests.ForEach(x => {
                        var startTime = x.TestHappenedTime.AddHours(1);
                        var endTime = czone.DateTimeNow();
                        if (x.TestStatus == (int)Enumerations.TestStatus.Completed)
                            x.TestStatusName = "Completed";
                        else if (x.TestStatus == (int)Enumerations.TestStatus.New && startTime > endTime)
                            x.TestStatusName = "Pending";
                        else if (x.TestStatus == (int)Enumerations.TestStatus.New && startTime < endTime)
                        {   x.TestStatusName = "Not Taken";
                            x.TestStatus = (int)Enumerations.TestStatus.NotTaken;
                        }
                                
                    });
                }
                else
                {
                    return model;
                }
         
                foreach(var item in model.listOfAllTests)
                {
                    if (item.TestStatus !=(int)Enumerations.TestStatus.Completed)
                    {
                        var rTime = item.TestHappenedTime;
                        var todayTime = czone.DateTimeNow();
                        var twodayoldtime = rTime.AddDays(-1);
                        if (rTime >= todayTime.AddHours(-1) && todayTime > twodayoldtime)
                        {
                            TestAssigned obj = new TestAssigned();
                            obj = item;
                            model.listOfUpcomingTests.Add(obj);
                        }
                    }
                }

                if (model.listOfUpcomingTests != null && model.listOfUpcomingTests.Count > 0)
                {
                    model.listOfUpcomingTests.ForEach(x => x.Test = context.Test.Where(y => y.TestID == x.FKTestID && y.IsActive == true).FirstOrDefault());
                    model.listOfUpcomingTests.ForEach(x => x.AssignedByName = context.UsersProfile.Where(y => y.FKUserID == x.FKAssignedBy).Select(y => new { Name = y.FirstName + " " + y.LastName }).FirstOrDefault().Name.ToString());
                    model.listOfUpcomingTests.ForEach(x => x.Test.GradeSubjectName = context.Subjects.Where(y => y.SubjectID == x.Test.FKSubjectID && y.IsActive == true).Select(y => y.SubjectName).FirstOrDefault().ToString());
                    model.listOfUpcomingTests.ForEach(x => x.TesthappenedString = x.TestHappenedTime.ToString("dd MMMM, yyyy hh:mm tt"));
                    model.listOfUpcomingTests.ForEach(x => x.RemainingTime = (x.TestHappenedTime - czone.DateTimeNow()).ToString(@"hh\:mm\:ss"));
                }
                else
                {
                    model.listOfUpcomingTests = null;
                    return model;
                }
                return model;

            }
            catch(Exception ex)
            {

            }

            return null;

        }


        public TestAssigned GetTestAssignedDetail(Int64 TestAssignedID)
        {
            try
            {
                CustomTimeZone czone = new CustomTimeZone();                
                var IsExist = context.TestAssigned.Where(x => x.TestAssignedID == TestAssignedID && x.IsActive == true && x.TestStatus!=(int)Enumerations.TestStatus.Completed).FirstOrDefault() ;
                if(IsExist!=null)
                {

                    var startTime = IsExist.TestHappenedTime.AddMinutes(-5);                
                    var endTime = IsExist.TestEndTime;
                    var currentTime = czone.DateTimeNow();
                    if(currentTime >= startTime && currentTime <= endTime)
                    {
                        return IsExist;
                    }
                    else
                    {
                        return null;
                    }                   
                }
                else
                {
                    return null;
                }
           
            }
            catch(Exception ex)
            {
                return null;
                throw ex;
            }
            return null;
        }



        #endregion


        #region [Submitting test answers]

        public List<TestQuestionViewModel> PopulateQuestion(Int64 TestID,Int64 TestAssignedID)
        {

            try
            {
                List<TestQuestionViewModel> list = new List<TestQuestionViewModel>();
                List<TestQuestions> TestQuestions = new List<TestQuestions>();
                TestQuestions = context.TestQuestions.Where(x => x.FKTestID == TestID && x.IsActive == true).ToList();
                if(TestQuestions!=null && TestQuestions.Count>0)
                {
                    foreach(var item in TestQuestions)
                    {
                        TestQuestionViewModel obj = new TestQuestionViewModel();
                        obj.QuestionBank = context.QuestionsBank.Where(y => y.QuestionBankID == item.FKQuestionID && y.IsActive == true).FirstOrDefault();
                        obj.TestAssignID = TestAssignedID;
                        obj.TestID = TestID;
                        obj.QuestionStartTime = null;
                        obj.QuestionEndTime = null;
                        list.Add(obj);
                    }
                   
                    return list;
                }
                else
                {
                    return null;
                }


                return null;
            }
            catch (Exception ex)
            {
                throw;
            }
        }


        public int AddStudentAnswers(StudentTestAnswers obj)
        {
            try
            {
                MyDbContext context = new MyDbContext();
                var isExist = context.StudentTestAnswers.Where(x => x.FKStudentID == obj.FKStudentID && x.FKAssignedTestID == obj.FKAssignedTestID && x.FKTestQuestionsID == obj.FKTestQuestionsID && x.IsActive == true).FirstOrDefault();
                if(isExist==null)
                {
                    context.StudentTestAnswers.Add(obj);
                    context.SaveChanges();
                    return 1;
                }
                else
                {
                    return 0;
                }
            }
            catch (Exception ex)
            {

                throw;
            }
        }



        public bool UpdateTestAssignedStatus(Int64 AssignedTestID)
        {
            try
            {
                MyDbContext cxtz = new MyDbContext();
                var isExist = cxtz.TestAssigned.Where(x => x.TestAssignedID == AssignedTestID && x.IsActive == true).FirstOrDefault();
                if(isExist!=null)
                {
                    cxtz.TestAssigned.Attach(isExist);
                    isExist.TestStatus = (int)Enumerations.TestStatus.Completed;
                    return (cxtz.SaveChanges()>0)?true:false;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception)
            {

                throw;
            }

        }


        public TestAssigned GetAssignedDetail(Int64 TestAssignedID)
        {
           try
            {
                TestAssigned obj = new TestAssigned();
                obj = context.TestAssigned.Where(x => x.TestAssignedID == TestAssignedID && x.IsActive == true).FirstOrDefault();
                if(obj!=null)
                {
                    obj.Test = context.Test.Where(y => y.TestID == obj.FKTestID && y.IsActive == true).FirstOrDefault();
                    obj.AssignedByName = context.UsersProfile.Where(y => y.FKUserID == obj.FKAssignedBy).Select(y => new { Name = y.FirstName + " " + y.LastName }).FirstOrDefault().Name.ToString();
                    obj.Test.GradeSubjectName = context.Subjects.Where(y => y.SubjectID == obj.Test.FKSubjectID && y.IsActive == true).Select(y => y.SubjectName).FirstOrDefault().ToString();
                    obj.TesthappenedString = obj.TestHappenedTime.ToString("dd MMMM, yyyy hh:mm tt");
                    obj.ListofStudentAnswers = context.StudentTestAnswers.Where(y => y.FKStudentID == obj.FKAssignedTo && y.FKAssignedTestID == obj.TestAssignedID && y.IsActive == true).ToList();
                    if (obj.ListofStudentAnswers != null && obj.ListofStudentAnswers.Count > 0)
                    {
                        var last = obj.ListofStudentAnswers.Last();
                        var First = obj.ListofStudentAnswers.First();
                        var EndTime = last.QuestionSubmitTime;
                        var StartTime = First.QuestionStartTime;
                        int pass = 0;
                        int fail = 0;
                        foreach (var answers in obj.ListofStudentAnswers)
                        {
                            if (answers.IsSkipped != true)
                            {
                                int status = context.QuestionsBank.Where(x => x.QuestionBankID == answers.FKTestQuestionsID).Select(x => x.AnswerKey).FirstOrDefault();
                                if (answers.FKStudentAnswer == status)
                                {
                                    pass = pass + 1;
                                    answers.StudentAnswerStatus = (int)Enumerations.StudentAnswerStatus.Right;
                                }
                                else
                                {
                                    fail = fail + 1;
                                    answers.StudentAnswerStatus = (int)Enumerations.StudentAnswerStatus.Wrong;
                                }
                            }
                            else
                            {
                                answers.StudentAnswerStatus = (int)Enumerations.StudentAnswerStatus.Skipped;
                            }
                        }

                        obj.GainedScores = pass * 4;
                        double span = (EndTime - StartTime).TotalMinutes;
                        obj.TotalSpendTime = span;
                        obj.TestStartTime = StartTime;
                        if (pass > fail)
                            obj.TestStatusName = "Pass";
                        else
                            obj.TestStatusName = "Fail";
                        return obj;
                    }
                }
                else
                {
                    return null;
                }

            }
            catch (Exception)
            {
                throw;
            }
            return null;
        }




        #endregion

    }
}
