﻿using BLL.BusinessModels.AdminModels;
using BLL.Helpers;
using DAL.Entities;
using DAL.MasterEntity;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL.BusinessManagers.AdminManager
{
    public class AdminManager
    {

        #region [TEACHER MANAGER METHODS]


        public List<UserViewModel> GetAllAdminsList()
        {
            var usersViewModelList = new List<UserViewModel>();

            try
            {
                MyDbContext context = new MyDbContext();

                var adminsList = (from u in context.Users
                                  where u.UserType == (int)Enumerations.UserType.Admin
                                  && u.UserStatus == (int)Enumerations.UserStatus.IsActive
                                  select u).ToList();

                if (adminsList.Count > 0)
                {
                    adminsList.ForEach(x => x.UsersProfile = context.UsersProfile.Where(i => i.FKUserID == x.UserID).FirstOrDefault());
                    Mapper.MappUsersListToModel(adminsList, ref usersViewModelList);
                }
            }
            catch (Exception ex)
            {

            }
            return usersViewModelList;
        }

        public void CreateNewAdmin(UserViewModel userViewModel)
        {
            try
            {
                var DbUser = new Users();
                Mapper.MappUserViewModelToUserEntity(userViewModel, ref DbUser);

                MyDbContext context = new MyDbContext();
                context.Users.Add(DbUser);
                context.SaveChanges();
                userViewModel.UserID = DbUser.UserID;

                DbUser.UsersProfile.FKUserID = DbUser.UserID;
                DbUser.UsersProfile.CreateDate = DateTime.Now;
                DbUser.UsersProfile.UpdatedDate = DateTime.Now;
                context.UsersProfile.Add(DbUser.UsersProfile);
                context.SaveChanges();



            }
            catch (Exception ex)
            { }
        }
        public UserViewModel GetAdminByUserID(long userID)
        {
            UserViewModel userViewModel = new UserViewModel();
            using (var context = new MyDbContext())
            {
                var user = (from u in context.Users
                            where u.UserID == userID
                            && u.IsActive == true
                            select u).FirstOrDefault();

                if (user != null)
                {
                    if (user.UserID > 0)
                    {
                        user.UsersProfile = (from up in context.UsersProfile where up.FKUserID == user.UserID select up).FirstOrDefault();

                        Mapper.MappUserToUserViewModel(user, ref userViewModel);
                    }
                }
            }

            return userViewModel;

        }

        public void UpdateAdmin(UserViewModel userViewModel)
        {
            try
            {
                var DbUser = new Users();
                Mapper.MappUserViewModelToUserEntity(userViewModel, ref DbUser);

                if (DbUser.UserID > 0)
                {
                    using (var context = new MyDbContext())
                    {
                        context.Users.Attach(DbUser);
                        context.Entry(DbUser).State = EntityState.Modified;

                        context.UsersProfile.Attach(DbUser.UsersProfile);
                        context.Entry(DbUser.UsersProfile).State = EntityState.Modified;

                        context.SaveChanges();

                    }
                }
            }
            catch (Exception ex)
            { }

        }

        public void DeleteAdminByUserID(int userID)
        {
            using (var ctx = new MyDbContext())
            {
                var userActivities = (from activities in ctx.UserActivity
                                      where activities.IsActive == true
                                      && activities.FKUserID == userID
                                      select activities).ToList();
                if (userActivities.Count > 0)
                {
                    foreach (var item in userActivities)
                    {
                        item.IsActive = false;
                        ctx.UserActivity.Attach(item);
                        ctx.Entry(item).State = EntityState.Modified;

                        ctx.SaveChanges();
                    }
                }

                var userpermissions = (from permission in ctx.UserPermissions where permission.FKUserID == userID select permission).ToList();
                if (userpermissions.Count > 0)
                {
                    foreach (var item in userpermissions)
                    {
                        ctx.UserPermissions.Remove(item);
                        ctx.SaveChanges();
                    }

                }

                var DbUserProfile = (from up in ctx.UsersProfile where up.FKUserID == userID select up).FirstOrDefault();
                if (DbUserProfile.UsersProfileID > 0)
                {
                    ctx.UsersProfile.Remove(DbUserProfile);
                    ctx.SaveChanges();
                }


                var dbUser = (from u in ctx.Users where u.UserID == userID select u).FirstOrDefault();
                if (dbUser.UserID > 0)
                {
                    ctx.Users.Remove(dbUser);
                    ctx.SaveChanges();
                }

            }
        }

        public List<PermissionViewModel> GetPermissionsList()
        {
            var permissionsList = new List<PermissionViewModel>();
            using (var ctx = new MyDbContext())
            {
                var DbPermissionsList = (from p in ctx.Permissions select p).ToList();
                if (DbPermissionsList.Count > 0)
                {
                    Mapper.MappPermissionsListToModel(DbPermissionsList, ref permissionsList);
                }
            }
            return permissionsList;
        }

        public void AddInUserPermissions(string[] permissionsIDs, int UserID, int userType)
        {
            using (var ctx = new MyDbContext())
            {

                for (int i = 0; i < permissionsIDs.Length; i++)
                {
                    UserPermission obj_userPermission = new UserPermission();
                    obj_userPermission.FKUserID = UserID;
                    obj_userPermission.UserType = userType;
                    obj_userPermission.FKPermissionID = int.Parse(permissionsIDs[i]);

                    ctx.UserPermissions.Add(obj_userPermission);
                    ctx.SaveChanges();
                }
            }
        }

        public List<PermissionViewModel> GetPermissionsListByUserID(long userID)
        {
            using (var ctx = new MyDbContext())
            {
                var userPermissions = (from up in ctx.UserPermissions
                                       join p in ctx.Permissions
                                       on up.FKPermissionID equals p.PermissionID
                                       where up.FKUserID == userID
                                       select new PermissionViewModel
                                       {
                                           PermissionID = p.PermissionID,
                                           PermissionName = p.PermissionName,
                                           FKUserID = (int)up.FKUserID,
                                           UserType = up.UserType,
                                           IsActive = p.IsActive

                                       }).ToList();



                return userPermissions;
            }
        }

        public List<int> GetPermissionIDsOfUserbyUserID(long userID)
        {
            var list = new List<int>();

            using (var ctx = new MyDbContext())
            {
                var userPermissions = (from up in ctx.UserPermissions
                                       join p in ctx.Permissions
                                       on up.FKPermissionID equals p.PermissionID
                                       where up.FKUserID == userID
                                       select new PermissionViewModel
                                       {
                                           PermissionID = p.PermissionID,
                                           PermissionName = p.PermissionName,
                                           FKUserID = (int)up.FKUserID,
                                           UserType = up.UserType,
                                           IsActive = p.IsActive

                                       }).Distinct().ToList();



                if (userPermissions.Count > 0)
                {
                    foreach (var permission in userPermissions)
                    {
                        list.Add(permission.PermissionID);
                    }
                }

                return list;
            }
        }

        public void DeleteUserPermissionsByUserID(int userID)
        {
            try
            {
                using (var ctx = new MyDbContext())
                {
                    var userPermissions = (from UP in ctx.UserPermissions where UP.FKUserID == userID select UP).ToList();
                    if (userPermissions.Count > 0)
                    {
                        foreach (var p in userPermissions)
                        {
                            ctx.UserPermissions.Remove(p);
                            ctx.SaveChanges();
                        }

                    }
                }
            }
            catch (Exception ex)
            {

            }
        }




        #endregion


        //public static List<TeacherViewModel> ListofTeachers()
        //{
        //    try
        //    {
        //        var model = Db.Persons.Where(x => x.IsSuper == false && x.IsAdmin == false && x.UserType == "Teacher").Select(x => new TeacherViewModel
        //        {
        //            PersonId = x.ID,
        //            FirstName = x.FirstName,
        //            LastName = x.LastName,
        //            EmailAddress = x.LoginEmail,
        //            ContactNumber = x.ContactNumber,
        //            IsActive = x.IsActive,
        //            IsVerfied = x.IsVerified

        //        }).ToList();
        //        return model;
        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }
        //}


        //Teacher View Model Get Teacher in List Admin manager AND Finding Teacher SEARCH RECORD
        public class TeacherViewModel
        {
            public long TeacherId { get; set; }

            public long PersonId { get; set; }

            public string FirstName { get; set; }

            public string LastName { get; set; }

            public string ContactNumber { get; set; }

            public string EmailAddress { get; set; }

            public bool IsActive { get; set; }

            public int UserStatus { get; set; }

            public int IsVerfied { get; set; }

            public string TeacherExperience { get; set; }

            public string TeacherQualification { get; set; }

            public bool IsOnline { get; set; }
        }


    }
}
