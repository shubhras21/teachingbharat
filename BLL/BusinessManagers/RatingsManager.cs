﻿using BLL.BusinessModels.TeacherModels;
using BLL.Helpers;
using DAL.Entities;
using DAL.MasterEntity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL.BusinessManagers
{
   public class RatingsManager
    {
       MyDbContext context = new MyDbContext();

        public RatingsViewModel ListOfRatings(Int64 UserID)
        {
            try
            {
                RatingsViewModel obj = new RatingsViewModel();
                obj.ReviewsList = new List<RatingReviews>();
                obj.ReviewsList = context.RatingReviews.Where(x=>x.FKTeacherID==UserID && x.IsActive==true).ToList();
                if(obj.ReviewsList!=null && obj.ReviewsList.Count>0)
                {
                    obj.ReviewsList.ForEach(x =>
                    {
                        x.StudentName = context.UsersProfile.Where(y => y.FKUserID == x.FKStudentID).Select(y => new { Name = y.FirstName + " " + y.LastName }).FirstOrDefault().Name.ToString();
                        var grade = context.StudentGrades.Where(y => y.FKUserID == x.FKStudentID).FirstOrDefault();
                        if (grade != null)
                        {
                            x.StudentGrade = grade.GradeName;
                        }
                        else
                        {
                            x.StudentGrade = null;
                        }
                        x.StudentLocation = context.UsersProfile.Where(y => y.FKUserID == x.FKStudentID).Select(y => y.CityName).FirstOrDefault().ToString();

                    });

                    var count = obj.ReviewsList.Select(x=>x.Rating).Count();
                    obj.TotalRatings = count;
                    var sum = obj.ReviewsList.Sum(x=>x.Rating);
                    count = count * 5;
                    obj.RatingAverage = (sum / count) * 5;
                    obj.TeacherID = UserID;      
                    return obj;
                }
                else
                {
                    return obj;
                }
            }
            catch (Exception)
            {

                throw;
            }
        } 

        public Int64 AddRatings(RatingInsertionModel model)
        {
            try
            {
                CustomTimeZone czone = new CustomTimeZone();
                RatingReviews rating = new RatingReviews
                {
                    FKStudentID=model.StudentID,
                    FKTeacherID=model.TeacherID,
                    Rating=model.RatingStars,
                    Comment=model.Comment,
                    CreatedDate=czone.DateTimeNow(),
                    IsActive=true
                };

                context.RatingReviews.Add(rating);
                context.SaveChanges();
                return rating.RatingReviewsID;

            }
            catch (Exception)
            {
                
                throw;
            }

            return 0;
        }

        public PublicRatingsViewModel ListOfRudraRatings()
        {
            try
            {
                PublicRatingsViewModel obj = new PublicRatingsViewModel();
                obj.ReviewsList = new List<RatingReviews>();
                obj.ReviewsList = context.RatingReviews.Where(x=>x.IsApproved == true).ToList();
                if (obj.ReviewsList != null && obj.ReviewsList.Count > 0)
                {

                    //obj.ReviewsList.ForEach(x =>
                    //{
                    //    if(x.IsApproved==true)
                    //    {
                    //        if (x.StudentName != null)
                    //        {
                    //            x.StudentName = context.UsersProfile.Where(y => y.FKUserID == x.FKStudentID).Select(y => new { Name = y.FirstName + " " + y.LastName }).FirstOrDefault().Name.ToString();
                    //            var grade = context.StudentGrades.Where(y => y.FKUserID == x.FKStudentID).FirstOrDefault();
                    //            if (grade != null)
                    //            {
                    //                x.StudentGrade = grade.GradeName;
                    //            }
                    //            else
                    //            {
                    //                x.StudentGrade = null;
                    //            }
                    //            x.StudentLocation = context.UsersProfile.Where(y => y.FKUserID == x.FKStudentID).Select(y => y.CityName).FirstOrDefault().ToString();
                    //        }
                            
                    //    }
                       
                    //});

                    var count = obj.ReviewsList.Select(x => x.Rating).Count();
                    var sum = obj.ReviewsList.Sum(x => x.Rating);
                    count = count * 5;
                    obj.RatingAverage = (sum / count) * 5;
                    obj.TeachersCount = context.Users.Where(x => x.IsActive == true && x.UserType == (int)Enumerations.UserType.Teacher).Count();
                    obj.UsersCount = context.Users.Where(x => x.IsActive == true).Count();
                    obj.SessionsCount = context.Sessions.Where(x => x.RequestStatus == (int)Enumerations.RequestStatus.Accepted).Count();
                    obj.TotalRatings = count;

                    return obj;

                }
                else
                {
                    return obj;
                }
            }
            catch (Exception)
            {

                throw;
            }
        }

        public Int64 AddRudraRatings(PublicRatingInsertionModel model)
        {
            try
            {
                CustomTimeZone czone = new CustomTimeZone();
                RudraaRatings rating = new RudraaRatings
                {
                    Name=model.Name,
                    Email=model.Email,
                    FKUserID = model.UserID,
                    Rating = model.RatingStars,
                    Comment = model.Comment,
                    CreatedDate = czone.DateTimeNow(),
                    IsActive = true,
                    IsLogin=model.IsActive
                };

                context.RudraaRatings.Add(rating);
                context.SaveChanges();
                return rating.RudraaRatingID;

            }
            catch (Exception)
            {

                throw;
            }

            return 0;
        }

    }
}
