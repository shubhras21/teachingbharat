﻿using BLL.BusinessManagers.SessionsManager;
using BLL.Helpers;
using DAL.Entities;
using DAL.MasterEntity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL.BusinessManagers.WebRTCManager
{
   public class WebRTCManager
    {
        MyDbContext context = new MyDbContext();
        SessionManager manager = new SessionManager();

        public Int64 AddRoom(WebRTCRooms obj)
        {
            try
            {
                var isExist = context.WebRTCRooms.Where(x=>x.RoomID==obj.RoomID && x.FKSessionID==obj.FKSessionID).FirstOrDefault();
                if (isExist==null)
                {
                    context.WebRTCRooms.Add(obj);
                    context.SaveChanges();
                    return obj.WRTCID;
                }
                else
                {
                    return isExist.WRTCID;
                }

            }
            catch (Exception)
            {

                throw;
            }
            return 0;
        }

        public WebRTCRooms GetRoomDetail(Int64 WRTCID)
        {
            try
            {
                WebRTCRooms model = new WebRTCRooms();
                model = context.WebRTCRooms.Where(x => x.WRTCID == WRTCID && x.IsActive == true).FirstOrDefault() ;
                if (model != null)
                {
                    Sessions obj = new Sessions();
                    obj = manager.GetSessionDetail(model.FKSessionID);
                    if (obj != null)
                    {
                        model.TeacherName = obj.RequestedToName;
                        model.StudentName = obj.RequestedByName;
                    }
                    return model;
                }
                else
                {
                    return null;
                }

            }
            catch (Exception)
            {

                throw;
            }
        }

        public WebRTCRooms GetRoomDetailByRoomID(string RoomID)
        {
            try
            {
                WebRTCRooms model = new WebRTCRooms();
                model = context.WebRTCRooms.Where(x => x.RoomID == RoomID && x.IsActive == true).FirstOrDefault();
                if (model != null)
                {
                    Sessions obj = new Sessions();
                    obj = manager.GetSessionDetail(model.FKSessionID);
                    if (obj != null)
                    {
                        model.TeacherName = obj.RequestedToName;
                        model.StudentName = obj.RequestedByName;
                    }
                    return model;
                }
                else
                {
                    return null;
                }

            }
            catch (Exception)
            {

                throw;
            }
        }


        public WebRTCRooms GetRoomDetailBySessionID(Int64 SessionID)
        {
            try
            {
                WebRTCRooms model = new WebRTCRooms();
                model = context.WebRTCRooms.Where(x => x.FKSessionID == SessionID && x.IsActive == true).FirstOrDefault();
                if (model != null)
                {
                    Sessions obj = new Sessions();
                    obj = manager.GetSessionDetail(model.FKSessionID);
                    if (obj != null)
                    {
                        model.TeacherName = obj.RequestedToName;
                        model.StudentName = obj.RequestedByName;
                    }
                    return model;
                }
                else
                {
                    return null;
                }

            }
            catch (Exception)
            {

                throw;
            }
        }


        public WebRTCRooms GetRoomDetailByCourseID(Int64 SessionID,Int64 UserID)
        {
            try
            {
                WebRTCRooms model = new WebRTCRooms();
                model = context.WebRTCRooms.Where(x => x.FKSessionID == SessionID && x.IsActive == true && x.IsCourse==(int)Enumerations.OneOrMany.Course).FirstOrDefault();

                if (model != null)
                {
                    CoursesHeader obj = new CoursesHeader();
                    CourseOfferingsManager cManager = new CourseOfferingsManager();
                    obj = cManager.GetCourseDetail(model.FKSessionID);
                    if (obj != null)
                    {
                        model.TeacherName = obj.TeacherName;
                        model.StudentName = context.UsersProfile.Where(x => x.FKUserID == UserID).Select(x => new { Name = x.FirstName + " " + x.LastName }).FirstOrDefault().Name.ToString();


                    }
                    return model;
                }
                else
                {
                    return null;
                }

            }
            catch (Exception)
            {

                throw;
            }
        }

    }
}
