﻿using BLL.BusinessManagers.UserManager;
using BLL.BusinessModels.CalenderModels;
using BLL.BusinessModels.StudentModels;
using BLL.BusinessModels.TeacherModels;
using BLL.BusinessModels.UserModels;
using BLL.Helpers;
using DAL.Entities;
using DAL.MasterEntity;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace BLL.BusinessManagers.SessionsManager
{
    public class SessionManager
    {
        MyDbContext context = new MyDbContext();
        CustomTimeZone customTimeZone = new CustomTimeZone();
        UsersActivityManager ActivityMgr = null;
        UserActivities obj_Activity = null;


        public BookSessionModel SessionBookingHelper (BookSessionModel obj)
        {
            UserBLL bll = new UserBLL();
            BookSessionModel model = new BookSessionModel();
            model.CurriculumID = obj.CurriculumID;
            model.GradeID = obj.GradeID;
            model.SubjectID = obj.SubjectID;
            var name= context.UsersProfile.Where(x => x.FKUserID == obj.TeacherID).Select(x=>new
            { a= x.FirstName +  " "+x.LastName,
              b=x.SessionCost,
              location=x.CityName,
              profilePic=x.ProfilePic,
              mobileNumber=x.ContactNumber
            }).FirstOrDefault();
            var cname = context.Curriculums.Where(x => x.CurriculumID == obj.CurriculumID).Select(x => new
            {
                a = x.CurriculumName 
            }).FirstOrDefault();

            var gname = context.Grades.Where(x => x.GradeID == obj.GradeID && x.IsActive == true).Select(x => new
            {
                a = x.GradeName
            }).FirstOrDefault();
            var sname = context.Subjects.Where(x => x.SubjectID == obj.SubjectID && x.IsActive==true).Select(x => new
            {
                a = x.SubjectName
            }).FirstOrDefault();

            model.SessionCost = (name.b!=null && name.b>0)?name.b:0;
            model.Location = (name.location!= null)?name.location:"N/A";
            model.ContactNumber = (name.mobileNumber!= null)?name.mobileNumber:"N/A";
            model.ProfilePic = (name.profilePic != null)? name.profilePic : null ;
            model.TeacherName = name.a;
            model.CurriculumName = cname.a;
            model.GradeName = gname.a;
            model.SubjectName = sname.a;
            model.TeacherID = obj.TeacherID;
            model.ListOfTeacherTimes = bll.ListOfTeacherBookingTimes(obj.TeacherID);
            //.OrderByDescending(i => i.Date)
            model.ListOfTeacherTimes = model.ListOfTeacherTimes.Take(3).ToList();
            model.sListofCurriculums = context.TeacherSubjects.Where(x => x.FKUserID == obj.TeacherID && x.IsActive == true).GroupBy(x=>x.FKCurriculumID).Select(x=>x.FirstOrDefault()).ToList().Select(x => new SelectListItem()
            {              
                Text = context.Curriculums.Where(y => y.CurriculumID == x.FKCurriculumID).Select(y => y.CurriculumName).FirstOrDefault().ToString(),
                Value = context.Curriculums.Where(y => y.CurriculumID == x.FKCurriculumID).Select(y => y.CurriculumID).FirstOrDefault().ToString(),
                Selected = (x.FKCurriculumID == model.CurriculumID) ? true : false
            });

            model.sListofGrades = context.TeacherSubjects.Where(x => x.FKUserID == obj.TeacherID && x.IsActive == true).GroupBy(x => x.FKGradeID).Select(x => x.FirstOrDefault()).ToList().Select(x => new SelectListItem()
            {
                Text = context.Grades.Where(y => y.GradeID == x.FKGradeID).Select(y => y.GradeName).FirstOrDefault().ToString(),
                Value = context.Grades.Where(y => y.GradeID == x.FKGradeID).Select(y => y.GradeID).FirstOrDefault().ToString(),
                Selected = (x.FKGradeID == model.GradeID) ? true : false
            });
            model.sListofSubjects = context.TeacherSubjects.Where(x => x.FKUserID == obj.TeacherID && x.IsActive==true).GroupBy(x => x.FKSubjectID).Select(x => x.FirstOrDefault()).ToList().Select(x => new SelectListItem()
            {
                Text = context.Subjects.Where(y => y.SubjectID == x.FKSubjectID && y.IsActive==true).Select(y => y.SubjectName).FirstOrDefault().ToString(),
                Value = context.Subjects.Where(y => y.SubjectID == x.FKSubjectID && y.IsActive == true).Select(y => y.SubjectID).FirstOrDefault().ToString(),
                Selected = (x.FKSubjectID == model.SubjectID) ? true : false
            });
            return model;
        }

        public BookSessionModel BookSessionHelper(BookSessionModel obj)
        {
            UserBLL bll = new UserBLL();
            BookSessionModel model = new BookSessionModel();
          
            var name = context.UsersProfile.Where(x => x.FKUserID == obj.TeacherID).Select(x => new
            {
                a = x.FirstName + " " + x.LastName,
                b = x.SessionCost,
                location = x.CityName,
                profilePic = x.ProfilePic,
                mobileNumber = x.ContactNumber
            }).FirstOrDefault();

            model.SessionCost = (name.b != null && name.b > 0) ? name.b : 0;
            model.Location = (name.location != null) ? name.location : "N/A";
            model.ContactNumber = (name.mobileNumber != null) ? name.mobileNumber : "N/A";
            model.ProfilePic = (name.profilePic != null) ? name.profilePic : null;

            model.TeacherName = name.a;
            model.TeacherID = obj.TeacherID;
            model.ListOfTeacherTimes = bll.ListOfTeacherBookingTimes(model.TeacherID);
            model.ListOfTeacherTimes = model.ListOfTeacherTimes.Take(3).ToList();
            model.sListofCurriculums = context.TeacherSubjects.Where(x => x.FKUserID == obj.TeacherID && x.IsActive == true).GroupBy(x => x.FKCurriculumID).Select(x => x.FirstOrDefault()).ToList().Select(x => new SelectListItem()
            {
                Text = context.Curriculums.Where(y => y.CurriculumID == x.FKCurriculumID).Select(y => y.CurriculumName).FirstOrDefault().ToString(),
                Value = context.Curriculums.Where(y => y.CurriculumID == x.FKCurriculumID).Select(y => y.CurriculumID).FirstOrDefault().ToString()
            });

            model.sListofGrades = context.TeacherSubjects.Where(x => x.FKUserID == obj.TeacherID && x.IsActive == true).GroupBy(x => x.FKGradeID).Select(x => x.FirstOrDefault()).ToList().Select(x => new SelectListItem()
            {
                Text = context.Grades.Where(y => y.GradeID == x.FKGradeID).Select(y => y.GradeName).FirstOrDefault().ToString(),
                Value = context.Grades.Where(y => y.GradeID == x.FKGradeID).Select(y => y.GradeID).FirstOrDefault().ToString()
            });
            model.sListofSubjects = context.TeacherSubjects.Where(x => x.FKUserID == obj.TeacherID && x.IsActive==true).ToList().Select(x => new SelectListItem()
            {
                Text = context.Subjects.Where(y => y.SubjectID == x.FKSubjectID).Select(y => y.SubjectName).FirstOrDefault().ToString(),
                Value = context.Subjects.Where(y => y.SubjectID == x.FKSubjectID).Select(y => y.SubjectID).FirstOrDefault().ToString()
            });
            return model;
        }

        public Int64 AddSession(BookSessionModel obj)
        {
            try
            {
                Sessions sObj = new Sessions
                {
                    FKCurriculumID = obj.CurriculumID,
                    FKGradeID = obj.GradeID,
                    FKSubjectID = obj.SubjectID,
                    FKRequestedBy = Config.CurrentUser,
                    FKRequestedTo = obj.TeacherID,
                    RequestDate = customTimeZone.DateTimeNow(),
                    TrialHappenDate = obj.TimeSlot,
                    TopicName = obj.TopicName,
                    RequestStatus = (int)Enumerations.RequestStatus.Pending,
                    SessionType = obj.SessionType,
                    SessionName = null,
                    StartDate = null,
                    EndDate = null,
                    CurrentStatus= (int)Enumerations.SessionStatus.Pending,
                };
            if(sObj.SessionType==(int)Enumerations.SessionType.Trial)
                {
                    sObj.SessionCost = null;
                }
            else
                {
                    sObj.SessionCost = context.UsersProfile.Where(x => x.FKUserID == sObj.FKRequestedTo).Select(x=>x.SessionCost).FirstOrDefault();
                }
            context.Sessions.Add(sObj);
            context.SaveChanges();
            Int64 sID = sObj.TrialSessionID;


                // Add session request status into notifications 


                //string teacherName = context.UsersProfile.Where(x => x.FKUserID == sObj.FKRequestedTo).Select(x => new { Name = x.FirstName + " " + x.LastName }).FirstOrDefault().Name.ToString();
                //Notifications nObj = new Notifications();
                //nObj.ActionPerformed = "You request new session with " + teacherName;
                //nObj.FKSessionID = sID;
                //nObj.FKUserID = Convert.ToInt64(sObj.FKRequestedBy);
                //nObj.IsCourse = (int)Enumerations.OneOrMany.Session;

                //NotificationManager nManager = new NotificationManager();
                //Int64 ID = nManager.AddNotification(nObj);


                // Add session request status into notifications 


                // Repeating twice . thats why commented
                //string StudentName = context.UsersProfile.Where(x => x.FKUserID == sObj.FKRequestedBy).Select(x => new { Name = x.FirstName + " " + x.LastName }).FirstOrDefault().Name.ToString();
                //Notifications nObj1 = new Notifications();
                //nObj1.ActionPerformed = "You got new session request from  " +StudentName;
                //nObj1.FKSessionID = sID;
                //nObj1.FKUserID = Convert.ToInt64(sObj.FKRequestedTo);
                //nObj1.IsCourse = (int)Enumerations.OneOrMany.Session;

                //Int64 ID1 = nManager.AddNotification(nObj);

                return sID;
            }
            catch(Exception ex)
            {
                throw ex;
            }
            return 0;
        }

        public List<Sessions> TeacherPendingSessions(Int64 UserID)
        {
            try
            {
                CustomTimeZone czone = new CustomTimeZone();
                var time = czone.DateTimeNow();
                List<Sessions> list = new List<Sessions>();
                list = context.Sessions.Where(x => x.FKRequestedTo == UserID && x.RequestStatus!=(int)Enumerations.RequestStatus.Accepted && x.RequestStatus != (int)Enumerations.RequestStatus.Rejected && x.TrialHappenDate>time).ToList();
                if(list!=null)
                {
                    list.ForEach(x => 
                        x.RequestedByName = Convert.ToString(context.UsersProfile.Where(y => y.FKUserID == x.FKRequestedBy).Select(t => new { Name = t.FirstName + " " + t.LastName }).FirstOrDefault().Name));
                    list.ForEach(x => x.ImageName = Convert.ToString(context.UsersProfile.Where(y => y.FKUserID == x.FKRequestedBy).Select(t => t.ProfilePic).FirstOrDefault()));

                    list.ForEach(x => x.CurriculumName = context.Curriculums.Where(y => y.CurriculumID == x.FKCurriculumID).Select(t => t.CurriculumName).FirstOrDefault().ToString());
                    list.ForEach(x => x.GradeName = context.Grades.Where(y => y.GradeID == x.FKGradeID).Select(t => t.GradeName).FirstOrDefault().ToString());
                    list.ForEach(x => x.SubjectName = context.Subjects.Where(y => y.SubjectID == x.FKSubjectID).Select(t => t.SubjectName).FirstOrDefault().ToString());
                    //  list.ForEach(x => x.SessionTimes = x.TrialHappenDate.ToString("f") +" - " +x.TrialHappenDate.AddHours(1).Hour +" "+ x.TrialHappenDate.AddHours(1).ToString("tt", CultureInfo.InvariantCulture));
                    list.ForEach(x => x.SessionTimes = x.TrialHappenDate.ToString("f") + " - " + x.TrialHappenDate.AddHours(1).AddHours(-0).ToString("hh:mm tt") + " ");

                 

                    return list;
                }
                else 
                return null;
            }
            catch(Exception ex)
            {
                throw ex;
            }  
        }

        public List<Sessions> TeacherUpcomingSessions(Int64 UserID)
        {
            try
            {
                List<Sessions> list = new List<Sessions>();
                DateTime time=customTimeZone.DateTimeNow();
                list = context.Sessions.Where(x => x.FKRequestedTo == UserID && x.TrialHappenDate> time && x.RequestStatus==(int)Enumerations.RequestStatus.Accepted).OrderBy(x=>x.TrialHappenDate).ToList();
                if (list != null)
                {
                    //list.ForEach(x => x.RequestedByName = context.UsersProfile.Where(y => y.FKUserID == x.FKRequestedBy).Select(t => new { Name = t.FirstName + " " + t.LastName }).FirstOrDefault().Name.ToString());

                    //list.ForEach(x => {
                    //    x.RequestedByName = context.UsersProfile.Where(y => y.FKUserID == x.FKRequestedBy).Select(t => new { Name = t.FirstName + " " + t.LastName }).FirstOrDefault().Name.ToString();
                    //    x.ImageName = context.UsersProfile.Where(y => y.FKUserID == x.FKRequestedBy).Select(t => t.ProfilePic).FirstOrDefault().ToString();
                    //});

                    foreach(var x in list)
                    {
                       var userprofile = (from y in context.UsersProfile where y.FKUserID == x.FKRequestedBy select y).FirstOrDefault();
                        if(userprofile != null)
                        {
                            x.RequestedByName = userprofile.FirstName + " " + userprofile.LastName;
                            x.ImageName = userprofile.ProfilePic;
                        }
                    }

                    list.ForEach(x => x.CurriculumName = context.Curriculums.Where(y => y.CurriculumID == x.FKCurriculumID).Select(t => t.CurriculumName).FirstOrDefault().ToString());
                    list.ForEach(x => x.GradeName = context.Grades.Where(y => y.GradeID == x.FKGradeID).Select(t => t.GradeName).FirstOrDefault().ToString());
                    list.ForEach(x => x.SubjectName = context.Subjects.Where(y => y.SubjectID == x.FKSubjectID).Select(t => t.SubjectName).FirstOrDefault().ToString());
                    //  list.ForEach(x => x.SessionTimes = x.TrialHappenDate.ToString("f") + " - " + x.TrialHappenDate.AddHours(1).Hour + " " + x.TrialHappenDate.AddHours(1).ToString("tt", CultureInfo.InvariantCulture));
                    list.ForEach(x => x.SessionTimes = x.TrialHappenDate.ToString("f") + " - " + x.TrialHappenDate.AddHours(1).AddHours(-0).ToString("hh:mm tt") + " ");
                    list.ForEach(x =>
                    {
                        string img = Convert.ToString(context.UsersProfile.Where(y => y.FKUserID == x.FKRequestedBy).Select(y => y.ProfilePic).FirstOrDefault());
                        x.ImageName = (img != null) ? img : null;

                    });
                    return list;
                }
                else
                    return list;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<Sessions> StudentUpcomingSessions(Int64 UserID)
        {
            try
            {
                List<Sessions> list = new List<Sessions>();
                DateTime time = customTimeZone.DateTimeNow();
                list = context.Sessions.Where(x => x.FKRequestedBy == UserID && x.TrialHappenDate >time && x.RequestStatus == (int)Enumerations.RequestStatus.Accepted).OrderBy(x => x.TrialHappenDate).ToList();
                if (list != null)
                {
                    //  list.ForEach(x => x.RequestedToName = context.UsersProfile.Where(y => y.FKUserID == x.FKRequestedTo).Select(t => new { Name = t.FirstName + " " + t.LastName }).FirstOrDefault().Name.ToString());

                    //list.ForEach(x => {
                    //    x.RequestedToName = context.UsersProfile.Where(y => y.FKUserID == x.FKRequestedTo).Select(t => new { Name = t.FirstName + " " + t.LastName }).FirstOrDefault().Name.ToString();
                    //    x.ImageName = context.UsersProfile.Where(y => y.FKUserID == x.FKRequestedTo).Select(t => t.ProfilePic).FirstOrDefault().ToString();
                    //});

                    
                    foreach(var x in list)
                    {
                        var userprofile = (from y in context.UsersProfile where y.FKUserID == x.FKRequestedTo select y).FirstOrDefault();
                        if(userprofile != null)
                        {
                            x.RequestedToName = userprofile.FirstName + " " + userprofile.LastName;
                            x.ImageName = userprofile.ProfilePic;
                        }
                    }

                    list.ForEach(x => x.CurriculumName = context.Curriculums.Where(y => y.CurriculumID == x.FKCurriculumID).Select(t => t.CurriculumName).FirstOrDefault().ToString());
                    list.ForEach(x => x.GradeName = context.Grades.Where(y => y.GradeID == x.FKGradeID).Select(t => t.GradeName).FirstOrDefault().ToString());
                    list.ForEach(x => x.SubjectName = context.Subjects.Where(y => y.SubjectID == x.FKSubjectID).Select(t => t.SubjectName).FirstOrDefault().ToString());
                    //  list.ForEach(x => x.SessionTimes = x.TrialHappenDate.ToString("f") + " - " + x.TrialHappenDate.AddHours(1).Hour + " " + x.TrialHappenDate.AddHours(1).ToString("tt", CultureInfo.InvariantCulture));
                    list.ForEach(x => x.SessionTimes = x.TrialHappenDate.ToString("f") + " - " + x.TrialHappenDate.AddHours(1).AddHours(-0).ToString("hh:mm tt") + " ");

                    list.ForEach(x =>
                    {
                        string img = Convert.ToString(context.UsersProfile.Where(y => y.FKUserID == x.FKRequestedTo).Select(y => y.ProfilePic).FirstOrDefault());
                        x.ImageName = (img != null) ? img : null;
                    });
                    return list;

                }
                else
                    return list;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        public List<Sessions> StudentPendingSessions(Int64 UserID)
        {
            try
            {

                CustomTimeZone czone = new CustomTimeZone();
                var time = czone.DateTimeNow();
                List<Sessions> list = new List<Sessions>();
                list = context.Sessions.Where(x => x.FKRequestedBy == UserID && x.RequestStatus==(int)Enumerations.RequestStatus.Pending &&x.TrialHappenDate> time).ToList();
                if (list != null)
                {
                    //  list.ForEach(x => x.RequestedToName= context.UsersProfile.Where(y => y.FKUserID == x.FKRequestedTo).Select(t => new { Name = t.FirstName + " " + t.LastName }).FirstOrDefault().Name.ToString());

                    list.ForEach(x => {
                        x.RequestedToName = context.UsersProfile.Where(y => y.FKUserID == x.FKRequestedTo).Select(t => new { Name = t.FirstName + " " + t.LastName }).FirstOrDefault().Name.ToString();
               // line commented temporarily
               // x.ImageName = context.UsersProfile.Where(y => y.FKUserID == x.FKRequestedTo).Select(t => t.ProfilePic).FirstOrDefault().ToString();
                       var image = (from up in context.UsersProfile where up.FKUserID == x.FKRequestedTo select up).FirstOrDefault();
                        if(image != null)
                        {
                            if(image.ProfilePic != null)
                            {
                                x.ImageName = image.ProfilePic;
                            }
                        }
                    });


                    list.ForEach(x => x.CurriculumName = context.Curriculums.Where(y => y.CurriculumID == x.FKCurriculumID).Select(t => t.CurriculumName).FirstOrDefault().ToString());
                    list.ForEach(x => x.GradeName = context.Grades.Where(y => y.GradeID == x.FKGradeID).Select(t => t.GradeName).FirstOrDefault().ToString());
                    list.ForEach(x => x.SubjectName = context.Subjects.Where(y => y.SubjectID == x.FKSubjectID).Select(t => t.SubjectName).FirstOrDefault().ToString());
                    //   list.ForEach(x => x.SessionTimes = x.TrialHappenDate.ToString("f") + " - " + x.TrialHappenDate.AddHours(1).Hour + " " + x.TrialHappenDate.AddHours(1).ToString("tt", CultureInfo.InvariantCulture));
                    list.ForEach(x => x.SessionTimes = x.TrialHappenDate.ToString("f") + " - " + x.TrialHappenDate.AddHours(1).AddHours(-0).ToString("hh:mm tt") + " ");

                    return list;
                }
                else
                    return list;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<Sessions> StudentSessions(Int64 UserID)
        {
            try
            {
                List<Sessions> list = new List<Sessions>();
                list = context.Sessions.Where(x => x.FKRequestedBy == UserID).ToList();
                if (list != null)
                {
                    list.ForEach(x => x.RequestedToName = context.UsersProfile.Where(y => y.FKUserID == x.FKRequestedTo).Select(t => new { Name = t.FirstName + " " + t.LastName }).FirstOrDefault().Name.ToString());
                    list.ForEach(x => x.CurriculumName = context.Curriculums.Where(y => y.CurriculumID == x.FKCurriculumID).Select(t => t.CurriculumName).FirstOrDefault().ToString());
                    list.ForEach(x => x.GradeName = context.Grades.Where(y => y.GradeID == x.FKGradeID).Select(t => t.GradeName).FirstOrDefault().ToString());
                    list.ForEach(x => x.SubjectName = context.Subjects.Where(y => y.SubjectID == x.FKSubjectID).Select(t => t.SubjectName).FirstOrDefault().ToString());
                    list.ForEach(x => x.SessionTimes = x.TrialHappenDate.ToString("f") + " - " + x.TrialHappenDate.AddHours(1).AddHours(-0).ToString("hh:mm tt") + " " );
                   // +x.TrialHappenDate.AddHours(1).Hour.ToString("tt", CultureInfo.InvariantCulture)

                    list.ForEach(x => x.RequestedTimes = x.RequestDate.ToString("f"));

                    list.ForEach(x => {
                        if (x.RequestStatus == (int)Enumerations.RequestStatus.Pending)
                            x.RequestStatusName = "Pending";
                       else if (x.RequestStatus == (int)Enumerations.RequestStatus.Accepted)
                            x.RequestStatusName = "Request Accepted";
                        else if (x.RequestStatus == (int)Enumerations.RequestStatus.Rejected)
                            x.RequestStatusName = "Request Rejected";
                    });

                    list.ForEach(x => {
                        if (x.RequestStatus == (int)Enumerations.RequestStatus.Accepted)
                        {
                            DateTime time = customTimeZone.DateTimeNow();
                            if (x.TrialHappenDate >time)
                                x.SessionStatus = "Upcoming";
                            else if (x.TrialHappenDate < time && x.RequestStatus!=(int)Enumerations.RequestStatus.Pending && x.RequestStatus != (int)Enumerations.RequestStatus.Rejected)
                                x.SessionStatus = "Past";
                        }
                        else
                        {
                            x.SessionStatus = " - ";
                        }

                    });
                    return list;
                }
                else
                    return list;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<Sessions> TeacherSessions(Int64 UserID)
        {
            try
            {
                List<Sessions> list = new List<Sessions>();
                list = context.Sessions.Where(x => x.FKRequestedTo == UserID).ToList();
                if (list != null)
                {
                    list.ForEach(x => x.RequestedByName = context.UsersProfile.Where(y => y.FKUserID == x.FKRequestedBy).Select(t => new { Name = t.FirstName + " " + t.LastName }).FirstOrDefault().Name.ToString());
                    list.ForEach(x => x.CurriculumName = context.Curriculums.Where(y => y.CurriculumID == x.FKCurriculumID).Select(t => t.CurriculumName).FirstOrDefault().ToString());
                    list.ForEach(x => x.GradeName = context.Grades.Where(y => y.GradeID == x.FKGradeID).Select(t => t.GradeName).FirstOrDefault().ToString());
                    list.ForEach(x => x.SubjectName = context.Subjects.Where(y => y.SubjectID == x.FKSubjectID).Select(t => t.SubjectName).FirstOrDefault().ToString());
                    //    list.ForEach(x => x.SessionTimes = x.TrialHappenDate.ToString("f") + " - " + x.TrialHappenDate.AddHours(1).Hour + " " + x.TrialHappenDate.AddHours(1).ToString("tt", CultureInfo.InvariantCulture));
                    list.ForEach(x => x.SessionTimes = x.TrialHappenDate.ToString("f") + " - " + x.TrialHappenDate.AddHours(1).AddHours(-0).ToString("hh:mm tt") + " ");

                    list.ForEach(x => x.RequestedTimes = x.RequestDate.ToString("f"));


                    list.ForEach(x => {
                        if (x.RequestStatus == (int)Enumerations.RequestStatus.Pending)
                            x.RequestStatusName = "Pending";
                        else if (x.RequestStatus == (int)Enumerations.RequestStatus.Accepted)
                            x.RequestStatusName = "Request Accepted";
                        else if (x.RequestStatus == (int)Enumerations.RequestStatus.Rejected)
                            x.RequestStatusName = "Request Rejected";
                    });

                    list.ForEach(x => {
                        if (x.RequestStatus == (int)Enumerations.RequestStatus.Accepted)
                        {
                            DateTime time = customTimeZone.DateTimeNow();
                            if (x.TrialHappenDate > time)
                            x.SessionStatus = "Upcoming";
                            else if (x.TrialHappenDate < time)
                            x.SessionStatus = "Past";
                        }
                        else
                        {
                            x.SessionStatus = " - ";
                        }

                    });

                    return list;
                }
                else
                    return list;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        public MySessions UpdatedMySession(Int64 UserID)
        {
            try
            {
                MySessions mSession = new MySessions();

                List<Sessions> listOfPendingSession = new List<Sessions>();

                List<Sessions> listOfUpcomingSession = new List<Sessions>();

                List<Sessions> listOfCancelSession = new List<Sessions>();

                List<Sessions> listOfJoinSession = new List<Sessions>();

                List<Sessions> listOfPastSession = new List<Sessions>();

                var CurrentTime = customTimeZone.DateTimeNow();

                listOfPendingSession = context.Sessions.Where(x => x.FKRequestedTo == UserID && x.TrialHappenDate>CurrentTime && x.CurrentStatus== (int)Enumerations.SessionStatus.Pending && x.RequestStatus == (int)Enumerations.RequestStatus.Pending).ToList();

                listOfUpcomingSession= context.Sessions.Where(x => x.FKRequestedTo == UserID && x.TrialHappenDate > CurrentTime && x.CurrentStatus == (int)Enumerations.SessionStatus.Pending && x.RequestStatus == (int)Enumerations.RequestStatus.Accepted).ToList();

                var pSessions = context.SessionParticipants.Where(x => x.FKStudentID == UserID && x.IsActive == true).GroupBy(x => x.FKSessionID).Select(x => x.FirstOrDefault()).ToList();

                if (pSessions != null && pSessions.Count > 0)
                {
                    List<Int64> pIntList = pSessions.Select(x => x.FKSessionID).ToList();

                    listOfPastSession = context.Sessions.Where(x => x.FKRequestedTo == UserID && x.TrialHappenDate < CurrentTime && (x.CurrentStatus == (int)Enumerations.SessionStatus.Pending || x.CurrentStatus == (int)Enumerations.SessionStatus.Completed)  && x.RequestStatus == (int)Enumerations.RequestStatus.Accepted && pIntList.Contains(x.TrialSessionID)).ToList();

                    if (listOfPastSession != null && listOfPastSession.Count > 0)
                    {
                        listOfPastSession.ForEach(x => x.RequestedByName = context.UsersProfile.Where(y => y.FKUserID == x.FKRequestedBy).Select(t => new { Name = t.FirstName + " " + t.LastName }).FirstOrDefault().Name.ToString());
                        listOfPastSession.ForEach(x => x.CurriculumName = context.Curriculums.Where(y => y.CurriculumID == x.FKCurriculumID).Select(t => t.CurriculumName).FirstOrDefault().ToString());
                        listOfPastSession.ForEach(x => x.GradeName = context.Grades.Where(y => y.GradeID == x.FKGradeID).Select(t => t.GradeName).FirstOrDefault().ToString());
                        listOfPastSession.ForEach(x => x.SubjectName = context.Subjects.Where(y => y.SubjectID == x.FKSubjectID).Select(t => t.SubjectName).FirstOrDefault().ToString());
                        listOfPastSession.ForEach(x => x.SessionTimes = x.TrialHappenDate.ToString("f") + " - " + x.TrialHappenDate.AddHours(1).AddHours(-0).ToString("hh:mm tt") + " ");
                        listOfPastSession.ForEach(x => x.RequestedTimes = x.RequestDate.ToString("f"));
                        listOfPastSession.ForEach(x => {
                            if (x.RequestStatus == (int)Enumerations.RequestStatus.Pending)
                                x.RequestStatusName = "Pending";
                            else if (x.RequestStatus == (int)Enumerations.RequestStatus.Accepted)
                                x.RequestStatusName = "Request Accepted";
                            else if (x.RequestStatus == (int)Enumerations.RequestStatus.Rejected)
                                x.RequestStatusName = "Request Rejected";
                        });

                        listOfPastSession.ForEach(x => {
                            if (x.RequestStatus == (int)Enumerations.RequestStatus.Accepted)
                            {
                                DateTime time = customTimeZone.DateTimeNow();
                                if (x.TrialHappenDate > time)
                                    x.SessionStatus = "Upcoming";
                                else if (x.TrialHappenDate < time)
                                    x.SessionStatus = "Past";
                            }
                            else
                            {
                                x.SessionStatus = " - ";
                            }

                        });

                        mSession.listOfPastSession = listOfPastSession;        
                    }
                    else
                    {
                        listOfPastSession = null;
                        mSession.listOfPastSession = null;
                    }

                    }
                else
                {
                    listOfPastSession = null;
                    mSession.listOfPastSession = null;

                }


                var jSessions = context.Sessions.Where(x => x.FKRequestedTo == UserID && x.RequestStatus == (int)Enumerations.RequestStatus.Accepted && x.CurrentStatus != (int)Enumerations.SessionStatus.Completed && x.CurrentStatus != (int)Enumerations.SessionStatus.NotTaken).ToList();
               
                if(jSessions!=null && jSessions.Count>0)
                {
                    jSessions.ForEach(x => {

                        var startTime = x.TrialHappenDate;
                        var endTime = x.TrialHappenDate.AddHours(1);
                        if (CurrentTime >= startTime && CurrentTime <= endTime)
                        {
                            listOfJoinSession.Add(x);
                        }
                    });
                    mSession.listOfJoinSession = listOfJoinSession;
                }
                else
                {
                    listOfJoinSession = null;
                    mSession.listOfJoinSession = null;
                }
                    
                listOfCancelSession= context.Sessions.Where(x => x.FKRequestedTo == UserID && x.TrialHappenDate < CurrentTime && x.CurrentStatus == (int)Enumerations.SessionStatus.Pending && x.RequestStatus == (int)Enumerations.RequestStatus.Pending).ToList();

                if(listOfCancelSession!=null && listOfCancelSession.Count>0)
                {
                    listOfCancelSession.ForEach(x => x.RequestedByName = context.UsersProfile.Where(y => y.FKUserID == x.FKRequestedBy).Select(t => new { Name = t.FirstName + " " + t.LastName }).FirstOrDefault().Name.ToString());
                    listOfCancelSession.ForEach(x => x.CurriculumName = context.Curriculums.Where(y => y.CurriculumID == x.FKCurriculumID).Select(t => t.CurriculumName).FirstOrDefault().ToString());
                    listOfCancelSession.ForEach(x => x.GradeName = context.Grades.Where(y => y.GradeID == x.FKGradeID).Select(t => t.GradeName).FirstOrDefault().ToString());
                    listOfCancelSession.ForEach(x => x.SubjectName = context.Subjects.Where(y => y.SubjectID == x.FKSubjectID).Select(t => t.SubjectName).FirstOrDefault().ToString());
                    listOfCancelSession.ForEach(x => x.SessionTimes = x.TrialHappenDate.ToString("f") + " - " + x.TrialHappenDate.AddHours(1).AddHours(-0).ToString("hh:mm tt") + " ");
                    listOfCancelSession.ForEach(x => x.RequestedTimes = x.RequestDate.ToString("f"));
                    listOfCancelSession.ForEach(x => {
                        if (x.RequestStatus == (int)Enumerations.RequestStatus.Pending)
                            x.RequestStatusName = "Pending";
                        else if (x.RequestStatus == (int)Enumerations.RequestStatus.Accepted)
                            x.RequestStatusName = "Request Accepted";
                        else if (x.RequestStatus == (int)Enumerations.RequestStatus.Rejected)
                            x.RequestStatusName = "Request Rejected";
                    });

                    listOfCancelSession.ForEach(x => {
                        if (x.RequestStatus == (int)Enumerations.RequestStatus.Accepted)
                        {
                            DateTime time = customTimeZone.DateTimeNow();
                            if (x.TrialHappenDate > time)
                                x.SessionStatus = "Upcoming";
                            else if (x.TrialHappenDate < time)
                                x.SessionStatus = "Past";
                        }
                        else
                        {
                            x.SessionStatus = " - ";
                        }

                    });


                    mSession.listOfCancelSession = listOfCancelSession;
                }
                else
                {
                    mSession.listOfCancelSession = null;
                }

                if (listOfJoinSession != null && listOfJoinSession.Count > 0)
                {
                    listOfJoinSession.ForEach(x => x.RequestedByName = context.UsersProfile.Where(y => y.FKUserID == x.FKRequestedBy).Select(t => new { Name = t.FirstName + " " + t.LastName }).FirstOrDefault().Name.ToString());
                    listOfJoinSession.ForEach(x => x.CurriculumName = context.Curriculums.Where(y => y.CurriculumID == x.FKCurriculumID).Select(t => t.CurriculumName).FirstOrDefault().ToString());
                    listOfJoinSession.ForEach(x => x.GradeName = context.Grades.Where(y => y.GradeID == x.FKGradeID).Select(t => t.GradeName).FirstOrDefault().ToString());
                    listOfJoinSession.ForEach(x => x.SubjectName = context.Subjects.Where(y => y.SubjectID == x.FKSubjectID).Select(t => t.SubjectName).FirstOrDefault().ToString());
                    listOfJoinSession.ForEach(x => x.SessionTimes = x.TrialHappenDate.ToString("f") + " - " + x.TrialHappenDate.AddHours(1).AddHours(-0).ToString("hh:mm tt") + " ");
                    listOfJoinSession.ForEach(x => x.RequestedTimes = x.RequestDate.ToString("f"));
                    listOfJoinSession.ForEach(x => {
                        if (x.RequestStatus == (int)Enumerations.RequestStatus.Pending)
                            x.RequestStatusName = "Pending";
                        else if (x.RequestStatus == (int)Enumerations.RequestStatus.Accepted)
                            x.RequestStatusName = "Request Accepted";
                        else if (x.RequestStatus == (int)Enumerations.RequestStatus.Rejected)
                            x.RequestStatusName = "Request Rejected";
                    });

                    listOfJoinSession.ForEach(x => {
                        if (x.RequestStatus == (int)Enumerations.RequestStatus.Accepted)
                        {
                            DateTime time = customTimeZone.DateTimeNow();
                            if (x.TrialHappenDate > time)
                                x.SessionStatus = "Upcoming";
                            else if (x.TrialHappenDate < time)
                                x.SessionStatus = "Past";
                        }
                        else
                        {
                            x.SessionStatus = " - ";
                        }

                    });


                    mSession.listOfJoinSession = listOfJoinSession;
                }
                else
                {
                    mSession.listOfJoinSession = null;
                }
                if (listOfPendingSession != null && listOfPendingSession.Count > 0)
                {
                    listOfPendingSession.ForEach(x => x.RequestedByName = context.UsersProfile.Where(y => y.FKUserID == x.FKRequestedBy).Select(t => new { Name = t.FirstName + " " + t.LastName }).FirstOrDefault().Name.ToString());
                    listOfPendingSession.ForEach(x => x.CurriculumName = context.Curriculums.Where(y => y.CurriculumID == x.FKCurriculumID).Select(t => t.CurriculumName).FirstOrDefault().ToString());
                    listOfPendingSession.ForEach(x => x.GradeName = context.Grades.Where(y => y.GradeID == x.FKGradeID).Select(t => t.GradeName).FirstOrDefault().ToString());
                    listOfPendingSession.ForEach(x => x.SubjectName = context.Subjects.Where(y => y.SubjectID == x.FKSubjectID).Select(t => t.SubjectName).FirstOrDefault().ToString());
                    listOfPendingSession.ForEach(x => x.SessionTimes = x.TrialHappenDate.ToString("f") + " - " + x.TrialHappenDate.AddHours(1).AddHours(-0).ToString("hh:mm tt") + " ");
                    listOfPendingSession.ForEach(x => x.RequestedTimes = x.RequestDate.ToString("f"));
                    listOfPendingSession.ForEach(x => {
                        if (x.RequestStatus == (int)Enumerations.RequestStatus.Pending)
                            x.RequestStatusName = "Pending";
                        else if (x.RequestStatus == (int)Enumerations.RequestStatus.Accepted)
                            x.RequestStatusName = "Request Accepted";
                        else if (x.RequestStatus == (int)Enumerations.RequestStatus.Rejected)
                            x.RequestStatusName = "Request Rejected";
                    });

                    listOfPendingSession.ForEach(x => {
                        if (x.RequestStatus == (int)Enumerations.RequestStatus.Accepted)
                        {
                            DateTime time = customTimeZone.DateTimeNow();
                            if (x.TrialHappenDate > time)
                                x.SessionStatus = "Upcoming";
                            else if (x.TrialHappenDate < time)
                                x.SessionStatus = "Past";
                        }
                        else
                        {
                            x.SessionStatus = " - ";
                        }

                    });


                    mSession.listOfPendingSession = listOfPendingSession;
                }
                else
                {
                    mSession.listOfPendingSession = null;
                }

                if (listOfUpcomingSession != null && listOfUpcomingSession.Count > 0)
                {
                    listOfUpcomingSession.ForEach(x => x.RequestedByName = context.UsersProfile.Where(y => y.FKUserID == x.FKRequestedBy).Select(t => new { Name = t.FirstName + " " + t.LastName }).FirstOrDefault().Name.ToString());
                    listOfUpcomingSession.ForEach(x => x.CurriculumName = context.Curriculums.Where(y => y.CurriculumID == x.FKCurriculumID).Select(t => t.CurriculumName).FirstOrDefault().ToString());
                    listOfUpcomingSession.ForEach(x => x.GradeName = context.Grades.Where(y => y.GradeID == x.FKGradeID).Select(t => t.GradeName).FirstOrDefault().ToString());
                    listOfUpcomingSession.ForEach(x => x.SubjectName = context.Subjects.Where(y => y.SubjectID == x.FKSubjectID).Select(t => t.SubjectName).FirstOrDefault().ToString());
                    listOfUpcomingSession.ForEach(x => x.SessionTimes = x.TrialHappenDate.ToString("f") + " - " + x.TrialHappenDate.AddHours(1).AddHours(-0).ToString("hh:mm tt") + " ");
                    listOfUpcomingSession.ForEach(x => x.RequestedTimes = x.RequestDate.ToString("f"));
                    listOfUpcomingSession.ForEach(x => {
                        if (x.RequestStatus == (int)Enumerations.RequestStatus.Pending)
                            x.RequestStatusName = "Pending";
                        else if (x.RequestStatus == (int)Enumerations.RequestStatus.Accepted)
                            x.RequestStatusName = "Request Accepted";
                        else if (x.RequestStatus == (int)Enumerations.RequestStatus.Rejected)
                            x.RequestStatusName = "Request Rejected";
                    });

                    listOfUpcomingSession.ForEach(x => {
                        if (x.RequestStatus == (int)Enumerations.RequestStatus.Accepted)
                        {
                            DateTime time = customTimeZone.DateTimeNow();
                            if (x.TrialHappenDate > time)
                                x.SessionStatus = "Upcoming";
                            else if (x.TrialHappenDate < time)
                                x.SessionStatus = "Past";
                        }
                        else
                        {
                            x.SessionStatus = " - ";
                        }

                    });


                    mSession.listOfUpcomingSession = listOfUpcomingSession;
                }
                else
                {
                    mSession.listOfUpcomingSession = null;
                }

                return mSession;

            }
            catch (Exception ex)
            {
                return null;
                throw;
            }
            return null;
        }

        public MySessions UpdatedStudentMySession(Int64 UserID)
        {
            try
            {
                MySessions mSession = new MySessions();

                List<Sessions> listOfPendingSession = new List<Sessions>();

                List<Sessions> listOfUpcomingSession = new List<Sessions>();

                List<Sessions> listOfCancelSession = new List<Sessions>();

                List<Sessions> listOfJoinSession = new List<Sessions>();

                List<Sessions> listOfPastSession = new List<Sessions>();

                var CurrentTime = customTimeZone.DateTimeNow();

                listOfPendingSession = context.Sessions.Where(x => x.FKRequestedBy == UserID && x.TrialHappenDate > CurrentTime && x.CurrentStatus == (int)Enumerations.SessionStatus.Pending && x.RequestStatus == (int)Enumerations.RequestStatus.Pending).ToList();

                listOfUpcomingSession = context.Sessions.Where(x => x.FKRequestedBy == UserID && x.TrialHappenDate > CurrentTime && x.CurrentStatus == (int)Enumerations.SessionStatus.Pending && x.RequestStatus == (int)Enumerations.RequestStatus.Accepted).ToList();

                var pSessions = context.SessionParticipants.Where(x => x.FKStudentID == UserID && x.IsActive == true).GroupBy(x => x.FKSessionID).Select(x => x.FirstOrDefault()).ToList();

                if (pSessions != null && pSessions.Count > 0)
                {
                    List<Int64> pIntList = pSessions.Select(x => x.FKSessionID).ToList();

                    listOfPastSession = context.Sessions.Where(x => x.FKRequestedBy == UserID && x.TrialHappenDate < CurrentTime && (x.CurrentStatus == (int)Enumerations.SessionStatus.Pending || x.CurrentStatus == (int)Enumerations.SessionStatus.Completed) && x.RequestStatus == (int)Enumerations.RequestStatus.Accepted && pIntList.Contains(x.TrialSessionID)).ToList();

                    if (listOfPastSession != null && listOfPastSession.Count > 0)
                    {
                        listOfPastSession.ForEach(x => x.RequestedByName = context.UsersProfile.Where(y => y.FKUserID == x.FKRequestedTo).Select(t => new { Name = t.FirstName + " " + t.LastName }).FirstOrDefault().Name.ToString());
                        listOfPastSession.ForEach(x => x.CurriculumName = context.Curriculums.Where(y => y.CurriculumID == x.FKCurriculumID).Select(t => t.CurriculumName).FirstOrDefault().ToString());
                        listOfPastSession.ForEach(x => x.GradeName = context.Grades.Where(y => y.GradeID == x.FKGradeID).Select(t => t.GradeName).FirstOrDefault().ToString());
                        listOfPastSession.ForEach(x => x.SubjectName = context.Subjects.Where(y => y.SubjectID == x.FKSubjectID).Select(t => t.SubjectName).FirstOrDefault().ToString());
                        listOfPastSession.ForEach(x => x.SessionTimes = x.TrialHappenDate.ToString("f") + " - " + x.TrialHappenDate.AddHours(1).AddHours(-0).ToString("hh:mm tt") + " ");
                        listOfPastSession.ForEach(x => x.RequestedTimes = x.RequestDate.ToString("f"));
                        listOfPastSession.ForEach(x => {
                            if (x.RequestStatus == (int)Enumerations.RequestStatus.Pending)
                                x.RequestStatusName = "Pending";
                            else if (x.RequestStatus == (int)Enumerations.RequestStatus.Accepted)
                                x.RequestStatusName = "Request Accepted";
                            else if (x.RequestStatus == (int)Enumerations.RequestStatus.Rejected)
                                x.RequestStatusName = "Request Rejected";
                        });

                        listOfPastSession.ForEach(x => {
                            if (x.RequestStatus == (int)Enumerations.RequestStatus.Accepted)
                            {
                                DateTime time = customTimeZone.DateTimeNow();
                                if (x.TrialHappenDate > time)
                                    x.SessionStatus = "Upcoming";
                                else if (x.TrialHappenDate < time)
                                    x.SessionStatus = "Past";
                            }
                            else
                            {
                                x.SessionStatus = " - ";
                            }

                        });

                        mSession.listOfPastSession = listOfPastSession;
                    }
                    else
                    {
                        listOfPastSession = null;
                        mSession.listOfPastSession = null;
                    }

                }
                else
                {
                    listOfPastSession = null;
                    mSession.listOfPastSession = null;

                }


                var jSessions = context.Sessions.Where(x => x.FKRequestedBy == UserID && x.RequestStatus == (int)Enumerations.RequestStatus.Accepted && x.CurrentStatus != (int)Enumerations.SessionStatus.Completed && x.CurrentStatus != (int)Enumerations.SessionStatus.NotTaken).ToList();

                if (jSessions != null && jSessions.Count > 0)
                {
                    jSessions.ForEach(x => {

                        var startTime = x.TrialHappenDate;
                        var endTime = x.TrialHappenDate.AddHours(1);
                        if (CurrentTime >= startTime && CurrentTime <= endTime)
                        {
                            listOfJoinSession.Add(x);
                        }
                    });
                    mSession.listOfJoinSession = listOfJoinSession;
                }
                else
                {
                    listOfJoinSession = null;
                    mSession.listOfJoinSession = null;
                }

                listOfCancelSession = context.Sessions.Where(x => x.FKRequestedBy == UserID && x.TrialHappenDate < CurrentTime && x.CurrentStatus == (int)Enumerations.SessionStatus.Pending && x.RequestStatus == (int)Enumerations.RequestStatus.Pending).ToList();

                if (listOfCancelSession != null && listOfCancelSession.Count > 0)
                {
                    listOfCancelSession.ForEach(x => x.RequestedByName = context.UsersProfile.Where(y => y.FKUserID == x.FKRequestedTo).Select(t => new { Name = t.FirstName + " " + t.LastName }).FirstOrDefault().Name.ToString());
                    listOfCancelSession.ForEach(x => x.CurriculumName = context.Curriculums.Where(y => y.CurriculumID == x.FKCurriculumID).Select(t => t.CurriculumName).FirstOrDefault().ToString());
                    listOfCancelSession.ForEach(x => x.GradeName = context.Grades.Where(y => y.GradeID == x.FKGradeID).Select(t => t.GradeName).FirstOrDefault().ToString());
                    listOfCancelSession.ForEach(x => x.SubjectName = context.Subjects.Where(y => y.SubjectID == x.FKSubjectID).Select(t => t.SubjectName).FirstOrDefault().ToString());
                    listOfCancelSession.ForEach(x => x.SessionTimes = x.TrialHappenDate.ToString("f") + " - " + x.TrialHappenDate.AddHours(1).AddHours(-0).ToString("hh:mm tt") + " ");
                    listOfCancelSession.ForEach(x => x.RequestedTimes = x.RequestDate.ToString("f"));
                    listOfCancelSession.ForEach(x => {
                        if (x.RequestStatus == (int)Enumerations.RequestStatus.Pending)
                            x.RequestStatusName = "Pending";
                        else if (x.RequestStatus == (int)Enumerations.RequestStatus.Accepted)
                            x.RequestStatusName = "Request Accepted";
                        else if (x.RequestStatus == (int)Enumerations.RequestStatus.Rejected)
                            x.RequestStatusName = "Request Rejected";
                    });

                    listOfCancelSession.ForEach(x => {
                        if (x.RequestStatus == (int)Enumerations.RequestStatus.Accepted)
                        {
                            DateTime time = customTimeZone.DateTimeNow();
                            if (x.TrialHappenDate > time)
                                x.SessionStatus = "Upcoming";
                            else if (x.TrialHappenDate < time)
                                x.SessionStatus = "Past";
                        }
                        else
                        {
                            x.SessionStatus = " - ";
                        }

                    });


                    mSession.listOfCancelSession = listOfCancelSession;
                }
                else
                {
                    mSession.listOfCancelSession = null;
                }

                if (listOfJoinSession != null && listOfJoinSession.Count > 0)
                {
                    listOfJoinSession.ForEach(x => x.RequestedByName = context.UsersProfile.Where(y => y.FKUserID == x.FKRequestedTo).Select(t => new { Name = t.FirstName + " " + t.LastName }).FirstOrDefault().Name.ToString());
                    listOfJoinSession.ForEach(x => x.CurriculumName = context.Curriculums.Where(y => y.CurriculumID == x.FKCurriculumID).Select(t => t.CurriculumName).FirstOrDefault().ToString());
                    listOfJoinSession.ForEach(x => x.GradeName = context.Grades.Where(y => y.GradeID == x.FKGradeID).Select(t => t.GradeName).FirstOrDefault().ToString());
                    listOfJoinSession.ForEach(x => x.SubjectName = context.Subjects.Where(y => y.SubjectID == x.FKSubjectID).Select(t => t.SubjectName).FirstOrDefault().ToString());
                    listOfJoinSession.ForEach(x => x.SessionTimes = x.TrialHappenDate.ToString("f") + " - " + x.TrialHappenDate.AddHours(1).AddHours(-0).ToString("hh:mm tt") + " ");
                    listOfJoinSession.ForEach(x => x.RequestedTimes = x.RequestDate.ToString("f"));
                    listOfJoinSession.ForEach(x => {
                        if (x.RequestStatus == (int)Enumerations.RequestStatus.Pending)
                            x.RequestStatusName = "Pending";
                        else if (x.RequestStatus == (int)Enumerations.RequestStatus.Accepted)
                            x.RequestStatusName = "Request Accepted";
                        else if (x.RequestStatus == (int)Enumerations.RequestStatus.Rejected)
                            x.RequestStatusName = "Request Rejected";
                    });

                    listOfJoinSession.ForEach(x => {
                        if (x.RequestStatus == (int)Enumerations.RequestStatus.Accepted)
                        {
                            DateTime time = customTimeZone.DateTimeNow();
                            if (x.TrialHappenDate > time)
                                x.SessionStatus = "Upcoming";
                            else if (x.TrialHappenDate < time)
                                x.SessionStatus = "Past";
                        }
                        else
                        {
                            x.SessionStatus = " - ";
                        }

                    });


                    mSession.listOfJoinSession = listOfJoinSession;
                }
                else
                {
                    mSession.listOfJoinSession = null;
                }
                if (listOfPendingSession != null && listOfPendingSession.Count > 0)
                {
                    listOfPendingSession.ForEach(x => x.RequestedByName = context.UsersProfile.Where(y => y.FKUserID == x.FKRequestedTo).Select(t => new { Name = t.FirstName + " " + t.LastName }).FirstOrDefault().Name.ToString());
                    listOfPendingSession.ForEach(x => x.CurriculumName = context.Curriculums.Where(y => y.CurriculumID == x.FKCurriculumID).Select(t => t.CurriculumName).FirstOrDefault().ToString());
                    listOfPendingSession.ForEach(x => x.GradeName = context.Grades.Where(y => y.GradeID == x.FKGradeID).Select(t => t.GradeName).FirstOrDefault().ToString());
                    listOfPendingSession.ForEach(x => x.SubjectName = context.Subjects.Where(y => y.SubjectID == x.FKSubjectID).Select(t => t.SubjectName).FirstOrDefault().ToString());
                    listOfPendingSession.ForEach(x => x.SessionTimes = x.TrialHappenDate.ToString("f") + " - " + x.TrialHappenDate.AddHours(1).AddHours(-0).ToString("hh:mm tt") + " ");
                    listOfPendingSession.ForEach(x => x.RequestedTimes = x.RequestDate.ToString("f"));
                    listOfPendingSession.ForEach(x => {
                        if (x.RequestStatus == (int)Enumerations.RequestStatus.Pending)
                            x.RequestStatusName = "Pending";
                        else if (x.RequestStatus == (int)Enumerations.RequestStatus.Accepted)
                            x.RequestStatusName = "Request Accepted";
                        else if (x.RequestStatus == (int)Enumerations.RequestStatus.Rejected)
                            x.RequestStatusName = "Request Rejected";
                    });

                    listOfPendingSession.ForEach(x => {
                        if (x.RequestStatus == (int)Enumerations.RequestStatus.Accepted)
                        {
                            DateTime time = customTimeZone.DateTimeNow();
                            if (x.TrialHappenDate > time)
                                x.SessionStatus = "Upcoming";
                            else if (x.TrialHappenDate < time)
                                x.SessionStatus = "Past";
                        }
                        else
                        {
                            x.SessionStatus = " - ";
                        }

                    });


                    mSession.listOfPendingSession = listOfPendingSession;
                }
                else
                {
                    mSession.listOfPendingSession = null;
                }

                if (listOfUpcomingSession != null && listOfUpcomingSession.Count > 0)
                {
                    listOfUpcomingSession.ForEach(x => x.RequestedByName = context.UsersProfile.Where(y => y.FKUserID == x.FKRequestedTo).Select(t => new { Name = t.FirstName + " " + t.LastName }).FirstOrDefault().Name.ToString());
                    listOfUpcomingSession.ForEach(x => x.CurriculumName = context.Curriculums.Where(y => y.CurriculumID == x.FKCurriculumID).Select(t => t.CurriculumName).FirstOrDefault().ToString());
                    listOfUpcomingSession.ForEach(x => x.GradeName = context.Grades.Where(y => y.GradeID == x.FKGradeID).Select(t => t.GradeName).FirstOrDefault().ToString());
                    listOfUpcomingSession.ForEach(x => x.SubjectName = context.Subjects.Where(y => y.SubjectID == x.FKSubjectID).Select(t => t.SubjectName).FirstOrDefault().ToString());
                    listOfUpcomingSession.ForEach(x => x.SessionTimes = x.TrialHappenDate.ToString("f") + " - " + x.TrialHappenDate.AddHours(1).AddHours(-0).ToString("hh:mm tt") + " ");
                    listOfUpcomingSession.ForEach(x => x.RequestedTimes = x.RequestDate.ToString("f"));
                    listOfUpcomingSession.ForEach(x => {
                        if (x.RequestStatus == (int)Enumerations.RequestStatus.Pending)
                            x.RequestStatusName = "Pending";
                        else if (x.RequestStatus == (int)Enumerations.RequestStatus.Accepted)
                            x.RequestStatusName = "Request Accepted";
                        else if (x.RequestStatus == (int)Enumerations.RequestStatus.Rejected)
                            x.RequestStatusName = "Request Rejected";
                    });

                    listOfUpcomingSession.ForEach(x => {
                        if (x.RequestStatus == (int)Enumerations.RequestStatus.Accepted)
                        {
                            DateTime time = customTimeZone.DateTimeNow();
                            if (x.TrialHappenDate > time)
                                x.SessionStatus = "Upcoming";
                            else if (x.TrialHappenDate < time)
                                x.SessionStatus = "Past";
                        }
                        else
                        {
                            x.SessionStatus = " - ";
                        }

                    });


                    mSession.listOfUpcomingSession = listOfUpcomingSession;
                }
                else
                {
                    mSession.listOfUpcomingSession = null;
                }

                return mSession;

            }
            catch (Exception ex)
            {
                return null;
                throw;
            }
            return null;
        }

        

        public Sessions GetSessionDetail(Int64 SessionID)
        {
            try
            {
               Sessions list = new Sessions();
                list = context.Sessions.Where(x => x.TrialSessionID == SessionID).FirstOrDefault();
                if (list != null)
                {
                    list.RequestedByName = context.UsersProfile.Where(y => y.FKUserID == list.FKRequestedBy).Select(t => new { Name = t.FirstName + " " + t.LastName }).FirstOrDefault().Name.ToString();
                    list.CurriculumName = context.Curriculums.Where(y => y.CurriculumID == list.FKCurriculumID).Select(t => t.CurriculumName).FirstOrDefault().ToString();
                    list.GradeName = context.Grades.Where(y => y.GradeID == list.FKGradeID).Select(t => t.GradeName).FirstOrDefault().ToString();
                    list.SubjectName = context.Subjects.Where(y => y.SubjectID == list.FKSubjectID).Select(t => t.SubjectName).FirstOrDefault().ToString();
                    list.SessionTimes = list.TrialHappenDate.ToString("f");// + " - " + list.TrialHappenDate.AddHours(1).Hour + " " + list.TrialHappenDate.AddHours(1).ToString("tt", CultureInfo.InvariantCulture);
                    list.RequestedTimes = list.RequestDate.ToString("f");
                    list.FromToTime = Convert.ToDateTime(list.TrialHappenDate).ToString("hh:mm tt") + " - " + Convert.ToDateTime(list.TrialHappenDate.AddHours(1)).ToString("hh:mm tt");  //list.TrialHappenDate.Hour + list.TrialHappenDate.ToString("tt", CultureInfo.InvariantCulture) + " -  "+ list.TrialHappenDate.AddHours(1).Hour + list.TrialHappenDate.AddHours(1).ToString("tt", CultureInfo.InvariantCulture);
                    list.RequestedToName = context.UsersProfile.Where(y => y.FKUserID == list.FKRequestedTo).Select(t => new { Name = t.FirstName + " " + t.LastName }).FirstOrDefault().Name.ToString();
                    if (list.RequestStatus == (int)Enumerations.RequestStatus.Pending)
                        list.RequestStatusName = "Pending";
                    else if (list.RequestStatus == (int)Enumerations.RequestStatus.Accepted)
                        list.RequestStatusName = "Request Accepted";
                    else if (list.RequestStatus == (int)Enumerations.RequestStatus.Rejected)
                        list.RequestStatusName = "Request Rejected";

                    if (list.SessionType == (int)Enumerations.SessionType.One)
                        list.SessionTypeName = "Once";
                    else if (list.SessionType == (int)Enumerations.SessionType.Many)
                        list.SessionTypeName = "Many";
                    else if (list.SessionType == (int)Enumerations.SessionType.Trial)
                        list.SessionTypeName = "Trial";

                    return list;
                }
                else
                    return list;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public int ChangeSessionRequestStatus(Int64 SessionID, int StatusID)
        {
            try
            {
                Sessions obj = context.Sessions.Where(x => x.TrialSessionID == SessionID && x.FKRequestedTo == Config.CurrentUser).FirstOrDefault();
                if(obj!=null)
                {
                    context.Sessions.Attach(obj);
                    obj.RequestStatus = StatusID;
                    if(StatusID==(int)Enumerations.RequestStatus.Accepted)
                    {
                        string p=string.Empty;
                        Guid g = Guid.NewGuid();
                        p = Convert.ToBase64String(g.ToByteArray());
                        p = p.Replace("=", "");
                        p = p.Replace("+", "");
                        obj.SessionLink = p;
                    }
                    else if (StatusID == (int)Enumerations.RequestStatus.Rejected)
                    {
                        obj.SessionLink = null;
                    }
                    int status=context.SaveChanges();
                    if(status>0)
                    {
                        string username = context.UsersProfile.Where(x => x.FKUserID == obj.FKRequestedBy).Select(x => new { Name = x.FirstName + " " + x.LastName }).FirstOrDefault().Name.ToString();
                        string useremail = context.Users.Where(x => x.UserID == obj.FKRequestedBy).Select(x => x.LoginEmail).FirstOrDefault().ToString();
                        string teacherName= context.UsersProfile.Where(x => x.FKUserID == obj.FKRequestedTo).Select(x => new { Name = x.FirstName + " " + x.LastName }).FirstOrDefault().Name.ToString();
                        string statuss = Enum.GetName(typeof(Enumerations.RequestStatus), StatusID).ToString();
                     //   bool result= EmailTemplate.EmailTemplate.SessionRequestStatus(username, useremail, obj.TopicName, teacherName, statuss);

                        // Add session request status into notifications 

                        Notifications nObj = new Notifications();
                        nObj.ActionPerformed = "Your request for session is being " + Enum.GetName(typeof(Enumerations.RequestStatus), StatusID).ToString() + " by " + teacherName;
                        nObj.FKSessionID = SessionID;
                        nObj.FKUserID = Convert.ToInt64(obj.FKRequestedBy);
                        nObj.IsCourse = (int)Enumerations.OneOrMany.Session;

                        NotificationManager nManager = new NotificationManager();
                        Int64 ID=nManager.AddNotification(nObj);


                        // add in UserActivities table
                        ActivityMgr = new UsersActivityManager();
                        obj_Activity = new UserActivities();
                        obj_Activity.ActivitySource = (int)Enumerations.ActivitySource.FrontsideActivity;
                        obj_Activity.ActionPerformed = "Session Request has " + Enum.GetName(typeof(Enumerations.RequestStatus), StatusID).ToString() + " by " + teacherName;
                        obj_Activity.FKUserID = Convert.ToInt64(obj.FKRequestedBy);
                        ActivityMgr.AddActivity(obj_Activity);
                    }

                    return status;
                }
                else
                {
                    return 0;
                }
            }
            catch(Exception ex)
            {
                throw ex;
            }

        }

        public List<Sessions> TeacherApprovedSessions(Int64 UserID)
        {
            try
            {
                List<Sessions> list = new List<Sessions>();
                list = context.Sessions.Where(x => x.FKRequestedTo == UserID).ToList();
                if (list != null)
                {
                    list.ForEach(x => x.RequestedByName = context.UsersProfile.Where(y => y.FKUserID == x.FKRequestedBy).Select(t => new { Name = t.FirstName + " " + t.LastName }).FirstOrDefault().Name.ToString());
                    list.ForEach(x => x.CurriculumName = context.Curriculums.Where(y => y.CurriculumID == x.FKCurriculumID).Select(t => t.CurriculumName).FirstOrDefault().ToString());
                    list.ForEach(x => x.GradeName = context.Grades.Where(y => y.GradeID == x.FKGradeID).Select(t => t.GradeName).FirstOrDefault().ToString());
                    list.ForEach(x => x.SubjectName = context.Subjects.Where(y => y.SubjectID == x.FKSubjectID).Select(t => t.SubjectName).FirstOrDefault().ToString());
                    //  list.ForEach(x => x.SessionTimes = x.TrialHappenDate.ToString("f") + " - " + x.TrialHappenDate.AddHours(1).Hour + " " + x.TrialHappenDate.AddHours(1).ToString("tt", CultureInfo.InvariantCulture));
                    list.ForEach(x => x.SessionTimes = x.TrialHappenDate.ToString("f") + " - " + x.TrialHappenDate.AddHours(1).AddHours(-0).ToString("hh:mm tt") + " ");

                    list.ForEach(x => x.RequestedTimes = x.RequestDate.ToString("f"));


                    list.ForEach(x => {
                        if (x.RequestStatus == (int)Enumerations.RequestStatus.Pending)
                            x.RequestStatusName = "Pending";
                        else if (x.RequestStatus == (int)Enumerations.RequestStatus.Accepted)
                            x.RequestStatusName = "Request Accepted";
                        else if (x.RequestStatus == (int)Enumerations.RequestStatus.Rejected)
                            x.RequestStatusName = "Request Rejected";
                    });
                    return list;
                }
                else
                    return list;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<Sessions> ListOfStudentSessions(Int64 UserID)
        {
            try
            {
                List<Sessions> list = new List<Sessions>();
                list = context.Sessions.Where(x => x.FKRequestedBy == UserID && x.RequestStatus == (int)Enumerations.RequestStatus.Accepted).ToList();
                return list;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return null;
        }

        public List<CalenderViewModel> ListOfUserSessions(Int64 UserID)
        {
            List<CalenderViewModel> list = new List<CalenderViewModel>();
            return list;
        }

        public Sessions TeacherJoiningSession(Int64 UserID)
        {
            List<Sessions> obj = new List<Sessions>();
            Sessions s = new Sessions();
            DateTime currentTime = customTimeZone.DateTimeNow();
            bool check = false;
            obj = context.Sessions.Where(x => x.FKRequestedTo == UserID && x.RequestStatus == (int)Enumerations.RequestStatus.Accepted && x.CurrentStatus!=(int)Enumerations.SessionStatus.Completed && x.CurrentStatus != (int)Enumerations.SessionStatus.NotTaken).ToList();
            obj.ForEach(x =>
            {
                var startTime = x.TrialHappenDate;
                var endTime = x.TrialHappenDate.AddHours(1);
                if (currentTime >= startTime && currentTime <= endTime)
                {
                    s = x;
                    check = true;
                }
                else if(check!=true)
                {
                    s = null;
                }

            });
            if (s != null)
            {
                s.SubjectName = Convert.ToString(context.Subjects.Where(x => x.SubjectID == s.FKSubjectID).Select(x => x.SubjectName).FirstOrDefault());
                s.RequestedByName =Convert.ToString( context.UsersProfile.Where(x => x.FKUserID == s.FKRequestedBy).Select(x => new { Name = x.FirstName + " " + x.LastName }).FirstOrDefault().Name);
                return s;
            }
            else
                return null;
        }

        public Sessions StudentJoiningSession(Int64 UserID)
        {
            List<Sessions> obj = new List<Sessions>();
            Sessions s = new Sessions();
            bool check = false;
            DateTime currentTime = customTimeZone.DateTimeNow();
            obj = context.Sessions.Where(x => x.FKRequestedBy == UserID && x.RequestStatus == (int)Enumerations.RequestStatus.Accepted && x.CurrentStatus != (int)Enumerations.SessionStatus.Completed && x.CurrentStatus != (int)Enumerations.SessionStatus.NotTaken).ToList();
            if(obj.Count>0)
            { 
            obj.ForEach(x =>
            {
                var startTime = x.TrialHappenDate;
                var endTime = x.TrialHappenDate.AddHours(1);
                if (currentTime >= startTime && currentTime <= endTime)
                {
                    s = x;
                    check = true;
                }
                else if (check != true)
                {
                    s = null;
                }

            });
                if (s != null)
                {
                    s.SubjectName = context.Subjects.Where(x => x.SubjectID == s.FKSubjectID).Select(x => x.SubjectName).FirstOrDefault().ToString();
                    s.RequestedToName = context.UsersProfile.Where(x => x.FKUserID == s.FKRequestedTo).Select(x => new { Name = x.FirstName + " " + x.LastName }).FirstOrDefault().Name.ToString();
                    return s;
                }
                else
                    return null;
            }
            return null;
        }


        public UpcomingCourses JoinCourseSession(Int64 UserID, Int64 CourseID)
        {

           UpcomingCourses ObjUpcoming = new UpcomingCourses();

            DateTime cTime= customTimeZone.DateTimeNow();
            CoursesHeader cObj = new CoursesHeader();
            cObj = context.CourseHeader.Where(x => x.CoursesHeaderID == CourseID && x.IsActive == true).FirstOrDefault();

            if(cObj!=null)
            {

                List<Int64> ListOfCourseSchedule = new List<Int64>();
                ListOfCourseSchedule = context.CourseSchedule.Where(x => x.FKCourseID == cObj.CoursesHeaderID && x.IsActive==true).Select(x=>x.CourseScheduleID).ToList();

                if(ListOfCourseSchedule!=null && ListOfCourseSchedule.Count>0)
                {

                    List<StudentCourseSchedule> ListOfStudentCourseSchedule = new List<StudentCourseSchedule>();
                    ListOfStudentCourseSchedule = context.StudentCourseSchedule.Where(x => x.FKStudentID == UserID && x.IsActive==true).ToList();

                    ObjUpcoming.JoinSchedule = new List<CourseSchedule>();
                    ObjUpcoming.UpcomingSchedules = new List<CourseSchedule>();

                    foreach(var item in ListOfCourseSchedule)
                    {
                        if(ListOfStudentCourseSchedule.Any(x=>x.FKCourseScheduleID==item))
                        {

                            var CourseScheduleObj = context.CourseSchedule.Where(x=>x.CourseScheduleID==item && x.IsActive==true && x.CurrentStatus == (int)Enumerations.SessionStatus.Pending).FirstOrDefault();
                            if (CourseScheduleObj != null)
                            {

                                var startTime = CourseScheduleObj.CourseSessionDate;
                                var endTime = CourseScheduleObj.CourseSessionDate.AddHours(1);

                                if (cTime >= startTime && cTime <= endTime)
                                {
                                    int count = context.CourseParticipants.Where(x => x.IsActive == true && x.FKCourseID == CourseScheduleObj.FKCourseID).Count();

                                    if (count>0)
                                    {
                                        ObjUpcoming.JoinSchedule.Add(CourseScheduleObj);

                                    }
                                }
                                else if (CourseScheduleObj.CourseSessionDate > cTime)
                                {
                                    ObjUpcoming.UpcomingSchedules.Add(CourseScheduleObj);
                                }
                            }
                        }
                    }

                    return ObjUpcoming;

                }
                else
                {
                    return null;
                }

            }
            else
            {
                return null;
            }         
        }


        public UpcomingCourses TeacherJoinCourseSession(Int64 UserID, Int64 CourseID)
        {

            UpcomingCourses ObjUpcoming = new UpcomingCourses();

            DateTime cTime = customTimeZone.DateTimeNow();
            CoursesHeader cObj = new CoursesHeader();
            cObj = context.CourseHeader.Where(x => x.CoursesHeaderID == CourseID && x.IsActive == true && x.FKOfferedBY==UserID).FirstOrDefault();

            if (cObj != null)
            {

                List<Int64> ListOfCourseSchedule = new List<Int64>();
                ListOfCourseSchedule = context.CourseSchedule.Where(x => x.FKCourseID == cObj.CoursesHeaderID && x.IsActive == true).Select(x => x.CourseScheduleID).ToList();

                if (ListOfCourseSchedule != null && ListOfCourseSchedule.Count > 0)
                {

                    //List<StudentCourseSchedule> ListOfStudentCourseSchedule = new List<StudentCourseSchedule>();
                    //ListOfStudentCourseSchedule = context.StudentCourseSchedule.Where(x => x.FKStudentID == UserID && x.IsActive == true).ToList();

                    ObjUpcoming.JoinSchedule = new List<CourseSchedule>();
                    ObjUpcoming.UpcomingSchedules = new List<CourseSchedule>();

                    foreach (var item in ListOfCourseSchedule)
                    {
                      //  if (ListOfStudentCourseSchedule.Any(x => x.FKCourseScheduleID == item))
                        //{

                            var CourseScheduleObj = context.CourseSchedule.Where(x => x.CourseScheduleID == item && x.IsActive == true && x.CurrentStatus==(int)Enumerations.SessionStatus.Pending).FirstOrDefault();

                        if(CourseScheduleObj!=null)
                        {
                            var startTime = CourseScheduleObj.CourseSessionDate;
                            var endTime = CourseScheduleObj.CourseSessionDate.AddHours(1);

                            if (cTime >= startTime && cTime <= endTime)
                            {

                                int count = context.CourseParticipants.Where(x => x.IsActive == true && x.FKCourseID == CourseScheduleObj.FKCourseID).Count();

                                if (count > 0)
                                {
                                    ObjUpcoming.JoinSchedule.Add(CourseScheduleObj);

                                }
                               
                            }
                            else if (CourseScheduleObj.CourseSessionDate > cTime)
                            {
                                ObjUpcoming.UpcomingSchedules.Add(CourseScheduleObj);
                            }
                        }
                           
                       // }
                    }

                    return ObjUpcoming;

                }
                else
                {
                    return null;
                }

            }
            else
            {
                return null;
            }
        }



        public bool IsUserOnline(Int64 UserID)
        {
            bool check = false;
            var s = context.Users.Where(x => x.UserID == UserID && x.IsActive==true).FirstOrDefault();
            if (s != null)
            {
               
                if (s.IsOnline!=true)
                {
                    check = true;
                }
                else
                {
                    check = false;
                }
            }
            else
            {
                check = false;
            }
            return check;
        }

        public bool CanJoinSession(string SessionLink,Int64 UserID, Int64 SessionID)
        {
            Sessions s = new Sessions();
            bool check = false;
            DateTime currentTime = customTimeZone.DateTimeNow();
            s = context.Sessions.Where(x => x.RequestStatus == (int)Enumerations.RequestStatus.Accepted && x.TrialSessionID==SessionID && x.SessionLink==SessionLink && (x.FKRequestedBy == UserID || x.FKRequestedTo==UserID)).FirstOrDefault();
            if (s != null)
            {
                var startTime = s.TrialHappenDate;
                var endTime = s.TrialHappenDate.AddHours(1);
                if (currentTime >= startTime && currentTime <= endTime)
                {
                    check = true;
                }
                else
                {
                    check = false;
                }
            }
            else
            {
                check = false;
            }

            return check;
        }

        public CourseSchedule GetCourseSchedule(Int64 SID)
        {
            try
            {
                CourseSchedule obj = new CourseSchedule();
                obj = context.CourseSchedule.Where(x => x.CourseScheduleID == SID && x.IsActive == true).FirstOrDefault();
                if (obj != null)
                    return obj;
                else
                    return null;
            }
            catch (Exception)
            {

                throw;
            }

        }


        public bool CanCourseJoinSession(Int64 CourseID, Int64 UserID, Int64 ScheduleID)
        {

            bool check = false;
            DateTime currentTime = customTimeZone.DateTimeNow();

            if(Config.User.UserType==(int)Enumerations.UserType.Teacher)
            {

                bool test = context.CourseHeader.Any(x => x.CoursesHeaderID == CourseID && x.FKOfferedBY == UserID && x.IsActive == true);

                if(test==true)
                {
                    var s = context.CourseSchedule.Where(x => x.CourseScheduleID == ScheduleID && x.IsActive == true).FirstOrDefault();
                    if (s != null)
                    {
                        var startTime = s.CourseSessionDate;
                        var endTime = s.CourseSessionDate.AddHours(1);
                        if (currentTime >= startTime && currentTime <= endTime)
                        {
                            check = true;
                        }
                        else
                        {
                            check = false;
                        }
                    }
                    else
                    {
                        check = false;
                    }
                }
                else
                {
                    check = false; 
                }

               

            }
            else if (Config.User.UserType == (int)Enumerations.UserType.Student)
            {
                bool test = context.CourseParticipants.Any(x => x.FKCourseID == CourseID && x.FKStudentID == UserID && x.IsActive == true);

                if (test == true)
                {
                    var a = context.StudentCourseSchedule.Where(x => x.FKCourseScheduleID == ScheduleID && x.IsActive == true && x.FKStudentID==UserID).FirstOrDefault();
                    if (a != null)
                    {
                        var s = context.CourseSchedule.Where(x => x.CourseScheduleID == ScheduleID && x.IsActive == true).FirstOrDefault();
                        if (s != null)
                        {
                            var startTime = s.CourseSessionDate;
                            var endTime = s.CourseSessionDate.AddHours(1);
                            if (currentTime >= startTime && currentTime <= endTime)
                            {
                                check = true;
                            }
                            else
                            {
                                check = false;
                            }
                        }
                        else
                        {
                            check = false;
                        }

                    }
                    else
                    {
                        check = false;
                    }
                }
                else
                {
                    check= false;
                }
            }
            else
            {
                check= false;
            }
      
            return check;
        }

       //public bool ServiceCanCourseJoinSession(Int64 CourseID, Int64 UserID, Int64 ScheduleID)
       // {

       //     bool check = false;
       //     DateTime currentTime = customTimeZone.DateTimeNow();

       //     if (Config.User.UserType == (int)Enumerations.UserType.Teacher)
       //     {

       //         bool test = context.CourseHeader.Any(x => x.CoursesHeaderID == CourseID && x.FKOfferedBY == UserID && x.IsActive == true);

       //         if (test == true)
       //         {
       //             var s = context.CourseSchedule.Where(x => x.CourseScheduleID == ScheduleID && x.IsActive == true).FirstOrDefault();
       //             if (s != null)
       //             {
       //                 var startTime = s.CourseSessionDate;
       //                 var endTime = s.CourseSessionDate.AddHours(1);
       //                 if (currentTime >= startTime && currentTime <= endTime)
       //                 {
       //                     check = true;
       //                 }
       //                 else
       //                 {
       //                     check = false;
       //                 }
       //             }
       //             else
       //             {
       //                 check = false;
       //             }
       //         }
       //         else
       //         {
       //             check = false;
       //         }

       //     }
       //     else if (Config.User.UserType == (int)Enumerations.UserType.Student)
       //     {
       //         bool test = context.CourseParticipants.Any(x => x.FKCourseID == CourseID && x.FKStudentID == UserID && x.IsActive == true);

       //         if (test == true)
       //         {
       //             var a = context.StudentCourseSchedule.Where(x => x.FKCourseScheduleID == ScheduleID && x.IsActive == true && x.FKStudentID == UserID).FirstOrDefault();
       //             if (a != null)
       //             {
       //                 var s = context.CourseSchedule.Where(x => x.CourseScheduleID == ScheduleID && x.IsActive == true).FirstOrDefault();
       //                 if (s != null)
       //                 {
       //                     var startTime = s.CourseSessionDate;
       //                     var endTime = s.CourseSessionDate.AddHours(1);
       //                     if (currentTime >= startTime && currentTime <= endTime)
       //                     {
       //                         check = true;
       //                     }
       //                     else
       //                     {
       //                         check = false;
       //                     }
       //                 }
       //                 else
       //                 {
       //                     check = false;
       //                 }

       //             }
       //             else
       //             {
       //                 check = false;
       //             }
       //         }
       //         else
       //         {
       //             check = false;
       //         }
       //     }
       //     else
       //     {
       //         check = false;
       //     }

       //     return check;
       // }





        public Sessions CanUserJoinSession(Int64 UserID)
        {
            Sessions s = new Sessions();
            DateTime currentTime = customTimeZone.DateTimeNow();
            List<Sessions> list = context.Sessions.Where(x => x.RequestStatus == (int)Enumerations.RequestStatus.Accepted &&  (x.FKRequestedBy == UserID || x.FKRequestedTo == UserID)).ToList();

            if (list != null && list.Count>0)
            {
                foreach(var item in list)
                {
                    var startTime = item.TrialHappenDate;
                    var endTime = item.TrialHappenDate.AddHours(1);
                    if (currentTime >= startTime && currentTime <= endTime)
                    {
                        return item;
                    }
                    
                }
                return null;
            }
            else
            {
                return null;
            }

            return null;
        }


        public bool UpdateSession(Int64 SessionID)
        {
            try
            {

            Sessions s = new Sessions();
            bool check = false;
            DateTime currentTime = customTimeZone.DateTimeNow();
            s = context.Sessions.Where(x => x.TrialSessionID == SessionID).FirstOrDefault();
            if (s != null)
            {
                context.Sessions.Attach(s);
                s.CurrentStatus = (int)Enumerations.SessionStatus.Completed;
                context.SaveChanges();
                check= UpdateSessionDuration(SessionID);
                
            }
            else
            {
                check = false;
            }

            return check;
            }
            catch (Exception ex)
            {

            }
            return false;
        }


        public bool UpdateSessionDuration(Int64 SessionID)
        {
            try
            {
            
            bool check = false;

            SessionsDuration duration = new SessionsDuration();
            duration = context.SessionsDuration.Where(x=>x.SessionID==SessionID).FirstOrDefault();
            DateTime CurrentTime = customTimeZone.DateTimeNow();

            if (duration != null)
            {
                context.SessionsDuration.Attach(duration);
                duration.EndTime = CurrentTime;
                context.SaveChanges();
                check = true;
            }
            else
            {
                check = false;
            }

            return check;
            }
            catch (Exception ex)
            {

            }
            return false;
        }


        public Int64 AddSessionDuration(Int64 SessionID)
        {
            try
            {

            var isExist = context.SessionsDuration.Where(x=>x.SessionID==SessionID).FirstOrDefault();

                if (isExist == null)
                { 
            SessionsDuration duration = new SessionsDuration();
            duration.StartTime = customTimeZone.DateTimeNow();
            duration.EndTime = null;
            duration.IsActive = true;
            duration.SessionID = SessionID;
            duration.FKJoinedBy = Config.CurrentUser;
            context.SessionsDuration.Add(duration);
            context.SaveChanges();

            return duration.SessionsDurationID;
                }
                else
                {
                    return 1;
                }
            }
            catch (Exception ex)
            {

            }
            return 0;
        }


        //public List<WeeklyReportViewModel> StudentWeeklyReport(Int64 UserID)
        //{
        //    List<WeeklyReportViewModel> list = new List<WeeklyReportViewModel>();
        //    WeeklyReportViewModel model = new WeeklyReportViewModel();

        //    List<SessionParticipants> sessionList = context.SessionParticipants.Where(x => x.FKStudentID == UserID && x.IsActive == true).ToList();
        //    if(sessionList!=null)
        //    {

        //    }

        //    return null;
        //}


        public List<WeeklyReportViewModel> StudentWeeklyReport(Int64 UserID)
        {
            DateTime today = DateTime.Today;
            int currentDayOfWeek = (int)today.DayOfWeek;
            var user = context.Users.Where(x => x.UserID == UserID && x.IsActive == true).FirstOrDefault();
            DateTime sunday = today.AddDays(-currentDayOfWeek);
            DateTime monday = sunday.AddDays(1);
            if (currentDayOfWeek == 0)
            {
                monday = monday.AddDays(-7);
            }
            var StartDate = monday;

            var dates = Enumerable.Range(0, 8).Select(days => monday.AddDays(days)).ToList();
            var EndDate = dates[7];

           List<DateTime> ls = context.SessionParticipants.Where(x => x.FKStudentID == UserID && x.IsActive == true && x.JoinDate<=EndDate && x.JoinDate >=StartDate).OrderBy(x=>x.JoinDate).Select(x=>x.JoinDate).ToList();
           List<SessionParticipants> participants = context.SessionParticipants.Where(x => x.FKStudentID == UserID && x.IsActive == true && x.JoinDate <= EndDate && x.JoinDate >= StartDate).OrderBy(x => x.JoinDate).ToList();

           if (ls.Count>0)
           {
                List<WeeklyReportViewModel> list = new List<WeeklyReportViewModel>();
               
                for (int i = 0; i < 7; i++)
                {
                   
                    if (dates.Contains(dates[i]))
                    {
                        WeeklyReportViewModel obj = new WeeklyReportViewModel();
                        obj.Date = dates[i].ToString("dd/MM/yyyy");
                        obj.Day = dates[i].ToString("ddd");

                        if (user.UserType == (int)Enumerations.UserType.Teacher)
                        {
                            var cost = participants.Where(x => x.JoinDate.Date == dates[i].Date).Select(x => new
                            {

                                sCost = context.Sessions.Where(y => y.TrialSessionID == x.FKSessionID).Select(y => y.SessionCost).FirstOrDefault()

                            }).Sum(x => x.sCost);

                            dynamic costD= (cost.HasValue) ? cost : 0;
                            obj.TotalDailyEarnings = costD;
                        }
                        else
                        {
                            obj.TotalDailyEarnings = 0;
                        }
                        var s = ls.Where(x => x.Date == dates[i].Date).Count();
                        obj.TotalHours = s;
                        
                        list.Add(obj);
                    }

                }
                return list;
            }
            else
            {
                List<WeeklyReportViewModel> list = new List<WeeklyReportViewModel>();
               
                for (int i = 0; i < 7; i++)
                {
                    WeeklyReportViewModel obj = new WeeklyReportViewModel();
                    obj.Date = dates[i].ToString("dd/MM/yyyy");
                    obj.Day = dates[i].ToString("ddd");
                    obj.TotalHours = 0;
                    obj.TotalDailyEarnings = 0;
                    list.Add(obj);
                }
                return list;
            }

            return null;
        }

        public double TeacherTotalEarnings(Int64 UserID)
        {
            List<SessionParticipants> p = context.SessionParticipants.Where(x=>x.FKStudentID==UserID).ToList();
            List<SessionParticipants> participants = context.SessionParticipants.Where(x => x.FKStudentID == UserID && x.IsActive == true).ToList();
            var cost = participants.Where(x => x.FKStudentID==UserID).Select(x => new
            {
                sCost = context.Sessions.Where(y => y.TrialSessionID == x.FKSessionID && y.FKRequestedTo==UserID).Select(y => y.SessionCost).FirstOrDefault()
            }).Sum(x => x.sCost);
            dynamic costD = (cost.HasValue) ? cost : 0;
            return costD;
       }

        public int TeacherTotalStudents(Int64 UserID)
        {
            List<Sessions> sessions = context.Sessions.Where(x => x.FKRequestedTo== UserID).ToList();
            List<Int64> alreadyExist = new List<Int64>();
            sessions.ForEach(x =>
            {                
                var a = context.SessionParticipants.Where(y => y.FKSessionID == x.TrialSessionID && y.FKStudentID != UserID).Select(y=>y.FKStudentID);
                foreach (var item in a)
                {
                    if (!alreadyExist.Contains(item))
                    {
                        alreadyExist.Add(item);
                    }
                }
            });
            int count = alreadyExist.Count();
            return count;
        }

        public int TeacherSessionCount(Int64 UserID)
        {
                       // context.SessionParticipants.Where(x => x.FKStudentID == UserID && x.IsActive == true).GroupBy(x => x.FKSessionID).Select(x => x.FirstOrDefault()).ToList();

            int count = context.SessionParticipants.Where(x=>x.FKStudentID==UserID && x.IsActive == true).GroupBy(x=>x.FKSessionID).Select(x=>x.FirstOrDefault()).ToList().Count();
            return count;
        }

        public int TeacherCourseCount(Int64 UserID)
        {
            int count = context.CourseHeader.Where(x => x.FKOfferedBY== UserID).Count();
            return count;
        }


        public Int64 AddSessionParticipant(SessionParticipants model)
        {
            try
            {             
                SessionParticipants participant = new SessionParticipants();
                participant = model;
                context.SessionParticipants.Add(participant);
                context.SaveChanges();
                return participant.ParticipantID;

            }
            catch (Exception ex)
            {
                throw ex;
            }
            return 0;
        }

        public List<UsersProfile> MyStudents(Int64 UserID)
        {
            List<Sessions> sessions = context.Sessions.Where(x => x.FKRequestedTo == UserID).ToList();
            List<Int64> alreadyExist = new List<Int64>();
            sessions.ForEach(x =>
            {
                var a = context.SessionParticipants.Where(y => y.FKSessionID == x.TrialSessionID && y.FKStudentID != UserID).Select(y => y.FKStudentID);
                foreach (var item in a)
                {
                    if (!alreadyExist.Contains(item))
                    {
                        alreadyExist.Add(item);
                    }
                }
            });
           
            if(alreadyExist.Count>0)
            {
                List<UsersProfile> profiles = new List<UsersProfile>();
                foreach(var item in alreadyExist)
                {
                    UsersProfile obj = new UsersProfile();
                    obj = context.UsersProfile.Where(x=>x.FKUserID==item).FirstOrDefault();
                    if (obj != null)
                    { profiles.Add(obj); }
                }
                profiles.ForEach(x =>
                {
                   string gName = context.StudentGrades.Where(y => y.FKUserID == x.FKUserID).Select(y => y.GradeName).FirstOrDefault();
                    if (!string.IsNullOrEmpty(gName))
                        x.StateName = gName;
                    else
                        x.StateName = "N/A";
                }
                );
                return profiles;
            }  
            else 
            {
                return null;
            }

            return null;
        }
        public List<UsersProfile> MyStudents_ForGrade(Int64 UserID, int GradeId)
        {
            List<Sessions> sessions = context.Sessions.Where(x => x.FKRequestedTo == UserID).ToList();
            List<Int64> alreadyExist = new List<Int64>();
            sessions.ForEach(x =>
            {
                var a = context.SessionParticipants.Where(y => y.FKSessionID == x.TrialSessionID && y.FKStudentID != UserID).Select(y => y.FKStudentID);
                foreach (var item in a)
                {
                    if (!alreadyExist.Contains(item))
                    {
                        alreadyExist.Add(item);
                    }
                }
            });

            if (alreadyExist.Count > 0)
            {
                List<UsersProfile> profiles = new List<UsersProfile>();
                foreach (var item in alreadyExist)
                {
                    UsersProfile obj = new UsersProfile();
                    obj = context.UsersProfile.Where(x => x.FKUserID == item).FirstOrDefault();
                    if (obj != null)
                    {
                        string gName = context.StudentGrades.Where(y => y.FKUserID == obj.FKUserID && y.StudentGradeID == GradeId).Select(y => y.GradeName).FirstOrDefault();
                        if (!string.IsNullOrEmpty(gName))
                        {
                            obj.StateName = gName;
                            profiles.Add(obj);
                        }
                       
                    }
                }
              
                return profiles;
            }
            else
            {
                return null;
            }

            return null;
        }

        public List<UsersProfile> MyStudents_ForCity(Int64 UserID, string cityName)
        {
            List<Sessions> sessions = context.Sessions.Where(x => x.FKRequestedTo == UserID).ToList();
            List<Int64> alreadyExist = new List<Int64>();
            sessions.ForEach(x =>
            {
                var a = context.SessionParticipants.Where(y => y.FKSessionID == x.TrialSessionID && y.FKStudentID != UserID).Select(y => y.FKStudentID);
                foreach (var item in a)
                {
                    if (!alreadyExist.Contains(item))
                    {
                        alreadyExist.Add(item);
                    }
                }
            });

            if (alreadyExist.Count > 0)
            {
                List<UsersProfile> profiles = new List<UsersProfile>();
                foreach (var item in alreadyExist)
                {
                    UsersProfile obj = new UsersProfile();
                    obj = context.UsersProfile.Where(x => x.FKUserID == item && x.CityName == cityName).FirstOrDefault();
                    if (obj != null)
                    { profiles.Add(obj); }
                }
                profiles.ForEach(x =>
                {
                    string gName = context.StudentGrades.Where(y => y.FKUserID == x.FKUserID).Select(y => y.GradeName).FirstOrDefault();
                    if (!string.IsNullOrEmpty(gName))
                        x.StateName = gName;
                    else
                        x.StateName = "N/A";
                }
                );
                return profiles;
            }
            else
            {
                return null;
            }

            return null;
        }
        public List<StudentGrades> StudentGrades(Int64 UserID)
        {
            List<StudentGrades> listGrades = new List< StudentGrades>();
            StudentGrades stGrade = new StudentGrades();

            stGrade.GradeName = "N/A";
            stGrade.StudentGradeID = 0;
            listGrades.Add(stGrade);

            List<Sessions> sessions = context.Sessions.Where(x => x.FKRequestedTo == UserID).ToList();
            List<Int64> alreadyExist = new List<Int64>();
            sessions.ForEach(x =>
            {
                var a = context.SessionParticipants.Where(y => y.FKSessionID == x.TrialSessionID && y.FKStudentID != UserID).Select(y => y.FKStudentID);
                foreach (var item in a)
                {
                    if (!alreadyExist.Contains(item))
                    {
                        alreadyExist.Add(item);
                    }
                }
            });

            if (alreadyExist.Count > 0)
            {
                List<UsersProfile> profiles = new List<UsersProfile>();
                foreach (var item in alreadyExist)
                {
                    UsersProfile obj = new UsersProfile();
                    obj = context.UsersProfile.Where(x => x.FKUserID == item).FirstOrDefault();
                    if (obj != null)
                    { profiles.Add(obj); }
                }
                profiles.ForEach(x =>
                {
                    stGrade = new StudentGrades();
                    var grd = context.StudentGrades.Where(y => y.FKUserID == x.FKUserID).FirstOrDefault();
                    string gName = context.StudentGrades.Where(y => y.FKUserID == x.FKUserID).Select(y => y.GradeName).FirstOrDefault();
                    if (!string.IsNullOrEmpty(gName))
                    {
                        x.StateName = gName;
                        stGrade.GradeName = grd.GradeName;
                        stGrade.StudentGradeID = grd.StudentGradeID;
                        stGrade.FKUserID = grd.FKUserID;
                        listGrades.Add(stGrade);
                    }
                    else
                        x.StateName = "N/A";

                   
                }
                );
                return listGrades;
            }
            else
            {
                return null;
            }

            return null;
        }
        public List<StudentGrades> StudentGrades()
        {
            return context.StudentGrades.ToList();
        }
        public List<UsersProfile> MyTeacher(Int64 UserID)
        {
            List<Sessions> sessions = context.Sessions.Where(x => x.FKRequestedBy == UserID).ToList();
            List<Int64> alreadyExist = new List<Int64>();
            sessions.ForEach(x =>
            {
                var a = context.SessionParticipants.Where(y => y.FKSessionID == x.TrialSessionID && y.FKStudentID != UserID).Select(y => y.FKStudentID);
                foreach (var item in a)
                {
                    if (!alreadyExist.Contains(item))
                    {
                        alreadyExist.Add(item);
                    }
                }
            });
            if (alreadyExist.Count > 0)
            {
                List<UsersProfile> profiles = new List<UsersProfile>();
                foreach (var item in alreadyExist)
                {
                    UsersProfile obj = new UsersProfile();
                    obj = context.UsersProfile.Where(x => x.FKUserID == item).FirstOrDefault();
                    if (obj != null)
                    { profiles.Add(obj); }
                }
                profiles.ForEach(x => x.StateName = context.States.Where(y => y.StateID == x.FKStateID).Select(y => y.StateName).FirstOrDefault());
                profiles.ForEach(x => x.IsWebcam = context.Users.Where(y => y.UserID == x.FKUserID).Select(y => y.IsOnline).FirstOrDefault());
                return profiles;
            }
            else
            {
                return null;
            }

            return null;
        }

        public List<UsersProfile> MyTeacherGetNotes(Int64 UserID)
        {
            List<Sessions> sessions = context.Sessions.Where(x => x.FKRequestedBy == UserID).GroupBy(c => c.FKRequestedTo).Select(grouping => grouping.FirstOrDefault()).ToList();
            //List<Int64> alreadyExist = new List<Int64>();
            //sessions.ForEach(x =>
            //{
            //    var a = context.SessionParticipants.Select(y => y.FKStudentID);
            //    foreach (var item in a)
            //    {
            //        if (!alreadyExist.Contains(item))
            //        {
            //            alreadyExist.Add(item);
            //        }
            //    }
            //});
            if (sessions.Count > 0)
            {
                List<UsersProfile> profiles = new List<UsersProfile>();
                foreach (var item in sessions)
                {
                    UsersProfile obj = new UsersProfile();
                    obj = context.UsersProfile.Where(x => x.FKUserID == item.FKRequestedTo).FirstOrDefault();
                    if (obj != null)
                    { profiles.Add(obj); }
                }
                profiles.ForEach(x => x.StateName = context.States.Where(y => y.StateID == x.FKStateID).Select(y => y.StateName).FirstOrDefault());
                profiles.ForEach(x => x.IsWebcam = context.Users.Where(y => y.UserID == x.FKUserID).Select(y => y.IsOnline).FirstOrDefault());
                return profiles;
            }
            else
            {
                return null;
            }

            return null;
        }


    }
}
