﻿using BLL.BusinessModels;
using BLL.BusinessModels.TeacherModels;
using BLL.Helpers;
using DAL.Entities;
using DAL.MasterEntity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;


namespace BLL.BusinessManagers
{
    public class TeachertestManager
    {
        MyDbContext context = new MyDbContext();
        public Test AddnewTest(Test model)
        {


            using (var ctx = new MyDbContext())
            {
                Test tst = ctx.Test.Add(model);
                ctx.SaveChanges();

                //model.TestID = bannerSlide.BannerSlidesID;
                model.TestID = tst.TestID;
                return model;
            }
        }

        

        public IEnumerable<SelectListItem> ListOfAnswers()
        {
            try
            {
                IEnumerable<SelectListItem> Answers = from Enumerations.Answer n in Enum.GetValues(typeof(Enumerations.Answer))
                                                      select new SelectListItem()
                                                      {
                                                          Text = n.ToString(),
                                                          Value = Convert.ToString((int)n)
                                                      };
                return Answers;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<QuestionsBank> GetQuestionList(int TestId)
        {
            List<QuestionsBank> lst = new List<QuestionsBank>();
            using (var ctx = new MyDbContext())
            {
                          lst=(from tq in ctx.TestQuestions
                                                   join q in ctx.QuestionsBank on tq.FKQuestionID equals q.QuestionBankID
                                                   where tq.FKTestID == TestId
                                                   select q).ToList();
            }
            return lst;
        }

        public Test UpdateTest(Test Test)
        {
            using (var ctx = new MyDbContext())
            {

                Test tst = ctx.Test.Where(x => x.TestID == Test.TestID).SingleOrDefault();
                tst.TestName = Test.TestName;
                tst.FKGradeID = Test.FKGradeID;
                tst.FKSubjectID = Test.FKSubjectID;
                tst.FKModifiedDate = Test.FKModifiedDate;
                tst.RemarksStudent = Test.RemarksStudent;
                tst.RemarksTeacher = Test.RemarksTeacher;

                tst.TotalScores = Test.TotalScores;
                tst.PassingMarks = Test.PassingMarks;
                tst.TestDuration = Test.TestDuration;
                tst.TotalQuestions = Test.TotalQuestions;
                tst.ISSkipable = Test.ISSkipable;
                tst.TestID = Test.TestID;

                tst.TestName = Test.TestName;


                ctx.SaveChanges();

                //model.TestID = bannerSlide.BannerSlidesID;
                Test.TestID = tst.TestID;
                return Test;
            }
        }

        public int DeleteTest(int Id)
        {
            using (var ctx = new MyDbContext())
            {

                Test tst = ctx.Test.Where(x => x.TestID == Id).SingleOrDefault();

                ctx.Test.Attach(tst);
                ctx.Test.Remove(tst);
                ctx.SaveChanges();

                return Id;
            }
        }
        public int DeleteQuestion(int QuestionId)
        {
            using (var ctx = new MyDbContext())
            {

                QuestionsBank Question = ctx.QuestionsBank.Where(x => x.QuestionBankID == QuestionId).SingleOrDefault();

                ctx.QuestionsBank.Attach(Question);
                ctx.QuestionsBank.Remove(Question);
                ctx.SaveChanges();

                return QuestionId;
            }
        }

        public int GetCurriculumId(int SubId)
        {
            return (int)context.Subjects.Where(x => x.SubjectID == SubId).Select(x => x.FKGradeID).FirstOrDefault();
        }

        public IEnumerable<SelectListItem> GetTeacherSubject(int TeacherId, int GradeId, int CurriculumID)
        {
            IEnumerable<SelectListItem> subjects = context.Subjects.Where(x => x.IsActive == true && x.FKCurriculumID == CurriculumID && x.FKGradeID == GradeId).ToList().Select(x => new SelectListItem()
            {
                Text = x.SubjectName,
                Value = x.SubjectID.ToString()
            });
            return subjects;
            //using (var ctx = new MyDbContext())
            //{
            //    IEnumerable<SelectListItem> subjects1 = (from s in ctx.Subjects
            //                                            join ts in ctx.TeacherSubjects on s.SubjectID equals ts.FKSubjectID
            //                                            where ts.FKUserID== TeacherId && ts.FKCurriculumID == CurriculumID
            //                                            && ts.FKGradeID==GradeId && s.FKCurriculumID==CurriculumID select s)
            //        .AsEnumerable().Select(s => new SelectListItem()
            //        {
            //            Text = s.SubjectName,
            //            Value = s.SubjectID.ToString()
            //        }).Distinct().ToList();
            //    return subjects;
            //}
        }


        public IEnumerable<SelectListItem> GetTeacherGradesold(int TeacherId, int CurriculumID)
        {
            // IEnumerable<SelectListItem> gradeList = new List<SelectListItem>();
            using (var ctx = new MyDbContext())
            {
                IEnumerable<SelectListItem> gradeList = (from g in ctx.Grades
                                                         join ts in ctx.TeacherSubjects on g.GradeID equals ts.FKGradeID
                                                         where ts.FKUserID == TeacherId && ts.FKCurriculumID == CurriculumID
                                                         && g.FKCurriculumID == CurriculumID
                                                         select g).AsEnumerable().Select(g => new SelectListItem()
                                                         {
                                                             Text = g.GradeName,
                                                             Value = g.GradeID.ToString()
                                                         }).ToList();
                return gradeList;
            }
        }


        public IEnumerable<SelectListItem> TestLevel()
        {
            try
            {
                IEnumerable<SelectListItem> TestLevel = from Enumerations.TestLevel n in Enum.GetValues(typeof(Enumerations.TestLevel))
                                                        select new SelectListItem()
                                                        {
                                                            Text = n.ToString(),
                                                            Value = Convert.ToString((int)n)
                                                        };
                return TestLevel;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public IEnumerable<SelectListItem> GetCurriculumsold(int TeacherId)
        {
            // IEnumerable<SelectListItem> gradeList = new List<SelectListItem>();
            using (var ctx = new MyDbContext())
            {
                IEnumerable<SelectListItem> Curriculums = (from c in ctx.Curriculums
                                                           join tc in ctx.TeacherCuriculums on c.CurriculumID equals tc.FKCurriculumID
                                                           where tc.FKUserID == TeacherId
                                                           select c).AsEnumerable().Select(c => new SelectListItem()
                                                           {
                                                               Text = c.CurriculumName,
                                                               Value = c.CurriculumID.ToString()
                                                           }).Distinct().ToList();
                return Curriculums;
            }
        }


        public IEnumerable<SelectListItem> QuestionSkipAble()
        {
            try
            {
                IEnumerable<SelectListItem> Skipable = from Enumerations.ISSkipAble n in Enum.GetValues(typeof(Enumerations.ISSkipAble))
                                                       select new SelectListItem()
                                                       {
                                                           Text = n.ToString(),
                                                           Value = Convert.ToString((int)n)
                                                       };
                return Skipable;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public IEnumerable<SelectListItem> GetCurriculums(int teacherId)
        {
            try
            {
                IEnumerable<SelectListItem> Curriculums = context.Curriculums.Where(x => x.IsActive == true).ToList().Select(x => new SelectListItem()
                {
                    Text = x.CurriculumName,
                    Value = x.CurriculumID.ToString()
                });
                return Curriculums;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public IEnumerable<SelectListItem> GetTeacherGrades(int TeacherId, int CurriculumID)
        {
            try
            {
                IEnumerable<SelectListItem> Grades = context.Grades.Where(x => x.IsActive == true && x.FKCurriculumID == CurriculumID).ToList().Select(x => new SelectListItem()
                {
                    Text = x.GradeName,
                    Value = x.GradeID.ToString()
                });
                return Grades;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        public List<TestListingModel> LoadTeachers(int TeacherId,string TestName)
        {
            if (TestName == "")
            {
                var tests = (from tst in context.Test
                             join g in context.Grades on tst.FKGradeID equals g.GradeID
                             join s in context.Subjects on tst.FKSubjectID equals s.SubjectID
                             join u in context.Users on tst.FKCreatedBy equals u.UserID
                             where tst.FKCreatedBy == TeacherId
                             select new TestListingModel
                             {
                                 TestName = tst.TestName,
                                 Assigned = "17",
                                 CreatedDate = tst.FKCreatedDate.ToString(),
                                 ModifiedDate = tst.FKModifiedDate.ToString(),
                                 SubjectName = s.SubjectName,
                                 GradeName = g.GradeName,
                                 TestID = (int)tst.TestID,
                                 TestLevel = (int)tst.TestLevel,

                             });
                return tests.ToList();
            }
            else
            {
                var tests = (from tst in context.Test
                             join g in context.Grades on tst.FKGradeID equals g.GradeID
                             join s in context.Subjects on tst.FKSubjectID equals s.SubjectID
                             join u in context.Users on tst.FKCreatedBy equals u.UserID
                             where tst.FKCreatedBy == TeacherId && tst.TestName.Contains(TestName)
                             select new TestListingModel
                             {
                                 TestName = tst.TestName,
                                 Assigned = "17",
                                 CreatedDate = tst.FKCreatedDate.ToString(),
                                 ModifiedDate = tst.FKModifiedDate.ToString(),
                                 SubjectName = s.SubjectName,
                                 GradeName = g.GradeName,
                                 TestID = (int)tst.TestID,
                                 TestLevel = (int)tst.TestLevel,

                             });
                return tests.ToList();
            }
            
        }

        public TestDetails GetTest(int TestId)
        {

            TestDetails tests = (from tst in context.Test
                                 join g in context.Grades on tst.FKGradeID equals g.GradeID
                                 join s in context.Subjects on tst.FKSubjectID equals s.SubjectID
                                 join u in context.Users on tst.FKCreatedBy equals u.UserID
                                 where tst.TestID == TestId
                                 select new TestDetails
                                 {
                                     TestName = tst.TestName,
                                     TotalMarks = tst.TotalScores.ToString(),
                                     TotalQuestions = tst.TotalQuestions.ToString(),
                                     PassingMarks = tst.PassingMarks.ToString(),
                                     RemarksStudent = tst.RemarksStudent,
                                     RemarksTeacher = tst.RemarksTeacher,
                                     SubjectName = s.SubjectName,
                                     GradeName = g.GradeName,
                                     TestID = (int)tst.TestID,
                                     TestDuration = tst.TestDuration.ToString(),
                                     Skipable = tst.ISSkipable.ToString(),
                                     TestLevel = tst.TestLevel.ToString()
                                 }).SingleOrDefault();
            return tests;
        }

        public Test EditTest(int Id)
        {
            return context.Test.Where(x => x.TestID == Id).FirstOrDefault();
        }

        public QuestionsBank GetQuestion(int QuestionId)
        {
            return context.QuestionsBank.Where(X => X.QuestionBankID == QuestionId).FirstOrDefault();
        }

        public int AddQuestion(QuestionsBank Question)
        {
            using (var ctx = new MyDbContext())
            {

                QuestionsBank ques=ctx.QuestionsBank.Add(Question);

               
                ctx.SaveChanges();

                return (int)ques.QuestionBankID;
            }
        }

        public int AsignTest(TestAssigned testAsign)
        {
            using (var ctx = new MyDbContext())
            {

                TestAssigned test = ctx.TestAssigned.Add(testAsign);


                ctx.SaveChanges();

                return (int)test.TestAssignedID;
            }
        }
        public int UpdateQuestionBank(QuestionsBank Question)
        {
            using (var ctx = new MyDbContext())
            {

                QuestionsBank ques = ctx.QuestionsBank.Where(x => x.QuestionBankID == Question.QuestionBankID).SingleOrDefault();
                ques.SortOrder = Question.SortOrder;
                ques.Question = Question.Question;
                ques.FirstOption = Question.FirstOption;
                ques.SecondOption = Question.SecondOption;
                ques.ThirdOption = Question.ThirdOption;
                ques.FourthOption = Question.FourthOption;
                ques.QuestionMarks = Question.QuestionMarks;
                ques.AnswerKey = Question.AnswerKey;


                ctx.SaveChanges();

                //model.TestID = bannerSlide.BannerSlidesID;
                return (int)ques.QuestionBankID;
            }
        }
        public int AddTestQuestion(TestQuestions TestQuestions)
        {
            using (var ctx = new MyDbContext())
            {

                TestQuestions ques = ctx.TestQuestions.Add(TestQuestions);
                ctx.SaveChanges();
                return (int)ques.TestQuestionsID;
            }
        }

        public List<TestResultModel> GetTestResults(int TestId)
        {
            List<TestResultModel> lst = new List<TestResultModel>();
            var testResults = (from s in context.tblStudentTestResult
                         join u in context.UsersProfile on s.StudentID equals u.FKUserID
                         where s.TestID == TestId 
                         select new TestResultModel
                         {
                             ResultId=s.ResultId,
                             StudentID=s.StudentID,
                             TestID=s.TestID,
                             TotalMarks=s.TotalMarks,
                             Result=s.Result,
                             TestTakenOn=s.TestTakenOn.ToString(),
                             TestAssignedOn=s.TestAssignedOn.ToString(),
                             DurationHours=s.DurationHours,
                             DurationMints=s.DurationMints,
                             DurationSeconds=s.DurationSeconds,
                             StudentName=u.FirstName+" " +u.LastName

                         });
            return testResults.ToList();
            
        }

        public List<TestResultModel> GetTestResultsByName(int TestId,string StuName)
        {
            List<TestResultModel> lst = new List<TestResultModel>();
            var testResults = (from s in context.tblStudentTestResult
                               join u in context.UsersProfile on s.StudentID equals u.FKUserID
                               where s.TestID == TestId
                               select new TestResultModel
                               {
                                   ResultId = s.ResultId,
                                   StudentID = s.StudentID,
                                   TestID = s.TestID,
                                   TotalMarks = s.TotalMarks,
                                   Result = s.Result,
                                   TestTakenOn = s.TestTakenOn.ToString(),
                                   TestAssignedOn = s.TestAssignedOn.ToString(),
                                   DurationHours = s.DurationHours,
                                   DurationMints = s.DurationMints,
                                   DurationSeconds = s.DurationSeconds,
                                   StudentName = u.FirstName + " " + u.LastName

                               });
            return testResults.Where(a=>a.StudentName.Contains(StuName)).ToList();

        }
    }
}
