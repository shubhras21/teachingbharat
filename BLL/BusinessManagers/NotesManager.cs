﻿using BLL.BusinessManagers.AdminManager;
using BLL.BusinessManagers.SessionsManager;
using BLL.BusinessModels.TeacherModels;
using BLL.Helpers;
using DAL.Entities;
using DAL.MasterEntity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BLL.BusinessManagers.UserManager;

namespace BLL.BusinessManagers
{
   public class NotesManager
    {
        MyDbContext context = new MyDbContext();
        CustomTimeZone czone = new CustomTimeZone();

        public Int64 AddFolder(AddFolderViewModel model)
        {
            try
            {
                Folders obj = new Folders();
                obj.FolderName = model.FolderName;
                obj.IsActive = true;
                obj.FKCreatedBy = model.FKCreatedBy;
                obj.CreatedDate = czone.DateTimeNow();

                context.Folders.Add(obj);
                context.SaveChanges();
                return obj.FolderID;
                

            }
            catch (Exception ex)
            {
                return 0;
                throw;
            }
        }

        public Folders GetFolder(Int64 FolderID)
        {
            try
            {
                Folders obj = new Folders();
                obj = context.Folders.Where(x => x.FolderID == FolderID && x.IsActive == true).FirstOrDefault();
                if (obj != null)
                    return obj;
                else
                    return null;


            }
            catch (Exception ex)
            {
                return null;
                throw;
            }
        }


        public List<Folders> ListOfFolder(Int64 UserID)
        {
            try
            {
                List<Folders> list = new List<Folders>();
                list = context.Folders.Where(x => x.FKCreatedBy == UserID && x.IsActive == true).ToList();
                if(list!=null && list.Count>0)
                {
                    list.ForEach(x =>
                    {
                        x.TotalFiles = context.Notes.Where(y => y.FKFolderID == x.FolderID && y.IsActive == true).Count();
                    });
                    return list;
                }
                else
                {
                    return null;
                }

            }
            catch (Exception ex)
            {
                return null;
                throw;
            }
        }


        public Int64 AddNotes(AddNotesViewModel model)
        {
            try
            {
                Notes obj = new Notes();
                obj.FileName = model.FileName;
                obj.FKCreatedBy = model.FKOfferedBy;
                obj.FKFolderID = model.FolderID;
                obj.FKSubjectID = model.FKSubjectID;
                obj.FKTopicID = model.FKTopicID;
                obj.NotesName = model.NotesTitle;
                obj.TopicDescription = model.NotesDescription;
                obj.TopicOptional = model.TopicOptional;
                obj.TotalDownloads = 0;
                obj.CreatedDate = czone.DateTimeNow();
                obj.IsActive = true;

                context.Notes.Add(obj);
                context.SaveChanges();

                return obj.NoteID ;
            }
            catch (Exception ex)
            {
                return 0;
                throw;
            }
        }


        public bool EditNotes(AddNotesViewModel model)
        {
            try
            {
                Notes obj = new Notes();

                obj = context.Notes.Where(x => x.NoteID == model.NoteID && x.IsActive == true).FirstOrDefault();

                if(obj!=null)
                {
                    context.Notes.Attach(obj);
                      
                      
                    obj.FileName =  (!string.IsNullOrEmpty(model.FileName))?model.FileName:obj.FileName;

                    obj.FKSubjectID = model.FKSubjectID;
                    obj.FKTopicID = model.FKTopicID;
                    obj.NotesName = model.NotesTitle;
                    obj.TopicDescription = model.NotesDescription;
                    obj.TopicOptional = model.TopicOptional;
                    obj.CreatedDate = czone.DateTimeNow();

                    context.SaveChanges();

                    return true;
                }
                else
                {
                    return false;
                }
        
            }
            catch (Exception ex)
            {
                return false;
                throw;
            }
        }



        public List<Notes> ListOfNotes(Int64 UserID, Int64 FolderID)
        {
            try
            {
                List<Notes> list = new List<Notes>();
                list = context.Notes.Where(x => x.FKCreatedBy == UserID && x.FKFolderID==FolderID && x.IsActive == true).ToList();
                if (list != null && list.Count > 0)
                {
                    CurriculumsManagers cManager = new CurriculumsManagers();
                    list.ForEach(x =>
                    {
                        var obj = cManager.GetSubjectDetail(x.FKSubjectID);
                        x.Subject = obj.GradeName + " / " + obj.SubjectName;
                        x.TopicName = Convert.ToString(context.Topics.Where(y => y.TopicID == x.FKTopicID && y.IsActive == true).Select(y => y.TopicName).FirstOrDefault());
                    });
                    return list;
                }
                else
                {
                    return null;
                }

            }
            catch (Exception ex)
            {
                return null;
                throw;
            }
        }

        public Notes GetNotes(Int64 id)
        {
            try
            {
                Notes obj = new Notes();
                obj = context.Notes.Where(x => x.NoteID==id && x.IsActive == true).FirstOrDefault();
                if (obj != null)
                {
                    CurriculumsManagers cManager = new CurriculumsManagers();

                        UserBLL uBLL = new UserBLL(); 
                        var objs = cManager.GetSubjectDetail(obj.FKSubjectID);
                        obj.Subject = objs.GradeName + " / " + objs.SubjectName;
                        obj.TopicName = Convert.ToString(context.Topics.Where(y => y.TopicID == obj.FKTopicID && y.IsActive == true).Select(y => y.TopicName).FirstOrDefault());
                        var user = uBLL.GetUserProfile(obj.FKCreatedBy);

                    if (user != null)
                        obj.TeacherName = user.FirstName + " " + user.LastName;
                    else
                        obj.TeacherName = "N/A";


                    return obj;
                }
                else
                {
                    return null;
                }

            }
            catch (Exception ex)
            {
                return null;
                throw;
            }
        }
        public Notes GetUserNotes(Int64 UserID, Int64 FolderID)
        {
            try
            {
                Notes obj = new Notes();
                obj = context.Notes.Where(x => x.FKCreatedBy == UserID && x.FKFolderID == FolderID && x.IsActive == true).FirstOrDefault();
                if (obj != null)
                {
                    CurriculumsManagers cManager = new CurriculumsManagers();

                    var objs = cManager.GetSubjectDetail(obj.FKSubjectID);
                    obj.Subject = objs.GradeName + " / " + objs.SubjectName;
                    obj.TopicName = Convert.ToString(context.Topics.Where(y => y.TopicID == obj.FKTopicID && y.IsActive == true).Select(y => y.TopicName).FirstOrDefault());

                    return obj;
                }
                else
                {
                    return null;
                }

            }
            catch (Exception ex)
            {
                return null;
                throw;
            }
        }
        public bool DeleteNotes(Int64 id)
        {
            try
            {

                var obj = context.Notes.Where(x => x.NoteID == id && x.IsActive == true).FirstOrDefault();
                if(obj!=null)
                {
                    context.Notes.Attach(obj);
                    obj.IsActive = false;
                    context.SaveChanges();
                    return true;
                }
                else
                {
                    return false;
                }

            }
            catch (Exception ex)
            {

                throw;
            }
        }


        public bool UpdateDownloadCount(Int64 NoteID)
        {
            try
            {

                var obj = context.Notes.Where(x => x.NoteID == NoteID && x.IsActive == true).FirstOrDefault();
                if (obj != null)
                {
                    context.Notes.Attach(obj);
                    obj.TotalDownloads = obj.TotalDownloads + 1;
                    context.SaveChanges();
                    return true;
                }
                else
                {
                    return false;
                }

            }
            catch (Exception ex)
            {

                throw;
            }
        }


        public List<Notes> StudentAssignedNotes(Int64 StudentID)
        {
            try
            {
                SessionManager manager = new SessionManager();
                List<UsersProfile> list = new List<UsersProfile>();
                list =  manager.MyTeacherGetNotes(Config.CurrentUser);  // manager.MyTeacher(Config.CurrentUser); // Aman Edited

                if (list!=null && list.Count>0)
                {
                    List<long> lInt = new List<long>();

                    foreach(var item in list)
                    {
                        Int64 i = new Int64();
                        i = item.FKUserID;
                        lInt.Add(i);

                    }

                    List<Notes> nList = new List<Notes>();
                    nList = context.Notes.Where(x => lInt.Contains(x.FKCreatedBy) && x.IsActive==true).ToList();
                    if(nList!=null && nList.Count>0)
                    {
                        nList.ForEach(x => { x = GetNotes(x.NoteID); });
                        return nList;
                    }
                    else
                    {
                        return null;
                    }


                }
                else
                {
                    return null;
                }


            }
            catch (Exception ex)
            {
                return null;
                throw;
            }
        }


    }
}
