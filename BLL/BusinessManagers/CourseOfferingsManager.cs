﻿using BLL.BusinessManagers.AdminManager;
using BLL.BusinessManagers.SessionsManager;
using BLL.BusinessModels.TeacherModels;
using BLL.Helpers;
using DAL.Entities;
using DAL.MasterEntity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL.BusinessManagers
{
   public class CourseOfferingsManager
    {

        MyDbContext context = new MyDbContext();

        CurriculumsManagers cManager = new CurriculumsManagers();

        public Int64 AddCourse(AddCourseViewModel model)
        {
            try
            {

                if(model!=null)
                {
                    CustomTimeZone czone = new CustomTimeZone();
                    var subjectDetail = cManager.GetSubject(model.FKSubjectID);
                    var courseHeader = new CoursesHeader
                    {

                        CourseTitle = model.CourseTitle,
                        CourseDescription = model.CourseDescription,
                        FKSubjectID = model.FKSubjectID,
                        CourseFee = model.CourseFee,
                        TotalSeats = model.TotalSeats,
                        TrialVideoURL = model.TrialVideoURL,
                        OfferedDate = czone.DateTimeNow(),
                        TotalHours = model.TotalHours,
                        TimeSlot = model.TimeSlot,
                        FKOfferedBY = model.FKOfferedBy,
                        StartDate=model.StartDate,
                        FKCurriculumID=subjectDetail.FKCurriculumID,
                        FKGradeID=subjectDetail.FKGradeID,
                        IsActive = true

                    };

                    context.CourseHeader.Add(courseHeader);
                    context.SaveChanges();
                    Int64 CourseHeaerID = courseHeader.CoursesHeaderID;


                    if (CourseHeaerID > 0)
                    {

                        #region[Add Days]

                        if(model.Days!=null && model.Days.Count>0)
                        {
                            foreach (var item in model.Days)
                            {

                                CourseDays cDay = new CourseDays();
                                cDay.DayID = item;
                                cDay.FKCourseID = CourseHeaerID;
                                cDay.IsActive = true;
                                context.CourseDay.Add(cDay);
                                context.SaveChanges();

                            }

                        }
                       
                        #endregion


                        #region [Add Course Schedule]

                        int slot = Convert.ToInt32(model.TimeSlot);
                        DateTime selectedTimeSlot = context.Timings.Where(x => x.TimeID == slot).Select(x => x.TimeSlot).FirstOrDefault();

                        int counter = 0;

                        CourseSchedule obj = new CourseSchedule();

                        DateTime a = model.StartDate;
                        TimeSpan b = selectedTimeSlot.TimeOfDay;

                        obj.FKCourseID = CourseHeaerID;
                        obj.CourseSessionDate = a.Add(b);
                        obj.SessionContent = null;
                        obj.IsActive = true;

                        obj.CurrentStatus = (int)Enumerations.SessionStatus.Pending;

                        context.CourseSchedule.Add(obj);
                        context.SaveChanges();

                        Int64 CourseScheduleID = obj.CourseScheduleID;
                        counter = counter + 1;
                        DateTime day = new DateTime();
                        day = model.StartDate.AddDays(1);
                        while (counter != model.TotalHours)
                        {

                            // Logic to add days
                           
                            day = day.AddDays(1);

                            if (model.Days.Contains((int)day.DayOfWeek))
                            {

                                CourseSchedule objCourse = new CourseSchedule();

                                DateTime date = day;
                                TimeSpan time = selectedTimeSlot.TimeOfDay;

                                objCourse.FKCourseID = CourseHeaerID;
                                objCourse.CourseSessionDate = date.Add(time);
                                objCourse.SessionContent = null;
                                objCourse.IsActive = true;

                                objCourse.CurrentStatus = (int)Enumerations.SessionStatus.Pending;

                                context.CourseSchedule.Add(objCourse);
                                context.SaveChanges();

                                Int64 ID = objCourse.CourseScheduleID;

                                if (ID > 0)
                                {
                                    counter = counter + 1;
                                }

                            }

                        }


                        #endregion
                    }

                    if (CourseHeaerID > 0)
                    {
                        #region [Course Link Generate]

                        CoursesLink cLink = new CoursesLink();
                        cLink.FKCourseID = CourseHeaerID;
                        cLink.IsActive = true;

                        string p = string.Empty;
                        Guid g = Guid.NewGuid();
                        p = Convert.ToBase64String(g.ToByteArray());
                        p = p.Replace("=", "");
                        p = p.Replace("+", "");

                        cLink.CourseLink = p;

                        context.CourseLink.Add(cLink);
                        context.SaveChanges();
                        Int64 CourseLinkID = cLink.CoursesLinkID;


                        if (CourseLinkID > 0)
                        {
                            return 1;
                        }
                        else
                        {
                            return 0;
                        }

                        #endregion
                    }
                }
                else
                {
                    return 0;
                }

                return 0;
            }
            catch (Exception)
            {

                throw;
            }
        }


        public List<CoursesHeader> ListByCurriculums(Int64 CurriculumID)
        {
            try
            {

                List<CoursesHeader> list = new List<CoursesHeader>();
                list=context.CourseHeader.Where(x=>x.FKCurriculumID==CurriculumID  && x.IsActive==true).ToList();
                if(list!=null && list.Count>0)
                {
                    list.ForEach(x => {

                        x.CourseParticipants = context.CourseParticipants.Where(y => y.FKCourseID == x.CoursesHeaderID).ToList();
                        x.TeacherName = context.UsersProfile.Where(y => y.FKUserID == x.FKOfferedBY).Select(y => new { Name = y.FirstName + " " + y.LastName }).FirstOrDefault().Name.ToString();
                        x.GradeName = context.Grades.Where(y => y.GradeID == x.FKGradeID).Select(y => y.GradeName).FirstOrDefault().ToString() + " - " + context.Subjects.Where(y=>y.SubjectID==x.FKSubjectID).Select(y=>y.SubjectName).FirstOrDefault();
                        x.TeacherPic= Convert.ToString(context.UsersProfile.Where(y => y.FKUserID == x.FKOfferedBY).Select(y => y.ProfilePic).FirstOrDefault());

                    });

                    return list;
                }
                else
                {
                    return null;
                }

            }
            catch (Exception)
            {
                throw;
            }
        }
        public List<CoursesHeader> ListByCurriculumsNoId()
        {
            try
            {

                List<CoursesHeader> list = new List<CoursesHeader>();
                list = context.CourseHeader.Where(x => x.IsActive == true).ToList();
                if (list != null && list.Count > 0)
                {
                    list.ForEach(x => {

                        x.CourseParticipants = context.CourseParticipants.Where(y => y.FKCourseID == x.CoursesHeaderID).ToList();
                        x.TeacherName = context.UsersProfile.Where(y => y.FKUserID == x.FKOfferedBY).Select(y => new { Name = y.FirstName + " " + y.LastName }).FirstOrDefault().Name.ToString();
                        x.GradeName = context.Grades.Where(y => y.GradeID == x.FKGradeID).Select(y => y.GradeName).FirstOrDefault().ToString() + " - " + context.Subjects.Where(y => y.SubjectID == x.FKSubjectID).Select(y => y.SubjectName).FirstOrDefault();
                        x.TeacherPic = Convert.ToString(context.UsersProfile.Where(y => y.FKUserID == x.FKOfferedBY).Select(y => y.ProfilePic).FirstOrDefault());

                    });

                    return list;
                }
                else
                {
                    return null;
                }

            }
            catch (Exception)
            {
                throw;
            }
        }
        public List<CoursesHeader> ListByGrades(Int64 GradeID)
        {
            try
            {

                List<CoursesHeader> list = new List<CoursesHeader>();
                list = context.CourseHeader.Where(x => x.FKCurriculumID == GradeID && x.IsActive == true).ToList();
                if (list != null && list.Count > 0)
                {
                    list.ForEach(x => {

                        x.CourseParticipants = context.CourseParticipants.Where(y => y.FKCourseID == x.CoursesHeaderID).ToList();
                        x.TeacherName = context.UsersProfile.Where(y => y.FKUserID == x.FKOfferedBY).Select(y => new { Name = y.FirstName + " " + y.LastName }).FirstOrDefault().Name.ToString();
                        x.GradeName = context.Grades.Where(y => y.GradeID == x.FKGradeID).Select(y => y.GradeName).FirstOrDefault().ToString() + " - " + context.Subjects.Where(y => y.SubjectID == x.FKSubjectID).Select(y => y.SubjectName).FirstOrDefault();
                        x.TeacherPic = Convert.ToString(context.UsersProfile.Where(y => y.FKUserID == x.FKOfferedBY).Select(y => y.ProfilePic).FirstOrDefault());

                    });

                    return list;
                }
                else
                {
                    return null;
                }

            }
            catch (Exception)
            {
                throw;
            }
        }
        public List<Subjects> ListBySubjects(Int64 CurriculumID)
        {
            try
            {

                List<Subjects> list = new List<Subjects>();
                list = context.Subjects.Where(x => x.FKCurriculumID == CurriculumID && x.IsActive == true).ToList();
                if (list != null && list.Count > 0)
                {
                    list.ForEach(x => {
                        x.GradeName = context.Grades.Where(y => y.GradeID == x.FKGradeID).Select(y => y.GradeName).FirstOrDefault().ToString() + " - " + context.Subjects.Where(y => y.SubjectID == x.SubjectID).Select(y => y.SubjectName).FirstOrDefault();
                    });

                    return list;
                }
                else
                {
                    return null;
                }

            }
            catch (Exception)
            {
                throw;
            }
        }
        public List<Subjects> ListByAllSubjects()
        {
            try
            {

                List<Subjects> list = new List<Subjects>();
                list = context.Subjects.Where(x => x.IsActive == true).ToList();
                if (list != null && list.Count > 0)
                {
                    list.ForEach(x => {
                        x.GradeName = context.Grades.Where(y => y.GradeID == x.FKGradeID).Select(y => y.GradeName).FirstOrDefault().ToString() + " - " + context.Subjects.Where(y => y.SubjectID == x.SubjectID).Select(y => y.SubjectName).FirstOrDefault();
                    });

                    return list;
                }
                else
                {
                    return null;
                }

            }
            catch (Exception)
            {
                throw;
            }
        }
        public List<CoursesHeader> ListByCurriculumsByIDandSubject(Int64 SubjectID)
        {
            try
            {

                List<CoursesHeader> list = new List<CoursesHeader>();
                list = context.CourseHeader.Where(x => x.FKSubjectID == SubjectID && x.IsActive == true).ToList();
                if (list != null && list.Count > 0)
                {
                    list.ForEach(x => {

                        x.CourseParticipants = context.CourseParticipants.Where(y => y.FKCourseID == x.CoursesHeaderID).ToList();
                        x.TeacherName = context.UsersProfile.Where(y => y.FKUserID == x.FKOfferedBY).Select(y => new { Name = y.FirstName + " " + y.LastName }).FirstOrDefault().Name.ToString();
                        x.GradeName = context.Grades.Where(y => y.GradeID == x.FKGradeID).Select(y => y.GradeName).FirstOrDefault().ToString() + " - " + context.Subjects.Where(y => y.SubjectID == x.FKSubjectID).Select(y => y.SubjectName).FirstOrDefault();
                        x.TeacherPic = Convert.ToString(context.UsersProfile.Where(y => y.FKUserID == x.FKOfferedBY).Select(y => y.ProfilePic).FirstOrDefault());

                    });

                    return list;
                }
                else
                {
                    return null;
                }

            }
            catch (Exception)
            {
                throw;
            }
        }
        public List<CoursesHeader> ListByCurriculumsAll()
        {
            try
            {

                List<CoursesHeader> list = new List<CoursesHeader>();
                list = context.CourseHeader.Where(x => x.IsActive == true).ToList();
                if (list != null && list.Count > 0)
                {
                    list.ForEach(x => {

                        x.CourseParticipants = context.CourseParticipants.Where(y => y.FKCourseID == x.CoursesHeaderID).ToList();
                        x.TeacherName = context.UsersProfile.Where(y => y.FKUserID == x.FKOfferedBY).Select(y => new { Name = y.FirstName + " " + y.LastName }).FirstOrDefault().Name.ToString();
                        x.GradeName = context.Grades.Where(y => y.GradeID == x.FKGradeID).Select(y => y.GradeName).FirstOrDefault().ToString() + " - " + context.Subjects.Where(y => y.SubjectID == x.FKSubjectID).Select(y => y.SubjectName).FirstOrDefault();
                        x.TeacherPic = Convert.ToString(context.UsersProfile.Where(y => y.FKUserID == x.FKOfferedBY).Select(y => y.ProfilePic).FirstOrDefault());

                    });

                    return list;
                }
                else
                {
                    return null;
                }

            }
            catch (Exception)
            {
                throw;
            }
        }
        public List<CoursesHeader> TeacherCourses(Int64 TeacherID)
        {
            try
            {

                List<CoursesHeader> list = new List<CoursesHeader>();
                list = context.CourseHeader.Where(x => x.FKOfferedBY==TeacherID && x.IsActive == true).ToList();
                if (list != null && list.Count > 0)
                {
                    list.ForEach(x => {

                        x.CourseParticipants = context.CourseParticipants.Where(y => y.FKCourseID == x.CoursesHeaderID).ToList();
                        x.TeacherName = context.UsersProfile.Where(y => y.FKUserID == x.FKOfferedBY).Select(y => new { Name = y.FirstName + " " + y.LastName }).FirstOrDefault().Name.ToString();
                        x.GradeName = context.Grades.Where(y => y.GradeID == x.FKGradeID).Select(y => y.GradeName).FirstOrDefault().ToString() + " - " + context.Subjects.Where(y => y.SubjectID == x.FKSubjectID).Select(y => y.SubjectName).FirstOrDefault();
                        x.TeacherPic = Convert.ToString(context.UsersProfile.Where(y => y.FKUserID == x.FKOfferedBY).Select(y => y.ProfilePic).FirstOrDefault());

                    });

                    return list;
                }
                else
                {
                    return null;
                }

            }
            catch (Exception)
            {
                throw;
            }
        }

        public List<CoursesHeader> ListBySubject(Int64 SubjectID)
        {
            try
            {
                List<CoursesHeader> list = new List<CoursesHeader>();
                list = context.CourseHeader.Where(x => x.FKSubjectID == SubjectID && x.IsActive == true).ToList();
                if (list != null && list.Count > 0)
                {
                    list.ForEach(x => {

                        x.CourseParticipants = context.CourseParticipants.Where(y => y.FKCourseID == x.CoursesHeaderID).ToList();
                        x.TeacherName = context.UsersProfile.Where(y => y.FKUserID == x.FKOfferedBY).Select(y => new { Name = y.FirstName + " " + y.LastName }).FirstOrDefault().Name.ToString();
                        x.GradeName = context.Grades.Where(y => y.GradeID == x.FKGradeID).Select(y => y.GradeName).FirstOrDefault().ToString() + " - " + context.Subjects.Where(y => y.SubjectID == x.FKSubjectID).Select(y => y.SubjectName).FirstOrDefault();
                        x.TeacherPic = context.UsersProfile.Where(y => y.FKUserID == x.FKOfferedBY).Select(y => y.ProfilePic).FirstOrDefault().ToString();

                    });

                    return list;
                }
                else
                {
                    return null;
                }

            }
            catch (Exception)
            {
                throw;
            }
        }


        public CoursesHeader GetCourseDetail(Int64 CourseID)
        {
            try
            {

                CoursesHeader obj = new CoursesHeader();
                obj = context.CourseHeader.Where(x => x.CoursesHeaderID == CourseID && x.IsActive == true).FirstOrDefault();
                if (obj != null )
                {
                                       
                        obj.CourseParticipants = context.CourseParticipants.Where(y => y.FKCourseID == obj.CoursesHeaderID).ToList();
                        obj.TeacherName = context.UsersProfile.Where(y => y.FKUserID == obj.FKOfferedBY).Select(y => new { Name = y.FirstName + " " + y.LastName }).FirstOrDefault().Name.ToString();
                        obj.GradeName = context.Grades.Where(y => y.GradeID == obj.FKGradeID).Select(y => y.GradeName).FirstOrDefault().ToString();
                        obj.TeacherPic = Convert.ToString(context.UsersProfile.Where(y => y.FKUserID == obj.FKOfferedBY).Select(y => y.ProfilePic).FirstOrDefault());
                        obj.IsOnline=context.Users.Where(y => y.UserID == obj.FKOfferedBY).Select(y => y.IsOnline).FirstOrDefault();

                        var university = context.TeacherQualifications.Where(x => x.FKUserID == obj.FKOfferedBY).FirstOrDefault();
                        
                        if(university!=null)
                        {
                        obj.UniversityName = university.UniversityName;
                        }
                        else
                        {
                        obj.UniversityName = "N/A";
                        }

                    RatingsManager rManager = new RatingsManager();
                    RatingsViewModel rating = new RatingsViewModel();
                    rating = rManager.ListOfRatings(obj.FKOfferedBY);

                    if (rating!=null)
                    {
                        obj.TeacherRatings = rating.RatingAverage;
                    }
                    else
                    {
                        obj.TeacherRatings = 0;
                    }

                    obj.Subject = cManager.GetSubjectDetail(obj.FKSubjectID);

                    obj.CourseSchedule = context.CourseSchedule.Where(x => x.FKCourseID == obj.CoursesHeaderID && x.IsActive==true).ToList();

                    obj.AboutCourse = context.AboutCourse.Where(x => x.FKCourseID== obj.CoursesHeaderID && x.IsActive==true).ToList();

                    obj.CourseFaqs = context.CourseFaqs.Where(x => x.FKCourseID == obj.CoursesHeaderID && x.IsActive == true).ToList();

                    obj.CourseCurriculum = context.CouurseCurriculum.Where(x => x.FKCourseID == obj.CoursesHeaderID && x.IsActive == true).ToList();

                    int timeID = Convert.ToInt32(obj.TimeSlot);
                    obj.DateTimeSlot = context.Timings.Where(x => x.TimeID == timeID).Select(x => x.TimeSlot).FirstOrDefault();

                    obj.CourseDays = context.CourseDay.Where(x => x.FKCourseID == obj.CoursesHeaderID && x.IsActive==true).ToList();

                    var courseLink = context.CourseLink.Where(x => x.FKCourseID == obj.CoursesHeaderID && x.IsActive == true).FirstOrDefault();

                    obj.CoursesLink = courseLink;

                    return obj;
                }
                else
                {
                    return null;
                }

            }
            catch (Exception)
            {
                throw;
            }
        }

        public CourseJoinDetails GetCourseJoinDetail(Int64 CourseID)
        {
            try
            {

                CourseJoinDetails obj = new CourseJoinDetails();

                obj.CourseHeader = GetCourseDetail(CourseID);

                SessionManager sManager = new SessionManager();
                obj.UpcomingCourses = sManager.JoinCourseSession(Config.CurrentUser, CourseID);

                obj.ClassesSheduled = ClassesAttendedByTeacher(obj.CourseHeader.FKOfferedBY, obj.CourseHeader.CoursesHeaderID);
                obj.ClassesRemained = obj.CourseHeader.TotalHours - obj.ClassesSheduled;
                obj.ClassesTaken= ClassesAttendedByTeacher(Config.CurrentUser, obj.CourseHeader.CoursesHeaderID);


                return obj;

            }
            catch (Exception)
            {
                throw;
            }
        }

        public CourseJoinDetails GetTeacherCourseJoinDetail(Int64 CourseID)
        {
            try
            {

                CourseJoinDetails obj = new CourseJoinDetails();

                obj.CourseHeader = GetCourseDetail(CourseID);

                SessionManager sManager = new SessionManager();
                obj.UpcomingCourses = sManager.TeacherJoinCourseSession(Config.CurrentUser, CourseID);

                obj.ClassesSheduled = ClassesAttendedByTeacher(obj.CourseHeader.FKOfferedBY, obj.CourseHeader.CoursesHeaderID);
                obj.ClassesRemained = obj.CourseHeader.TotalHours - obj.ClassesSheduled;
                obj.ClassesTaken = ClassesAttendedByTeacher(Config.CurrentUser, obj.CourseHeader.CoursesHeaderID);

              
                

                return obj;

            }
            catch (Exception)
            {
                throw;
            }
        }


        


        public int ClassesAttendedByTeacher(Int64 UserID, Int64 CourseHeaderID)
        {

            try
            {

                var list = context.CourseSessionsDuration.Where(x => x.FKJoinedBy == UserID && x.FKCourseID == CourseHeaderID).ToList();

                if(list!=null && list.Count>0)
                {
                   return list.Count();
                }
                else
                {
                    return 0;
                }
            }
            catch (Exception)
            {

                throw;
            }


            return 0;

        }


        public CourseClashTest AddCourseParticipant(Int64 CourseID, Int64 StudentID)
        {

            try
            {

                CustomTimeZone czone = new CustomTimeZone();

                // Check if student has no clash in his old and new courses

                // already registered courses schedule
                List<CourseSchedule> ExistedSchedule = new List<CourseSchedule>();

                List<Int64> StudentCourses = context.CourseParticipants.Where(x => x.FKStudentID == StudentID && x.IsActive == true).Select(x=>x.FKCourseID).ToList();
               
                if(StudentCourses!=null && StudentCourses.Count>0)
                {

                    // Schedule of already existed courses 
                  
                    DateTime cTime = czone.DateTimeNow();
             
                    ExistedSchedule = context.CourseSchedule.Where(x => x.IsActive == true && x.CourseSessionDate>cTime && !StudentCourses.Contains(x.FKCourseID)).ToList();

                    if(ExistedSchedule!=null && ExistedSchedule.Count>0)
                    {
                        
                        // new course scheduled
                        List<CourseSchedule> ListOfCourseSchedule1 = new List<CourseSchedule>();
                        ListOfCourseSchedule1 = context.CourseSchedule.Where(x => x.FKCourseID == CourseID && x.IsActive == true).ToList();

                        foreach(var item in ExistedSchedule)
                        {
                            if(ListOfCourseSchedule1.Any(x=>x.CourseSessionDate==item.CourseSessionDate && x.FKCourseID!=CourseID))
                            {
                                var obj = GetCourseDetail(item.FKCourseID);
                                CourseClashTest model = new CourseClashTest();
                                model.InsertStatus = -1;
                                model.CourseName = obj.CourseTitle;
                                return model;
                            }
                        } 

                    }

                }

                // new course scheduled
                List<CourseSchedule> ListOfCourseSchedule = new List<CourseSchedule>();
                ListOfCourseSchedule = context.CourseSchedule.Where(x => x.FKCourseID == CourseID && x.IsActive == true).ToList();

                var isExist = context.CourseParticipants.Where(x => x.FKCourseID == CourseID && x.FKStudentID == StudentID).FirstOrDefault();

                if (isExist==null)
                { 

                    CourseParticipants obj = new CourseParticipants();
                    obj.FKCourseID = CourseID;
                    obj.FKStudentID = StudentID;
                    obj.JoinDate = czone.DateTimeNow();
                    obj.IsActive = false;
                    context.CourseParticipants.Add(obj);
                    context.SaveChanges();

                    // Add Student Course Schedule
                    if (ListOfCourseSchedule != null && ListOfCourseSchedule.Count > 0)
                    {
                        foreach (var item in ListOfCourseSchedule)
                        {
                            StudentCourseSchedule csObj = new StudentCourseSchedule();
                            csObj.FKCourseScheduleID = item.CourseScheduleID;
                            csObj.FKStudentID = StudentID;
                            csObj.IsActive = false;
                            csObj.FKCourseParticipantID = obj.CourseParticipantID;
                            context.StudentCourseSchedule.Add(csObj);
                            context.SaveChanges();
                        }
                    }

                    CourseClashTest model = new CourseClashTest();
                    model.InsertStatus = obj.CourseParticipantID;
                    model.CourseName = null;
                    return model;
                }
                else
                {
                    CourseClashTest model = new CourseClashTest();
                    model.InsertStatus = 0;
                    model.CourseName = null;
                    return model;
                   
                }
                
            }
            catch (Exception)
            {

                throw;
            }

        }


        public List<CourseParticipants> StudentSubscriptions(Int64 StudentID)
        {
            try
            {
                List<CourseParticipants> list = new List<CourseParticipants>();
                list = context.CourseParticipants.Where(x => x.FKStudentID == StudentID && x.IsActive == true).ToList();

                if(list!=null && list.Count>0)
                {
                    list.ForEach(x => x.Course = GetCourseDetail(x.FKCourseID));
                    list.ForEach(x => {

                        var cScheduled = ClassesAttendedByTeacher(x.Course.FKOfferedBY, x.Course.CoursesHeaderID);
                        var cRemained = x.Course.TotalHours - cScheduled;
                        var cTaken = ClassesAttendedByTeacher(Config.CurrentUser, x.Course.CoursesHeaderID);
                        x.CompletionPercentage= ((cScheduled * 100) / x.Course.TotalHours);

                    });


                    return list;
                }
                else
                {
                    return null;
                }
                
            }
            catch (Exception)
            {

                throw;
            }

        }

        public List<CoursesHeader> TeacherSubscriptions(Int64 TeacherID)
        {
            try
            {
                List<CoursesHeader> list = new List<CoursesHeader>();
                list = context.CourseHeader.Where(x => x.FKOfferedBY == TeacherID && x.IsActive == true).ToList();

                if (list != null && list.Count > 0)
                {
                    list.ForEach(x => x = GetCourseDetail(x.CoursesHeaderID));
                    list.ForEach(x => {

                        var cScheduled = ClassesAttendedByTeacher(x.FKOfferedBY, x.CoursesHeaderID);
                        var cRemained = x.TotalHours - cScheduled;
                        var cTaken = ClassesAttendedByTeacher(Config.CurrentUser, x.CoursesHeaderID);
                        x.CompletionPercentage = ((cScheduled * 100) / x.TotalHours);

                    });
                    return list;
                }
                else
                {
                    return null;
                }

            }
            catch (Exception)
            {

                throw;
            }

        }


        public CourseSchedule GetCourseSchedule(Int64 CourseScheduleID)
        {
            try
            {
                CourseSchedule obj = new CourseSchedule();

                obj = context.CourseSchedule.Where(x => x.CourseScheduleID == CourseScheduleID && x.IsActive == true).FirstOrDefault();

                if (obj != null)
                    return obj;
                else
                    return null;

            }
            catch (Exception)
            {
                
                throw;
            }
        }

        public List<CourseSchedule> GetStudentCourseSchedule(Int64 StudentID)
        {
            try
            {
                List<CourseSchedule> CourseScheduleList = new List<CourseSchedule>();
                List<Int64> CourseParticipantList = new List<Int64>();
                CourseParticipantList = context.CourseParticipants.Where(x => x.FKStudentID == StudentID && x.IsActive == true).Select(x=>x.FKCourseID).ToList();

                if (CourseParticipantList!=null && CourseParticipantList.Count>0)
                {

                  //  CourseParticipantList.Any(p2 => p2.FKCourseID == x.FKCourseID)
                    CourseScheduleList = context.CourseSchedule.Where(x=> CourseParticipantList.Contains(x.FKCourseID)).ToList();
                    return CourseScheduleList;
                }
                else
                {
                    return null;
                } 


            }
            catch (Exception)
            {

                throw;
            }
        }


        public List<CourseSchedule> GetTeacherCourseSchedule(Int64 TeacherID)
        {
            try
            {
                List<CourseSchedule> CourseScheduleList = new List<CourseSchedule>();
                List<Int64> CourseParticipantList = new List<Int64>();
                CourseParticipantList = context.CourseHeader.Where(x => x.FKOfferedBY== TeacherID&& x.IsActive == true).Select(x => x.CoursesHeaderID).ToList();

                if (CourseParticipantList != null && CourseParticipantList.Count > 0)
                {
                    CourseScheduleList = context.CourseSchedule.Where(x => CourseParticipantList.Contains(x.FKCourseID)).ToList();
                    return CourseScheduleList;
                }
                else
                {
                    return null;
                }


            }
            catch (Exception)
            {

                throw;
            }
        }


        public Int64 AddCourseSessionDuration(CourseSessionsDuration obj, Int64 UserID)
        {
            try
            {
                bool isExist = context.CourseSessionsDuration.Any(x => x.FKStudentCourseScheduleID == obj.CourseSessionsDurationID && x.IsActive == true && x.FKJoinedBy==UserID);

                if (isExist == false)
                {
                    if (obj != null)
                    {
                        context.CourseSessionsDuration.Add(obj);
                        context.SaveChanges();
                        return obj.CourseSessionsDurationID;
                    }
                    else
                    {
                        return 0;
                    }
                }
                else
                {
                    return 1;
                }


            }
            catch (Exception)
            {

                throw;
            }
        }

        public string GetCourseLink(Int64 CourseID)
        {
            try
            {
                var obj = context.CourseLink.Where(x => x.FKCourseID == CourseID && x.IsActive == true).FirstOrDefault();
                if(obj!=null)
                {
                    return obj.CourseLink;
                }
                else
                {
                    return null;
                }
            }
            catch (Exception)
            {

                throw;
            }
        }



        public bool UpdateCourseSchedule(Int64 CourseScheduleID)
        {
            try
            {
                CustomTimeZone czone = new CustomTimeZone();
                CourseSchedule s = new CourseSchedule();
                bool check = false;
                DateTime currentTime = czone.DateTimeNow();
                s = context.CourseSchedule.Where(x => x.CourseScheduleID == CourseScheduleID).FirstOrDefault();

                if (s != null)
                {
                    context.CourseSchedule.Attach(s);
                    s.CurrentStatus = (int)Enumerations.SessionStatus.Completed;
                    context.SaveChanges();
                    check = true;
                }
                else
                {
                    check = false;
                }

                return check;
            }
            catch (Exception ex)
            {

            }
            return false;
        }



        public CoursesHeader GetCourseAmount(Int64 CourseId)
        {
            CoursesHeader objCourseheader=new CoursesHeader();
            try
            {
                objCourseheader=context.CourseHeader.FirstOrDefault(x => x.CoursesHeaderID == CourseId);
            }
            catch(Exception e)
            {

            }
            return objCourseheader;
        }
        public Users GetStudentDetails(Int64 UserId)
        {
            Users objStudent = new Users();
            try
            {
                objStudent = context.Users.FirstOrDefault(x => x.UserID == UserId);
            }
            catch (Exception e)
            {

            }
            return objStudent;
        }
        public UsersProfile GetUserProfileDetails(Int64 UserId)
        {
            UsersProfile objStudent = new UsersProfile();
            try
            {
                objStudent = context.UsersProfile.FirstOrDefault(x => x.FKUserID == UserId);
            }
            catch (Exception e)
            {

            }
            return objStudent;
        }
        //public bool AddCourseSchedule(List<int> Days,DateTime StartDate, int TotalHrs, Int64 CourseHeaderID, Int64 OfferedBy)
        //{
        //    try
        //    {

        //        CustomTimeZone czone = new CustomTimeZone();

        //        DateTime NewDate




        //    }
        //    catch (Exception)
        //    {

        //        throw;
        //    }
        //    return false;
        //}


        public void AddTransaction(string TransactionId, Int64 CourseParticipantId)
        {

            try
            {

                PaymentTransactions objpayment = new PaymentTransactions();
                objpayment.CourseParticipantID = CourseParticipantId;
                objpayment.TransactionId = TransactionId;
                objpayment.status = (int)Enumerations.TransactionStatus.pending;
                context.PaymentTransactions.Add(objpayment);
                context.SaveChanges();

            }
            catch (Exception)
            {

                throw;
            }

        }
        public void updateTransaction(string TransactionId, int status)
        {

            try
            {

                PaymentTransactions objpayment = new PaymentTransactions();
                objpayment=context.PaymentTransactions.FirstOrDefault(x=>x.TransactionId== TransactionId);
                objpayment.status = status;
                context.SaveChanges();
                if(status== (int)Enumerations.TransactionStatus.success)
                {
                    CourseParticipants objcourse = new CourseParticipants();
                    objcourse= context.CourseParticipants.FirstOrDefault(x => x.CourseParticipantID == objpayment.CourseParticipantID);
                    objcourse.IsActive = true;
                    context.SaveChanges();
                }



            }
            catch (Exception)
            {

                throw;
            }

        }
    }
}
