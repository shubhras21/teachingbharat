﻿using BLL.BusinessModels;
using BLL.BusinessModels.TeacherModels;
using BLL.Helpers;
using DAL.Entities;
using DAL.MasterEntity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace BLL.BusinessManagers
{
    public static class StaticMethods
    {
        static MyDbContext context = new MyDbContext();
        public static string AsignedTestCount(int TestId)
        {
            List<TestAssigned> lst = context.TestAssigned.Where(a => a.FKTestID == TestId).ToList();
            return lst.Count().ToString();
        }
    }
}
