﻿using BLL.BusinessModels.AdminModels;
using DAL.MasterEntity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL.BusinessManagers.StateManager
{
   public  class StateManager
    {
        public List<StateViewModel> GetAllStates()
        {
            var statesViewModelList = new List<StateViewModel>();
            MyDbContext context = new MyDbContext();
            var statesList = context.States.ToList();

            Mapper.MappStatesListToModel(statesList, ref statesViewModelList);

            return statesViewModelList;
        }

        public StateViewModel GetStateByStateID (long? stateID)
        {
            var stateModel = new StateViewModel();
            using (var ctx = new MyDbContext())
            {
              var state =  (from s in ctx.States where s.StateID == stateID select s).FirstOrDefault();
                if(state.StateID > 0)
                {
                    Mapper.MappStateToStateModel(state, ref stateModel);
                }
            }
            return stateModel;
        }
    }
}
