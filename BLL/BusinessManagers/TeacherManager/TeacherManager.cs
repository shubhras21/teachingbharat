﻿using BLL.BusinessModels.AdminModels;
using BLL.BusinessModels.TeacherModels;
using BLL.Helpers;
using DAL.Entities;
using DAL.MasterEntity;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL.BusinessManagers.TeacherManager
{
    public class TeacherManager
    {


        public MyDbContext context = new MyDbContext();
        CustomTimeZone customTimeZone = new CustomTimeZone();

        public  List<TeacherViewModel> ListofTeachers()
        {
            try
            {
                var model = new List<TeacherViewModel>();
                MyDbContext context = new MyDbContext();
                List<Users> users = context.Users.Where(x => x.UserType == (int)Enumerations.UserType.Teacher).ToList();

                if(users.Count > 0)
                {
                    users.ForEach(x => x.UsersProfile = context.UsersProfile.Where(i => i.FKUserID == x.UserID).FirstOrDefault());
                    model = users.Select(x => new TeacherViewModel
                    {
                        PersonId = x.UserID,
                        FirstName = x.UsersProfile != null ? x.UsersProfile.FirstName : null,
                        LastName = x.UsersProfile != null ? x.UsersProfile.LastName : null,
                        EmailAddress = x.LoginEmail,
                        ContactNumber = x.UsersProfile != null ? x.UsersProfile.ContactNumber : null,
                        IsActive = x.IsActive,
                        IsVerfied = x.IsVerified,
                        UserStatus = x.UserStatus

                    }).ToList();
                }

                return model;

            }
            catch (Exception ex)
            {
                throw ex;
            }


        }

        public List<TeacherCourseHeaderModel> GetTeacherCourses(long userID)
        {
            var courseHeadersList = new List<TeacherCourseHeaderModel>();
            using (var ctx = new MyDbContext())
            {
                var coursesList = (from CH in ctx.CourseHeader
                                   where CH.FKOfferedBY == userID
                                   select CH).ToList();

                if(coursesList.Count > 0 )
                {
                    Mapper.MappCourseHeaderListToModel(coursesList, ref courseHeadersList);
                }
            }
            return courseHeadersList;
        }


        #region [Block User]

        public int BlockUser(Int64 id)
        {
            try
            {
                var userdata = context.Users.FirstOrDefault(x => x.UserID == id);
                context.Users.Attach(userdata);
                if (userdata != null)
                {
                    userdata.UserStatus = (int)Enumerations.UserStatus.NotApproved;
                    userdata.IsVerified = (int)Enumerations.VerificationType.NotVerified;
                }
                context.SaveChanges();
                return (int)Enumerations.ResponsHelpers.Block;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion

        #region [Delete User]

        public int DeleteUser(long id)
        {
            try
            {
                var userdata = context.Users.FirstOrDefault(x => x.UserID == id);
                context.Users.Attach(userdata);
                if (userdata != null)
                {
                    userdata.IsActive = false;
                }
                context.SaveChanges();
                return (int)Enumerations.ResponsHelpers.Deleted;
            }
            catch (Exception e)
            {
                throw e;
            }
        }


        public UserViewModel GetTeacherByUserID(long? userID)
        {
            UserViewModel userViewModel = new UserViewModel();
            using (var context = new MyDbContext())
            {
                var user = (from u in context.Users
                            where u.UserID == userID
                            && u.IsActive == true
                            select u).FirstOrDefault();

                if(user != null)
                {
                    if (user.UserID > 0)
                    {
                        user.UsersProfile = (from up in context.UsersProfile where up.FKUserID == user.UserID select up).FirstOrDefault();

                        Mapper.MappUserToUserViewModel(user, ref userViewModel);
                    }
                }  
            }

            return userViewModel;

        }

        public TeacherQualificationModel GetTeacherQualificationByUserID(int userID)
        {
            var teacherQualificationModel = new TeacherQualificationModel();
            using (var ctx = new MyDbContext())
            {
                var teacherQualification = (from TQ in ctx.TeacherQualifications
                                            where TQ.FKUserID == userID
                                            select TQ).FirstOrDefault();

                if(teacherQualification != null)
                {
                    if (teacherQualification.TeacherQualificationID > 0)
                    {
                        Mapper.MappTeacherQualificationToModel(teacherQualification, ref teacherQualificationModel);
                    }
                }
            }

            return teacherQualificationModel;
        }

        public int DeleteTeacherByUserID(int userID)
        {
            try
            {
                using (var ctx = new MyDbContext())
                {
                    var userActivities = (from activities in ctx.UserActivity
                                          where activities.IsActive == true
                                          && activities.FKUserID == userID
                                          select activities).ToList();
                    if (userActivities.Count > 0)
                    {
                        foreach (var item in userActivities)
                        {
                            item.IsActive = false;
                            ctx.UserActivity.Attach(item);
                            ctx.Entry(item).State = EntityState.Modified;

                            ctx.SaveChanges();
                        }
                    }

                    var teacherAvailableHours = (from TAH in ctx.TeacherAvailableHours where TAH.FKUserID == userID select TAH).ToList();
                    if (teacherAvailableHours.Count > 0)
                    {
                        foreach (var item in teacherAvailableHours)
                        {
                            ctx.TeacherAvailableHours.Remove(item);
                            ctx.SaveChanges();
                        }
                    }

                   var teacherAvailability = (from TA in ctx.TeacherAvailability where TA.FKTeacherID == userID select TA).ToList();
                    if(teacherAvailability.Count > 0)
                    {
                        foreach(var item in teacherAvailability)
                        {
                            ctx.TeacherAvailability.Remove(item);
                            ctx.SaveChanges();
                        }
                    }

                   var teacherSessions = (from s in ctx.Sessions where s.FKRequestedTo == userID select s).ToList();
                    if(teacherSessions.Count > 0)
                    {
                        foreach(var item in teacherSessions)
                        {
                            ctx.Sessions.Remove(item);
                            ctx.SaveChanges();
                        }
                    }

                    var teacherQualification = (from TQ in ctx.TeacherQualifications where TQ.FKUserID == userID select TQ).ToList();
                    if (teacherQualification.Count > 0)
                    {
                        foreach (var item in teacherQualification)
                        {
                            ctx.TeacherQualifications.Remove(item);
                            ctx.SaveChanges();
                        }
                    }

                    var teacherSubjects = (from TS in ctx.TeacherSubjects where TS.FKUserID == userID select TS).ToList();
                    if (teacherSubjects.Count > 0)
                    {
                        foreach (var item in teacherSubjects)
                        {
                            ctx.TeacherSubjects.Remove(item);
                            ctx.SaveChanges();
                        }
                    }


                    var teacherCurriculums = (from TC in ctx.TeacherCuriculums where TC.FKUserID == userID select TC).ToList();
                    if (teacherCurriculums.Count > 0)
                    {
                        foreach (var item in teacherCurriculums)
                        {
                            ctx.TeacherCuriculums.Remove(item);
                            ctx.SaveChanges();
                        }
                    }

                    var courses = (from c in ctx.CourseHeader where c.FKOfferedBY == userID select c).ToList();
                    if(courses.Count > 0)
                    {
                        foreach(var item in courses)
                        {
                            ctx.CourseHeader.Remove(item);
                            ctx.SaveChanges();
                        }
                    }

                    var userprofile = (from UP in ctx.UsersProfile where UP.FKUserID == userID select UP).ToList();
                    if (userprofile.Count > 0)
                    {
                        foreach (var item in userprofile)
                        {
                            ctx.UsersProfile.Remove(item);
                            ctx.SaveChanges();
                        }
                    }

                    var user = (from U in ctx.Users where U.UserID == userID select U).FirstOrDefault();
                    if (user.UserID > 0)
                    {
                        ctx.Users.Remove(user);
                        ctx.SaveChanges();
                    }
                }

                return (int)Enumerations.ResponsHelpers.Deleted;
            }
            catch (Exception ex)
            {
                return (int)Enumerations.ResponsHelpers.Error;
            }
        }

        public UserViewModel GetTeacherCompleteActivities(UserViewModel model)
        {
            var teachersSubjectsListModel = new List<TeacherSubjectsViewModel>();
            var teacherAvailableHoursList = new List<TeacherAvailableHoursModel>();
            using (var ctx = new MyDbContext())
            {
               var teacherSubjects = (from TS in ctx.TeacherSubjects where TS.FKUserID == model.UserID select TS).ToList();
                if(teacherSubjects.Count > 0 )
                {
                    Mapper.MappTeacherSubjectsListToModel(teacherSubjects , ref teachersSubjectsListModel);
                    model.teacherSubjectsList = teachersSubjectsListModel;
                }

                var teacherAvailableHours = (from TAH in ctx.TeacherAvailableHours
                                             where TAH.FKUserID == model.UserID
                                             select TAH).ToList();
                if(teacherAvailableHours.Count > 0)
                {
                    Mapper.MappTeacherAvailableHoursListToModel(teacherAvailableHours, ref teacherAvailableHoursList);
                    model.teacherAvailableHours = teacherAvailableHoursList;
                }
            }
            return model;
        }

        public TimingModel GetTeacherTimingsByTimeID(long timeID)
        {
            var timeModel = new TimingModel();
            using (var ctx = new MyDbContext())
            {
                var timings = (from T in ctx.Timings where T.TimeID == timeID select T).FirstOrDefault();
                if(timings != null)
                {
                    if(timings.TimeID > 0)
                    {
                        Mapper.MappTimingToModel(timings, ref timeModel);
                    }
                }
            }
            return timeModel;
        }

        public DaysModel GetDayByDayID (long dayID)
        {
            var dayModel = new DaysModel();
            using (var ctx = new MyDbContext())
            {
              var day =  (from D in ctx.Days where D.DayID == dayID select D).FirstOrDefault();
                if(day != null)
                {
                    if(day.DayID > 0)
                    {
                        Mapper.MappDayToModel(day,ref dayModel);
                    }
                }
            }
            return dayModel;
        }


      public int GetTotalNoOfTeachersCount()
        {
            
            using (var ctx = new MyDbContext())
            {
                var totalTeachersCount  = (from u in ctx.Users
                                where u.UserType == (int)Enumerations.UserType.Student
                                select u).ToList().Count;

                return totalTeachersCount;
            }
        }

        public int GetTeachersRegisteredThisMonthCount()
        {
            using (var ctx = new MyDbContext())
            {
                var today = DateTime.Today;
                var TeachersCount = (from u in ctx.Users
                                     join up in ctx.UsersProfile
                                     on u.UserID equals up.FKUserID
                                     where up.CreateDate.Value.Month == today.Month
                                     && up.CreateDate.Value.Year == today.Year
                                     && u.UserType == (int)Enumerations.UserType.Teacher
                                     select u).ToList().Count;

                return TeachersCount;
            }
        } 

        public int GetTeachersRegisteredTodayCount()
        {
            using (var ctx = new MyDbContext())
            {
                DateTime startDateTime = DateTime.Today;
                DateTime endDateTime = DateTime.Today.AddDays(1).AddTicks(-1);

                var totalTeachersRegisteredTodayCount = (from u in ctx.Users
                                                         join up in ctx.UsersProfile
                                                         on u.UserID equals up.FKUserID
                                                         where up.CreateDate >= startDateTime
                                                         && up.CreateDate <= endDateTime
                                                          && u.UserType == (int)Enumerations.UserType.Teacher
                                                         select u).ToList().Count;

                return totalTeachersRegisteredTodayCount;
            }
        }

        public int UpdateTeacher(UserViewModel userViewModel)
        {
            try
            {
                var DbUser = new Users();
                Mapper.MappUserViewModelToUserEntity(userViewModel, ref DbUser);

                if (DbUser.UserID > 0)
                {
                    using (var context = new MyDbContext())
                    {
                        context.Users.Attach(DbUser);
                        context.Entry(DbUser).State = EntityState.Modified;
                        context.UsersProfile.Attach(DbUser.UsersProfile);
                        context.Entry(DbUser.UsersProfile).State = EntityState.Modified;
                        context.SaveChanges();

                        return (int)Enumerations.ResponsHelpers.Updated;
                    }
                }
                return (int)Enumerations.ResponsHelpers.Updated;
            }
            catch (Exception ex)
            {
                return (int)Enumerations.ResponsHelpers.Error;
            }

        }

      public List<UserViewModel>  GetUsersByUserType(int userType)
        {
            var usersModelList = new List<UserViewModel>(); 
            using (var ctx = new MyDbContext())
            {
                var users = (from u in ctx.Users where u.UserType == userType select u).ToList();

                if(users.Count > 0)
                {
                    users.ForEach(x => x.UsersProfile = context.UsersProfile.Where(i => i.FKUserID == x.UserID).FirstOrDefault());
                    Mapper.MappUsersListToModel(users,ref usersModelList);
                }
            }
            return usersModelList;
        }

        public List<long> GetTeachersByGradeID(int gradeID)
        {
            var teacherIDs = new List<long>();
            using (var ctx = new MyDbContext())
            {
                var sessionsList = (from session in ctx.Sessions where session.FKGradeID == gradeID select session).ToList();
                if(sessionsList.Count > 0 )
                {
                    foreach (var item in sessionsList)
                    {
                        if(!teacherIDs.Contains((long)item.FKRequestedTo))
                        {
                            teacherIDs.Add((long)item.FKRequestedTo);
                        }
                       
                    }
                }
            }
            return teacherIDs;
        }
        public List<long> GetTeachersBySubjectID(int subjectID)
        {
            var teacherIDs = new List<long>();
            using (var ctx = new MyDbContext())
            {
                var sessionsList = (from session in ctx.Sessions
                                    where session.FKSubjectID == subjectID select session).ToList();
                if (sessionsList.Count > 0)
                {
                    foreach (var item in sessionsList)
                    {
                        if (!teacherIDs.Contains((long)item.FKRequestedTo))
                        {
                            teacherIDs.Add((long)item.FKRequestedTo);
                        }
                    }
                }
            }
            return teacherIDs;
        }
        public List<long> GetTeachersByCity(string cityName)
        {
            var teacherIDs = new List<long>();
            using (var ctx = new MyDbContext())
            {
                var userprofiles = (from up in ctx.UsersProfile
                                    where up.CityName.Contains(cityName)
                                    select up).ToList();
                if (userprofiles.Count > 0)
                {
                    foreach (var item in userprofiles)
                    {
                        if(!teacherIDs.Contains((long)item.FKUserID ))
                        {
                            teacherIDs.Add((long)item.FKUserID);
                        }
                    }
                }

            }
            return teacherIDs;
        }
        public List<long> GetTeachersByStatus(int status)
        {
            var teacherIDs = new List<long>();
            using (var ctx = new MyDbContext())
            {
                var userprofiles = (from up in ctx.Users
                                    where up.UserStatus.Equals(status)
                                    select up).ToList();
                if (userprofiles.Count > 0)
                {
                    foreach (var item in userprofiles)
                    {
                        if (!teacherIDs.Contains((long)item.UserID))
                        {

                            teacherIDs.Add((long)item.UserID);
                        }
                    }
                }

            }
            return teacherIDs;
        }
        //public void ConvertTeacherEmailTemplateToEmailTemplateModel (TeacherEmailTemplateModel teacherEmailTemplateModel)
        //{
        //    EmailTemplatesModel emailTemplateModel = new EmailTemplatesModel();

        //    Mapper.MappTeacherEmailTemplateToEmailTemplateModel(teacherEmailTemplateModel, ref emailTemplateModel);
        //}


        #endregion

        #region [Approve Teacher]

        public int ActivateUser(long id)
    {
        try
        {
            var userdata = context.Users.FirstOrDefault(x => x.UserID == id);
            context.Users.Attach(userdata);
            if (userdata != null)
            {
                userdata.IsActive = true;
            }
            if (userdata != null && userdata.UserType == (int)Enumerations.UserType.Teacher)
            {
                userdata.UserStatus = (int)Enumerations.UserStatus.Approved;
                userdata.IsVerified = (int)Enumerations.VerificationType.VerifiedByAdmin;
                userdata.UsersProfile = context.UsersProfile.Where(x => x.FKUserID == userdata.UserID).FirstOrDefault();
                var username = userdata.UsersProfile.FirstName + " " + userdata.UsersProfile.LastName;

                // CODE COMMENTED TEMPORARILY
                //var result = EmailTemplate.EmailTemplate.UserRegisterEmail(username, userdata.LoginEmail,
                //    userdata.Password);
                //if (!result)
                //    return (int)Enumerations.ResponsHelpers.EmailSentFail;
            }
            context.SaveChanges();
            return (int)Enumerations.UserStatus.Approved;
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    #endregion


    #region [Set Teacher Availability]

    public int SetAvailability(TeachersAvailabilityViewModel model)
    {
        try
        {

            var userexsist = context.TeacherAvailability.Any(x => x.FKTeacherID == model.FKTeacherID && x.AvailableTime == model.AvailableTime && x.IsActive == true);
            if (userexsist)
            {
                return (int)Enumerations.ResponsHelpers.Alreadyexsists;
            }
            else
            {
                TeachersAvailability obj = new TeachersAvailability();
                DateTime time = model.AvailableTime ?? customTimeZone.DateTimeNow();
                obj.AvailableTime = time;
                obj.FKTeacherID = model.FKTeacherID;
                obj.IsActive = model.IsActive;
                context.TeacherAvailability.Add(obj);
                context.SaveChanges();
                var UserID = obj.TAID;
                if (UserID > 0)
                {
                    return (int)Enumerations.ResponsHelpers.Created;
                }
                else
                {
                    return (int)Enumerations.ResponsHelpers.Error;
                }
            }
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    public List<Sessions> ListOfTeacherSessions(Int64 UserID)
    {
        try
        {
            List<Sessions> list = new List<Sessions>();
            list = context.Sessions.Where(x => x.FKRequestedTo == UserID && x.RequestStatus == (int)Enumerations.RequestStatus.Accepted).ToList();
            return list;
        }
        catch (Exception ex)
        {
            throw ex;
        }
        return null;
    }


    public List<TeacherSetAvailabilityModel> ListOfLayout()
    {
        List<TeacherSetAvailabilityModel> list = new List<TeacherSetAvailabilityModel>();
        TeacherSetAvailabilityModel obj = new TeacherSetAvailabilityModel();
        List<DateTime> time = new List<DateTime>();
        time = listOfAllowDates();
        List<string> PushedDates = new List<string>();
        foreach (var item in time)
        {
            obj = new TeacherSetAvailabilityModel();
            obj.RealTime = new List<DateTime>();
            obj.LeftHeaderTime = new List<string>();
            string s = item.Date.ToString();

            var g = PushedDates.Contains(s);
            if (g == false)
            {
                PushedDates.Add(s);

                foreach (var lists in time)
                {
                    string k = lists.Date.ToString();
                    if (k.Equals(s))
                    {
                        obj.RealTime.Add(lists);
                        string timeSlot = null;
                        var from = lists.ToString("{0:hh:mm:tt}");
                        var to = lists.AddHours(1).ToString("{0:hh:mm:tt}");
                        timeSlot = from + " - " + to;
                        obj.LeftHeaderTime.Add(timeSlot);
                        timeSlot = "";
                    }
                }
                obj.TopHeaderDate = item.Day + " " + item.ToString("ddd");
                list.Add(obj);
            }
        }
        List<TeacherSetAvailabilityModel> dummy = list;
        return dummy;

    }

    public List<DateTime> listOfAllowDates()
    {
        var date = customTimeZone.DateTimeNow();
        List<string> lst = new List<string>();
        lst.Add("04:00AM");
        lst.Add("05:00AM");
        lst.Add("06:00AM");
        lst.Add("07:00AM");
        lst.Add("08:00AM");
        lst.Add("09:00AM");
        lst.Add("10:00AM");
        lst.Add("11:00AM");
        lst.Add("12:00PM");
        lst.Add("01:00PM");
        lst.Add("02:00PM");
        lst.Add("03:00PM");
        lst.Add("04:00PM");
        lst.Add("05:00PM");
        lst.Add("06:00PM");
        lst.Add("07:00PM");
        lst.Add("08:00PM");
        lst.Add("09:00PM");
        lst.Add("10:00PM");
        lst.Add("11:00PM");

        List<DateTime> remainingDateTimes = new List<DateTime>();
        for (int i = 0; i < 168; i++)
        {
            string strHour = date.Date.AddHours(i).ToString("hh:mmtt");
            if (lst.Contains(strHour))
            {
                remainingDateTimes.Add(date.Date.AddHours(i));
            }
        }
        //   List<DateTime> dummy = remainingDateTimes;
        return remainingDateTimes;
    }




    public List<TeacherSetAvailabilityModel> ListOfLayout1(int count, DateTime mydate)
    {
        List<TeacherSetAvailabilityModel> list = new List<TeacherSetAvailabilityModel>();
        TeacherSetAvailabilityModel obj = new TeacherSetAvailabilityModel();
        List<DateTime> time = new List<DateTime>();
        time = listOfAllowDates1(count, mydate);
        List<string> PushedDates = new List<string>();
        foreach (var item in time)
        {
            obj = new TeacherSetAvailabilityModel();
            obj.RealTime = new List<DateTime>();
            obj.LeftHeaderTime = new List<string>();
            string s = item.Date.ToString();

            var g = PushedDates.Contains(s);
            if (g == false)
            {
                PushedDates.Add(s);

                foreach (var lists in time)
                {
                    string k = lists.Date.ToString();
                    if (k.Equals(s))
                    {
                        obj.RealTime.Add(lists);
                        string timeSlot = null;
                        var from = lists.ToString("{0:hh:mm:tt}");
                        var to = lists.AddHours(1).ToString("{0:hh:mm:tt}");
                        timeSlot = from + " - " + to;
                        obj.LeftHeaderTime.Add(timeSlot);
                        timeSlot = "";
                    }
                }
                obj.TopHeaderDate = item.Day + " " + item.ToString("ddd");
                list.Add(obj);
            }
        }
        List<TeacherSetAvailabilityModel> dummy = list;
        return dummy;

    }

    public List<DateTime> listOfAllowDates1(int status, DateTime dated)
    {
        // var date = DateTime.Now;
        List<string> lst = new List<string>();
        lst.Add("04:00AM");
        lst.Add("05:00AM");
        lst.Add("06:00AM");
        lst.Add("07:00AM");
        lst.Add("08:00AM");
        lst.Add("09:00AM");
        lst.Add("10:00AM");
        lst.Add("11:00AM");
        lst.Add("12:00PM");
        lst.Add("01:00PM");
        lst.Add("02:00PM");
        lst.Add("03:00PM");
        lst.Add("04:00PM");
        lst.Add("05:00PM");
        lst.Add("06:00PM");
        lst.Add("07:00PM");
        lst.Add("08:00PM");
        lst.Add("09:00PM");
        lst.Add("10:00PM");
        lst.Add("11:00PM");

        List<DateTime> remainingDateTimes = new List<DateTime>();

        int p = 0;
        if (status == 2)
        {
            p = p + 1;
            for (int i = 0; i < 168; i++)
            {
                string strHour = dated.Date.AddHours(i).ToString("hh:mmtt");
                if (lst.Contains(strHour))
                {
                    remainingDateTimes.Add(dated.AddDays(1).Date.AddHours(i));
                }
            }
        }
        else if (status == 1)
        {

            for (int i = 168; i > 0; i--)
            {
                string strHour = dated.Date.AddHours(-i).ToString("hh:mmtt");
                if (lst.Contains(strHour))
                {
                    remainingDateTimes.Add(dated.Date.AddHours(-i));
                }
            }
        }

        return remainingDateTimes;
    }



    public bool CheckAvailability(Int64 UserID, DateTime time)
    {
        bool checks = false;
        MyDbContext context = new MyDbContext();
        checks = context.TeacherAvailability.Any(x => x.FKTeacherID == UserID && x.AvailableTime == time && x.IsActive == true) ? true : false;
        return checks;
    }

    public int UpdateTeacherAvailability(Int64 UserID, DateTime time)
    {
        MyDbContext context = new MyDbContext();
        int id;
        var obj = context.TeacherAvailability.Where(x => x.FKTeacherID == UserID && x.AvailableTime == time && x.IsActive == true).FirstOrDefault();
        if (obj != null)
        {
            context.TeacherAvailability.Attach(obj);
            obj.IsActive = false;
            context.Entry(obj).State = EntityState.Modified;
            id = context.SaveChanges();
            return id;
        }
        else
        {
            return 0;
        }

    }

    public List<string> TeacherAvailableTimes()
    {
        List<DateTime> list = new List<DateTime>();
        list = context.TeacherAvailability.Where(x => x.FKTeacherID == Config.CurrentUser && x.IsActive == true).Select(x => x.AvailableTime).ToList();
        List<string> nlist = new List<string>();
        foreach (var item in list)
        {
            string f = item.ToString();//.ToString("{0:yyyy-MM-dd hh:mm}");
            nlist.Add(f);
        }
        return nlist;
    }

}
    #endregion





}
