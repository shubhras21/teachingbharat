﻿using DAL.Entities;
using DAL.MasterEntity;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL.BusinessManagers
{
    public static class ActionsManager
    {

        public static List<Actions> GetAllActions()
        {
            using (var ctx = new MyDbContext())
            {
                var actionslist = (from action in ctx.Actions select action).ToList();
                if (actionslist.Count > 0)
                {
                    return actionslist;
                }

                return actionslist;
            }
        }

        public static bool CheckActionStatus(int id)
        {
            using (var ctx = new MyDbContext())
            {
                var actiondetail = (from action in ctx.Actions where action.ActionsID == id select action).FirstOrDefault();
                if (actiondetail != null)
                {
                    if (actiondetail.ActionsID > 0)
                    {
                        if (actiondetail.IsActive == true)
                        {
                            return true;
                        }
                        else
                        {
                            return false;
                        }
                    }
                    return false;
                }
                return false;
            }
        }

        public static void SetActionStatus(int[] actionIDs, bool status)
        {
            using (var ctx = new MyDbContext())
            {
               if(actionIDs != null)
                {
                    foreach (var itm in actionIDs)
                    {
                        var actiondata = GetActionByID(itm);
                        if (actiondata != null)
                        {
                            if (actiondata.ActionsID > 0)
                            {
                                actiondata.IsActive = status;
                                ctx.Actions.Attach(actiondata);
                                ctx.Entry(actiondata).State = EntityState.Modified;

                                ctx.SaveChanges();
                            }
                        }
                    }
                    var restIds = GetAllActionsExceptGivenIDs(actionIDs);
                    foreach (var item in restIds)
                    {
                        var actiondata = GetActionByID(item);
                        if (actiondata != null)
                        {
                            if (actiondata.ActionsID > 0)
                            {
                                actiondata.IsActive = false;
                                ctx.Actions.Attach(actiondata);
                                ctx.Entry(actiondata).State = EntityState.Modified;
                                ctx.SaveChanges();
                            }
                        }
                    }
                }
               else if(actionIDs == null)
                {
                    var allactionslist = GetAllActions();
                    foreach(var action in allactionslist)
                    {
                        action.IsActive = false;
                        ctx.Actions.Attach(action);
                        ctx.Entry(action).State = EntityState.Modified;
                        ctx.SaveChanges();
                    }
                }
                     
            }
        }

        public static List<int> GetAllActionsExceptGivenIDs(int[] actionIDs)
        {
            using (var ctx = new MyDbContext())
            {
                var allactionsids = (from action in ctx.Actions select action.ActionsID).ToList();
                var restIds = allactionsids.Where(n => !actionIDs.Contains(n)).ToList();

                return restIds;
            }
        }

        public static Actions GetActionByID(int actionID)
        {
            using (var ctx = new MyDbContext())
            {
                var actionDetail = (from action in ctx.Actions
                                    where action.ActionsID == actionID
                                    select action).FirstOrDefault();

                return actionDetail;
            }
        }
    }
}
