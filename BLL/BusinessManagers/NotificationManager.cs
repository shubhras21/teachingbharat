﻿using DAL.MasterEntity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DAL.Entities;
using BLL.BusinessModels.TeacherModels;
using BLL.Helpers;
using BLL.BusinessModels;
using BLL.BusinessModels.NotificationModel;

namespace BLL.BusinessManagers
{
   public class NotificationManager
    {
        MyDbContext context = new MyDbContext();
        CustomTimeZone czone = new CustomTimeZone();

        public Int64 AddNotification(Notifications model)
        {

            try
            {
                var obj = new UserNotifications {

                    IsActive = true,
                    ActionPerformed=model.ActionPerformed,
                    Time=czone.DateTimeNow(),
                    IsCourse=model.IsCourse,
                    FKSessionID=model.FKSessionID,
                    FKUserID=model.FKUserID
                };

                context.UserNotification.Add(obj);
                context.SaveChanges();

                return obj.NotificationID;
            }
            catch (Exception ex)
            {
                return 0;
                throw;
            }
        }
        public static List<NotificationTable> GetAllSlides()
        {
            var bannermodel = new List<NotificationModel>();

            using (var ctx = new MyDbContext())
            {
                var bannerslides = ctx.Notifications.Where(p => p.IsSeen != true).ToList();
                foreach (var item in bannerslides)
                {
                    long id = Convert.ToInt64(item.UserID);
                    var user = ctx.Users.Where(p => p.UserID == id).FirstOrDefault();
                    if (user.UserType == 1)
                    {
                        item.NotificationBy = "Supper";
                    }
                    else if (user.UserType == 2)
                    {
                        item.NotificationBy = "Admin";
                    }
                    else if (user.UserType == 3)
                    {
                        item.NotificationBy = "Teacher";
                    }
                    else if (user.UserType == 4)
                    {
                        item.NotificationBy = "Student";
                    }
                    else if (user.UserType == 5)
                    {
                        item.NotificationBy = "Student";
                    }
                }
                Mapper.MappBannerNotificationsListToModel(bannerslides, ref bannermodel);

                return bannerslides;
            }
        }

        public List<UserNotifications> ListofUserNotifications(Int64 UserID)
        {
            try
            {

                //if (Config.User.UserType == (int)Enumerations.UserType.Student)
                //{
                //    ConditionalReadStudent(UserID);
                //}
                //else
                //{
                //    ConditionalReadTeacher(UserID);
                //}

                List<UserNotifications> list = new List<UserNotifications>();
                list = context.UserNotification.Where(x => x.FKUserID == UserID && x.IsActive == true).ToList();
                if(list!=null && list.Count>0)
                {
                    list = list.OrderByDescending(x => x.Time).ToList();

                    List<UserNotifications> uList = new List<UserNotifications>();
                    foreach(var item in list)
                    {
                        UserNotifications obj = new UserNotifications();
                        if(!uList.Any(x=>x.ActionPerformed==item.ActionPerformed && x.Time.Day==item.Time.Day))
                        {
                            obj = item;
                            uList.Add(obj);
                        }
                    }


                    return uList;
                }
                else
                {
                    return null;
                }

            }
            catch (Exception ex)
            {
                return null;
                throw;
            }
        }


        public bool ConditionalReadStudent(Int64 FKStudentID)
        {
            try
            {

                List<UserNotifications> nList = new List<UserNotifications>();
                nList = ListofUserNotifications(FKStudentID);

                List<Sessions> sList = new List<Sessions>();
                sList = context.Sessions.Where(x => x.FKRequestedBy== FKStudentID && x.RequestStatus == (int)Enumerations.RequestStatus.Accepted).OrderByDescending(x=>x.RequestDate).ToList();

                if(sList!=null && sList.Count>0)
                {
                    
                    foreach(var item in sList)
                    {
                        if(sList.First()==item)
                        {
                            DateTime LED = item.RequestDate;
                            int diff = (LED.Subtract(DateTime.Now)).Days;

                            if (0 < diff && diff == 30)
                            {

                                // Add session request status into notifications 

                                Notifications nObj = new Notifications();
                                nObj.ActionPerformed = "student has not scheduled a session since 1 month";
                                nObj.FKSessionID = 0;
                                nObj.FKUserID = Convert.ToInt64(FKStudentID);
                                nObj.IsCourse = (int)Enumerations.OneOrMany.Session;

                                NotificationManager nManager = new NotificationManager();
                                Int64 ID = nManager.AddNotification(nObj);

                            }
                            else if (diff > 30 && diff <= 60)
                            {
                                // Add session request status into notifications 

                                Notifications nObj = new Notifications();
                                nObj.ActionPerformed = "student has not scheduled a session since 2 month";
                                nObj.FKSessionID = 0;
                                nObj.FKUserID = Convert.ToInt64(FKStudentID);
                                nObj.IsCourse = (int)Enumerations.OneOrMany.Session;

                                NotificationManager nManager = new NotificationManager();
                                Int64 ID = nManager.AddNotification(nObj);
                            }
                            else if (diff > 60 && diff >= 90)
                            {
                                // Add session request status into notifications 

                                Notifications nObj = new Notifications();
                                nObj.ActionPerformed = "student has not scheduled a session since 3 month";
                                nObj.FKSessionID = 0;
                                nObj.FKUserID = Convert.ToInt64(FKStudentID);
                                nObj.IsCourse = (int)Enumerations.OneOrMany.Session;

                                NotificationManager nManager = new NotificationManager();
                                Int64 ID = nManager.AddNotification(nObj);
                            }
                            else
                            {
                                DateTime tHappenDate = item.RequestDate;
                                int lHour = (tHappenDate.Subtract(DateTime.Now)).Hours;

                                if (lHour<24 && lHour>0)
                                {

                                    // Add session request status into notifications 

                                    Notifications nObj = new Notifications();
                                    nObj.ActionPerformed = lHour +" Hours left for your session.";
                                    nObj.FKSessionID = 0;
                                    nObj.FKUserID = Convert.ToInt64(FKStudentID);
                                    nObj.IsCourse = (int)Enumerations.OneOrMany.Session;

                                    NotificationManager nManager = new NotificationManager();
                                    Int64 ID = nManager.AddNotification(nObj);

                                }
                                

                            }

                        }
                        else
                        {
                            
                            DateTime tHappenDate = item.RequestDate;
                            int lHour = (tHappenDate.Subtract(DateTime.Now)).Hours;

                            if (lHour < 24 && lHour > 0)
                            {

                                // Add session request status into notifications 

                                Notifications nObj = new Notifications();
                                nObj.ActionPerformed = lHour + " Hours left for your session.";
                                nObj.FKSessionID = 0;
                                nObj.FKUserID = Convert.ToInt64(FKStudentID);
                                nObj.IsCourse = (int)Enumerations.OneOrMany.Session;

                                NotificationManager nManager = new NotificationManager();
                                Int64 ID = nManager.AddNotification(nObj);

                            }
                        }

                    }


                }
                else
                {
                    return false;
                }





                return true;
            }
            catch (Exception ex)
            {

                throw;
            }
        }

        public bool ConditionalReadTeacher(Int64 FKTeacherID)
        {
            try
            {

                List<UserNotifications> nList = new List<UserNotifications>();
                nList = ListofUserNotifications(FKTeacherID);


                List<Sessions> sList = new List<Sessions>();
                sList = context.Sessions.Where(x => x.FKRequestedTo == FKTeacherID && x.RequestStatus == (int)Enumerations.RequestStatus.Accepted).ToList();



                if (sList != null && sList.Count > 0)
                {

                    foreach (var item in sList)
                    {
                        if (sList.First() == item)
                        {
                            DateTime LED = item.RequestDate;
                            int diff = (LED.Subtract(DateTime.Now)).Days;

                            if (0 < diff && diff == 30)
                            {

                                // Add session request status into notifications 

                                Notifications nObj = new Notifications();
                                nObj.ActionPerformed = "student has not scheduled a session since 1 month";
                                nObj.FKSessionID = 0;
                                nObj.FKUserID = Convert.ToInt64(FKTeacherID);
                                nObj.IsCourse = (int)Enumerations.OneOrMany.Session;

                                NotificationManager nManager = new NotificationManager();
                                Int64 ID = nManager.AddNotification(nObj);

                            }
                            else if (diff > 30 && diff <= 60)
                            {
                                // Add session request status into notifications 

                                Notifications nObj = new Notifications();
                                nObj.ActionPerformed = "student has not scheduled a session since 2 month";
                                nObj.FKSessionID = 0;
                                nObj.FKUserID = Convert.ToInt64(FKTeacherID);
                                nObj.IsCourse = (int)Enumerations.OneOrMany.Session;

                                NotificationManager nManager = new NotificationManager();
                                Int64 ID = nManager.AddNotification(nObj);
                            }
                            else if (diff > 60 && diff >= 90)
                            {
                                // Add session request status into notifications 

                                Notifications nObj = new Notifications();
                                nObj.ActionPerformed = "student has not scheduled a session since 3 month";
                                nObj.FKSessionID = 0;
                                nObj.FKUserID = Convert.ToInt64(FKTeacherID);
                                nObj.IsCourse = (int)Enumerations.OneOrMany.Session;

                                NotificationManager nManager = new NotificationManager();
                                Int64 ID = nManager.AddNotification(nObj);
                            }
                            else
                            {
                                DateTime tHappenDate = item.RequestDate;
                                int lHour = (tHappenDate.Subtract(DateTime.Now)).Hours;

                                if (lHour < 24 && lHour > 0)
                                {

                                    // Add session request status into notifications 

                                    Notifications nObj = new Notifications();
                                    nObj.ActionPerformed = lHour + " Hours left for your session.";
                                    nObj.FKSessionID = 0;
                                    nObj.FKUserID = Convert.ToInt64(FKTeacherID);
                                    nObj.IsCourse = (int)Enumerations.OneOrMany.Session;

                                    NotificationManager nManager = new NotificationManager();
                                    Int64 ID = nManager.AddNotification(nObj);

                                }


                            }

                        }
                        else
                        {

                            DateTime tHappenDate = item.RequestDate;
                            int lHour = (tHappenDate.Subtract(DateTime.Now)).Hours;

                            if (lHour < 24 && lHour > 0)
                            {

                                // Add session request status into notifications 

                                Notifications nObj = new Notifications();
                                nObj.ActionPerformed = lHour + " Hours left for your session.";
                                nObj.FKSessionID = 0;
                                nObj.FKUserID = Convert.ToInt64(FKTeacherID);
                                nObj.IsCourse = (int)Enumerations.OneOrMany.Session;

                                NotificationManager nManager = new NotificationManager();
                                Int64 ID = nManager.AddNotification(nObj);

                            }
                        }

                    }


                }
                else
                {
                    return false;
                }





                return true;

            }
            catch (Exception ex)
            {

                throw;
            }
        }

    }
}
