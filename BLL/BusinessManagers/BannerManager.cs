﻿using BLL.BusinessModels;
using DAL.Entities;
using DAL.MasterEntity;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL.BusinessManagers
{
  public static  class BannerManager
    {

        public static List<BannerSlides> GetAllSlides ()
        {
            var bannermodel = new List<BannerModel>();

            using (var ctx = new MyDbContext())
            {
                var bannerslides = (from BS in ctx.BannerSlides select BS).ToList();

                Mapper.MappBannerSlidesListToModel(bannerslides, ref bannermodel);

                return bannerslides;
            }
        }

        public static BannerModel AddnewSlide(BannerModel model)
        {
            var bannerSlide = new BannerSlides();

            using (var ctx = new MyDbContext())
            {

                Mapper.MappBannerModelToDB(model, ref bannerSlide);
                ctx.BannerSlides.Add(bannerSlide);
                ctx.SaveChanges();

                model.ID = bannerSlide.BannerSlidesID;

                return model;
            }
        }
        
        public static BannerModel GetBannerSlideByID(int slideID)
        {
            var bannerslideModel = new BannerModel();

            using (var ctx = new MyDbContext())
            {
                var bannerslide = (from BS in ctx.BannerSlides
                                   where BS.BannerSlidesID == slideID select BS).FirstOrDefault();

                if (bannerslide != null)
                {
                    Mapper.MappBannerSlideToModel(bannerslide, ref bannerslideModel);

                    return bannerslideModel;
                }

                return bannerslideModel;
            }
        }
        
        public static void UpdateBannerSlide(BannerModel model)
        {
            var bannerslide = new BannerSlides();

            using (var ctx = new MyDbContext())
            {
                if(model != null)
                {
                    Mapper.MappBannerModelUpdateToDB(model, ref bannerslide);

                    ctx.BannerSlides.Attach(bannerslide);
                    ctx.Entry(bannerslide).State = EntityState.Modified;

                    ctx.SaveChanges();
                }

                
            }
        }

        public static int DeleteBannerSlide(int slideID)
        {
            using (var ctx = new MyDbContext())
            {
                var bannerslide = (from BS in ctx.BannerSlides
                                   where BS.BannerSlidesID == slideID
                                   select BS).FirstOrDefault();

                if(bannerslide != null)
                {
                    ctx.BannerSlides.Remove(bannerslide);
                    ctx.SaveChanges();

                    return (int)Helpers.Enumerations.ResponsHelpers.Deleted;
                }

                return 0;
            }
        }
    }
}
