﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL.BusinessManagers
{
    public class TestResultModel
    {
        public Int64 ResultId { get; set; }

        public Int64 StudentID { get; set; }

        public string StudentName { get; set; }

        public Int64 TestID { get; set; }

        public Int64 TotalMarks { get; set; }

        public Int64 Result { get; set; }

        //public Int64 Duration { get; set; }

        public Int64 DurationHours { get; set; }

        public Int64 DurationMints { get; set; }

        public Int64 DurationSeconds { get; set; }

        public string TestTakenOn { get; set; }

        public string TestAssignedOn { get; set; }
    }

    public class ResultDetail
    {
        public List<TestResultModel> lstTestResultModel { get; set; }
        public int PassCount { get; set; }
        public int FailCount { get; set; }
    }
}
