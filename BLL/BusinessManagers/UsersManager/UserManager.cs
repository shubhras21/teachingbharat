﻿using System;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using DAL.MasterEntity;
using BLL.Helpers;
using DAL.Entities;
using BLL.BusinessModels.UserModels;
using BLL.BusinessModels.TeacherModels;
using System.Web.Mvc;
using System.Collections.Generic;
using System.Globalization;
using System.Data.Entity.SqlServer;
using System.Data.Entity.Core.Objects;
using BLL.BusinessManagers.SessionsManager;
using BLL.BusinessModels.CalenderModels;
using BLL.BusinessModels.AdminModels;
using System.ComponentModel.DataAnnotations;

namespace BLL.BusinessManagers.UserManager
{

    // Manage all login and signup user methods 


    public static class UserManager
    {

        public static MyDbContext context = new MyDbContext();
        public static UsersActivityManager activityMgr = null;
        public static UserActivities obj_activity = null;


        #region [Student and Parent Signup]

        public static Int64 UserSignup(StudentRegisterViewModel model)
        {
            try
            {
                var userexsist = context.Users.Any(x => x.LoginEmail == model.LoginEmail);
                if (userexsist)
                {
                    return 0;
                }
                var id = Registeruser(model);
                return id;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static Int64 UserExternalLogin(ExternalLoginViewModel model)
        {
            try
            {
                var userexsist = context.Users.Any(x => x.LoginEmail == model.EmailAddress && x.IsActive && x.UserType == model.UserType && x.ExternalLoginID.Equals(model.TokenId));
                long result = 0;
                if (userexsist)
                {
                    result = context.Users.Where(x => x.LoginEmail == model.EmailAddress && x.ExternalLoginID == model.TokenId).Select(x => x.UserID).FirstOrDefault();
                    return result;
                }
                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        //GetUser Information for external login
        public static Users getUserDetails(Int64 id)
        {
            try
            {
                var personmodel = context.Users.FirstOrDefault(x => x.UserID == id);
                return personmodel;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static bool CheckEmailInUsersTable(string email)
        {
            bool EmailExists = false;
            using (var ctx = new MyDbContext())
            {
                var userEmail = (from u in ctx.Users where u.LoginEmail == email select u).FirstOrDefault();
                if (userEmail != null)
                {
                    if (userEmail.UserID > 0)
                    {
                        EmailExists = true;
                    }
                }
            }
            return EmailExists;
        }

        public static Int64 Registeruser(StudentRegisterViewModel model)
        {
            CustomTimeZone customTimeZone = new CustomTimeZone();
            try
            {
                #region [If external Login ID exists or not]
                if (model.ExternalLoginId != null)
                {
                    var externalUser = context.Users.Where(x => x.IsActive == true && x.ExternalLoginID == model.ExternalLoginId).FirstOrDefault();

                    if (externalUser != null)
                    {
                        // it means user already exist with that token id
                        return 0;
                    }
                }


                #endregion

                #region [Users]

                var user = new Users
                {
                    LoginEmail = model.LoginEmail,
                    Password = model.Password,
                    UserType = model.UserType,
                    UserStatus = (int)Enumerations.UserStatus.Pending,
                    IsActive = true,
                    IsVerified =1,
                    IsOnline = false,
                    ExternalLoginID = model.ExternalLoginId
                };
                if (user.ExternalLoginID != null)
                    user.IsVerified = 1;// (int)Enumerations.VerificationType.EmailVerfied;
                context.Users.Add(user);
                context.SaveChanges();
                var UserID = user.UserID;
                #endregion


                Int64? stID = null;
                if (model.StateID != 0)
                    stID = model.StateID;
                else
                    stID = null;


                if (model.UserType == (int)Enumerations.UserType.Student)
                {
                    #region [STUDENT]



                    var UserProfile = new UsersProfile
                    {
                        FirstName = model.FirstName,
                        LastName = model.LastName,
                        ContactNumber = model.ContactNumber,
                        AlternateNumber = null,
                        ProfilePic = null,
                        AboutMe = null,
                        DateOfBirth = null,
                        CreateDate = customTimeZone.DateTimeNow(),
                        UpdatedDate = customTimeZone.DateTimeNow(),
                        WeekHours = 0,
                        WeekendHours = 0,
                        IsPen = (int)Enumerations.AccessoryStatus.No,
                        IsWebcam = false,
                        IsInternetSpeed = (int)Enumerations.AccessoryStatus.No,
                        CityName = model.CityName,
                        ExperienceName = null,
                        FKStateID = stID,
                        FKOccupationID = null,
                        HeardAboutRudra = null,
                        FKUserID = UserID

                    };

                    context.UsersProfile.Add(UserProfile);
                    context.SaveChanges();
                    #endregion
                }
                else if (model.UserType == (int)Enumerations.UserType.Parent)
                {
                    #region [PARENT]

                    var UserProfile = new UsersProfile
                    {
                        FirstName = model.FirstName,
                        LastName = model.LastName,
                        ContactNumber = model.ContactNumber,
                        AlternateNumber = null,
                        ProfilePic = null,
                        AboutMe = null,
                        DateOfBirth = null,
                        CreateDate = customTimeZone.DateTimeNow(),
                        UpdatedDate = customTimeZone.DateTimeNow(),
                        WeekHours = 0,
                        WeekendHours = 0,
                        IsPen = (int)Enumerations.AccessoryStatus.No,
                        IsWebcam = false,
                        IsInternetSpeed = (int)Enumerations.AccessoryStatus.No,
                        CityName = model.CityName,
                        ExperienceName = null,
                        FKStateID = stID,
                        FKOccupationID = null,
                        HeardAboutRudra = null,
                        FKUserID = UserID

                    };

                    context.UsersProfile.Add(UserProfile);
                    context.SaveChanges();

                    #endregion
                }
                return UserID;

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        public static StudentRegisterViewModel StudentRegisterhelpers()
        {
            MyDbContext context = new MyDbContext();
            try
            {
                var model = new StudentRegisterViewModel
                {
                    sListofStates = context.States.ToList().Select(x => new SelectListItem()
                    {
                        Text = x.StateName,
                        Value = x.StateID.ToString()
                    })


                };
                return model;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        public static StudentRegisterViewModel StudentRegisterhelpersR()
        {
            MyDbContext context = new MyDbContext();
            try
            {
                var model = new StudentRegisterViewModel();
                model = null;
                model.sListofStates = context.States.ToList().Select(x => new SelectListItem()
                {
                    Text = x.StateName,
                    Value = x.StateID.ToString()
                });
                return model;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion


        #region [Teacher Registration]


        #endregion

        #region [Sending Email]

        public static int SendVerificationEmail(string loginEmail, string emailbody)
        {
            try
            {
                var emailtemplate = EmailTemplate.EmailTemplate.StudentRegistrationEmail(loginEmail, emailbody);
                if (emailtemplate)
                    return (int)Enumerations.ResponsHelpers.Created;
                return (int)Enumerations.ResponsHelpers.EmailSentFail;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion

        #region [Email Verification]

        public static bool VerifiyEmail(Int64? id)
        {
            try
            {
                var user = context.Users.FirstOrDefault(x => x.UserID == id);

                if (user != null)
                {
                    context.Users.Attach(user);
                    user.IsVerified = (int)Enumerations.VerificationType.EmailVerfied;
                    //  user.UserStatus = (int)Enumerations.UserStatus
                }
                else
                {
                    return false;
                }
                context.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion


        public static void DeleteFromUserBlockReason(int userID)
        {
            using (var ctx = new MyDbContext())
            {
                var userblockReasonObj = (from UBR in ctx.UserBlockReasons where UBR.FKUserID == userID select UBR).FirstOrDefault();
                if (userblockReasonObj != null)
                {
                    if (userblockReasonObj.UserBlockReasonID > 0)
                    {
                        ctx.UserBlockReasons.Remove(userblockReasonObj);
                        ctx.SaveChanges();
                    }
                }
            }

        }

        public static void ConvertUserViewModelToTeacherViewModel(UserViewModel userModel, ref TeacherViewModel teacherModel)
        {
            teacherModel = new TeacherViewModel
            {
                ContactNumber = userModel.UsersProfileViewModel != null ? userModel.UsersProfileViewModel.ContactNumber : null,
                EmailAddress = userModel.LoginEmail,
                FirstName = userModel.UsersProfileViewModel != null ? userModel.UsersProfileViewModel.FirstName : null,
                IsActive = userModel.IsActive,
                IsOnline = userModel.IsOnline,
                IsVerfied = userModel.IsVerified,
                LastName = userModel.UsersProfileViewModel != null ? userModel.UsersProfileViewModel.LastName : null,
                PersonId = userModel.UserID,
                TeacherId = userModel.UserID,
                UserStatus = userModel.UserStatus

            };
        }

        public static TeacherRegisterationViewModel TeacherRegisterhelpers()
        {
            MyDbContext context = new MyDbContext();
            try
            {
                var model = new TeacherRegisterationViewModel
                {

                    sListofCurriculums = context.Curriculums.ToList().Select(x => new SelectListItem()
                    {
                        Text = x.CurriculumName,
                        Value = x.CurriculumID.ToString()
                    }),
                    //sListofSubjects = context.Subjects.ToList().Select(x => new SelectListItem()
                    //{
                    //    Text = x.SubjectName,
                    //    Value = x.SubjectID.ToString()
                    //}),
                    //sListofPSubjects = Enumerable.Empty<SelectListItem>(),
                    //sListofSSubjects = Enumerable.Empty<SelectListItem>(),

                    //sListofCountries = context.Countries.ToList().Select(x => new SelectListItem()
                    //{
                    //    Text = x.CountryName,
                    //    Value = x.CountryID.ToString()
                    //}),
                    sListofStates = context.States.ToList().Select(x => new SelectListItem()
                    {
                        Text = x.StateName,
                        Value = x.StateID.ToString()
                    }),
                    sListofCities = Enumerable.Empty<SelectListItem>(),
                    sListofGrade = context.Grades.ToList().Select(x => new SelectListItem()
                    {
                        Text = x.GradeName,
                        Value = x.GradeID.ToString()
                    }),
                    sListofOccupation = context.Occupations.ToList().Select(x => new SelectListItem()
                    {
                        Text = x.OccupationName,
                        Value = x.OccupationID.ToString()
                    }),


                    //sListofUniversities = context.Universities.ToList().Select(x => new SelectListItem()
                    //{
                    //    Text = x.UniversityName,
                    //    Value = x.UnivesityID.ToString()
                    //}),

                    //sListofExperiences = context.Experience.ToList().Select(x => new SelectListItem()
                    //{
                    //    Text = x.ExperienceName,
                    //    Value = x.ExperienceID.ToString()
                    //}),

                    //sListofQualification = context.Qualifications.ToList().Select(x => new SelectListItem()
                    //{
                    //    Text = x.QualificationName,
                    //    Value = x.QualificationID.ToString()
                    //}),
                    ListofGrade = context.Grades.Select(g => new GradeModel
                    {
                        GradeId = g.GradeID,
                        GradeName = g.GradeName

                    }).ToList(),
                    ListofavailableTimes = context.Timings.Select(g => new TimingModel
                    {
                        TimingId = g.TimeID,
                        FromTime = g.FromTime,
                        ToTime = g.ToTime

                    }).ToList(),
                    sListofSubjects = context.Subjects.ToList().Select(x => new SelectListItem()
                    {
                        Text = x.SubjectName,
                        Value = x.SubjectID.ToString()
                    }),
                    sListofDigitalPen = new List<SelectListItem>
                   {

                    new SelectListItem {Text = "Yes",Value = Convert.ToString((int)Enumerations.AccessoryStatus.Yes) },
                    new SelectListItem {Text = "No",Value = Convert.ToString((int)Enumerations.AccessoryStatus.No) },

                   },
                    sListofWebcam = new List<SelectListItem>
                   {

                    new SelectListItem {Text = "Yes",Value = Convert.ToString((int)Enumerations.AccessoryStatus.Yes) ,Selected=false },
                    new SelectListItem {Text = "No",Value = Convert.ToString((int)Enumerations.AccessoryStatus.No) ,Selected=false },

                   },
                    sListofInternetConnection = new List<SelectListItem>
                   {

                    new SelectListItem {Text = "Yes",Value = Convert.ToString((int)Enumerations.AccessoryStatus.Yes) , Selected=false },
                    new SelectListItem {Text = "No",Value = Convert.ToString((int)Enumerations.AccessoryStatus.No) ,Selected=false  },

                   },
                    sListofavailableTimes = new List<SelectListItem>
                   {
                    new SelectListItem{Text = "1", Value = "1"},
                    new SelectListItem{Text = "2", Value = "2"},
                    new SelectListItem{Text = "3", Value = "3"},
                    new SelectListItem{Text = "4", Value = "4"},
                    new SelectListItem{Text = "5", Value = "5"},
                    new SelectListItem{Text = "6", Value = "6"},
                    new SelectListItem{Text = "7", Value = "7"},
                    new SelectListItem{Text = "8", Value = "8"},


                   }

                };
                return model;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #region [Teacher Registration]

        public static TeacherRegisterationViewModel TeacherRegisteration(TeacherRegisterationViewModel model)
        {
            try
            {
                MyDbContext context = new MyDbContext();
                var userexsist = context.Users.Any(x => x.LoginEmail == model.LoginEmail);
                if (userexsist)
                {
                    model.Response = (int)Enumerations.ResponsHelpers.Alreadyexsists;
                    return model;
                    //  return (int)Enumerations.ResponsHelpers.Alreadyexsists;
                }

                #region [User]

                var password = System.Web.Security.Membership.GeneratePassword(8, 3);    // Convert.ToString(Guid.NewGuid());

                var user = new Users
                {
                    IsActive = true,
                    IsVerified = (int)Enumerations.VerificationType.NotVerified,
                    UserType = (int)Enumerations.UserType.Teacher,
                    LoginEmail = model.LoginEmail,
                    Password = password,
                    ExternalLoginID = null,
                    IsOnline = false,
                    UserStatus = (int)Enumerations.UserStatus.Pending

                };
                context.Users.Add(user);
                context.SaveChanges();
                model.Password = password;
                model.TeacherID = user.UserID;
                var userid = user.UserID;

                #endregion

                #region [Teacher]
                CustomTimeZone customTimeZone = new CustomTimeZone();
                var teachermodel = new UsersProfile
                {
                    FKUserID = userid,
                    CreateDate = customTimeZone.DateTimeNow(),
                    DateOfBirth = model.TeacherDob,
                    FirstName = model.FirstName,
                    LastName = model.LastName,
                    IsPen = model.DigitalPen,
                    IsInternetSpeed = model.InternetSpeed,
                    FKOccupationID = model.OccupationID,
                    CityName = model.CityName,
                    FKStateID = model.StateID,
                    ExperienceName = model.ExperienceName,
                    ContactNumber = model.ContactNumber,
                    AlternateNumber = model.AlternateNumber,
                    UpdatedDate = customTimeZone.DateTimeNow(),
                    WeekHours = model.AvailableHours,
                    IsWebcam = false,
                    HeardAboutRudra = model.HeardAboutRudra

                };
                //if (model.FileBase != null)
                //{
                //    foreach (var file in model.FileBase)
                //    {
                //        // string fileName = Path.GetFileName(file.FileName);
                //        var fileName = Convert.ToString(Guid.NewGuid());

                //        if (fileName != null)
                //        {
                //            var path = Path.Combine(System.Web.HttpContext.Current.Server.MapPath("~/TeacherProfileImages"), fileName);
                //           file.SaveAs(path);
                //            teachermodel.ProfilePic = fileName;
                //        }
                //    }
                //}

                #region [TeacherDocument]



                if (model.FileBase != null)
                {
                    foreach (var file in model.FileBase)
                    {
                        string fileName = Path.GetFileName(file.FileName);
                        if (fileName != null)
                        {
                            byte[] bytes = null;
                            using (var binaryReader = new BinaryReader(file.InputStream))
                            {
                                bytes = binaryReader.ReadBytes(file.ContentLength);
                            }
                            teachermodel.Resume = bytes;
                        }
                    }
                }

                #endregion

                context.UsersProfile.Add(teachermodel);
                context.SaveChanges();
                var teacherid = teachermodel.UsersProfileID;

                #endregion

                #region [Teacher Target (Curriculum)]

                var teacherCurriculum = new TeacherCurriculums
                {
                    FKUserID = userid,
                    FKCurriculumID = model.CurriculumId,
                    IsActive = true
                };
                context.TeacherCuriculums.Add(teacherCurriculum);
                context.SaveChanges();

                #endregion

                //#region [Grade]

                //foreach (var items in model.Grade)
                //{
                //    #region [TeacherSubject]

                //  var teachersubjects = new TeacherSubjects
                //            {
                //                FKUserID = teacherid,
                //                FKSubjectID = model.PrimarySubjectID,
                //                FKCurriculumID=model.CurriculumId,
                //                FKGradeID=items,
                //                IsActive=true,
                //                Type=(int)Enumerations.SubjectType.Primary
                //            };
                //            context.TeacherSubjects.Add(teachersubjects);
                //            context.SaveChanges();

                //            var teachersubjectss = new TeacherSubjects
                //            {
                //                FKUserID = teacherid,
                //                FKSubjectID = model.SecondarySubjectID,
                //                FKCurriculumID = model.CurriculumId,
                //                FKGradeID = items,
                //                IsActive = true,
                //                Type = (int)Enumerations.SubjectType.Secondary
                //            };
                //            context.TeacherSubjects.Add(teachersubjectss);
                //            context.SaveChanges();


                //    #endregion

                //    var teachergrade = new TeacherGrades
                //    {
                //        FKUserID = teacherid,
                //        FKGradeID = items,
                //        IsActive=true
                //    };
                //    context.TeacherGrades.Add(teachergrade);
                //    context.SaveChanges();
                //}

                //#endregion

                //#region [TeacherDocument]

                //var teacherdocument = new TeacherDocument();

                //if (model.FileBase != null)
                //{
                //    foreach (var file in model.FileBase)
                //    {
                //        string fileName = Path.GetFileName(file.FileName);
                //        if (fileName != null)
                //        {
                //            byte[] bytes = null;
                //            using (var binaryReader = new BinaryReader(file.InputStream))
                //            {
                //                bytes = binaryReader.ReadBytes(file.ContentLength);
                //            }

                //            teacherdocument.FkTeacherId = teacherid;
                //            teacherdocument.Document = bytes;
                //            teacherdocument.DocumentName = model.ResumeTitle;
                //            Db.TeacherDocument.Add(teacherdocument);
                //            Db.SaveChanges();
                //        }
                //    }
                //}

                //#endregion


                #region [TeacherSubject]

                var teachersubjects = new TeacherSubjects
                {
                    FKUserID = userid,
                    FKSubjectID = model.PrimarySubjectID,
                    FKCurriculumID = model.CurriculumId,
                    FKGradeID = model.GradeID,
                    IsActive = true,
                    Type = (int)Enumerations.SubjectType.Primary
                };
                context.TeacherSubjects.Add(teachersubjects);
                context.SaveChanges();

                var teachersubjectss = new TeacherSubjects
                {
                    FKUserID = userid,
                    FKSubjectID = model.SecondarySubjectID,
                    FKCurriculumID = model.CurriculumId,
                    FKGradeID = model.GradeID,
                    IsActive = true,
                    Type = (int)Enumerations.SubjectType.Secondary
                };
                context.TeacherSubjects.Add(teachersubjectss);
                context.SaveChanges();

                #endregion

                var teacherQualification = new TeacherQualification
                {
                    FKUserID = userid,
                    UniversityName = model.UniversityName,
                    QualificationName = model.QualificationName,
                    StartDate = customTimeZone.DateTimeNow(),
                    EndDate = customTimeZone.DateTimeNow(),
                    IsActive = true

                };
                context.TeacherQualifications.Add(teacherQualification);
                context.SaveChanges();

                #region [TeacherTiming]


                foreach (var item in model.Time)
                {
                    var teachertiming = new TeacherAvailableHours
                    {
                        FKUserID = userid,
                        FKTimeID = item,
                        IsActive = true,
                        FKDayID = 1
                    };
                    context.TeacherAvailableHours.Add(teachertiming);                   
                }
                context.SaveChanges();
                #endregion

                model.Response = (int)Enumerations.ResponsHelpers.Created;
                return model;
            }
            catch (Exception ex)
            {
                model.Response = (int)Enumerations.ResponsHelpers.Error;
            }
            return model;
        }

        public static string GetUserNameByID(long userID)
        {
            string username = null;
            using (var ctx = new MyDbContext())
            {
                var user = (from u in ctx.Users where u.UserID == userID select u).FirstOrDefault();
                if (user != null)
                {
                    if (user.UserID > 0)
                    {
                        var userprofile = (from u in ctx.Users
                                           join up in ctx.UsersProfile
                                           on u.UserID equals up.FKUserID
                                           where up.FKUserID == userID
                                           select up).FirstOrDefault();

                        if (userprofile != null)
                        {
                            if (userprofile.UsersProfileID > 0)
                            {
                                username = userprofile.FirstName + " " + userprofile.LastName;
                            }
                        }
                    }
                }
            }
            return username;
        }

        public static List<UserViewModel> GetAllUsers()
        {
            var usersModelList = new List<UserViewModel>();

            using (var ctx = new MyDbContext())
            {
                var allUsersList = (from user in ctx.Users
                                    where
                                 user.UserStatus == (int)Enumerations.UserStatus.Approved
                              || user.UserStatus == (int)Enumerations.UserStatus.IsActive
                              || user.UserStatus == (int)Enumerations.UserStatus.IsOnline
                                    select user).ToList();
                if (allUsersList.Count > 0)
                {
                    foreach (var item in allUsersList)
                    {
                        item.UsersProfile = (from up in ctx.UsersProfile
                                             where up.FKUserID == item.UserID
                                             select up).FirstOrDefault();
                    }
                    Mapper.MappUsersListToModel(allUsersList, ref usersModelList);
                }

                return usersModelList;


            }
        }


        #endregion

        #region [Login Check]
        public static Users CanLogin(string emailaddress, string password, string externalid)
        {
            MyDbContext context = new MyDbContext();
            try
            {
                var userlogin = context.Users.Where(x => x.LoginEmail.ToLower().Equals(emailaddress) && x.Password.ToLower().Equals(password) && x.IsActive == true).FirstOrDefault();
                if (userlogin != null)
                {
                    UserBLL uBLL = new UserBLL();
                    obj_activity = new UserActivities();
                    activityMgr = new UsersActivityManager();

                    bool status = uBLL.UpdateOnlineStatus(userlogin.UserID, true);

                    obj_activity.FKUserID = userlogin.UserID;
                    obj_activity.ActionPerformed = "Login request from ";

                    if (userlogin.UserType == (int)Enumerations.UserType.Admin || userlogin.UserType == (int)Enumerations.UserType.Super)
                    {
                        obj_activity.ActivitySource = (int)Enumerations.ActivitySource.BacksideActivity;
                    }
                    else if (userlogin.UserType == (int)Enumerations.UserType.Student || userlogin.UserType == (int)Enumerations.UserType.Teacher)
                    {
                        obj_activity.ActivitySource = (int)Enumerations.ActivitySource.FrontsideActivity;
                    }

                    Int64 ID = activityMgr.AddActivity(obj_activity);

                    return userlogin;
                }
                return null;

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static List<UserViewModel> GetUsersOfSpecificType(int? type)
        {
            var usersModelList = new List<UserViewModel>();

            try
            {
                using (var ctx = new MyDbContext())
                {
                    var users = (from user in ctx.Users
                                 where user.UserType == type
                                    && (user.UserStatus == (int)Enumerations.UserStatus.IsActive
                                      || user.UserStatus == (int)Enumerations.UserStatus.Approved
                                      || user.UserStatus == (int)Enumerations.UserStatus.IsOnline)
                                 select user).ToList();

                    if (users.Count > 0)
                    {
                        foreach (var item in users)
                        {
                            item.UsersProfile = (from up in ctx.UsersProfile
                                                 where up.FKUserID == item.UserID
                                                 select up).FirstOrDefault();
                        }
                        Mapper.MappUsersListToModel(users, ref usersModelList);

                    }
                }

                return usersModelList;
            }
            catch (Exception ex)
            {
                throw ex;
            }
           // return usersModelList;
        }

        public static void SendEmailToUsers(List<UserViewModel> usersModelList, EmailTemplatesModel modelData)
        {

            if(usersModelList.Count > 0)
            {
                foreach (var item in usersModelList)
                {
                    EmailTemplate.EmailTemplate.SendEmail(item.LoginEmail, modelData.Body, modelData.Subject);
                }

            }

         
        }

        public static bool IsValidEmail (string address)
        {
            EmailAddressAttribute e = new EmailAddressAttribute();
            if (e.IsValid(address))
                return true;
            else
                return false;
        }






















        #endregion

        #region [Login Check]
        public static Users CanExternalLogin(string emailaddress, string password, string externalid)
        {
            MyDbContext context = new MyDbContext();
            try
            {
                var userlogin = context.Users.Where(x => x.LoginEmail.ToLower().Equals(emailaddress) && x.Password.ToLower().Equals(password) && x.ExternalLoginID.Equals(externalid) && x.IsActive == true).FirstOrDefault();
                if (userlogin != null)
                {
                    UserBLL uBLL = new UserBLL();
                    bool status = uBLL.UpdateOnlineStatus(userlogin.UserID, true);
                    return userlogin;
                }
                return null;

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region [Add Subject ]
        public static AddSubjectViewModel SubjectRegisterhelpers()
        {
            MyDbContext context = new MyDbContext();
            try
            {
                var model = new AddSubjectViewModel
                {

                    sListofCurriculums = context.Curriculums.ToList().Select(x => new SelectListItem()
                    {
                        Text = x.CurriculumName,
                        Value = x.CurriculumID.ToString()
                    }),
                    sListofSubjects = Enumerable.Empty<SelectListItem>()
                };
                return model;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion
    }

    public class UserBLL
    {
        MyDbContext context = new MyDbContext();


        public Users GetUserType(Int64 UserID)
        {
            Users obj = new Users();
            obj = context.Users.Where(x => x.UserID == UserID).FirstOrDefault();
            return obj;
        }

        public UsersProfile GetUserProfile(Int64 UserID)
        {
            var user = context.Users.Where(x => x.UserID == UserID).FirstOrDefault();
            UsersProfile obj = new UsersProfile();
            if (user.UserType != (int)Enumerations.UserType.Super && user.UserType != (int)Enumerations.UserType.Admin)
            {

                if (user.ExternalLoginID != null)
                { obj = context.UsersProfile.Where(x => x.FKUserID == UserID).FirstOrDefault(); }
                else
                {
                    obj = context.UsersProfile.Where(x => x.FKUserID == UserID).FirstOrDefault();
                    obj.StateName = obj.StateName != null ? context.States.Where(x => x.StateID == obj.FKStateID && x.IsActive == true).Select(x => x.StateName).FirstOrDefault().ToString() : null;
                }
            }
            return obj;
        }
        public TeacherQualification GetUserQualification(Int64 UserID)
        {
            TeacherQualification obj = new TeacherQualification();
            SessionManager manager = new SessionManager();
            obj = context.TeacherQualifications.Where(x => x.FKUserID == UserID).FirstOrDefault();
            if (obj != null)
                return obj;
            else
                return null;
        }

        public List<TeacherSubjects> ListOfTeacherSubjects(Int64 UserID)
        {
            List<TeacherSubjects> ListOfTeacherSubjects = new List<TeacherSubjects>();
            ListOfTeacherSubjects = context.TeacherSubjects.Where(x => x.FKUserID == UserID && x.IsActive == true).ToList();
            ListOfTeacherSubjects.ForEach(x =>
            {
                x.CurriculumName = context.Curriculums.Where(y => y.CurriculumID == x.FKCurriculumID && y.IsActive == true).Select(y => y.CurriculumName).FirstOrDefault();
                x.GradeName = context.Grades.Where(y => y.GradeID == x.FKGradeID && y.IsActive == true).Select(y => y.GradeName).FirstOrDefault();
                x.SubjectName = context.Subjects.Where(y => y.SubjectID == x.FKSubjectID && y.IsActive == true).Select(y => y.SubjectName).FirstOrDefault();
                x.SubjectType = Enum.GetName(typeof(Enumerations.SubjectType), x.Type);
            });
            return ListOfTeacherSubjects;
        }

        public List<TeachersAvailability> ListOfTeacherAvailability(Int64 UserID)
        {
            List<TeachersAvailability> ListOfTeacherAvailability = new List<TeachersAvailability>();
            ListOfTeacherAvailability = context.TeacherAvailability.Where(x => x.FKTeacherID == UserID && x.IsActive == true).OrderBy(x => x.AvailableTime).ToList();
            ListOfTeacherAvailability.ForEach(x =>
            {
                x.Hour = x.AvailableTime.ToString("HH");
                x.TimeCulture = x.AvailableTime.ToString("tt", CultureInfo.InvariantCulture);
                x.Date = x.AvailableTime.ToString("D");
            });



            return ListOfTeacherAvailability;
        }

        public List<DateViewModel> ListOfTeacherTimes(Int64 UserID)
        {

            //var Date = context.TeacherAvailability.Where(x => x.FKTeacherID == UserID && x.IsActive == true).Select(x => SqlFunctions.DatePart("year", x.AvailableTime) == x.AvailableTime.Year
            //            && SqlFunctions.DatePart("month", x.AvailableTime) == x.AvailableTime.Month
            //            && SqlFunctions.DatePart("day", x.AvailableTime) == x.AvailableTime.Day).ToList();

            //var Date = context.TeacherAvailability.Where(x => x.FKTeacherID == UserID && x.IsActive == true).Select(x=>new
            //{
            //    Dates = DbFunctions.TruncateTime(x.AvailableTime)
            //}).ToList();
            var Date = context.TeacherAvailability.Where(x => x.FKTeacherID == UserID && x.IsActive == true).Select(x => new
            {
                Dates = x.AvailableTime
            }).OrderBy(x => x.Dates).ToList();

            List<DateViewModel> store = new List<DateViewModel>();
            DateViewModel model = new DateViewModel();
            foreach (var item in Date)
            {
                string s = item.Dates.Day + " " + item.Dates.ToString("ddd");
                var g = store.FirstOrDefault(x => x.Date == s);
                if (g == null)
                {
                    model = new DateViewModel();
                    model.Time = new List<string>();
                    model.RealTime = new List<DateTime>();
                    foreach (var list in Date)
                    {
                        string k = list.Dates.Day + " " + list.Dates.ToString("ddd");
                        if (k == s)
                        {
                            string a = list.Dates.ToString("HH") + list.Dates.ToString("tt", CultureInfo.InvariantCulture);
                            model.Time.Add(a.ToString());
                            model.RealTime.Add(list.Dates);
                        }
                    }
                    model.Date = s;
                    store.Add(model);
                }
            }
            return store;
        }

        public List<DateViewModel> ListOfTeacherBookingTimes(Int64 UserID)
        {
            CustomTimeZone customTimeZone = new CustomTimeZone();
            DateTime time = customTimeZone.DateTimeNow();
            var SessionsListTimes = context.Sessions.Where(x => x.FKRequestedTo == UserID && x.TrialHappenDate > time).Select(x => x.TrialHappenDate).ToList();
            var Date = context.TeacherAvailability.Where(x => x.FKTeacherID == UserID && x.IsActive == true && x.AvailableTime > time).Select(x => new
            {
                Dates = x.AvailableTime
            }).OrderBy(x => x.Dates).ToList();

            List<DateViewModel> store = new List<DateViewModel>();
            DateViewModel model = new DateViewModel();
            foreach (var item in Date)
            {
                //if (!SessionsListTimes.Contains(item.Dates))  
                // Here I need to get teacher only accepted times

                if (!SessionsListTimes.Any(x => x == item.Dates))
                {
                    string s = item.Dates.Day + " " + item.Dates.ToString("ddd");
                    var g = store.FirstOrDefault(x => x.Date == s);
                    if (g == null)
                    {
                        model = new DateViewModel();
                        model.Time = new List<string>();
                        model.RealTime = new List<DateTime>();
                        foreach (var list in Date)
                        {
                            string k = list.Dates.Day + " " + list.Dates.ToString("ddd");
                            if (k == s && !SessionsListTimes.Any(x => x == list.Dates))
                            {
                                string a = list.Dates.ToString("hh") + " - " + list.Dates.AddHours(1).ToString("hh tt");//list.Dates.ToString("HH") + list.Dates.ToString("tt", CultureInfo.InvariantCulture);
                                model.Time.Add(a.ToString());
                                model.RealTime.Add(list.Dates);
                            }
                        }
                        model.Date = s;
                        store.Add(model);
                    }
                }
            }
            return store;
        }



        public List<CalenderDateViewModel> StudentBookedSessionsTiming(Int64 UserID)
        {
            CustomTimeZone customTimeZone = new CustomTimeZone();
            DateTime time = customTimeZone.DateTimeNow();
            var Date = context.Sessions.Where(x => x.FKRequestedBy == UserID && x.TrialHappenDate > time && x.RequestStatus == (int)Enumerations.RequestStatus.Accepted).Select(x => new
            {
                TeacherID = x.FKRequestedTo,
                Dates = x.TrialHappenDate,
                TeacherName = context.UsersProfile.Where(y => y.FKUserID == x.FKRequestedTo).Select(z => new { Name = z.FirstName + " " + z.LastName }).FirstOrDefault().Name.ToString(),
                SubjectName = context.Subjects.Where(y => y.SubjectID == x.FKSubjectID).Select(t => t.SubjectName).FirstOrDefault().ToString()
            }).OrderBy(x => x.Dates).ToList();

            List<CalenderDateViewModel> store = new List<CalenderDateViewModel>();
            CalenderDateViewModel model = new CalenderDateViewModel();
            foreach (var item in Date)
            {
                string s = item.Dates.ToString("MMMM dd, yyyy");
                var g = store.FirstOrDefault(x => x.Date == s);
                if (g == null)
                {
                    model = new CalenderDateViewModel();
                    model.Sessions = new List<CalenderSessionsModel>();
                    foreach (var list in Date)
                    {
                        string k = list.Dates.ToString("MMMM dd, yyyy");
                        if (k.Equals(s))
                        {
                            var m = new CalenderSessionsModel();
                            string a = list.Dates.ToString("HH") + list.Dates.ToString("tt", CultureInfo.InvariantCulture) + " - " + list.Dates.AddHours(1).ToString("HH") + list.Dates.AddHours(1).ToString("tt", CultureInfo.InvariantCulture);
                            m.FromToTime = a;
                            m.StudentName = null;
                            m.TeacherName = item.TeacherName;
                            m.TopicName = item.SubjectName;
                            var ids = item.TeacherID.HasValue ? item.TeacherID : 0;
                            m.Teacher = (Int64)ids;
                            model.Sessions.Add(m);
                        }
                    }
                    model.Date = item.Dates.ToString("MMMM dd, yyyy");
                    store.Add(model);
                }
            }
            return store;
        }

        public List<CalenderDateViewModel> TeacherBookedSessionsTiming(Int64 UserID)
        {
            CustomTimeZone customTimeZone = new CustomTimeZone();
            DateTime time = customTimeZone.DateTimeNow();
            var Date = context.Sessions.Where(x => x.FKRequestedTo == UserID && x.TrialHappenDate > time && x.RequestStatus == (int)Enumerations.RequestStatus.Accepted).Select(x => new
            {
                Dates = x.TrialHappenDate,
                TeacherName = context.UsersProfile.Where(y => y.FKUserID == x.FKRequestedBy).Select(z => new { Name = z.FirstName + " " + z.LastName }).FirstOrDefault().Name.ToString(),
                SubjectName = context.Subjects.Where(y => y.SubjectID == x.FKSubjectID).Select(t => t.SubjectName).FirstOrDefault().ToString()
            }).OrderBy(x => x.Dates).ToList();

            List<CalenderDateViewModel> store = new List<CalenderDateViewModel>();
            CalenderDateViewModel model = new CalenderDateViewModel();
            foreach (var item in Date)
            {
                string s = item.Dates.ToString("MMMM dd, yyyy");
                var g = store.FirstOrDefault(x => x.Date == s);
                if (g == null)
                {
                    model = new CalenderDateViewModel();
                    model.Sessions = new List<CalenderSessionsModel>();
                    foreach (var list in Date)
                    {
                        string k = list.Dates.ToString("MMMM dd, yyyy");
                        if (k.Equals(s))
                        {
                            var m = new CalenderSessionsModel();
                            string a = list.Dates.ToString("HH") + list.Dates.ToString("tt", CultureInfo.InvariantCulture) + " - " + list.Dates.AddHours(1).ToString("HH") + list.Dates.AddHours(1).ToString("tt", CultureInfo.InvariantCulture);
                            m.FromToTime = a;
                            m.TeacherName = null;
                            m.StudentName = item.TeacherName;
                            m.TopicName = item.SubjectName;
                            model.Sessions.Add(m);
                        }
                    }
                    model.Date = item.Dates.ToString("MMMM dd, yyyy");
                    store.Add(model);
                }
            }
            return store;
        }


        public MyProfileViewModel GetMyProfile(Int64 UserID)
        {
            var item = context.UsersProfile.Where(x => x.FKUserID == UserID).FirstOrDefault();

            if (item != null)
            {
                MyProfileViewModel obj = new MyProfileViewModel();
                obj.Name = item.FirstName + " " + item.LastName;
                obj.Location = context.States.Where(x => x.StateID == item.FKStateID).Select(x => x.StateName).FirstOrDefault() + " - " + item.CityName;
                obj.AboutMe = (item.AboutMe != null) ? item.AboutMe : "Not Provided";
                obj.ProfileImage = (item.ProfilePic != null) ? item.ProfilePic : null;
                string lName = Convert.ToString(context.UserLanguages.Where(x => x.FKUserID == UserID).Select(x => x.LanguageName).FirstOrDefault());
                obj.Languages = (lName != null) ? lName : "Not Provided";
                string grade = Convert.ToString(context.StudentGrades.Where(x => x.FKUserID == UserID).Select(x => x.GradeName).FirstOrDefault());
                obj.Grade = (grade != null) ? grade : "Not Provided";
                string email = Convert.ToString(context.Users.Where(x => x.UserID == UserID).Select(x => x.LoginEmail).FirstOrDefault());
                obj.EmailAddress = (email != null) ? email : null;
                obj.ContactNumber = (item.ContactNumber != null) ? item.ContactNumber : "Not Provided";
                return obj;
            }
            return null;
        }


        public EditProfileViewModel EditMyProfile(Int64 UserID)
        {
            var item = context.UsersProfile.Where(x => x.FKUserID == UserID).FirstOrDefault();

            if (item != null)
            {
                EditProfileViewModel obj = new EditProfileViewModel();
                obj.Name = item.FirstName + " " + item.LastName;
                dynamic sId = item.FKStateID.HasValue ? item.FKStateID : 0;

                obj.StateID = sId;
                obj.sListofStates = context.States.ToList().Select(x => new SelectListItem()
                {
                    Text = x.StateName,
                    Value = x.StateID.ToString(),
                    Selected = (x.StateID == obj.StateID) ? true : false
                });
                obj.CityName = item.CityName;
                obj.AboutMe = (item.AboutMe != null) ? item.AboutMe : null;
                obj.ProfileImage = (item.ProfilePic != null) ? item.ProfilePic : null;
                string lName = Convert.ToString(context.UserLanguages.Where(x => x.FKUserID == UserID).Select(x => x.LanguageName).FirstOrDefault());
                obj.Languages = (lName != null) ? lName : null;
                string grade = Convert.ToString(context.StudentGrades.Where(x => x.FKUserID == UserID).Select(x => x.GradeName).FirstOrDefault());
                obj.Grade = (grade != null) ? grade : null;
                string email = Convert.ToString(context.Users.Where(x => x.UserID == UserID).Select(x => x.LoginEmail).FirstOrDefault());
                obj.EmailAddress = (email != null) ? email : null;
                obj.ContactNumber = (item.ContactNumber != null) ? item.ContactNumber : null;
                return obj;
            }
            return null;
        }

        public TeacherEditProfileViewModel TeacherEditMyProfile(Int64 UserID)
        {
            var item = context.UsersProfile.Where(x => x.FKUserID == UserID).FirstOrDefault();

            if (item != null)
            {
                TeacherEditProfileViewModel obj = new TeacherEditProfileViewModel();
                obj.Name = item.FirstName + " " + item.LastName;
                dynamic sId = item.FKStateID.HasValue ? item.FKStateID : 0;
                obj.StateID = sId;
                obj.sListofStates = context.States.ToList().Select(x => new SelectListItem()
                {
                    Text = x.StateName,
                    Value = x.StateID.ToString(),
                    Selected = (x.StateID == obj.StateID) ? true : false
                });
                obj.CityName = item.CityName;
                obj.AboutMe = (item.AboutMe != null) ? item.AboutMe : null;
                obj.ProfileImage = (item.ProfilePic != null) ? item.ProfilePic : null;
                string lName = Convert.ToString(context.UserLanguages.Where(x => x.FKUserID == UserID).Select(x => x.LanguageName).FirstOrDefault());
                obj.Languages = (lName != null) ? lName : null;
                string grade = Convert.ToString(context.StudentGrades.Where(x => x.FKUserID == UserID).Select(x => x.GradeName).FirstOrDefault());
                var objQualification = GetUserQualification(Config.CurrentUser);
                string uName = objQualification.UniversityName;
                obj.UniversityName = (uName != null) ? uName : null;
                string email = Convert.ToString(context.Users.Where(x => x.UserID == UserID).Select(x => x.LoginEmail).FirstOrDefault());
                obj.EmailAddress = (email != null) ? email : null;
                obj.ContactNumber = (item.ContactNumber != null) ? item.ContactNumber : null;
                obj.SessionCost = item.SessionCost;
                return obj;

            }
            return null;
        }

        public int UpdateStudentProfile(EditProfileViewModel model)
        {
            try
            {
                MyDbContext contexts = new MyDbContext();
                var item = contexts.UsersProfile.Where(x => x.FKUserID == model.UserID).FirstOrDefault();
                if (item != null)
                {
                    contexts.Dispose();
                    MyDbContext cxt = new MyDbContext();
                    cxt.UsersProfile.Attach(item);
                    item.ProfilePic = (model.ProfileImage != null) ? model.ProfileImage : item.ProfilePic;
                    item.FKStateID = (model.StateID > 0) ? model.StateID : item.FKStateID;
                    item.CityName = (model.CityName != null) ? model.CityName : item.CityName;
                    item.AboutMe = (model.AboutMe != null && model.AboutMe != "") ? model.AboutMe : item.AboutMe;
                    item.ContactNumber = (model.ContactNumber != null && model.ContactNumber != item.ContactNumber) ? model.ContactNumber : item.ContactNumber;
                    cxt.SaveChanges();
                    var isGradeExist = context.StudentGrades.Where(x => x.FKUserID == model.UserID).FirstOrDefault();
                    if (isGradeExist == null)
                    {
                        // It means need to register Grades
                        MyDbContext cxtz = new MyDbContext();
                        StudentGrades obj = new StudentGrades();
                        obj.FKUserID = model.UserID;
                        obj.GradeName = model.Grade;
                        cxtz.StudentGrades.Add(obj);
                        cxtz.SaveChanges();

                    }
                    else
                    {
                        MyDbContext contextz = new MyDbContext();
                        contextz.StudentGrades.Attach(isGradeExist);
                        isGradeExist.GradeName = (model.Grade != null && model.Grade != "") ? model.Grade : isGradeExist.GradeName;
                        contextz.SaveChanges();
                        // It means Update Grade Value
                    }


                    var isLanguageExist = context.UserLanguages.Where(x => x.FKUserID == model.UserID).FirstOrDefault();
                    if (isLanguageExist == null)
                    {
                        // It means need to register Language
                        MyDbContext contextz = new MyDbContext();
                        UserLanguages obj = new UserLanguages();
                        obj.FKUserID = model.UserID;
                        obj.LanguageName = model.Languages;
                        contextz.UserLanguages.Add(obj);
                        contextz.SaveChanges();
                    }
                    else
                    {
                        MyDbContext contextz = new MyDbContext();
                        contextz.UserLanguages.Attach(isLanguageExist);
                        isLanguageExist.LanguageName = (model.Languages != null && model.Languages != "") ? model.Languages : isLanguageExist.LanguageName;
                        contextz.SaveChanges();
                        // It means Update Language Value
                    }

                    // Add Activity of Update Profile


                    UsersActivityManager uManager = new UsersActivityManager();
                    UserActivities uActivity = new UserActivities();
                    uActivity.FKUserID = model.UserID;
                    uActivity.ActionPerformed = "Your profile updated";
                    Int64 ID = uManager.AddActivity(uActivity);


                    return 1;




                }
                else
                {
                    return 0;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return 0;
        }


        public int ChangePassword(ChangePasswordViewModel model)
        {
            var item = context.Users.Where(x => x.UserID == Config.CurrentUser).FirstOrDefault();

            if (item != null)
            {
                MyDbContext cxtz = new MyDbContext();
                cxtz.Users.Attach(item);
                item.Password = (model.Password != null && model.Password != "") ? model.Password : item.Password;
                int status = cxtz.SaveChanges();


                // Add Activity of Change Password


                UsersActivityManager uManager = new UsersActivityManager();
                UserActivities uActivity = new UserActivities();
                uActivity.FKUserID = model.UserID;
                uActivity.ActionPerformed = "You changed your password";
                Int64 ID = uManager.AddActivity(uActivity);




                return status;
            }
            return 0;
        }

        public int TeachersChangePassword(TeachersChangePasswordViewModel model)
        {
            var item = context.Users.Where(x => x.UserID == Config.CurrentUser).FirstOrDefault();

            if (item != null)
            {
                MyDbContext cxtz = new MyDbContext();
                cxtz.Users.Attach(item);
                item.Password = (model.Password != null && model.Password != "") ? model.Password : item.Password;
                int status = cxtz.SaveChanges();
                return status;
            }
            return 0;
        }

        public int UpdateTeacherProfile(TeacherEditProfileViewModel model)
        {
            try
            {
                MyDbContext contexts = new MyDbContext();
                var item = contexts.UsersProfile.Where(x => x.FKUserID == model.UserID).FirstOrDefault();
                if (item != null)
                {
                    contexts.Dispose();
                    MyDbContext cxt = new MyDbContext();
                    cxt.UsersProfile.Attach(item);
                    item.ProfilePic = (model.ProfileImage != null) ? model.ProfileImage : item.ProfilePic;
                    item.FKStateID = (model.StateID > 0) ? model.StateID : item.FKStateID;
                    item.CityName = (model.CityName != null) ? model.CityName : item.CityName;
                    item.AboutMe = (model.AboutMe != null && model.AboutMe != "") ? model.AboutMe : item.AboutMe;
                    item.ContactNumber = (model.ContactNumber != null && model.ContactNumber != item.ContactNumber) ? model.ContactNumber : item.ContactNumber;
                    item.SessionCost = (model.SessionCost > 0) ? model.SessionCost : item.SessionCost;
                    cxt.SaveChanges();
                    var isQualificationExist = context.TeacherQualifications.Where(x => x.FKUserID == model.UserID).FirstOrDefault();
                    if (isQualificationExist == null)
                    {
                        // It means need to register Qualification
                        MyDbContext cxtz = new MyDbContext();
                        TeacherQualification obj = new TeacherQualification();
                        obj.FKUserID = model.UserID;
                        obj.UniversityName = model.UniversityName;
                        cxtz.TeacherQualifications.Add(obj);
                        cxtz.SaveChanges();

                    }
                    else
                    {
                        // It means Update Grade Value
                        MyDbContext contextz = new MyDbContext();
                        context.TeacherQualifications.Attach(isQualificationExist);
                        isQualificationExist.UniversityName = (model.UniversityName != null && model.UniversityName != "") ? model.UniversityName : isQualificationExist.UniversityName;
                        context.SaveChanges();
                    }

                    var isLanguageExist = context.UserLanguages.Where(x => x.FKUserID == model.UserID).FirstOrDefault();
                    if (isLanguageExist == null)
                    {
                        // It means need to register Language
                        MyDbContext contextz = new MyDbContext();
                        UserLanguages obj = new UserLanguages();
                        obj.FKUserID = model.UserID;
                        obj.LanguageName = model.Languages;
                        contextz.UserLanguages.Add(obj);
                        contextz.SaveChanges();
                    }
                    else
                    {
                        MyDbContext contextz = new MyDbContext();
                        context.UserLanguages.Attach(isLanguageExist);
                        isLanguageExist.LanguageName = (model.Languages != null && model.Languages != "") ? model.Languages : isLanguageExist.LanguageName;
                        context.SaveChanges();
                        // It means Update Language Value
                    }
                    return 1;
                }
                else
                {
                    return 0;
                }
            }
            catch (Exception ex)
            {
                return 0;
                throw ex;
            }
            return 0;
        }

        public int RemoveTeacherSubject(Int64 TeacherSubjectID, Int64 UserID)
        {
            try
            {
                MyDbContext contexts = new MyDbContext();
                var item = contexts.TeacherSubjects.Where(x => x.TeacherSubjectsID == TeacherSubjectID && x.IsActive == true && x.FKUserID == UserID).FirstOrDefault();
                if (item != null)
                {
                    contexts.Dispose();
                    MyDbContext cxt = new MyDbContext();
                    cxt.TeacherSubjects.Attach(item);
                    item.IsActive = false;
                    cxt.SaveChanges();
                    return 1;
                }
                else
                {
                    return 0;
                }
            }
            catch (Exception ex)
            {
                return 0;
                throw ex;
            }
            return 0;
        }


        public int AddTeacherSubject(AddSubjectViewModel model, Int64 UserID)
        {
            try
            {
                MyDbContext contexts = new MyDbContext();
                //var item = contexts.TeacherSubjects.Where(x => x.FKCurriculumID == model.CurriculumId && x.FKSubjectID==model.PrimarySubjectID && x.FKGradeID==model.GradeID && x.IsActive == false && x.FKUserID == UserID).FirstOrDefault();
                if (model != null)
                {
                    var alreadyExists = contexts.TeacherSubjects.Any(x => x.FKCurriculumID == model.CurriculumId && x.FKSubjectID == model.PrimarySubjectID && x.FKGradeID == model.GradeID && x.IsActive == true && x.FKUserID == UserID); //.Where(x => x.FKCurriculumID == model.CurriculumId && x.FKSubjectID == model.PrimarySubjectID && x.FKGradeID == model.GradeID && x.IsActive == true && x.FKUserID == UserID).FirstOrDefault();
                    if (alreadyExists == false)
                    {
                        TeacherSubjects obj = new TeacherSubjects();
                        obj.FKUserID = UserID;
                        obj.FKCurriculumID = model.CurriculumId;
                        obj.FKGradeID = model.GradeID;
                        obj.FKSubjectID = model.PrimarySubjectID;
                        obj.IsActive = true;
                        obj.Type = (int)Enumerations.SubjectType.Primary;
                        contexts.TeacherSubjects.Add(obj);
                        contexts.SaveChanges();
                        return 1;
                    }
                    else
                    {
                        return 2;
                    }
                }
                else
                {
                    return 0;
                }
            }
            catch (Exception ex)
            {
                return 0;
                throw ex;
            }
            return 0;
        }


        #region [Update Is Online]
        public bool UpdateOnlineStatus(Int64 UserID, bool status)
        {
            try
            {
                var user = context.Users.Where(x => x.UserID == UserID && x.IsActive == true).FirstOrDefault();
                if (user != null)
                {

                    context.Users.Attach(user);
                    user.IsOnline = status;
                    CustomTimeZone czone = new CustomTimeZone();
                    user.LastLogin = czone.DateTimeNow();
                    context.SaveChanges();

                }
                else
                {
                    return false;
                }
            }
            catch (Exception)
            {
                throw;
            }

            return false;
        }



        #endregion




        #region [Api Methods]




        #endregion



    }
}
