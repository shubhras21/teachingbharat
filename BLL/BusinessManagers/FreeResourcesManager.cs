﻿using BLL.BusinessManagers.AdminManager;
using BLL.BusinessModels.TeacherModels;
using BLL.Helpers;
using DAL.Entities;
using DAL.MasterEntity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL.BusinessManagers
{
   public class FreeResourcesManager
    {
        MyDbContext context = new MyDbContext();
        CustomTimeZone czone = new CustomTimeZone();
        CurriculumsManagers cManager = new CurriculumsManagers();


        public Int64 AddResource(AddResourceViewModel model)
        {
            try
            {

                var subjectDetail = cManager.GetSubject(model.FKSubjectID);

                var FreeResource = new FreeResources
                {
                    ResourceTitle = model.ResourceTitle,
                    VideoURL = model.VideoURL,
                    VideoDescription = model.ResourceDescription,
                    FKSubjectID = model.FKSubjectID,
                    FKCurriculumID = (subjectDetail != null) ? subjectDetail.FKCurriculumID : 0,
                    FKGradeID= (subjectDetail != null) ? subjectDetail.FKGradeID : 0,
                    FKTopicID = model.FKTopicID,
                    FKUploadedBy=model.FKOfferedBy,
                    UploadDate=czone.DateTimeNow(),
                    NumberOfViews=0,
                    OptionalTopic=model.TopicOptional,
                    IsActive=true,
                    Year=model.ResourceYear

                  };
                context.FreeResources.Add(FreeResource);
                context.SaveChanges();
                return FreeResource.FreeResourcesID;
                
            }
            catch (Exception)
            {

                throw;
            }
        }


        public FreeResources GetFreeResource(Int64 ResourceID)
        {

            try
            {
                FreeResources obj = new FreeResources();
                obj = context.FreeResources.Where(x => x.FreeResourcesID == ResourceID && x.IsActive == true).FirstOrDefault();
                if(obj!=null)
                {
                      obj.OfferedBy = context.UsersProfile.Where(y => y.FKUserID == obj.FKUploadedBy).Select(y => new { Name = y.FirstName + " " + y.LastName }).FirstOrDefault().Name.ToString();
                      obj.TeacherPic = Convert.ToString(context.UsersProfile.Where(y => y.FKUserID == obj.FKUploadedBy).Select(y => y.ProfilePic).FirstOrDefault());
                      return obj;
                }

                else
                {
                    return null;
                }

                    
            }
            catch (Exception)
            {
                
                throw;
            }

            return null;

        }


        public bool UpdateResource(AddResourceViewModel model)
        {
            try
            {

                var subjectDetail = cManager.GetSubject(model.FKSubjectID);

                FreeResources obj = new FreeResources();
                obj = context.FreeResources.Where(x => x.FreeResourcesID == model.ResourceID && x.IsActive == true).FirstOrDefault();

                if(obj!=null)
                {

                    context.FreeResources.Attach(obj);
                    obj.ResourceTitle = model.ResourceTitle;
                    obj.VideoURL = model.VideoURL;
                    obj.VideoDescription = model.ResourceDescription;
                    obj.FKSubjectID = model.FKSubjectID;
                    obj.FKCurriculumID = (subjectDetail != null) ? subjectDetail.FKCurriculumID : 0;
                    obj.FKGradeID = (subjectDetail != null) ? subjectDetail.FKGradeID : 0;
                    obj.FKTopicID = model.FKTopicID;
                    obj.OptionalTopic = model.TopicOptional;
                    obj.Year = model.ResourceYear;
                    context.SaveChanges();

                    return true;

                }
                else
                {
                    return false;
                }

               

            }
            catch (Exception)
            {

                throw;
            }
        }


        public bool DeleteResource(Int64 ResourceID)
        {
            try
            {
               
                FreeResources obj = new FreeResources();
                obj = context.FreeResources.Where(x => x.FreeResourcesID == ResourceID && x.IsActive == true).FirstOrDefault();
                if (obj != null)
                {
                    context.FreeResources.Attach(obj);
                    obj.IsActive = false;
                    context.SaveChanges();
                    return true;
                }
                else
                {
                    return false;
                }


            }
            catch (Exception)
            {

                throw;
            }
        }


        public List<FreeResources> TeacherResources(Int64 UserID)
        {
            try
            {
                List<FreeResources> list = context.FreeResources.Where(x => x.FKUploadedBy == UserID && x.IsActive == true).ToList();

                if (list != null && list.Count > 0)
                {



                    var tSubjectList = list.GroupBy(y => y.FKSubjectID).Select(y => y.FirstOrDefault()).Distinct().ToList();

                    List<Subjects> listOfSubject = new List<Subjects>();

                    foreach (var item in tSubjectList)
                    {
                        bool isExist = listOfSubject.Any(x => x.SubjectID == item.FKSubjectID);
                        if (isExist==false)
                        {
                            Subjects obj = new Subjects();
                            obj = cManager.GetSubjectDetail(item.FKSubjectID);
                            listOfSubject.Add(obj);
                        }
                       
                    }


                    list.ForEach(x =>
                    {

                        x.TeacherSubjects = new List<Subjects>();
                        x.TeacherSubjects = listOfSubject;
                        x.OfferedBy = context.UsersProfile.Where(y => y.FKUserID == x.FKUploadedBy).Select(y => new { Name = y.FirstName + " " + y.LastName }).FirstOrDefault().Name.ToString();
                        x.TeacherPic = Convert.ToString(context.UsersProfile.Where(y => y.FKUserID == x.FKUploadedBy).Select(y => y.ProfilePic).FirstOrDefault());

                    });

                    return list;
                }
                else
                {
                    return null;
                }
                  

            }
            catch (Exception)
            {
                
                throw;
            }
        }


        public List<FreeResources> TeacherResourcesBySubject(Int64 UserID, Int64 SubjectID)
        {
            try
            {
                if(SubjectID==0)
                {
                    List<FreeResources> list = TeacherResources(UserID);
                    return list;
                }
                else
                {
                    List<FreeResources> list = context.FreeResources.Where(x => x.FKUploadedBy == UserID && x.FKSubjectID == SubjectID && x.IsActive == true).ToList();

                    if (list != null && list.Count > 0)
                    {

                        var tSubjectList = list.GroupBy(y => y.FKSubjectID).Select(y => y.FirstOrDefault()).ToList();

                        List<Subjects> listOfSubject = new List<Subjects>();

                        foreach (var item in tSubjectList)
                        {
                            Subjects obj = new Subjects();
                            obj = cManager.GetSubjectDetail(item.FKSubjectID);
                            listOfSubject.Add(obj);
                        }


                        list.ForEach(x =>
                        {

                            x.TeacherSubjects = new List<Subjects>();
                            x.TeacherSubjects = listOfSubject;
                            x.OfferedBy = context.UsersProfile.Where(y => y.FKUserID == x.FKUploadedBy).Select(y => new { Name = y.FirstName + " " + y.LastName }).FirstOrDefault().Name.ToString();
                            x.TeacherPic = Convert.ToString(context.UsersProfile.Where(y => y.FKUserID == x.FKUploadedBy).Select(y => y.ProfilePic).FirstOrDefault());

                        });

                        return list;
                    }
                    else
                    {
                        return null;
                    }
                }
               


            }
            catch (Exception)
            {

                throw;
            }
        }


        public List<FreeResources> AllResources()
        {
            try
            {
                List<FreeResources> list = context.FreeResources.Where(x=>x.IsActive == true).ToList();

                if (list != null && list.Count > 0)
                {

                    list.ForEach(x =>
                    {
                        x.OfferedBy = context.UsersProfile.Where(y => y.FKUserID == x.FKUploadedBy).Select(y => new { Name = y.FirstName + " " + y.LastName }).FirstOrDefault().Name.ToString();
                        x.TeacherPic = Convert.ToString(context.UsersProfile.Where(y => y.FKUserID == x.FKUploadedBy).Select(y => y.ProfilePic).FirstOrDefault());
                        x.Subject = cManager.GetSubjectDetail(x.FKSubjectID);
                    });

                    return list;
                }
                else
                {
                    return null;
                }


            }
            catch (Exception)
            {

                throw;
            }
        }

        public List<FreeResources> AllResourcesBySubject(Int64 SubjectID)
        {
            try
            {
               
                    List<FreeResources> list = context.FreeResources.Where(x=>x.FKSubjectID == SubjectID && x.IsActive == true).ToList();

                    if (list != null && list.Count > 0)
                    {
                      
                        list.ForEach(x =>
                        {

                            x.Subject=  cManager.GetSubjectDetail(x.FKSubjectID);
                            x.OfferedBy = context.UsersProfile.Where(y => y.FKUserID == x.FKUploadedBy).Select(y => new { Name = y.FirstName + " " + y.LastName }).FirstOrDefault().Name.ToString();
                            x.TeacherPic = Convert.ToString(context.UsersProfile.Where(y => y.FKUserID == x.FKUploadedBy).Select(y => y.ProfilePic).FirstOrDefault());

                        });

                        return list;

                    }
                    else
                    {
                        return null;
                    }
                



            }
            catch (Exception)
            {

                throw;
            }
        }



        public List<FreeResourcesFolder> ListOfResourcesFolder()
        {

            try
            {
                List<FreeResourcesFolder> list = new List<FreeResourcesFolder>();

                var FreeResourceList = context.FreeResources.Where(x => x.IsActive == true).ToList();

                if (FreeResourceList!=null && FreeResourceList.Count>0)
                {

                    List<FreeResources> tSubjectList = FreeResourceList.GroupBy(y => y.FKSubjectID).Select(y => y.FirstOrDefault()).ToList();

                    foreach(var item in tSubjectList)
                    {

                        var innerlist = context.FreeResources.Where(x => x.FKSubjectID == item.FKSubjectID && x.IsActive == true).ToList();

                        int count = innerlist.Count();
                        int min = innerlist.Min(x => x.Year);
                        int max = innerlist.Max(x => x.Year);

                        FreeResourcesFolder obj = new FreeResourcesFolder();
                        obj.TotalVideos = count;
                        obj.FromToYear = (min == max) ? Convert.ToString(min) : Convert.ToString(min + " - " + max);
                        obj.SubjectID = innerlist.First().FKSubjectID;
                        var subject = cManager.GetSubjectDetail(obj.SubjectID);
                        obj.FolderName = subject.CompleteSubjectName;
                        obj.ClassName = subject.GradeName;
                        obj.SubjectName = subject.SubjectName;
                        list.Add(obj);
                    }

                    return list;

                }
                else
                {
                    return null;
                }
               

            }
            catch (Exception)
            { 
                throw;
            }
        }

        public bool AddViewCount(Int64 ResourceID)
        {
            try
            {
                var obj = context.FreeResources.Where(x => x.FreeResourcesID == ResourceID && x.IsActive == true).FirstOrDefault();
                if(obj!=null)
                {
                    context.FreeResources.Attach(obj);
                    obj.NumberOfViews = obj.NumberOfViews + 1;
                    context.SaveChanges();
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception)
            {
                
                throw;
            }
        }

    }
}
