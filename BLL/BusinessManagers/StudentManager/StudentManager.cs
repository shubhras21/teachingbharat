﻿using BLL.BusinessModels;
using BLL.BusinessModels.AdminModels;
using BLL.BusinessModels.StudentModels;
using BLL.BusinessModels.TeacherModels;
using BLL.BusinessModels.UserModels;
using BLL.Helpers;
using DAL.Entities;
using DAL.MasterEntity;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace BLL.BusinessManagers.StudentManager
{
    public class StudentManager
    {
        public MyDbContext context = new MyDbContext();
        CustomTimeZone customTimeZone = new CustomTimeZone();
        #region [DETAILS FOR FINDING TEACHER]

        public FindTeacherViewModel GetDetails()
        {
            try
            {
                var model = new FindTeacherViewModel
                {
                    ListofTargets = context.Curriculums.Where(t => t.IsActive == true).Select(t => new CurriculamModel
                    {
                        CurriculumId = t.CurriculumID,
                        CurriculumName = t.CurriculumName

                    }).ToList(),
                    ListofGrades = null,
                    ListofSubjects = null,
                    ListofFrequency = null
                    //ListofGrades = context.Grades.Where(t => t.IsActive == true).Select(g => new GradeModel
                    //{

                    //    GradeId = g.GradeID,
                    //    GradeName = g.GradeName

                    //}).ToList(),
                    //ListofSubjects = context.Subjects.Where(t => t.IsActive == true).Select(s => new SubjectModel
                    //{
                    //    SubjectId = s.SubjectID,
                    //    SubjectName = s.SubjectName

                    //  }).ToList()
                };

                return model;
            }
            catch (System.Exception ex)
            {

                throw ex;
            }

        }

        public FindTeacherViewModel GetGrades(Int64 CurriculumID)
        {
            try
            {
                var model = new FindTeacherViewModel();
                model.ListofTargets = context.Curriculums.Where(t => t.IsActive == true).Select(t => new CurriculamModel
                {
                    CurriculumId = t.CurriculumID,
                    CurriculumName = t.CurriculumName

                }).ToList();
                model.ListofGrades = context.Grades.Where(t => t.IsActive == true && t.FKCurriculumID == CurriculumID).Select(t => new GradeModel
                {
                    GradeId = t.GradeID,
                    GradeName = t.GradeName

                }).ToList();

                return model;
            }
            catch (System.Exception ex)
            {

                throw ex;
            }

        }

        public FindTeacherViewModel GetSubjects(Int64 GradeID)
        {
            try
            {
                var model = new FindTeacherViewModel();
                model.ListofTargets = context.Curriculums.Where(t => t.IsActive == true).Select(t => new CurriculamModel
                {
                    CurriculumId = t.CurriculumID,
                    CurriculumName = t.CurriculumName

                }).ToList();
                model.ListofGrades = context.Grades.Where(t => t.IsActive == true).Select(t => new GradeModel
                {
                    GradeId = t.GradeID,
                    GradeName = t.GradeName

                }).ToList();

                model.ListofSubjects = context.Subjects.Where(t => t.IsActive == true && t.FKGradeID == GradeID).Select(t => new SubjectModel
                {
                    SubjectId = t.SubjectID,
                    SubjectName = t.SubjectName

                }).ToList();
                return model;
            }
            catch (System.Exception ex)
            {

                throw ex;
            }

        }

        #endregion
        public List<TeacherSubjects> ListOfRelevantTeachers(Int64 SubjectId, Int64 GradeId, Int64 CurriculumID)
        {
            List<TeacherViewModel> list = new List<TeacherViewModel>();

            var lists = context.TeacherSubjects.Where(x => x.FKGradeID == GradeId && x.FKCurriculumID == CurriculumID && x.FKSubjectID == SubjectId && x.IsActive == true).ToList();

            lists.ForEach(x => x.User = context.Users.Where(y => y.UserID == x.FKUserID && x.IsActive == true).FirstOrDefault());
            lists.ForEach(x =>
            {
                x.User.FirstName = context.UsersProfile.Where(y => y.FKUserID == x.FKUserID).Select(z => z.FirstName).FirstOrDefault();
                x.User.LastName = context.UsersProfile.Where(y => y.FKUserID == x.FKUserID).Select(z => z.LastName).FirstOrDefault();
                x.User.Experience = context.UsersProfile.Where(y => y.FKUserID == x.FKUserID).Select(z => z.ExperienceName).FirstOrDefault();
                x.User.Qualification = context.TeacherQualifications.Where(y => y.FKUserID == x.FKUserID).Select(z => z.QualificationName).FirstOrDefault();
                x.ProfilePic = Convert.ToString(context.UsersProfile.Where(y => y.FKUserID == x.FKUserID).Select(y => y.ProfilePic).FirstOrDefault());
                RatingsViewModel obj = new RatingsViewModel();
                RatingsManager rManager = new RatingsManager();
                obj = rManager.ListOfRatings(x.FKUserID);
                if (obj != null)
                {
                    x.Ratings = obj.RatingAverage;
                }
            });
            return lists;
        }


        public List<UserViewModel> GetStudentsList()
        {
            var usersList = new List<UserViewModel>();
            using (var ctx = new MyDbContext())
            {
                var DbUsers = (from s in ctx.Users
                               where s.UserType == (int)Enumerations.UserType.Student
                               //  && s.UserStatus != (int)Enumerations.UserStatus.IsBlocked
                               && s.IsActive == true
                               select s).ToList();

                if (DbUsers.Count > 0)
                {
                    DbUsers.ForEach(x => x.UsersProfile = ctx.UsersProfile.Where(i => i.FKUserID == x.UserID).FirstOrDefault());
                    Mapper.MappUsersListToModel(DbUsers, ref usersList);

                    foreach (var s in usersList.Where(x => x.UserStatus == (int)Enumerations.UserStatus.IsBlocked))
                    {
                        var blockreason = (from BR in ctx.UserBlockReasons
                                           where s.UserID == BR.FKUserID
                                           select BR).FirstOrDefault();

                        if (blockreason != null)
                        {
                            s.BlockReason = blockreason.Reason;
                        }
                    }
                }
            }
            return usersList;
        }
    
        public List<GradeViewModel> GetGradesListByCurriculumID(long? curriculumID)
        {
            var gradeViewModel = new List<GradeViewModel>();

            using (var ctx = new MyDbContext())
            {
                var DbGradesList = (from grd in ctx.Grades where grd.FKCurriculumID == curriculumID select grd).ToList();
                if (DbGradesList.Count > 0)
                {
                    Mapper.MappGradesListToModel(DbGradesList, ref gradeViewModel);
                }

                return gradeViewModel;
            }

        }

        public UserViewModel CreateNewStudent(UserViewModel userViewModel)
        {
            try
            {
                var DbUser = new Users();
                Mapper.MappUserViewModelToUserEntity(userViewModel, ref DbUser);

                MyDbContext context = new MyDbContext();
                context.Users.Add(DbUser);
                context.SaveChanges();
                userViewModel.UserID = DbUser.UserID;

                DbUser.UsersProfile.FKUserID = DbUser.UserID;
                DbUser.UsersProfile.CreateDate = DateTime.Now;
                DbUser.UsersProfile.UpdatedDate = DateTime.Now;

                userViewModel.UsersProfileViewModel.FKUserId = DbUser.UserID;
                userViewModel.UsersProfileViewModel.CreateDate = DateTime.Now;
                userViewModel.UsersProfileViewModel.UpdatedDate = DateTime.Now;

                context.UsersProfile.Add(DbUser.UsersProfile);
                context.SaveChanges();

                userViewModel.Response = (int)Enumerations.ResponsHelpers.Created;

            }
            catch (Exception ex)
            { }
            return userViewModel;
        }

        public void AddStudentGrades(string gradeName, long FKUserID)
        {
            try
            {
                using (var ctx = new MyDbContext())
                {
                    StudentGrades studentGrade = new StudentGrades();
                    studentGrade.GradeName = gradeName;
                    studentGrade.FKUserID = FKUserID;

                    ctx.StudentGrades.Add(studentGrade);
                    ctx.SaveChanges();

                }
            }
            catch (Exception ex)
            { }

        }

        public GradeViewModel GetGradeByGradeID(long gradeID)
        {
            var gradeModel = new GradeViewModel();
            using (var ctx = new MyDbContext())
            {
                var DbGrade = (from grd in ctx.Grades where grd.GradeID == gradeID select grd).FirstOrDefault();

                if (DbGrade.GradeID > 0)
                {
                    Mapper.MappGradeToModel(DbGrade, ref gradeModel);
                }
            }
            return gradeModel;
        }

        public UserViewModel GetStudentByID(long userID)
        {
            var userModel = new UserViewModel();
            using (var ctx = new MyDbContext())
            {
                var user = (from u in ctx.Users where u.UserID == userID select u).FirstOrDefault();

                if (user != null)
                {
                    if (user.UserID > 0)
                    {
                        user.UsersProfile = (from up in context.UsersProfile where up.FKUserID == user.UserID select up).FirstOrDefault();
                        Mapper.MappUserToUserViewModel(user, ref userModel);
                    }
                }

            }
            return userModel;
        }

        public StudentGradeViewModel GetStudentGradesByUserID(long UserID)
        {
            var studentGradeModel = new StudentGradeViewModel();
            using (var ctx = new MyDbContext())
            {
                var studentGrade = (from SG in ctx.StudentGrades
                                    where SG.FKUserID == UserID
                                    select SG).FirstOrDefault();

                if (studentGrade.StudentGradeID > 0)
                {
                    Mapper.MappStudentGradeToModel(studentGrade, ref studentGradeModel);
                }
            }
            return studentGradeModel;
        }

        public GradeViewModel GetGradeByCurriculumID(long curriculumID)
        {
            var gradeViewModel = new GradeViewModel();
            using (var ctx = new MyDbContext())
            {
                var grade = (from grd in ctx.Grades where grd.FKCurriculumID == curriculumID select grd).FirstOrDefault();
                if (grade.GradeID > 0)
                {
                    Mapper.MappGradeToModel(grade, ref gradeViewModel);
                }
            }
            return gradeViewModel;
        }



        public int UpdateStudent(UserViewModel userViewModel)
        {
            var status = 0;
            try
            {
                var DbUser = new Users();
                Mapper.MappUserViewModelToUserEntity(userViewModel, ref DbUser);

                if (DbUser.UserID > 0)
                {
                    using (var context = new MyDbContext())
                    {
                        context.Users.Attach(DbUser);
                        context.Entry(DbUser).State = EntityState.Modified;
                        context.SaveChanges();
                        status = (int)Enumerations.ResponsHelpers.Block;
                        return status;
                    }
                }
                return status;

            }
            catch (Exception ex)
            {

            }
            return status;
        }

        public int UpdateStudentWithStudentProfile(UserViewModel userViewModel)
        {
            try
            {
                var DbUser = new Users();
                Mapper.MappUserViewModelToUserEntity(userViewModel, ref DbUser);

                if (DbUser.UserID > 0)
                {
                    using (var context = new MyDbContext())
                    {
                        context.Users.Attach(DbUser);
                        context.Entry(DbUser).State = EntityState.Modified;
                        context.SaveChanges();

                        context.UsersProfile.Attach(DbUser.UsersProfile);
                        context.Entry(DbUser.UsersProfile).State = EntityState.Modified;
                        context.SaveChanges();
                    }
                }
                return (int)Enumerations.ResponsHelpers.Updated;
            }

            catch (Exception ex)
            {
                return (int)Enumerations.ResponsHelpers.Error;
            }
        }

        public int AddInUserBlockReason(UserBlockReason obj)
        {
            try
            {
                using (var ctx = new MyDbContext())
                {
                    if (obj != null)
                    {
                        ctx.UserBlockReasons.Add(obj);
                        ctx.SaveChanges();
                    }
                }
                return (int)Enumerations.ResponsHelpers.Created;
            }
            catch (Exception ex)
            {
                return (int)Enumerations.ResponsHelpers.Error;
            }
        }

        public int DeleteStudentByUserID(int userID)
        {
            try
            {
                using (var ctx = new MyDbContext())
                {
                    var userActivities = (from activities in ctx.UserActivity
                                          where activities.IsActive == true
                                          && activities.FKUserID == userID
                                          select activities).ToList();

                    if (userActivities.Count > 0)
                    {
                        foreach (var item in userActivities)
                        {
                            item.IsActive = false;
                            ctx.UserActivity.Attach(item);
                            ctx.Entry(item).State = EntityState.Modified;

                            ctx.SaveChanges();
                        }
                    }

                    var DbUserProfile = (from up in ctx.UsersProfile where up.FKUserID == userID select up).FirstOrDefault();
                    if (DbUserProfile.UsersProfileID > 0)
                    {
                        ctx.UsersProfile.Remove(DbUserProfile);
                        ctx.SaveChanges();
                    }


                    var blockReasons = (from b in ctx.UserBlockReasons where b.FKUserID == userID select b).ToList();
                    if (blockReasons.Count > 0)
                    {
                        foreach (var i in blockReasons)
                        {
                            ctx.UserBlockReasons.Remove(i);
                            ctx.SaveChanges();
                        }
                    }


                    var dbUser = (from u in ctx.Users where u.UserID == userID select u).FirstOrDefault();
                    ctx.Users.Remove(dbUser);
                    ctx.SaveChanges();
                }

                return (int)Enumerations.ResponsHelpers.Deleted;

            }
            catch (Exception ex)
            {
                return (int)Enumerations.ResponsHelpers.Deleted;
            }


        }

        public int TotalStudentsCount()
        {
            using (var ctx = new MyDbContext())
            {
                var totalStudentsCount = (from s in ctx.Users
                                          where
                                           // s.UserStatus == (int)Enumerations.UserStatus.IsActive
                                           s.UserType == (int)Enumerations.UserType.Student
                                          select s).ToList().Count;

                return totalStudentsCount;
            }
        }

        public int TotalStudentsRegisteredThisMonthCount()
        {
            using (var ctx = new MyDbContext())
            {
                var today = DateTime.Today;

                var totalStudentsRegisteredThisMonthCount = (from u in ctx.Users
                                                             join up in ctx.UsersProfile
                                                             on u.UserID equals up.FKUserID
                                                             where up.CreateDate.Value.Month == today.Month
                                                             && up.CreateDate.Value.Year == today.Year
                                                             && u.UserType == (int)Enumerations.UserType.Student
                                                             select u).ToList().Count;

                return totalStudentsRegisteredThisMonthCount;
            }
        }

        public int TotalStudentsRegisteredTodayCount()
        {
            using (var ctx = new MyDbContext())
            {
                DateTime startDateTime = DateTime.Today;
                DateTime endDateTime = DateTime.Today.AddDays(1).AddTicks(-1);

                var totalStudentsRegisteredTodayCount = (from u in ctx.Users
                                                         join up in ctx.UsersProfile
                                                         on u.UserID equals up.FKUserID
                                                         where up.CreateDate >= startDateTime
                                                         && up.CreateDate <= endDateTime
                                                          && u.UserType == (int)Enumerations.UserType.Student
                                                         select u).ToList().Count;

                return totalStudentsRegisteredTodayCount;
            }

        }

        public UserViewModel GetStudentDetails(int userID)
        {

            var listOfCourseParticipants = new List<CourseParticipants>();
            var userViewModel = new UserViewModel();
            var UserSessionsList = new List<SessionViewModel>();
            var listOfJoinSession = new List<Sessions>();
            var joinSessionsListModel = new List<SessionViewModel>();

            var CurrentTime = customTimeZone.DateTimeNow();

            using (var ctx = new MyDbContext())
            {
                var user = (from u in ctx.Users
                            where u.UserID == userID
                            && u.UserType == (int)Enumerations.UserType.Student
                            select u).FirstOrDefault();

                if (user != null)
                    user.UsersProfile = (from UP in ctx.UsersProfile where UP.FKUserID == userID select UP).FirstOrDefault();

                if (user.UserID > 0)
                {
                    Mapper.MappUserToUserViewModel(user, ref userViewModel);
                }


                var sessionsList = (from S in ctx.Sessions where S.FKRequestedBy == userID select S).Distinct().ToList();

                if (sessionsList.Count > 0)
                {
                    Mapper.MappSessionListToModel(sessionsList, ref UserSessionsList);
                    userViewModel.SessionsList = UserSessionsList;
                }

                //Getting Join Sessions
                var jSessions = ctx.Sessions.Where(x => x.FKRequestedBy == userID
                                 && x.RequestStatus == (int)Enumerations.RequestStatus.Accepted
                                 && x.CurrentStatus != (int)Enumerations.SessionStatus.Completed
                                 && x.CurrentStatus != (int)Enumerations.SessionStatus.NotTaken).ToList();

                if (jSessions != null && jSessions.Count > 0)
                {
                    foreach (var joinSession in jSessions)
                    {
                        var startTime = joinSession.TrialHappenDate;
                        var endTime = joinSession.TrialHappenDate.AddHours(1);
                        if (CurrentTime >= startTime && CurrentTime <= endTime)
                        {
                            listOfJoinSession.Add(joinSession);
                        }
                    }
                    Mapper.MappSessionListToModel(listOfJoinSession, ref joinSessionsListModel);

                    userViewModel.joinSessionsList = joinSessionsListModel;
                }



                // Getting student courses
                listOfCourseParticipants = ctx.CourseParticipants
                                              .Where(x => x.FKStudentID == userID && x.IsActive == true).ToList();
                if (listOfCourseParticipants.Count > 0)
                {
                    foreach (var cp in listOfCourseParticipants)
                    {
                        cp.Course = (from c in ctx.CourseHeader
                                     where c.CoursesHeaderID == cp.FKCourseID
                                     select c).FirstOrDefault();

                    }
                }
                userViewModel.courseParticipantsList = listOfCourseParticipants;


            }

            return userViewModel;
        }

        public SubjectModel GetSubjectBySubjectID(long subjectID)
        {
            var subjectModel = new SubjectModel();
            using (var ctx = new MyDbContext())
            {
                var subject = (from s in ctx.Subjects where s.SubjectID == subjectID select s).FirstOrDefault();
                if (subject != null)
                {
                    if (subject.SubjectID > 0)
                    {
                        Mapper.MappSubjectToModel(subject, ref subjectModel);
                    }
                }
            }
            return subjectModel;
        }

        public List<long> MyStudents(Int64 UserID)
        {
            List<UserViewModel> usersModellist = new List<UserViewModel>();
            List<int> userIds = new List<int>();

            List<Sessions> sessions = context.Sessions.Where(x => x.FKRequestedTo == UserID).ToList();
            List<Int64> alreadyExist = new List<Int64>();
            sessions.ForEach(x =>
            {
                var a = context.SessionParticipants.Where(y => y.FKSessionID == x.TrialSessionID && y.FKStudentID != UserID).Select(y => y.FKStudentID);
                foreach (var item in a)
                {
                    if (!alreadyExist.Contains(item))
                    {
                        alreadyExist.Add(item);
                    }
                }
            });

            //if (alreadyExist.Count > 0)
            //{

            //    foreach (var item in alreadyExist)
            //    {
            //        var DbUser = (from u in context.Users where u.UserID == item select u).FirstOrDefault();
            //        if (DbUser != null)
            //        {
            //            var userModel = new UserViewModel();
            //            Mapper.MappUserToUserViewModel(DbUser, ref userModel);
            //            usersModellist.Add(userModel);
            //        }
            //    }
            //}
            // return usersModellist;
            return alreadyExist;


            //if (alreadyExist.Count > 0)
            //{
            //    List<UsersProfile> profiles = new List<UsersProfile>();
            //    foreach (var item in alreadyExist)
            //    {
            //        UsersProfile obj = new UsersProfile();
            //        obj = context.UsersProfile.Where(x => x.FKUserID == item).FirstOrDefault();
            //        if (obj != null)
            //        { profiles.Add(obj); }
            //    }
            //    profiles.ForEach(x =>
            //    {
            //        string gName = context.StudentGrades.Where(y => y.FKUserID == x.FKUserID).Select(y => y.GradeName).FirstOrDefault();
            //        if (!string.IsNullOrEmpty(gName))
            //            x.StateName = gName;
            //        else
            //            x.StateName = "N/A";
            //    }
            //    );
            //    return profiles;
            //}
            //else
            //{
            //    return null;
            //}

            //   return null;
        }
        public List<GradeViewModel> GetGradesList()
        {
            var gradesListModel = new List<GradeViewModel>();
            using (var ctx = new MyDbContext())
            {
                var grades = (from g in ctx.Grades select g).ToList();
                if (grades.Count > 0)
                {
                    Mapper.MappGradesListToModel(grades, ref gradesListModel);
                }
                return gradesListModel;
            }
        }
        public List<long> GetStudentsByGradeID(int gradeID)
        {
            var studentIDs = new List<long>();
            using (var ctx = new MyDbContext())
            {
                // GET STUDENTS LIST BY GRADE ID
                var sessionsList = (from s in ctx.Sessions where s.FKGradeID == gradeID select s).ToList();
                if (sessionsList.Count > 0)
                {
                    foreach (var item in sessionsList)
                    {
                        if (!studentIDs.Contains((long)item.FKRequestedBy))
                        {
                            studentIDs.Add((long)item.FKRequestedBy);
                        }
                    }
                }

            }

            return studentIDs;
        }
        public List<long> GetUserProfileByGradeID(int gradeID)
        {
            var studentIDs = new List<long>();
            using (var ctx = new MyDbContext())
            {
                // GET STUDENTS LIST BY GRADE ID
                var sessionsList = (from s in ctx.Sessions where s.FKGradeID == gradeID select s).ToList();
                if (sessionsList.Count > 0)
                {
                    foreach (var item in sessionsList)
                    {
                        if (!studentIDs.Contains((long)item.FKRequestedBy))
                        {
                            studentIDs.Add((long)item.FKRequestedBy);
                        }
                    }
                }

            }

            return studentIDs;

            //var studentIDs = new List<long>();
            //using (var ctx = new MyDbContext())
            //{
            //    // GET STUDENTS LIST BY GRADE ID
            //    var sessionsList = (from s in ctx.UsersProfile where s.FKGradeID == gradeID select s).ToList();
            //    if (sessionsList.Count > 0)
            //    {
            //        foreach (var item in sessionsList)
            //        {
            //            if (!studentIDs.Contains((long)item.UsersProfileID))
            //            {
            //                studentIDs.Add((long)item.UsersProfileID);
            //            }
            //        }
            //    }

            //}

            //return studentIDs;
        }
        public List<long> GetStudentsBySubjectID(int subjectID)
        {
            var studentIDs = new List<long>();
            using (var ctx = new MyDbContext())
            {
                // GET STUDENTS LIST BY SUBJECT ID
                var sessionsList = (from s in ctx.Sessions where s.FKSubjectID == subjectID select s).ToList();
                if (sessionsList.Count > 0)
                {
                    foreach (var item in sessionsList)
                    {
                        if (!studentIDs.Contains((long)item.FKRequestedBy))
                        {
                            studentIDs.Add((long)item.FKRequestedBy);
                        }
                    }
                }

            }

            return studentIDs;
        }


        public List<long> GetStudentsByCity(string cityName)
        {
            var studentIDs = new List<long>();
            using (var ctx = new MyDbContext())
            {
                var userprofiles = (from up in ctx.UsersProfile
                                    where up.CityName.Contains(cityName)
                                    select up).ToList();
                if (userprofiles.Count > 0)
                {
                    foreach (var item in userprofiles)
                    {
                        if (!studentIDs.Contains((long)item.FKUserID))
                        {
                            studentIDs.Add((long)item.FKUserID);
                        }

                    }
                }

            }

            return studentIDs;
        }
        public List<UsersProfile> GetUserProfileByCity(string cityName)
        {
            var studentIDs = new List<long>();
            using (var ctx = new MyDbContext())
            {
                var userprofiles = (from up in ctx.UsersProfile
                                    where up.CityName.Contains(cityName)
                                    select up).ToList();

                return userprofiles;
            }

            return null;
        }
        public List<long> GetStudentsByStatus(int status)
        {
            var studentIds = new List<long>();
            using (var ctx = new MyDbContext())
            {
                var userprofiles = (from up in ctx.Users
                                    where up.UserStatus.Equals(status)
                                    select up).ToList();
                if (userprofiles.Count > 0)
                {
                    foreach (var item in userprofiles)
                    {
                        if (!studentIds.Contains((long)item.UserID))
                        {

                            studentIds.Add((long)item.UserID);
                        }
                    }
                }

            }
            return studentIds;
        }
        //public List<long> GetStudentsByStatus(int status)
        //{
        //    var studentIDs = new List<long>();
        //    using (var ctx = new MyDbContext())
        //    {
        //        var userprofiles = (from up in ctx.Users
        //                            where up.UserStatus.Equals(status )
        //                            select up).ToList();
        //        if (userprofiles.Count > 0)
        //        {
        //            foreach (var item in userprofiles)
        //            {
        //                if (!studentIDs.Contains((long)item.UserID))
        //                {
        //                    studentIDs.Add((long)item.UserID);
        //                }

        //            }
        //        }

        //    }

        //    return studentIDs;
        //}
        public List<Subjects> GetSubjectsList()
        {
            var subjects = new List<Subjects>();
            using (var ctx = new MyDbContext())
            {
                subjects = (from sub in ctx.Subjects select sub).ToList();
                if (subjects.Count > 0)
                {
                    return subjects;
                }
            }
            return subjects;
        }
        public List<string> GetAllCities()
        {
            var cityNames = new List<string>();

            using (var ctx = new MyDbContext())
            {
                var userprofiles = (from up in ctx.UsersProfile where up.CityName != null select up).ToList();
                if (userprofiles.Count > 0)
                {
                    foreach (var item in userprofiles)
                    {
                        if (!cityNames.Contains(item.CityName))
                        {
                            cityNames.Add(item.CityName);
                        }
                    }

                }
            }
            return cityNames;
        }
        public List<string> GetAllStatus()
        {
            var cityNames = new List<string>();

            using (var ctx = new MyDbContext())
            {
                var userprofiles = (from up in ctx.Users select up).ToList();
                if (userprofiles.Count > 0)
                {
                    foreach (var item in userprofiles)
                    {
                        if (!cityNames.Contains(item.UserStatus.ToString()))
                        {
                            cityNames.Add(item.UserStatus.ToString());
                        }
                    }

                }
            }
            return cityNames;
        }
        public int DeleteAllBlockReasonsOfUserByUserID(long userID)
        {
            using (var ctx = new MyDbContext())
            {
                try
                {
                    var blockReasons = (from BR in ctx.UserBlockReasons where BR.FKUserID == userID select BR).ToList();
                    if (blockReasons.Count > 0)
                    {
                        foreach (var item in blockReasons)
                        {
                            ctx.UserBlockReasons.Remove(item);
                            ctx.SaveChanges();
                        }
                    }

                    return (int)Enumerations.ResponsHelpers.Deleted;
                }
                catch (Exception ex)
                {
                    return (int)Enumerations.ResponsHelpers.Error;
                }

            }
        }

       
    }
}
