﻿using BLL.BusinessModels.AdminModels;
using DAL.MasterEntity;
using DAL.Entities;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL.BusinessManagers
{
    public static class EmailTemplatesManager
    {
        public static void UpdateEmailTemplate(EmailTemplatesModel templateModel)
        {
            try
            {
                var emailTemplate = new DAL.Entities.EmailTemplate();

                using (var ctx = new MyDbContext())
                {
                    Mapper.MappEmailTemplateToDB(templateModel, ref emailTemplate);


                    ctx.EmailTemplates.Attach(emailTemplate);
                    ctx.Entry(emailTemplate).State = EntityState.Modified;

                    ctx.SaveChanges();

                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        public static EmailTemplatesModel GetEmailTemplate(int? type)
        {
            var emailTemplateModel = new EmailTemplatesModel();

            try
            {
                using (var ctx = new MyDbContext())
                {
                    var emailTemplate = (from ET in ctx.EmailTemplates where ET.Type == type select ET).FirstOrDefault();
                    if (emailTemplate != null)
                    {
                        Mapper.MappEmailTemplateToModel(emailTemplate, ref emailTemplateModel);
                    }
                }

                return emailTemplateModel;
            }
            catch(Exception ex)
            {
                throw ex;
            }
           
        }
    }
}
