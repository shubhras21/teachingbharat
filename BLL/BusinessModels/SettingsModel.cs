﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL.BusinessModels
{
    public class SettingsModel
    {
        public Int64 MetaID { get; set; }

        public string Title { get; set; }
        public string Description { get; set; }
        public string KeyWords { get; set; }

        public bool IsEnabled { get; set; }
    }
}
