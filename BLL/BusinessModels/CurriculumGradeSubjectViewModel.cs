﻿using DAL.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL.BusinessModels
{
    public class CurriculumGradeSubjectViewModel
    {
        public IEnumerable<Curriculums> CurriculumsList { get; set; }
        public IEnumerable<Grades> GradesList { get; set; }
        public IEnumerable<Subjects> SubjectsList { get; set; }
    }
}
