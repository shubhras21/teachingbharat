﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace BLL.BusinessModels
{
    public class StudentReqSubModel
    {
        
        public Int64 Id { get; set; } 
        public string LookingFor { get; set; }

        
        public string Board_Target { get; set; }
        public int CurriculumId { get; set; }
        public int SubjectId { get; set; }
        public DateTime? StartDate { get; set; }
        public string Pref_Day { get; set; }
        public string Pref_StartTime { get; set; }
        public string Pref_EndTime { get; set; }
        public string Group_1To1 { get; set; }
        public string Remarks { get; set; }

        public Int64 CreatedBy { get; set; }
        public bool IsActive { get; set; }
        public IEnumerable<SelectListItem> ListofSubjects { get; set; }
        public IEnumerable<SelectListItem> ListofCurriculums { get; set; }

    }
}
