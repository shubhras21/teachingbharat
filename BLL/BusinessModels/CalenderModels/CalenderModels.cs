﻿using BLL.BusinessModels.TeacherModels;
using DAL.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL.BusinessModels.CalenderModels
{
    public class CalenderViewModel
    {        
        // list of Session Timings one date and multiple sessions
        public List<CalenderDateViewModel> ListOfSessionTimings { get; set; }

    }

    public class CalenderDateViewModel
    {
        public string Date { get; set; }
        public List<CalenderSessionsModel> Sessions { get; set; }
    } 
    public class CalenderSessionsModel
    {
        public string TopicName { get; set; }

        public string FromToTime { get; set; }

        public string TeacherName { get; set; }

        public string StudentName { get; set; }

        public long Teacher { get; set; }
    }
}
