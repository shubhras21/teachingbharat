﻿using BLL.BusinessModels.StudentModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL.BusinessModels.TeacherModels
{
   public class TeacherCourseHeaderModel
    {
        public Int64 CoursesHeaderID { get; set; }

        public string CourseTitle { get; set; }

        public string CourseDescription { get; set; }

        public double CourseFee { get; set; }

        public int TotalHours { get; set; }

        public int TotalSeats { get; set; }

        public string TrialVideoURL { get; set; }

        public DateTime? OfferedDate { get; set; }

        public string TimeSlot { get; set; }

        public DateTime? StartDate { get; set; }

        public DateTime? EndDate { get; set; }

        public Int64 FKCurriculumID { get; set; }

        public Int64 FKGradeID { get; set; }

        public Int64 FKSubjectID { get; set; }

        public Int64 FKOfferedBY { get; set; }

        public bool IsActive { get; set; }

        public CurriculumsModel curriculumViewModel { get; set; }
        public GradeViewModel gradeViewModel { get; set; }
        public SubjectModel subjectModel { get; set; }
    }
}
