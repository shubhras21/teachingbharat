﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace BLL.BusinessModels.TeacherModels
{
    public class TeacherEmailTemplateModel
    {
        public long TeacherID { get; set; }
        public string TeacherFullName { get; set; }
        public string LoginEmail { get; set; }
        public string Password { get; set; }
        public string body { get; set; }


        [AllowHtml]
       // [UIHint("tinymce_full")]
        public string bodyWrittenFromAdmin { get; set; }

    }
}
