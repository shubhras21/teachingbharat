﻿using BLL.BusinessModels.UserModels;
using DAL.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace BLL.BusinessModels.TeacherModels
{
    public class TeacherModel
    {
    }

    //Subject List View Model
    public class SubjectModel
    {
        public Int64 SubjectId { get; set; }

        public string SubjectName { get; set; }
    }

    //Qualification List View Model
    public class QualifiationModel
    {
        public Int64 QualificationId { get; set; }

        public string QualificationName { get; set; }
    }

    //Experience List View Model
    public class ExperienceModel
    {
        public Int64 ExperienceId { get; set; }

        public string ExperienceValue { get; set; }

    }

    //Grade List View Model
    public class GradeModel
    {
        public Int64 GradeId { get; set; }

        public string GradeName { get; set; }
    }

    //Cities List View Model
    public class CityModel
    {
        public Int64 CityId { get; set; }

        public string CityName { get; set; }
    }


    //Timing List View Model
    public class TimingModel
    {
        public Int64 TimingId { get; set; }

        public string FromTime { get; set; }

        public string ToTime { get; set; }

        public DateTime TimeSlot { get; set; }

    }



    public class DaysModel
    {
        public int DayOfWeek { get; set; }

        public string FullName { get; set; }

        public string HalfName { get; set; }
    }

    //List of Unviersity 
    public class UniversityModel
    {
        public Int64 UniversityId { get; set; }

        public string UniversityName { get; set; }
    }

    //List of Occupation
    public class OccupationModel
    {
        public Int64 OccupationId { get; set; }

        public string OccupationName { get; set; }
    }
    // List of Qualification
    public class QualicitationModel
    {
        public Int64 QualificationId { get; set; }

        public string QualificationName { get; set; }
    }


    //List of InternetConnection
    public class InternetConnectionModel
    {
        public Int64 InternetConnectionId { get; set; }

        public string InternetConnectionName { get; set; }
    }

    //List of Connection Speed 
    public class InternetConnectionSpeedModel
    {
        public Int64 InternetSpeedId { get; set; }

        public string InternetConnectionName { get; set; }
    }

    //List of Curriculum 
    public class CurriculamModel
    {
        public Int64 CurriculumId { get; set; }

        public string CurriculumName { get; set; }
    }


    //List of Countries
    public class Countries
    {
        public Int64 CountryId { get; set; }

        public string CountryName { get; set; }
    }

    //Teacher View Model Get Teacher in List Admin manager AND Finding Teacher SEARCH RECORD
    public class TeacherViewModel
    {
        public Int64 TeacherId { get; set; }

        public Int64 PersonId { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string ContactNumber { get; set; }

        public string EmailAddress { get; set; }

        public bool IsActive { get; set; }

        public int IsVerfied { get; set; }
        public int UserStatus { get; set; }

        public string TeacherExperience { get; set; }

        public string TeacherQualification { get; set; }

        public bool IsOnline { get; set; }
        public double earnings { get; set; }
        public double Rating { get; set; }
    }

    //List of Language View Model
    public class LanguageViewModel
    {
        public Int64 LanguageId { get; set; }

        public string LanguageName { get; set; }

    }

    //Frequency View Model .....
    public class FrequencyViewModel
    {
        public Int64 FrequencyId { get; set; }

        public string FrequencyName { get; set; }
    }

    //Finding Teacher View Model Student Manager
    public class FindTeacherViewModel
    {
        [Required(ErrorMessage = "Please Select Target")]
        public List<Int64> TargetId { get; set; }

        [Required(ErrorMessage = "Please Select Grade")]
        public List<Int64> GradeId { get; set; }

        [Required(ErrorMessage = "Please Select Subject")]
        public List<Int64> SubjectId { get; set; }

        [Required(ErrorMessage = "Please Select Frequency")]
        public List<Int64> FrequencyId { get; set; }

        [Required(ErrorMessage = "Please Select Target")]
        public Int64 CurriculumID { get; set; }

        [Required(ErrorMessage = "Please Select Grade")]
        public Int64 varGradeId { get; set; }

        [Required(ErrorMessage = "Please Select Subject")]
        public Int64 varSubjectId { get; set; }

        public List<CurriculamModel> ListofTargets { get; set; }

        public List<GradeModel> ListofGrades { get; set; }

        public List<SubjectModel> ListofSubjects { get; set; }

        public List<FrequencyViewModel> ListofFrequency { get; set; }

    }

    //Teacher DashBoard View Model All data related datashboard cames here
    public class TeacherDashboardViewModel
    {
        public Int64 TeacherID { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string InstituteName { get; set; }

        public string Qualification { get; set; }

        public string Experience { get; set; }

        public string StateName { get; set; }

        public string City { get; set; }

        public string ContactNumber { get; set; }

        public string Email { get; set; }

        public List<TeacherSubjects> listOfSubjects { get; set; }

        public List<TeachersAvailability> listOfTeachersAvailability { get; set; }

        public List<DateViewModel> listOfDates { get; set; }

        public List<Sessions> listOfSession { get; set; }

        public List<Sessions> listOfUpcomingSession { get; set; }

        public List<WeeklyReportViewModel> listOfWeeklyReport { get; set; }

        public List<UsersProfile> listOfMyStudents { get; set; }

        public List<UserNotifications> listOfMyNotifications{ get; set; }

        public int MyStudentsCount { get; set; }

        public int MySessionsCount { get; set; }

        public double MyTotalEarnings { get; set; }

        public string ProfileImage { get; set; }

        public MyProfileViewModel MyProfileViewModel { get; set; }

        public double SessionCost { get; set; }

        public UsersProfile UserProfile { get; set; }

        public RatingsViewModel RatingsModel { get; set; }

        public int CourseCount { get; set; }
    }

    public class DateViewModel
    {
        public string Date { get; set; }
        public List<string> Time { get; set; }
        public List<DateTime> RealTime { get; set; }
    }


    //Teacher Profile View Model
    public class TeacherProfileViewModel
    {
        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string InstituteName { get; set; }

        public string Qualification { get; set; }

        public string Experience { get; set; }

        public string AboutMe { get; set; }

        public string StateName { get; set; }

        public byte[] ProfileImage { get; set; }

        public List<SubjectModel> SubjectList { get; set; }

        public List<LanguageViewModel> LanguageList { get; set; }

    }

    //Teacher Change Password ViewModel 
    public class TeacherChangePasswordViewModel
    {
        [Required(ErrorMessage = "Enter Password")]
        [DataType(DataType.Password)]
        [DisplayName("Password")]
        public string Password { get; set; }

        [Required(ErrorMessage = "Enter Confirm Password")]
        [DataType(DataType.Password)]
        [DisplayName("Confirm Password")]
        [System.ComponentModel.DataAnnotations.Compare("Password", ErrorMessage = "The password and confirmation password do not match.")]
        public string ConfirmPassword { get; set; }
    }

    //uni, Experience, rate, From, speaks, contact, image.
    public class TeacherProfileUpdateViewModel
    {

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string Qualification { get; set; }

        public string InstituteName { get; set; }

        public string Experience { get; set; }

        public string Rate { get; set; }

        public string CityName { get; set; }

        public string AboutMe { get; set; }

        public List<Int64> LanguageId { get; set; }

        public string ContactNumber { get; set; }

    }


    public class TeachersAvailabilityViewModel
    {

        public Int64 TAID { get; set; }

        [Required(ErrorMessage = "Please Select Date Time")]
        public DateTime? AvailableTime { get; set; }

        public Int64 FKTeacherID { get; set; }

        public bool IsActive { get; set; }

    }

    public class FullCalender
    {

        public string title { get; set; }

        public string start { get; set; }

        public string end { get; set; }

        public string url { get; set; }

        public bool allDay { get; set; }

        public int slotMinutes { get; set; }
    }

    public class TeacherSetAvailabilityModel
    {
        public string TopHeaderDate { get; set; }
        public List<string> LeftHeaderTime { get; set; }
        public List<DateTime> RealTime { get; set; }
        public List<DateTime> TeacherOldAvailableTimes { get; set; }

    }

    public class CreateTestViewModel
    {
        public Int64 TestID { get; set; }

        [Required(ErrorMessage = "Please Select Grade")]
        public Int64 FKGradeID { get; set; }

        [Required(ErrorMessage = "Please Select Subject")]
        public Int64 FKSubjectID { get; set; }

        public Int64 FKCreatedBy { get; set; }

        [RegularExpression("([1-9][0-9]*)", ErrorMessage = "Please enter valid Number")]
        [Range(0, int.MaxValue, ErrorMessage = "Please enter valid integer Number")]
        [Required(ErrorMessage = "Select Count")]
        public int EasyCount { get; set; }

        [RegularExpression("([1-9][0-9]*)", ErrorMessage = "Please enter valid Number")]
        [Range(0, int.MaxValue, ErrorMessage = "Please enter valid integer Number")]
        [Required(ErrorMessage = "Select Count")]
        public int MediumCount { get; set; }

        [RegularExpression("([1-9][0-9]*)", ErrorMessage = "Please enter valid Number")]
        [Range(0, int.MaxValue, ErrorMessage = "Please enter valid integer Number")]
        [Required(ErrorMessage = "Select Count")]
        public int HardCount { get; set; }

        //[RegularExpression("([100]*)", ErrorMessage = "Please enter valid Number")]    
        public int OverallPercentage { get; set; }

        public int TestLevel { get; set; }

        //[RegularExpression("([1-9][0-9]*)", ErrorMessage = "Please enter valid Integer Number")]
        //[Range(0, int.MaxValue, ErrorMessage = "Please enter valid integer Number")]
        //[Required(ErrorMessage = "Please enter total scores")]
        public int TotalScores { get; set; }

        [Required(ErrorMessage = "Please enter Test Name")]
        public string TestName { get; set; }

        public int TotalQuestions { get; set; }

        public int TestDuration { get; set; }

        [Required(ErrorMessage = "Please Select Topic")]
        public Int64[] FKTopicID { get; set; }


        public IEnumerable<SelectListItem> ListofGrades { get; set; }

        public IEnumerable<SelectListItem> ListofSubjects { get; set; }

        public IEnumerable<SelectListItem> ListofTopics { get; set; }

        //public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        //{
        //    if (true)
        //        yield return new ValidationResult("Invalid Date! : IValidatableObject", new[] { "valideDate" });
        //}

    }

    public class QuestionPaperViewModel
    {
        public Int64 UserID { get; set; }
        public Int64 TestID { get; set; }

        public string TestName { get; set; }

        public string GradeSubjectName { get; set; }

        public List<TestCoveredTopics> listOfTopics { get; set; }

        public string TestLevel { get; set; }

        public string TestDuration { get; set; }

        public string TotalQuestions { get; set; }

        public string TotalMarks { get; set; }

        public List<TestQuestions> listOfQuestion { get; set; }

       

    }


    public class TestAssignViewModel
    {
        public Int64 UserID { get; set; }

        public Int64 TestID { get; set; }

        public string TestName { get; set; }

        public string GradeSubjectName { get; set; }

        public List<TestCoveredTopics> listOfTopics { get; set; }

        public string TestLevel { get; set; }

        public string TestDuration { get; set; }

        public string TotalQuestions { get; set; }

        public string TotalMarks { get; set; }

        public int TotalAssignedStudents { get; set; }

        public List<UsersProfile> listOfStudents { get; set; }

        public IEnumerable<SelectListItem> ListofMyStudents { get; set; }

        [Required(ErrorMessage = "Please Select student")]
        public List<Int64> FKStudentID { get; set; }

        [Required(ErrorMessage = "Please Select Date")]
        [DataType(DataType.Date)]
        //[Remote("CheckTime", "Teacher", ErrorMessage = "Please select valid date")]
        public DateTime TestDate { get; set; }

        [Required(ErrorMessage = "Please Select Time")]
        [DataType(DataType.Time)]
        //[Remote("CheckTime", "Teacher", ErrorMessage = "Please select valid date")]
        public DateTime TestTime{ get; set; }

        public Int64 FKAssignedBy { get; set; }

        public Int64 FKAssignedTo { get; set; }

        public DateTime TestHappenDate { get; set; }


        public List<TestAssigned> listOfAssignedStudents { get; set; }

        public int TotalPass { get; set; }

        public int TotalFail { get; set; }

        public int TotalPending { get; set; }

        public int TotalStudents { get; set; }

        public Int64 ReAssignedUserID { get; set; }

        public List<CalculateRankViewModel> RankViewModel { get; set; }

    }

    public class AssignedTestViewModel
    {

    }

    public class StudentsTestListViewModel
    {
        public List<TestAssigned> listOfUpcomingTests { get; set; }
        public List<TestAssigned> listOfAllTests { get; set; }
    }

    public class TestQuestionViewModel
    {

        public Int64 TestAssignID { get; set; }

        public Int64 TestID { get; set; }

        public Int64 UserID { get; set; }

        public QuestionsBank QuestionBank { get; set; }

        [Required(ErrorMessage = "Please Select Answer")]
        public int StudentMarkedAnswer { get; set; }

        public bool IsSkipped { get; set; }

        public DateTime? QuestionStartTime { get; set; }

        public DateTime? QuestionEndTime { get; set; }

        public int index { get; set; }

    }



    public class AnswerSheetViewModel
    {


        public Int64 UserID { get; set; }

        public Int64 TestID { get; set; }

        public string TestName { get; set; }

        public string GradeSubjectName { get; set; }

        public List<StudentTestAnswers> listOfStudentTestAnswers { get; set; }

        public string TestLevel { get; set; }

        public string TestDuration { get; set; }

        public string TotalQuestions { get; set; }

        public string TotalMarks { get; set; }

        public int MarksObtained { get; set; }

        public DateTime TestHappenDate { get; set; }

        public DateTime TestAssignDate { get; set; }

        public string PassFailedStatus { get; set; }

        public int WrongCount { get; set; }
  
        public int RightCount { get; set; }

        public int SkippedCount { get; set; }

        public string StudentName { get; set; }

        public double TotalSpendTime { get; set; }

        public UsersProfile UserProfile { get; set; }

        public Test Test { get; set; }

        public string TeacherName { get; set; }

        public double FastCountPercentage { get; set; }

        public double SlowCountPercentage { get; set; }

        public float FastCorrect { get; set; }
        public float SlowCorrect { get; set; }
        public float FastIncorrect { get; set; }
        public float SlowIncorrect { get; set; }

        public int TotalAssignedStudent { get; set; }
        public int IndividualRank { get; set; }


        public Int64 TAID { get; set; }

        public Int64 SID { get; set; }
    }


    public class CalculateRankViewModel
    {
        public int ObtainedMarks { get; set; }

        public Int64 UserID { get; set; }

        public int Index { get; set; }

    }

    public class RatingsViewModel
    {

        public List<RatingReviews> ReviewsList { get; set; }

        public double RatingAverage{get;set;}

        public Int64 TeacherID { get; set; }

        public int TotalRatings { get; set; }
    }

    public class PublicRatingsViewModel
    {

        public List<RatingReviews> ReviewsList { get; set; }

        public double RatingAverage { get; set; }


        public int TeachersCount { get; set; }
        public int SessionsCount { get; set; }
        public int UsersCount { get; set; }

        public int TotalRatings { get; set; }
    }

    public class RatingInsertionModel
    {

        public Int64 TeacherID { get; set; }

        public Int64 StudentID { get; set; }

        [Required(ErrorMessage = "Please Select Stars")]
        public int RatingStars { get; set; }

        [Required(ErrorMessage = "Please Enter Comments")]
        public string Comment { get; set; }

        public IEnumerable<SelectListItem> ListofStars { get; set; }
    }


    public class PublicRatingInsertionModel
    {

        [Required(ErrorMessage = "Please Enter Your Name")]
        public string Name { get; set; }

        [Required(ErrorMessage = "Please Enter Your Email")]
        [RegularExpression(@"^([a-zA-Z0-9_\-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$", ErrorMessage = "Please enter a valid e-mail adress")]
        public string Email { get; set; }

        [Required(ErrorMessage = "Please Select Stars")]
        public int RatingStars { get; set; }

        [Required(ErrorMessage = "Please Enter Comments")]
        public string Comment { get; set; }

        public DateTime CraetedTime { get; set; }

        public int TotalTeachers { get; set; }

        public Int64 UserID { get; set; }

        public bool IsActive  { get; set; }
        public string createdBy { get; set; }
        public bool isApproved { get; set; }

        public IEnumerable<SelectListItem> ListofStars { get; set; }

    }


    public class AddCourseViewModel
    {
       
        public Int64 CoursesHeaderID { get; set; }

        [Required(ErrorMessage = "Please Enter Title")]
        public string CourseTitle { get; set; }

        [Required(ErrorMessage = "Please Enter Course Description")]
        public string CourseDescription { get; set; }

        [Required(ErrorMessage = "Please Enter Course Fee")]
        [RegularExpression("([1-9][0-9]*)", ErrorMessage = "Please enter valid Fee")]
        public double CourseFee { get; set; }

        [RegularExpression("([1-9][0-9]*)", ErrorMessage = "Please enter valid Hours")]
        [Required(ErrorMessage = "Please Enter Course Fee")]
        public int TotalHours { get; set; }

        [RegularExpression("([1-9][0-9]*)", ErrorMessage = "Please enter valid Hours")]
        [Range(0, 5, ErrorMessage = "Max 5 seats are allowed")]
        [Required(ErrorMessage = "Please Enter Course Fee")]
        public int TotalSeats { get; set; }

        [Url(ErrorMessage = "Please Enter Valid url")]
        public string TrialVideoURL { get; set; }

        public DateTime? OfferedDate { get; set; }

        [Required(ErrorMessage = "Please select Time Slot")]
        public string TimeSlot { get; set; }


        [Required(ErrorMessage = "Please Select Days")]
        public List<int> Days{ get; set; }

        //[CheckBoxRequired(ErrorMessage = "Please Select Days.")]
        [Required(ErrorMessage = "Please select Time Slot")]
        public bool Dayss { get; set; }

        // [Required(ErrorMessage = "Please select Time Slot")]
        // public List<string> ListOfTimeSlot { get; set; }

        [Required(ErrorMessage = "Please select Start Date")]
        public DateTime StartDate { get; set; }
    
        public DateTime? EndDate { get; set; }

        [Required(ErrorMessage = "Please Select Subject")]
        public Int64 FKSubjectID { get; set; }

        [Required(ErrorMessage = "Please Select Available Hours")]
        public int AvailableHours { get; set; }

        public Int64 FKOfferedBy { get; set; }

        [Required(ErrorMessage = "Please select when classes will conduct")]
        public List<int> ClassesConductedOn { get; set; }

        public IEnumerable<SelectListItem> ListofTeacherSubjects { get; set; }

        public List<TimingModel> ListofavailableTimes { get; set; }

        public List<DaysModel> ListofDays { get; set; }

       // public Int64 FKGradeID { get; set; }

     //   public Int64 FKCurriculumID { get; set; }

    }


    public class AddResourceViewModel
    {

        public Int64 ResourceID { get; set; }

        [Required(ErrorMessage = "Please Enter Title")]
        public string ResourceTitle { get; set; }

        [Required(ErrorMessage = "Please Enter Resource Description")]
        public string ResourceDescription { get; set; }

        [Required(ErrorMessage = "Please Select Topic")]
        public Int64 FKTopicID { get; set; }

        [Url(ErrorMessage = "Please Enter Valid url")]
        [Required(ErrorMessage = "Please Enter URL")]
        public string VideoURL { get; set; }
      

        [Required(ErrorMessage = "Please Select Subject")]
        public Int64 FKSubjectID { get; set; }

        [Required(ErrorMessage = "Please Select Year")]
        public int ResourceYear { get; set; }

        public string TopicOptional { get; set; }

        public Int64 FKOfferedBy { get; set; }

        public IEnumerable<SelectListItem> ListofTeacherSubjects { get; set; }

        public IEnumerable<SelectListItem> ListofSubjectTopics { get; set; }

    }

    public class AddNotesViewModel
    {

        public Int64 NoteID { get; set; }

        [Required(ErrorMessage = "Please Enter Title")]
        public string NotesTitle { get; set; }

        [Required(ErrorMessage = "Please Enter File Description")]
        public string NotesDescription { get; set; }

        [Required(ErrorMessage = "Please Select Topic")]
        public Int64 FKTopicID { get; set; }

        [Required(ErrorMessage = "Please Select Subject")]
        public Int64 FKSubjectID { get; set; }

        [Required(ErrorMessage = "Please Select File")]
        public string FileName { get; set; }

        public string TopicOptional { get; set; }

        public Int64 FolderID { get; set; }

        public Int64 FKOfferedBy { get; set; }

        public IEnumerable<SelectListItem> ListofTeacherSubjects { get; set; }

        public IEnumerable<SelectListItem> ListofSubjectTopics { get; set; }

    }

    public class AddFolderViewModel
    {

        [Required(ErrorMessage = "Please Enter Folder Name")]
        public string FolderName { get; set; }

        public Int64 FKCreatedBy { get; set; }

    }

    public class FreeResourcesFolder
    {

        public string FolderName { get; set; }

        public Int64 SubjectID { get; set; }

        public int TotalVideos { get; set; }

        public string FromToYear { get; set; }

        public string ClassName { get; set; }

        public string SubjectName { get; set; }


    } 


    public class CourseClashTest
    {
        public Int64 InsertStatus { get; set; }

        public string CourseName { get; set; }
    }


    public class CourseJoinDetails
    {

        public CoursesHeader CourseHeader { get; set; }

        public UpcomingCourses UpcomingCourses { get; set; }

        public int ClassesTaken { get; set; }

        public int ClassesSheduled { get; set; }

        public int ClassesRemained { get; set; }

        public string CourseLink { get; set; }


    }

    public class UpcomingCourses
    {

        public List<CourseSchedule> JoinSchedule { get; set; }

        public List<CourseSchedule> UpcomingSchedules { get; set; }

    }


    public class MySessions
    {

        public List<Sessions> listOfPendingSession { get; set; }

        public List<Sessions> listOfUpcomingSession { get; set; }

        public List<Sessions> listOfCancelSession { get; set; }

        public List<Sessions> listOfJoinSession { get; set; }

        public List<Sessions> listOfPastSession { get; set; }


    }

    public class ShareEmailViewModel
    {

        public Int64 NoteID { get; set; }

        [Required(ErrorMessage = "Please Enter Your Email")]
        [RegularExpression(@"^([a-zA-Z0-9_\-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$", ErrorMessage = "Please enter a valid e-mail adress")]
        public string Email { get; set; }

        [Required(ErrorMessage = "Please Enter Your message")]
        public string Message { get; set; }

    }



    public class Notifications
    {

        public string ActionPerformed { get; set; }

        public Int64 FKUserID { get; set; }

        public Int64 FKSessionID { get; set; }

        public int IsCourse { get; set; }

    }



    public class CheckBoxRequired : ValidationAttribute, IClientValidatable
    {
        public override bool IsValid(object value)
        {
            if (value is bool)
        {
                return (bool)value;
            }

            return false;
        }

        public IEnumerable<ModelClientValidationRule> GetClientValidationRules(ModelMetadata metadata, ControllerContext context)
        {
            yield return new ModelClientValidationRule
            {
                ErrorMessage = FormatErrorMessage(metadata.GetDisplayName()),
                ValidationType = "checkboxrequired"
            };
        }




    }

}
