﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL.BusinessModels.TeacherModels
{
  public  class TeacherQualificationModel
    {
        public Int64 TeacherQualificationID { get; set; }

        public DateTime? StartDate { get; set; }

        public DateTime? EndDate { get; set; }

        public bool IsActive { get; set; }

        public Int64 FKUserID { get; set; }

        public string QualificationName { get; set; }

        public string UniversityName { get; set; }
    }
}
