﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL.BusinessModels.TeacherModels
{
   public class TeacherAvailableHoursModel
    {
        public Int64 FKUserID { get; set; }

       
        public Int64 FKTimeID { get; set; }

       
        public Int64 FKDayID { get; set; }

        public bool IsActive { get; set; }

        public TimingModel timingModel { get; set; }
        public DaysModel dayModel { get; set; }
    }
}
