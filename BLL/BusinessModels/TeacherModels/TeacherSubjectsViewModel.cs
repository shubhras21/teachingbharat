﻿using BLL.BusinessModels.StudentModels;
using BLL.BusinessModels.TeacherModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL.BusinessModels.TeacherModels
{
  public  class TeacherSubjectsViewModel
    {
        public Int64 TeacherSubjectsID { get; set; }

        public Int64 FKUserID { get; set; }

        public Int64 FKCurriculumID { get; set; }

        public Int64 FKSubjectID { get; set; }

        public Int64 FKGradeID { get; set; }

        public int Type { get; set; }

        public bool IsActive { get; set; }

        public CurriculumsModel curriculumModel { get; set; }
        public SubjectModel subjectModel { get; set; }
        public GradeViewModel gradeModel { get; set; }
    }
}
