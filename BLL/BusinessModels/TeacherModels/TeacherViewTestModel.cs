﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using DAL.Entities;

namespace BLL.BusinessModels.TeacherModels
{
    public class TeacherViewTestModel
    {
        public TestDetails TestDetails { get; set; }
        public QuestionsBank Question { get; set; }
        public IEnumerable<SelectListItem> ListOfAnswers { get; set; }
        public List<UsersProfile> StudentList { get; set; }
        public List<QuestionsBank> Questions { get; set; }

    }
    public class Questions
    {
        public string Question { get; set; }
        public string Option1 { get; set; }
        public string Option2 { get; set; }
        public string Option3 { get; set; }
        public string Option4 { get; set; }
    }
    public class TestDetails
    {
        public int TestID { get; set; }
        public string TestName { get; set; }
        public string GradeName { get; set; }
        public string SubjectName { get; set; }
        public string TotalMarks { get; set; }
        public string PassingMarks { get; set; }
        public string TestDuration { get; set; }
        public string TotalQuestions { get; set; }
        public string Skipable { get; set; }
        public string TestLevel { get; set; }
        public string RemarksStudent { get; set; }
        public string RemarksTeacher { get; set; }
    }
}
