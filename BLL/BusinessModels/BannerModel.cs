﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace BLL.BusinessModels
{
   public  class BannerModel
    {
        public int ID { get; set; }

        public string Link { get; set; }
        public string Title { get; set; }
        public  HttpPostedFileBase image { get; set; } 
        public bool Status { get; set; }
    }
}
