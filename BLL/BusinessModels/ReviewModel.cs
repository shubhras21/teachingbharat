﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL.BusinessModels
{
    public class ReviewModel
    {
        public int ratingID { get; set; }
        public int techerID { get; set; }
        public int studentID { get; set; }
        public DateTime createdDate { get; set; }
        public double ratings { get; set; }
        public string Comments { get; set; }
        public string CreatedBy { get; set; }
        public string StudentName { get; set; }
        public string TeacherName { get; set; }
        public bool IsActive { get; set; }
        public bool IsApproved { get; set; }
    }
}
