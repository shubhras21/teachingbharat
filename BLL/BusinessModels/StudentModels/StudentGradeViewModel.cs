﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL.BusinessModels.StudentModels
{
  public  class StudentGradeViewModel
    {
        public Int64 StudentGradeID { get; set; }

        public string GradeName { get; set; }

        public Int64 FKUserID { get; set; }
    }
}
