﻿using BLL.BusinessModels.TeacherModels;
using BLL.BusinessModels.UserModels;
using DAL.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace BLL.BusinessModels.StudentModels
{
   public class BookSessionModel
    {

        public string CurriculumName { get; set; }

        public string GradeName { get; set; }

        public string SubjectName { get; set; }

        public string TeacherName { get; set; }

        [Required(ErrorMessage = "Please Enter Topic Name")]
        public string TopicName { get; set; }

        public DateTime SelectedTime { get; set; }

        [Required(ErrorMessage ="Please Select Curriculum")]
        public Int64 CurriculumID{ get; set; }

        [Required(ErrorMessage = "Please Select Grade")]
        public Int64 GradeID { get; set; }

        [Required(ErrorMessage = "Please Select Subject")]
        public Int64 SubjectID { get; set; }

        [Required(ErrorMessage = "Please Select Time Slot")]
        public DateTime TimeSlot { get; set; }

        public Int64 TeacherID { get; set; }

        public int SessionType { get; set; }

        // select lists
        public IEnumerable<SelectListItem> sListofCurriculums { get; set; }

        public IEnumerable<SelectListItem> sListofGrades { get; set; }

        public IEnumerable<SelectListItem> sListofSubjects { get; set; }

        // list of teacher timings
        public List<DateViewModel> ListOfTeacherTimes { get; set; }

        public double SessionCost { get; set; }

        public string Location { get; set; }

        public string ProfilePic { get; set; }

        public string ContactNumber { get; set; }

    }


    //Student DashBoard View Model All data related datashboard cames here
    public class StudentDashboardViewModel
    {
        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string StateName { get; set; }

        public string City { get; set; }

        public string ContactNumber { get; set; }

        public string Email { get; set; }

        public List<DateViewModel> listOfDates { get; set; }

        public List<Sessions> listOfSession { get; set; }

        public List<Sessions> listOfUpcomingSession { get; set; }

        public List<WeeklyReportViewModel> listOfWeeklyReport { get; set; }

        public List<UserActivity> listOfUserActivities { get; set; }

        public List<UserNotifications> listOfMyNotifications { get; set; }

        public string ProfileImage { get; set; }


    }





}
