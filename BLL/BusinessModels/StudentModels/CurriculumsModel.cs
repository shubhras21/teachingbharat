﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL.BusinessModels.StudentModels
{
   public class CurriculumsModel
    {
        public long  CurriculumID { get; set; }
        public string CurriculumName { get; set; }
        public bool IsActive { get; set; }
    }
}
