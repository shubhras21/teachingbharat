﻿using BLL.BusinessModels.AdminModels;
using BLL.BusinessModels.TeacherModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL.BusinessModels.StudentModels
{
   public class SessionViewModel
    {
        public Int64 TrialSessionID { get; set; }

        public string SessionName { get; set; }

        public string TopicName { get; set; }

        public DateTime? StartDate { get; set; }

        public DateTime? EndDate { get; set; }

        public DateTime RequestDate { get; set; }

        public DateTime TrialHappenDate { get; set; }

        public Int64 FKCurriculumID { get; set; }

        public Int64 FKGradeID { get; set; }

        public Int64 FKSubjectID { get; set; }

        public Int64? FKRequestedBy { get; set; }

        public Int64? FKRequestedTo { get; set; }

        public int SessionType { get; set; }

        public int RequestStatus { get; set; }

        public string SessionLink { get; set; }

        public double? SessionCost { get; set; }

        public int CurrentStatus { get; set; }

        public CurriculumsModel CurriculumModel { get; set; }
        public GradeViewModel GradeModel { get; set; }
        public SubjectModel SubjectModel { get; set; }
        public UserViewModel userModel { get; set; }
    }
}
