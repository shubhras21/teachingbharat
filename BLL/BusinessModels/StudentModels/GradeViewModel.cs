﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL.BusinessModels.StudentModels
{
  public  class GradeViewModel
    {
        public long GradeViewModelID { get; set; }
        public string GradeName { get; set; }
        public bool IsActive { get; set; }
        public Int64 FKCurriculumID { get; set; }
        public virtual CurriculumsModel Curriculum { get; set; }
    }
}
