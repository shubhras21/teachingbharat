﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace BLL.BusinessModels.StudentModels
{
   public class StudentEmailTemplateModel
    {

        public long StudentID { get; set; }

        public string StudentFullName { get; set; }
        public string Url { get; set; }


        [AllowHtml]
        public string emailBodyWrittenByAdmin { get; set; }

        public string emailBody { get; set; }

        public int? Type { get; set; }
    }
}
