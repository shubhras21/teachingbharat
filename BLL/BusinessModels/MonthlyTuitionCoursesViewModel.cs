﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DAL.Entities;

namespace BLL.BusinessModels
{
    public class MonthlyTuitionCoursesViewModel
    {
        public IEnumerable<CoursesHeader> courseList { get; set; }
    }
}
