﻿using DAL.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL.BusinessModels
{
    public class HomeViewModel
    {
        public IEnumerable<RatingReviews> ReviewList { get; set; }
        public IEnumerable<BannerSlides> SlideList { get; set; }

    }
}
