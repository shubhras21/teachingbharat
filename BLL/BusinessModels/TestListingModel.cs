﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL.BusinessModels
{
    public class TestListingModel
    {
        public int TestID { get; set; }
        public string GradeName { get; set; }
        public string SubjectName { get; set; }
        public string TestName { get; set; }
        public string CreatedDate { get; set; }
        public string ModifiedDate { get; set; }
        public string Assigned { get; set; }
        public int TestLevel { get; set; }
        public string RemarksStudent { get; set; }
        public string RemarksTeacher { get; set; }

    }
}
