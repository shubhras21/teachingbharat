﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL.BusinessModels.NotificationModel
{
    public class NotificationModel
    {
        public int NotificationID { get; set; }
        public string Comments { get; set; }
        public string UserID { get; set; }
        public string ReviewID { get; set; }
        public DateTime NotificationDateTime { get; set; }
        public bool IsSeen { get; set; }
    }
}
