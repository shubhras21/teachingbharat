﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL.BusinessModels.AdminModels
{
   public class UserActivitiesModel
    {
        public Int64 ActivityID { get; set; }

        public string ActionPerformed { get; set; }

        public Int64 FKUserID { get; set; }

        public DateTime Time { get; set; }

        public bool IsActive { get; set; }
        public int ActivitySource { get; set; }

        public UserViewModel userModel { get; set; }
    }
}
