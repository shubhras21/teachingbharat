﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace BLL.BusinessModels.AdminModels
{
    #region [Curriculums, Grades and Subjects, Topics Related View Models]

    public class CurriculumsViewModel
    {
        [Required(ErrorMessage = "Enter Curriculum/Board Name")]
        [DisplayName("Curriculum/Board")]
        public string CurriculumName { get; set; }

    }

    public class GradesViewModel
    {
        [Required(ErrorMessage = "Enter Grade Name")]
        [DisplayName("Grade Name")]
        public string GradeName { get; set; }

        [Required(ErrorMessage = "Please Select Curriculum")]
        [DisplayName("Curriculum Name")]
        public Int64 CurriculumID { get; set; }

        // Dropdowns List
        public IEnumerable<SelectListItem> ListOfCurriculums { get; set; }

    }

    public class SubjectViewModel
    {

        [Required(ErrorMessage = "Please Select Curriculum")]
        [DisplayName("Curriculum Name")]
        public Int64 CurriculumID { get; set; }

        [Required(ErrorMessage = "Please Select Grade")]
        [DisplayName("Grade Name")]
        public Int64 GradeID { get; set; }

        [Required(ErrorMessage = "Enter Subject Name")]
        [DisplayName("Subject Name")]
        public string SubjectName { get; set; }

        [Required(ErrorMessage = "Enter Subject Description")]
        [DisplayName("Subject Name")]
        public string SubjectDescription { get; set; }

        // Dropdowns List
        public IEnumerable<SelectListItem> ListOfCurriculums { get; set; }
        public IEnumerable<SelectListItem> ListOfGrades { get; set; }

    }

    public class TopicsViewModel
    {

        //[Required(ErrorMessage = "Please Select Curriculum")]
        [DisplayName("Curriculum Name")]
        public Int64 CurriculumID { get; set; }

        //[Required(ErrorMessage = "Please Select Grade")]
        [DisplayName("Grade Name")]
        public Int64 GradeID { get; set; }

//        [Required(ErrorMessage = "Enter Select Name")]
        [DisplayName("Subject Name")]
        public Int64 SubjectID { get; set; }

        [Required(ErrorMessage = "Enter Topic Name")]
        [DisplayName("Topic Name")]
        public string TopicName { get; set; }

        [Required(ErrorMessage = "Enter Topic Description")]
        [DisplayName("Topic Description")]
        public string TopicDescription { get; set; }

        // Dropdowns List
        public IEnumerable<SelectListItem> ListOfCurriculums { get; set; }
        public IEnumerable<SelectListItem> ListOfGrades { get; set; }
        public IEnumerable<SelectListItem> ListOfSubjects { get; set; }

    }




    public class SubjectFAQViewModel
    {
        public Int64 SubjectID { get; set; }

        [Required(ErrorMessage = "Enter Question")]
        [DisplayName("Question")]
        public string Question { get; set; }

        [Required(ErrorMessage = "Enter Answer")]
        [DisplayName("Answer")]
        public string Answer { get; set; }

        public Int64 QuestionBy { get; set; }
    }



    public class QuestionsBankViewModel
    {

        [Required(ErrorMessage = "Please Select Curriculum")]
        [DisplayName("Curriculum Name")]
        public Int64 CurriculumID { get; set; }

        [Required(ErrorMessage = "Please Select Grade")]
        [DisplayName("Grade Name")]
        public Int64 GradeID { get; set; }

        [Required(ErrorMessage = "Please Select Subject")]
        [DisplayName("Subject Name")]
        public Int64 SubjectID { get; set; }

        [Required(ErrorMessage = "Please Select Topic")]
        [DisplayName("Topic Name")]
        public Int64 TopicID { get; set; }

        [Required(ErrorMessage = "Please Select Question Level")]
        [DisplayName("Question Level")]
        public int QuestionLevel { get; set; }

        [Required(ErrorMessage = "Enter Question")]
        [DisplayName("Question")]
        public string Question { get; set; }

        [Required(ErrorMessage = "Enter First Option")]
        [DisplayName("First Option")]
        public string FirstOption { get; set; }

        [Required(ErrorMessage = "Enter Second Option")]
        [DisplayName("Second Option")]
        public string SecondOption { get; set; }

        [Required(ErrorMessage = "Enter Third Option")]
        [DisplayName("Third Option")]
        public string ThirdOption { get; set; }

        [Required(ErrorMessage = "Enter Fourth Option")]
        [DisplayName("Fourth Option")]
        public string FourthOption { get; set; }

        [Required(ErrorMessage = "Please Select Answer")]
        [DisplayName("Answer")]
        public int AnswerKey { get; set; }


        // Dropdowns List
        public IEnumerable<SelectListItem> ListOfCurriculums { get; set; }
        public IEnumerable<SelectListItem> ListOfGrades { get; set; }
        public IEnumerable<SelectListItem> ListOfSubjects { get; set; }
        public IEnumerable<SelectListItem> ListOfTopics { get; set; }
        public IEnumerable<SelectListItem> ListOfQuestionLevel { get; set; }
        public IEnumerable<SelectListItem> ListOfAnswers { get; set; }


    }


    public class ChangeFiltersViewModel
    {

        [Required(ErrorMessage = "Please Select Curriculum")]
        [DisplayName("Curriculum Name")]
        public Int64 CurriculumID { get; set; }

        [Required(ErrorMessage = "Please Select Grade")]
        [DisplayName("Grade Name")]
        public Int64 GradeID { get; set; }

        [Required(ErrorMessage = "Please Select Subject")]
        [DisplayName("Subject Name")]
        public Int64 SubjectID { get; set; }


        // Dropdowns List
        public IEnumerable<SelectListItem> ListOfCurriculums { get; set; }
        public IEnumerable<SelectListItem> ListOfGrades { get; set; }
        public IEnumerable<SelectListItem> ListOfSubjects { get; set; }

    }

    #endregion
}
