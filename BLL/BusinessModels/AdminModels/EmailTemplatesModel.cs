﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace BLL.BusinessModels.AdminModels
{
   public class EmailTemplatesModel
    {

       public int EmailTemplatesModelID { get; set; }

        public string Receipents { get; set; }

        [AllowHtml]
        [UIHint("tinymce_full")]
        public string Body { get; set; }

        public string Subject { get; set; }

        public int Type { get; set; }
    }
}
