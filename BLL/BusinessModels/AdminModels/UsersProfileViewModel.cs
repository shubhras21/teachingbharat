﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL.BusinessModels.AdminModels
{
   public class UsersProfileViewModel
    {
        public Int64 UsersProfileID { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string ContactNumber { get; set; }

        public string AlternateNumber { get; set; }

        public string ProfilePic { get; set; }

        public string HeardAboutRudra { get; set; }

        public byte[] Resume { get; set; }

        public string AboutMe { get; set; }

        public DateTime? DateOfBirth { get; set; }

        public DateTime? CreateDate { get; set; }

        public DateTime? UpdatedDate { get; set; }

        public int WeekHours { get; set; }

        public int WeekendHours { get; set; }

        public string CityName { get; set; }
        public string ExperienceName { get; set; }

        // Bool variables

        public int IsPen { get; set; }

        public bool IsWebcam { get; set; }

        public int IsInternetSpeed { get; set; }

        public double SessionCost { get; set; }

        public long? FKStateId { get; set; }
        public long FKUserId { get; set; }
        public long? FKOccupationId { get; set; } 
        public int GradeID { get; set; }
        

    }
}
