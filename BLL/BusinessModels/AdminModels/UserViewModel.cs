﻿using BLL.BusinessManagers.AdminManager;
using BLL.BusinessManagers.StudentManager;
using BLL.BusinessModels.StudentModels;
using BLL.BusinessModels.TeacherModels;
using DAL.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace BLL.BusinessModels.AdminModels
{
   public class UserViewModel
    {
        public UserViewModel()
        {
            gradeViewModel = new GradeViewModel();
        }

        public Int64 UserID { get; set; }

        public string LoginEmail { get; set; }

        public string Password { get; set; }

        public string ExternalLoginID { get; set; }

        public int UserType { get; set; }

        public int UserStatus { get; set; }

        public int IsVerified { get; set; }

        public bool IsActive { get; set; }

        public bool IsOnline { get; set; }

        public string BlockReason { get; set; }

        public long CurriculumID { get; set; }

        public string RequestFrom { get; set; }
        
        public int Response { get; set; }

        

        public List<CourseParticipants> courseParticipantsList { get; set; }
        public List<TeacherCourseHeaderModel> teacherCoursesList { get; set; }
        public List<TeacherAvailableHoursModel> teacherAvailableHours { get; set; }
        public List<TeacherSubjectsViewModel> teacherSubjectsList { get; set; }
        public List<SessionViewModel> SessionsList { get; set; }
        public List<SessionViewModel> joinSessionsList { get; set; }
        public List<PermissionViewModel> permissionsList { get; set; }
        public List<PermissionViewModel> listOfAllPermissions { get; set; }
        public UsersProfileViewModel UsersProfileViewModel { get; set; }
        public CurriculumsModel curriculumViewModel { get; set; }
        public GradeViewModel gradeViewModel { get; set; }
        public StateViewModel stateViewModel { get; set; } 
        public TeacherQualificationModel teacherQualification { get; set; }
        public UserActivitiesModel lastLoginActivityModel { get; set; }
        public UserActivitiesModel lastLogoutActivityModel { get; set; }

        public List<UsersProfileViewModel> UsersProfilesList { get; set; }

        public List<SelectListItem> StatesList { get; set; }
        public List<SelectListItem> GradesList { get; set; }
        public List<SelectListItem> CurriculumsList { get; set; }

        public List<SelectListItem> GetStatesList(List<StateViewModel> statesList)
        {
            List<SelectListItem> ListOfStates = new List<SelectListItem>();
            ListOfStates.Add(new SelectListItem { Value = "", Text = "Select State", Selected = false });
            foreach (var s in statesList)
            {
                ListOfStates.Add(new SelectListItem { Value = s.StateId.ToString(), Text = s.StateName , Selected = false });
            }
            return ListOfStates;
        }
        public List<SelectListItem> GetCurriculumsList(List<CurriculumsModel> curriculumsList)
        {
            List<SelectListItem> ListOfCurriculums = new List<SelectListItem>();
            ListOfCurriculums.Add(new SelectListItem { Value = "", Text = "Select Curriculum", Selected = false });
            foreach (var curr in curriculumsList)
            {
                ListOfCurriculums.Add(new SelectListItem { Value = curr.CurriculumID.ToString(), Text = curr.CurriculumName, Selected = false });
            }
            return ListOfCurriculums;
        }

        public List<SelectListItem> GetGradesList(List<GradeViewModel> gradesList )
        {
            CurriculumsManagers curriculumMgr = new CurriculumsManagers();

            List<SelectListItem> ListOfGrades = new List<SelectListItem>();
            ListOfGrades.Add(new SelectListItem { Value = "", Text = "Select Grade", Selected = false });
            foreach (var grd in gradesList)
            {
               var curriculumModel = curriculumMgr.GetCurriculumByCurriculumID(grd.FKCurriculumID);
                ListOfGrades.Add(new SelectListItem { Value = grd.GradeViewModelID.ToString(), Text = curriculumModel.CurriculumName + " - " +  grd.GradeName, Selected = false });
            }
            return ListOfGrades;
        }
    }
} 
