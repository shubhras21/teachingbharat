﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL.BusinessModels.AdminModels
{
  public  class PermissionViewModel
    {

        public int PermissionID { get; set; }
        public string PermissionName { get; set; }
        public int UserType { get; set; }
        public bool IsActive { get; set; }
        public int FKUserID { get; set; }

       
    }

}
