﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL.BusinessModels.AdminModels
{
  public  class StateViewModel
    {
        public long StateId { get; set; }
        public string StateName { get; set; }
        public bool IsActive { get; set; }
        public long FKCountryId { get; set; }

    }
}
