﻿using BLL.BusinessModels.TeacherModels;
using DAL.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace BLL.BusinessModels.UserModels
{
    // Student REGISTER View Model
    public class StudentRegisterViewModel
    {
        [Required(ErrorMessage = "Enter First Name")]
        [DisplayName("First Name")]
        public string FirstName { get; set; }

        [Required(ErrorMessage = "Enter Last Name")]
        [DisplayName("Last Name")]
        public string LastName { get; set; }

        [Required(ErrorMessage = "Enter Email Address")]
        [RegularExpression(@"^([a-zA-Z0-9_\-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$", ErrorMessage = "Please enter a valid e-mail adress")]
        [DisplayName("Email Address")]
        [Remote("CheckEmail", "Account", ErrorMessage = "Email Already Exists!")]
        public string LoginEmail { get; set; }

        [Required(ErrorMessage = "Enter Contact Number")]
        [Range(0, long.MaxValue, ErrorMessage = "Please enter valid contact Number")]
        [DisplayName("Contact Number")]
        public string ContactNumber { get; set; }

        [Required(ErrorMessage = "Enter Password")]
        [DataType(DataType.Password)]
        [DisplayName("Password")]
        public string Password { get; set; }

        [Required(ErrorMessage = "Enter Confirm Password")]
        [DataType(DataType.Password)]
        [DisplayName("Confirm Password")]
        [System.ComponentModel.DataAnnotations.Compare("Password", ErrorMessage = "The password and confirm password does not match.")]
        public string ConfirmPassword { get; set; }

        [Required(ErrorMessage = "Please Select State")]
        public Int64 StateID { get; set; }

        [Required(ErrorMessage = "Please Enter City")]
        public string CityName { get; set; }

        [Required(ErrorMessage = "Please choose one of them to register.")]
        public int UserType { get; set; }

        public string ExternalLoginId { get; set; }

        public IEnumerable<SelectListItem> sListofStates { get; set; }
        

    }


    // Teacher Registration View Model

    public class TeacherRegisterationViewModel
    {
        public long TeacherID { get; set; }

        [Required(ErrorMessage = "Enter First Name")]
        [DisplayName("First Name")]
        public string FirstName { get; set; }


        [Required(ErrorMessage = "Enter Experience")]
        [DisplayName("Experience")]
        public string ExperienceName { get; set; }

        [Required(ErrorMessage = "Enter Last Name")]
        [DisplayName("Last Name")]
        public string LastName { get; set; }

        public string Password { get; set; }

        [Required(ErrorMessage = "Enter Email Address")]
        [RegularExpression(@"^([a-zA-Z0-9_\-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$", ErrorMessage = "Please enter a valid e-mail adress")]
        [DisplayName("Email Address")]
        [Remote("CheckEmail", "Account", ErrorMessage = "Email Already Exists!")]
        public string LoginEmail { get; set; }

        [Required(ErrorMessage = "Enter Contact Number")]
        //[Range(0, long.MaxValue, ErrorMessage = "Please enter valid Contact Number")]
        [RegularExpression("^[1-9]\\d[\\d ]*\\d$", ErrorMessage = "Phone Number Is Not Valid ")]

        [DisplayName("Contact Number")]
        public string ContactNumber { get; set; }

 
        //[Range(0, long.MaxValue, ErrorMessage = "Please enter valid Contact Number")]
        [RegularExpression("^[1-9]\\d[\\d ]*\\d$", ErrorMessage = "Phone Number Is Not Valid ")]
        [DisplayName("Alt. Number")]
        public string AlternateNumber { get; set; }

        [Required(ErrorMessage = "Enter Date of Birth")]
        [DisplayName("Date of Birth")]
        public DateTime TeacherDob { get; set; }

        [Required(ErrorMessage = "Enter Source")]
        [DisplayName("About Rudra")]
        public string HeardAboutRudra { get; set; }

        [Required(ErrorMessage = "Enter University Name")]
        [DisplayName("University Name")]
        public string UniversityName { get; set; }

        // bool vaiables
        [Required(ErrorMessage = "Select Digital Pen")]
        [DisplayName("Do you have digital pen or tablet?")]
        public int DigitalPen { get; set; }

        [Required(ErrorMessage = "Please Select Internet Speed")]
        public int InternetSpeed { get; set; }

        [Required(ErrorMessage = "Please Select Resume")]
        public IEnumerable<HttpPostedFileBase> FileBase { get; set; }


        //[Required(ErrorMessage = "Please Select Subject")]
        //public List<Int64> Curriculums { get; set; }


        //[Required(ErrorMessage = "Please Select Grade")]
        //public List<Int64> Grade { get; set; }

        //[Required(ErrorMessage = "Please Select Subject")]
        //public List<Int64> Subject { get; set; }

        [Required(ErrorMessage = "Please Select Time")]
        public List<Int64> Time { get; set; }

        //[Required(ErrorMessage = "Please Select Occupation")]
        //public List<Int64> Occupations { get; set; }

        [Required(ErrorMessage = "Please Select Available Hours")]
        public int AvailableHours { get; set; }


        // Foreign Key required

        [Required(ErrorMessage = "Please Enter Qualification")]
        public string QualificationName { get; set; }

        [Required(ErrorMessage = "Please Select Occupation")]
        public Int64 OccupationID { get; set; }

        [Required(ErrorMessage = "Please Select Grade")]
        public Int64 GradeID { get; set; }

        [Required(ErrorMessage = "Please Select University")]
        public Int64 UniversityID { get; set; }

        [Required(ErrorMessage = "Please Select Experience")]
        public Int64 ExperienceID { get; set; }

        [Required(ErrorMessage = "Please Select State")]
        public Int64 StateID { get; set; }

        [Required(ErrorMessage = "Please Enter City")]
        public string CityName { get; set; }


        [Required(ErrorMessage = "Please Select Curriculum")]
        public Int64 CurriculumId { get; set; }

        [Required(ErrorMessage = "Please Select Primary Subject")]
        public Int64 PrimarySubjectID { get; set; }

        [Required(ErrorMessage = "Please Select Secondary Subject")]
        public Int64 SecondarySubjectID { get; set; }

        public int Response { get; set; }

        //List of Supporting Model

        public List<CurriculamModel> ListofCurriculums { get; set; }

        public List<Countries> ListofCountries { get; set; }

        public List<SubjectModel> ListofSubjects { get; set; }

        public List<GradeModel> ListofGrade { get; set; }

        public List<TimingModel> ListofavailableTimes { get; set; }

        public List<ExperienceModel> ListofExperiences { get; set; }

        public List<OccupationModel> ListofOccupation { get; set; }

        public List<QualicitationModel> ListofQualification { get; set; }

        public List<UniversityModel> ListofUniversities { get; set; }


        // select lists
        public IEnumerable<SelectListItem> sListofCurriculums { get; set; }

        public IEnumerable<SelectListItem> sListofCountries { get; set; }

        public IEnumerable<SelectListItem> sListofStates { get; set; }

        public IEnumerable<SelectListItem> sListofCities { get; set; }

        public IEnumerable<SelectListItem> sListofSubjects { get; set; }

        public IEnumerable<SelectListItem> sListofPSubjects { get; set; }

        public IEnumerable<SelectListItem> ListofSubjectssListofSSubjects { get; set; }

        public IEnumerable<SelectListItem> sListofGrade { get; set; }

        public IEnumerable<SelectListItem> sListofavailableTimes { get; set; }

        public IEnumerable<SelectListItem> sListofExperiences { get; set; }

        public IEnumerable<SelectListItem> sListofOccupation { get; set; }

        public IEnumerable<SelectListItem> sListofQualification { get; set; }

        public IEnumerable<SelectListItem> sListofUniversities { get; set; }

        public IEnumerable<SelectListItem> sListofDigitalPen { get; set; }

        public IEnumerable<SelectListItem> sListofWebcam { get; set; }

        public IEnumerable<SelectListItem> sListofInternetConnection { get; set; }


    }

    //LOGIN
    public class LoginViewModel
    {

        [Required(ErrorMessage = "Enter Email Address")]
        [EmailAddress]
        [DisplayName("Email Address")]
        public string LoginEmail { get; set; }

        [Required(ErrorMessage = "Enter Password")]
        [DataType(DataType.Password)]
        [DisplayName("Password")]
        public string Password { get; set; }

        public string ExternalLoginId { get; set; }

    }


	public class FacebookLoginModel
	{
		public string uid { get; set; }
		public string accessToken { get; set; }
	}

	//External SIGN IN VIEW MODEL
	public class ExternalLoginViewModel
    {
        public string EmailAddress { get; set; }

        public string Password { get; set; }

        public string TokenId { get; set; }

        public int UserType { get; set; }
    }


    //My Profile View Model
    public class MyProfileViewModel
    {

        public string EmailAddress { get; set; }

        public string Name { get; set; }

        public string Grade { get; set; }

        public string ProfileImage { get; set; }

        public string Location { get; set; }

        public string ContactNumber { get; set; }

        public string Languages { get; set; }

        public string AboutMe { get; set; }

    }


    //My Profile View Model
    public class EditProfileViewModel
    {
        public Int64 UserID { get; set; }
        public string EmailAddress { get; set; }

        public string Name { get; set; }

        [Required(ErrorMessage = "Please enter grade")]
        public string Grade { get; set; }


        public string ProfileImage { get; set; }


        public IEnumerable<SelectListItem> sListofStates { get; set; }

        [Required(ErrorMessage = "Please select state")]
        public Int64 StateID { get; set; }

        [Required(ErrorMessage = "Enter City name")]
      
        public string CityName { get; set; }

        [Required(ErrorMessage = "Enter Contact Number")]
          [RegularExpression("^[1-9]\\d[\\d ]*\\d$", ErrorMessage = "Phone Number Is Not Valid ")]

        public string ContactNumber { get; set; }

        [Required(ErrorMessage = "Please enter language")]
           public string Languages { get; set; }

        [Required(ErrorMessage = "Please enter something about yourself")]
        public string AboutMe { get; set; }

    }


    //My Profile View Model
    public class TeacherEditProfileViewModel
    {
        public Int64 UserID { get; set; }
        public string EmailAddress { get; set; }

        public string Name { get; set; }

        [Required(ErrorMessage = "Please enter University name")]
        public string UniversityName { get; set; }

        public string ProfileImage { get; set; }

        public IEnumerable<SelectListItem> sListofStates { get; set; }

        [Required(ErrorMessage = "Please select state")]
        public Int64 StateID { get; set; }

        [Required(ErrorMessage = "Enter City name")]

        public string CityName { get; set; }

        [Required(ErrorMessage = "Enter Contact Number")]
        [RegularExpression("^[1-9]\\d[\\d ]*\\d$", ErrorMessage = "Phone Number Is Not Valid ")]

        public string ContactNumber { get; set; }

        [Required(ErrorMessage = "Please enter language")]
        public string Languages { get; set; }

        [Required(ErrorMessage = "Please enter something about yourself")]
        public string AboutMe { get; set; }

        //  [RegularExpression(@"^\$?([1-9]{1}[0-9]{0,2}(\,[0-9]{3})*(\.[0-9]{0,2})?|[1-9]{1}[0-9]{0,}(\.[0-9]{0,2})?|0(\.[0-9]{0,2})?|(\.[0-9]{1,2})?)$", ErrorMessage = "{0} must be a Number.")]

        [DisplayName("Rate")]
        [RegularExpression(@"[0-9]*\.?[0-9]+", ErrorMessage = "{0} must be a Number.")]
        [Range(typeof(int), "1", "5000",ErrorMessage = "{0} can only be between {1} and {2}")]
        public double SessionCost { get; set; }

        public List<TeacherSubjects> listOfSubjects { get; set; }

        public RatingsViewModel RatingsModel { get; set; }

    }


    public class ChangePasswordViewModel
    {

        public Int64 UserID { get; set; }

        [Required(ErrorMessage = "Enter Old Password")]
        [DataType(DataType.Password)]
        [DisplayName("Old Password")]
        [Remote("CheckOldPassword", "Student", ErrorMessage = "Old Password not match!")]
        public string OldPassword { get; set; }

        [Required(ErrorMessage = "Enter New Password")]
        [DataType(DataType.Password)]
        [DisplayName("Password")]
        public string Password { get; set; }

        [Required(ErrorMessage = "Enter Confirm Password")]
        [DataType(DataType.Password)]
        [DisplayName("Confirm Password")]
        [System.ComponentModel.DataAnnotations.Compare("Password", ErrorMessage = "The password and confirmation password do not match.")]
        public string ConfirmPassword { get; set; }
    }


    public class TeachersChangePasswordViewModel
    {

        public Int64 UserID { get; set; }

        [Required(ErrorMessage = "Enter Old Password")]
        [DataType(DataType.Password)]
        [DisplayName("Old Password")]
        [Remote("CheckOldPassword", "Teacher", ErrorMessage = "Old Password not match!")]
        public string OldPassword { get; set; }

        [Required(ErrorMessage = "Enter New Password")]
        [DataType(DataType.Password)]
        [DisplayName("Password")]
        public string Password { get; set; }

        [Required(ErrorMessage = "Enter Confirm Password")]
        [DataType(DataType.Password)]
        [DisplayName("Confirm Password")]
        [System.ComponentModel.DataAnnotations.Compare("Password", ErrorMessage = "The password and confirmation password do not match.")]
        public string ConfirmPassword { get; set; }
    }

    public class WeeklyReportViewModel
    {
        public string Date { get; set; }
        public string Day { get; set; }
        public int TotalHours { get; set; }
        public double TotalDailyEarnings { get; set; }
    }

    public class AddSubjectViewModel
    {
        [Required(ErrorMessage = "Please Select Curriculum")]
        public Int64 CurriculumId { get; set; }

        [Required(ErrorMessage = "Please Select Grade")]
        public Int64 GradeID { get; set; }

        [Required(ErrorMessage = "Please Select Subject")]
        public Int64 PrimarySubjectID { get; set; }

        // select lists
        public IEnumerable<SelectListItem> sListofCurriculums { get; set; }

        public IEnumerable<SelectListItem> sListofSubjects { get; set; }

    }
}
