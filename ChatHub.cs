﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using Microsoft.AspNet.SignalR;
using Microsoft.AspNet.SignalR.Hubs;


namespace Tutoring
{
    public class ChatHub : Hub
    {
        static ConcurrentDictionary<string, string> dic = new ConcurrentDictionary<string, string>();
        private static string conString = ConfigurationManager.ConnectionStrings["Tutoring"].ToString();

        public void Hello()
        {
            Clients.All.hello();
        }

        public void Send(string name, string message)
        {
            // Call the addNewMessageToPage method to update clients.
            //Clients.All.addNewMessageToPage(name, message);
            Clients.All.broadcastMessage(name, message);
        }

        [HubMethodName("sendMessages")]
        public static void SendMessages()
        {
            IHubContext context = GlobalHost.ConnectionManager.GetHubContext<ChatHub>();
            context.Clients.All.updateMessages();
        }

        public void SendToSpecific(string name, string message, string to)
        {
            // Call the broadcastMessage method to update clients.
            Clients.Caller.broadcastMessage(name, message);
            Clients.Client(dic[to]).broadcastMessage(name, message);
        }

        public void Notify(string name, string id)
        {
            if (dic.ContainsKey(name))
            {
                Clients.Caller.differentName();
            }
            else
            {
                dic.TryAdd(name, id);
                foreach (KeyValuePair<String, String> entry in dic)
                {
                    Clients.Caller.online(entry.Key);
                }
                Clients.Others.enters(name);
            }
        }
        //public void UserObject(UserObject_forChat userObj, string id)
        //{
        //    string name = userObj.UserName;
        //    if (dic.ContainsKey(name))
        //    {
        //        Clients.Caller.differentName();
        //    }
        //    else
        //    {
        //        dic.TryAdd(name, id);
        //        foreach (KeyValuePair<String, String> entry in dic)
        //        {
        //            Clients.Caller.online(entry.Key);
        //        }
        //        Clients.Others.enters(name);
        //    }
        //}
        //public override Task OnDisconnected()
        //{
        //    var name = dic.FirstOrDefault(x => x.Value == Context.ConnectionId.ToString());
        //    string s;
        //    dic.TryRemove(name.Key, out s);
        //    return Clients.All.disconnected(name.Key);
        //}

        public override Task OnDisconnected(bool stopCalled)
        {
            var name = dic.FirstOrDefault(x => x.Value == Context.ConnectionId.ToString());
            string s;
            dic.TryRemove(name.Key, out s);
            return Clients.All.disconnected(name.Key);
        }

    }
}