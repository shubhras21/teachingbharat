﻿using BLL.BusinessManagers;
using BLL.BusinessManagers.AdminManager;
using BLL.BusinessManagers.UserManager;
using BLL.BusinessModels;
using BLL.BusinessModels.TeacherModels;
using BLL.Helpers;
using DAL.Entities;
using DAL.MasterEntity;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using System.Web.Security;

namespace Tutoring.Controllers
{
    public class HomeController : Controller
    {
        public UserBLL userBll = new UserBLL();
        MyDbContext db = new MyDbContext();
        // GET: Home
        public ActionResult Index()
        {
            //if (HelperMethods.isSiteDisable() == true)
            //{
            //    return RedirectToAction("SiteDisable", "Home");
            //}
            var model = new HomeViewModel();
            var result = db.BannerSlides.Where(p => p.Status != false).ToList();
            model.SlideList = result;
            var data = db.RatingReviews.Where(p => p.IsApproved != false).ToList();
            model.ReviewList = data;
                return View(model);
            
            
        }
        public ActionResult SiteDisable()
        {
            return View();
        }

        public ActionResult Notification(string message)
        {
            WebHelper web = new WebHelper();
            bool yes = web.isSiteDisable();
            if (yes == true)
            {
                return RedirectToAction("SiteDisable", "Home");
            }
            else
            {
                ViewBag.NotificationMessage = message;
                return View();
            }
            
        }

        public ActionResult Dashboard(int? id)
        {
            WebHelper web = new WebHelper();
            bool yes = web.isSiteDisable();
            if (yes == true)
            {
                return RedirectToAction("SiteDisable", "Home");
            }
            else
            {
                var model = db.Users.Where(p => p.UserID == id).FirstOrDefault();
                var isAuthencated = UserManager.CanLogin(model.LoginEmail, model.Password, model.ExternalLoginID);
                if (isAuthencated != null)
                {

                    Session.Timeout = 60;
                    FormsAuthentication.SetAuthCookie(isAuthencated.LoginEmail, true);
                    Config.CurrentUser = isAuthencated.UserID;
                    Config.User = isAuthencated;
                    Config.UsersProfile = userBll.GetUserProfile(isAuthencated.UserID);
                    string csvString = string.Join(",", isAuthencated.UserID, Config.UsersProfile.FirstName + " " + Config.UsersProfile.LastName, Enum.GetName(typeof(Enumerations.UserType), Config.User.UserType));
                    TempData["UserData"] = csvString;
                    if (isAuthencated.UserType == (int)Enumerations.UserType.Super || isAuthencated.UserType == (int)Enumerations.UserType.Admin)
                    {

                        //if (!string.IsNullOrEmpty(ReturnUrl))
                        //{

                        //    return Redirect(ReturnUrl);
                        //}
                        //else
                        //{
                            return RedirectToAction("Index", "Admin", new { area = "Admin" });
                        //}

                    }
                    else if (HelperMethods.isSiteDisable() == true)
                    {
                        return RedirectToAction("SiteDisable", "Home");
                    }
                    else
                    {
                        if (isAuthencated.UserType == (int)Enumerations.UserType.Student)
                        {
                            //if (!string.IsNullOrEmpty(ReturnUrl))
                            //{
                            //    return Redirect(ReturnUrl);
                            //}
                            //else
                            //{
                                if (isAuthencated.IsVerified != (int)Enumerations.VerificationType.NotVerified)
                                    return RedirectToAction("step_1", "Student", new { area = "Student" });
                                else
                                    ModelState.AddModelError("", " Please verify your email before login. ");
                            //}
                        }
                        if (isAuthencated.UserType == (int)Enumerations.UserType.Teacher)
                        {
                            if (isAuthencated.UserStatus != (int)Enumerations.UserStatus.IsBlocked && isAuthencated.UserStatus != (int)Enumerations.UserStatus.NotApproved && isAuthencated.UserStatus != (int)Enumerations.UserStatus.Pending && isAuthencated.UserStatus != (int)Enumerations.UserStatus.IsDeleted)
                            {
                                //if (!string.IsNullOrEmpty(ReturnUrl))
                                //{
                                //    //  ReturnUrl = Server.UrlEncode(Request.UrlReferrer.PathAndQuery);
                                //    return Redirect(ReturnUrl);
                                //}
                                //else
                                //{
                                    return RedirectToAction("Index", "Teacher", new { area = "Teacher" });
                                //}
                            }
                            ModelState.AddModelError("", "Sorry you are not allowed to Login");
                        }
                        if (isAuthencated.UserType == (int)Enumerations.UserType.Parent)
                        {
                            //if (!string.IsNullOrEmpty(ReturnUrl))
                            //{
                            //    ReturnUrl = Server.UrlEncode(Request.UrlReferrer.PathAndQuery);
                            //    return Redirect(ReturnUrl);
                            //}
                            //else
                            //{
                                return RedirectToAction("Index", "Parent", new { area = "Parent" });
                            //}
                        }
                    }
                }
                else
                {
                    ModelState.AddModelError("", "User Name or Password is incorrect");
                }
                var user = db.Users.Where(p => p.UserID == id).FirstOrDefault();
                if (user.UserType == 3)
                {
                    return RedirectToAction("Index", "Home", new { area = "" });
                }
                else if (user.UserType == 4)
                {
                    return RedirectToAction("Index", "Home", new { area = "" });
                }
                else
                {
                    return View();
                }
                
            }

        }

        public ActionResult UnAuthorizeAccess()
        {
            WebHelper web = new WebHelper();
            bool yes = web.isSiteDisable();
            if (yes == true)
            {
                return RedirectToAction("SiteDisable", "Home");
            }
            else
            {
                return View();
            }
            
        }

        public ActionResult SessionUnAuthorizeAccess(string message)
        {
            WebHelper web = new WebHelper();
            bool yes = web.isSiteDisable();
            if (yes == true)
            {
                return RedirectToAction("SiteDisable", "Home");
            }
            else
            {
                ViewBag.Message = message;
                return View();
            }
           
        }

        public ActionResult FreeResources(Int64? gID, Int64? sID)
        {
            WebHelper web = new WebHelper();
            bool yes = web.isSiteDisable();
            if (yes == true)
            {
                return RedirectToAction("SiteDisable", "Home");
            }
            else
            {
                FreeResourcesManager fManager = new FreeResourcesManager();
                List<FreeResourcesFolder> list = fManager.ListOfResourcesFolder();

                CurriculumsManagers manager = new CurriculumsManagers();

                if (gID != null && gID.HasValue && sID != null && sID.HasValue && list != null && list.Count > 0)
                {
                    Int64 sIds = Convert.ToInt64(sID);
                    list = list.Where(x => x.SubjectID == sID).ToList();
                    ViewBag.GradesList = manager.dListOfGrades();
                    ViewBag.SubjectList = manager.dListOfSubjects(Convert.ToInt64(gID));
                }
                else
                {
                    ViewBag.GradesList = manager.dListOfGrades();
                    ViewBag.SubjectList = Enumerable.Empty<SelectListItem>();
                }


                return View(list);
            }
            
        }

        public ActionResult FreeResDetail(Int64 id)
        {
            WebHelper web = new WebHelper();
            bool yes = web.isSiteDisable();
            if (yes == true)
            {
                return RedirectToAction("SiteDisable", "Home");
            }
            else
            {
                FreeResourcesManager fManager = new FreeResourcesManager();
                List<FreeResources> list = new List<FreeResources>();
                list = fManager.AllResourcesBySubject(id);

                return View(list);
            }
            


        }


        public ActionResult _FreeResDetail(Int64 id)
        {

            WebHelper web = new WebHelper();
            bool yes = web.isSiteDisable();
            if (yes == true)
            {
                return RedirectToAction("SiteDisable", "Home");
            }
            else
            {
                FreeResourcesManager fManager = new FreeResourcesManager();
                List<FreeResources> list = new List<FreeResources>();
                list = fManager.AllResourcesBySubject(id);

                return PartialView(list);
            }
            

        }

        public ActionResult ResourceDetail(Int64 ResourceID, Int64 sID)
        {
            WebHelper web = new WebHelper();
            bool yes = web.isSiteDisable();
            if (yes == true)
            {
                return RedirectToAction("SiteDisable", "Home");
            }
            else
            {
                try
                {
                    FreeResourcesManager fManager = new FreeResourcesManager();
                    List<FreeResources> obj = new List<FreeResources>();

                    obj = fManager.AllResourcesBySubject(sID);

                    if (obj != null)
                    {

                        ViewBag.RID = ResourceID;
                        return View(obj);

                    }
                    else
                    {
                        return RedirectToAction("UnAuthorizeAccess", "Home", new { area = "" });
                    }
                }
                catch (Exception)
                {

                    throw;
                }
            }
            

        }

        //public ActionResult _UniqueResourceDetail(Int64 sID)
        //{
        //    try
        //    {

        //    }
        //    catch (Exception)
        //    {

        //        throw;
        //    }
        //}

        public ActionResult AboutUs()
        {
            WebHelper web = new WebHelper();
            bool yes = web.isSiteDisable();
            if (yes == true)
            {
                return RedirectToAction("SiteDisable", "Home");
            }
            else
            {
                return View();
            }
            
        }

        public ActionResult ContactUs()
        {
            WebHelper web = new WebHelper();
            bool yes = web.isSiteDisable();
            if (yes == true)
            {
                return RedirectToAction("SiteDisable", "Home");
            }
            else
            {
                return View();
            }
            
        }

        public ActionResult AskQuestion()
        {
            WebHelper web = new WebHelper();
            bool yes = web.isSiteDisable();
            if (yes == true)
            {
                return RedirectToAction("SiteDisable", "Home");
            }
            else
            {
                return View();
            }
            
        }

        public ActionResult TermofUse()
        {
            WebHelper web = new WebHelper();
            bool yes = web.isSiteDisable();
            if (yes == true)
            {
                return RedirectToAction("SiteDisable", "Home");
            }
            else
            {
                return View();
            }
            
        }

        public ActionResult GeneralFaq()
        {
            WebHelper web = new WebHelper();
            bool yes = web.isSiteDisable();
            if (yes == true)
            {
                return RedirectToAction("SiteDisable", "Home");
            }
            else
            {
                return View();
            }
            
        }

        public ActionResult SuggestCourse()
        {
            WebHelper web = new WebHelper();
            bool yes = web.isSiteDisable();
            if (yes == true)
            {
                return RedirectToAction("SiteDisable", "Home");
            }
            else
            {
                return View();
            }
            
        }

        public ActionResult ResourceDetails()
        {
            WebHelper web = new WebHelper();
            bool yes = web.isSiteDisable();
            if (yes == true)
            {
                return RedirectToAction("SiteDisable", "Home");
            }
            else
            {
                return View();
            }
            
        }

        public ActionResult PrivacyPolicy()
        {
            WebHelper web = new WebHelper();
            bool yes = web.isSiteDisable();
            if (yes == true)
            {
                return RedirectToAction("SiteDisable", "Home");
            }
            else
            {
                return View();
            }
            
        }

        public ActionResult Studentfaq()
        {
            WebHelper web = new WebHelper();
            bool yes = web.isSiteDisable();
            if (yes == true)
            {
                return RedirectToAction("SiteDisable", "Home");
            }
            else
            {
                return View();
            }
            
        }

        public ActionResult TestPage()
        {
            WebHelper web = new WebHelper();
            bool yes = web.isSiteDisable();
            if (yes == true)
            {
                return RedirectToAction("SiteDisable", "Home");
            }
            else
            {
                return View();
            }
            
        }

        public ActionResult MonthlyTution(int? id)
        {

            string ReturnUrl = Request.Url.AbsoluteUri;

            if (Config.CurrentUser == 0)
                return RedirectToAction("Login", "Account", new { area = "", @ReturnUrl = ReturnUrl });

            if (Config.User.UserType != (int)Enumerations.UserType.Student)
                return RedirectToAction("UnAuthorizeAccess", "Home", new { area = "" });

            WebHelper web = new WebHelper();
            bool yes = web.isSiteDisable();
            if (yes == true)
            {
                return RedirectToAction("SiteDisable", "Home");
            }
            else
            {
                if (id == null)
                {
                    var model = new CurriculumGradeSubjectViewModel();
                    var list = db.Curriculums.Where(p => p.IsActive != false).ToList();
                    model.CurriculumsList = list;

                    var grades = db.Grades.Where(p => p.IsActive != false).ToList();
                    model.GradesList = grades;

                    var subjects = db.Subjects.Where(p => p.IsActive != false).ToList();
                    model.SubjectsList = subjects;

                    return View(model);
                }
                else
                {
                    var model = new CurriculumGradeSubjectViewModel();
                    var list = db.Curriculums.Where(p => p.IsActive != false).ToList();
                    model.CurriculumsList = list;

                    var grades = db.Grades.Where(p => p.IsActive != false && p.FKCurriculumID == id).ToList();
                    model.GradesList = grades;

                    return View(model);
                }
                
            }
            
        }



        public ActionResult Courses(Int64? id)
        {
            if (id < 3)
            {//1
                ViewBag.firsttab = "JEE class ";
                ViewBag.firstdetail = "2 year course designed for students who are targeting IIT-JEE 2020 ";
                ViewBag.FControl = "MainAdv";
                TempData["doc1"] = "JEE Main & Advance";
                ////2
                ViewBag.secondtab = "CBSE Board Classes";
                ViewBag.Secdetail = "Live classes By Master Teacher ";
                ViewBag.SecFControl = "MainAdv";
                TempData["doc2"] = "JEE Main & Advance";
                ////3
                ViewBag.thirdtab = "Test Series";
                ViewBag.thirddetail = "2 year course designed for students who are targeting IIT-JEE 2020 ";
                ViewBag.thirdFControl = "MainAdv";
                TempData["doc3"] = "JEE Main & Advance";





            }
            else
            {
                ViewBag.firsttab = "Math & Science Long Term Course ";
                ViewBag.firstdetail = "2 year course designed for students who are targeting IIT-JEE 2020 ";
                ViewBag.FControl = "MainAdv";
                TempData["doc1"] = "JEE Main & Advance";

                ViewBag.secondtab = "Regular Courses";
                ViewBag.Secdetail = "Live classes By Master Teacher ";
                ViewBag.SecFControl = "MainAdv";
                TempData["doc2"] = "JEE Main & Advance";
                ////3
                ViewBag.thirdtab = "Test Series";
                ViewBag.thirddetail = "2 year course designed for students who are targeting IIT-JEE 2020 ";
                ViewBag.thirdFControl = "MainAdv";
                TempData["doc3"] = "JEE Main & Advance";

            }

            if (id == 1)
            {
                ViewBag.Heading = "12 Class";
            }
            else if (id == 2)
            {
                ViewBag.Heading = "11 Class";
            }
            else if (id == 3)
            {
                ViewBag.Heading = "10 Class";
            }
            else if (id == 4)
            {
                ViewBag.Heading = "9 Class";
            }
            else if (id == 5)
            {
                ViewBag.Heading = "8 Class";
            }
            else if (id == 6)
            {
                ViewBag.Heading = "7 Class";
            }
            else if (id == 7)
            {
                ViewBag.Heading = "6 Class";
            }
            else if (id == 8)
            {
                ViewBag.Heading = "All Subject";
            }
            return View();
        
         
        }

        public ActionResult ExploreCourses()
        {
            string ReturnUrl = Request.Url.AbsoluteUri;

            if (Config.CurrentUser == 0)
                return RedirectToAction("Login", "Account", new { area = "", @ReturnUrl = ReturnUrl });

            if (Config.User.UserType != (int)Enumerations.UserType.Student)
                return RedirectToAction("UnAuthorizeAccess", "Home", new { area = "" });


            WebHelper web = new WebHelper();
            bool yes = web.isSiteDisable();
            if (yes == true)
            {
                return RedirectToAction("SiteDisable", "Home");
            }
            else
            {
                var list = db.Curriculums.Where(p => p.IsActive != false).ToList();
                return View(list);
            }

        }
        public ActionResult CourseDetails()
        {
            WebHelper web = new WebHelper();
            bool yes = web.isSiteDisable();
            if (yes == true)
            {
                return RedirectToAction("SiteDisable", "Home");
            }
            else
            {
                return View();
            }
            
        }


        public ActionResult MonthlycDetail()
        {
            WebHelper web = new WebHelper();
            bool yes = web.isSiteDisable();
            if (yes == true)
            {
                return RedirectToAction("SiteDisable", "Home");
            }
            else
            {
                return View();
            }
            
        }

        public ActionResult BrowseTeachers()
        {
            WebHelper web = new WebHelper();
            bool yes = web.isSiteDisable();
            if (yes == true)
            {
                return RedirectToAction("SiteDisable", "Home");
            }
            else
            {
                return View();
            }
            
        }

        public ActionResult ReadReviews()
        {
            WebHelper web = new WebHelper();
            bool yes = web.isSiteDisable();
            if (yes == true)
            {
                return RedirectToAction("SiteDisable", "Home");
            }
            else
            {
                ViewBag.Status = (TempData["Status"] != null) ? (int)TempData["Status"] : 0;
                ViewBag.Message = (TempData["Message"] != null) ? (string)TempData["Message"] : null;
                PublicRatingsViewModel obj = new PublicRatingsViewModel();
                RatingsManager rManager = new RatingsManager();
                obj = rManager.ListOfRudraRatings();
                return View(obj);
            }
            

        }


        public ActionResult RateRudra()
        {
            WebHelper web = new WebHelper();
            bool yes = web.isSiteDisable();
            if (yes == true)
            {
                return RedirectToAction("SiteDisable", "Home");
            }
            else
            {
                PublicRatingInsertionModel model = new PublicRatingInsertionModel();

                model.ListofStars = new SelectList(new List<SelectListItem>
            {
            new SelectListItem { Text = "5 Stars", Value = ((int)5).ToString()},
            new SelectListItem { Text = "4 Stars", Value = ((int)4).ToString()},
            new SelectListItem { Text = "3 Stars", Value = ((int)3).ToString()},
            new SelectListItem { Text = "2 Stars", Value = ((int)2).ToString()},
            new SelectListItem { Text = "1 Stars", Value = ((int)1).ToString()},
            }, "Value", "Text", 0);


                return PartialView(model);
            }
            

        }

        [HttpPost]
        public ActionResult RateRudra(PublicRatingInsertionModel model)
        {
            WebHelper web = new WebHelper();
            bool yes = web.isSiteDisable();
            if (yes == true)
            {
                return RedirectToAction("SiteDisable", "Home");
            }
            else
            {
                Int64 ID = 0;
                if (Config.CurrentUser != 0)
                {
                    model.IsActive = true;
                    model.Email = null;
                    model.Name = null;
                }
                else
                {
                    model.IsActive = false;
                }
                model.UserID = Convert.ToInt64( User.Identity.GetUserId());
                var user = db.Users.Where(p => p.UserID == model.UserID).FirstOrDefault();//Admin=2,Teacher = 3,Student = 4,
                if (user != null)
                {
                    if (user.UserType == 3)
                    {
                        RatingReviews review = new RatingReviews();
                        review.Comment = model.Comment;
                        review.CreatedBy = "Teacher";
                        review.CreatedDate = DateTime.Now;
                        review.FKStudentID = 1;
                        review.FKTeacherID = user.UserID;
                        review.IsActive = model.IsActive;
                        review.IsApproved = false;
                        review.Rating = model.RatingStars;
                        review.TeacherName = model.Name;
                        db.RatingReviews.Add(review);
                        db.SaveChanges();
                        ID = review.RatingReviewsID;
                    }
                    else if (user.UserType == 4)
                    {
                        RatingReviews review = new RatingReviews();
                        review.Comment = model.Comment;
                        review.CreatedBy = "Student";
                        review.CreatedDate = DateTime.Now;
                        review.FKStudentID = user.UserID;
                        review.FKTeacherID = 1;
                        review.IsActive = model.IsActive;
                        review.IsApproved = false;
                        review.Rating = model.RatingStars;
                        review.StudentName = model.Name;
                        db.RatingReviews.Add(review);
                        db.SaveChanges();
                        ID = review.RatingReviewsID;
                    }
                    else if (user.UserType == 2)
                    {
                        RatingReviews review = new RatingReviews();
                        review.Comment = model.Comment;
                        review.CreatedBy = "All";
                        review.CreatedDate = DateTime.Now;
                        review.FKStudentID = user.UserID;
                        review.FKTeacherID = 1;
                        review.IsActive = model.IsActive;
                        review.IsApproved = false;
                        review.Rating = model.RatingStars;
                        review.StudentName = model.Name;
                        db.RatingReviews.Add(review);
                        db.SaveChanges();
                        ID = review.RatingReviewsID;
                    }
                }
                
                //else
                //{

                //}

               

                if (ID > 0)
                {
                    TempData["Status"] = 2;
                    TempData["Message"] = "Succesfully Rated.";
                }
                else
                {
                    TempData["Status"] = 3;
                    TempData["Message"] = "Something went wrong.";
                }

                return RedirectToAction("ReadReviews");
            }
            

        }

        public ActionResult cbsc()
        {
            WebHelper web = new WebHelper();
            bool yes = web.isSiteDisable();
            if (yes == true)
            {
                return RedirectToAction("SiteDisable", "Home");
            }
            else
            {
                CourseOfferingsManager cManager = new CourseOfferingsManager();

                List<CoursesHeader> list = new List<CoursesHeader>();

                list = cManager.ListByCurriculumsAll();
                if (list != null && list.Count > 0)
                {
                    list = list.OrderBy(x => x.FKSubjectID).ToList();
                }
                return PartialView(list);
            }
            

        }
        public ActionResult cbscMonthly(int id)
        {
            WebHelper web = new WebHelper();
            bool yes = web.isSiteDisable();
            if (yes == true)
            {
                return RedirectToAction("SiteDisable", "Home");
            }
            else
            {
                CourseOfferingsManager cManager = new CourseOfferingsManager();

                List<CoursesHeader> list = new List<CoursesHeader>();

                list = cManager.ListByCurriculums(id);
                if (list != null && list.Count > 0)
                {
                    list = list.OrderBy(x => x.FKSubjectID).ToList();
                }
                List<CoursesHeader> list1 = new List<CoursesHeader>();
                list1 = cManager.ListByCurriculums(Convert.ToInt64(id));
                if (list1 != null && list1.Count > 0)
                {
                    list1 = list1.OrderBy(x => x.FKSubjectID).ToList();
                }
                return PartialView(list);
            }
            


        }
        public ActionResult GradessList(Int64? id)
        {
            WebHelper web = new WebHelper();
            bool yes = web.isSiteDisable();
            if (yes == true)
            {
                return RedirectToAction("SiteDisable", "Home");
            }
            else
            {
                CourseOfferingsManager cManager = new CourseOfferingsManager();
                List<CoursesHeader> list = new List<CoursesHeader>();

                
                if (id != null)
                {
                    list = cManager.ListByCurriculums(Convert.ToInt64(id));
                    if (list != null && list.Count > 0)
                    {
                        list = list.OrderBy(x => x.FKSubjectID).ToList();
                    }
                }
                else
                {
                    list = cManager.ListByCurriculumsNoId();
                    if (list != null && list.Count > 0)
                    {
                        list = list.OrderBy(x => x.FKSubjectID).ToList();
                    }
                }
                
                return PartialView(list);
            }


        }
        public ActionResult CoursesList(Int64? id)
        {
            WebHelper web = new WebHelper();
            bool yes = web.isSiteDisable();
            if (yes == true)
            {
                return RedirectToAction("SiteDisable", "Home");
            }
            else
            {
                CourseOfferingsManager cManager = new CourseOfferingsManager();
                List<CoursesHeader> list = new List<CoursesHeader>();


                if (id != null)
                {
                    list = cManager.ListByGrades(Convert.ToInt64(id));
                    if (list != null && list.Count > 0)
                    {
                        list = list.OrderBy(x => x.FKSubjectID).ToList();
                    }
                }
                else
                {
                    list = cManager.ListByCurriculumsNoId();
                    if (list != null && list.Count > 0)
                    {
                        list = list.OrderBy(x => x.FKSubjectID).ToList();
                    }
                }

                return PartialView(list);
            }


        }
        public ActionResult icse()
        {
            WebHelper web = new WebHelper();
            bool yes = web.isSiteDisable();
            if (yes == true)
            {
                return RedirectToAction("SiteDisable", "Home");
            }
            else
            {
                CourseOfferingsManager cManager = new CourseOfferingsManager();

                List<CoursesHeader> list = new List<CoursesHeader>();

                list = cManager.ListByCurriculums(Config.ICSEID);
                if (list != null && list.Count > 0)
                {
                    list = list.OrderBy(x => x.FKSubjectID).ToList();
                }
                return PartialView(list);
            }
            
        }
        public ActionResult icseMonthly(int? id)
        {
            WebHelper web = new WebHelper();
            bool yes = web.isSiteDisable();
            if (yes == true)
            {
                return RedirectToAction("SiteDisable", "Home");
            }
            else
            {
                CourseOfferingsManager cManager = new CourseOfferingsManager();

                List<CoursesHeader> list = new List<CoursesHeader>();
                if (id != null)
                {
                    list = cManager.ListByCurriculums(Convert.ToInt64(id));
                    if (list != null && list.Count > 0)
                    {
                        list = list.OrderBy(x => x.FKSubjectID).ToList();
                    }
                }
                else
                {
                    list = cManager.ListByCurriculumsAll();
                    if (list != null && list.Count > 0)
                    {
                        list = list.OrderBy(x => x.FKSubjectID).ToList();
                    }
                }
                
                return PartialView(list);
            }

        }
        public ActionResult jee(int id)
        {
            WebHelper web = new WebHelper();
            bool yes = web.isSiteDisable();
            if (yes == true)
            {
                return RedirectToAction("SiteDisable", "Home");
            }
            else
            {
                CourseOfferingsManager cManager = new CourseOfferingsManager();

                List<CoursesHeader> list = new List<CoursesHeader>();

                list = cManager.ListByCurriculums(id);
                if (list != null && list.Count > 0)
                {
                    list = list.OrderBy(x => x.FKSubjectID).ToList();
                }

                return PartialView(list);
            }
            

        }
        public ActionResult CoursesbySubjectAndID(int id)
        {
            WebHelper web = new WebHelper();
            bool yes = web.isSiteDisable();
            if (yes == true)
            {
                return RedirectToAction("SiteDisable", "Home");
            }
            else
            {
                CourseOfferingsManager cManager = new CourseOfferingsManager();

                List<CoursesHeader> list = new List<CoursesHeader>();

                list = cManager.ListByCurriculumsByIDandSubject(id);
                if (list != null && list.Count > 0)
                {
                    list = list.OrderBy(x => x.FKSubjectID).ToList();
                }

                return PartialView(list);
            }


        }
        public ActionResult jeeMontly()
        {
            WebHelper web = new WebHelper();
            bool yes = web.isSiteDisable();
            if (yes == true)
            {
                return RedirectToAction("SiteDisable", "Home");
            }
            else
            {
                CourseOfferingsManager cManager = new CourseOfferingsManager();

                List<CoursesHeader> list = new List<CoursesHeader>();

                list = cManager.ListByCurriculums(Config.JEEID);
                if (list != null && list.Count > 0)
                {
                    list = list.OrderBy(x => x.FKSubjectID).ToList();
                }

                return PartialView(list);
            }


        }

        public ActionResult LoadBySubject(Int64 SubjectID)
        {
            WebHelper web = new WebHelper();
            bool yes = web.isSiteDisable();
            if (yes == true)
            {
                return RedirectToAction("SiteDisable", "Home");
            }
            else
            {
                CourseOfferingsManager cManager = new CourseOfferingsManager();

                List<CoursesHeader> list = new List<CoursesHeader>();

                list = cManager.ListBySubject(SubjectID);
                if (list != null && list.Count > 0)
                {
                    list = list.OrderBy(x => x.FKSubjectID).ToList();
                }

                return PartialView(list);
            }
            

        }

        public ActionResult CourseDetail(Int64 CourseID)
        {
            WebHelper web = new WebHelper();
            bool yes = web.isSiteDisable();
            if (yes == true)
            {
                return RedirectToAction("SiteDisable", "Home");
            }
            else
            {
                try
                {
                    if (CourseID > 0)
                    {

                        CourseOfferingsManager cManager = new CourseOfferingsManager();
                        CoursesHeader Course = new CoursesHeader();
                        Course = cManager.GetCourseDetail(CourseID);

                        return View(Course);

                    }
                    else
                    {
                        return RedirectToAction("ExploreCourses");
                    }

                }
                catch (Exception)
                {
                    throw;
                }
            }
            
        }


        public ActionResult EnrollStudent(Int64 CourseID)
        {
            WebHelper web = new WebHelper();
            bool yes = web.isSiteDisable();
            if (yes == true)
            {
                return RedirectToAction("SiteDisable", "Home");
            }
            else
            {
                string ReturnUrl = Request.Url.AbsoluteUri;

                if (Config.CurrentUser == 0)
                    return RedirectToAction("Login", "Account", new { area = "", @ReturnUrl = ReturnUrl });

                if (Config.User.UserType != (int)Enumerations.UserType.Student)
                    return RedirectToAction("UnAuthorizeAccess", "Home", new { area = "" });

                try
                {
                    if (CourseID > 0)
                    {

                        CourseOfferingsManager cManager = new CourseOfferingsManager();

                        CourseClashTest obj = new CourseClashTest();

                        obj = cManager.AddCourseParticipant(CourseID, Config.CurrentUser);

                        if (obj.InsertStatus > 0)
                        {


                            // Add Activity of Sending Session Request


                            UsersActivityManager uManager = new UsersActivityManager();
                            UserActivities uActivity = new UserActivities();
                            uActivity.FKUserID = Config.CurrentUser;
                            uActivity.ActionPerformed = "You enrolled yourself for course : " + obj.CourseName;
                            Int64 ID = uManager.AddActivity(uActivity);

                            ViewBag.Message = "WOHO.. You Succesfully enrolled.. !";
                            ViewBag.Status = 0;
                            return View();
                        }
                        else if (obj.InsertStatus == 0)
                        {
                            ViewBag.Message = "You are already enrolled in this course";
                            ViewBag.Status = 0;
                            return View();
                        }
                        else if (obj.InsertStatus == -1)
                        {
                            ViewBag.Message = "You cannot register for this course because we find class of classes with course : " + obj.CourseName;
                            ViewBag.Status = 2;
                            return View();
                        }
                    }
                    else
                    {
                        return RedirectToAction("ExploreCourses");
                    }
                    return RedirectToAction("ExploreCourses");

                }
                catch (Exception)
                {
                    throw;
                }
            }


        }


        [AllowAnonymous]
        public JsonResult GradesList()
        {
    
                CurriculumsManagers manager = new CurriculumsManagers();
                IEnumerable<SelectListItem> ListOfGrades = manager.dListOfGrades();
                return Json(
                     ListOfGrades, JsonRequestBehavior.AllowGet
                );
          
          
        }

        [AllowAnonymous]
        public JsonResult dSubjectsList(Int64 GradeID)
        {
            CurriculumsManagers manager = new CurriculumsManagers();

            if (GradeID > 0)
            {
                IEnumerable<SelectListItem> ListOfGrades = manager.dListOfSubjects(GradeID);
                return Json(
                     ListOfGrades, JsonRequestBehavior.AllowGet
                );
            }

            return Json(
                null,
                JsonRequestBehavior.AllowGet
            );
        }



        public JsonResult AddViewCount(Int64 ResourceID)
        {
            if (ResourceID > 0)
            {

                FreeResourcesManager fManager = new FreeResourcesManager();
                bool status = fManager.AddViewCount(ResourceID);


                return Json(
          null,
          JsonRequestBehavior.AllowGet
      );
            }
            else
            {
                return Json(
                null,
                JsonRequestBehavior.AllowGet
            );
            }

        }


        [AllowAnonymous]
        public JsonResult CFGradesList(Int64 CurriculumID)
        {

            if (CurriculumID > 0)
            {
                CurriculumsManagers manager = new CurriculumsManagers();
                IEnumerable<SelectListItem> ListOfGrades = manager.dListOfGrades(CurriculumID);
                return Json(
                     ListOfGrades, JsonRequestBehavior.AllowGet
                );
            }
            return Json(
                null,
                JsonRequestBehavior.AllowGet
            );
        }

        [AllowAnonymous]
        public JsonResult CFSubjectsList(Int64 GradeID)
        {

            if (GradeID > 0)
            {
                CurriculumsManagers manager = new CurriculumsManagers();
                IEnumerable<SelectListItem> ListOfGrades = manager.dListOfSubjects(GradeID);
                return Json(
                     ListOfGrades, JsonRequestBehavior.AllowGet
                );
            }
            return Json(
                null,
                JsonRequestBehavior.AllowGet
            );
        }


		public ActionResult WhiteBoard(string p)
		{
			ViewData["IsNewGroup"] = false;
			if (string.IsNullOrWhiteSpace(p))
			{
				Guid g = Guid.NewGuid();
				p = Convert.ToBase64String(g.ToByteArray());
				p = p.Replace("=", "");
				p = p.Replace("+", "");
				ViewData["IsNewGroup"] = true;
				ViewData["url"] = Request.Url.AbsoluteUri.ToString() + "?p=" + p;
			}
			else
			{
				ViewData["url"] = Request.Url.AbsoluteUri.ToString();
			}
			ViewData["GroupName"] = p;
			ViewBag.GroupName = p;
			return View();
		}



	}


    
}