﻿using BLL.BusinessManagers;
using BLL.BusinessManagers.SessionsManager;
using BLL.BusinessManagers.WebRTCManager;
using BLL.Helpers;
using DAL.Entities;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using Tutoring.Models;

namespace Tutoring.Controllers
{
    public class WhiteboardController : Controller
    {
        // GET: Whiteboard


        [OutputCache(NoStore = true, Duration = 1, VaryByParam = "*")]
        public ActionResult index(string link, Int64 SessionID)
        {
            WebHelper web = new WebHelper();
            bool yes = web.isSiteDisable();
            if (yes == true)
            {
                return RedirectToAction("SiteDisable", "Home");
            }
            else
            {
                string ReturnUrl = Request.Url.AbsoluteUri;

                if (Config.CurrentUser == 0)
                    return RedirectToAction("Login", "Account", new { area = "", @ReturnUrl = ReturnUrl });
                // Sessions model = new Sessions();

                if (!string.IsNullOrEmpty(link) && SessionID > 0)
                {

                    SessionManager manager = new SessionManager();
                    bool canJoin = manager.CanJoinSession(link, Config.CurrentUser, SessionID);
                    if (canJoin)
                    {
                        Sessions model = manager.GetSessionDetail(SessionID);
                        CustomTimeZone czone = new CustomTimeZone();
                        DateTime EndTime = model.TrialHappenDate.AddHours(1);

                        // Set timer
                        double span = (EndTime - czone.DateTimeNow()).TotalMinutes;
                        var time = czone.DateTimeNow().AddMinutes(1);
                        ViewBag.EndDate = span; //Convert.ToDateTime(time).ToString("dd-MM-yyyy h:mm:ss tt");

                        return View(model);
                    }
                    else
                    {
                        return RedirectToAction("SessionUnAuthorizeAccess", "Home", new { area = "", message = "abc" });
                    }


                }
                else
                {
                    return RedirectToAction("UnAuthorizeAccess", "Home", new { area = "" });

                }

                //return RedirectToAction("UnAuthorizeAccess", "Home", new { area = "" });
                // return View(model);
            }


        }


        public ActionResult cJoinIndex(string link, Int64 CID, Int64 SID)
        {
            WebHelper web = new WebHelper();
            bool yes = web.isSiteDisable();
            if (yes == true)
            {
                return RedirectToAction("SiteDisable", "Home");
            }
            else
            {
                string ReturnUrl = Request.Url.AbsoluteUri;

                if (Config.CurrentUser == 0)
                    return RedirectToAction("Login", "Account", new { area = "", @ReturnUrl = ReturnUrl });
                // Sessions model = new Sessions();

                if (!string.IsNullOrEmpty(link) && CID > 0 && SID > 0)
                {

                    CourseOfferingsManager cManager = new CourseOfferingsManager();
                    SessionManager manager = new SessionManager();
                    bool canJoin = manager.CanCourseJoinSession(CID, Config.CurrentUser, SID);

                    if (canJoin)
                    {

                        CoursesHeader obj = new CoursesHeader();
                        obj = cManager.GetCourseDetail(CID);

                        var courseSchedule = cManager.GetCourseSchedule(SID);

                        CustomTimeZone czone = new CustomTimeZone();
                        DateTime EndTime = courseSchedule.CourseSessionDate.AddHours(1);

                        // Set timer
                        double span = (EndTime - czone.DateTimeNow()).TotalMinutes;
                        var time = czone.DateTimeNow().AddMinutes(1);
                        ViewBag.EndDate = span; //Convert.ToDateTime(time).ToString("dd-MM-yyyy h:mm:ss tt");
                        ViewBag.ScheduleID = SID;
                        return View(obj);

                    }
                    else
                    {
                        return RedirectToAction("UnAuthorizeAccess", "Home", new { area = "" });
                    }


                }
                else
                {
                    return RedirectToAction("UnAuthorizeAccess", "Home", new { area = "" });

                }
            }
            

         
        }
        public ActionResult CheckJoinSession(string name)
        {
            WebHelper web = new WebHelper();
            bool yes = web.isSiteDisable();
            if (yes == true)
            {
                return RedirectToAction("SiteDisable", "Home");
            }
            else
            {
                // Jo Session id mere pass I he us k against check 
                // 1. Session ho raha he k nahen is sessionlink k against
                // 2. Sirf 1 taecher or 1 hi student hen k nahen

                if (Config.CurrentUser == 0)
                    return RedirectToAction("Login", "Account", new { area = "" });

                return View();
            }
            
        }
        public ActionResult studentview()
        {
            WebHelper web = new WebHelper();
            bool yes = web.isSiteDisable();
            if (yes == true)
            {
                return RedirectToAction("SiteDisable", "Home");
            }
            else
            {
                string ReturnUrl = Request.Url.AbsoluteUri;
                if (Config.CurrentUser == 0)
                    return RedirectToAction("Login", "Account", new { area = "", @ReturnUrl = ReturnUrl });

                if (Config.User.UserType == (int)Enumerations.UserType.Teacher)
                    return RedirectToAction("UnAuthorizeAccess", "Home", new { area = "" });

                return View();
            }
            
        }

        public ActionResult testboard(string link, Int64 SessionID)
        {
            WebHelper web = new WebHelper();
            bool yes = web.isSiteDisable();
            if (yes == true)
            {
                return RedirectToAction("SiteDisable", "Home");
            }
            else
            {
                if (Config.CurrentUser == 0)
                    return RedirectToAction("Login", "Account", new { area = "" });

                SessionManager manager = new SessionManager();
                bool canJoin = manager.CanJoinSession(link, Config.CurrentUser, SessionID);
                if (canJoin)
                {
                    CustomTimeZone time = new CustomTimeZone();
                    DateTime joinTime = time.DateTimeNow();
                    SessionParticipants participants = new SessionParticipants();
                    participants.FKSessionID = SessionID;
                    participants.FKStudentID = Config.CurrentUser;
                    participants.IsActive = true;
                    participants.JoinDate = joinTime;
                    participants.LeaveDate = joinTime;

                    Int64 status = manager.AddSessionParticipant(participants);
                    if (status > 0)
                    {
                        if (Config.User.UserType == (int)Enumerations.UserType.Teacher)
                        {
                            Int64 val = manager.AddSessionDuration(SessionID);
                            PostBackReturn model = new PostBackReturn();
                            model = ConsumeExternalAPI(link);
                            if (!string.IsNullOrEmpty(model._id))
                            {
                                WebRTCRooms roomObj = new WebRTCRooms();
                                roomObj.FKSessionID = SessionID;
                                roomObj.RoomID = model._id;
                                roomObj.IsActive = true;
                                roomObj.IsCourse = (int)Enumerations.OneOrMany.Session;
                                WebRTCManager wManager = new WebRTCManager();
                                Int64 ID = wManager.AddRoom(roomObj);
                            }

                        }

                        return RedirectToAction("index", new { link = link, SessionID = SessionID });
                    }
                    else
                    {
                        return RedirectToAction("SessionUnAuthorizeAccess", "Home", new { area = "", message = "abc" });
                    }
                }
                else
                {
                    return RedirectToAction("UnAuthorizeAccess", "Home", new { area = "" });
                }
            }

        }


        public ActionResult ctestboard(Int64 CID, Int64 SID)
        {
            WebHelper web = new WebHelper();
            bool yes = web.isSiteDisable();
            if (yes == true)
            {
                return RedirectToAction("SiteDisable", "Home");
            }
            else
            {
                if (Config.CurrentUser == 0)
                    return RedirectToAction("Login", "Account", new { area = "" });

                SessionManager manager = new SessionManager();
                bool canJoin = manager.CanCourseJoinSession(CID, Config.CurrentUser, SID);
                if (canJoin)
                {
                    CustomTimeZone time = new CustomTimeZone();
                    DateTime joinTime = time.DateTimeNow();

                    CourseOfferingsManager cManager = new CourseOfferingsManager();

                    CourseSessionsDuration participants = new CourseSessionsDuration();
                    participants.FKJoinedBy = Config.CurrentUser;
                    participants.FKCourseID = CID;
                    participants.EndTime = joinTime;
                    participants.StartTime = joinTime;
                    participants.FKStudentCourseScheduleID = SID;
                    participants.IsActive = true;

                    Int64 status = cManager.AddCourseSessionDuration(participants, Config.CurrentUser);

                    string link = null;
                    if (status > 0)
                    {
                        link = cManager.GetCourseLink(CID);
                        if (Config.User.UserType == (int)Enumerations.UserType.Teacher)
                        {

                            PostBackReturn model = new PostBackReturn();

                            if (link != null)
                            {
                                //  model = ConsumeExternalAPI(link);
                            }

                            // if (!string.IsNullOrEmpty(model._id))
                            // {
                            WebRTCRooms roomObj = new WebRTCRooms();
                            roomObj.FKSessionID = CID;
                            roomObj.RoomID = model._id;
                            roomObj.IsCourse = (int)Enumerations.OneOrMany.Course;
                            roomObj.IsActive = true;
                            WebRTCManager wManager = new WebRTCManager();
                            Int64 ID = wManager.AddRoom(roomObj);

                            //}

                        }

                        return RedirectToAction("cJoinIndex", new { @link = link, @CID = CID, @SID = SID });

                    }
                    else
                    {
                        return RedirectToAction("UnAuthorizeAccess", "Home", new { area = "" });
                    }
                }
                else
                {
                    return RedirectToAction("UnAuthorizeAccess", "Home", new { area = "" });
                }
            }

        }





        public ActionResult Download(string FileName)
        {
            WebHelper web = new WebHelper();
            bool yes = web.isSiteDisable();
            if (yes == true)
            {
                return RedirectToAction("SiteDisable", "Home");
            }
            else
            {
                // = "";
                Response.AppendHeader("Content-Disposition", "inline;" + FileName);
                return File("~/Extensions/" + FileName, "application/zip", "ext.zip");
            }
            
        }


        public ActionResult HandlingSignalR()
        {
            WebHelper web = new WebHelper();
            bool yes = web.isSiteDisable();
            if (yes == true)
            {
                return RedirectToAction("SiteDisable", "Home");
            }
            else
            {
                Guid fId = Guid.NewGuid();
                Guid sId = Guid.NewGuid();
                ViewBag.FirstGroup = fId;
                ViewBag.SecondGroup = sId;
                return View();
            }
            
        }

        public JsonResult SaveImage()
        {

            var file = Request.Files[0];
            if (file != null)
            {
                var fileName = Path.GetFileName(file.FileName);
                var extension = Path.GetExtension(file.FileName).ToLower();
                Guid id = Guid.NewGuid();
                string path = Server.MapPath("~/DrawingImages/" + id + extension);
                file.SaveAs(path);
                string imagePath = Config.WebsiteURL + "/DrawingImages/" + id.ToString() + extension;
                return Json(new { path = imagePath }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(new { path = "" }, JsonRequestBehavior.AllowGet);
            }

        }


        public ActionResult UpdateSession(Int64 SID,string link)
        {
            WebHelper web = new WebHelper();
            bool yes = web.isSiteDisable();
            if (yes == true)
            {
                return RedirectToAction("SiteDisable", "Home");
            }
            else
            {
                if (Config.CurrentUser == 0)
                    return RedirectToAction("Login", "Account", new { area = "" });

                SessionManager manager = new SessionManager();
                bool canJoin = manager.CanJoinSession(link, Config.CurrentUser, SID);
                if (canJoin)
                {
                    // need to update session logic here 

                    CustomTimeZone czone = new CustomTimeZone();

                    bool status = manager.UpdateSession(SID);
                    if (status == true)
                    {
                        return RedirectToAction("Index", "Teacher", new { area = "Teacher" });
                        //  TempData["Message"] = "Session ended Succesfully";
                        //  TempData["Status"] = 2;               
                    }
                    else
                    {
                        return RedirectToAction("Index", "Teacher", new { area = "Teacher" });
                        //  TempData["Message"] = "Something went wrong.";
                        //  TempData["Status"] = 3;
                    }
                }
                else
                {
                    return RedirectToAction("Index", "Teacher", new { area = "Teacher" });
                    //return RedirectToAction("UnAuthorizeAccess", "Home", new { area = "" });
                }
            }
            

          

        }


        public ActionResult UpdateCourseSchedule(Int64 SID)
        {
            WebHelper web = new WebHelper();
            bool yes = web.isSiteDisable();
            if (yes == true)
            {
                return RedirectToAction("SiteDisable", "Home");
            }
            else
            {
                if (Config.CurrentUser == 0)
                    return RedirectToAction("Login", "Account", new { area = "" });

                if (Config.User.UserType != (int)Enumerations.UserType.Teacher)
                    return RedirectToAction("UnAuthorizeAccess", "Home", new { area = "" });



                CourseOfferingsManager cManager = new CourseOfferingsManager();

                // need to update session logic here 

                CustomTimeZone czone = new CustomTimeZone();

                bool status = cManager.UpdateCourseSchedule(SID);
                if (status == true)
                {

                }
                else
                {

                }


                return RedirectToAction("Index", "Teacher", new { area = "Teacher" });
            }
            

        }



        public PostBackReturn ConsumeExternalAPI(string RName)
        {
            
            ServicePointManager.ServerCertificateValidationCallback += (sender, certificate, chain, sslPolicyErrors) => true;
            using (WebClient client = new WebClient())
            {                
                byte[] response =
                client.UploadValues("https://media.rudraa.org:3004/createRoom", new NameValueCollection()
                {
                   { "roomName", RName },
                  
                });
                string result = System.Text.Encoding.UTF8.GetString(response);
                var serializer = new JavaScriptSerializer();
                var model = serializer.Deserialize<PostBackReturn>(result);
                return model;
            }

            return null;
        }

    }
}