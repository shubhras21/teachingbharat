﻿using BLL.BusinessManagers.SessionsManager;
using BLL.BusinessManagers.WebRTCManager;
using BLL.Helpers;
using DAL.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Cors;
using System.Web.Script.Serialization;
using Tutoring.Models;

namespace Tutoring.Controllers
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class ServicesController : ApiController
    {

        // GET: api/Services/5
        //public JSONModel Get(int id)
        //{
        //    JSONModel model = new JSONModel();
        //    model.Data = 123;
        //    model.Count = 1;
        //    model.Message = "You got id";
        //    return model;
        //}

        // POST: api/Services
        public void Post([FromBody]string value)
        {
        }

        // PUT: api/Services/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE: api/Services/5
        public void Delete(int id)
        {
        }


        [HttpGet]
        public Object CurrentUserInfo()
        {
            CurrentUserInformation user = new CurrentUserInformation();
            if (Config.CurrentUser != 0)
            {
                user.UserID = Config.CurrentUser;
                user.Name = Config.UsersProfile.FirstName + " " + Config.UsersProfile.LastName;
                user.Role = Enum.GetName(typeof(Enumerations.UserType), Config.User.UserType);
                user.status = true;
                return user;
            }
            else
            {
                user.status = false;
                user.UserID = 0;
                user.Name = null;
                user.Role = null;
                return user;
            }

        }


        [HttpGet]
        public Object RoomID(Int64? UserID)
        {

            if (UserID > 0)
            {
                SessionManager manager = new SessionManager();
                Sessions obj = manager.CanUserJoinSession(Config.CurrentUser);
                if (obj != null)
                {
                    WebRTCManager wmanager = new WebRTCManager();
                    WebRTCRooms room = wmanager.GetRoomDetailBySessionID(obj.TrialSessionID);
                    if (room != null)
                    {
                        return Json(new { status = true, roomID = room.RoomID });
                    }
                    else
                    {
                        return Json(new { status = false, roomID = 0 });
                    }

                }
                else
                {
                    return Json(new { status = false, roomID = 0 });
                }
            }
            else
            {
                return Json(new { status = false, roomID = 0 });
            }

        }


        [HttpGet]
        public Object CourseRoomID(Int64? UserID)
        {

            if (UserID > 0)
            {
                SessionManager manager = new SessionManager();
                bool obj = manager.CanCourseJoinSession(10004, (Int64)UserID, 10043);
                if (obj ==true)
                {
                    WebRTCManager wmanager = new WebRTCManager();
                    WebRTCRooms room = wmanager.GetRoomDetailByCourseID(10004, (Int64)UserID);
                    if (room != null)
                    {
                        return Json(new { status = true, roomID = room.RoomID });
                    }
                    else
                    {
                        return Json(new { status = false, roomID = 0 });
                    }

                }
                else
                {
                    return Json(new { status = false, roomID = 0 });
                }
            }
            else
            {
                return Json(new { status = false, roomID = 0 });
            }

        }



        [HttpGet]
        public Object CourseRoomIDN(Int64? UserID)
        {

            if (UserID > 0)
            {
                SessionManager manager = new SessionManager();
                bool obj = manager.CanCourseJoinSession(10004, Config.CurrentUser, 10043);
                if (obj == true)
                {
                    WebRTCManager wmanager = new WebRTCManager();
                    WebRTCRooms room = wmanager.GetRoomDetailByCourseID(10004, Config.CurrentUser);
                    if (room != null)
                    {
                        return Json(new { status = true, roomID = room.RoomID });
                    }
                    else
                    {
                        return Json(new { status = false, roomID = 0 });
                    }

                }
                else
                {
                    return Json(new { status = false, roomID = 0 });
                }
            }
            else
            {
                return Json(new { status = false, roomID = 0 });
            }

        }


        [HttpPost]
        public JSONModel MyTesting([FromBody]string text)
        {
            JSONModel model = new JSONModel();
            model.Data = text;
            return model;
        }


    }
}
