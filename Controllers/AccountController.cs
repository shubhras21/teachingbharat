﻿using System;
using System.Globalization;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using BLL.BusinessManagers.UserManager;
using BLL.Helpers;
using BLL.BusinessModels.UserModels;
using DAL.MasterEntity;
using System.Web.Security;
using DAL.Entities;
using BLL.BusinessManagers;
using BLL.BusinessManagers.AdminManager;
using BLL.EmailTemplate;
using System.IO;
using BLL.BusinessModels.TeacherModels;
using BLL.BusinessModels.AdminModels;
using BLL;
using BLL.BusinessManagers.TeacherManager;
using BLL.BusinessModels.StudentModels;
using System.Net.Mail;

namespace Tutoring.Controllers
{
    public class AccountController : Controller
    {
        public UserBLL userBll = new UserBLL();
        UsersActivityManager activityMgr = new UsersActivityManager();
        UserActivities obj_Activity = new UserActivities();
        TeacherManager teacherMgr = new TeacherManager();
        StudentEmailTemplateModel studentTemplateModel = new StudentEmailTemplateModel();
        MyDbContext context = new MyDbContext();

        private IAuthenticationManager AuthenticationManager
        {
            get
            {
                return HttpContext.GetOwinContext().Authentication;
            }
        }


        #region [Login]

        public ActionResult Login(string ReturnUrl)
        {
                //Hello
                ViewBag.ReturnURL = ReturnUrl;
                ViewBag.ConfirmationMessage = TempData["msg"];

                ViewBag.Status = (TempData["Status"] != null) ? (int)TempData["Status"] : 0;
                ViewBag.Message = (TempData["Message"] != null) ? (string)TempData["Message"] : null;

                UserBLL uBLL = new UserBLL();

                if (Config.CurrentUser > 0)
                {
                    bool status = uBLL.UpdateOnlineStatus(Config.CurrentUser, false);

                    obj_Activity.FKUserID = Config.CurrentUser;
                    obj_Activity.ActionPerformed = "Logout request from ";
                    obj_Activity.ActivitySource = (int)Enumerations.ActivitySource.BacksideActivity;
                    Int64 ID = activityMgr.AddActivity(obj_Activity);
                }
                Session.Clear();

                return View();
        }

        [HttpPost]
        public ActionResult Login(LoginViewModel model, string ReturnUrl)
        {

                ViewBag.Status = 0;
                ViewBag.Message = null;

                if (ModelState.IsValid)
                {
                    var isAuthencated = UserManager.CanLogin(model.LoginEmail, model.Password, model.ExternalLoginId);
                    if (isAuthencated != null)
                    {
                        Session.Timeout = 60;
                        FormsAuthentication.SetAuthCookie(isAuthencated.LoginEmail, true);
                        Config.CurrentUser = isAuthencated.UserID;
                        Config.User = isAuthencated;
                        Config.UsersProfile = userBll.GetUserProfile(isAuthencated.UserID);
                        string csvString = string.Join(",", isAuthencated.UserID, Config.UsersProfile.FirstName + " " + Config.UsersProfile.LastName, Enum.GetName(typeof(Enumerations.UserType), Config.User.UserType));
                        TempData["UserData"] = csvString;
                        if (isAuthencated.UserType == (int)Enumerations.UserType.Super || isAuthencated.UserType == (int)Enumerations.UserType.Admin)
                        {

                            if (!string.IsNullOrEmpty(ReturnUrl))
                            {

                                return Redirect(ReturnUrl);
                            }
                            else
                            {
                                return RedirectToAction("Index", "Admin", new { area = "Admin" });
                            }

                        }
                        else if (HelperMethods.isSiteDisable() == true)
                        {
                            return RedirectToAction("SiteDisable", "Home");
                        }
                        else
                        {
                            if (isAuthencated.UserType == (int)Enumerations.UserType.Student)
                            {
                                if (!string.IsNullOrEmpty(ReturnUrl))
                                {
                                    return Redirect(ReturnUrl);
                                }
                                else
                                {
                                    if (isAuthencated.IsVerified != (int)Enumerations.VerificationType.NotVerified)
                                       // return RedirectToAction("step_1", "Student", new { area = "Student" });
                                       return RedirectToAction("Index", "Student", new { area = "Student" });
                                    else
                                        ModelState.AddModelError("", " Please verify your email before login. ");
                                }
                            }
                            if (isAuthencated.UserType == (int)Enumerations.UserType.Teacher)
                            {
                                if (isAuthencated.UserStatus != (int)Enumerations.UserStatus.IsBlocked && isAuthencated.UserStatus != (int)Enumerations.UserStatus.NotApproved && isAuthencated.UserStatus != (int)Enumerations.UserStatus.Pending && isAuthencated.UserStatus != (int)Enumerations.UserStatus.IsDeleted)
                                {
                                    if (!string.IsNullOrEmpty(ReturnUrl))
                                    {
                                        //  ReturnUrl = Server.UrlEncode(Request.UrlReferrer.PathAndQuery);
                                        return Redirect(ReturnUrl);
                                    }
                                    else
                                    {
                                        return RedirectToAction("Index", "Teacher", new { area = "Teacher" });
                                    }
                                }
                                ModelState.AddModelError("", "Sorry you are not allowed to Login");
                            }
                            if (isAuthencated.UserType == (int)Enumerations.UserType.Parent)
                            {
                                if (!string.IsNullOrEmpty(ReturnUrl))
                                {
                                    ReturnUrl = Server.UrlEncode(Request.UrlReferrer.PathAndQuery);
                                    return Redirect(ReturnUrl);
                                }
                                else
                                {
                                    return RedirectToAction("Index", "Parent", new { area = "Parent" });
                                }
                            }
                        }
                    }
                    else
                    {
                        ModelState.AddModelError("", "User Name or Password is incorrect");
                    }
                }
                return View(model);
        }


        #endregion



        #region [Registration]


        public ActionResult Signup(string msg)
        {
            WebHelper web = new WebHelper();
            bool yes = web.isSiteDisable();
            if (yes == true)
            {
                return RedirectToAction("SiteDisable", "Home");
            }
            else
            {
                var model = UserManager.StudentRegisterhelpers();
                ViewBag.Message = msg;
                return View(model);
            }
            
        }

        [HttpPost]
        public ActionResult Signup(StudentRegisterViewModel model)
        {
            WebHelper web = new WebHelper();
            bool yes = web.isSiteDisable();
            if (yes == true)
            {
                return RedirectToAction("SiteDisable", "Home");
            }
            else
            {
                try
                {
                    if (ModelState.IsValid)
                    {
                        //  url = "http://rudraa.org/Account/EmailVerification?userid=" + id;
                      
                        var id = UserManager.UserSignup(model);
                        string[] useremail = model.LoginEmail.Split('@');
                        var username = useremail[0];
                        if (id != 0)
                        {
                            if (ActionsManager.CheckActionStatus((int)Enumerations.Actions.Registeration))
                            {
                                var url = "";
                                if (Request.Url != null)
                                    url = Config.WebsiteURL + "/Account/EmailVerification?userid=" + id;

                                studentTemplateModel.StudentID = id;
                                studentTemplateModel.StudentFullName = username;
                                studentTemplateModel.Url = url;
                                var emailtemplatesModelData = EmailTemplatesManager.GetEmailTemplate((int)Enumerations.EmailTemplates.EmailTemplateForStudentRegistration);
                                if (emailtemplatesModelData != null)
                                {
                                    studentTemplateModel.emailBodyWrittenByAdmin = emailtemplatesModelData.Body;
                                }
                                studentTemplateModel.emailBody = GetEmailTemplateAsString("_EmailTemplateForStudents", studentTemplateModel);

                               // var emailresult = UserManager.SendVerificationEmail(model.LoginEmail, studentTemplateModel.emailBody);
								NotificationTable notify = new NotificationTable();
                                notify.UserID = id.ToString();
                                notify.Comments = "New Student got Registered " + "   " + username;
                                notify.NotificationDateTime = DateTime.Now;
                                notify.IsSeen = false;
                                context.Notifications.Add(notify);
                                context.SaveChanges();
								//if (emailresult == (int)Enumerations.ResponsHelpers.Created)
								//	ViewBag.Message = "Email has been sent please verify your account.";
								//else
								//	ViewBag.Message = "Email Sending Failed";
							}

                            else
                            {
                                ViewBag.Message = "Your registration request has been sent to Admin . Admin will activate you soon. Thank you";
                                NotificationTable notify = new NotificationTable();
                                notify.UserID = id.ToString();
                                notify.Comments = "New Student got Registered Waiting for Activation" + "   " + username;
                                notify.NotificationDateTime = DateTime.Now;
                                notify.IsSeen = false;
                                context.Notifications.Add(notify);
                                context.SaveChanges();
                            }



                            obj_Activity.ActionPerformed = "New Student got Registered";
                            obj_Activity.FKUserID = id;
                            obj_Activity.ActivitySource = (int)Enumerations.ActivitySource.BacksideActivity;
                            var activityId = activityMgr.AddActivity(obj_Activity);
                            @ViewBag.Message = "New Student got Registered";
                        }
                        else if (id == 0)
                        {
                            ViewBag.Message = "Email already exists.";
                        }

                        return RedirectToAction("Signup", new { @msg = @ViewBag.Message });
                    }

                    return View(model);
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }      
        }


		public int SendEmail(string Email,string Body)
		{
			try
			{
				MailMessage mail = new MailMessage();
				SmtpClient SmtpServer = new SmtpClient("smtp.gmail.com");

				mail.From = new MailAddress("rudraa.org.olt@gmail.com");
				mail.To.Add(Email);
				mail.Subject = "Test Mail";
				mail.Body = Body;

				SmtpServer.Port = 587;
				SmtpServer.Credentials = new System.Net.NetworkCredential("rudraa.org.olt@gmail.com", "boyboyboy");
				SmtpServer.EnableSsl = true;

				SmtpServer.Send(mail);
				return (int)Enumerations.ResponsHelpers.Created;
			}
			catch
			{
				return (int)Enumerations.ResponsHelpers.EmailSentFail;
			}
		
		}

        #endregion


        #region [TeacherRegistration]

        public ActionResult TeacherRegistration()
        {
            WebHelper web = new WebHelper();
            bool yes = web.isSiteDisable();
            if (yes == true)
            {
                return RedirectToAction("SiteDisable", "Home");
            }
            else
            {
                var model = UserManager.TeacherRegisterhelpers();
                ViewBag.Message = 0;
                return View(model);
            }
            
        }

        [HttpPost]
        public ActionResult TeacherRegistration(TeacherRegisterationViewModel modeldata)
        {
            WebHelper web = new WebHelper();
            bool yes = web.isSiteDisable();
            if (yes == true)
            {
                return RedirectToAction("SiteDisable", "Home");
            }
            else
            {
                TeacherEmailTemplateModel obj_teacherEmailTemplate = new TeacherEmailTemplateModel();
                EmailTemplatesModel obj_emailTemplateModel = new EmailTemplatesModel();

                var model = UserManager.TeacherRegisterhelpers();
                if (ModelState.IsValid)
                {
                    var newTeacher = UserManager.TeacherRegisteration(modeldata);
                    ViewBag.Message = newTeacher.Response;
                    if (newTeacher.Response == (int)Enumerations.ResponsHelpers.Created)
                    {
                        if (ActionsManager.CheckActionStatus((int)Enumerations.Actions.Registeration))
                        {
                            obj_teacherEmailTemplate.TeacherID = newTeacher.TeacherID;
                            obj_teacherEmailTemplate.TeacherFullName = newTeacher.FirstName + " " + newTeacher.LastName;
                            obj_teacherEmailTemplate.LoginEmail = newTeacher.LoginEmail;
                            obj_teacherEmailTemplate.Password = newTeacher.Password;

                            using (var ctx = new MyDbContext())
                            {

                                var emailtemplateDetail = EmailTemplatesManager.GetEmailTemplate((int)Enumerations.EmailTemplates.EmailTemplateForTeacherRegistration);
                                if (emailtemplateDetail != null)
                                {
                                    obj_teacherEmailTemplate.bodyWrittenFromAdmin = emailtemplateDetail.Body;
                                }
                            }

                            obj_teacherEmailTemplate.body = GetEmailTemplateAsString("_EmailTemplateForTeachers", obj_teacherEmailTemplate);

                            bool emailresult= BLL.EmailTemplate.EmailTemplate.TeacherRegistrationEmail(obj_teacherEmailTemplate.LoginEmail, obj_teacherEmailTemplate.body);
							if (emailresult == true)
								ViewBag.SucessRegister = "Email has been sent please verify your account.";
							else
								ViewBag.SucessRegister = "Email Sending Failed";
							NotificationTable notify = new NotificationTable();
                            notify.UserID = newTeacher.TeacherID.ToString();
                            notify.Comments = "New Teacher Registered " + "   " + newTeacher.FirstName + " " + newTeacher.LastName;
                            notify.NotificationDateTime = DateTime.Now;
                            notify.IsSeen = false;
                            context.Notifications.Add(notify);
                            context.SaveChanges();

                        }
                        else
                        {
                            string msg = "Thank you for choosing us. Our admin will contact shortly.";
                            obj_Activity.ActionPerformed = "New Teacher Registration";
                            obj_Activity.ActivitySource = (int)Enumerations.ActivitySource.FrontsideActivity;
                            obj_Activity.FKUserID = newTeacher.TeacherID;
                            activityMgr.AddActivity(obj_Activity);
                            NotificationTable notify = new NotificationTable();
                            notify.UserID = newTeacher.TeacherID.ToString();
                            notify.Comments = "New Teacher Registered & waiting for your response" + "   " + newTeacher.FirstName + " " + newTeacher.LastName;
                            notify.NotificationDateTime = DateTime.Now;
                            notify.IsSeen = false;
                            context.Notifications.Add(notify);
                            context.SaveChanges();
                            return RedirectToAction("Notification", "Home", new { @message = @msg });
                        }

                    }

                    return View(model);
                }
                else
                {
                    var errors = ModelState.Select(x => x.Value.Errors)
                           .Where(y => y.Count > 0)
                           .ToList();
                }
                return View(model);
            }
            
        }

        public string GetEmailTemplateAsString(string viewName, object model)
        {

            ViewData.Model = model;
            using (var sw = new StringWriter())
            {
                var viewResult = ViewEngines.Engines.FindPartialView(ControllerContext,
                                                                         viewName);
                var viewContext = new ViewContext(ControllerContext, viewResult.View,
                                             ViewData, TempData, sw);
                viewResult.View.Render(viewContext, sw);
                viewResult.ViewEngine.ReleaseView(ControllerContext, viewResult.View);
                return sw.GetStringBuilder().ToString();
            }
        }


        // [AllowAnonymous]
        //public JsonResult CitiesList(int id)
        //{
        //    MyDbContext context = new MyDbContext();

        //    if (id >0)
        //    {
        //        return Json(
        //           context.Cities.Where(x=>x.FKCountryID==id).Select(x => new { value = x.CityID, text = x.CityName}),
        //            JsonRequestBehavior.AllowGet
        //        );
        //    }
        //    return Json(
        //        null,
        //        JsonRequestBehavior.AllowGet
        //    );
        //}

        [AllowAnonymous]
        public JsonResult GradesList(int id)
        {
            MyDbContext context = new MyDbContext();

            if (id > 0)
            {
                return Json(
                   context.Grades.Where(x => x.FKCurriculumID == id && x.IsActive == true).Select(x => new { value = x.GradeID, text = x.GradeName }),
                    JsonRequestBehavior.AllowGet
                );
            }
            return Json(
                null,
                JsonRequestBehavior.AllowGet
            );
        }

        public JsonResult SubjectsList(int id)
        {
            MyDbContext context = new MyDbContext();

            if (id > 0)
            {
                return Json(
                  context.Subjects.Where(x => x.FKGradeID == id && x.IsActive == true).Select(x => new { value = x.SubjectID, text = x.SubjectName }),
                   JsonRequestBehavior.AllowGet
               );
            }
            return Json(
                null,
                JsonRequestBehavior.AllowGet
            );
        }

        #endregion

        [AllowAnonymous]
        public JsonResult CheckEmail(string LoginEmail)
        {
            MyDbContext context = new MyDbContext();
            return Json(!context.Users.Any(lo => lo.LoginEmail == LoginEmail), JsonRequestBehavior.AllowGet);
        }


        [HttpPost]
        public ActionResult LogOff()
        {
            WebHelper web = new WebHelper();
            bool yes = web.isSiteDisable();
            if (yes == true)
            {
                return RedirectToAction("SiteDisable", "Home");
            }
            else
            {
                UserBLL uBLL = new UserBLL();
                bool status = uBLL.UpdateOnlineStatus(Config.CurrentUser, false);
                Session.Clear();
                Config.CurrentUser = 0;
                Config.User = null;
                Config.UsersProfile = null;
                return RedirectToAction("Index", "Home");
            }
            
        }

        public ActionResult LogOut()
        {
            WebHelper web = new WebHelper();
            bool yes = web.isSiteDisable();
            if (yes == true)
            {
                return RedirectToAction("SiteDisable", "Home");
            }
            else
            {
                UserBLL uBLL = new UserBLL();
                bool status = uBLL.UpdateOnlineStatus(Config.CurrentUser, false);

                obj_Activity.FKUserID = Config.CurrentUser;
                obj_Activity.ActionPerformed = "Logout request from ";
                obj_Activity.ActivitySource = (int)Enumerations.ActivitySource.BacksideActivity;
                Int64 ID = activityMgr.AddActivity(obj_Activity);

                Session.Clear();
                Config.CurrentUser = 0;
                Config.User = null;
                Config.UsersProfile = null;
                return RedirectToAction("Index", "Home");
            }
            
        }

        public ActionResult EmailVerification(Int64? userid)
        {
            WebHelper web = new WebHelper();
            bool yes = web.isSiteDisable();
            if (yes == true)
            {
                return RedirectToAction("SiteDisable", "Home");
            }
            else
            {
                try
                {
                    DAL.Entities.Users user = new Users();
                    user.UserID = Convert.ToInt64(userid);
                    var result = UserManager.VerifiyEmail(userid);
                    if (result)
                        return View(user);
                    return RedirectToAction("EmailVerificationFailed");
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
            
        }

        public ActionResult EmailVerificationFailed()
        {
            WebHelper web = new WebHelper();
            bool yes = web.isSiteDisable();
            if (yes == true)
            {
                return RedirectToAction("SiteDisable", "Home");
            }
            else
            {
                try
                {
                    return View();
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
            
        }

        #region [FACEBOOK LOGIN]

        private Uri RedirectUri
        {
            get
            {
                var uribuilder = new UriBuilder(Request.Url);
                uribuilder.Query = null;
                uribuilder.Fragment = null;
                uribuilder.Path = Url.Action("FacebookCallback");
                return uribuilder.Uri;
            }
        }

		//[AllowAnonymous]
		//public ActionResult Facebook()
		//{
		//	var fb = new Facebook.FacebookClient();
		//	var loginurl = fb.GetLoginUrl(new
		//	{
		//		client_id = "1897652543847390",
		//		client_secret = "f02bc32da3bc10b4129fceeebbba9bef",
		//		redirect_uri = RedirectUri.AbsoluteUri,
		//		response_type = "code",
		//		scope = "email"
		//	});
		//	return Redirect(loginurl.AbsoluteUri);
		//}

        [HttpPost]
        public ActionResult FacebookLogin(string user)
        {
            WebHelper web = new WebHelper();
            bool yes = web.isSiteDisable();
            if (yes == true)
            {
                return RedirectToAction("SiteDisable", "Home");
            }
            else
            {
                Session["UserType"] = Convert.ToInt32(user);
                var fb = new Facebook.FacebookClient();
                var loginurl = fb.GetLoginUrl(new
                {
                    client_id = "1977001025962721",
                    client_secret = "7217bd9652d7fdd473e6c0384fcfa88c",
                    redirect_uri = RedirectUri.AbsoluteUri,
                    response_type = "code",
                    scope = "email"
                });
				return Json(new { result = "Redirect", url = loginurl.AbsoluteUri }, JsonRequestBehavior.AllowGet);
			}
            
        }

        public ActionResult FacebookCallback(string code)
        {
            WebHelper web = new WebHelper();
            bool yes = web.isSiteDisable();
            if (yes == true)
            {
                return RedirectToAction("SiteDisable", "Home");
            }
            else
            {
                Config.CurrentUser = 0;
                Config.User = null;
                Config.UsersProfile = null;

                var fb = new Facebook.FacebookClient();
                var model = new ExternalLoginViewModel();
                dynamic result = fb.Post("oauth/access_token", new
                {
                    client_id = "1977001025962721",
                    client_secret = "7217bd9652d7fdd473e6c0384fcfa88c",
                    redirect_uri = RedirectUri.AbsoluteUri,
                    code = code
                });

                var accessToken = result.access_token;
                fb.AccessToken = accessToken;
                dynamic userdetails = fb.Get("me?fields=link,id,first_name,currency,last_name,email,gender,locale,timezone,verified,picture,age_range"); var username = userdetails.email;
                if (userdetails.email == null)
                    username = userdetails.first_name + "_" + userdetails.id;

                model.EmailAddress = username;
                model.UserType = (int)Session["UserType"];
                string firstname = userdetails.first_name;
                string lastname = userdetails.last_name;
                string tokenid = userdetails.id;
                model.TokenId = tokenid;
                var resultsid = UserManager.UserExternalLogin(model);    // here token check from database 
                if (resultsid != 0)
                {
                    // means I need to Direct login no reg need because user is already registered 
                    var userdetailss = UserManager.getUserDetails(resultsid);
                    if (userdetailss.UserType != model.UserType)
                    {
                        TempData["Message"] = "you are already registered with us as different user";
                        TempData["Status"] = 3;
                        return RedirectToAction("Login");
                    }
                    var isAuthencated = UserManager.CanExternalLogin(userdetailss.LoginEmail, userdetailss.Password, userdetailss.ExternalLoginID);
                    if (isAuthencated != null)
                    {
                        FormsAuthentication.SetAuthCookie(isAuthencated.LoginEmail, true);
                        Config.CurrentUser = isAuthencated.UserID;
                        Config.User = isAuthencated;
                        Config.UsersProfile = userBll.GetUserProfile(isAuthencated.UserID);
                        if (isAuthencated.UserType == (int)Enumerations.UserType.Super || isAuthencated.UserType == (int)Enumerations.UserType.Admin)
                        {
                            return RedirectToAction("Index", "Admin", new { area = "Admin" });
                        }
                        if (isAuthencated.UserType == (int)Enumerations.UserType.Student)
                        {
                            return RedirectToAction("step_1", "Student", new { area = "Student" });
                        }
                        if (isAuthencated.UserType == (int)Enumerations.UserType.Teacher)
                        {
                            if (isAuthencated.UserStatus != (int)Enumerations.UserStatus.IsBlocked && isAuthencated.UserStatus != (int)Enumerations.UserStatus.NotApproved && isAuthencated.UserStatus != (int)Enumerations.UserStatus.Pending && isAuthencated.UserStatus != (int)Enumerations.UserStatus.IsDeleted)
                            {
                                return RedirectToAction("Index", "Teacher", new { area = "Teacher" });
                            }

                        }
                        if (isAuthencated.UserType == (int)Enumerations.UserType.Parent)
                        {
                            return RedirectToAction("Index", "Parent", new { area = "Parent" });
                        }
                    }
                    else
                    {
                        TempData["Message"] = "you are already registered with us as different user";
                        TempData["Status"] = 3;
                        return RedirectToAction("Login");
                    }
                }
                else
                {
                    // Here I need to register User means first time login using facebook
                    var registermodel = new StudentRegisterViewModel();
                    registermodel.FirstName = firstname;
                    registermodel.LastName = lastname;
                    registermodel.LoginEmail = username;
                    registermodel.UserType = (int)Session["UserType"];
                    registermodel.ExternalLoginId = tokenid;

                    var regresult = UserManager.Registeruser(registermodel);
                    if (regresult != 0)
                    {
                        var isAuthencated = UserManager.CanExternalLogin(registermodel.LoginEmail, registermodel.Password, registermodel.ExternalLoginId);
                        if (isAuthencated != null)
                        {
                            FormsAuthentication.SetAuthCookie(isAuthencated.LoginEmail, true);
                            Config.CurrentUser = isAuthencated.UserID;
                            Config.User = isAuthencated;
                            Config.UsersProfile = userBll.GetUserProfile(isAuthencated.UserID);
                            if (isAuthencated.UserType == (int)Enumerations.UserType.Super || isAuthencated.UserType == (int)Enumerations.UserType.Admin)
                            {
                                return RedirectToAction("Index", "Admin", new { area = "Admin" });
                            }
                            if (isAuthencated.UserType == (int)Enumerations.UserType.Student)
                            {
                                return RedirectToAction("step_1", "Student", new { area = "Student" });
                            }
                            if (isAuthencated.UserType == (int)Enumerations.UserType.Teacher)
                            {
                                if (isAuthencated.UserStatus != (int)Enumerations.UserStatus.IsBlocked && isAuthencated.UserStatus != (int)Enumerations.UserStatus.NotApproved && isAuthencated.UserStatus != (int)Enumerations.UserStatus.Pending && isAuthencated.UserStatus != (int)Enumerations.UserStatus.IsDeleted)
                                {
                                    return RedirectToAction("Index", "Teacher", new { area = "Teacher" });
                                }
                                return RedirectToAction("Index", "Teacher", new { area = "Teacher" });
                            }
                            if (isAuthencated.UserType == (int)Enumerations.UserType.Parent)
                            {
                                return RedirectToAction("Index", "Parent", new { area = "Parent" });
                            }
                        }
                    }
                    else
                    {
                        TempData["Message"] = "you are already registered with us as different user";
                        TempData["Status"] = 3;
                        return RedirectToAction("Login");
                    }

                }
                return View("login");
            }

        }
        #endregion


        #region [GOOGLE LOGIN]

        [HttpPost]
        public ActionResult GoogleLogin(string usertype, string email, string fname, string lname, string token)
        {
            WebHelper web = new WebHelper();
            bool yes = web.isSiteDisable();
            if (yes == true)
            {
                return RedirectToAction("SiteDisable", "Home");
            }
            else
            {
                Config.CurrentUser = 0;
                Config.User = null;
                Config.UsersProfile = null;

                var model = new ExternalLoginViewModel();
                model.EmailAddress = email;
                model.UserType = Convert.ToInt32(usertype);
                model.TokenId = token;
                var resultsid = UserManager.UserExternalLogin(model);    // here token check from database 
                if (resultsid != 0)
                {
                    // means I need to Direct login no reg need because user is already registered 
                    var userdetailss = UserManager.getUserDetails(resultsid);
                    if (userdetailss.UserType != model.UserType)
                    {
                        TempData["Message"] = "you are already registered with us as different user";
                        TempData["Status"] = 3;
                        return Json(new { url = Url.Action("Login", "Account", new { area = "" }) });
                        //  return RedirectToAction("Login");
                    }

                    var isAuthencated = UserManager.CanExternalLogin(userdetailss.LoginEmail, userdetailss.Password, userdetailss.ExternalLoginID);
                    if (isAuthencated != null)
                    {
                        FormsAuthentication.SetAuthCookie(isAuthencated.LoginEmail, true);
                        Config.CurrentUser = isAuthencated.UserID;
                        Config.User = isAuthencated;
                        Config.UsersProfile = userBll.GetUserProfile(isAuthencated.UserID);
                        if (isAuthencated.UserType == (int)Enumerations.UserType.Super || isAuthencated.UserType == (int)Enumerations.UserType.Admin)
                        {
                            return Json(new { url = Url.Action("Index", "Admin", new { area = "Admin" }) });
                            // return RedirectToAction("Index", "Admin", new { area = "Admin" });
                        }
                        if (isAuthencated.UserType == (int)Enumerations.UserType.Student)
                        {
                            return Json(new { url = Url.Action("step_1", "Student", new { area = "Student" }) });
                            // return RedirectToAction("step_1", "Student", new { area = "Student" });
                        }
                        if (isAuthencated.UserType == (int)Enumerations.UserType.Teacher)
                        {
                            if (isAuthencated.UserStatus != (int)Enumerations.UserStatus.IsBlocked && isAuthencated.UserStatus != (int)Enumerations.UserStatus.NotApproved && isAuthencated.UserStatus != (int)Enumerations.UserStatus.Pending && isAuthencated.UserStatus != (int)Enumerations.UserStatus.IsDeleted)
                            {
                                return Json(new { url = Url.Action("Index", "Teacher", new { area = "Teacher" }) });
                                // return RedirectToAction("Index", "Teacher", new { area = "Teacher" });
                            }

                        }
                        if (isAuthencated.UserType == (int)Enumerations.UserType.Parent)
                        {
                            return Json(new { url = Url.Action("Index", "Parent", new { area = "Parent" }) });

                            //   return RedirectToAction("Index", "Parent", new { area = "Parent" });
                        }
                    }
                    else
                    {
                        Config.CurrentUser = 0;
                        Config.User = null;
                        Config.UsersProfile = null;
                        TempData["Message"] = "you are already registered with us as different user";
                        TempData["Status"] = 3;
                        return Json(new { url = Url.Action("Login", "Account", new { area = "" }) });

                        //  return RedirectToAction("Login");
                    }
                }
                else
                {
                    // Here I need to register User means first time login using facebook
                    var registermodel = new StudentRegisterViewModel();
                    registermodel.FirstName = fname;
                    registermodel.LastName = lname;
                    registermodel.LoginEmail = email;
                    registermodel.UserType = Convert.ToInt32(usertype);
                    registermodel.ExternalLoginId = token;

                    var regresult = UserManager.Registeruser(registermodel);
                    if (regresult != 0)
                    {
                        var isAuthencated = UserManager.CanExternalLogin(registermodel.LoginEmail, registermodel.Password, registermodel.ExternalLoginId);
                        if (isAuthencated != null)
                        {
                            FormsAuthentication.SetAuthCookie(isAuthencated.LoginEmail, true);
                            Config.CurrentUser = isAuthencated.UserID;
                            Config.User = isAuthencated;
                            Config.UsersProfile = userBll.GetUserProfile(isAuthencated.UserID);
                            if (isAuthencated.UserType == (int)Enumerations.UserType.Super || isAuthencated.UserType == (int)Enumerations.UserType.Admin)
                            {
                                return Json(new { url = Url.Action("Index", "Admin", new { area = "Admin" }) });

                            }
                            if (isAuthencated.UserType == (int)Enumerations.UserType.Student)
                            {
                                return Json(new { url = Url.Action("step_1", "Student", new { area = "Student" }) });


                            }
                            if (isAuthencated.UserType == (int)Enumerations.UserType.Teacher)
                            {
                                if (isAuthencated.UserStatus != (int)Enumerations.UserStatus.IsBlocked && isAuthencated.UserStatus != (int)Enumerations.UserStatus.NotApproved && isAuthencated.UserStatus != (int)Enumerations.UserStatus.Pending && isAuthencated.UserStatus != (int)Enumerations.UserStatus.IsDeleted)
                                {

                                    return Json(new { url = Url.Action("Index", "Teacher", new { area = "Teacher" }) });

                                }
                                return Json(new { url = Url.Action("Index", "Teacher", new { area = "Teacher" }) });

                            }
                            if (isAuthencated.UserType == (int)Enumerations.UserType.Parent)
                            {
                                return Json(new { url = Url.Action("Index", "Parent", new { area = "Parent" }) });
                            }
                        }
                    }
                    else
                    {
                        TempData["Message"] = "you are already registered with us as different user"; ;
                        TempData["Status"] = 3;
                        return Json(new { url = Url.Action("Login", "Account", new { area = "" }) });
                        // return RedirectToAction("Login");
                    }

                }

                return View("Login");
            }
            
        }


		#endregion
	}
}