﻿using BLL.BusinessManagers;
using BLL.BusinessManagers.AdminManager;
using BLL.BusinessManagers.UserManager;
using BLL.BusinessModels;
using BLL.BusinessModels.TeacherModels;
using BLL.Helpers;
using DAL.Entities;
using DAL.MasterEntity;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;

using System.Configuration;


namespace Tutoring.Controllers
{
    public class PaymentController : Controller
    {
        public UserBLL userBll = new UserBLL();

        public string action1 = string.Empty;
        public string hash1 = string.Empty;
        public string txnid1 = string.Empty;

        // GET: Payment
        //public ActionResult Index()
        //{
        //    return View();
        //}

        public ActionResult CourseDetailsExpand(Int64 CourseID)
        {
            WebHelper web = new WebHelper();
            bool yes = web.isSiteDisable();
            if (yes == true)
            {
                return RedirectToAction("SiteDisable", "Home");
            }
            else
            {
                try
                {
                    if (CourseID > 0)
                    {

                        CourseOfferingsManager cManager = new CourseOfferingsManager();
                        CoursesHeader Course = new CoursesHeader();
                        Course = cManager.GetCourseDetail(CourseID);

                        return View(Course);

                    }
                    else
                    {
                        return RedirectToAction("ExploreCourses");
                    }

                }
                catch (Exception)
                {
                    throw;
                }
            }
            //return View();
        }


        public ActionResult PaymentDetails(Int64 CourseID, string Cancel = null)
        {
            if (Config.CurrentUser == 0)
                return RedirectToAction("Login", "Account", new { area = "" });

            if (Config.User.UserType != (int)Enumerations.UserType.Student)
                return RedirectToAction("UnAuthorizeAccess", "Home", new { area = "" });

            UserBLL userBLL = new UserBLL();
            UsersProfile user = new UsersProfile();
            user = userBLL.GetUserProfile(Config.CurrentUser);

            try
            {
                if (CourseID > 0)
                {

                    CourseOfferingsManager cManager = new CourseOfferingsManager();
                    CoursesHeader Course = new CoursesHeader();
                    Course = cManager.GetCourseDetail(CourseID);

                    return View(Course);

                }
                else
                {
                    return RedirectToAction("ExploreCourses");
                }

            }
            catch (Exception)
            {
                throw;
            }

           // return View();

        }






        #region
        public string Generatehash512(string text)
        {

            byte[] message = Encoding.UTF8.GetBytes(text);

            UnicodeEncoding UE = new UnicodeEncoding();
            byte[] hashValue;
            SHA512Managed hashString = new SHA512Managed();
            string hex = "";
            hashValue = hashString.ComputeHash(message);
            foreach (byte x in hashValue)
            {
                hex += String.Format("{0:x2}", x);
            }
            return hex;
        }


        #endregion

    }


}