﻿using BLL.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Tutoring.Controllers
{
    public class AdminsController : Controller
    {
        // GET: Admins
        public ActionResult Index()
        {
            WebHelper web = new WebHelper();
            bool yes = web.isSiteDisable();
            if (yes == true)
            {
                return RedirectToAction("SiteDisable", "Home");
            }
            else
            {
                return View();
            }

            
        }
    }
}