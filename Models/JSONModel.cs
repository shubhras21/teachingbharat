﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Tutoring.Models
{
    public class JSONModel
    {
        public bool Status { get; set; }
        public string Message { get; set; }
        public int Count { get; set; }
        public object Data { get; set; }
    }

    public class PostBackReturn
    {
        public string name { get; set; }
        public string _id { get; set; }
    }

    public class CurrentUserInformation
    {
        public Int64 UserID { get; set; }
        public string Name { get; set; }
        public string Role { get; set; }
        public bool status { get; set; }
    }
}