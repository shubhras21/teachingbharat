﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Entities
{
    [Table("tblUsersProfile")]
    public class UsersProfile
    {
        [Key]
         
        public Int64 UsersProfileID { get; set; }
        
        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string ContactNumber { get; set; }

        public string AlternateNumber { get; set; }

        public string ProfilePic { get; set; }

        public string HeardAboutRudra { get; set; }

        public byte[] Resume { get; set; }

        public string AboutMe { get; set; }

        public DateTime? DateOfBirth { get; set; }

        public DateTime? CreateDate { get; set; }

        public DateTime? UpdatedDate { get; set; }

        public int WeekHours { get; set; }

        public int WeekendHours { get; set; }

        public string CityName { get; set; }

        // Bool variables

        public int IsPen { get; set; }

        public bool IsWebcam { get; set; }

        public int IsInternetSpeed { get; set; }
        
        public double SessionCost { get; set; }
        public long FKGradeID { get; set; }



        #region [Relationships}

        public Int64 FKUserID { get; set; }

        public Int64? FKStateID { get; set; }

        public Int64? FKOccupationID { get; set; }

        public string ExperienceName { get; set; }

        [ForeignKey("FKUserID")]
        public virtual Users User { get; set; }

        //[ForeignKey("FKCountryID")]
        //public virtual Country Country { get; set; }

        [ForeignKey("FKStateID")]
        public virtual States States { get; set; }

        [ForeignKey("FKOccupationID")]
        public virtual Occupations Occupation { get; set; }

      //  [ForeignKey("FKExperienceID")]
     //   public virtual Experience Experiences { get; set; }

        #endregion

        // not mapped property

        [NotMapped]
        public string StateName { get; set; } 

    }
}
