﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Entities
{

    [Table("tblFreeResources")]
    public class FreeResources
    {

        [Key]
        public Int64 FreeResourcesID { get; set; }

        public string ResourceTitle { get; set; }

        public string VideoURL { get; set; }

        public string VideoDescription { get; set; }

        public Int64 FKCurriculumID { get; set; }

        public Int64 FKGradeID { get; set; }

        public Int64 FKSubjectID { get; set; }

        public Int64 FKTopicID { get; set; }

        public Int64 FKUploadedBy { get; set; }

        public DateTime? UploadDate { get; set; }

        public int NumberOfViews { get; set; }

        public string OptionalTopic { get; set; }

        public int Year { get; set; }

        public bool IsActive { get; set; }


        [NotMapped]
        public Subjects Subject { get; set; }

        [NotMapped]
        public List<Subjects> TeacherSubjects { get; set; }

        [NotMapped]
        public string OfferedBy { get; set; }

        [NotMapped]
        public string TeacherPic { get; set; }



    }
}
