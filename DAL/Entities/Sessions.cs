﻿
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Entities
{
    [Table("tblSessions")]
    public class Sessions
    {
        [Key]
        public Int64 TrialSessionID { get; set; }

        public string SessionName { get; set; }

        public string TopicName { get; set; }

        public DateTime? StartDate { get; set; }

        public DateTime? EndDate { get; set; }

        public DateTime RequestDate { get; set; }

        public DateTime TrialHappenDate { get; set; }

        public Int64 FKCurriculumID { get; set; }

        public Int64 FKGradeID { get; set; }

        public Int64 FKSubjectID { get; set; }

        public Int64? FKRequestedBy { get; set; }

        public Int64? FKRequestedTo { get; set; }

        public int SessionType { get; set;}

        public int RequestStatus { get; set; }
              
        public string SessionLink { get; set; }

        public double? SessionCost { get; set; }

        public int CurrentStatus { get; set; }

        #region [RelationShip]


        //[ForeignKey("FKRequestedBy")]
        //public virtual Users Student { get; set; }

        //[ForeignKey("FKRequestedTo")]
        //public virtual Users Teacher { get; set; }

        //[ForeignKey("FKCurriculumID")]
        //public virtual Curriculums Target { get; set; }

        //[ForeignKey("FKGradeID")]
        //public virtual Grades Grade { get; set; }

        //[ForeignKey("FKSubjectID")]
        //public virtual Subjects Subject { get; set; }

        #endregion


        // Not Mapped Properties

        [NotMapped]
        public string RequestedByName { get; set; }

        [NotMapped]
        public string RequestedToName { get; set; }

        [NotMapped]
        public string CurriculumName { get; set; }

        [NotMapped]
        public string GradeName { get; set; }

        [NotMapped]
        public string SubjectName { get; set; }

        [NotMapped]
        public string SessionTimes { get; set; }

        [NotMapped]
        public string RequestedTimes { get; set; }

        [NotMapped]
        public string RequestStatusName { get; set; }

        [NotMapped]
        public string SessionTypeName { get; set; }

        [NotMapped]
        public string FromToTime { get; set; }

        [NotMapped]
        public string SessionStatus { get; set; }

        [NotMapped]
        public Int64 TotalStudents { get; set; }

        [NotMapped]
        public string ImageName { get; set; }
    }
}
