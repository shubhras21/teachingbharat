﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Entities
{
    [Table("tblUserLanguages")]
   public class UserLanguages
    {
        [Key]
        public Int64 UserLanguageID { get; set; }

        public string LanguageName { get; set; }

        public Int64 FKUserID { get; set; }
    }
}
