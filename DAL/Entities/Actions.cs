﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Entities
{
   public class Actions
    {
        public int ActionsID { get; set; }
        public string ActionName { get; set; }
        public bool IsActive { get; set; }
    }
}
