﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Entities
{
    [Table("tblTimings")]
    public class Timings
    {
        [Key]
        public Int64 TimeID { get; set; }

        public string FromTime { get; set; }

        public string ToTime { get; set; }

        public DateTime TimeSlot { get; set; }

        public bool IsActive { get; set; }


    }
}
