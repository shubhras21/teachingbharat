﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Entities
{
    [Table("tblUserActivities")]
  
    public class UserActivities
    {

        [Key]
        public Int64 ActivityID { get; set; }

        public string ActionPerformed { get; set; }

        public Int64 FKUserID { get; set; }

        public DateTime Time { get; set; }

        public bool IsActive { get; set; }

        public int ActivitySource { get; set; }

    }
}
