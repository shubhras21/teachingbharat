﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Entities
{

    [Table("tblCoursesHeader")]
    public class CoursesHeader
    {

        [Key]
        public Int64 CoursesHeaderID { get; set; }

        public string CourseTitle { get; set; }

        public string CourseDescription { get; set; }

        public double CourseFee { get; set; }

        public int TotalHours { get; set; }

        public int TotalSeats { get; set; }

        public string TrialVideoURL { get; set; }

        public DateTime? OfferedDate { get; set; }

        public string TimeSlot { get; set; }

        public DateTime? StartDate { get; set; }

        public DateTime? EndDate { get; set; }

        public Int64 FKCurriculumID { get; set; }

        public Int64 FKGradeID { get; set; }

        public Int64 FKSubjectID { get; set; }

        public Int64 FKOfferedBY { get; set; }

        public bool IsActive { get; set; }



        // Not Mapped Properties for Course Offerings 

        [NotMapped]
        public List<CourseSchedule> CourseSchedule { get; set; }

        [NotMapped]
        public CoursesLink CoursesLink { get; set; }

        [NotMapped]
        public List<CourseFaqs> CourseFaqs { get; set; }

        [NotMapped]
        public List<CourseParticipants> CourseParticipants { get; set; }

        [NotMapped]
        public Subjects Subject { get; set; }

        [NotMapped]
        public List<AboutCourse> AboutCourse { get; set; }

        [NotMapped]
        public List<CourseCurriculum> CourseCurriculum { get; set; }

        [NotMapped]
        public List<CourseDays> CourseDays { get; set; }

        [NotMapped]
        public string GradeName { get; set; }

        [NotMapped]
        public string TeacherName { get; set; }

        [NotMapped]
        public string TeacherPic { get; set; }

        [NotMapped]
        public string UniversityName { get; set; }

        [NotMapped]
        public double TeacherRatings { get; set; }

        [NotMapped]
        public bool IsOnline { get; set; }

        [NotMapped]
        public DateTime DateTimeSlot{ get; set; }

        [NotMapped]
        public double CompletionPercentage { get; set; }

    }

}
