﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Entities
{
  public  class BannerSlides
    {
        public int BannerSlidesID { get; set; }
        public string Title { get; set; }
        public string Link { get; set; }
        public bool Status { get; set; }
    }
}
