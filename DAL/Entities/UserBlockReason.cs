﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Entities
{
   public class UserBlockReason
    {
        public int UserBlockReasonID { get; set; }
        public string Reason { get; set; }
        public long FKUserID { get; set; }
    }
}
