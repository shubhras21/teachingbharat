﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Entities
{
    // Test k andar kon kon se questions create hue hen jo k question bank se random aye hen
    [Table("tblTestQuestions")]
    public class TestQuestions
    {

        [Key]
        public Int64 TestQuestionsID { get; set; }

        public Int64 FKTestID { get; set; }

        public Int64 FKQuestionID { get; set; }

        public bool IsActive { get; set; }


        // Not Mapped Property

        [NotMapped]
        public QuestionsBank listOfQuestions { get; set; }




    }
}
