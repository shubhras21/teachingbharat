﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Entities
{
    [Table("tblMetaSettings")]
    public class MetaSetting
    {
        [Key]
        public Int64 MetaID { get; set; }

        public string Title { get; set; }
        public string Description { get; set; }
        public string KeyWords { get; set; }

        public bool IsEnabled { get; set; }
    }
}
