﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Entities
{
   public  class UserPermission
    {
        public long UserPermissionID { get; set; }
        public long FKUserID { get; set; }
        public long FKPermissionID { get; set; }
        public int UserType { get; set; }
    }
}
