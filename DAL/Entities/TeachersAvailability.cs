﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Entities
{
    [Table("tblTeachersAvailability")]
    public class TeachersAvailability
    {
        [Key,DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Int64 TAID { get; set; }

        public DateTime AvailableTime { get; set; }

        public Int64 FKTeacherID { get; set; }

        public bool IsActive { get; set; }

        #region [Relationships]

        [ForeignKey("FKTeacherID")]
        public virtual Users User { get; set; }

        #endregion



        // Not Mapped Properties

        [NotMapped]
        public string TimeCulture { get; set; }

        [NotMapped]
        public  string Hour { get; set; }

        [NotMapped]
        public string Date { get; set; }

        
    }
}
