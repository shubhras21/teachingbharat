﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Entities
{
    [Table("tblCourseParticipants")]
    public class CourseParticipants
    {

        [Key]
        public Int64 CourseParticipantID { get; set; }

        public Int64 FKStudentID { get; set; }

        public Int64 FKCourseID  { get; set; }

        public DateTime JoinDate { get; set; }

        public bool IsActive { get; set; }



        [NotMapped]
        public CoursesHeader Course { get; set; }


        [NotMapped]
        public double CompletionPercentage { get; set; }




    }
}
