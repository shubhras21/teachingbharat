﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Entities
{
   public  class Permission
    {
        public int PermissionID { get; set;}
        public string PermissionName { get; set; }
        public bool IsActive { get; set; }
      
    }
}
