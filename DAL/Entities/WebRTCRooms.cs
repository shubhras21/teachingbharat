﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Entities
{
    [Table("tblWebRTCRooms")]
    public class WebRTCRooms
    {

        [Key]
        public Int64 WRTCID { get; set; }

        public Int64 FKSessionID { get; set; }

        public string RoomID { get; set; }

        public bool IsActive { get; set; }

        public int IsCourse { get; set; }


        [NotMapped]
        public string TeacherName { get; set; }

        [NotMapped]
        public string StudentName { get; set; }



    }
}
