﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Entities
{
    [Table("tblCity")]
    public class City
    {
        [Key]
        public Int64 CityID { get; set; }

        public string CityName { get; set; }

        public bool IsActive { get; set; }

        public Int64 FKCountryID { get; set; }


        #region [Relationships]

        [ForeignKey("FKCountryID")]
        public virtual Country Country { get; set; }

        #endregion

    }
}
