﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Entities
{

    [Table("tblLanguages")]
    public class Languages
    {

        [Key]
        public Int64 LanguageID { get; set; }

        public string LanguageName { get; set; }

        public bool IsActive { get; set; }
    }
}
