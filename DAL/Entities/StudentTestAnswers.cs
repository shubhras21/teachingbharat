﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Entities
{
    [Table("tblStudentTestAnswers")]
    public class StudentTestAnswers
    {
        [Key]
        public Int64 TestAnswerID { get; set; }

        public Int64 FKStudentID { get; set; }

        public Int64 FKAssignedTestID { get; set; }

        public Int64 FKTestQuestionsID { get; set; }

        public int FKStudentAnswer { get; set; }

        public DateTime QuestionStartTime { get; set; }

        public DateTime QuestionSubmitTime { get; set; }

        public bool IsSkipped { get; set; }

        public bool IsActive { get; set; }

        // Aman Added
        public bool ISStuedentAnswerTrue { get; set; }

        public Int64 MarksGain { get; set; }

        public Int64 ResultId { get; set; }

       // public bool IsResultCalculated { get; set; }        

        [NotMapped]
        public int StudentAnswerStatus { get; set; }

        [NotMapped]
        public string RightAnswer { get; set; }

        [NotMapped]
        public int StudentGiverAnswersCount { get; set; }

        [NotMapped]
        public int TestResult { get; set; }

        [NotMapped]
        public bool TrueAnswer { get; set; }

        [NotMapped]
        public QuestionsBank QuestionBank { get; set; }



    }
}
