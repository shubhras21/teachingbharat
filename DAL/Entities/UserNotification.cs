﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Entities
{
    [Table("tblUserNotification")]
    public class UserNotification
    {
        [Key]
        public Int64 NotificationID { get; set; }
        public string Comments { get; set; }
        public DateTime NotificationDateTime { get; set; }
        public Int64 UserID { get; set; }
        public bool IsSeen { get; set; }
        public string NotificationBy { get; set; }
    }
}
