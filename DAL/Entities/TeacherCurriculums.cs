﻿
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Entities
{
    [Table("tblTeacherCurriculums")]
    public class TeacherCurriculums
    {
        [Key, Column(Order = 0)]
        public Int64 FKUserID { get; set; }

        [Key, Column(Order = 1)]
        public Int64 FKCurriculumID { get; set; }

        public bool IsActive { get; set; }

        #region [RelationShip]

        [ForeignKey("FKUserID")]
        public virtual Users User { get; set; }

        [ForeignKey("FKCurriculumID")]
        public virtual Curriculums Curriculum { get; set; }

        #endregion
    }
}
