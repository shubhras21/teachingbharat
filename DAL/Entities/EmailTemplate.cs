﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Entities
{
    public class EmailTemplate
    {
        public int EmailTemplateID { get; set; }
       
        public string Body { get; set; }


        public int Type { get; set; }
    }
}
