﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Entities
{
    [Table("tblCourseFaqs")]
    public class CourseFaqs
    {

        [Key]
        public Int64 CourseFaqID { get; set; }

        public string Question { get; set; }

        public string Answer { get; set; }

        public Int64? FKAskedBy { get; set; }

        public Int64 FKCourseID { get; set; }

        public DateTime? CreatedDate { get; set; }

        public bool IsActive { get; set; }

    }
}
