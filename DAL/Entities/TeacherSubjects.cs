﻿
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Entities
{

    [Table("tblTeacherSubjects")]
    public class TeacherSubjects
    {
        [Key]
        public Int64 TeacherSubjectsID { get; set; }

        public Int64 FKUserID { get; set; }

        public Int64 FKCurriculumID { get; set; }

        public Int64 FKSubjectID { get; set; }

        public Int64 FKGradeID { get; set; }

        public int Type { get; set; }

        public bool IsActive { get; set; }

        #region [RelationShip]

        [ForeignKey("FKUserID")]
        public virtual Users User { get; set; }

        [ForeignKey("FKSubjectID")]
        public virtual Subjects Subject { get; set; }

        [ForeignKey("FKCurriculumID")]
        public virtual Curriculums Curriculum { get; set; }

        [ForeignKey("FKGradeID")]
        public virtual Grades Grades { get; set; }

        #endregion



        // Not mapped Properties

        [NotMapped]
        public string CurriculumName { get; set; }

        [NotMapped]
        public string GradeName { get; set; }

        [NotMapped]
        public string SubjectName { get; set; }

        [NotMapped]
        public string SubjectType { get; set; }

        [NotMapped]
        public List<Users> TeacherViewModel { get; set; }

        [NotMapped]
        public string ProfilePic { get; set; }

        [NotMapped]
        public double Ratings { get; set; }
    }
}
