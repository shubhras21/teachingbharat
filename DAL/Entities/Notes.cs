﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Entities
{
    [Table("tblNotes")]
    public class Notes
    {
        [Key]
        public Int64 NoteID { get; set; }

        public string NotesName { get; set; }

        public Int64 FKSubjectID { get; set; }

        public Int64 FKTopicID { get; set; }

        public string TopicOptional { get; set; }

        public Int64 FKFolderID { get; set; }

        public Int64 FKCreatedBy { get; set; }

        public string TopicDescription { get; set; }

        public string FileName { get; set; }

        public int TotalDownloads { get; set; }

        public DateTime CreatedDate { get; set; }

        public bool IsActive { get; set; }


        [NotMapped]
        public string Subject { get; set; }

        [NotMapped]
        public string TopicName { get; set; }

        [NotMapped]
        public string TeacherName { get; set; }

    }
}
