﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Entities
{
    [Table("tblUserActivity")]
    public class UserActivity
    {
        [Key]
        public Int64 ActivityID { get; set; }
        public string Comments { get; set; }
        public DateTime ActivityDateTime { get; set; }
        public Int64 UserID { get; set; }
        public bool IsSeen { get; set; }
        public string ActivityBy { get; set; }
    }
}
