﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Entities
{
    [Table("tblSubjectFAQS")]
    public class SubjectFAQS
    {

        [Key]
        public Int64 SubjectFAQSID { get; set; }

        public Int64 FKSubjectID { get; set; }

        public string Question { get; set; }

        public string Answer { get; set; }

        public Int64 QuestionBy { get; set; }

        public bool IsActive { get; set; }

        [NotMapped]
        public string SubjectName { get; set; }

    }
}
