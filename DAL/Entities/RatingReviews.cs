﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Entities
{
    [Table("tblRatingReviews")]
    public class RatingReviews
    {

        [Key]
        public Int64 RatingReviewsID { get; set; }
        public Int64 FKTeacherID { get; set; }
        public Int64 FKStudentID { get; set; }
        public string Comment { get; set; }
        public DateTime CreatedDate { get; set; }

        public double Rating { get; set; }
        public bool IsActive { get; set; }

        public bool IsApproved { get; set; }
        public string CreatedBy { get; set; }
        public string StudentName { get; set; }
        public string TeacherName { get; set; }

        [NotMapped]
        public string StudentGrade { get; set; }

        [NotMapped]
        public string StudentLocation { get; set; }


    }
}
