﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Entities
{
    [Table("tblCoursesLink")]
    public class CoursesLink
    {

        [Key]
        public Int64 CoursesLinkID { get; set; }

        public string CourseLink { get; set; }

        public Int64 FKCourseID { get; set; }

        public bool IsActive { get; set; }

        
    }
}
