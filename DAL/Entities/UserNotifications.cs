﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Entities
{

    [Table("tblUserNotifications")]
    public class UserNotifications
    {

        [Key]
        public Int64 NotificationID { get; set; }

        public string ActionPerformed { get; set; }

        public Int64 FKUserID { get; set; }

        public DateTime Time { get; set; }

        public Int64 FKSessionID { get; set; }

        public int IsCourse { get; set; }
        public bool IsSeen { get; set; }

        public bool IsActive { get; set; }

    }
}

