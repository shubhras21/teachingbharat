﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Entities
{

    [Table("tblCourseSessionsDuration")]
    public class CourseSessionsDuration
    {

        [Key]
        public Int64 CourseSessionsDurationID { get; set; }

        public DateTime StartTime { get; set; }

        public DateTime EndTime { get; set; }

        public Int64 FKStudentCourseScheduleID { get; set; }

        public Int64 FKCourseID { get; set; }

        public Int64 FKJoinedBy { get; set; }

        public bool IsActive { get; set; }

    }
}
