﻿
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Entities
{

    [Table("tblCourseCurriculum")]
    public class CourseCurriculum
    {

        [Key]
        public Int64 CourseCurriculumID { get; set; }

        public Int64 FKCourseID { get; set; }

        public string CoveredTopic { get; set; }

        public bool IsActive { get; set; }


    }
}
