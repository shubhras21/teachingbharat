﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Entities
{
    [Table("tblFolders")]
    public class Folders
    {
        [Key]
        public Int64 FolderID { get; set; }

        public string FolderName { get; set; }

        public DateTime CreatedDate { get; set; }

        public Int64 FKCreatedBy { get; set; }

        public bool IsActive { get; set; }


        [NotMapped]
        public int TotalFiles { get; set; }


    }
}
