﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Entities
{
    [Table("tblStates")]
    public class States
    {

        [Key]
        public Int64 StateID { get; set; }

        public string StateName { get; set; }

        public bool IsActive { get; set; }

        public Int64 FKCountryID { get; set; }


        #region [Relationships]

        [ForeignKey("FKCountryID")]
        public virtual Country Country { get; set; }

        #endregion

    }
}
