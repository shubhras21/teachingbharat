﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Entities
{
    [Table("tblTrialSessions")]
    public class TrialSessions
    {
        [Key]
        public Int64 TrialSessionID { get; set; }

        public string SessionName { get; set; }

        public DateTime RequestedDate { get; set; }

        public DateTime TrialRequestedDate { get; set; }

        public int Status { get; set; }

        public Int64 FKRequestedBy { get; set; }

        public Int64 FKRequestedTo { get; set; }

        public Int64 FKCurriculumID { get; set; }

        public Int64 FKGradeID { get; set; }

        public Int64 FKSubjectID { get; set; }

        public Int64 FKTimingID { get; set; }

        #region [RelationShip]

        [ForeignKey("FKRequestedBy")]
        public virtual Users Student { get; set; }

        [ForeignKey("FKRequestedTo")]
        public virtual Users Teacher { get; set; }

        [ForeignKey("FKCurriculumID")]
        public virtual Curriculums Target { get; set; }

        [ForeignKey("FKGradeID")]
        public virtual Grades Grade { get; set; }

        [ForeignKey("FKSubjectID")]
        public virtual Subjects Subject { get; set; }

        [ForeignKey("FKTimingID")]
        public virtual Timings Time { get; set; }

        #endregion

    }
}
