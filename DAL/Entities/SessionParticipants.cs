﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Entities
{
    [Table("tblSessionParticipants")]
    public class SessionParticipants
    {

        [Key]
        public Int64 ParticipantID { get; set; }

        public DateTime JoinDate { get; set; }

        public DateTime LeaveDate { get; set; }

        public bool IsActive { get; set; }

        public Int64 FKStudentID { get; set; }

        public Int64 FKSessionID { get; set; }

        #region [RelationShip]

        //[ForeignKey("FKStudentID")]
        //public virtual Users User { get; set; }

        //[ForeignKey("FKSessionID")]
        //public virtual Sessions Session { get; set; }

        #endregion
    }
}
