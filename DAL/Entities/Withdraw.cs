﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Entities
{
    [Table("tblWithDraw")]
    public class Withdraw
    {
        [Key]
        public Int64 TransactionID { get; set; }
        public Int64 TeacherID { get; set; }
        public double Amount { get; set; }
        public double Balanace { get; set; }
        public DateTime withdrawDate { get; set; }
    }
}
