﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Entities
{
    [Table("tblCourseSchedule")]
    public class CourseSchedule
    {

        [Key]
        public Int64 CourseScheduleID { get; set; }

        public DateTime CourseSessionDate { get; set; }

        public string SessionContent { get; set; }

        public Int64 FKCourseID { get; set; }

        public int CurrentStatus { get; set; }

        public bool IsActive { get; set; }



    }
}
