﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Entities
{
    // Kon se students ko kon sa test assign he

    [Table("tblStudentTest")]
    public class StudentTest
    {
        [Key]
        public Int64 StudentTestID { get; set; }

        public Int64 FKStudentID { get; set; }

        public Int64 FKTestID { get; set; }

        public bool IsActive { get; set; }
    }
}
