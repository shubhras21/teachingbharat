﻿
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Entities
{
    [Table("tblSessionVideos")]
   public class SessionVideos
    {
        [Key]
        public Int64 VideoID { get; set; }

        public DateTime RecordDate { get; set; }

        public string VideoName { get; set; }

        public string VideoURL { get; set; }

        public bool IsActive { get; set; }

        public Int64 FKSenderID { get; set; }

        public Int64 FKSessionID { get; set; }


        #region [RelationShip]

        [ForeignKey("FKSenderID")]
        public virtual Users User { get; set; }

        [ForeignKey("FKSessionID")]
        public virtual Sessions Session { get; set; }

        #endregion
    }
}
