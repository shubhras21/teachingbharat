﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Entities
{
    // Jo test create hua he us k kon kon se topics cover hue hen
    [Table("tblTestCoveredTopics")]
   public class TestCoveredTopics
    {

        [Key]
        public Int64 TestCoveredTopicsID { get; set; }

        public Int64 FKTestID { get; set; }

        public Int64 FKTopicID { get; set; }

        public bool IsActive { get; set; }


        // Not Mapped Property

        [NotMapped]
        public string TopicName { get; set; }

    }
}
