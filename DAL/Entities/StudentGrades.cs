﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Entities
{
    [Table("tblStudentGrades")]
   public class StudentGrades
    {
      
            [Key]
            public Int64 StudentGradeID { get; set; }

            public string GradeName { get; set; }

            public Int64 FKUserID { get; set; }
       
    }
}
