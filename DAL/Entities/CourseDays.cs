﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Entities
{
    [Table("tblCourseDays")]
    public class CourseDays
    {

        [Key]
        public Int64 CourseDaysID { get; set; }

        public Int64 FKCourseID { get; set; }

        public int DayID { get; set; }

        public bool IsActive { get; set; }


    }
}
