﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Entities
{
    [Table("tblQualifications")]
    public class Qualifications
    {
        [Key]
        public Int64 QualificationID { get; set; }

        public string QualificationName { get; set; }

        public bool IsActive { get; set; }
    }
}
