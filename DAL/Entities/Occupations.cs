﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Entities
{
    [Table("tblOccupations")]
    public class Occupations
    {
        [Key]
        public Int64 OccupationID { get; set; }

        public string OccupationName { get; set; }

        public bool IsActive { get; set; }
    }
}
