﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Entities
{
    // Question Bank se Test k liye questions pick hon ge
    [Table("tblTest")]
    public  class Test
    {
        [Key]
        public Int64 TestID { get; set; }

        public Int64 FKGradeID { get; set; }

        public Int64 FKSubjectID { get; set; }

        public Int64 FKCreatedBy { get; set; }

        public DateTime FKCreatedDate { get; set; }

        public DateTime FKModifiedDate { get; set; }

        public int EasyCount { get; set; }

        public int MediumCount { get; set; }

        public int HardCount { get; set; }

        public int TestLevel { get; set; }

        public int TotalScores { get; set; }

        public int TotalQuestions { get; set; }

        public int TestDuration { get; set; }

        public string TestName { get; set; }

        public string RemarksTeacher { get; set; }

        public string RemarksStudent { get; set; }

        public int ISSkipable { get; set; }

        public bool IsActive { get; set; }
        public int PassingMarks { get; set; }


        //Not Mapped Property

        //[NotMapped]
        //public List<Int64> FKTopicID { get; set; }

        [NotMapped]
        public Int64[] FKTopicID { get; set; }

        [NotMapped]
        public string TestLevelName { get; set; }


        [NotMapped]
        public string QuestionBankName { get; set; }

        [NotMapped]
        public string GradeSubjectName { get; set; }

        [NotMapped]
        public int TotalAssignedCount { get; set; }

        [NotMapped]
        public string LastAssignedDate { get; set; }

        [NotMapped]
        public Int64 TestAssignedID { get; set;}

        [NotMapped]
        public string AssignedByName { get; set; }

    }
}
