﻿
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Entities
{
    [Table("tblSessionsChat")]
    public class SessionChat
    {
        [Key]
        public Int64 ChatID { get; set; }

        public DateTime ChatDate { get; set; }

        public string ChatMessage { get; set; }

        public bool IsActive { get; set; }

        public Int64 FKSenderID { get; set; }

        public Int64 FKSessionID { get; set; }

        #region [RelationShip]

        [ForeignKey("FKSenderID")]
        public virtual Users User { get; set; }

        [ForeignKey("FKSessionID")]
        public virtual Sessions Session { get; set; }

        #endregion
    }
}
