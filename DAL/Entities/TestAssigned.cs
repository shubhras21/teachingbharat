﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Entities
{

    [Table("tblTestAssigned")]
    public class TestAssigned
    {
        [Key]
        public Int64 TestAssignedID { get; set; }

        public DateTime TestHappenedTime { get; set; }

        public DateTime TestEndTime { get; set; }

        public Int64 FKTestID { get; set; }

        public Int64 FKAssignedBy { get; set; }

        public Int64 FKAssignedTo { get; set; }

        public DateTime AssignedDate { get; set; }

        public int TestStatus { get; set; }

        public bool IsActive { get; set; }    


        
        [NotMapped]
        public string AssignedByName { get; set; }

        [NotMapped]
        public string AssignedToName { get; set; }

        [NotMapped]
        public Test Test { get; set; }

        [NotMapped]
        public string TesthappenedString { get; set; }

        [NotMapped]
        public string RemainingTime { get; set; }

        [NotMapped]
        public string TestStatusName { get; set; }

        [NotMapped]
        public List<StudentTestAnswers> ListofStudentAnswers { get; set; }

        [NotMapped]
        public string TestStatusPassFail { get; set; }

        [NotMapped]
        public string GradeSubjectName { get; set; }

        [NotMapped]
        public double TotalSpendTime { get; set; }

        [NotMapped]
        public DateTime TestStartTime { get; set; }

        [NotMapped]
        public int GainedScores { get; set; }
    }
}
