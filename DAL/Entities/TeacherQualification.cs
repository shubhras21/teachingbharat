﻿
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Entities
{
    [Table("tblTeacherQualifications")]
    public class TeacherQualification
    {
        [Key]
        public Int64 TeacherQualificationID { get; set; }

        public DateTime? StartDate { get; set; }

        public DateTime? EndDate { get; set; }

        public bool IsActive { get; set; }

        public Int64 FKUserID { get; set; }

        public string QualificationName { get; set; }

        public string UniversityName { get; set; }


        #region [RelationShip]

        [ForeignKey("FKUserID")]
        public virtual Users User { get; set; }

        #endregion


    }
}
