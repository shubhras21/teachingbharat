﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Entities
{
    [Table("tblUsers")]
    public class Users
    {

        [Key]
        public Int64 UserID { get; set; }

        public string LoginEmail { get; set; }

        public string Password { get; set; }

        public string ExternalLoginID { get; set; }

        // Enum supported variables

        public int UserType { get; set; }

        public int UserStatus { get; set; }

        public int IsVerified { get; set; }

        public bool IsActive { get; set; }

        public bool IsOnline { get; set; }

        public DateTime? LastLogin { get; set; }

        [NotMapped]
        public UsersProfile UsersProfile { get; set; }

        [NotMapped]
        public List<UsersProfile> UsersProfiles { get; set; }

        [NotMapped]
        public string FirstName { get; set; }

        [NotMapped]
        public string LastName { get; set; }

        [NotMapped]
        public string Experience { get; set; }

        [NotMapped]
        public string Qualification { get; set; }


    }
}
