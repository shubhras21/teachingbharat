﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Entities
{
    [Table("tblStudentTestResult")]
    public class tblStudentTestResult
    {
        [Key]
        public Int64 ResultId { get; set; }

        public Int64 StudentID { get; set; }

        public Int64 TestID { get; set; }

        public Int64 TotalMarks { get; set; }

        public Int64 Result { get; set; }

        //public Int64 Duration { get; set; }

        public Int64 DurationHours { get; set; }

        public Int64 DurationMints { get; set; }

        public Int64 DurationSeconds { get; set; }

        public DateTime TestTakenOn { get; set; }

        public DateTime TestAssignedOn { get; set; }
    }
}
