﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
namespace DAL.Entities
{

    [Table("tblStudentCourseSchedule")]
    public class StudentCourseSchedule
    {

        [Key]
        public Int64 StudentCourseScheduleID { get; set; }

        public Int64 FKStudentID { get; set; }
        
        public Int64 FKCourseScheduleID { get; set; }

        public Int64 FKCourseParticipantID { get; set; }

        public bool IsActive { get; set; }

    }

}
