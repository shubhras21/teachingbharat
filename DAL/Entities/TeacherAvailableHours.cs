﻿
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Entities
{
    [Table("tblTeacherAvailableHours")]
    public class TeacherAvailableHours
    {
        [Key, Column(Order = 0)]
        public Int64 FKUserID { get; set; }

        [Key, Column(Order = 1)]
        public Int64 FKTimeID { get; set; }

        [Key, Column(Order = 2)]
        public Int64 FKDayID { get; set; }

        public bool IsActive { get; set; }

        #region [RelationShip]

        [ForeignKey("FKUserID")]
        public virtual Users User { get; set; }

        [ForeignKey("FKTimeID")]
        public virtual Timings Timing { get; set; }

        [ForeignKey("FKDayID")]
        public virtual Days Day { get; set; }

        #endregion
    }
}
