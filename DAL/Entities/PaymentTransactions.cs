﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Entities
{
    [Table("tblPaymentTransactions")]
    public class PaymentTransactions
    {

        [Key]
        public Int64 Id { get; set; }

        public Int64 CourseParticipantID { get; set; }

        public string  TransactionId { get; set; }

        public int status { get; set; }
    }
}
