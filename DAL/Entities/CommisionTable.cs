﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Entities
{
    [Table("tblCommision")]
    public class CommisionTable
    {
        [Key]
        public int CommisionID { get; set; }
        public int TeacherId { get; set; }
        public decimal CommisionPercentage { get; set; }
        public int Status { get; set; }
    }
}
