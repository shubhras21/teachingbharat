﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Entities
{
    [Table("tblGrades")]
    public class Grades
    {
        [Key]
        public Int64 GradeID { get; set; }

        public string GradeName { get; set; }

        public bool IsActive { get; set; }

        public Int64 FKCurriculumID { get; set; }

        #region [Relationships]

        [ForeignKey("FKCurriculumID")]
        public virtual Curriculums Curriculum { get; set; }

        #endregion

        [NotMapped]
        public string CurriculumName { get; set; }

    }
}
