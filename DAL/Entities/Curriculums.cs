﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Entities
{
    [Table("tblCurriculums")]
    public class Curriculums
    {
        [Key]
        public Int64 CurriculumID { get; set; }

        public string CurriculumName { get; set; }

        public bool IsActive { get; set; }
    }
}
