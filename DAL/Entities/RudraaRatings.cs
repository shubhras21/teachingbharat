﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Entities
{
    [Table("tblRudraaRatings")]
    public class RudraaRatings
    {
        [Key]
        public Int64 RudraaRatingID { get; set; }
      
        public Int64 FKUserID { get; set; }

        public string Email { get; set; }

        public string Name { get; set; }

        public string Comment { get; set; }

        public DateTime CreatedDate { get; set; }

        public double Rating { get; set; }

        public bool IsLogin { get; set; }

        public bool IsActive { get; set; }


        [NotMapped]
        public string StudentName { get; set; }

        [NotMapped]
        public string StudentGrade { get; set; }

        [NotMapped]
        public string StudentLocation { get; set; }
    }
}
