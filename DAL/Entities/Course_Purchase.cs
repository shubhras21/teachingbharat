﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Entities
{
    [Table("tblCourse_Purchase")]
    public class Course_Purchase
    {
        [Key]
        public Int64 TransactionID { get; set; }

        public Int64 CourseID { get; set; }
        public DateTime purchaseDate { get; set; }
        public Int64 TeacherID { get; set; }
        public Int64 StudentID { get; set; }
        public double Amount { get; set; }
    }
}
