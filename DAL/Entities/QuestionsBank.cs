﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Entities
{
    // Admin Question bank Add kre ga 
    [Table("tblQuestionsBank")]
    public class QuestionsBank
    {
        [Key]
        public Int64 QuestionBankID { get; set; }

       
        public string Question { get; set; }

        public string FirstOption { get; set; }

        public string SecondOption { get; set; }

        public string ThirdOption { get; set; }

        public string FourthOption { get; set; }

        public int AnswerKey { get; set; }

        public int QuestionLevel { get; set; }

        public Int64 FKCreatedBy { get; set; }

        public Int64 FKTopicID { get; set; }

        public int QuestionMarks { get; set; }

        public bool IsActive { get; set; }

        [NotMapped]
        public string TopicName { get; set; }

        [NotMapped]
        public string QuestionLevelName { get; set; }

        [NotMapped]
        public string Answer { get; set; }

        public int SortOrder { get; set; }

    }
}
