﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Entities
{
    [Table("tblTestParticipants")]
    public class TestParticipants
    {
            [Key]
            public Int64 ParticipantID { get; set; }

            public DateTime JoinTime { get; set; }

            public DateTime LeaveTime { get; set; }

            public Int64 FKStudentID { get; set; }

            public Int64 FKAssignedTestID { get; set; }

            public bool IsActive { get; set; }
    }
}
