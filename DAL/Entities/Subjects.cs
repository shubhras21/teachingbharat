﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Entities
{
    [Table("tblSubjects")]
    public class Subjects
    {
        [Key]
        public Int64 SubjectID { get; set; }

        public string SubjectName { get; set; }

        public string SubjectDescription { get; set; }

        public Int64 FKCurriculumID { get; set; }

        public Int64 FKGradeID { get; set; }


        public bool IsActive { get; set; }

        #region [Relationships]

        [ForeignKey("FKGradeID")]
        public virtual Grades Grades { get; set; }

        [ForeignKey("FKCurriculumID")]
        public virtual Curriculums Curriculum { get; set; }

        #endregion

        [NotMapped]
        public string CurriculumName { get; set; }

        [NotMapped]
        public string GradeName { get; set; }


        [NotMapped]
        public string CompleteSubjectName { get; set; }


    }
}
