﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Entities
{
   
    [Table("tblAboutCourse")]
    public class AboutCourse
    {

        [Key]
        public Int64 AboutCourseID { get; set; }

        public Int64 FKCourseID { get; set; }

        public string BulletPoint { get; set; }

        public bool IsActive { get; set; }


    }
}
