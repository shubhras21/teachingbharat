﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Entities
{
    [Table("tblExperience")]
    public class Experience
    {

        [Key]
        public Int64 ExperienceID { get; set; }

        public string ExperienceName { get; set; }

        public bool IsActive { get; set; }
    }
}
