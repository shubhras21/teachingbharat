﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Entities
{
    [Table("tblDays")]
    public class Days
    {
        [Key]
        public Int64 DayID { get; set; }

        public string DayName { get; set; }

        public string FullDayName { get; set; }

        public int DayOfWeek { get; set; }
    }
}
