﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Entities
{
    [Table("tblTopics")]
    public class Topics
    {
        [Key]
        public Int64 TopicID { get; set; }

        public string TopicName { get; set; }

        public string TopicDescription { get; set; }

        public Int64 FKCurriculumID { get; set; }

        public Int64 FKGradeID { get; set; }

        public bool IsActive { get; set; }
        
        #region [Relationships}

        public Int64 FKSubjectID { get; set; }

        [ForeignKey("FKSubjectID")]
        public virtual Subjects Subject { get; set; }

        #endregion

        [NotMapped]
        public string CurriculumName { get; set; }

        [NotMapped]
        public string GradeName { get; set; }

        [NotMapped]
        public string SubjectName { get; set; }

    }
}
