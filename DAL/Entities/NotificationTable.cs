﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Entities
{
    [Table("tblNotification")]
    public class NotificationTable
    {
        [Key]
        public int NotificationID { get; set; }
        public string Comments { get; set; }
        public DateTime NotificationDateTime { get; set; }
        public string UserID { get; set; }
        public string ReviewID { get; set; }
        public bool IsSeen { get; set; }
        [NotMapped]
        public string NotificationBy { get; set; }
    }
}
