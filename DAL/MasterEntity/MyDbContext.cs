﻿using DAL.Entities;
using DAL.Migrations;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.MasterEntity
{
    public class MyDbContext : DbContext
    {
        public MyDbContext() : base("name=Tutoring")
        {
            this.Configuration.LazyLoadingEnabled = false;
            Database.SetInitializer<MyDbContext>(null);
            Database.SetInitializer(new MigrateDatabaseToLatestVersion<MyDbContext, Configuration>());
            //Database.SetInitializer(new Configuration());
            //Database.SetInitializer<MyDbContext>(new CreateDatabaseIfNotExists<MyDbContext>());
        }

        #region [My Entities]
        public DbSet<NotificationTable> Notifications { get; set; }
        public DbSet<UserNotification> UserNotifications { get; set; }
        //public DbSet<UserActivity> UserActivities { get; set; }
        public DbSet<UserActivity> UsersActivity { get; set; }
        public DbSet<Course_Purchase> Course_Purchases { get; set; }
        public DbSet<Withdraw> Withdraws { get; set; }
        public  DbSet<Country> Countries { get; set; }
        public DbSet<MetaSetting> MetaSettings { get; set; }
        public DbSet<City> Cities { get; set; }
        public DbSet<StudentReqSub> StudentReqSubs { get; set; }

        public DbSet<States> States { get; set; }

        public DbSet<Users> Users { get; set; }

        public DbSet<UsersProfile> UsersProfile { get; set; }

        public DbSet<Occupations> Occupations { get; set; }

        public DbSet<Universities> Universities { get; set; }

        public DbSet<Qualifications> Qualifications { get; set; }

        public DbSet<TeacherQualification> TeacherQualifications { get; set; }

        public DbSet<Experience> Experience { get; set; }

        public DbSet<Languages> Languages { get; set; }

        public DbSet<Timings> Timings { get; set; }

        public DbSet<Curriculums> Curriculums { get; set; }

        public DbSet<TeacherCurriculums> TeacherCuriculums  { get; set; }

        public DbSet<Grades> Grades { get; set; }

        public DbSet<TeacherGrades> TeacherGrades { get; set; }

        public DbSet<Subjects> Subjects { get; set; }

        public DbSet<TeacherSubjects> TeacherSubjects { get; set; }

        public DbSet<Topics> Topics { get; set; }

        public DbSet<SubjectFAQS> SubjectFAQS { get; set; }

        public DbSet<TeacherAvailableHours> TeacherAvailableHours { get; set; }

        public DbSet<TrialSessions> TrialSessions { get; set; }

        public DbSet<Sessions> Sessions { get; set; }

        public DbSet<SessionParticipants> SessionParticipants { get; set; }

        public DbSet<SessionChat> SessionChat { get; set; }

        public DbSet<SessionFiles> SessionFiles { get; set; }

        public DbSet<SessionVideos> SessionVideos { get; set; }

        public DbSet<TeachersAvailability> TeacherAvailability { get; set; }

        public DbSet<UserLanguages> UserLanguages { get; set; }

        public DbSet<StudentGrades> StudentGrades { get; set; }
     


        public DbSet<SessionsDuration> SessionsDuration { get; set; }

        // Test related classes

        public DbSet<QuestionsBank> QuestionsBank { get; set; }
        public DbSet<Test> Test { get; set; }
        public DbSet<TestQuestions> TestQuestions { get; set; }
        public DbSet<TestCoveredTopics> TestCoveredTopics { get; set; }
        public DbSet<TestAssigned> TestAssigned { get; set; }
        public DbSet<TestParticipants> TestParticipants { get; set; }
        

        public DbSet<tblStudentTestResult> tblStudentTestResult { get; set; }
        public DbSet<StudentTestAnswers> StudentTestAnswers { get; set; }
        
        // Web RTC Related Classes
        public DbSet<WebRTCRooms> WebRTCRooms { get; set; }

        public DbSet<RatingReviews> RatingReviews { get; set; }

        public DbSet<RudraaRatings> RudraaRatings { get; set; }


        // Course Offerings Related Schema

        public DbSet<CoursesHeader> CourseHeader { get; set; }

        public DbSet<CoursesLink> CourseLink { get; set; }

        public DbSet<CourseSchedule> CourseSchedule { get; set; }

        public DbSet<CourseParticipants> CourseParticipants { get; set; }

        public DbSet<CourseFaqs> CourseFaqs { get; set; }

        public DbSet<CourseDays> CourseDay { get; set; }

        public DbSet<CourseCurriculum> CouurseCurriculum { get; set; }

        public DbSet<AboutCourse> AboutCourse { get; set; }

        public DbSet<Days> Days { get; set; }

        public DbSet<StudentCourseSchedule> StudentCourseSchedule { get; set; }

        public DbSet<CourseSessionsDuration> CourseSessionsDuration { get; set; }

        public DbSet<FreeResources> FreeResources { get; set; }


        // Activity log related tables

        public DbSet<UserActivities> UserActivity { get; set; }

        public DbSet<UserNotifications> UserNotification { get; set; }


        // Notes Related Entities 

        public DbSet<Folders> Folders { get; set; }

        public DbSet<Notes> Notes { get; set; }
        public DbSet<UserBlockReason> UserBlockReasons { get; set; }
        public DbSet<Permission> Permissions { get; set; }
        public DbSet<UserPermission> UserPermissions { get; set; }
        public DbSet<EmailTemplate> EmailTemplates { get; set; }
        public DbSet<Actions> Actions { get; set; }
        public DbSet<BannerSlides> BannerSlides { get; set; }
        public DbSet<PaymentTransactions> PaymentTransactions { get; set; }

        /// <summary>
        /// New Task List Tables Required
        /// </summary>
        public DbSet<CouponTable> CouponTable { get; set; }
        public DbSet<CommisionTable> CommisionTable { get; set; }
        #endregion





        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            modelBuilder.Conventions.Remove<OneToManyCascadeDeleteConvention>();
        }

    }
}
