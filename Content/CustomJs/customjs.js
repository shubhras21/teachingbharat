﻿


// TEACHER REGISTERATION JS
function formclear() {
    $(".formresetClass")[0].reset();
}

function Showmessage(value) {
    var msgvalue = 0;
    if (value === "Created") {
        msgvalue = 1;
    }
    if (value === "Error") {
        msgvalue = 8;
    }
    if (value === "Alreadyexsists") {
        msgvalue = 4;
    }
    if (msgvalue !== 0) {
        message(msgvalue);
        formclear();
    }
}

function otherDescription(e) {
    var isdisabled = $(e).parent('.form-group').find('.form-control').is('[readonly="readonly"]');
    if (isdisabled)
        $(e).parent('.form-group').find('.form-control').attr('readonly', false);
    else
        $(e).parent('.form-group').find('.form-control').attr('readonly', true);
    $(e).parent('.form-group').find('.form-control').val('');

}

function otherDisableDescription(e) {
    $(e).parent('.form-group').find('.form-control').attr('readonly', true);
    $(e).parent('.form-group').find('.form-control').val('');
}

$('.digitalpen').on('change', function () {
    $(this).parent('.form-group').find('.form-control').attr('readonly', true);
    $(e).parent('.form-group').find('.form-control').val('');
});

function onloadpageMessage() {
    var value = $('#errorMessage').text();
    var infovalue = $('#infoMessage').text();

    Showmessage(value);
    if (infovalue != '') {
        $.notify({
            icon: 'pe-7s-gift',
            message: infovalue
        }, {
            type: 'info',
            timer: 3000
        });
    }

}

$('.dropdownlist').on('change', function () {
    var value = $(this).val();
    if (value == 0 && value != '') {
        $(this).parent('.parentclass').find('.othertextfield').fadeIn('slow');
    } else {
        $(this).parent('.parentclass').find('.othertextfield').fadeOut('slow');
    }
});


//EXTERNAL LOGIN JS
function onSignIn(googleUser) {

    var profile = googleUser.getBasicProfile();
    var id_token = googleUser.getAuthResponse().id_token;
    Checkrecord(profile);

};

function assignData(profile) {

    var inst = $('[data-remodal-id=modal]').remodal();
    $('#firstname').val(profile.getGivenName());
    $('#lastname').val(profile.getFamilyName());
    $('#email').val(profile.getEmail());
    $('#tokenid').val(profile.getId());
    inst.open();

}

function Checkrecord(profile) {

    var obj = new Object();
    obj.EmailAddress = profile.getEmail();
    obj.TokenId = profile.getId();

    $.ajax({
        url: '/Account/ExternalSignin',
        type: 'POST',
        cache: false,
        data: JSON.stringify(obj),
        contentType: "application/json;charset=utf-8",
        success: function (data) {
            if (data.result == "Redirect")
                window.location.href = data.url;
            else if (data == 0)
                assignData(profile);

        }
    });

}

function facebookLogin() {
    var inst = $('[data-remodal-id=modalfacebook]').remodal();
    inst.open();
    if (!$("input:radio[name='UserType']:checked").val()) {
        $('#errorMessage').fadeIn('slow');
        return false;
    }
    var value = $("input:radio[name='UserType']:checked").val();
    $.ajax({
        url: '/Account/FacebookLogin',
        type: 'POST',
        cache: false,
        data: "{ 'user': '" + value + "' }",
        //contentType: "application/json;charset=utf-8",
        //success: function (data) {
        //    if (data.result == "Redirect") {
        //        window.location.href = data.url;
        //    }
        //}
    });

}

function signUp() {

    if (!$("input:radio[name='UserType']:checked").val()) {
        $('#errorMessage').fadeIn('slow');
        return false;
    }
    var obj = new Object();
    obj.UserType = $("input:radio[name='UserType']:checked").val();
    obj.FirstName = $('#firstname').val();
    obj.LastName = $('#lastname').val();
    obj.LoginEmail = $('#email').val();
    obj.ExternalLoginId = $('#tokenid').val();

    $.ajax({
        url: '/Account/ExternalSignUp',
        type: 'POST',
        cache: false,
        data: JSON.stringify(obj),
        contentType: "application/json;charset=utf-8",
        success: function (data) {
            if (data.result == "Redirect")
                window.location.href = data.url;
            else
                message(data);
        }
    });

}

