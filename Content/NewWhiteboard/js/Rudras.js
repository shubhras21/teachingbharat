

function SaveScreenShot() {

    html2canvas($('#screen_shot'), {
        onrendered: function (canvas) {
            var a = document.createElement('a');
            a.href = canvas.toDataURL("image/jpeg").replace("image/jpeg", "image/octet-stream");
            a.download = 'MyDrawing.jpg';
            a.click();

        }
    });
}


var SelectedTool;
// Whitebaord JS

var DrawState =
{
    Started: 0,
    Inprogress: 1,
    Completed: 2
}
var DrawTool =
{
    Pencil: 0,
    Line: 1,
    Text: 2,
    Rect: 3,
    Oval: 4,
    Erase: 5,
    Circle: 6,
    Image: 7,
    Undo: 8,
    Redo: 9,
    Page: 10,
    Picture: 11,
    NoTool: 12,
}
var whiteboardHub;
var tool_default = 'pencil';
var canvas, context, canvaso, contexto;

var tools = {};
var tool;
var WIDTH;
var HEIGHT;
var INTERVAL = 20;

var mx, my;
var URL = window.URL || window.URL;

var selectedLineWidth;
var defaultLineWidth;


var currentColor;
var defaultColor;



var isImage;
var isUndo;

// current image on
var vls;

var store = 0;
var gh = 0;

var drawObjectsCollection = [];
var drawPlaybackCollection = [];

// Variables for Maintain History
var cPushArray = new Array();
var cStep = -1;
var ctxs;


// Variables for New and old Canvases
var oPushArray = new Array();
var oStep = -1;
var maintainIndex = undefined;
var count = 2;


// Drag and Drop Variables
var x = 75;
var y = 50;
var dragok = false;




// Functions for Redo and Undo
var mycnas;
var ctxz;
var savedData

function cPush() {
    cStep++;
    if (cStep < cPushArray.length) { cPushArray.length = cStep; }

    console.log("In cpush");
    mycnas = document.getElementById('whiteBoard');
    ctxz = canvas.getContext("2d");
    cPushArray.push(mycnas.toDataURL("image/png"));
    // document.title = cStep + ":" + cPushArray.length;
}

function cUndo() {

    // Old Undo Code

    //if (cStep > 0) {

    //    cStep--;
    //    var src = cPushArray[cStep];

    //    // Send draw
    //    var drawObject = new DrawObject();
    //    drawObject.Tool = DrawTool.Undo;
    //    SetImageName(src);
    //    drawObject.isUndo = GetImageName();
    //    tool.started = true;
    //    drawObject.currentState = DrawState.Completed;
    //    DrawIt(drawObject, true);
    //    tool.started = false;
    //    SelectTool(SelectedTool);

    //}


    if (cStep > 0) {
        cStep--;
        var img = new Image();
        img.crossOrigin = 'Anonymous';
        img.src = cPushArray[cStep];
        img.onload = function () {
            context.drawImage(img, 0, 0);
            Clear();
            updatecanvas();
            var sTool = SelectedTool;
            SelectTool('rect');
            SelectTool(sTool);

            // Updated Undo

            var rName = $("#RName").val();
            var shift = $("#CShift").val();
            if (shift == "Teacher") {

                whiteboardHub.server.undoRedo(1, $("#GNAME").val(), img.src);
            }

        }
        img.src = cPushArray[cStep];


    }

}

function ClientSideUndo(src) {
    //  console.log(src);
    // var rName = $("#RName").val();
    //  var shift = $("#CShift").val();
    //  if (shift == "Teacher" && rName == "Student") {
    var img = new Image();
    img.crossOrigin = 'Anonymous';
    img.onload = function () {

        context.drawImage(img, 0, 0);
        Clear();
        updatecanvas();
        var sTool = SelectedTool;
        SelectTool('rect');
        SelectTool(sTool);
    }
    img.src = src;

}

//}

function ClientSideRedo(src) {
    var rName = $("#RName").val();
    var shift = $("#CShift").val();
    //  if (shift == "Teacher" && rName == "Student") {
    var canvasPic = new Image();
    canvasPic.crossOrigin = 'Anonymous';
    canvasPic.onload = function () {

        context.drawImage(canvasPic, 0, 0);
        Clear();
        updatecanvas();
        var sTool = SelectedTool;
        SelectTool('rect');
        SelectTool(sTool);
    }
    canvasPic.src = src;

    // }

}
function cRedo() {

    if (cStep < cPushArray.length - 1) {
        cStep++;
        var canvasPic = new Image();
        canvasPic.crossOrigin = 'Anonymous';
        canvasPic.onload = function () {

            context.drawImage(canvasPic, 0, 0);
            Clear();
            updatecanvas();
            var sTool = SelectedTool;
            SelectTool('rect');
            SelectTool(sTool);
        }
        canvasPic.src = cPushArray[cStep];
        var rName = $("#RName").val();
        var shift = $("#CShift").val();
        if (shift == "Teacher") {
            whiteboardHub.server.undoRedo(2, $("#GNAME").val(), canvasPic.src);
        }

    }



    //if (cStep < cPushArray.length - 1) {
    //    cStep++;
    //    var src = cPushArray[cStep];
    //    // Send draw
    //    var drawObject = new DrawObject();
    //    drawObject.Tool = DrawTool.Undo;
    //    SetImageName(src);
    //    drawObject.isUndo = GetImageName();
    //    tool.started = true;
    //    drawObject.currentState = DrawState.Completed;
    //    DrawIt(drawObject, true);
    //    tool.started = false;
    //    SelectTool(SelectedTool);
    //}

}

function toggleBG1() {

    setTimeout(function () { $('#divShare').css("background-color", "silver"); toggleBG2() }, 800);
}
function toggleBG2() {
    setTimeout(function () { $('#divShare').css("background-color", "#C8C8C8"); toggleBG1() }, 800);

}
function DrawObject() {
}
function UpdatePlayback(drawObject) {
    if (drawPlaybackCollection.length > 1000) {
        drawPlaybackCollection = [];
        alert("Playback cache is cleared due to more than 1000 items");
    }
    drawPlaybackCollection.push(drawObject);
}
function Clear() {
    canvaso.hieght = canvas.hieght;
    canvaso.width = canvas.width;
}
function Playback() {
    if (drawPlaybackCollection.length == 0) {
        alert("No drawing to play"); return;
    }
    canvaso.hieght = canvas.hieght;
    canvaso.width = canvas.width;

    for (var i = 0; i < drawPlaybackCollection.length; i++) {
        var drawObject = drawPlaybackCollection[i];
        setTimeout(function () { DrawIt(drawObject, false, false); }, 3000);
    }
    drawPlaybackCollection = [];
}
$(document).ready(function () {

    JoinHub();

    try {

        canvaso = document.getElementById('whiteBoard');
        if (!canvaso) {
            alert('Error: Cannot find the imageView canvas element!');
            return;
        }

        if (!canvaso.getContext) {
            alert('Error: no canvas.getContext!');
            return;
        }
        //  setOStep(oStep+1);

        canvaso.width = 1000;
        canvaso.height = 530;

        // canvaso.width = 610;
        // canvaso.height = 650;
        // Get the 2D canvas context.
        contexto = canvaso.getContext('2d');
        ctxs = canvaso.getContext("2d");
        if (!contexto) {
            alert('Error: failed to getContext!');
            return;
        }

        // Add the temporary canvas.
        var container = canvaso.parentNode;


        canvas = document.createElement('canvas');
        if (!canvas) {
            alert('Error: Cannot create a new canvas element!');
            return;
        }

        canvas.id = 'imageTemp';
        canvas.width = canvaso.width;
        canvas.height = canvaso.height;



        container.appendChild(canvas);

        context = canvas.getContext('2d');

        // Activate the default tool.
        var rVal = $("#RName").val();
        SelectTool(tool_default);
        if (rVal !== "Student") {

            SelectTool(tool_default);
            // Attach the mousedown, mousemove and mouseup event listeners.
            canvas.addEventListener('mousedown', ev_canvas, false);
            canvas.addEventListener('mousemove', ev_canvas, false);
            canvas.addEventListener('mouseup', ev_canvas, false);
            canvas.addEventListener('mouseout', ev_canvas, false);

        }
        else {
            SelectTool('notool');

            // Remove the mousedown, mousemove and mouseup event listeners.
            canvas.removeEventListener('mousedown', ev_canvas, false);
            canvas.removeEventListener('mousemove', ev_canvas, false);
            canvas.removeEventListener('mouseup', ev_canvas, false);
            canvas.removeEventListener('mouseout', ev_canvas, false);
        }


        // Custom Drag Events
        //canvas.onmousedown = myDown;
        //canvas.onmouseup = myUp;
        context.clearRect(0, 0, canvas.width, canvas.height);
        toggleBG1();

        // Initialize canvas count
        setOStep(0);
        //OldPush();
        //updatecanvas();

        // Send draw

        //    if (rVal !== "Student") {
        var drawObject = new DrawObject();
        drawObject.Tool = DrawTool.Pencil;
        tool.started = true;
        drawObject.currentState = DrawState.Completed;
        DrawIt(drawObject, true);
        tool.started = false;
        //     }

        if (rVal !== "Student") {

            SelectTool(tool_default);
            // Attach the mousedown, mousemove and mouseup event listeners.
            canvas.addEventListener('mousedown', ev_canvas, false);
            canvas.addEventListener('mousemove', ev_canvas, false);
            canvas.addEventListener('mouseup', ev_canvas, false);
            canvas.addEventListener('mouseout', ev_canvas, false);

        }
        else {
            SelectTool('notool');

            // Remove the mousedown, mousemove and mouseup event listeners.
            canvas.removeEventListener('mousedown', ev_canvas, false);
            canvas.removeEventListener('mousemove', ev_canvas, false);
            canvas.removeEventListener('mouseup', ev_canvas, false);
            canvas.removeEventListener('mouseout', ev_canvas, false);
        }

        cPush();
    }
    catch (err) {
        alert(err.message);
    }

});

function clear(c) {
    c.clearRect(0, 0, WIDTH, HEIGHT);
}

function ev_canvas(ev) {

    var iebody = (document.compatMode && document.compatMode != "BackCompat") ? document.documentElement : document.body

    var dsocleft = document.all ? iebody.scrollLeft : pageXOffset
    var dsoctop = document.all ? iebody.scrollTop : pageYOffset
    var appname = window.navigator.appName;
    try {
        if (ev.layerX || ev.layerX == 0) {  // Firefox
            ev._x = ev.layerX;
            if ('Netscape' == appname)
                ev._y = ev.layerY;
            else
                ev._y = ev.layerY - dsoctop;

        } else if (ev.offsetX || ev.offsetX == 0) { // Opera
            ev._x = ev.offsetX;
            ev._y = ev.offsetY - dsoctop;
        }

        //   ev._x = ev.layerX;
        //   ev._y = ev.layerY;
        // Call the event handler of the tool.
        var func = tool[ev.type];

        if (func) {
            func(ev);
        }
    }
    catch (err) {
        alert(err.message);
    }
}

function getMouse(e) {
    var element = canvaso, offsetX = 0, offsetY = 0;

    if (element.offsetParent) {
        do {
            offsetX += element.offsetLeft;
            offsetY += element.offsetTop;
        } while ((element = element.offsetParent));
    }

    // Add padding and border style widths to offset
    offsetX += stylePaddingLeft;
    offsetY += stylePaddingTop;

    offsetX += styleBorderLeft;
    offsetY += styleBorderTop;

    mx = e.pageX - offsetX;
    my = e.pageY - offsetY;

    mx = e._x;
    my = e._y;
}

function updatecanvas() {
    contexto.drawImage(canvas, 0, 0);
    context.clearRect(0, 0, canvas.width, canvas.height);
}

tools.pencil = function () {
    var tool = this;
    this.started = false;
    drawObjectsCollection = [];
    this.mousedown = function (ev) {
        var drawObject = new DrawObject();
        drawObject.Tool = DrawTool.Pencil;
        tool.started = true;
        drawObject.currentState = DrawState.Started;
        drawObject.StartX = ev._x;
        drawObject.StartY = ev._y;
        DrawIt(drawObject, true);
        drawObjectsCollection.push(drawObject);
    };

    this.mousemove = function (ev) {
        if (tool.started) {
            //     context.lineWidth = selectedLineWidth;
            var drawObject = new DrawObject();
            drawObject.Tool = DrawTool.Pencil;
            drawObject.currentState = DrawState.Inprogress;
            drawObject.CurrentX = ev._x;
            drawObject.CurrentY = ev._y;
            DrawIt(drawObject, true);
            drawObjectsCollection.push(drawObject);
        }
    };

    // This is called when you release the mouse button.
    this.mouseup = function (ev) {
        if (tool.started) {

            var drawObject = new DrawObject();
            drawObject.Tool = DrawTool.Pencil;
            tool.started = false;
            drawObject.currentState = DrawState.Completed;
            drawObject.CurrentX = ev._x;
            drawObject.CurrentY = ev._y;
            DrawIt(drawObject, true);
            drawObject.currentColor = selectedColor();
            drawObject.selectedLineWidth = selectedThickness();
            drawObject.isImage = GetImageName();
            drawObjectsCollection.push(drawObject);
            var message = JSON.stringify(drawObjectsCollection);
            whiteboardHub.server.sendDraw(message, $("#GNAME").val());

        }
    };
    this.mouseout = function (ev) {
        if (tool.started) {
            //var message = JSON.stringify(drawObjectsCollection);
            //whiteboardHub.server.sendDraw(message, $("#GNAME").val());

            tool.started = false;
        }
        tool.started = false;
    }
};

tools.rect = function () {
    var tool = this;
    var drawObject = new DrawObject();
    drawObject.Tool = DrawTool.Rect;
    this.started = false;

    this.mousedown = function (ev) {
        drawObject.currentState = DrawState.Started;
        drawObject.StartX = ev._x;
        drawObject.StartY = ev._y;
        tool.started = true;

    };

    this.mousemove = function (ev) {
        if (!tool.started) {
            return;
        }
        drawObject.currentState = DrawState.Inprogress;
        drawObject.CurrentX = ev._x;
        drawObject.CurrentY = ev._y;
        DrawIt(drawObject, true);
    };

    this.mouseup = function (ev) {
        if (tool.started) {
            drawObject.currentState = DrawState.Completed;
            drawObject.CurrentX = ev._x;
            drawObject.CurrentY = ev._y;
            DrawIt(drawObject, true);
            tool.started = false;

        }

    };
};


tools.circle = function () {
    var tool = this;
    var drawObject = new DrawObject();
    drawObject.Tool = DrawTool.Circle;
    this.started = false;

    this.mousedown = function (ev) {
        drawObject.currentState = DrawState.Started;
        drawObject.StartX = ev._x;
        drawObject.StartY = ev._y;
        tool.started = true;

    };

    this.mousemove = function (ev) {
        if (!tool.started) {
            return;
        }
        drawObject.currentState = DrawState.Inprogress;
        drawObject.CurrentX = ev._x;
        drawObject.CurrentY = ev._y;
        DrawIt(drawObject, true);
    };

    this.mouseup = function (ev) {
        if (tool.started) {
            drawObject.currentState = DrawState.Completed;
            drawObject.CurrentX = ev._x;
            drawObject.CurrentY = ev._y;
            DrawIt(drawObject, true);
            tool.started = false;

        }
    };
};

tools.line = function () {
    var tool = this;
    var drawObject = new DrawObject();
    drawObject.Tool = DrawTool.Line;
    this.started = false;

    this.mousedown = function (ev) {
        drawObject.currentState = DrawState.Started;
        drawObject.StartX = ev._x;
        drawObject.StartY = ev._y;
        tool.started = true;
    };

    this.mousemove = function (ev) {
        if (!tool.started) {
            return;
        }
        drawObject.currentState = DrawState.Inprogress;
        drawObject.CurrentX = ev._x;
        drawObject.CurrentY = ev._y;
        DrawIt(drawObject, true);
    };

    this.mouseup = function (ev) {
        if (tool.started) {
            drawObject.currentState = DrawState.Completed;
            drawObject.CurrentX = ev._x;
            drawObject.CurrentY = ev._y;
            DrawIt(drawObject, true);
            tool.started = false;
        }
    };
};

tools.text = function () {
    var tool = this;
    this.started = false;
    var drawObject = new DrawObject();
    drawObject.Tool = DrawTool.Text;
    this.mousedown = function (ev) {

        if (!tool.started) {
            tool.started = true;
            drawObject.currentState = DrawState.Started;
            drawObject.StartX = ev._x;
            drawObject.StartY = ev._y;
            var text_to_add = prompt('Enter the text:', ' ', 'Add Text');
            drawObject.Text = "";
            drawObject.Text = text_to_add;
            if (text_to_add.length < 1) {
                tool.started = false;
                return;
            }

            DrawIt(drawObject, true);
            tool.started = false;
            updatecanvas();
        }
    };

    this.mousemove = function (ev) {
        if (!tool.started) {
            return;
        }
    };

    this.mouseup = function (ev) {
        if (tool.started) {
            tool.mousemove(ev);
            tool.started = false;
            updatecanvas();
        }
    };
}

tools.erase = function (ev) {


    var tool = this;
    this.started = false;
    var drawObject = new DrawObject();
    drawObject.Tool = DrawTool.Erase;
    this.mousedown = function (ev) {
        tool.started = true;
        drawObject.currentState = DrawState.Started;
        drawObject.StartX = ev._x;
        drawObject.StartY = ev._y;
        DrawIt(drawObject, true);
    };
    this.mousemove = function (ev) {
        if (!tool.started) {
            return;
        }
        drawObject.currentState = DrawState.Inprogress;
        drawObject.CurrentX = ev._x;
        drawObject.CurrentY = ev._y;
        DrawIt(drawObject, true);
    };
    this.mouseup = function (ev) {
        drawObject.currentState = DrawState.Completed;
        drawObject.CurrentX = ev._x;
        drawObject.CurrentY = ev._y;
        DrawIt(drawObject, true);
        tool.started = false;
    }
}

function fireEvent(element, event) {
    var evt;
    if (document.createEventObject) {
        // dispatch for IE
        evt = document.createEventObject();
        return element.fireEvent('on' + event, evt)
    }
    else {
        // dispatch for firefox + others
        evt = document.createEvent("HTMLEvents");
        evt.initEvent(event, true, true); // event type,bubbling,cancelable
        return !element.dispatchEvent(evt);
    }
}

function SelectTool(toolName) {
    debugger;
    if (tools[toolName]) {
        tool = new tools[toolName]();
        SelectedTool = toolName;
    }

    if (toolName == "line" || toolName == "curve" || toolName == "measure" || toolName == "rect" || toolName == "circle")

        // canvas.style.cursor = "crosshair";
        //    canvas.style.cursor = "pointer";
        // $(".dum")
        $("#imageTemp").addClass("customP");
    else if (toolName == "select")
        canvas.style.cursor = "default";
    else if (toolName == "text")
        canvas.style.cursor = "text";

    ChangeIcons(toolName);

}
var currentActiveId;
function ChangeIcons(toolName) {

    if (toolName == "line") {
        $("." + currentActiveId + "-icon").removeClass(currentActiveId + "-active");
        $(".shape-icon").addClass("shape-active"); currentActiveId = 'shape';
    }
    else if (toolName == "pencil") {
        $("." + currentActiveId + "-icon").removeClass(currentActiveId + "-active");
        $(".pencil-icon").addClass("pencil-active"); currentActiveId = 'pencil';
    }
    else if (toolName == "rect") {
        $("." + currentActiveId + "-icon").removeClass(currentActiveId + "-active");
        $(".shape-icon").addClass("shape-active"); currentActiveId = 'shape';
    }
    else if (toolName == "circle") {
        $("." + currentActiveId + "-icon").removeClass(currentActiveId + "-active");
        $(".shape-icon").addClass("shape-active"); currentActiveId = 'shape';
    }

    else if (toolName == "erase") {
        $("." + currentActiveId + "-icon").removeClass(currentActiveId + "-active");
        $(".erasor-icon").addClass("erasor-active"); currentActiveId = 'erasor';
    }
    else if (toolName == "text") {
        $("." + currentActiveId + "-icon").removeClass(currentActiveId + "-active");
        $(".text-icon").addClass("text-active"); currentActiveId = 'text';
    }
    else if ((toolName == "G1.png") || (toolName == "G2.png") || (toolName == "G3.png")) {
        $("." + currentActiveId + "-icon").removeClass(currentActiveId + "-active");
        $(".grid-icon").addClass("grid-active"); currentActiveId = 'grid';
    }
}

function ChangeGrids(imgName) {

    var drawObject = new DrawObject();
    drawObject.Tool = DrawTool.Image;
    tool.started = true;
    drawObject.currentState = DrawState.Started;
    drawObject.StartX = 0;
    drawObject.StartY = 0;
    var a = "http://localhost/tutoring/GridImages/" + imgName;
    //  alert("a from change grid : " + a);
    SetImageName(a);
    DrawIt(drawObject, true);
    tool.started = false;

}

var drawobjects = [];

function JoinHub() {

    whiteboardHub = $.connection.whiteboardHub;
    $.connection.hub.start().done(function () {
        console.log("Connection Started");
        whiteboardHub.server.joinGroup($("#GNAME").val(), $("#UNAME").val())
    });

    //whiteboardHub.client.HandleMyDraw = function (message) {
    //    var drawObjectCollection = jQuery.parseJSON(message);
    //    for (var i = 0; i < drawObjectCollection.length; i++) {
    //        ControlColor(drawObjectCollection[i].currentColor);
    //        ControlThickness(drawObjectCollection[i].selectedLineWidth);
    //        SetImageName(drawObjectCollection[i].isImage);
    //        DrawIt(drawObjectCollection[i], false);
    //    }
    //}
}

// Drag Event
function myMove(e) {
    // alert("In out Mouse Move" + dragok);
    if (dragok) {
        x = e.pageX - canvas.offsetLeft;
        y = e.pageY - canvas.offsetTop;
    }
}

function myDown(e) {

    if (e.pageX < x + 15 + canvas.offsetLeft && e.pageX > x - 15 +
    canvas.offsetLeft && e.pageY < y + 15 + canvas.offsetTop &&
    e.pageY > y - 15 + canvas.offsetTop) {

        x = e.pageX - canvas.offsetLeft;
        y = e.pageY - canvas.offsetTop;
        dragok = true;
        canvas.onmousemove = myMove;
    }
    dragok = true;
}

function myUp() {

    dragok = false;
    canvas.onmousemove = null;
}

function SaveDrawings() {

    var canvas1 = document.getElementById("imageTempt");
    var context1 = canvas1.getContext("2d");

    var imgs = canvas1.toDataURL("image/png");
    var img = canvaso.toDataURL("image/png");

    WindowObject = window.open('', "PrintPaintBrush", "toolbars=no,scrollbars=yes,status=no,resizable=no");
    WindowObject.document.open();
    WindowObject.document.writeln('<img src="' + imgs + '"/>');
    WindowObject.document.close();
    WindowObject.focus();

}


function toDataUrl(src, callback, outputFormat) {
    var img = new Image();
    img.crossOrigin = 'Anonymous';
    img.onload = function () {
        //  var canvas = document.createElement('CANVAS');
        //  var ctx = canvas.getContext('2d');
        var mycnas = document.getElementById('whiteBoard');
        var ctxz = mycnas.getContext("2d");
        var dataURL;
        //canvas.height = this.height;
        //canvas.width = this.width;
        ctxz.drawImage(this, 30, 5, 400, 400);
        dataURL = mycnas.toDataURL(outputFormat);
        callback(dataURL);
    };
    img.src = src;
    if (img.complete || img.complete === undefined) {
        img.src = "data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///ywAAAAAAQABAAACAUwAOw==";
        img.src = src;
    }
}


//var base64 = getBase64Image(bgImageUrl);

//function LoadImageIntoCanvas(bgImageUrl) {

//    //if (bgImageUrl !== null && bgImageUrl !== '') {
//    //    var srcss;
//    //    var img = new Image();
//    //    //   img.crossOrigin = 'Anonymous';
//    //    img.src = bgImageUrl;
//    //    img.onload = function () {
//    //        //  context.drawImage(img, 0, 0);
//    //        context.drawImage(img, 30, 5, 400, 400);
//    //        Clear();
//    //        updatecanvas();

//    //        var sTool = SelectedTool;
//    //        SelectTool('rect');
//    //        SelectTool(sTool);

//    //        var oldIndex = getOStep();

//    //        if (typeof oPushArray[oldIndex] == 'undefined') {
//    //            cPush();
//    //            OldPush();
//    //        }
//    //        else {
//    //            cPush();
//    //            UpdatePush(oldIndex);
//    //        }

//    //        mycnas = document.getElementById('whiteBoard');
//    //        ctxz = canvas.getContext("2d");
//    //        srcss = mycnas.toDataURL("image/png");

//    //        var rName = $("#RName").val();
//    //        var shift = $("#CShift").val();
//    //        if (shift == "Teacher" && srcss != '' && srcss != null) {
//    //            var msgs = JSON.stringify(srcss);
//    //            whiteboardHub.server.loadImage($("#GNAME").val(), msgs);
//    //        }

//    //    }
//    //    var srcs;
//    //    //toDataUrl(bgImageUrl, function (base64Img) {

//    //    //    img.src = base64Img;
//    //    //   // console.log("In first : " + srcs);
//    //    //    srcs = base64Img;

//    //    //});


//    //}




//    // Send draw
//    var drawObject = new DrawObject();
//    drawObject.Tool = DrawTool.Picture;
//    SetImageName(bgImageUrl);
//    drawObject.isUndo = GetImageName();
//    tool.started = true;
//    drawObject.currentState = DrawState.Completed;
//    DrawIt(drawObject, true);
//    tool.started = false;

//    var sTool = SelectedTool;
//    SelectTool('rect');
//    SelectTool(sTool);

//}

function LoadImageIntoCanvas(bgImageUrl) {

    if (bgImageUrl !== null && bgImageUrl !== '') {

        var srcss;
        var img = new Image();
        //   img.crossOrigin = 'Anonymous';
        img.src = bgImageUrl;
        img.onload = function () {
            //  context.drawImage(img, 0, 0);
            context.drawImage(img, 30, 5, 400, 400);
            Clear();
            updatecanvas();

            var sTool = SelectedTool;
            SelectTool('rect');
            SelectTool(sTool);

            var oldIndex = getOStep();

            if (typeof oPushArray[oldIndex] == 'undefined') {
                cPush();
                OldPush();
            }
            else {
                cPush();
                UpdatePush(oldIndex);
            }

            //mycnas = document.getElementById('whiteBoard');
            //ctxz = canvas.getContext("2d");
            //srcss = mycnas.toDataURL("image/png");

            // srcss = bgImageUrl;

            var rName = $("#RName").val();
            var shift = $("#CShift").val();
            if (shift == "Teacher") {
                // console.log("here we are!!");
                var msgs = JSON.stringify(bgImageUrl);
                whiteboardHub.server.loadImage($("#GNAME").val(), msgs);
            }


        }
        var srcs;



        //toDataUrl(bgImageUrl, function (base64Img) {

        //    img.src = base64Img;
        //   // console.log("In first : " + srcs);
        //    srcs = base64Img;

        //});


    }




    // Send draw
    //var drawObject = new DrawObject();
    //drawObject.Tool = DrawTool.Picture;
    //SetImageName(bgImageUrl);
    //drawObject.isUndo = GetImageName();
    //tool.started = true;
    //drawObject.currentState = DrawState.Completed;
    //DrawIt(drawObject, true);
    //tool.started = false;

    //var sTool = SelectedTool;
    //SelectTool('rect');
    //SelectTool(sTool);

}


function CalledAfterCompleted() {
    alert("completed after");
    mycnas = document.getElementById('whiteBoard');
    ctxz = canvas.getContext("2d");
    var srcss = mycnas.toDataURL("image/png");

    var rName = $("#RName").val();
    var shift = $("#CShift").val();
    if (shift == "Teacher" && srcss != '' && srcss != null) {
        var msgs = JSON.stringify(srcss);
        whiteboardHub.server.loadImage($("#GNAME").val(), msgs);
    }
}

function ClientSideLoadImageIntoCanvas(src) {

    // alert(src);

    var img = new Image();
    img.crossOrigin = 'Anonymous';
    img.src = src;
    img.onload = function () {

        //context.drawImage(img, 0, 0);
        // context = canvas.getContext('2d');
        context.drawImage(img, 30, 5, 400, 400);
        Clear();
        updatecanvas();
        var oldIndex = getOStep();

        //if (typeof oPushArray[oldIndex] == 'undefined') {
        //    cPush();
        //    OldPush();
        //}
        //else {
        //    cPush();
        //    UpdatePush(oldIndex);
        //}



        //var sTool = SelectedTool;
        //SelectTool('rect');
        //SelectTool(sTool);
    }
    img.src = src;
}

function UpdateCanvass(path) {

    var file_UploadImg = document.getElementById("fileUploadImg");
    LoadImageIntoCanvas(path);

}


//$("#fileUploadImg").on('change', function () {
//    var selectedFile = this.files[0];
//    selectedFile.convertToBase64(function (base64) {
//        UpdateCanvass(base64);
//    })
//});


//File.prototype.convertToBase64 = function (callback) {
//    var reader = new FileReader();
//    reader.onload = function (e) {
//        callback(e.target.result)
//    };
//    reader.onerror = function (e) {
//        callback(null);
//    };
//    reader.readAsDataURL(this);
//};

document.getElementById('fileUploadImg').onchange = function () {

    var paths;

    $("#validatormsg").empty();
    var val = $(this).val();
    var input = this;
    switch (val.substring(val.lastIndexOf('.') + 1).toLowerCase()) {
        case 'jpg': case 'jpeg': case 'png':

            var formData = new FormData();
            var file = input.files[0];
            formData.append("imageUploadForm", file);

            $.ajax({
                type: "POST",
                url: '/tutoring/Whiteboard/SaveImage',
                data: formData,
                dataType: 'json',
                contentType: false,
                processData: false,
                success: function (response) {
                    paths = response.path;
                    UpdateCanvass(paths);
                    // alert('succes!! ' + response.path);
                },
                error: function (error) {
                    alert("errror");
                }
            });

            break;
        default:
            $(this).val('');
            alert("Please choose valid image format jpg, jpeg, png");
            break;

    }
};

// Control Thickness of Pencils

function ControlThickness(value) {
    selectedLineWidth = value;
}
function selectedThickness() {
    return selectedLineWidth;
}

function ControlColor(value) {
    currentColor = value;
}
function selectedColor() {
    return currentColor;
}
function SetImageName(value) {
    if (value == 'none') {
        isImage = 'none';

    }
    else
        isImage = value;
    //  alert(value);
}
function GetImageName() {
    return isImage;
}


function AddCanvas() {

    try {

        cPushArray = [];

        cStep = -1;


        gh = oStep + 1;
        setOStep(gh);

        canvaso = document.getElementById('whiteBoard');

        if (!canvaso) {
            alert('Error: Cannot find the imageView canvas element!');
            return;
        }

        if (!canvaso.getContext) {
            alert('Error: no canvas.getContext!');
            return;
        }
        canvaso.width = 1000;
        canvaso.height = 530;

        // Get the 2D canvas context.
        contexto = canvaso.getContext('2d');
        ctxs = canvaso.getContext("2d");
        if (!contexto) {
            alert('Error: failed to getContext!');
            return;
        }

        // Add the temporary canvas.
        var container = canvaso.parentNode;
        canvas = document.createElement('canvas');
        if (!canvas) {
            alert('Error: Cannot create a new canvas element!');
            return;
        }

        canvas.id = 'imageTemp';
        canvas.width = canvaso.width;
        canvas.height = canvaso.height;
        container.appendChild(canvas);

        context = canvas.getContext('2d');

        // Activate the default tool.
        SelectTool(tool_default);

        // Attach the mousedown, mousemove and mouseup event listeners.
        canvas.addEventListener('mousedown', ev_canvas, false);
        canvas.addEventListener('mousemove', ev_canvas, false);
        canvas.addEventListener('mouseup', ev_canvas, false);
        canvas.addEventListener('mouseout', ev_canvas, false);

        context.clearRect(0, 0, canvas.width, canvas.height);
        Clear();

    }
    catch (err) {
        alert(err.message);
    }

    //  OldPush();
    //  updatecanvas();


    // Send draw
    var drawObject = new DrawObject();
    drawObject.Tool = DrawTool.Pencil;

    tool.started = true;
    drawObject.currentState = DrawState.Completed;
    DrawIt(drawObject, true);
    tool.started = false;

    // Dynamic generate a HTML Element and assign IDs to onclick


    var a = document.createElement('a');
    var linkText = document.createTextNode("whiteboard " + count);
    count = count + 1;
    a.appendChild(linkText);
    a.title = oStep + 1;
    a.href = "#";
    //a.style.fontSize = "30px";
    //a.style.color = "white";
    //a.style.background = "grey";
    //a.style.margin = "0.2%";
    a.id = "New_" + oStep;

    //  document.body.appendChild(a);
    $("#liref").append(a);
    var concat = "New_" + oStep;
    var element = document.getElementById(concat);
    var value = oStep;
    var rVal = $("#RName").val();
    if (rVal == "Student") {
        element.setAttribute("onclick", "EnableActive(this);");
        element.setAttribute("onclick", "return false;");
    }
    else {
        element.setAttribute("onclick", "OldUndo(" + value + "); EnableActive(this);SendUndoRedoAlert(" + value + ");");
    }

    $("#liref").children().removeClass("active");
    element.setAttribute("class", "active");

    if (count > 5) {
        $(".AddBoard").hide();
    }



}

// Functions for Redo and Undo
var OldCanvas;
var Oldctxz;
var OldsavedData;

function OldPush() {

    oStep++;
    if (oStep < oPushArray.length) { oPushArray.length = oStep; }

    OldCanvas = document.getElementById('whiteBoard');
    Oldctxz = canvas.getContext("2d");
    oPushArray.push(OldCanvas.toDataURL("image/png"));
}

function UpdatePush(value) {
    // alert("In update Push : " + value)
    var OldCanvass = document.getElementById('whiteBoard');
    var Oldctxzs = OldCanvass.getContext("2d");
    var az = OldCanvas.toDataURL("image/png");
    oPushArray[value] = az;
}

function OldUndo(id) {

    var rVal = $("#RName").val();

    //if (rVal !== "Student") {

    cPushArray = [];
    var cns = document.getElementById('whiteBoard');
    var isExist = cns.toDataURL("image/png");
    if (id <= oPushArray.length) {
        var img = new Image();
        img.crossOrigin = 'Anonymous';
        img.onload = function () {
            context.drawImage(img, 0, 0);
            Clear();
            updatecanvas();
        }
        img.src = oPushArray[id];
        SelectTool('pencil');
    }
    updatecanvas();
    setOStep(id);
    // }

}

function OldRedo() {
    var rVal = $("#RName").val();

    // if (rVal !== "Student") {

    if (oStep < oPushArray.length - 1) {
        oStep++;
        var canvasPic = new Image();
        canvasPic.crossOrigin = 'Anonymous';
        canvasPic.onload = function () {
            context.drawImage(canvasPic, 0, 0);
            Clear();
            updatecanvas();
        }
        canvasPic.src = oPushArray[oStep];

    }
    // }
}


function setOStep(value) {
    maintainIndex = value;
}
function getOStep() {
    return maintainIndex;
}

function setCurrentIndex(value) {
    vls = value;
}
function getCurrentIndex() {
    return vls;
}

function RequestFromStudent(ids, name) {
    var gName = $("#GNAME").val();
    // alert(name);
    whiteboardHub.server.requestFromStudent(ids, gName, name);
}

$(function () {


})

jQuery(document).ready(function () {
    setTimeout(function () {




    }, 0);


    whiteboardHub.client.HandleMyDraw = function (message) {
        var drawObjectCollection = jQuery.parseJSON(message);
        for (var i = 0; i < drawObjectCollection.length; i++) {
            ControlColor(drawObjectCollection[i].currentColor);
            ControlThickness(drawObjectCollection[i].selectedLineWidth);
            SetImageName(drawObjectCollection[i].isImage);
            DrawIt(drawObjectCollection[i], false);
        }
    }


    setInterval(function () {
        // Ensure we're connected (don't want to be pinging in any other state).
        if ($.connection.hub.state === $.signalR.connectionState.connected) {
            whiteboardHub.server.ping($("#GNAME").val()).fail(function () {
                // Failed to ping the server, we could either try one more time to ensure we can't reach the server
                // or we could fail right here.
                // TryAndRestartConnection(); // Your method
                alert("Connection failed");

            });
        }
    }, 10000);


    whiteboardHub.client.SendRequest = function (requestFrom, rName) {

        var rNames = $("#RName").val();
        if (rNames == "Teacher") {
            // alert("here we are ! ");
            $("#UpdateIDS").html(rName);
            $('#myModal').modal('show');
        }
    }

    whiteboardHub.client.PerformUndoRedo = function (status, src) {
        if (status == 1) {
            // cUndo();
            ClientSideUndo(src);
        }
        else if (status == 2) {
            //    cRedo();
            ClientSideRedo(src);
        }
    }

    whiteboardHub.client.LoadImageIntoCanvas = function (src) {
        var srcs = jQuery.parseJSON(src);
        ClientSideLoadImageIntoCanvas(srcs);
    }

    whiteboardHub.client.AddCanvas = function () {

        AddCanvas();

        var rName = $("#RName").val();
        var sName = $("#CShift").val();
        if (rName == "Student" && sName == "Student") {
            SelectTool('notool');

            // Remove the mousedown, mousemove and mouseup event listeners.
            canvas.removeEventListener('mousedown', ev_canvas, false);
            canvas.removeEventListener('mousemove', ev_canvas, false);
            canvas.removeEventListener('mouseup', ev_canvas, false);
            canvas.removeEventListener('mouseout', ev_canvas, false);
        }
    };

    whiteboardHub.client.SendUndoRedoAlert = function (id) {
        OldUndo(id);
        $("#liref").children().removeClass("active");
        $("#New_" + id).addClass("active");
    };
    whiteboardHub.client.AlertClear = Clear;

    whiteboardHub.client.AllowAccess = function (id) {
        var uName = $("#UNAME").val();
        if (uName == id) {
            SelectTool('pencil');

            // Attach the mousedown, mousemove and mouseup event listeners.
            canvas.addEventListener('mousedown', ev_canvas, false);
            canvas.addEventListener('mousemove', ev_canvas, false);
            canvas.addEventListener('mouseup', ev_canvas, false);
            canvas.addEventListener('mouseout', ev_canvas, false);

            $("#TopBHide").css('display', 'block');
            $(".dummys").css('display', 'block');

            $("#CShift").val("Teacher");


            //  $("#RName").val("Teacher");
        }



    }


    $.connection.hub.disconnected(function () {
        clearInterval(intervalHandle);
    });


    whiteboardHub.client.BlockAccess = function (id) {

        var uName = $("#UNAME").val();
        if (uName == id) {

            SelectTool('notool');

            // Remove the mousedown, mousemove and mouseup event listeners.
            canvas.removeEventListener('mousedown', ev_canvas, false);
            canvas.removeEventListener('mousemove', ev_canvas, false);
            canvas.removeEventListener('mouseup', ev_canvas, false);
            canvas.removeEventListener('mouseout', ev_canvas, false);

            $("#TopBHide").css('display', 'none');
            $(".dummys").css('display', 'none');

            $("#CShift").val("Student");

            //$("#AllowStudent").show();
            //$("#BlockStudent").hide();

        }

    }



    whiteboardHub.client.PauseSession = function () {

        var rNames = $("#RName").val();
        if (rNames == "Student") {

            pause();
            $("#UpdateCon").empty();
            $("#UpdateCon").html("Session is paused");
            $('#SessionStatus').modal('show');

        }
    }

    whiteboardHub.client.ResumeSession = function () {

        var rNames = $("#RName").val();
        if (rNames == "Student") {

            resume();
            $("#UpdateCon").empty();
            $("#UpdateCon").html("Session is Resumed");
            $('#SessionStatus').modal('hide');

        }
    }


    whiteboardHub.client.EndSession = function () {

        var rNames = $("#RName").val();
        if (rNames == "Student") {

            $("#UpdateCon").empty();
            $("#UpdateCon").html("Session is Ended by teacher. You will be redirected to dashboard within 5 seconds");
            $('#SessionStatus').modal('show');

            setTimeout(function () {
                window.location.href = "http://localhost/tutoring/student/student/index";
            }, 5000);

        }
    }
});


function SendBoardAlert() {
    // alert("In send Board Alert");
    whiteboardHub.server.addCanvas($("#GNAME").val());
}

function SendUndoRedoAlert(id) {
    whiteboardHub.server.sendUndoRedoAlert($("#GNAME").val(), id);
}

function EnableActive(c) {

    $("#liref").children().removeClass("active");
    $(c).addClass("active");
}

function AlertClear() {
    whiteboardHub.server.alertClear($("#GNAME").val());
}

function AllowAccess() {

    var id = this.id,
    idArr = id.split("_");

    $("#AllowStudent_" + idArr[1]).hide();
    $("#BlockStudent_" + idArr[1]).show();

    whiteboardHub.server.allowAccess($("#GNAME").val(), idArr[1]);
}

function BlockAccess() {
    var id = this.id,
    idArr = id.split("_");

    $("#AllowStudent_" + idArr[1]).show();
    $("#BlockStudent_" + idArr[1]).hide();
    whiteboardHub.server.blockAccess($("#GNAME").val(), idArr[1]);
}

function UpdateMyself() {
    var img = new Image();
    var mycnas = document.getElementById('whiteBoard');
    var srcss = mycnas.toDataURL("image/png");
    img.src = srcss;
    img.onload = function () {
        context.drawImage(img, 30, 5, 400, 400);
        Clear();
        //   updatecanvas();
    }
    // img.src = srcss;
}

function Paused() {
    $("#Pse").hide();
    $("#Rse").show();
    whiteboardHub.server.pauseSession($("#GNAME").val());
}

function Resumed() {
    $("#Rse").hide();
    $("#Pse").show();
    whiteboardHub.server.resumeSession($("#GNAME").val());
}

function EndSessions() {
    whiteboardHub.server.endSession($("#GNAME").val());

    var link = $("#GNAME").val();
    var sid = $("#SIDS").val();
    window.location.href = "http://localhost/tutoring/whiteboard/UpdateSession?SID=" + sid + "&link=" + link;

}

// All Timer Logic

paused = false;
// Set Minutes
var rem = Math.floor($("#EndTime").val());
var mins = rem;

// calculate the seconds
var secs = mins * 60;

var t = 0;
var flagTimer = 'resume';
function countdown() {
    t = setTimeout('Decrement()', 1000);
}
function Decrement() {
    if (document.getElementById) {

        minutes = document.getElementById("minutes");
        seconds = document.getElementById("seconds");

        // if less than a minute remaining
        if (seconds < 59) {
            seconds.value = secs;
        }
        else {

            minutes.value = getminutes();

            if (minutes.value < 0) {
                clearTimeout(t);
                t = 0;

                $("#UpdateCon").empty();
                $("#UpdateCon").html("Session is Ended. You will be redirected to dashboard within 5 seconds");
                $('#SessionStatus').modal('show');

                setTimeout(function () {
                    var value = $("#RName").val();
                    if (value == "Student") {
                        window.location.href = "http://localhost/tutoring/student/student/index";
                    }
                    else {
                        var link = $("#GNAME").val();
                        var sid = $("#SIDS").val();
                        window.location.href = "http://localhost/tutoring/whiteboard/UpdateSession?SID=" + sid + "&link=" + link;
                        clearTimeout(t);
                        t = 0;
                    }
                }, 3000);

            }
            seconds.value = getseconds();
        }
        secs--;
        t = setTimeout('Decrement()', 1000);
    }
}
function getminutes() {
    // minutes is seconds divided by 60, rounded down
    mins = Math.floor(secs / 60);
    return mins;
}
function getseconds() {
    // take mins remaining (as seconds) away from total seconds remaining
    return secs - Math.round(mins * 60);
}
function pause() {
    clearTimeout(t);
    t = 0;
}
function resume() {
    t = setTimeout('Decrement()', 1000);
}



function AllowSelf() {

    $("#AllowSelf").show();
    $("#BlockSelf").hide();

    SelectTool('pencil');

    // Attach the mousedown, mousemove and mouseup event listeners.
    canvas.addEventListener('mousedown', ev_canvas, false);
    canvas.addEventListener('mousemove', ev_canvas, false);
    canvas.addEventListener('mouseup', ev_canvas, false);
    canvas.addEventListener('mouseout', ev_canvas, false);

}

function BlockSelf() {
    $("#BlockSelf").show();
    $("#AllowSelf").hide();

    SelectTool('notool');

    // Remove the mousedown, mousemove and mouseup event listeners.
    canvas.removeEventListener('mousedown', ev_canvas, false);
    canvas.removeEventListener('mousemove', ev_canvas, false);
    canvas.removeEventListener('mouseup', ev_canvas, false);
    canvas.removeEventListener('mouseout', ev_canvas, false);

}