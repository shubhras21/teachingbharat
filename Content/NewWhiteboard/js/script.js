/* globals Erizo */
'use strict';
var serverUrl = '/';
var localStream, screenStream, room, recording, recordingId;
var streamList = [];


function getParameterByName(name) {
    name = name.replace(/[\[]/, '\\\[').replace(/[\]]/, '\\\]');
    var regex = new RegExp('[\\?&]' + name + '=([^&#]*)'),
        results = regex.exec(location.search);
    return results == null ? '' : decodeURIComponent(results[1].replace(/\+/g, ' '));
}

function testConnection() {  // jshint ignore:line
    window.location = '/connection_test.html';
}

function startRecording() {  // jshint ignore:line
    if (room !== undefined) {
        if (!recording) {
            room.startRecording(localStream, function (id) {
                recording = true;
                recordingId = id;
            });

        } else {
            room.stopRecording(recordingId);
            recording = false;
        }
    }
}

var slideShowMode = false;

function toggleSlideShowMode() {  // jshint ignore:line
    var streams = room.remoteStreams;
    var cb = function (evt) {
        console.log('SlideShowMode changed', evt);
    };
    slideShowMode = !slideShowMode;
    for (var index in streams) {
        var stream = streams[index];
        if (localStream.getID() !== stream.getID()) {
            console.log('Updating config');
            stream.updateConfiguration({ slideShowMode: slideShowMode }, cb);
        }
    }
}

function sendSignal(trigger) {
    if (trigger == 2) {
        localStream.muteAudio(true, function (result) {
            if (result === 'error') {
                console.log("There was an error unmuting the steram")
            }
        });
        /*
        for(var i in streamList){
        streamList[i].muteAudio(true, function (result) {
     if (result === 'error') {
       console.log("There was an error unmuting the steram")
     }
   });*/


    } else if (trigger == 3) {
        localStream.muteAudio(false, function (result) {
            if (result === 'error') {
                console.log("There was an error unmuting the steram")
            }
        });

    }
    localStream.sendData({ trigger: trigger }, function (result) { console.log(result); });
    /*
    for(var i in streamList){
     var stream = streamList[i];
        console.log(stream);
          streamList[i].sendData({text:'Hello test signal from data', timestamp:12321312});
    
    }
    */
}



window.onload = function () {


    recording = false;
    var screen = getParameterByName('screen');
    var roomName = getParameterByName('room') || 'basicExampleRoom';
    console.log('Selected Room', roomName);

    //screenStream = Erizo.Stream({screen: true, data: true, attributes: {name:'myStream'}});
    var createToken = function (userName, role, roomName, callback) {

        var req = new XMLHttpRequest();
        var url = "https://media.rudraa.org:3004/" + 'createToken/';
        var body = { username: userName, role: role, room: roomName };
        console.log("create" + createToken);
        req.onreadystatechange = function () {
            if (req.readyState === 4) {
                callback(req.responseText);
            }
        };

        req.open('POST', url, true);
        req.setRequestHeader('Content-Type', 'application/json');
        req.send(JSON.stringify(body));
        /*
              $.ajax({
                   traditional: true,
                       type: "POST",
                       url: "https://10.90.1.167:3004/createToken/",
                        data: body ,
                       
                       //data: "test_id=" + id  ,
                     
                    
                  }).done(function( response ) {
                   
                  console.log("********************************************************");
                    console.log(response);
                  });
        */
    };

};

function initLicode(_roomId, token, _name, _id) {

    var config = {
        audio: true,
        video: true,
        data: true,
        //screen: screen,
        videoSize: [320, 240, 320, 240],
        videoFrameRate: [10, 20],
        attributes: { name: _name, type: 'teacher' }
    };
    // If we want screen sharing we have to put our Chrome extension id.videoFrameRate: [30, 30]
    // The default one only works in our Lynckia test servers.
    // If we are not using chrome, the creation of the stream will fail regardless.
    if ('screen') {

        config.extensionId = 'nmkelgcaokmaenedanppappbhndeknfn';
    }
    localStream = Erizo.Stream(config);

    //createToken('ehsan', 'presenter', roomName, function (response) {
    //var token = response;
    console.log("Token : " + token);
    room = Erizo.Room({ token: token });

    localStream.addEventListener('access-accepted', function () {

        var subscribeToStreams = function (streams) {
            var cb = function (evt) {
                console.log('Bandwidth Alert', evt.msg, evt.bandwidth);
            };
            for (var index in streams) {
                var stream = streams[index];
                if (localStream.getID() !== stream.getID()) {
                    room.subscribe(stream, { slideShowMode: false, metadata: { type: 'subscriber' } });
                    stream.addEventListener('bandwidth-alert', cb);
                }
            }
        };

        room.addEventListener('room-connected', function (roomEvent) {

            var options = { metadata: { type: 'publisher' } };

            var enableSimulcast = getParameterByName('simulcast');
            if (enableSimulcast) options._simulcast = { numSpatialLayers: 2 };

            var onlySubscribe = getParameterByName('onlySubscribe');
            if (!onlySubscribe) room.publish(localStream, options);

            subscribeToStreams(roomEvent.streams);
            //console.log("xxxxxxxxxxxxxxxxxxxxxxxxx");
            //console.log(roomEvent);
            // console.log(roomEvent.streams);


        });

        room.addEventListener('stream-subscribed', function (streamEvent) {

            var stream = streamEvent.stream;
            chatListener(stream);
            //(_name, _id, _type, _streamId, _stream)
            findNode(stream.getAttributes().id, function (user, err) {
                console.log(user);
                if (user) {

                } else {
                    if (stream.getAttributes().type == "screen") { } else {
                        pushData_ToList(stream.getAttributes().name, stream.getAttributes().id, stream.getAttributes().type, stream.getID(), stream);
                        makeHTML_List(stream.getAttributes().id, stream.getAttributes().name);
                        populateDropDown(stream.getAttributes().name, stream.getAttributes().id);
                    }
                }
            });

            streamList.push(stream);


            // console.log(stream.getAttributes());
            var div = document.createElement('div');
            div.setAttribute('style', 'width: 180px; height: 120px;');
            div.setAttribute('class', 'sb-vdo-dv');
            div.setAttribute('id', stream.getID());

            var parentDiv = document.getElementById('participants');

            parentDiv.appendChild(div);
            stream.show(stream.getID());

            console.log("************************");
            //  if (localStream.getID() !== stream.getID()) {


        });

        room.addEventListener('stream-added', function (streamEvent) {

            var streams = [];
            streams.push(streamEvent.stream);
            subscribeToStreams(streams);

            // document.getElementById('recordButton').disabled = false;
        });

        room.addEventListener('stream-removed', function (streamEvent) {
            /*
                    getUserList(_roomId, function(response, error){
                     var data = JSON.parse(response);
                      for(var i in data){
                        if(data[i].name == ){
            
                        }
                        console.log(data[i].name);
                      }
                      console.log("data List here");
                      console.log(data);
                    });
            */
            console.log('SSSSSSSSSSSSSStream removed');
            // Remove stream from DOM
            var stream = streamEvent.stream;
            if (stream.elementID !== undefined) {
                //var parentDiv = document.getElementById('participants');
                //alert(stream.elementID);

                // removeNodeList(stream.elementID)
                var element = document.getElementById(stream.elementID);
                //alert(element);
                element.remove();
            }
        });

        room.addEventListener('stream-failed', function () {
            console.log('Stream Failed, act accordingly');
        });

        room.connect();

        localStream.show('local');

    });
    localStream.init();
    //});
}
function initScreenShare() {
    var configScreen = {
        data: true,
        screen: true,
        videoSize: [1280, 720, 1280, 720],
        videoFrameRate: [10, 20],
        attributes: { name: "teacher", type: 'screen' }
    };
    // If we want screen sharing we have to put our Chrome extension id.videoFrameRate: [30, 30]
    // The default one only works in our Lynckia test servers.
    // If we are not using chrome, the creation of the stream will fail regardless.
    if (configScreen.screen) {

        configScreen.extensionId = 'jdolbnggdminejdlbdgneggjpmgefbha';
        // nmkelgcaokmaenedanppappbhndeknfn
    }
    screenStream = Erizo.Stream(configScreen);

    screenStream.addEventListener('access-accepted', function () {
        //console.log("halalalalalalalalalllllllllllllllll");
        room.publish(screenStream);

    });
    screenStream.init();

}


