/* globals Erizo */
'use strict';
var serverUrl = '/';
var localStream, room, recording, recordingId;
var streamList = [];


function getParameterByName(name) {
    name = name.replace(/[\[]/, '\\\[').replace(/[\]]/, '\\\]');
    var regex = new RegExp('[\\?&]' + name + '=([^&#]*)'),
        results = regex.exec(location.search);
    return results == null ? '' : decodeURIComponent(results[1].replace(/\+/g, ' '));
}

function testConnection() {  // jshint ignore:line
    window.location = '/connection_test.html';
}

function startRecording() {  // jshint ignore:line
    if (room !== undefined) {
        if (!recording) {
            room.startRecording(localStream, function (id) {
                recording = true;
                recordingId = id;
            });

        } else {
            room.stopRecording(recordingId);
            recording = false;
        }
    }
}

var slideShowMode = false;

function toggleSlideShowMode() {  // jshint ignore:line
    var streams = room.remoteStreams;
    var cb = function (evt) {
        console.log('SlideShowMode changed', evt);
    };
    slideShowMode = !slideShowMode;
    for (var index in streams) {
        var stream = streams[index];
        if (localStream.getID() !== stream.getID()) {
            console.log('Updating config');
            stream.updateConfiguration({ slideShowMode: slideShowMode }, cb);
        }
    }
}

window.onload = function () {
    recording = false;
    var screen = getParameterByName('screen');
    var roomName = getParameterByName('room') || 'basicExampleRoom';
    console.log('Selected Room', room);

    var createToken = function (userName, role, roomName, callback) {

        var req = new XMLHttpRequest();
        var url = "https://media.rudraa.org/" + 'createToken/';
        var body = { username: userName, role: role, room: roomName };

        req.onreadystatechange = function () {
            if (req.readyState === 4) {
                callback(req.responseText);
            }
        };

        req.open('POST', url, true);
        req.setRequestHeader('Content-Type', 'application/json');
        req.send(JSON.stringify(body));
        /*
              $.ajax({
                   traditional: true,
                       type: "POST",
                       url: "https://10.90.1.167:3004/createToken/",
                        data: body ,
                       
                       //data: "test_id=" + id  ,
                     
                    
                  }).done(function( response ) {
                   
                  console.log("********************************************************");
                    console.log(response);
                  });
        */
    };


};
//initLicode("58e6140b4a581c07979d6cae",token, data[1], data[0]);
function initLicode(_room, token, _name, _id) {
    var userId = _id;

    var config = {
        video: true,
        data: true,
        //screen: screen,
        videoSize: [320, 240, 640, 480],
        videoFrameRate: [10, 20],
        //  attributes: {name:'EHsans'},
        attributes: { name: _name, id: _id, type: 'student' }
    };
    // If we want screen sharing we have to put our Chrome extension id.videoFrameRate: [30, 30]
    // The default one only works in our Lynckia test servers.
    // If we are not using chrome, the creation of the stream will fail regardless.
    if (screen) {
        config.extensionId = 'aleejaedhnhjkilacapjbkdfjbkohfdf';
    }
    localStream = Erizo.Stream(config);

    console.log(token);
    room = Erizo.Room({ token: token });

    localStream.addEventListener('access-accepted', function () {
        var subscribeToStreams = function (streams) {
            var cb = function (evt) {
                alert(evt.bandwidth);
                console.log('Bandwidth Alert', evt.msg, evt.bandwidth);
            };
            for (var index in streams) {
                var stream = streams[index];
                if (localStream.getID() !== stream.getID()) {
                    room.subscribe(stream, { slideShowMode: false, metadata: { type: 'subscriber' } });
                    stream.addEventListener('bandwidth-alert', cb);
                }
            }
        };

        room.addEventListener('room-connected', function (roomEvent) {
            var options = { metadata: { type: 'publisher' } };
            var enableSimulcast = getParameterByName('simulcast');
            if (enableSimulcast) options._simulcast = { numSpatialLayers: 2 };

            var onlySubscribe = getParameterByName('onlySubscribe');
            if (!onlySubscribe) room.publish(localStream, options);
            subscribeToStreams(roomEvent.streams);
        });

        room.addEventListener('stream-subscribed', function (streamEvent) {

            var parentDiv = document.getElementById('parentDiv');

            var stream = streamEvent.stream;

            chatListener(stream);
            streamList.push(stream);
            // console.log("localStreamlocalStreamlocalStreamlocalStreamlocalStream");
            //console.log(localStream);
            /*
                          stream.addEventListener("stream-data", function(evt){
                        console.log('Received data ', evt.msg, 'from stream ', evt.stream.getAttributes().name);
                         console.log(evt.msg);
                         if(evt.msg.trigger == 0){
                           room.unpublish(localStream);
                         }else if(evt.msg.trigger == 1){
                      room.publish(localStream);
                         }else if(evt.msg.trigger == 2){
                      localStream.muteAudio(true, function (result) {
                        if (result === 'error') {
                          console.log("There was an error unmuting the steram")
                        }});
            
                        for(var i in streamList){
                          console.log("lllllllllllllllllllll muting")
                           streamList[i].muteAudio(true, function (result) {
                          if (result === 'error') {
                            console.log("There was an error muting the steram")
                          }
                        });
                        }
                    }else if(evt.msg.trigger == 3){
                localStream.muteAudio(false, function (result) {
                if (result === 'error') {
                  console.log("There was an error unmuting the steram")
                }});
                   console.log("5555555555555555555 unmuting")
                  for(var i in streamList){
              streamList[i].muteAudio(false, function (result) {
                if (result === 'error') {
                  console.log("There was an error unmuting the steram")
                }
              });
              }
               }
             
            });
               */
            console.log("************************");
            console.log(stream.getAttributes());


            if (stream.getAttributes().type == "screen") {
                var div = document.createElement('div');
                var div = document.createElement('div');
                var width = $('#screenParent').width();
                var height = $('#screenParent').height();
                div.setAttribute('style', 'width:' + width + 'px; height:' + height + 'px;');
                div.setAttribute('class', 'sb-vdo-dv');
                div.setAttribute('id', 'test' + stream.getID());

                var parentDiv = document.getElementById('screenParent');

                $('#screenParent').prepend(div);
                // parentDiv.appendChild(div);
                stream.show('test' + stream.getID());
            }

            if (stream.getAttributes().type == "teacher") {
                var div = document.createElement('div');
                var width = $('#local').width();
                var height = $('#local').height();
                div.setAttribute('style', 'width: 300px; height: 210px;');
                //div.setAttribute('class', 'sb-vdo-dv');
                div.setAttribute('id', 'test_' + stream.getID());

                var parentDiv = document.getElementById('local');

                // parentDiv.appendChild(div);
                $('#local').prepend(div);
                stream.show('test_' + stream.getID());

            } else if (stream.getAttributes().type == "student") {
                var div = document.createElement('div');
                var div = document.createElement('div');
                div.setAttribute('style', 'width: 180px; height: 120px;');
                div.setAttribute('class', 'sb-vdo-dv');
                div.setAttribute('id', 'test' + stream.getID());

                var parentDiv = document.getElementById('participants');

                parentDiv.appendChild(div);
                stream.show('test' + stream.getID());
            }
        });

        room.addEventListener('stream-added', function (streamEvent) {
            var streams = [];
            streams.push(streamEvent.stream);
            subscribeToStreams(streams);
            // document.getElementById('recordButton').disabled = false;
        });

        room.addEventListener('stream-removed', function (streamEvent) {
            // Remove stream from DOM
            var stream = streamEvent.stream;

            if (stream.elementID !== undefined) {
                var element = document.getElementById(stream.elementID);
                element.remove();
            }
        });

        room.addEventListener('stream-failed', function () {
            console.log('Stream Failed, act accordingly');
        });

        room.connect();

        //localStream.show('remote');


    });
    localStream.init();
}
