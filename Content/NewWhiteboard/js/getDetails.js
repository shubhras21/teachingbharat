var userID = null,
name = null,
role = null,
status = null
token = null,


$(document).ready(function () {

    var readyStateCheckInterval = setInterval(function () {
        console.log("document.readyState :" + document.readyState);
        if (document.readyState === "complete") {
            clearInterval(readyStateCheckInterval);
            // Put the object into storage
            window.localStorage.setItem('UserData', '1,ehsan,student');
            console.log(userID+ "UserData")
            var data = window.localStorage.getItem("UserData"),
                 data = data.split(","),
                 userID = data[0],
                 name = data[1];

            webServiceGetToken("58e6140b4a581c07979d6cae", data[0], "presenter", function (token, err) {
                if (!err) {
                    console.log("Token :" + token + " Error : " + err);

                    initLicode("58e6140b4a581c07979d6cae", token, data[1], data[0]);
                }

            });

            //requestSession();
        }
    }, 100);

});




function requestSession() {
    console.log("Web Service url to get the Session Details :" + Config.request.GET.getUserDetail);

    $.ajax({
        traditional: true,
        dataType: "json",
        type: "GET",
        url: Config.request.GET.getUserDetail,
    }).done(function (response) {
        console.log(response);
        if (!response) {

            setTimeout(function () { requestSession(); }, 2000);

        } else {
            setValues(response);
        }

    });
}

function setValues(response) {
    //{"$id":"1","userID":2,"name":"Adeel Khan","role":"Teacher","status":true}
    userID = response.userID;
    name = response.name;
    status = response.status;
    role = response.role;


    webServiceGetToken("58e6140b4a581c07979d6cae", name, "presenter", function (token, err) {
        if (!err) {
            console.log("Token :" + token);

            initLicode(token, name, userID);
        }

    });

}

function webServiceGetToken(_roomId, _userName, _role, callback) {
    console.log(Config.request.POST.getToken);
    console.log("room=" + _roomId + "&username=107"+ "&role=" + _role);
    $.ajax({
        traditional: true,
        type: "POST",
        url: Config.request.POST.getToken,
        data: "room=" + _roomId + "&username=107"+ "&role=" + _role,
    }).done(function (_token) {
        //var obj = JSON.parse(respone);

        token = _token;
        if (_token) {
            if (typeof callback == 'function') {
                callback(_token, null);
            }
        } else {
            if (typeof callback == 'function') {
                callback(null, token);
            }
        }

    });

}

function getUserList(_roomId, callback) {
    console.log("************************************" + _roomId);


    $.ajax({
        traditional: true,
        type: "POST",
        url: Config.request.POST.getUsers,
        data: "room=" + _roomId,
    }).done(function (response) {
        //var obj = JSON.parse(respone);

        if (response) {
            if (typeof callback == 'function') {
                callback(response, null);
            }
        } else {
            if (typeof callback == 'function') {
                callback(null, response);
            }
        }

    });

}





