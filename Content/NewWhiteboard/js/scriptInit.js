var cameraImg = '/Content/NewWhiteboard/images/video-camera.png';
var noneImg = '/Content/NewWhiteboard/images/none.png';
var speakerImg = '/Content/NewWhiteboard/images/volume.png';
var microphone = '/Content/NewWhiteboard/images/voice.png';
var micImg = '/Content/NewWhiteboard/images/mick.png';
var noneImg = '/Content/NewWhiteboard/images/none.png';
var editImg = '/Content/NewWhiteboard/images/edit01.png';
var listUserObjects = [];

var signalType = {
    chat: '1',
    muteAudio: "2",
    muteVideo: "3",
    unmuteAudio: "4",
    unmuteVideo: "5"
}

function pushData_ToList(_name, _id, _type, _streamId, _stream) {
    var singleObj = {}

    singleObj['name'] = _name;
    singleObj['id'] = _id;
    singleObj['type'] = _type;
    singleObj['streamId'] = _streamId;
    singleObj['stream'] = _stream;

    listUserObjects.push(singleObj);
}

function removeNodeList(_streamId) {
    for (var obj in listUserObjects) {
        if (listUserObjects[obj].streamId == _streamId) {

            //delete listUserObjects[ obj ];
            listUserObjects.splice(obj, 1);
            break;
        }

    }
}

function findNode(_id, callback) {
    console.log("cccccccccccccccccccccccccccccccccccccc");

    for (var obj in listUserObjects) {

        if (listUserObjects[obj].id == _id) {

            if (typeof callback == 'function') {
                callback(listUserObjects[obj], null);
            }
            return;
        }

    }

    if (typeof callback == 'function') {
        callback(null, null);
    }
}

function makeHTML_List(_id, _name) {


    /********/
    var li = document.createElement('li');
    li.setAttribute('id', "li_" + _id);
    /********/
    var span = document.createElement('span');

    var name = document.createTextNode(_name);
    span.appendChild(name);
    /********/
    var div2 = document.createElement('div');
    div2.setAttribute('class', 'video');

    var img1 = document.createElement('img');


    img1.setAttribute('class', 'image');

    img1.src = editImg;


    img1.setAttribute('id', 'AllowStudent_' + _id);
    img1.style.color = "white";
    img1.onclick = AllowAccess;

    var blockImage = document.createElement('img');
    blockImage.setAttribute('class', 'image');

    blockImage.src = "/Content/NewWhiteboard/images/none.png";
    blockImage.setAttribute('id', 'BlockStudent_' + _id);
    blockImage.style.color = "white";
    blockImage.style.display = "none";

    blockImage.onclick = BlockAccess;


    var img2 = document.createElement('img');
    img2.setAttribute('class', 'none');
    img2.src = noneImg;

    div2.appendChild(img1);
    div2.appendChild(img2);
    div2.appendChild(blockImage);

    /********/
    var div3 = document.createElement('div');
    div3.setAttribute('class', 'video');

    var img3 = document.createElement('img');
    img3.setAttribute('class', 'image');
    img3.setAttribute('id', "mic_" + _id);
    img3.src = micImg;
    img3.onclick = micBtn;

    div3.appendChild(img3);
    div3.appendChild(img2);
    /********/
    var div4 = document.createElement('div');
    div4.setAttribute('class', 'video');

    var img4 = document.createElement('img');
    img4.setAttribute('class', 'image');
    img4.src = cameraImg;
    img4.setAttribute('id', "vid_" + _id);
    img4.onclick = videoBtn;
    div4.appendChild(img4);
    div4.appendChild(img2);
    /********/
    li.appendChild(span);
    li.appendChild(div2);
    li.appendChild(div3);
    li.appendChild(div4);
    /********/


    //div.setAttribute('style', 'width: 180px; height: 120px;');
    //div.setAttribute('class', 'sb-vdo-dv');
    //div.setAttribute('id',  stream.getID());

    var ul = document.getElementById('attendeeUl');
    ul.appendChild(li);
}

function videoBtn() {
    var streamArray = this.id.split("_"),
        id = streamArray[1];
    toggleVideoMsg(this, id);
}

function micBtn() {

    var streamArray = this.id.split("_"),
    streamId = streamArray[1];

    alert(streamArray[1]);
}
var tem = 1;
var temAudio = 1;

function toggleButton(_elem, _imgSrc, _id) {
    _elem.setAttribute('src', _imgSrc);
    _elem.setAttribute('id', _id);
}

function toggleVideoMsg(_elem, _id) {
    console.log(_id);
    if (_elem.getAttribute("id") == "muteVid_" + _id) {
        //unmute here
        var signal = makeSignal(signalType.unmuteVideo, _id, "unmute video to this id : " + _id);
        localStream.sendData({ trigger: signal }, function (result) { console.log(result); });
        toggleButton(_elem, cameraImg, "vid_" + _id);

    } else {
        //mute here
        var signal = makeSignal(signalType.muteVideo, _id, "unmute video to this id : " + _id);
        localStream.sendData({ trigger: signal }, function (result) { console.log(result); });
        toggleButton(_elem, noneImg, "muteVid_" + _id);

    }
}

function toggleAudioMsg(_streamId) {


    if (temAudio % 2 == 0) {

        localStream.sendData({ trigger: 3 });
        tem++;
    } else {

        localStream.sendData({ trigger: 2 });
        tem = true;
        tem++;
    }
}



function makeSignal(type, userName, msgString) {
    return type + "," + userName + "," + msgString;
}

function sendChat() {
    var chatString = document.getElementById('inputChat').value;

    var p = document.createElement('p');
    p.setAttribute('class', 'rightp');
    p.innerHTML = chatString;

    var mainDiv = document.getElementById('chatHistory');
    mainDiv.appendChild(p);

    var msg = makeSignal(signalType.chat, "Teacher Name", chatString);

    localStream.sendData({ trigger: msg }, function (result) { console.log(result); });
}

function recieveChat(_name, _chatString) {
    var p = document.createElement('p');
    p.setAttribute('class', 'leftp');
    p.innerHTML = _chatString;

    var span = document.createElement('span');
    span.innerHTML = "( " + _name + " )";
    p.appendChild(span);



    var mainDiv = document.getElementById('chatHistory');

    mainDiv.appendChild(p);
}

function chatListener(_stream) {
    _stream.addEventListener("stream-data", function (evt) {
        console.log('Received data ', evt.msg, 'from stream ', evt.stream.getAttributes().name);

        var arrMsg = evt.msg.trigger.split(","),
        type = arrMsg[0],
        name = arrMsg[1],
        msg = arrMsg[2];

        if (type == signalType.chat) {
            recieveChat(evt.stream.getAttributes().name, msg);
        } else if (type == signalType.muteAudio) {

        } else if (type == signalType.muteVideo) {

        }


    });
}

function shareScreenBtn() {
    initScreenShare();
}


function stopVideo(_elem) {

    var streamArray = _elem.id.split("_"),
    id = streamArray[1];
    if (_elem.getAttribute("id") == "unMuteLocalVid_" + id) {
        //unmute here

        toggleButton(_elem, cameraImg, "muteLocalVid_" + id);
        room.publish(localStream);
        localStream.show('local');
    } else if (_elem.getAttribute("id") == "muteLocalVid_" + id) {
        toggleButton(_elem, noneImg, "unMuteLocalVid_" + id);
        room.unpublish(localStream);
        localStream.stop();
    }


}


function muteRemoteAudio(_elem) {

    if (_elem.getAttribute("id") == "unMuteRemoteAudio_") {
        //unmute here
        localStream.muteAudio(false, function (result) {
            if (result === 'error') {
                console.log("There was an error muting the steram")
            }

        });
        toggleButton(_elem, speakerImg, "muteRemoteAudio_");


    } else if (_elem.getAttribute("id") == "muteRemoteAudio_") {

        localStream.muteAudio(true, function (result) {
            if (result === 'error') {
                console.log("There was an error muting the steram")
            }
        });


        toggleButton(_elem, noneImg, "unMuteRemoteAudio_");

    }
}

function muteMicrophone(_elem) {


    if (_elem.getAttribute("id") == "unMuteSelf_") {
        this.localStream.stream.getAudioTracks()[0].enabled = true;
        toggleButton(_elem, microphone, "muteSelf_");
        // localStream.audio = true;
    }
    else if (_elem.getAttribute("id") == "muteSelf_") {

        toggleButton(_elem, noneImg, "unMuteSelf_");
        this.localStream.stream.getAudioTracks()[0].enabled = false;
        //localStream.audio = false;
    }
}
function populateDropDown(_name, _id) {
    // var studentList = document.getElementById("studentList");
    //  var opt = document.createElement("option");
    //                   opt.value = _id;
    //                   opt.text = _name;
    //                   studentList.appendChild(opt);
}
