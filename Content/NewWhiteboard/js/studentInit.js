var signalType = {
    chat: '1',
    muteAudio: "2",
    muteVideo: "3",
    unmuteAudio: "4",
    unmuteVideo: "5"
}

function sendChat() {
    var chatString = document.getElementById('inputChat').value;

    var p = document.createElement('p');
    p.setAttribute('class', 'rightp');
    p.innerHTML = chatString;


    var mainDiv = document.getElementById('chatHistory');
    mainDiv.appendChild(p);

    var msg = makeSignal(signalType.chat, "Student Name", chatString);

    localStream.sendData({ trigger: msg }, function (result) { console.log("************"); console.log(result); });

}


function makeSignal(type, userName, msgString) {
    return type + "," + userName + "," + msgString;
}


function recieveChat(_name, _chatString) {
    var p = document.createElement('p');
    p.setAttribute('class', 'leftp');
    p.innerHTML = _chatString;

    var span = document.createElement('span');
    span.innerHTML = "( " + _name + " )";
    p.appendChild(span);

    var mainDiv = document.getElementById('chatHistory');

    mainDiv.appendChild(p);
}

function chatListener(_stream) {
    _stream.addEventListener("stream-data", function (evt) {
        console.log('Received data ', evt.msg, 'from stream ', evt.stream.getAttributes().name);

        var arrMsg = evt.msg.trigger.split(","),
        type = arrMsg[0],
        name = arrMsg[1],
        msg = arrMsg[2];

        switch (type) {
            case signalType.chat:
                recieveChat(evt.stream.getAttributes().name, msg);
                break;
            case signalType.muteVideo:
                if (name == localStream.getAttributes().id) { room.unpublish(localStream); }
                break;
            case signalType.unmuteVideo:
                if (name == localStream.getAttributes().id) { room.publish(localStream); }
                break;
        }

    });
}



function muteSelfAudio() {
    localStream.muteAudio(false, function (result) {
        if (result === 'error') {
            console.log("There was an error unmuting the steram")
        }
    });
}

function un_muteSelfAudio() {
    localStream.muteAudio(true, function (result) {
        if (result === 'error') {
            console.log("There was an error muting the steram")
        }
    });
}