var webApp_url = 'https://10.90.1.162';
var licode_server = 'https://media.rudraa.org:3004';
//var images = webApp_url+'assets/images/';

var Config = {
	 request : {
		GET : {
			getUserDetail : "https://demo.rudraa.org/api/Services/CurrentUserInfo"
		},
		POST : {
		    getToken: licode_server + "/createToken/",
			getUsers : licode_server+"/getUsers"
			//getTestInfoProctor : siteURL+"services/webrtc/get_test_info"
		}
	 }
}