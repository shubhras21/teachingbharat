function includeStudent() {
    var connectionCount = 0;
    var user = 0;
    // Initialize an OpenTok Session object
    var session = OT.initSession(sessionId);

    // Initialize a Publisher, and place it into the element with id="publisher"
    var publisher;

    // Attach event handlers
    session.on({

        // This function runs when session.connect() asynchronously completes
        sessionConnected: function (event) {
            user++;

        },

        // This function runs when another client publishes a stream (eg. session.publish())
        streamCreated: function (event) {
            console.log("Stream Object Name : " + event.stream.name);
            console.log("Stream Object Id : " + event.stream.streamId);

            // Create a container for a new Subscriber, assign it an id using the streamId, put it inside
            // the element with id="subscribers"

            var subOptionsPeople = { insertMode: 'append', width: '100%', height: 180 };
            var subOptionsScreens = { insertMode: 'append', width: 1044, height: 750 };

            if (event.stream.videoType === 'screen') {
                session.subscribe(event.stream, 'screens', subOptionsScreens);
            } else {
                session.subscribe(event.stream, 'people', subOptionsPeople);
            }

        },
        connectionCreated: function (event) {
            connectionCount++;
            console.log('Publisher :' + connectionCount + ' connections.');
        },
        connectionDestroyed: function (event) {
            connectionCount--;
            console.log('Publisher :' + connectionCount + ' connections.');
        },
        sessionDisconnected: function sessionDisconnectHandler(event) {
            // The event is defined by the SessionDisconnectEvent class
            console.log('Disconnected from the session.');
            document.getElementById('disconnectBtn').style.display = 'none';
            if (event.reason == 'networkDisconnected') {
                alert('Your network connection terminated.')
            }
        }

    });

    // Connect to the Session using the 'apiKey' of the application and a 'token' for permission
    session.connect(apiKey, token, function (error) {
        if (error) {
            if (error.name === "OT_NOT_CONNECTED") {
                alert("You are not connected to the internet. Check your network connection.");
            }
            console.log("Failed to connect: ", error.message);
        } else {
            console.log("Session Connected");
        }
    });

    // Listen for exceptions
    OT.on("exception", function (event) {
        console.log("OT exception : " + event.message);
    });
}
