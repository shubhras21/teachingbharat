function includeTeacher() {


    var connectionCount = 0;
    var user = 0;

    var session = OT.initSession(sessionId);
    // Connect to the Session using the 'apiKey' of the application and a 'token' for permission
    session.connect(apiKey, token, function (error) {
        if (error) {
            if (error.name === "OT_NOT_CONNECTED") {
                alert("You are not connected to the internet. Check your network connection.");
            }
            console.log("Failed to connect: ", error.message);
        } else {
            console.log("Session Connected");
        }
    });

    // Initialize a Publisher, and place it into the element with id="publisher"
    var publisher;

    // Attach event handlers
    session.on({

        // This function runs when session.connect() asynchronously completes
        sessionConnected: function (event) {
            user++;
            //publisher = OT.initPublisher(apiKey, 'publisher',{name: "Video Learner "+user,width: 360, height:160,audioSource: null});
            publisher = OT.initPublisher(apiKey, 'people', { insertMode: 'append', name: "Video Learner " + user, width: '100%', height: 160 }, function (error) {
                if (error) {
                    console.log(error);
                } else {
                    console.log("Publisher initialized.");
                }
            });


            console.log("Session Id : " + sessionId);
            // Publish the publisher we initialzed earlier (this will trigger 'streamCreated' on other
            // clients)

            session.publish(publisher);
            screenshare();
        },
        // This function runs when another client publishes a stream (eg. session.publish())
        streamCreated: function (event) {
            console.log("Stream Object Name : " + event.stream.name);
            console.log("Stream Object Id : " + event.stream.streamId);
        },

        connectionCreated: function (event) {

            connectionCount++;
            console.log('Learner : ' + connectionCount + ' connections.');
        },
        connectionDestroyed: function (event) {
            connectionCount--;
            console.log('Learner : ' + connectionCount + ' connections.');
        },
        sessionDisconnected: function sessionDisconnectHandler(event) {
            // The event is defined by the SessionDisconnectEvent class
            // console.log('Disconnected from the session.');
            // document.getElementById('disconnectBtn').style.display = 'none';
            if (event.reason == 'networkDisconnected') {
                alert('Your network connection terminated.')
            }
        }

    });
    // Screen Share
    function screenshare() {
        // For Google Chrome only, register your extension by ID. You can
        // find it at chrome://extensions once the extension is installed.
        //
        // The last parameter assumes you are using the latest version (version 2)
        // of the OpenTok Chrome extension source code.
        OT.registerScreenSharingExtension('chrome', 'ffjoomghpnfedjfpokjagffogeipmgce', 2);

        OT.checkScreenSharingCapability(function (response) {

            if (!response.supported || response.extensionRegistered === false) {
                alert('This browser does not support screen sharing.');
            } else if (response.extensionInstalled === false) {
                alert('Please install the screen sharing extension and load your app over https.');
            } else {
                // Screen sharing is available. Publish the screen.
                var screenSharingPublisher = OT.initPublisher({ insertDefaultUI: false, audioSource: null, videoSource: 'screen', name: "Screen Learner " + user, insertMode: 'append' });
                session.publish(screenSharingPublisher, function (error) {
                    if (error) {
                        alert('Could not share the screen: ' + error.message);
                    }
                });
                //screenSharingPublisher.element.style.visibility = 'hidden';
            }
        });
    }

    // Listen for exceptions
    OT.on("exception", function (event) {
        console.log("OT exception : " + event.message);
    });

}
