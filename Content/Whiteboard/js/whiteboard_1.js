//// Author : Alvin George, KPMG


//var DrawState =
//{
//    Started: 0,
//    Inprogress: 1,
//    Completed: 2
//}
//var DrawTool =
//{
//    Pencil: 0,
//    Line: 1,
//    Text: 2,
//    Rect: 3,
//    Oval: 4,
//    Erase: 5,
//    Circle: 6,
//    Image: 7,
//    Undo: 8,
//    Page:9,
//}
//var whiteboardHub;
//var tool_default = 'pencil';
//var canvas, context, canvaso, contexto;

//var tools = {};
//var tool;
//var WIDTH;
//var HEIGHT;
//var INTERVAL = 20;

//var mx, my;
//var URL = window.URL || window.URL;


//var selectedLineWidth ;
//var defaultLineWidth;


//var currentColor;
//var defaultColor;

//var isImage;

//// current image on
//var vls;

//var store=0;
//var gh=0;

//var drawObjectsCollection = [];
//var drawPlaybackCollection = [];

//// Variables for Maintain History
//var cPushArray = new Array();
//var cStep = -1;
//var ctxs;


//// Variables for New and old Canvases
//var oPushArray = new Array();
//var oStep = -1;
//var maintainIndex = undefined;
//var count=2;


//// Drag and Drop Variables
//var x = 75;
//var y = 50;
//var dragok = false;


//function DrawIt(drawObject, syncServer)
//{
//   // var context = document.getElementById('whiteBoard').getContext("2d");
//  //  alert("IN draw fuct " + drawObject.Tool);

//    if (drawObject.Tool == DrawTool.Line) {

//        switch (drawObject.currentState) {
//            case DrawState.Inprogress:
//            case DrawState.Completed:
//                context.clearRect(0, 0, canvas.width, canvas.height);

//                // Set pencil thickness
//                context.lineWidth = selectedThickness();

//                // Reset line Color
//                context.strokeStyle = selectedColor();

//                context.beginPath();
//                context.moveTo(drawObject.StartX, drawObject.StartY);
//                context.lineTo(drawObject.CurrentX, drawObject.CurrentY);
//                context.stroke();
//                context.closePath();

//                // Reset pencil thickness
//                context.lineWidth = defaultLineWidth;

//                // Set line Color
//                context.strokeStyle = selectedColor();

//                if (drawObject.currentState == DrawState.Completed) {

//                    updatecanvas();

//                     var oldIndex = getOStep();

//                    if (typeof oPushArray[oldIndex] == 'undefined')
//                    {
//                        // does not exist
//                        cPush();
//                          OldPush(); 
//                    }
//                    else {
//                        // does exist
//                        cPush();
//                        UpdatePush(oldIndex);
//                    }

//                }
//                break;
//        }
//    }
//    else if (drawObject.Tool == DrawTool.Pencil) {

//        switch (drawObject.currentState) {
//            case DrawState.Started:


//                // Set pencil thickness
//                context.lineWidth = selectedThickness();

//                // Reset line Color
//                context.strokeStyle = defaultColor

//                context.beginPath();
//                context.moveTo(drawObject.StartX, drawObject.StartY);

//                // Reset pencil thickness
//                context.lineWidth = defaultLineWidth;

//                // Set line Color
//                context.strokeStyle = selectedColor();

//                break;
//            case DrawState.Inprogress:
//            case DrawState.Completed:

//                // Set pencil thickness
//                context.lineWidth = selectedThickness();

//                // Reset line Color
//                context.strokeStyle = defaultColor;

//                context.lineTo(drawObject.CurrentX, drawObject.CurrentY);
//                context.stroke();

//                // Reset pencil thickness
//                context.lineWidth = defaultLineWidth;

//                // Set line Color
//                context.strokeStyle = selectedColor();

//                if (drawObject.currentState == DrawState.Completed) {

//                   //alert("Completed Called");
//                    updatecanvas();
//                  //  cPush();
//                   // var oldIndex = getOStep();
//                   // UpdatePush(oldIndex);

//                    var oldIndex = getOStep();

//                    if (typeof oPushArray[oldIndex] == 'undefined')
//                    {
//                        // does not exist    
//                               cPush();
//                               OldPush(); 
//                    }
//                    else {
//                        // does exist
//                        cPush();
//                        UpdatePush(oldIndex);
//                    }
//                   // updatecanvas();
//                }
//                break;
//        }
//    }
//    else if (drawObject.Tool == DrawTool.Text) {
//        switch (drawObject.currentState) {
//            case DrawState.Started:
//              //  context.clearRect(0, 0, canvas.width, canvas.height);
//               // clear(context);
//                // context.save();
//                var size = selectedColor();
//              // var th= selectedThickness();
//              // th = th * 6;
//             //  var s = th + "px";
//              //  context.font = 'normal 25px Calibri';
//                //   context.fillStyle = "blue";
//               // context.font = 'normal';
//               // context.font = '25px';
//                // context.font = 'Calibri';

//             //  alert(s);
//                if (isNaN(size))
//                {
//                    context.fillStyle = size;
//                    context.font = "normal 20px Calibri";

//                }
//                else
//                {
//                  //  context.font = s;
//                    //context.font = context.font.replace(/\d+px/, (parseInt(context.font.match(/\d+px/)) + size) + "px");
//                  //  alert("In else Condition : " +size);
//                }


//                // Reset Line Color
//                //context.strokeStyle = selectedColor();

//                context.textAlign = "left";
//                context.textBaseline = "top";
//                context.fillText(drawObject.Text, drawObject.StartX, drawObject.StartY);

//                // Set Line Color
//                context.strokeStyle = defaultColor;

//                updatecanvas();
//                context.restore();
//              //  cPush();

//                var oldIndex = getOStep();

//                if (typeof oPushArray[oldIndex] == 'undefined') {
//                    // does not exist
//                    //   context.restore();
//                    cPush();
//                      OldPush(); 
//                }
//                else {
//                    // does exist
//                    cPush();
//                    UpdatePush(oldIndex);
//                }

//                break;

//        }

//    }
//    else if (drawObject.Tool == DrawTool.Erase) {


//        switch (drawObject.currentState) {

//            case DrawState.Started:
//              //  var wd;
//                // Set pencil thickness
//              //  wd = selectedThickness();

//                context.fillStyle = "#FFFFFF";
//                context.fillRect(drawObject.StartX, drawObject.StartY, 40, 10);
//                context.restore();
//                updatecanvas();

//                // Default pencil thickness
//                // wd = defaultLineWidth;

//                //context.clearRect(drawObject.StartX, drawObject.StartY, 5, 5);
//                break;
//            case DrawState.Inprogress:
//            case DrawState.Completed:

//                //var wd;
//                //// Set pencil thickness
//                //wd = selectedThickness();

//                context.fillStyle = "#FFFFFF";
//                context.fillRect(drawObject.CurrentX, drawObject.CurrentY, 40, 10);
//                context.restore();
//               // updatecanvas();

//                // Default pencil thickness
//                   //     wd = defaultLineWidth;
//                if (drawObject.currentState == DrawState.Completed) {
//                    updatecanvas();
//                   // cPush();
//                     var oldIndex = getOStep();

//                    if (typeof oPushArray[oldIndex] == 'undefined')
//                    {
//                        // does not exist
//                        cPush();
//                          OldPush(); 
//                    }
//                    else {
//                        // does exist
//                        cPush();
//                        UpdatePush(oldIndex);
//                    }


//                    //context.restore();
//                }

//                // context.clearRect(drawObject.CurrentX, drawObject.CurrentY, 5, 5);
//                break;
//        }
//    }
//    else if (drawObject.Tool == DrawTool.Rect) {

//        switch (drawObject.currentState) {
//            case DrawState.Inprogress:
//            case DrawState.Completed:

//                // Set pencil thickness
//                context.lineWidth = selectedThickness();

//                // Reset line Color
//                context.strokeStyle = defaultColor;

//                var x = Math.min(drawObject.CurrentX, drawObject.StartX),
//                        y = Math.min(drawObject.CurrentY, drawObject.StartY),
//                        w = Math.abs(drawObject.CurrentX - drawObject.StartX),
//                        h = Math.abs(drawObject.CurrentY - drawObject.StartY);


//               context.clearRect(0, 0, canvas.width, canvas.height);


//                if (!w || !h) {
//                    return;
//                }

//                context.strokeRect(x, y, w, h);
//                context.globalCompositeOperation = 'destination-atop';

//                // Default pencil thickness
//                context.lineWidth = defaultLineWidth;

//                // Set line Color
//                context.strokeStyle = selectedColor();


//                if (drawObject.currentState == DrawState.Completed) {
//                    updatecanvas();
//                    cPush();
//                    var oldIndex = getOStep();

//                    if (typeof oPushArray[oldIndex] == 'undefined') {
//                        // does not exist
//                          OldPush(); 
//                    }
//                    else {
//                        // does exist
//                        UpdatePush(oldIndex);
//                    }

//                    //context.restore();
//                }
//                break;
//        }

//    }

//    else if (drawObject.Tool == DrawTool.Circle) {

//        switch (drawObject.currentState) {

//            case DrawState.Inprogress:
//            case DrawState.Completed:

//                // Set pencil thickness
//                context.lineWidth = selectedThickness();

//                // Reset line Color
//                context.strokeStyle = defaultColor;


//                var x = Math.min(drawObject.CurrentX, drawObject.StartX),
//                        y = Math.min(drawObject.CurrentY, drawObject.StartY),
//                        w = Math.abs(drawObject.CurrentX - drawObject.StartX),
//                        h = Math.abs(drawObject.CurrentY - drawObject.StartY);

//                context.clearRect(0, 0, canvas.width, canvas.height);

//                if (!w || !h) {
//                    return;
//                }



//                var radius = Math.sqrt(Math.pow((drawObject.StartX - drawObject.CurrentX), 2) + Math.pow((drawObject.StartY - drawObject.CurrentY), 2));
//                context.beginPath();
//                context.arc(drawObject.StartX, drawObject.StartY, radius, 0, 2 * Math.PI, false);
//                context.stroke();

//                // Default pencil thickness
//                context.lineWidth = defaultLineWidth;

//                // Set line Color
//                context.strokeStyle = selectedColor();


//                if (drawObject.currentState == DrawState.Completed) {
//                    updatecanvas();
//                  //  cPush();
//                    var oldIndex = getOStep();

//                    if (typeof oPushArray[oldIndex] == 'undefined') {
//                        // does not exist
//                        cPush();
//                          OldPush(); 
//                    }
//                    else {
//                        // does exist
//                        cPush();
//                        UpdatePush(oldIndex);
//                    }
//                }
//                break;
//        }

//    }
//    else if (drawObject.Tool == DrawTool.Image) {
//        switch (drawObject.currentState) {
//            case DrawState.Started:
//                // context.clearRect(0, 0, canvas.width, canvas.height);
//                context.clearRect(0, 0, canvas.width, canvas.height);
//                var a = GetImageName();           //"url(/GridImages/"+imgName+")";     //"~/GridImages/" + imgName; "url(/GridImages/G1.png)" 
//               if(a!=='none')
//               {
//                var bg = "url(" + a + ")";
//                $("#imageTempt").css("background-image", bg);
//               }
//                else
//               {
//                   $("#imageTempt").css("background-image", 'none');
//               }


//                break;



//            case DrawState.Completed:


//                break;

//        }

//    }

//    //else if (drawObject.Tool == DrawTool.Page) {
//    //    switch (drawObject.currentState) {
//    //        case DrawState.Started:

//    //            updatecanvas();
//    //            cPush();
//    //            var oldIndex = getOStep();

//    //            if (typeof oPushArray[oldIndex] == 'undefined') {
//    //                // does not exist
//    //            //    alert("In not exists");
//    //                OldPush();
//    //            }
//    //            else {
//    //                // does exist
//    //                alert("In exists");
//    //                UpdatePush(oldIndex);
//    //            }

//    //            break;


//    //    }

//    //}

//    if (syncServer && drawObject.Tool != DrawTool.Pencil) {
//      //  alert("In Server Sync");
//        drawObjectsCollection = [];      
//        drawObject.currentColor = selectedColor();
//        drawObject.selectedLineWidth = selectedThickness();
//        drawObject.isImage = GetImageName();
//       // alert("img name in sync : " + GetImageName());
//        drawObjectsCollection.push(drawObject);
//       // var message = JSON.stringify(drawObjectsCollection);
//      //  whiteboardHub.server.sendDraw(message, $("#sessinId").val(), $("#groupName").val(), $("#userName").val());
//    }


//}

//// Functions for Redo and Undo
//var mycnas;
//var ctxz;
//var savedData
//function cPush() {
//    cStep++;
//    if (cStep < cPushArray.length) { cPushArray.length = cStep; }

//    mycnas = document.getElementById('whiteBoard');
//    ctxz = canvas.getContext("2d");
//    cPushArray.push(mycnas.toDataURL("image/png"));
//    document.title = cStep + ":" + cPushArray.length;
//}

//function cUndo() {

//    if (cStep > 0) {
//        cStep--;
//        var img = new Image();
//        img.crossOrigin = 'Anonymous';    
//        img.onload = function () 
//        {
//            context.drawImage(img,0,0);
//            Clear();
//            updatecanvas();                   
//        }
//        img.src = cPushArray[cStep];
//     }

//}

//function cRedo() {
//    if (cStep < cPushArray.length -1) {
//        cStep++;
//        var canvasPic = new Image();
//        canvasPic.crossOrigin = 'Anonymous';
//        canvasPic.onload = function ()
//        {

//            context.drawImage(canvasPic, 0, 0);
//            Clear();
//            updatecanvas();      
//        }
//        canvasPic.src = cPushArray[cStep];
//    }
//}


//function toggleBG1() {

//    setTimeout(function () { $('#divShare').css("background-color", "silver"); toggleBG2() }, 800);
//}
//function toggleBG2() {
//    setTimeout(function () { $('#divShare').css("background-color", "#C8C8C8"); toggleBG1() }, 800);

//}
//function DrawObject() {
//}
//function UpdatePlayback(drawObject) {
//    if (drawPlaybackCollection.length > 1000) {
//        drawPlaybackCollection = [];
//        alert("Playback cache is cleared due to more than 1000 items");
//    }
//    drawPlaybackCollection.push(drawObject);
//}
//function Clear() {
//    canvaso.hieght = canvas.hieght;
//    canvaso.width = canvas.width;
//}
//function Playback() {
//    if (drawPlaybackCollection.length == 0) {
//        alert("No drawing to play"); return;
//    }
//    canvaso.hieght = canvas.hieght;
//    canvaso.width = canvas.width;


//    for (var i = 0; i < drawPlaybackCollection.length; i++) {
//        var drawObject = drawPlaybackCollection[i];
//        setTimeout(function () { DrawIt(drawObject, false, false); }, 3000);
//    }
//    drawPlaybackCollection = [];
//}
//$(document).ready(function () {

//    //JoinHub();
//    //$("#userName").val("");
//    //$("#dialog-form").dialog({ autoOpen: false, width: 350, modal: true, closeOnEscape: false });
//    //$("#dialog-form").dialog("open");
//    //$("#name").keypress(function (e) {
//    //    if (e && e.keyCode == 13) {
//    //        $("#btnJoin").click();
//    //    }
//    //});

//    try {

//        canvaso = document.getElementById('whiteBoard');
//        if (!canvaso) {
//            alert('Error: Cannot find the imageView canvas element!');
//            return;
//        }

//        if (!canvaso.getContext) {
//            alert('Error: no canvas.getContext!');
//            return;
//        }
//        //  setOStep(oStep+1);

//        canvaso.width = 830;
//        canvaso.height = 400;

//        // Get the 2D canvas context.
//        contexto = canvaso.getContext('2d');
//        ctxs = canvaso.getContext("2d");
//        if (!contexto) {
//            alert('Error: failed to getContext!');
//            return;
//        }

//        // Add the temporary canvas.
//        var container = canvaso.parentNode;


//        canvas = document.createElement('canvas');
//        if (!canvas) {
//            alert('Error: Cannot create a new canvas element!');
//            return;
//        }

//        canvas.id = 'imageTemp';
//        canvas.width = canvaso.width;
//        canvas.height = canvaso.height;
//        container.appendChild(canvas);

//        context = canvas.getContext('2d');

//        // Activate the default tool.
//        SelectTool(tool_default);

//        // Attach the mousedown, mousemove and mouseup event listeners.
//        canvas.addEventListener('mousedown', ev_canvas, false);
//        canvas.addEventListener('mousemove', ev_canvas, false);
//        canvas.addEventListener('mouseup', ev_canvas, false);
//        canvas.addEventListener('mouseout', ev_canvas, false);

//        // Custom Drag Events
//        //canvas.onmousedown = myDown;
//        //canvas.onmouseup = myUp;
//        context.clearRect(0, 0, canvas.width, canvas.height);
//        toggleBG1();

//        // Initialize canvas count
//        setOStep(0);
//        //OldPush();
//        //updatecanvas();

//        // Send draw
//        var drawObject = new DrawObject();
//        drawObject.Tool = DrawTool.Pencil;

//        tool.started = true;
//        drawObject.currentState = DrawState.Completed;
//        DrawIt(drawObject, true);
//        tool.started = false;
//    }
//    catch (err) {
//        alert(err.message);
//    }

//});

//function clear(c) {
//    c.clearRect(0, 0, WIDTH, HEIGHT);
//}

//function ev_canvas(ev) {
//    var iebody = (document.compatMode && document.compatMode != "BackCompat") ? document.documentElement : document.body

//    var dsocleft = document.all ? iebody.scrollLeft : pageXOffset
//    var dsoctop = document.all ? iebody.scrollTop : pageYOffset
//    var appname = window.navigator.appName;
//    try {
//        if (ev.layerX || ev.layerX == 0) {  // Firefox
//            ev._x = ev.layerX;
//            if ('Netscape' == appname)
//                ev._y = ev.layerY;
//            else
//                ev._y = ev.layerY - dsoctop;

//        } else if (ev.offsetX || ev.offsetX == 0) { // Opera
//            ev._x = ev.offsetX;
//            ev._y = ev.offsetY - dsoctop;
//        }

//        // Call the event handler of the tool.
//        var func = tool[ev.type];

//        if (func) {
//            func(ev);
//        }
//    }
//    catch (err) {
//        alert(err.message);
//    }
//}

//function getMouse(e) {
//    var element = canvaso, offsetX = 0, offsetY = 0;

//    if (element.offsetParent) {
//        do {
//            offsetX += element.offsetLeft;
//            offsetY += element.offsetTop;
//        } while ((element = element.offsetParent));
//    }

//    // Add padding and border style widths to offset
//    offsetX += stylePaddingLeft;
//    offsetY += stylePaddingTop;

//    offsetX += styleBorderLeft;
//    offsetY += styleBorderTop;

//    mx = e.pageX - offsetX;
//    my = e.pageY - offsetY;

//    mx = e._x;
//    my = e._y;
//}

//function updatecanvas()
//{
//    contexto.drawImage(canvas, 0, 0);
//    context.clearRect(0, 0, canvas.width, canvas.height);
//}

//tools.pencil = function () {
//    var tool = this;
//    this.started = false;
//    drawObjectsCollection = [];
//    this.mousedown = function (ev) {
//        var drawObject = new DrawObject();
//        drawObject.Tool = DrawTool.Pencil;
//        tool.started = true;
//        drawObject.currentState = DrawState.Started;
//        drawObject.StartX = ev._x;
//        drawObject.StartY = ev._y;
//        DrawIt(drawObject, true);
//        drawObjectsCollection.push(drawObject);
//    };

//    this.mousemove = function (ev) {
//        if (tool.started) {
//       //     context.lineWidth = selectedLineWidth;
//            var drawObject = new DrawObject();
//            drawObject.Tool = DrawTool.Pencil;
//            drawObject.currentState = DrawState.Inprogress;
//            drawObject.CurrentX = ev._x;
//            drawObject.CurrentY = ev._y;
//            DrawIt(drawObject, true);
//            drawObjectsCollection.push(drawObject);
//        }
//    };

//    // This is called when you release the mouse button.
//    this.mouseup = function (ev) {
//        if (tool.started) {

//            var drawObject = new DrawObject();
//            drawObject.Tool = DrawTool.Pencil;
//            tool.started = false;
//            drawObject.currentState = DrawState.Completed;
//            drawObject.CurrentX = ev._x;
//            drawObject.CurrentY = ev._y;
//            DrawIt(drawObject, true);
//            drawObjectsCollection.push(drawObject);
//            var message = JSON.stringify(drawObjectsCollection);
//           // whiteboardHub.server.sendDraw(message, $("#sessinId").val(), $("#groupName").val(), $("#userName").val());

//        }
//    };
//    this.mouseout = function (ev) {
//        if (tool.started) {
//            var message = JSON.stringify(drawObjectsCollection);
//           // whiteboardHub.server.sendDraw(message, $("#sessinId").val(), $("#groupName").val(), $("#userName").val());
//        }
//        tool.started = false;
//        //context.lineWidth = 3;

//    }
//};

//tools.rect = function () {
//    var tool = this;
//    var drawObject = new DrawObject();
//    drawObject.Tool = DrawTool.Rect;
//    this.started = false;

//    this.mousedown = function (ev) {
//        drawObject.currentState = DrawState.Started;
//        drawObject.StartX = ev._x;
//        drawObject.StartY = ev._y;
//        tool.started = true;   

//    };

//    this.mousemove = function (ev) {
//        if (!tool.started) {
//            return;
//        }
//        drawObject.currentState = DrawState.Inprogress;
//        drawObject.CurrentX = ev._x;
//        drawObject.CurrentY = ev._y;
//        DrawIt(drawObject, true);




//    };

//    this.mouseup = function (ev) {
//        if (tool.started) {
//            drawObject.currentState = DrawState.Completed;
//            drawObject.CurrentX = ev._x;
//            drawObject.CurrentY = ev._y;
//            DrawIt(drawObject, true);
//            tool.started = false;

//        }

//    };
//};


//tools.circle = function () {
//    var tool = this;
//    var drawObject = new DrawObject();
//    drawObject.Tool = DrawTool.Circle;
//    this.started = false;

//    this.mousedown = function (ev) {
//        drawObject.currentState = DrawState.Started;
//        drawObject.StartX = ev._x;
//        drawObject.StartY = ev._y;
//        tool.started = true;

//    };

//    this.mousemove = function (ev) {
//        if (!tool.started) {
//            return;
//        }
//        drawObject.currentState = DrawState.Inprogress;
//        drawObject.CurrentX = ev._x;
//        drawObject.CurrentY = ev._y;  
//        DrawIt(drawObject, true);
//    };

//    this.mouseup = function (ev) {
//        if (tool.started) {
//            drawObject.currentState = DrawState.Completed;
//            drawObject.CurrentX = ev._x;
//            drawObject.CurrentY = ev._y;   
//            DrawIt(drawObject, true);
//            tool.started = false;

//        }
//    };
//};

//tools.line = function () {
//    var tool = this;
//    var drawObject = new DrawObject();
//    drawObject.Tool = DrawTool.Line;
//    this.started = false;

//    this.mousedown = function (ev) {
//        drawObject.currentState = DrawState.Started;
//        drawObject.StartX = ev._x;
//        drawObject.StartY = ev._y;
//        tool.started = true;
//    };

//    this.mousemove = function (ev) {
//        if (!tool.started) {
//            return;
//        }
//        drawObject.currentState = DrawState.Inprogress;
//        drawObject.CurrentX = ev._x;
//        drawObject.CurrentY = ev._y;
//        DrawIt(drawObject, true);
//    };

//    this.mouseup = function (ev) {
//        if (tool.started) {
//            drawObject.currentState = DrawState.Completed;
//            drawObject.CurrentX = ev._x;
//            drawObject.CurrentY = ev._y;
//            DrawIt(drawObject, true);
//            tool.started = false;
//        }
//    };
//};

//tools.text = function () {
//    var tool = this;
//    this.started = false;
//    var drawObject = new DrawObject();
//    drawObject.Tool = DrawTool.Text;
//    this.mousedown = function (ev) {

//        if (!tool.started) {
//            tool.started = true;
//            drawObject.currentState = DrawState.Started;
//            drawObject.StartX = ev._x;
//            drawObject.StartY = ev._y;
//            var text_to_add = prompt('Enter the text:', ' ', 'Add Text');
//            drawObject.Text = "";
//            drawObject.Text = text_to_add;
//            if (text_to_add.length < 1) {
//                tool.started = false;
//                return;
//            }

//            DrawIt(drawObject, true);
//            tool.started = false;
//            updatecanvas();
//        }
//    };

//    this.mousemove = function (ev) {
//        if (!tool.started) {
//            return;
//        }
//    };

//    this.mouseup = function (ev) {
//        if (tool.started) {
//            tool.mousemove(ev);
//            tool.started = false;
//            updatecanvas();
//        }
//    };
//}

//tools.erase = function (ev) {


//    var tool = this;
//    this.started = false;
//    var drawObject = new DrawObject();
//    drawObject.Tool = DrawTool.Erase;
//    this.mousedown = function (ev) {
//        tool.started = true;
//        drawObject.currentState = DrawState.Started;
//        drawObject.StartX = ev._x;
//        drawObject.StartY = ev._y;
//        DrawIt(drawObject, true);
//    };
//    this.mousemove = function (ev) {
//        if (!tool.started) {
//            return;
//        }
//        drawObject.currentState = DrawState.Inprogress;
//        drawObject.CurrentX = ev._x;
//        drawObject.CurrentY = ev._y;
//        DrawIt(drawObject, true);
//    };
//    this.mouseup = function (ev) {
//        drawObject.currentState = DrawState.Completed;
//        drawObject.CurrentX = ev._x;
//        drawObject.CurrentY = ev._y;
//        DrawIt(drawObject, true);
//        tool.started = false;
//    }
//}

//function fireEvent(element, event) {
//    var evt;
//    if (document.createEventObject) {
//        // dispatch for IE
//        evt = document.createEventObject();
//        return element.fireEvent('on' + event, evt)
//    }
//    else {
//        // dispatch for firefox + others
//        evt = document.createEvent("HTMLEvents");
//        evt.initEvent(event, true, true); // event type,bubbling,cancelable
//        return !element.dispatchEvent(evt);
//    }
//}


//function SelectTool(toolName) {
//    if (tools[toolName]) {
//        tool = new tools[toolName]();

//    }


//    if (toolName == "line" || toolName == "curve" || toolName == "measure" || toolName == "rect" || toolName == "circle")
//        canvaso.style.cursor = "crosshair";
//    else if (toolName == "select")
//        canvaso.style.cursor = "default";
//    else if (toolName == "text")
//        canvaso.style.cursor = "text";

//    ChangeIcons(toolName);

//}
//var currentActiveId;
//function ChangeIcons(toolName) {

//    if (toolName == "line")
//    {
//        $("." + currentActiveId + "-icon").removeClass(currentActiveId + "-active");
//        $(".shape-icon").addClass("shape-active"); currentActiveId = 'shape';
//    }
//    else if (toolName == "pencil")
//    {
//        $("."+currentActiveId + "-icon").removeClass(currentActiveId+"-active");
//        $(".pencil-icon").addClass("pencil-active"); currentActiveId = 'pencil';
//    }
//    else if (toolName == "rect")
//    {
//        $("." + currentActiveId + "-icon").removeClass(currentActiveId + "-active");
//        $(".shape-icon").addClass("shape-active"); currentActiveId = 'shape';
//    }
//    else if (toolName == "circle") {
//        $("." + currentActiveId + "-icon").removeClass(currentActiveId + "-active");
//        $(".shape-icon").addClass("shape-active"); currentActiveId = 'shape';
//    }

//    else if (toolName == "erase")
//    {
//        $("." + currentActiveId + "-icon").removeClass(currentActiveId + "-active");
//        $(".erasor-icon").addClass("erasor-active"); currentActiveId = 'erasor';
//    }
//    else if (toolName == "text")
//    {
//        $("." + currentActiveId + "-icon").removeClass(currentActiveId + "-active");
//        $(".text-icon").addClass("text-active"); currentActiveId = 'text';
//    }
//    else if ((toolName == "G1.png") || (toolName == "G2.png") || (toolName == "G3.png"))
//    {
//        $("." + currentActiveId + "-icon").removeClass(currentActiveId + "-active");
//        $(".grid-icon").addClass("grid-active"); currentActiveId = 'grid';
//    }

//}

//function ChangeGrids(imgName)
//{

//    var drawObject = new DrawObject();
//    drawObject.Tool = DrawTool.Image;

//        tool.started = true;
//        drawObject.currentState = DrawState.Started;
//        drawObject.StartX = 0;
//        drawObject.StartY = 0;
//        var a = "/GridImages/" + imgName;
//      //  alert("a from change grid : " + a);
//        SetImageName(a);
//        DrawIt(drawObject, true);
//        tool.started = false;


//}


//var drawobjects = [];


////function JoinHub() {


////    $("#btnJoin").click(function () {

////        var name = $("#name").val();
////        var name = $.trim(name);

////        if (name.length > 0) {
////            $("#userName").val(name);



////            whiteboardHub = $.connection.whiteboardHub;
////            whiteboardHub.client.handleDraw = function (message, sessnId, name) {
////                var sessId = $('#sessinId').val();
////                if (sessId != sessnId) {
////                    $("#divStatus").html("");

////                    $("#divStatus").html("<i>" + name + " drawing...</i>")
////                    var drawObjectCollection = jQuery.parseJSON(message)
////                    for (var i = 0; i < drawObjectCollection.length; i++) {

////                        ControlColor(drawObjectCollection[i].currentColor);
////                        ControlThickness(drawObjectCollection[i].selectedLineWidth);
////                        SetImageName(drawObjectCollection[i].isImage);
////                      //  isImage = drawObjectCollection[i].selectedLineWidth
////                   //     alert("Is Image from Hub : " + GetImageName());
////                        DrawIt(drawObjectCollection[i], false);
////                        if (drawObjectCollection[i].currentState) {
////                            if (drawObjectCollection[i].currentState == DrawState.Completed)
////                            {

////                                $("#divStatus").html("<i>" + name + " drawing completing...</i>")
////                                $("#divStatus").html("");
////                            }
////                        }
////                    }
////                }

////            };
////            whiteboardHub.client.chatJoined = function (name) {
////                $("#divMessage").append("<span><i> <b>" + name + " joined. <br/></b></i></span>");
////                $("#dialog-form").dialog("close");
////            }
////            whiteboardHub.client.chat = function (name, message) {
////                $("#divMessage").append("<span>" + name + ": " + message + "</span><br/>");
////                var objDiv = document.getElementById("divMessage");
////                objDiv.scrollTop = objDiv.scrollHeight;
////            };
////            var sendMessage = function () {
////                whiteboardHub.sendChat($(".chat-message").val(), $("#groupName").val(), $("#userName").val());
////            };


////            $.connection.hub.start().done(function () {
////                whiteboardHub.server.joinGroup($("#groupName").val()).done(function () { whiteboardHub.server.joinChat($("#userName").val(), $("#groupName").val()); });
////            });

////            $("#btnSend").click(
////            function () {
////                var message = $("#txtMessage").val();
////                var message = $.trim(message);
////                if (message.length > 0) {
////                    whiteboardHub.server.sendChat(message, $("#groupName").val(), $("#userName").val());
////                    $("#txtMessage").val("");
////                }
////            }
////            );

////        }
////    });
////}

//// Drag Event
//function myMove(e) {
//   // alert("In out Mouse Move" + dragok);
//    if (dragok) {
//        x = e.pageX - canvas.offsetLeft;
//        y = e.pageY - canvas.offsetTop;
//    }
//}

//function myDown(e) {

//    if (e.pageX < x + 15 + canvas.offsetLeft && e.pageX > x - 15 +
//    canvas.offsetLeft && e.pageY < y + 15 + canvas.offsetTop &&
//    e.pageY > y - 15 + canvas.offsetTop) {

//        x = e.pageX - canvas.offsetLeft;
//        y = e.pageY - canvas.offsetTop;
//        dragok = true;
//       canvas.onmousemove = myMove;
//    }
//    dragok = true;
//}

//function myUp() {

//    dragok = false;
//    canvas.onmousemove = null;
//}


////function getAbsolutePosition(e) {
////    var curleft = curtop = 0;
////    if (e.offsetParent) {
////        curleft = e.offsetLeft;
////        curtop = e.offsetTop;
////        while (e = e.offsetParent) {
////            curleft += e.offsetLeft;
////            curtop += e.offsetTop;
////        }
////    }
////    return [curleft, curtop];
////}

//function SaveDrawings() {

//    var canvas1 = document.getElementById("imageTempt");
//    var context1 = canvas1.getContext("2d");

//    var imgs = canvas1.toDataURL("image/png");
//    var img = canvaso.toDataURL("image/png");


//    WindowObject = window.open('', "PrintPaintBrush", "toolbars=no,scrollbars=yes,status=no,resizable=no");
//    WindowObject.document.open();
//    WindowObject.document.writeln('<img src="' + imgs + '"/>');
//    WindowObject.document.close();
//    WindowObject.focus();

//}


//function LoadImageIntoCanvas(bgImageUrl) {

//   var image_View = document.getElementById("imageTemp");
//   var context = canvas.getContext("2d");

//    var img = new Image();
//    img.onload = function () {
//     //   image_View.width = img.width;
//     //   image_View.height = img.height;
//    //    WIDTH = img.width;
//    //    HEIGHT = img.height;
//  //      context.clearRect(0, 0, img.width, img.height);
//        // context.drawImage(img,0,0);
//        context.globalCompositeOperation="destination-over";
//         context.drawImage(img, 30, 5, 400, 400);
//        context.globalCompositeOperation="destination-over";
//            Clear(); 
//            updatecanvas();

//    }
//     context.globalCompositeOperation="destination-over";
//   // context.clearRect(0, 0, canvas.width, canvas.height);
//    img.src = bgImageUrl;
//  //  drawObject.currentState = DrawState.Completed;
////    updatecanvas();
//    cPush();
//    // Activate the default tool.
//    SelectTool('pencil');

//}

//function UpdateCanvass() {
//    var file_UploadImg = document.getElementById("fileUploadImg");
//    LoadImageIntoCanvas(URL.createObjectURL(file_UploadImg.files[0]));
//}
//document.getElementById('fileUploadImg').onchange = function () {

//    UpdateCanvass();
//};

//// Control Thickness of Pencils

//function ControlThickness(value)
//{
//    selectedLineWidth = value;
//}
//function selectedThickness()
//{
//    return selectedLineWidth;
//}

//function ControlColor(value)
//{
//    currentColor = value;
//}
//function selectedColor() {
//    return currentColor;
//}
//function SetImageName(value)
//{
//    if (value == 'none')
//    {
//        isImage = 'none';
//    }
//    else
//    isImage=value;
//}
//function GetImageName() {
//    return isImage;
//}


//function AddCanvas()
//{

//    try {

//        cPushArray=[];

//      //  setOStep(oStep);

//      //  setOStep('undefined'); 
//        gh=oStep+1;
//        setOStep(gh);


//      // alert("current o step : " +oStep);

//        canvaso = document.getElementById('whiteBoard');

//        if (!canvaso) {
//            alert('Error: Cannot find the imageView canvas element!');
//            return;
//        }

//        if (!canvaso.getContext) {
//            alert('Error: no canvas.getContext!');
//            return;
//        }
//        canvaso.width = 830;
//        canvaso.height = 400;

//        // Get the 2D canvas context.
//        contexto = canvaso.getContext('2d');
//        ctxs = canvaso.getContext("2d");
//        if (!contexto) {
//            alert('Error: failed to getContext!');
//            return;
//        }

//        // Add the temporary canvas.
//        var container = canvaso.parentNode;
//        canvas = document.createElement('canvas');
//        if (!canvas) {
//            alert('Error: Cannot create a new canvas element!');
//            return;
//        }

//        canvas.id = 'imageTemp';
//        canvas.width = canvaso.width;
//        canvas.height = canvaso.height;
//        container.appendChild(canvas);

//        context = canvas.getContext('2d');

//        // Activate the default tool.
//        SelectTool(tool_default);

//        // Attach the mousedown, mousemove and mouseup event listeners.
//        canvas.addEventListener('mousedown', ev_canvas, false);
//        canvas.addEventListener('mousemove', ev_canvas, false);
//        canvas.addEventListener('mouseup', ev_canvas, false);
//        canvas.addEventListener('mouseout', ev_canvas, false);

//        context.clearRect(0, 0, canvas.width, canvas.height);
//        Clear();

//    }
//    catch (err) {
//        alert(err.message);
//    }

//  //  OldPush();
//    //  updatecanvas();


//    // Send draw
//    var drawObject = new DrawObject();
//    drawObject.Tool = DrawTool.Pencil;

//    tool.started = true;
//    drawObject.currentState = DrawState.Completed;
//    DrawIt(drawObject, true);
//    tool.started = false;

//    // Dynamic generate a HTML Element and assign IDs to onclick
//    var a = document.createElement('a'); 
//    var linkText = document.createTextNode(count);
//    count = count + 1;
//    a.appendChild(linkText);
//    a.title = oStep+1;
//    a.href = "#";
//    a.style.fontSize = "30px";
//    a.style.color = "white";
//    a.style.background = "grey";
//    a.style.margin = "0.2%";
//    a.id = "New_" + oStep;
//    //  document.body.appendChild(a);
//    $("#liref").append(a);
//    var concat = "New_" + oStep;
//    var element = document.getElementById(concat);
//    var value = oStep;
//    element.setAttribute("onclick", "OldUndo(" + value + ")");

//}



//// Functions for Redo and Undo
//var OldCanvas;
//var Oldctxz;
//var OldsavedData;

//function OldPush() {

//   alert("In Old Push : ");
//    oStep++;
//    if (oStep < oPushArray.length) { oPushArray.length = oStep; }

//    OldCanvas = document.getElementById('whiteBoard');
//    Oldctxz = canvas.getContext("2d");
//    oPushArray.push(OldCanvas.toDataURL("image/png"));
//}

//function UpdatePush(value)
//{
//    alert("In update Push : " + value)
//   var OldCanvass = document.getElementById('whiteBoard');
//   var Oldctxzs = OldCanvass.getContext("2d");
//    var az = OldCanvas.toDataURL("image/png");
//    oPushArray[value] = az;
//}

//function OldUndo(id) {

//    cPushArray=[];

//    var cns = document.getElementById('whiteBoard');
//    var isExist = cns.toDataURL("image/png");
//    if (id <= oPushArray.length )
//    {
//        var img = new Image();
//        img.crossOrigin = 'Anonymous';  
//        img.onload = function () {
//            context.drawImage(img, 0, 0);
//            Clear();
//            updatecanvas();
//        }
//        img.src = oPushArray[id];
//        SelectTool('pencil');     
//    }
//    updatecanvas();
//    setOStep(id);
//}

//function OldRedo() {
//    if (oStep < oPushArray.length - 1) {
//        oStep++;
//        var canvasPic = new Image();
//        canvasPic.crossOrigin = 'Anonymous';
//        canvasPic.onload = function () {
//        context.drawImage(canvasPic, 0, 0);
//        Clear();
//        updatecanvas();
//        }
//        canvasPic.src = oPushArray[oStep];

//    }
//}


//function setOStep(value)
//{
//     maintainIndex=value;
//}
//function getOStep() {
//    return maintainIndex;
//}

//function setCurrentIndex(value)
//{
//    vls=value;
//}
//function getCurrentIndex()
//{
//    return vls;
//}