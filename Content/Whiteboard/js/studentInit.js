var apiKey = null;
var sessionId = null;
var token = null;
$(document).ready(function () {
    console.log("Web Service url to get the Session Details :" + Application.Config.request.GET.url);
    requestSession();

});

function requestSession() {
    $.ajax({
        traditional: true,
        type: "GET",
        url: Application.Config.request.GET.url,


    }).done(function (response) {
        console.log(response);
        //var str = JSON.stringify(resp.Data);
        var data = JSON.parse(response);
        apiKey = data.apiKey;
        sessionId = data.sessionId;
        token = data.token;
        includeStudent();
    });
}
