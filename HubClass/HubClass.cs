﻿using System.Threading.Tasks;
using Microsoft.AspNet.SignalR;
using Microsoft.AspNet.SignalR.Hubs;
using System;
using BLL.Helpers;
using System.Linq;
using System.Collections.Generic;
using System.Web;

namespace WhiteBoardApplication.HubClass
{
    [HubName("whiteboardHub")]
    public class HubClass : Hub
    {
        public static Int64 UserID {get;set;}
        public void JoinGroup(string groupName, Int64 CurrentUser)
        {
            var myInfo = Context.QueryString["myInfo"];       
            Groups.Add(Context.ConnectionId, groupName);         
        }
        public void JoinChat(string name, string groupName)
        {
            Clients.Group(groupName).ChatJoined(name);
        }

        public void SendDraw(string drawObject, string groupName)
        {
             Clients.OthersInGroup(groupName).HandleMyDraw(drawObject);
        }
        public void sendUndoDraw(string a)
        {
              Clients.Others.sendUndoDraw(a);
        }
        

        public void undoRedo(int status,string gName,string src)
        {
            // 1 For Undo 2 For Redo
            Clients.OthersInGroup(gName).PerformUndoRedo(status,src);

        }

        public void SendChat(string message, string groupName, string name)
        {
            Clients.Group(groupName).Chat(name, message);
        }

        public override Task OnConnected()
        {
            var myInfo = Context.QueryString["myInfo"];

            //Int64 UserID = GetUserID();
            //string ConnectionID = Context.ConnectionId;
            // UserHandler.ConnectedIds.Add(Context.ConnectionId);

            return base.OnConnected();
        }
        public override Task OnReconnected()
        {
            //Int64 UserID = GetUserID();
            return base.OnReconnected();
        }

        public override Task OnDisconnected(bool stopCalled)
        {
          
          //  Int64 UserID = GetUserID();
          //  string ConnectionID = Context.ConnectionId;
          //  UserHandler.ConnectedIds.Remove(Context.ConnectionId);
            return base.OnDisconnected(stopCalled);
        }

        public void JoinHub(string id)
        {
            Clients.Group("Whiteboard").JoinWhite();          
        }

        public void AddUserToGroup(string GroupName)
        {
            Groups.Add(Context.ConnectionId, GroupName);
        }

        public void AddNewGroup(string link,Int64 UserID)
        {
            Groups.Add(Context.ConnectionId, link);
        }
        public void AddUserInGroup()
        {
           
            //Groups.Add(Context.ConnectionId, roomName);
        }

        public void SendUsername(string name)
        {
           // string a = name;
          //  Clients.Group("Whiteboard").SendName(a);
        }

        public void SetUserID(Int64 id)
        {
            if (id != 0 && id > 0)
                UserID = id;
            else
                UserID = 0;
        }

        public Int64 GetUserID()
        {
            return UserID;
        }

        public void AllowAccess(string gName,Int64 RequestedBy)
        {
            Clients.OthersInGroup(gName).AllowAccess(RequestedBy);
        }

        public void BlockAccess(string gName, Int64 RequestedBy)
        {
            Clients.OthersInGroup(gName).BlockAccess(RequestedBy);
        }

        public void RequestFromStudent(Int64 RequestedBy, string gName, string name)
        {
            Clients.OthersInGroup(gName).SendRequest(RequestedBy, name);
        }

        public void AddCanvas(string gName)
        {
            Clients.OthersInGroup(gName).AddCanvas();
        }

        public void SendUndoRedoAlert(string gName,int id)
        {
            Clients.OthersInGroup(gName).SendUndoRedoAlert(id);
        }
        public void LoadImage(string gName, object src)
        {
            Clients.OthersInGroup(gName).LoadImageIntoCanvas(src);
        }
        public void AlertClear(string gName)
        {
            Clients.OthersInGroup(gName).AlertClear();
        }

        public void PauseSession(string gName)
        {
            Clients.OthersInGroup(gName).PauseSession();
        }

        public void ResumeSession(string gName)
        {
            Clients.OthersInGroup(gName).ResumeSession();
        }

        public void EndSession(string gName)
        {
            Clients.OthersInGroup(gName).EndSession();
        }


        public void Ping(string gName)
        {

        }

    }













    public class ConnectionMapping<T>
    {
        private readonly Dictionary<T, HashSet<string>> _connections =
            new Dictionary<T, HashSet<string>>();

        public int Count
        {
            get
            {
                return _connections.Count;
            }
        }

        public void Add(T key, string connectionId)
        {
            lock (_connections)
            {
                HashSet<string> connections;
                if (!_connections.TryGetValue(key, out connections))
                {
                    connections = new HashSet<string>();
                    _connections.Add(key, connections);
                }

                lock (connections)
                {
                    connections.Add(connectionId);
                }
            }
        }

        public IEnumerable<string> GetConnections(T key)
        {
            HashSet<string> connections;
            if (_connections.TryGetValue(key, out connections))
            {
                return connections;
            }

            return Enumerable.Empty<string>();
        }

        public void Remove(T key, string connectionId)
        {
            lock (_connections)
            {
                HashSet<string> connections;
                if (!_connections.TryGetValue(key, out connections))
                {
                    return;
                }

                lock (connections)
                {
                    connections.Remove(connectionId);

                    if (connections.Count == 0)
                    {
                        _connections.Remove(key);
                    }
                }
            }

        }
     


    }
    public static class UserHandler
    {
        public static HashSet<string> ConnectedIds = new HashSet<string>();
    }
}