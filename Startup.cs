﻿using Microsoft.AspNet.SignalR;
using Microsoft.Owin;
using Owin;
using System;

[assembly: OwinStartup(typeof(Tutoring.Startup))]
namespace Tutoring
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            app.MapSignalR();
            //GlobalHost.Configuration.ConnectionTimeout = new TimeSpan(0, 60, 110);
            //GlobalHost.Configuration.DisconnectTimeout = new TimeSpan(0, 60, 120);
            //GlobalHost.Configuration.KeepAlive = new TimeSpan(0, 60, 120);


            // Make long polling connections wait a maximum of 110 seconds for a
            // response. When that time expires, trigger a timeout command and
            // make the client reconnect.
            GlobalHost.Configuration.ConnectionTimeout = TimeSpan.FromSeconds(110);
            // Wait a maximum of 30 seconds after a transport connection is lost
            // before raising the Disconnected event to terminate the SignalR connection.
            GlobalHost.Configuration.DisconnectTimeout = TimeSpan.FromSeconds(60);
            // For transports other than long polling, send a keepalive packet every
            // 10 seconds. 
            // This value must be no more than 1/3 of the DisconnectTimeout value.
            GlobalHost.Configuration.KeepAlive = TimeSpan.FromSeconds(20);
            //Setting up the message buffer size
            GlobalHost.Configuration.DefaultMessageBufferSize = 50000000;

            GlobalHost.Configuration.MaxIncomingWebSocketMessageSize = null;

        }
    }
}
