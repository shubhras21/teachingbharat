﻿using System.Web;
using System.Web.Optimization;

namespace Tutoring
{
    public class BundleConfig
    {
        // For more information on bundling, visit http://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            #region [FRONT LAYOUT Content]

            #region [Js Bundle]

            //Jquery js
            bundles.Add(new ScriptBundle("~/Content/Jquery").Include(
                     "~/Content/Front/frontjs/jquery2.1.1.js"));

            //Modernizer js
            bundles.Add(new ScriptBundle("~/Content/modernizer").Include(
                        "~/Content/Front/frontjs/modernizr.custom.97074.js"));

            //Unobtrosive js
            bundles.Add(new ScriptBundle("~/Content/unobtrusiveajax").Include(
                        "~/scripts/jquery.unobtrusive-ajax.min.js"));

            //Unobtrosive js
            bundles.Add(new ScriptBundle("~/Content/validate").Include(
                        "~/scripts/jquery.validate.js"));

            //Unobtrosive js
            bundles.Add(new ScriptBundle("~/Content/unobtrusive").Include(
                        "~/scripts/jquery.validate.unobtrusive.js"));

            //WOW js
            //bundles.Add(new ScriptBundle("~/Content/wowjs").Include(
            //         "~/Content/Front/frontjs/wow.min.js"));
            bundles.Add(new ScriptBundle("~/Content/wowjs").Include(
                    "~/Content/Include/js/wow.min.js"));

            //bootstrap and whiteboard js
            //bundles.Add(new ScriptBundle("~/Content/bootstrapjs").Include(
            //         "~/Content/Front/frontjs/bootstrap.min.js"
            //         ));
            bundles.Add(new ScriptBundle("~/Content/bootstrapjs").Include(
                    "~/Content/Include/js/bootstrap.min.js"
                    ));
            
            bundles.Add(new ScriptBundle("~/Content/TLayoutjs").Include(
                //"~/Content/Front/frontjs/jquery2.1.1.js",
                "~/Content/Include/js/bootstrap.min.js",
                "~/scripts/jquery.validate.js",
                     "~/scripts/jquery.validate.unobtrusive.js",
                 "~/scripts/jquery.unobtrusive-ajax.min.js",
                  "~/Content/DashBoard/adminjs/bootstrap-notify.js",
                      "~/Content/DashBoard/adminjs/notificationjs.js",
                      "~/Content/CustomJs/customjs.js",
                      "~/Content/Front/frontjs/bootstrap-select.min.js",
                      "~/Content/TeacherDasboard/js/fullcalendar.min.js",
                       "~/Scripts/jquery.dataTables.min.js",
                     "~/Scripts/dataTables.bootstrap.min.js"

                     ));


            #endregion

            #region [Style Sheet]

            //CSS 
            bundles.Add(new StyleBundle("~/Content/homecss").Include(
                "~/Content/Include/css/animate.css",
               "~/Content/Whiteboard/css/whitbrd.css",
                 "~/Content/Whiteboard/css/StyleWhite.css",
                   "~/Content/Whiteboard/css/custom.css"
                ));

            #endregion

            #endregion

            #region [ADMIN LAYOUT Content]

            #region [js]

            bundles.Add(new ScriptBundle("~/Content/adminjs").Include(
                     "~/Content/DashBoard/adminjs/bootstrap.min.js",
                     "~/Content/DashBoard/adminjs/bootstrap-checkbox-radio-switch.js",
                     "~/Content/DashBoard/adminjs/chartist.min.js",
                     "~/Content/DashBoard/adminjs/bootstrap-notify.js",
                     "~/Content/DashBoard/adminjs/light-bootstrap-dashboard.js",
                     "~/Content/DashBoard/adminjs/demo.js",
                     "~/Content/DashBoard/adminjs/notificationjs.js",
                     "~/Content/Whiteboard/js/whiteboard_1.js",
                     "~/Scripts/jquery.dataTables.min.js",
                     "~/Scripts/dataTables.bootstrap.min.js"

                     ));

            #endregion

            #region [css]

            //CSS 
            bundles.Add(new StyleBundle("~/Content/admincss").Include(
                   "~/Content/DashBoard/admincss/bootstrap.min.css",
                   "~/Content/DashBoard/admincss/animate.min.css",
                   "~/Content/DashBoard/admincss/light-bootstrap-dashboard.css",
                   "~/Content/UdatedCSS/dataTables.bootstrap.min.css",
                   "~/Content/DashBoard/admincss/demo.css"
                   ));

            #endregion

            #endregion



            #region [Student Layout]


            //CSS 
            //bundles.Add(new StyleBundle("~/Content/studentcss").Include(

            //   "~/Content/TeacherDasboard/css/bootstrap.css",
            //   "~/Content/UpdatedCSS/Dstyle.css",
            //   "~/Content/Include/css/animate.css"
            //   ));
            bundles.Add(new StyleBundle("~/Content/studentcss").Include(

                "~/Content/Include/css/bootstrap.css",
                "~/Content/Include/css/Dstyle.css",
                "~/Content/Include/css/animate.css"
                ));

			#endregion

			bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
			"~/Scripts/jquery-1.*"));

			bundles.Add(new ScriptBundle("~/bundles/jqueryui").Include(
						"~/Scripts/jquery-ui*"));

			bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
						"~/Scripts/jquery.unobtrusive*",
						"~/Scripts/jquery.validate*"));

			bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
						"~/Scripts/modernizr-*"));

			bundles.Add(new StyleBundle("~/Content/css").Include("~/Content/site.css"));

			bundles.Add(new StyleBundle("~/Content/themes/base/css").Include(
						"~/Content/themes/base/jquery.ui.core.css",
						"~/Content/themes/base/jquery.ui.resizable.css",
						"~/Content/themes/base/jquery.ui.selectable.css",
						"~/Content/themes/base/jquery.ui.accordion.css",
						"~/Content/themes/base/jquery.ui.autocomplete.css",
						"~/Content/themes/base/jquery.ui.button.css",
						"~/Content/themes/base/jquery.ui.dialog.css",
						"~/Content/themes/base/jquery.ui.slider.css",
						"~/Content/themes/base/jquery.ui.tabs.css",
						"~/Content/themes/base/jquery.ui.datepicker.css",
						"~/Content/themes/base/jquery.ui.progressbar.css",
						"~/Content/themes/base/jquery.ui.theme.css"));


		}


	}
}
