﻿using BLL.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace Tutoring.Filter
{
    
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Method)]
    public class CustomAdminAuthorize : AuthorizeAttribute
    {
        public string AccessLevel { get; set; }

        public string RedirectUrl = "~/Home/UnAuthorizeAccess";

        protected override bool AuthorizeCore(System.Web.HttpContextBase httpContext)
        {
            bool isAuthorized = base.AuthorizeCore(httpContext);
            var user = Config.CurrentUser;
            var response = httpContext.Response;
            var request = httpContext.Request;

            var filterContext = new AuthorizationContext();
            if (user == 0)
            {
                if (request.IsAjaxRequest())
                {
                    SessionHandler(filterContext);
                    isAuthorized = false;
                }
                else
                {
                    SessionHandler(filterContext);
                    isAuthorized = false;
                }
            }
            return isAuthorized;
        }

        public override void OnAuthorization(AuthorizationContext filterContext)
        {
            base.OnAuthorization(filterContext);
            var httpContext = filterContext.HttpContext;
            var currentUser = Config.CurrentUser;
            if(currentUser!=0)
            {
                var user = Config.User;
                if (user != null)
                {
                    var isAuthorized = true;
                    if (user.UserType != (int)Enumerations.UserType.Admin && user.UserType != (int)Enumerations.UserType.Super)
                    {
                        isAuthorized = false;
                    }
                    if (!isAuthorized)
                    {
                        RedirectUrl = "~/Home/UnAuthorizeAccess";
                        filterContext.RequestContext.HttpContext.Response.Redirect(RedirectUrl);
                    }
                }
                else
                {
                    RedirectUrl = "~/Account/Login?ReturnUrl=" + filterContext.HttpContext.Request.Path;
                    filterContext.RequestContext.HttpContext.Response.Redirect(RedirectUrl);
                }
            }
            else
            {
                RedirectUrl = "~/Account/Login?ReturnUrl="+ filterContext.HttpContext.Request.Path;
                filterContext.RequestContext.HttpContext.Response.Redirect(RedirectUrl);
            }      
        }

        public void SessionHandler(AuthorizationContext filterContext)
        {
            base.HandleUnauthorizedRequest(filterContext);
        }

    }

    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Method)]
    public class CustomTeacherAuthorize : AuthorizeAttribute
    {
        public string AccessLevel { get; set; }

        public string RedirectUrl = "~/Home/UnAuthorizeAccess";

        protected override bool AuthorizeCore(System.Web.HttpContextBase httpContext)
        {
            bool isAuthorized = base.AuthorizeCore(httpContext);
            var user = Config.CurrentUser;
            var response = httpContext.Response;
            var request = httpContext.Request;

            var filterContext = new AuthorizationContext();
            if (user == 0)
            {
                if (request.IsAjaxRequest())
                {
                    SessionHandler(filterContext);
                    isAuthorized = false;
                }
                else
                {
                    SessionHandler(filterContext);
                    isAuthorized = false;
                }
            }
            return isAuthorized;
        }

        public override void OnAuthorization(AuthorizationContext filterContext)
        {
            base.OnAuthorization(filterContext);
            var httpContext = filterContext.HttpContext;
            var currentUser = Config.CurrentUser;
            if (currentUser != 0)
            {
                var user = Config.User;
                if (user != null)
                {
                    var isAuthorized = true;
                    if (user.UserType != (int)Enumerations.UserType.Teacher)
                    {
                        isAuthorized = false;
                    }
                    if (!isAuthorized)
                    {
                        RedirectUrl = "~/Home/UnAuthorizeAccess";
                        filterContext.RequestContext.HttpContext.Response.Redirect(RedirectUrl);
                    }
                }
                else
                {
                    RedirectUrl = "~/Account/Login?ReturnUrl=" + filterContext.HttpContext.Request.Path;
                    filterContext.RequestContext.HttpContext.Response.Redirect(RedirectUrl);
                }
            }
            else
            {
                RedirectUrl = "~/Account/Login?ReturnUrl=" + filterContext.HttpContext.Request.Path;
                filterContext.RequestContext.HttpContext.Response.Redirect(RedirectUrl);
            }
        }

        public void SessionHandler(AuthorizationContext filterContext)
        {
            base.HandleUnauthorizedRequest(filterContext);
        }

    }

    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Method)]
    public class CustomStudentAuthorize : AuthorizeAttribute
    {
        public string AccessLevel { get; set; }

        public string RedirectUrl = "~/Home/UnAuthorizeAccess";

        protected override bool AuthorizeCore(System.Web.HttpContextBase httpContext)
        {
            bool isAuthorized = base.AuthorizeCore(httpContext);
            var user = Config.CurrentUser;
            var response = httpContext.Response;
            var request = httpContext.Request;

            var filterContext = new AuthorizationContext();
            if (user == 0)
            {
                if (request.IsAjaxRequest())
                {
                    SessionHandler(filterContext);
                    isAuthorized = false;
                }
                else
                {
                    SessionHandler(filterContext);
                    isAuthorized = false;
                }
            }
            return isAuthorized;
        }

        public override void OnAuthorization(AuthorizationContext filterContext)
        {
            base.OnAuthorization(filterContext);
            var httpContext = filterContext.HttpContext;
            var currentUser = Config.CurrentUser;
            if (currentUser != 0)
            {
                var user = Config.User;
                if (user != null)
                {
                    var isAuthorized = true;
                    if (user.UserType != (int)Enumerations.UserType.Student && user.UserType != (int)Enumerations.UserType.Parent)
                    {
                        isAuthorized = false;
                    }
                    if (!isAuthorized)
                    {
                        RedirectUrl = "~/Home/UnAuthorizeAccess";
                        filterContext.RequestContext.HttpContext.Response.Redirect(RedirectUrl);
                    }
                }
                else
                {
                    RedirectUrl = "~/Account/Login?ReturnUrl=" + filterContext.HttpContext.Request.Path;
                    filterContext.RequestContext.HttpContext.Response.Redirect(RedirectUrl);
                }
            }
            else
            {
                RedirectUrl = "~/Account/Login?ReturnUrl=" + filterContext.HttpContext.Request.Path;
                filterContext.RequestContext.HttpContext.Response.Redirect(RedirectUrl);
            }
        }

        public void SessionHandler(AuthorizationContext filterContext)
        {
            base.HandleUnauthorizedRequest(filterContext);
        }

    }

}