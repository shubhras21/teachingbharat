﻿using BLL.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using Tutoring.Controllers;

namespace Tutoring.Filter
{
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Method)]
    public class CustomAdminFilter: ActionFilterAttribute
    {
        string ReturnUrl = null;
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {

            base.OnActionExecuting(filterContext);
            if (Config.CurrentUser==0)
            {
                RouteValueDictionary redirectTargetDictionary = new RouteValueDictionary();
                redirectTargetDictionary.Add("action", "Login");
                redirectTargetDictionary.Add("controller", "Account");
                redirectTargetDictionary.Add("area", "");
                ReturnUrl = filterContext.HttpContext.Request.Path;
                redirectTargetDictionary.Add("ReturnUrl", ReturnUrl);
            
                filterContext.Result = new RedirectToRouteResult(redirectTargetDictionary);
            }
            //if (Config.CurrentUser != 0 && Config.User.UserType == (int)Enumerations.UserType.Super)
            //{
            //    RouteValueDictionary redirectTargetDictionary = new RouteValueDictionary();
            //    redirectTargetDictionary.Add("action", "Login");
            //    redirectTargetDictionary.Add("controller", "Account");
            //    redirectTargetDictionary.Add("area", "");
            //    ReturnUrl = filterContext.HttpContext.Request.Path;
            //    redirectTargetDictionary.Add("ReturnUrl", ReturnUrl);

            //    filterContext.Result = new RedirectToRouteResult(redirectTargetDictionary);
            //    //

            //}
            //else
            //{
            //    RouteValueDictionary redirectTargetDictionary = new RouteValueDictionary();
            //    redirectTargetDictionary.Add("action", "UnAuthorizeAccess");
            //    redirectTargetDictionary.Add("controller", "Home");
            //    redirectTargetDictionary.Add("area", "");
            //    filterContext.Result = new RedirectToRouteResult(redirectTargetDictionary);
            //    //base.OnActionExecuting(filterContext);

            //}
            //base.OnActionExecuting(filterContext);
        }

        public override void OnActionExecuted(ActionExecutedContext filterContext)
        {
            base.OnActionExecuted(filterContext);
            if (Config.CurrentUser != 0 && Config.User.UserType == (int)Enumerations.UserType.Super)
            {
                RouteValueDictionary redirectTargetDictionary = new RouteValueDictionary();
                redirectTargetDictionary.Add("action", "Login");
                redirectTargetDictionary.Add("controller", "Account");
                redirectTargetDictionary.Add("area", "");
                ReturnUrl = filterContext.HttpContext.Request.Path;
                redirectTargetDictionary.Add("ReturnUrl", ReturnUrl);

                filterContext.Result = new RedirectToRouteResult(redirectTargetDictionary);
                //

            }
            else
            {
                RouteValueDictionary redirectTargetDictionary = new RouteValueDictionary();
                redirectTargetDictionary.Add("action", "UnAuthorizeAccess");
                redirectTargetDictionary.Add("controller", "Home");
                redirectTargetDictionary.Add("area", "");
                filterContext.Result = new RedirectToRouteResult(redirectTargetDictionary);
                //base.OnActionExecuting(filterContext);

            }
        }
    }

    

}