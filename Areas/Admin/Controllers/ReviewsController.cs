﻿using BLL;
using BLL.BusinessManagers;
using BLL.Helpers;
using DAL.Entities;
using DAL.MasterEntity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Mvc;

namespace Tutoring.Areas.Admin.Controllers
{
    public class ReviewsController : Controller
    {
        MyDbContext context = new MyDbContext();
        // GET: Admin/Reviews
        public ActionResult StudentReviews()
        {
            if (Config.CurrentUser == 0)
                return RedirectToAction("Login", "Account", new { area = "" });

            if (Config.User.UserType == (int)Enumerations.UserType.Super
               || Config.User.UserType == (int)Enumerations.UserType.Admin)
            {

                var bannerslides = context.RatingReviews.OrderByDescending(p => p.RatingReviewsID).Where(p => p.CreatedBy == "Student").ToList();

                return View(bannerslides);

            }

            else
                return RedirectToAction("UnAuthorizeAccess", "Home", new { area = "" });
            
        }
        public ActionResult ChangeStatus(int? id, bool status)
        {
            if (Config.CurrentUser == 0)
                return RedirectToAction("Login", "Account", new { area = "" });

            if (Config.User.UserType == (int)Enumerations.UserType.Super
               || Config.User.UserType == (int)Enumerations.UserType.Admin)
            {

                var update = context.RatingReviews.Where(o => o.RatingReviewsID == id).FirstOrDefault();
                update.IsActive = status;
                context.SaveChanges();
                var bannerslides = context.RatingReviews.OrderByDescending(p => p.RatingReviewsID).Where(p => p.CreatedBy == "Student").ToList();

                return View(bannerslides);

            }

            else
                return RedirectToAction("UnAuthorizeAccess", "Home", new { area = "" });

            
        }
        public ActionResult ChangeApprovedStatus(int? id, bool status)
        {
            if (Config.CurrentUser == 0)
                return RedirectToAction("Login", "Account", new { area = "" });

            if (Config.User.UserType == (int)Enumerations.UserType.Super
               || Config.User.UserType == (int)Enumerations.UserType.Admin)
            {

                var update = context.RatingReviews.Where(o => o.RatingReviewsID == id).FirstOrDefault();
                update.IsApproved = status;
                context.SaveChanges();
                var bannerslides = context.RatingReviews.OrderByDescending(p => p.RatingReviewsID).Where(p => p.CreatedBy == "Student").ToList();

                return View(bannerslides);

            }

            else
                return RedirectToAction("UnAuthorizeAccess", "Home", new { area = "" });
            
        }
        public ActionResult ChangeAdminStatus(int? id, bool status)
        {
            if (Config.CurrentUser == 0)
                return RedirectToAction("Login", "Account", new { area = "" });

            if (Config.User.UserType == (int)Enumerations.UserType.Super
               || Config.User.UserType == (int)Enumerations.UserType.Admin)
            {

                var update = context.RatingReviews.Where(o => o.RatingReviewsID == id).FirstOrDefault();
                update.IsActive = status;
                context.SaveChanges();
                var result = context.RatingReviews.OrderByDescending(p => p.RatingReviewsID).ToList();
                return View(result);

            }

            else
                return RedirectToAction("UnAuthorizeAccess", "Home", new { area = "" });
            

            
        }
        public ActionResult ChangeAdminApprovedStatus(int? id, bool status)
        {
            if (Config.CurrentUser == 0)
                return RedirectToAction("Login", "Account", new { area = "" });

            if (Config.User.UserType == (int)Enumerations.UserType.Super
               || Config.User.UserType == (int)Enumerations.UserType.Admin)
            {

                var update = context.RatingReviews.Where(o => o.RatingReviewsID == id).FirstOrDefault();
                update.IsApproved = status;
                context.SaveChanges();
                var result = context.RatingReviews.OrderByDescending(p => p.RatingReviewsID).ToList();

                return View(result);

            }

            else
                return RedirectToAction("UnAuthorizeAccess", "Home", new { area = "" });
            
        }
        public ActionResult ChangeStatusTeacher(int? id, bool status)
        {
            if (Config.CurrentUser == 0)
                return RedirectToAction("Login", "Account", new { area = "" });

            if (Config.User.UserType == (int)Enumerations.UserType.Super
               || Config.User.UserType == (int)Enumerations.UserType.Admin)
            {

                var update = context.RatingReviews.Where(o => o.RatingReviewsID == id).FirstOrDefault();
                update.IsActive = status;
                context.SaveChanges();
                var bannerslides = ReviewManagers.GetAllReviewsByTeachers();

                return View(bannerslides);

            }

            else
                return RedirectToAction("UnAuthorizeAccess", "Home", new { area = "" });
            
        }
        public ActionResult ChangeApprovedStatusTeacher(int? id, bool status)
        {
            if (Config.CurrentUser == 0)
                return RedirectToAction("Login", "Account", new { area = "" });

            if (Config.User.UserType == (int)Enumerations.UserType.Super
               || Config.User.UserType == (int)Enumerations.UserType.Admin)
            {

                var update = context.RatingReviews.Where(o => o.RatingReviewsID == id).FirstOrDefault();
                update.IsApproved = status;
                context.SaveChanges();
                var bannerslides = ReviewManagers.GetAllReviewsByTeachers();

                return View(bannerslides);

            }

            else
                return RedirectToAction("UnAuthorizeAccess", "Home", new { area = "" });
            
        }

        public ActionResult ButtonClick(bool? divClicked)
        {
            if (Config.CurrentUser == 0)
                return RedirectToAction("Login", "Account", new { area = "" });

            if (Config.User.UserType == (int)Enumerations.UserType.Super
               || Config.User.UserType == (int)Enumerations.UserType.Admin)
            {

                ViewBag.Active = divClicked.HasValue ? divClicked : false;
                return View();

            }

            else
                return RedirectToAction("UnAuthorizeAccess", "Home", new { area = "" });

           
        }

        public ActionResult TeacherReviews()
        {
            if (Config.CurrentUser == 0)
                return RedirectToAction("Login", "Account", new { area = "" });

            if (Config.User.UserType == (int)Enumerations.UserType.Super
               || Config.User.UserType == (int)Enumerations.UserType.Admin)
            {

                var bannerslides = ReviewManagers.GetAllReviewsByTeachers();

                return View(bannerslides);

            }

            else
                return RedirectToAction("UnAuthorizeAccess", "Home", new { area = "" });
            
        }

        public ActionResult AdminReviews()
        {
            if (Config.CurrentUser == 0)
                return RedirectToAction("Login", "Account", new { area = "" });

            if (Config.User.UserType == (int)Enumerations.UserType.Super
               || Config.User.UserType == (int)Enumerations.UserType.Admin)
            {

                var result = context.RatingReviews.ToList();
                return View(result);

            }

            else
                return RedirectToAction("UnAuthorizeAccess", "Home", new { area = "" });
            
        }
        public ActionResult AddReviews(string nameIs, string rating, string comment)
        {
            if (Config.CurrentUser == 0)
                return RedirectToAction("Login", "Account", new { area = "" });

            if (Config.User.UserType == (int)Enumerations.UserType.Super
               || Config.User.UserType == (int)Enumerations.UserType.Admin)
            {

                string numericPhone = new String(rating.ToCharArray().Where(c => Char.IsDigit(c)).ToArray());
                RatingReviews rate = new RatingReviews();
                rate.CreatedDate = DateTime.Now;
                rate.CreatedBy = "All";
                rate.Comment = comment;
                rate.IsActive = true;
                rate.IsApproved = true;
                rate.Rating = Convert.ToDouble(numericPhone);
                rate.FKStudentID = 1;
                rate.FKTeacherID = 1;
                rate.StudentName = nameIs;
                context.RatingReviews.Add(rate);
                context.SaveChanges();

                var result = context.RatingReviews.ToList();
                return RedirectToAction("AdminReviews", "Reviews", new { area = "Admin" });

            }

            else
                return RedirectToAction("UnAuthorizeAccess", "Home", new { area = "" });
            
        }
    }
}