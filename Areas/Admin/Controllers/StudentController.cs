﻿using BLL.BusinessManagers;
using BLL.BusinessManagers.AdminManager;
using BLL.BusinessManagers.SessionsManager;
using BLL.BusinessManagers.StateManager;
using BLL.BusinessManagers.StudentManager;
using BLL.BusinessManagers.TeacherManager;
using BLL.BusinessManagers.UserManager;
using BLL.BusinessModels.AdminModels;
using BLL.BusinessModels.StudentModels;
using BLL.Helpers;
using DAL.Entities;
using DAL.MasterEntity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace Tutoring.Areas.Admin.Controllers
{
    public class StudentController : Controller
    {
        UsersActivityManager activityMgr = new UsersActivityManager();
        UserActivities obj_activity = null;
        MyDbContext context = new MyDbContext();
        // GET: Admin/Student
        public ActionResult Index(int? flag)
        {
            ViewBag.StudentsMenu = "class = active";
            StudentManager studentMgr = new StudentManager();
            AdminManager adminManager = new AdminManager();

            var usersList = new List<UserViewModel>();
            var update = context.Notifications.Where(p => p.IsSeen != true).ToList();
            foreach (var item in update)
            {
                long id = Convert.ToInt64(item.UserID);
                var user = context.Users.Where(p => p.UserID == id).FirstOrDefault();
                if (user.UserType == 4 || user.UserType == 5)
                {
                    item.IsSeen = true;
                }

            }
            context.SaveChanges();
            if (Config.CurrentUser == 0)
                return RedirectToAction("Login", "Account", new { area = "" });

            if (Config.User.UserType == (int)Enumerations.UserType.Super
                || Config.User.UserType == (int)Enumerations.UserType.Admin)
            {
                if (flag == 1)
                {
                    ViewBag.TotalStudentsCount = studentMgr.TotalStudentsCount();
                    ViewBag.TotalStudentsRegisteredThisMonthCount = studentMgr.TotalStudentsRegisteredThisMonthCount();
                    ViewBag.TotalStudentsRegisteredTodayCount = studentMgr.TotalStudentsRegisteredTodayCount();

                    if (Config.User.UserType == (int)Enumerations.UserType.Admin)
                    {
                        var currentUserPermissions = adminManager.GetPermissionIDsOfUserbyUserID(Config.CurrentUser);

                        ViewBag.currentUserPermissions = currentUserPermissions;

                    }

                    var studentsIds = TempData["studentIds"] as List<long>;
                    foreach (var item in studentsIds)
                    {
                        var model = studentMgr.GetStudentByID(item);
                        usersList.Add(model);
                    }
                    return View(usersList);
                }

                if (flag == 2)
                {
                    ViewBag.TotalStudentsCount = studentMgr.TotalStudentsCount();
                    ViewBag.TotalStudentsRegisteredThisMonthCount = studentMgr.TotalStudentsRegisteredThisMonthCount();
                    ViewBag.TotalStudentsRegisteredTodayCount = studentMgr.TotalStudentsRegisteredTodayCount();

                    if (Config.User.UserType == (int)Enumerations.UserType.Admin)
                    {
                        var currentUserPermissions = adminManager.GetPermissionIDsOfUserbyUserID(Config.CurrentUser);

                        ViewBag.currentUserPermissions = currentUserPermissions;

                    }

                    var studentsIds = TempData["studentIds"] as List<long>;
                    foreach (var item in studentsIds)
                    {
                        var model = studentMgr.GetStudentByID(item);
                        usersList.Add(model);
                    }
                    return View(usersList);
                }

                if (flag == 3)
                {
                    ViewBag.TotalStudentsCount = studentMgr.TotalStudentsCount();
                    ViewBag.TotalStudentsRegisteredThisMonthCount = studentMgr.TotalStudentsRegisteredThisMonthCount();
                    ViewBag.TotalStudentsRegisteredTodayCount = studentMgr.TotalStudentsRegisteredTodayCount();

                    if (Config.User.UserType == (int)Enumerations.UserType.Admin)
                    {
                        var currentUserPermissions = adminManager.GetPermissionIDsOfUserbyUserID(Config.CurrentUser);

                        ViewBag.currentUserPermissions = currentUserPermissions;

                    }

                    var studentsIds = TempData["studentIds"] as List<long>;
                    foreach (var item in studentsIds)
                    {
                        var model = studentMgr.GetStudentByID(item);
                        usersList.Add(model);
                    }
                    return View(usersList);
                }
                if (flag == 4)
                {
                    ViewBag.TotalStudentsCount = studentMgr.TotalStudentsCount();
                    ViewBag.TotalStudentsRegisteredThisMonthCount = studentMgr.TotalStudentsRegisteredThisMonthCount();
                    ViewBag.TotalStudentsRegisteredTodayCount = studentMgr.TotalStudentsRegisteredTodayCount();

                    if (Config.User.UserType == (int)Enumerations.UserType.Admin)
                    {
                        var currentUserPermissions = adminManager.GetPermissionIDsOfUserbyUserID(Config.CurrentUser);

                        ViewBag.currentUserPermissions = currentUserPermissions;

                    }

                    var studentsIds = TempData["studentIds"] as List<long>;
                    foreach (var item in studentsIds)
                    {
                        var model = studentMgr.GetStudentByID(item);
                        usersList.Add(model);
                    }
                    return View(usersList);
                }

                var studentsList = studentMgr.GetStudentsList();

                ViewBag.TotalStudentsCount = studentMgr.TotalStudentsCount();
                ViewBag.TotalStudentsRegisteredThisMonthCount = studentMgr.TotalStudentsRegisteredThisMonthCount();
                ViewBag.TotalStudentsRegisteredTodayCount = studentMgr.TotalStudentsRegisteredTodayCount();

                if (Config.User.UserType == (int)Enumerations.UserType.Admin)
                {
                    var currentUserPermissions = adminManager.GetPermissionIDsOfUserbyUserID(Config.CurrentUser);

                    ViewBag.currentUserPermissions = currentUserPermissions;

                }
                return View(studentsList);
            }
            else

                return RedirectToAction("UnAuthorizeAccess", "Home", new { area = "" });
        }


        public ActionResult CreateStudent()
        {
            if (Config.CurrentUser == 0)
                return RedirectToAction("Login", "Account", new { area = "" });

            if (Config.User.UserType == (int)Enumerations.UserType.Super
                || Config.User.UserType == (int)Enumerations.UserType.Admin)
            {
                var studentModel = new UserViewModel();
                studentModel.IsActive = true;
                studentModel.UserType = (int)Enumerations.UserType.Student;
                studentModel.UserStatus = (int)Enumerations.UserStatus.IsActive;

                CurriculumsManagers curriculumsMgr = new CurriculumsManagers();

                var curriculumsList = curriculumsMgr.GetCurriculumsList();
                studentModel.CurriculumsList = studentModel.GetCurriculumsList(curriculumsList);

                StudentManager curriculumMgr = new StudentManager();
                var gradesList = curriculumMgr.GetGradesListByCurriculumID(1);
                studentModel.GradesList = studentModel.GetGradesList(gradesList);

                StateManager stateManager = new StateManager();
                var statesList = stateManager.GetAllStates();
                studentModel.StatesList = studentModel.GetStatesList(statesList);

                return View(studentModel);
            }
            else
                return RedirectToAction("UnAuthorizeAccess", "Home", new { area = "" });


        }

        [HttpPost]
        public ActionResult CreateStudent(UserViewModel userModel)
        {
            if (Config.CurrentUser == 0)
                return RedirectToAction("Login", "Account", new { area = "" });

            if (Config.User.UserType == (int)Enumerations.UserType.Super
                || Config.User.UserType == (int)Enumerations.UserType.Admin)
            {
                if (ModelState.IsValid)
                {
                    StudentManager obj_stdManager = new StudentManager();
                    userModel.LoginEmail = userModel.LoginEmail.ToLower();
                    userModel.IsActive = true;
                    userModel.IsVerified = (int)Enumerations.VerificationType.VerifiedByAdmin;
                    userModel.UserStatus = (int)Enumerations.UserStatus.Approved;
                    userModel.curriculumViewModel.IsActive = true;
                    userModel.gradeViewModel.IsActive = true;
                    userModel.UsersProfileViewModel.FKOccupationId = (int)Enumerations.Occupation.Student;

                    var newStudent = obj_stdManager.CreateNewStudent(userModel);
                    if (newStudent.Response == (int)Enumerations.ResponsHelpers.Created)
                    {
                        obj_activity = new UserActivities();
                        obj_activity.ActionPerformed = "New Student Created";
                        obj_activity.FKUserID = newStudent.UserID;
                        obj_activity.ActivitySource = (int)Enumerations.ActivitySource.BacksideActivity;

                        activityMgr.AddActivity(obj_activity);
                    }
                }

                return RedirectToAction("Index", "Student");
            }
            else
                return RedirectToAction("UnAuthorizeAccess", "Home", new { area = "" });

            
        }

        public ActionResult GetGradesByCurriculumID(int curriculumID)
        {
            if (Config.CurrentUser == 0)
                return RedirectToAction("Login", "Account", new { area = "" });

            if (Config.User.UserType == (int)Enumerations.UserType.Super
                || Config.User.UserType == (int)Enumerations.UserType.Admin)
            {
                StudentManager studentMgr = new StudentManager();
                List<GradeViewModel> gradesList = new List<GradeViewModel>();
                List<SelectListItem> GradesList = new List<SelectListItem>();
                if (curriculumID > 0)
                {
                    gradesList = studentMgr.GetGradesListByCurriculumID(curriculumID);
                    UserViewModel userModel = new UserViewModel();
                    GradesList = userModel.GetGradesList(gradesList);

                }
                return Json(new { Success = "true", GradesList = GradesList });
            }
            else
                return RedirectToAction("UnAuthorizeAccess", "Home", new { area = "" });
        }

        public ActionResult GetStudent(int userID)
        {
            if (Config.CurrentUser == 0)
                return RedirectToAction("Login", "Account", new { area = "" });

            if (Config.User.UserType == (int)Enumerations.UserType.Super
                || Config.User.UserType == (int)Enumerations.UserType.Admin)
            {
                StudentManager StudentMgr = new StudentManager();
                StateManager stateMgr = new StateManager();

                CurriculumsManagers curriculumsMgr = new CurriculumsManagers();

                var studentModel = StudentMgr.GetStudentByID(userID);

                if (studentModel.UsersProfileViewModel != null)
                {
                    if (studentModel.UsersProfileViewModel.FKStateId > 0)
                    {
                        studentModel.stateViewModel = stateMgr.GetStateByStateID(studentModel.UsersProfileViewModel.FKStateId);
                    }
                }

                var statesList = stateMgr.GetAllStates();
                studentModel.StatesList = studentModel.GetStatesList(statesList);

                var curriculumsList = curriculumsMgr.GetCurriculumsList();
                studentModel.CurriculumsList = studentModel.GetCurriculumsList(curriculumsList);

                if (studentModel.UsersProfileViewModel != null)
                {
                    if (studentModel.UsersProfileViewModel.GradeID > 0)
                    {
                        var gradeModel = StudentMgr.GetGradeByGradeID(studentModel.UsersProfileViewModel.GradeID);
                        var gradesList = StudentMgr.GetGradesListByCurriculumID(gradeModel.FKCurriculumID);
                        studentModel.CurriculumID = gradeModel.FKCurriculumID;

                        studentModel.GradesList = studentModel.GetGradesList(gradesList);
                    }
                    else
                    {
                        List<GradeViewModel> list = new List<GradeViewModel>();
                        studentModel.GradesList = studentModel.GetGradesList(list);
                    }
                }                
                return View(studentModel);
            }
            else
                return RedirectToAction("UnAuthorizeAccess", "Home", new { area = "" });        
        }

        [HttpPost]
        public ActionResult UpdateStudent(UserViewModel model)
        {
            if (Config.CurrentUser == 0)
                return RedirectToAction("Login", "Account", new { area = "" });

            if (Config.User.UserType == (int)Enumerations.UserType.Super
                || Config.User.UserType == (int)Enumerations.UserType.Admin)
            {
                UsersProfileViewModel userProfile = new UsersProfileViewModel();
                userProfile.UpdatedDate = DateTime.Now;

                StudentManager StudentMgr = new StudentManager();
                var response = StudentMgr.UpdateStudentWithStudentProfile(model);
                if (response == (int)Enumerations.ResponsHelpers.Updated)
                {
                    obj_activity = new UserActivities();
                    obj_activity.FKUserID = model.UserID;
                    obj_activity.ActionPerformed = "Student Updated";
                    obj_activity.ActivitySource = (int)Enumerations.ActivitySource.BacksideActivity;
                    activityMgr.AddActivity(obj_activity);
                }

                return RedirectToAction("Index", "Student");
            }
            else
                return RedirectToAction("UnAuthorizeAccess", "Home", new { area = "" });

            
        }

        public ActionResult ApproveStudent(int userID)
        {
            if (Config.CurrentUser == 0)
                return RedirectToAction("Login", "Account", new { area = "" });

            if (Config.User.UserType == (int)Enumerations.UserType.Super
                || Config.User.UserType == (int)Enumerations.UserType.Admin)
            {
                if (userID > 0)
                {
                    StudentManager StudentMgr = new StudentManager();
                    var studentDetail = StudentMgr.GetStudentByID(userID);
                    studentDetail.UserStatus = (int)Enumerations.UserStatus.Approved;
                    studentDetail.IsVerified = (int)Enumerations.VerificationType.VerifiedByAdmin;
                    StudentMgr.UpdateStudent(studentDetail);

                    // UserManager.DeleteFromUserBlockReason(userID);

                    obj_activity = new UserActivities();
                    obj_activity.ActionPerformed = "Student got Approved";
                    obj_activity.FKUserID = userID;
                    obj_activity.ActivitySource = (int)Enumerations.ActivitySource.BacksideActivity;
                    activityMgr.AddActivity(obj_activity);

                }
                return RedirectToAction("Index");
            }
            else
                return RedirectToAction("UnAuthorizeAccess", "Home", new { area = "" });

            
        }

        public ActionResult GetStudentToBlock(int userID, string requestFrom)
        {
            if (Config.CurrentUser == 0)
                return RedirectToAction("Login", "Account", new { area = "" });

            if (Config.User.UserType == (int)Enumerations.UserType.Super
                || Config.User.UserType == (int)Enumerations.UserType.Admin)
            {
                StudentManager StudentMgr = new StudentManager();
                var studentModel = StudentMgr.GetStudentByID(userID);

                studentModel.RequestFrom = requestFrom;

                return View(studentModel);
            }
            else
                return RedirectToAction("UnAuthorizeAccess", "Home", new { area = "" });

           
        }

        public ActionResult BlockStudent(UserViewModel userModel)
        {
            if (Config.CurrentUser == 0)
                return RedirectToAction("Login", "Account", new { area = "" });

            if (Config.User.UserType == (int)Enumerations.UserType.Super
                || Config.User.UserType == (int)Enumerations.UserType.Admin)
            {
                StudentManager StudentMgr = new StudentManager();
                var studentmodel = new UserViewModel();

                studentmodel = StudentMgr.GetStudentByID(userModel.UserID);
                studentmodel.UserStatus = (int)Enumerations.UserStatus.IsBlocked;
                studentmodel.IsVerified = (int)Enumerations.VerificationType.NotVerified;
                var status = StudentMgr.UpdateStudent(studentmodel);

                if (status == (int)Enumerations.ResponsHelpers.Block)
                {

                    UserBlockReason obj_userBlock = new UserBlockReason();
                    obj_userBlock.FKUserID = userModel.UserID;
                    obj_userBlock.Reason = userModel.BlockReason;
                    StudentMgr.AddInUserBlockReason(obj_userBlock);

                    obj_activity = new UserActivities();
                    obj_activity.FKUserID = userModel.UserID;
                    obj_activity.ActivitySource = (int)Enumerations.ActivitySource.BacksideActivity;
                    if (userModel.RequestFrom == "StudentMenu")
                    {
                        obj_activity.ActionPerformed = "Student Blocked";
                    }
                    else if (userModel.RequestFrom == "TeacherMenu")
                    {
                        obj_activity.ActionPerformed = "Teacher Blocked";
                    }

                    activityMgr.AddActivity(obj_activity);


                    if (!string.IsNullOrEmpty(studentmodel.LoginEmail))
                    {
                        string username = null;

                        if (studentmodel.UsersProfileViewModel != null)
                        {
                            username = studentmodel.UsersProfileViewModel.FirstName + " " + studentmodel.UsersProfileViewModel.LastName;
                        }

                        HelperMethods.SendEmailRegardingBlock(studentmodel.LoginEmail, username, userModel.BlockReason);
                    }

                }


                if (userModel.RequestFrom == "StudentMenu")
                {
                    return RedirectToAction("Index", "Student");
                }

                else if (userModel.RequestFrom == "TeacherMenu")
                {
                    return RedirectToAction("ViewTeachers", "Teacher");
                }

                return RedirectToAction("ViewTeachers", "Teacher");
            }
            else
                return RedirectToAction("UnAuthorizeAccess", "Home", new { area = "" });

           
        }

        public ActionResult DeleteStudent(int userID)
        {
            if (Config.CurrentUser == 0)
                return RedirectToAction("Login", "Account", new { area = "" });

            if (Config.User.UserType == (int)Enumerations.UserType.Super
                || Config.User.UserType == (int)Enumerations.UserType.Admin)
            {
                StudentManager StudentMgr = new StudentManager();
                if (userID > 0)
                {
                    var username = UserManager.GetUserNameByID(userID);

                    var response = StudentMgr.DeleteStudentByUserID(userID);
                    if (response == (int)Enumerations.ResponsHelpers.Deleted)
                    {
                        obj_activity = new UserActivities();
                        obj_activity.ActionPerformed = "Student with the name " + username + " has been Deleted";
                        obj_activity.ActivitySource = (int)Enumerations.ActivitySource.BacksideActivity;
                        obj_activity.FKUserID = userID;

                        activityMgr.AddActivity(obj_activity);
                    }
                }

                return RedirectToAction("Index", "Student");
            }
            else
                return RedirectToAction("UnAuthorizeAccess", "Home", new { area = "" });

           
        }

        public ActionResult StudentDetails(int userID)
        {
            if (Config.CurrentUser == 0)
                return RedirectToAction("Login", "Account", new { area = "" });

            if (Config.User.UserType == (int)Enumerations.UserType.Super
                || Config.User.UserType == (int)Enumerations.UserType.Admin)
            {
                UsersActivityManager activityMgr = new UsersActivityManager();
                StudentManager studentMgr = new StudentManager();

                var studentDetail = studentMgr.GetStudentDetails(userID);
                studentDetail.lastLoginActivityModel = activityMgr.GetLastLoginActivity(userID);
                studentDetail.lastLogoutActivityModel = activityMgr.GetLastLogoutActivity(userID);
                
                return View(studentDetail);
            }
            else
                return RedirectToAction("UnAuthorizeAccess", "Home", new { area = "" });
            
        }
        public ActionResult FilterMethod(string request)
        {
            if (Config.CurrentUser == 0)
                return RedirectToAction("Login", "Account", new { area = "" });

            if (Config.User.UserType == (int)Enumerations.UserType.Super
                || Config.User.UserType == (int)Enumerations.UserType.Admin)
            {
                TeacherManager teacherMgr = new TeacherManager();
                StudentManager studentMgr = new StudentManager();
                UserViewModel userModel = new UserViewModel();

                if (request == "FilterByTeacher")
                {
                    // it will give all teachers
                    var teachersList = teacherMgr.GetUsersByUserType((int)Enumerations.UserType.Teacher);
                    var teachersListItems = GenerateTeachersDropDown(teachersList);
                    return Json(new { Success = "FilterByTeacher", teachersList = teachersListItems });
                }
                if (request == "FilterByGrade")
                {
                    // it will give all grades
                    var gradesList = studentMgr.GetGradesList();
                    var gradesListItems = userModel.GetGradesList(gradesList);
                    return Json(new { Success = "FilterByGrade", gradesList = gradesListItems });
                }
                if (request == "FilterBySubject")
                {
                    var subjectsList = studentMgr.GetSubjectsList();
                    var subjectsListItems = GenerateSubjectsDropdown(subjectsList);
                    return Json(new { Success = "FilterBySubject", subjectsList = subjectsListItems });

                }
                if (request == "FilterByCity")
                {
                    var citiesList = studentMgr.GetAllCities();
                    var citiesListItems = GenerateCitiesDropdown(citiesList);
                    return Json(new { Success = "FilterByCity", citiesList = citiesListItems });

                }
                if (request == "FilterByStatus")
                {
                    var citiesList = studentMgr.GetAllStatus();
                    var citiesListItems = GenerateStatusDropdown(citiesList);
                    return Json(new { Success = "FilterByStatus", citiesList = citiesListItems });

                }
                return Json(new { Success = "true" });
            }
            else
                return RedirectToAction("UnAuthorizeAccess", "Home", new { area = "" });
            
        }

        [HttpPost]
        public ActionResult ApplyFilter(string mainfilterRequest, string subfilterRequest)
        {
            if (Config.CurrentUser == 0)
                return RedirectToAction("Login", "Account", new { area = "" });

            if (Config.User.UserType == (int)Enumerations.UserType.Super
                || Config.User.UserType == (int)Enumerations.UserType.Admin)
            {
                TempData["mainfilterValue"] = null;
                TempData["subfilterValue"] = null;
                TempData["studentIds"] = null;

                SessionManager sessionMgr = new SessionManager();
                StudentManager studentMgr = new StudentManager();

                if (mainfilterRequest == "FilterByTeacher")
                {
                    var studentsList = studentMgr.MyStudents(int.Parse(subfilterRequest));

                    TempData["studentIds"] = studentsList;
                    TempData["mainfilterValue"] = mainfilterRequest;
                    TempData["subfilterValue"] = subfilterRequest;

                    return RedirectToAction("Index", new { flag = 1 });

                }
                if (mainfilterRequest == "FilterByGrade")
                {
                    var studentsList = studentMgr.GetStudentsByGradeID(int.Parse(subfilterRequest));

                    TempData["studentIds"] = studentsList;
                    TempData["mainfilterValue"] = mainfilterRequest;
                    TempData["subfilterValue"] = subfilterRequest;

                    return RedirectToAction("Index", new { flag = 2 });

                }
                if (mainfilterRequest == "FilterBySubject")
                {
                    var studentsList = studentMgr.GetStudentsBySubjectID(int.Parse(subfilterRequest));

                    TempData["studentIds"] = studentsList;
                    TempData["mainfilterValue"] = mainfilterRequest;
                    TempData["subfilterValue"] = subfilterRequest;

                    return RedirectToAction("Index", new { flag = 3 });

                }
                if (mainfilterRequest == "FilterByCity")
                {
                    var studentsList = studentMgr.GetStudentsByCity(subfilterRequest);

                    TempData["studentIds"] = studentsList;
                    TempData["mainfilterValue"] = mainfilterRequest;
                    TempData["subfilterValue"] = subfilterRequest;

                    return RedirectToAction("Index", new { flag = 4 });
                }
                if (mainfilterRequest == "FilterByStatus")
                {
                    var studentsList = studentMgr.GetStudentsByStatus(int.Parse(subfilterRequest));

                    TempData["studentIds"] = studentsList;
                    TempData["mainfilterValue"] = mainfilterRequest;
                    TempData["subfilterValue"] = subfilterRequest;

                    return RedirectToAction("Index", new { flag = 4 });
                }

                return View();
            }
            else
            {
                return RedirectToAction("UnAuthorizeAccess", "Home", new { area = "" });
            }
        }

        public List<SelectListItem> GenerateTeachersDropDown(List<UserViewModel> teachersList)
        {
            List<SelectListItem> ListOfTeachers = new List<SelectListItem>();
            ListOfTeachers.Add(new SelectListItem { Value = "", Text = "Select Teacher", Selected = false });
            foreach (var teacher in teachersList)
            {
                ListOfTeachers.Add(new SelectListItem { Value = teacher.UserID.ToString(), Text = teacher.UsersProfileViewModel.FirstName + " " + teacher.UsersProfileViewModel.LastName, Selected = false });
            }
            return ListOfTeachers;
        }
        public List<SelectListItem> GenerateSubjectsDropdown(List<Subjects> subjectsList)
        {
            StudentManager stdMgr = new StudentManager();

            List<SelectListItem> ListOfsubjcts = new List<SelectListItem>();
            ListOfsubjcts.Add(new SelectListItem { Value = "", Text = "Select Subject", Selected = false });
            foreach (var s in subjectsList)
            {
                var gradeModel = stdMgr.GetGradeByGradeID(s.FKGradeID);
                ListOfsubjcts.Add(new SelectListItem { Value = s.SubjectID.ToString(), Text = "Grade-" + gradeModel.GradeName + " " + s.SubjectName, Selected = false });
            }
            return ListOfsubjcts;
        }

        public List<SelectListItem> GenerateCitiesDropdown(List<string> citiesList)
        {
            StudentManager stdMgr = new StudentManager();

            List<SelectListItem> ListOfcity = new List<SelectListItem>();
            ListOfcity.Add(new SelectListItem { Value = "", Text = "Select City", Selected = false });
            foreach (var city in citiesList)
            {
                ListOfcity.Add(new SelectListItem { Value = city.ToString(), Text = city, Selected = false });
            }
            return ListOfcity;
        }
        public List<SelectListItem> GenerateStatusDropdown(List<string> citiesList)
        {
            //StudentManager stdMgr = new StudentManager();

            List<SelectListItem> ListOfcity = new List<SelectListItem>();
            ListOfcity.Add(new SelectListItem { Value = "", Text = "Select Status", Selected = false });
            foreach (var city in citiesList)
            {
                if (city == "1")
                {
                    ListOfcity.Add(new SelectListItem { Text = "IsDeleted", Value = city, Selected = false });
                }
                else if (city == "2")
                {
                    ListOfcity.Add(new SelectListItem { Text = "IsBlocked", Value = city, Selected = false });
                }
                else if (city == "3")
                {
                    ListOfcity.Add(new SelectListItem { Text = "IsActive", Value = city, Selected = false });
                }
                else if (city == "4")
                {
                    ListOfcity.Add(new SelectListItem { Text = "IsOnline", Value = city, Selected = false });
                }
                else if (city == "5")
                {
                    ListOfcity.Add(new SelectListItem { Text = "NotApproved", Value = city, Selected = false });
                }
                else if (city == "6")
                {
                    ListOfcity.Add(new SelectListItem { Text = "Approved", Value = city, Selected = false });
                }
                else if (city == "7")
                {
                    ListOfcity.Add(new SelectListItem { Text = "Pending", Value = city, Selected = false });
                }
                //ListOfcity.Add(new SelectListItem { Value = city.ToString(), Text = city, Selected = false });
            }
            return ListOfcity;
        }

    }
}