﻿using BLL.BusinessManagers;
using BLL.BusinessManagers.AdminManager;
using BLL.BusinessManagers.StateManager;
using BLL.BusinessManagers.StudentManager;
using BLL.BusinessManagers.TeacherManager;
using BLL.BusinessManagers.UserManager;
using BLL.BusinessModels.AdminModels;
using BLL.BusinessModels.StudentModels;
using BLL.Helpers;
using DAL.MasterEntity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Tutoring.Filter;

namespace Tutoring.Areas.Admin.Controllers
{

    public class AdminController : Controller
    {
        UsersActivityManager ActivityMgr = new UsersActivityManager();
        MyDbContext context = new MyDbContext();
        // GET: Admin/Admin
        public ActionResult Index()
        {
            if (Config.CurrentUser == 0 )
                return RedirectToAction("Login", "Account", new { area = "" });

            if ((Config.User.UserType == (int)Enumerations.UserType.Super
                || Config.User.UserType == (int)Enumerations.UserType.Admin) )
            {
                ViewBag.DashboardMenu = "class = active";
                return View();
            }
            
            else
            {
                return RedirectToAction("UnAuthorizeAccess", "Home", new { area = "" });
            }

        }
        public ActionResult SiteDisable()
        {
            return View();
        }


        public ActionResult AdminsList()
        {
            if (Config.CurrentUser == 0)
                return RedirectToAction("Login", "Account", new { area = "" });

            if (Config.User.UserType == (int)Enumerations.UserType.Super
                || Config.User.UserType == (int)Enumerations.UserType.Admin)
            {
                ViewBag.SubAdminsMenu = "class = active";
                AdminManager adminManager = new AdminManager();
                var update = context.Notifications.Where(p => p.IsSeen != true).ToList();
                foreach (var item in update)
                {
                    long id = Convert.ToInt64(item.UserID);
                    var user = context.Users.Where(p => p.UserID == id).FirstOrDefault();
                    if (user.UserType == 1 || user.UserType == 2)
                    {
                        item.IsSeen = true;
                    }

                }
                context.SaveChanges();
                var adminsList = adminManager.GetAllAdminsList();

                if (Config.User.UserType == (int)Enumerations.UserType.Admin)
                {
                    var currentUserPermissions = adminManager.GetPermissionIDsOfUserbyUserID(Config.CurrentUser);

                    ViewBag.currentUserPermissions = currentUserPermissions;

                }

                return View(adminsList);
            }
            else

                return RedirectToAction("UnAuthorizeAccess", "Home", new { area = "" });

        }

        public ActionResult CreateAdmin()
        {
            if (Config.CurrentUser == 0)
                return RedirectToAction("Login", "Account", new { area = "" });

            if (Config.User.UserType == (int)Enumerations.UserType.Super
                || Config.User.UserType == (int)Enumerations.UserType.Admin)
            {
                var userViewModel = new UserViewModel();
                userViewModel.UserType = (int)Enumerations.UserType.Admin;
                userViewModel.UserStatus = (int)Enumerations.UserStatus.IsActive;

                StateManager stateManager = new StateManager();
                var statesList = stateManager.GetAllStates();
                userViewModel.StatesList = userViewModel.GetStatesList(statesList);

                return View(userViewModel);
            }

            else
                return RedirectToAction("UnAuthorizeAccess", "Home", new { area = "" });

        }

        [HttpPost]
        public ActionResult CreateAdmin(UserViewModel userViewModelObj)
        {
            if (Config.CurrentUser == 0)
                return RedirectToAction("Login", "Account", new { area = "" });

            if (Config.User.UserType == (int)Enumerations.UserType.Super
               || Config.User.UserType == (int)Enumerations.UserType.Admin)
            {
                if (ModelState.IsValid)
                {

                    AdminManager obj_adminManager = new AdminManager();

                    userViewModelObj.LoginEmail = userViewModelObj.LoginEmail.ToLower();
                    userViewModelObj.IsActive = true;
                    obj_adminManager.CreateNewAdmin(userViewModelObj);
                }

                return RedirectToAction("AdminsList", "Admin");
            }

            else
                return RedirectToAction("UnAuthorizeAccess", "Home", new { area = "" });

        }

        public ActionResult AssignPermissions(int userID, int userType)
        {
            if (Config.CurrentUser == 0)
                return RedirectToAction("Login", "Account", new { area = "" });

            if (Config.User.UserType == (int)Enumerations.UserType.Super
               || Config.User.UserType == (int)Enumerations.UserType.Admin)
            {
                AdminManager adminMgr = new AdminManager();
                var usermodel = adminMgr.GetAdminByUserID(userID);

                usermodel.listOfAllPermissions = adminMgr.GetPermissionsList();
                usermodel.permissionsList = adminMgr.GetPermissionsListByUserID(userID);

                return View(usermodel);
            }
            else
                return RedirectToAction("UnAuthorizeAccess", "Home", new { area = "" });

        }

        public ActionResult SaveAssignedPermissionsToUser(string[] permissionIDs, int userID, int userType)
        {
            if (Config.CurrentUser == 0)
                return RedirectToAction("Login", "Account", new { area = "" });

            if (Config.User.UserType == (int)Enumerations.UserType.Super
               || Config.User.UserType == (int)Enumerations.UserType.Admin)
            {
                AdminManager adminManager = new AdminManager();

                adminManager.DeleteUserPermissionsByUserID(userID);

                if (permissionIDs != null)
                {
                    adminManager.AddInUserPermissions(permissionIDs, userID, userType);
                }

                return RedirectToAction("AdminsList", "Admin");
            }
            else
                return RedirectToAction("UnAuthorizeAccess", "Home", new { area = "" });
        }

        public ActionResult GetAdmin(int userID)
        {
            if (Config.CurrentUser == 0)
                return RedirectToAction("Login", "Account", new { area = "" });

            if (Config.User.UserType == (int)Enumerations.UserType.Super
               || Config.User.UserType == (int)Enumerations.UserType.Admin)
            {
                AdminManager adminMgr = new AdminManager();
                StateManager stateMgr = new StateManager();
                var adminModel = adminMgr.GetAdminByUserID(userID);

                var states = stateMgr.GetAllStates();
                adminModel.StatesList = adminModel.GetStatesList(states);

                return View(adminModel);
            }
            else
                return RedirectToAction("UnAuthorizeAccess", "Home", new { area = "" });

        }

        public ActionResult UpdateAdmin(UserViewModel model)
        {
            if (Config.CurrentUser == 0)
                return RedirectToAction("Login", "Account", new { area = "" });

            if (Config.User.UserType == (int)Enumerations.UserType.Super
               || Config.User.UserType == (int)Enumerations.UserType.Admin)
            {
                AdminManager adminMgr = new AdminManager();
                adminMgr.UpdateAdmin(model);

                return RedirectToAction("AdminsList", "Admin");
            }
            else
                return RedirectToAction("UnAuthorizeAccess", "Home", new { area = "" });
        }

        public ActionResult DeleteAdmin(int userID)
        {
            if (Config.CurrentUser == 0)
                return RedirectToAction("Login", "Account", new { area = "" });

            if (Config.User.UserType == (int)Enumerations.UserType.Super
               || Config.User.UserType == (int)Enumerations.UserType.Admin)
            {
                AdminManager adminMgr = new AdminManager();
                adminMgr.DeleteAdminByUserID(userID);

                return RedirectToAction("AdminsList", "Admin");
            }
            else
                return RedirectToAction("UnAuthorizeAccess", "Home", new { area = "" });


        }

        public ActionResult CheckEmailInDatabase(string email)
        {
            if (Config.CurrentUser == 0)
                return RedirectToAction("Login", "Account", new { area = "" });

            if (Config.User.UserType == (int)Enumerations.UserType.Super
               || Config.User.UserType == (int)Enumerations.UserType.Admin)
            {
                var emailExists = UserManager.CheckEmailInUsersTable(email);
                if (emailExists)

                    return Json(new { Success = "true" });

                else
                    return Json(new { Success = "false" });
            }
            else
                return RedirectToAction("UnAuthorizeAccess", "Home", new { area = "" });

        }

        public ActionResult MainActivities(int? side)
        {
            if (Config.CurrentUser == 0)
                return RedirectToAction("Login", "Account", new { area = "" });

            if (Config.User.UserType == (int)Enumerations.UserType.Super
               || Config.User.UserType == (int)Enumerations.UserType.Admin)
            {
                ViewBag.ActivitiesMenu = "class = active";

                var activitiesList = new List<UserActivitiesModel>();
                if (side == (int)Enumerations.ActivitySource.FrontsideActivity)
                {
                    ViewBag.FrontActivitiesLink = "color: #846c6c";
                    activitiesList = ActivityMgr.GetFrontendActivities();
                }
                else if (side == (int)Enumerations.ActivitySource.BacksideActivity)
                {
                    ViewBag.BackActivities = "color: #846c6c";
                    activitiesList = ActivityMgr.GetBackendActivities();
                }

                ViewBag.Requestside = side;

                return View(activitiesList);
            }
            else
            {
                return RedirectToAction("UnAuthorizeAccess", "Home", new { area = "" });
            }
        }

        public ActionResult Templates()
        {
            if (Config.CurrentUser == 0)
                return RedirectToAction("Login", "Account", new { area = "" });

            if (Config.User.UserType == (int)Enumerations.UserType.Super
               || Config.User.UserType == (int)Enumerations.UserType.Admin)
            {
                ViewBag.ManageEmailMenu = "class = active";

                return View();
            }
            else
            {
                return RedirectToAction("UnAuthorizeAccess", "Home", new { area = "" });
            }

        }

        public ActionResult EmailTemplate(int? type)
        {
            if (Config.CurrentUser == 0)
                return RedirectToAction("Login", "Account", new { area = "" });

            if (Config.User.UserType == (int)Enumerations.UserType.Super
             || Config.User.UserType == (int)Enumerations.UserType.Admin)
            {
                var emailtemplateData = EmailTemplatesManager.GetEmailTemplate(type);

                return View(emailtemplateData);
            }
            else
            {
                return RedirectToAction("UnAuthorizeAccess", "Home", new { area = "" });
            }

        }

        public ActionResult UpdateTemplate(EmailTemplatesModel model)
        {
            if (Config.CurrentUser == 0)
                return RedirectToAction("Login", "Account", new { area = "" });

            if (Config.User.UserType == (int)Enumerations.UserType.Super
             || Config.User.UserType == (int)Enumerations.UserType.Admin)
            {
                EmailTemplatesManager.UpdateEmailTemplate(model);

                return RedirectToAction("Templates");
            }
            else
            {
                return RedirectToAction("UnAuthorizeAccess", "Home", new { area = "" });
            }
        }


        public ActionResult Broadcast()
        {
            if (Config.CurrentUser == 0)
                return RedirectToAction("Login", "Account", new { area = "" });

            if (Config.User.UserType == (int)Enumerations.UserType.Super
             || Config.User.UserType == (int)Enumerations.UserType.Admin)
            {
                return View();
            }
            else
            {
                return RedirectToAction("UnAuthorizeAccess", "Home", new { area = "" });
            }
        }

        public ActionResult Email(int? type)
        {
            if (Config.CurrentUser == 0)
                return RedirectToAction("Login", "Account", new { area = "" });

            if (Config.User.UserType == (int)Enumerations.UserType.Super
             || Config.User.UserType == (int)Enumerations.UserType.Admin)
            {
                EmailTemplatesModel emailtemplatemodel = new EmailTemplatesModel();
                emailtemplatemodel.Type = (int)type;

                return View(emailtemplatemodel);
            }
            else
            {
                return RedirectToAction("UnAuthorizeAccess", "Home", new { area = "" });
            }

        }

        public ActionResult EmailUsersOnRequest(EmailTemplatesModel model)
        {
            if (Config.CurrentUser == 0)
                return RedirectToAction("Login", "Account", new { area = "" });

            if (Config.User.UserType == (int)Enumerations.UserType.Super
             || Config.User.UserType == (int)Enumerations.UserType.Admin)
            {
                var users = UserManager.GetUsersOfSpecificType(model.Type);
                if (users.Count > 0)
                {
                    UserManager.SendEmailToUsers(users, model);
                }

                return RedirectToAction("Broadcast", "Admin");
            }
            else
            {
                return RedirectToAction("UnAuthorizeAccess", "Home", new { area = "" });
            }

        }

        public ActionResult EmailSelectedUsers()
        {
            if (Config.CurrentUser == 0)
                return RedirectToAction("Login", "Account", new { area = "" });

            if (Config.User.UserType == (int)Enumerations.UserType.Super
             || Config.User.UserType == (int)Enumerations.UserType.Admin)
            {
                return View();
            }
            else
            {
                return RedirectToAction("UnAuthorizeAccess", "Home", new { area = "" });
            }
        }

        public ActionResult SendEmailToSelectedUsers(EmailTemplatesModel modelData  )
        {
            if (Config.CurrentUser == 0)
                return RedirectToAction("Login", "Account", new { area = "" });

            if (Config.User.UserType == (int)Enumerations.UserType.Super
             || Config.User.UserType == (int)Enumerations.UserType.Admin)
            {
                string[] receipentsEmail  = modelData.Receipents.Split(',');

                foreach(var item in receipentsEmail)
                {
                   
                    if(UserManager.IsValidEmail(item))
                    {
                        BLL.EmailTemplate.EmailTemplate.SendEmail(item, modelData.Body, modelData.Subject);
                    }
                   
                }
                return RedirectToAction("Broadcast", "Admin");
            }
            else
            {
                return RedirectToAction("UnAuthorizeAccess", "Home", new { area = "" });
            }

        }

      

        [HttpPost]
        public ActionResult GetReceipents(string Prefix)
        {
            if (Config.CurrentUser == 0)
                return RedirectToAction("Login", "Account", new { area = "" });

            if (Config.User.UserType == (int)Enumerations.UserType.Super
             || Config.User.UserType == (int)Enumerations.UserType.Admin)
            {
                var ObjList = UserManager.GetAllUsers();

                var userData = (from e in ObjList
                                where e.UsersProfileViewModel.FirstName.ToLower().StartsWith(Prefix.ToLower())
                                select new
                                {
                                    name = e.UsersProfileViewModel.FirstName,
                                    email = e.LoginEmail
                                });
                return Json(userData, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return RedirectToAction("UnAuthorizeAccess", "Home", new { area = "" });
            }

        }

        public ActionResult Actions()
        {
            if (Config.CurrentUser == 0)
                return RedirectToAction("Login", "Account", new { area = "" });

            if (Config.User.UserType == (int)Enumerations.UserType.Super
             || Config.User.UserType == (int)Enumerations.UserType.Admin)
            {
                var actionslist = ActionsManager.GetAllActions();


                return View(actionslist);
            }
            else
            {
                return RedirectToAction("UnAuthorizeAccess", "Home", new { area = "" });
            }

        }

        [HttpPost]
        public ActionResult SaveActions(int[] actionIDs)
        {
            if (Config.CurrentUser == 0)
                return RedirectToAction("Login", "Account", new { area = "" });

            if (Config.User.UserType == (int)Enumerations.UserType.Super
             || Config.User.UserType == (int)Enumerations.UserType.Admin)
            {

                ActionsManager.SetActionStatus(actionIDs, true);



                return RedirectToAction("Actions","Admin");
            }
            else
            {
                return RedirectToAction("UnAuthorizeAccess", "Home", new { area = "" });
            }
        }

    }
}
