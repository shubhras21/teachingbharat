﻿using BLL.BusinessManagers.AdminManager;
using BLL.BusinessModels.AdminModels;
using BLL.Helpers;
using DAL.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Tutoring.Areas.Admin.Controllers
{
    public class CurriculumController : Controller
    {
        CurriculumsManagers manager = new CurriculumsManagers();

        // GET: Admin/Curriculum
        public ActionResult Index()
        {
            if (Config.CurrentUser == 0)
                return RedirectToAction("Login", "Account", new { area = "" });

            if (Config.User.UserType == (int)Enumerations.UserType.Super
                || Config.User.UserType == (int)Enumerations.UserType.Admin)
            {
                return View();
            }
            else
                return RedirectToAction("UnAuthorizeAccess", "Home", new { area = "" });

            
        }

        public ActionResult CurriculumsList()
        {
            if (Config.CurrentUser == 0)
                return RedirectToAction("Login", "Account", new { area = "" });

            if (Config.User.UserType == (int)Enumerations.UserType.Super
             || Config.User.UserType == (int)Enumerations.UserType.Admin)
            {
                ViewBag.SubjectsMenu = "class = active";
                ViewBag.Curriculumlink = "color: #846c6c";

                List<Curriculums> list = manager.ListOfCurriculums();
                return View(list);
            }
            else
                return RedirectToAction("UnAuthorizeAccess", "Home", new { area = "" });
            
        }

        public ActionResult CreateCurriculum()
        {
            if (Config.CurrentUser == 0)
                return RedirectToAction("Login", "Account", new { area = "" });

            if (Config.User.UserType == (int)Enumerations.UserType.Super
                  || Config.User.UserType == (int)Enumerations.UserType.Admin)
            {
                var model = new CurriculumsViewModel();
                return View(model);
            }
            else
                return RedirectToAction("UnAuthorizeAccess", "Home", new { area = "" });
            
        }

        [HttpPost]
        public ActionResult CreateCurriculum(CurriculumsViewModel model)
        {
            if (Config.CurrentUser == 0)
                return RedirectToAction("Login", "Account", new { area = "" });

            if (Config.User.UserType == (int)Enumerations.UserType.Super
                 || Config.User.UserType == (int)Enumerations.UserType.Admin)
            {
                if (ModelState.IsValid)
                {
                    CurriculumsManagers manager = new CurriculumsManagers();
                    Int64 CurriculumID = manager.AddCurriculum(model);
                    if (CurriculumID > 0)
                    {
                        TempData["Message"] = "Add succesfully";
                        TempData["Status"] = 1;
                        return RedirectToAction("CurriculumsList");
                    }
                    else
                    {
                        ViewBag.Message = "Something went wrong!";
                        TempData["Status"] = 2;
                        ModelState.AddModelError("", "Something went wrong!");
                    }
                }
                else
                {
                    ViewBag.Message = "Something went wrong!";
                    TempData["Status"] = 2;
                    ModelState.AddModelError("", "Please recheck your feilds");
                }

                return View(model);
            }
                        
           else
            {
                return RedirectToAction("UnAuthorizeAccess", "Home", new { area = "" });
            }
        }

        public ActionResult GradesLists()
        {
            if (Config.CurrentUser == 0)
                return RedirectToAction("Login", "Account", new { area = "" });

            if (Config.User.UserType == (int)Enumerations.UserType.Super
                 || Config.User.UserType == (int)Enumerations.UserType.Admin)
            {
                ViewBag.SubjectsMenu = "class = active";
                ViewBag.GradeLink = "color: #846c6c";

                List<Grades> list = manager.ListOfGrades();
                return View(list);
            }

            else
                return RedirectToAction("UnAuthorizeAccess", "Home", new { area = "" });
        }

        public ActionResult CreateGrade()
        {
            if (Config.CurrentUser == 0)
                return RedirectToAction("Login", "Account", new { area = "" });

            if (Config.User.UserType == (int)Enumerations.UserType.Super
                  || Config.User.UserType == (int)Enumerations.UserType.Admin)
            {
                var model = new GradesViewModel();
                model.ListOfCurriculums = manager.dListOfCurriculums();
                return View(model);
            }

            else
                return RedirectToAction("UnAuthorizeAccess", "Home", new { area = "" });
           
        }

        [HttpPost]
        public ActionResult CreateGrade(GradesViewModel model)
        {
            if (Config.CurrentUser == 0)
                return RedirectToAction("Login", "Account", new { area = "" });

            if (Config.User.UserType == (int)Enumerations.UserType.Super
                   || Config.User.UserType == (int)Enumerations.UserType.Admin)
            {
                if (ModelState.IsValid)
                {

                    Int64 GradeID = manager.AddGrade(model);
                    if (GradeID > 0)
                    {
                        TempData["Message"] = "Add succesfully";
                        TempData["Status"] = 1;
                        return RedirectToAction("GradesLists");
                    }
                    else
                    {
                        ViewBag.Message = "Something went wrong!";
                        TempData["Status"] = 2;
                        ModelState.AddModelError("", "Something went wrong!");
                    }
                }
                else
                {
                    ViewBag.Message = "Something went wrong!";
                    TempData["Status"] = 2;
                    ModelState.AddModelError("", "Please recheck your feilds");
                }
                return View(model);
            }

            else
                return RedirectToAction("UnAuthorizeAccess", "Home", new { area = "" });
        }

        public ActionResult SubjectsLists()
        {
            if (Config.CurrentUser == 0)
                return RedirectToAction("Login", "Account", new { area = "" });

            if (Config.User.UserType == (int)Enumerations.UserType.Super
                    || Config.User.UserType == (int)Enumerations.UserType.Admin)
            {
                ViewBag.SubjectsMenu = "class = active";
                ViewBag.SubjectsLink = "color: #846c6c";

                List<Subjects> list = manager.ListOfSubjects();
                return View(list);
            }

            else
                return RedirectToAction("UnAuthorizeAccess", "Home", new { area = "" });
        }

        public ActionResult CreateSubject()
        {
            if (Config.CurrentUser == 0)
                return RedirectToAction("Login", "Account", new { area = "" });

            if (Config.User.UserType == (int)Enumerations.UserType.Super
                       || Config.User.UserType == (int)Enumerations.UserType.Admin)
            {
                var model = new SubjectViewModel();
                model.ListOfCurriculums = manager.dListOfCurriculums();
                model.ListOfGrades = Enumerable.Empty<SelectListItem>();
                return View(model);
            }

            else
                return RedirectToAction("UnAuthorizeAccess", "Home", new { area = "" });
        }

        [HttpPost]
        public ActionResult CreateSubject(SubjectViewModel model)
        {
            if (Config.CurrentUser == 0)
                return RedirectToAction("Login", "Account", new { area = "" });

            if (Config.User.UserType == (int)Enumerations.UserType.Super
                     || Config.User.UserType == (int)Enumerations.UserType.Admin)
            {
                if (ModelState.IsValid)
                {

                    Int64 SubjectID = manager.AddSubject(model);
                    if (SubjectID > 0)
                    {
                        TempData["Message"] = "Add succesfully";
                        TempData["Status"] = 1;
                        //CoursesHeader course = new CoursesHeader();
                        //course.CourseTitle = model.SubjectName;
                        //course.CourseDescription = model.SubjectDescription;
                        //course.FKCurriculumID = model.CurriculumID;
                        //course.FKGradeID = model.GradeID;
                        //course.Subject
                        return RedirectToAction("SubjectsLists");
                    }
                    else
                    {
                        ViewBag.Message = "Something went wrong!";
                        TempData["Status"] = 2;
                        ModelState.AddModelError("", "Something went wrong!");
                    }
                }
                else
                {
                    ViewBag.Message = "Something went wrong!";
                    TempData["Status"] = 2;
                    ModelState.AddModelError("", "Please recheck your feilds");
                }
                return View(model);
            }

            else
                return RedirectToAction("UnAuthorizeAccess", "Home", new { area = "" });

        }

        public ActionResult TopicsList(Int64 SubjectID)
        {
            if (Config.CurrentUser == 0)
                return RedirectToAction("Login", "Account", new { area = "" });

            if (Config.User.UserType != (int)Enumerations.UserType.Super)
                return RedirectToAction("UnAuthorizeAccess", "Home", new { area = "" });

            Int64 ids = Convert.ToInt64((TempData["SubjectID"] != null) ? (Int64)TempData["SubjectID"] : 0);
            if (ids > 0)
            {
                ViewBag.SubjectID = ids;
                SubjectID = ids;
            }
            else
            {
                ViewBag.SubjectID = SubjectID;
            }

            List<Topics> list = manager.ListOfTopics(SubjectID);                      
            return View(list);

        }

        public ActionResult CreateTopic(Int64 SubjectID)
        {
            if (Config.CurrentUser == 0)
                return RedirectToAction("Login", "Account", new { area = "" });

            if (Config.User.UserType != (int)Enumerations.UserType.Super)
                return RedirectToAction("UnAuthorizeAccess", "Home", new { area = "" });


            var model = new TopicsViewModel();
            //model.ListOfCurriculums = manager.dListOfCurriculums();
            //model.ListOfGrades = Enumerable.Empty<SelectListItem>();
            //model.ListOfSubjects = Enumerable.Empty<SelectListItem>();

            model.SubjectID = SubjectID;
            return View(model);
        }

        [HttpPost]
        public ActionResult CreateTopic(TopicsViewModel model)
        {
            if (Config.CurrentUser == 0)
                return RedirectToAction("Login", "Account", new { area = "" });

            if (Config.User.UserType != (int)Enumerations.UserType.Super)
                return RedirectToAction("UnAuthorizeAccess", "Home", new { area = "" });

            if (ModelState.IsValid)
            {

                Subjects obj = manager.GetSubject(model.SubjectID);
                model.CurriculumID = obj.FKCurriculumID;
                model.GradeID = obj.FKGradeID;
                Int64 TopicID = manager.AddTopic(model);

                if (TopicID > 0)
                {
                    TempData["SubjectID"] = model.SubjectID;
                    TempData["Message"] = "Add succesfully";
                    TempData["Status"] = 1;
                    return RedirectToAction("TopicsList", new { @SubjectID=@model.SubjectID});
                }
                else
                {

                    ViewBag.Message = "Something went wrong!";
                    TempData["Status"] = 2;
                    ModelState.AddModelError("", "Something went wrong!");
                }
            }
            else
            {
                ViewBag.Message = "Something went wrong!";
                TempData["Status"] = 2;
                ModelState.AddModelError("", "Please recheck your feilds");
            }
            return View(model);
        }

        public ActionResult SubjectFAQList(Int64 SubjectID)
        {
            if (Config.CurrentUser == 0)
                return RedirectToAction("Login", "Account", new { area = "" });

            if (Config.User.UserType != (int)Enumerations.UserType.Super)
                return RedirectToAction("UnAuthorizeAccess", "Home", new { area = "" });


            Int64 ids = Convert.ToInt64((TempData["SubjectID"] != null) ? (Int64)TempData["SubjectID"] : 0);
            if (ids > 0)
            {
                ViewBag.SubjectID = ids;
                SubjectID = ids;
            }
            else
            {
                ViewBag.SubjectID = SubjectID;
            }
            List<SubjectFAQS> list = manager.ListOfSubjectFAQ(SubjectID);
            return View(list);
        }

        public ActionResult CreateSubjectFaq(Int64 SubjectID)
        {
            if (Config.CurrentUser == 0)
                return RedirectToAction("Login", "Account", new { area = "" });

            if (Config.User.UserType != (int)Enumerations.UserType.Super)
                return RedirectToAction("UnAuthorizeAccess", "Home", new { area = "" });

            var model = new SubjectFAQViewModel();
            model.SubjectID = SubjectID;
            return View(model);
        }

        [HttpPost]
        public ActionResult CreateSubjectFaq(SubjectFAQViewModel model)
        {
            if (Config.CurrentUser == 0)
                return RedirectToAction("Login", "Account", new { area = "" });

            if (Config.User.UserType != (int)Enumerations.UserType.Super)
                return RedirectToAction("UnAuthorizeAccess", "Home", new { area = "" });

            if (ModelState.IsValid)
            {
                Subjects obj = manager.GetSubject(model.SubjectID);
                model.QuestionBy = Config.CurrentUser;
              
                Int64 SubjectID = manager.AddSubjectFAQ(model);
                if (SubjectID > 0)
                {
                    TempData["Message"] = "Add succesfully";
                    TempData["Status"] = 1;
                    TempData["SubjectID"] = model.SubjectID;
                    return RedirectToAction("SubjectFAQList", new { @SubjectID = @model.SubjectID });
               
                }
                else
                {
                    ViewBag.Message = "Something went wrong!";
                    TempData["Status"] = 2;
                    ModelState.AddModelError("", "Something went wrong!");
                }
            }
            else
            {
                ViewBag.Message = "Something went wrong!";
                TempData["Status"] = 2;
                ModelState.AddModelError("", "Please recheck your feilds");
            }
            return View(model);

        }


        public ActionResult QuestionsBank()
        {
            if (Config.CurrentUser == 0)
                return RedirectToAction("Login", "Account", new { area = "" });

            if (Config.User.UserType == (int)Enumerations.UserType.Super
                || Config.User.UserType == (int)Enumerations.UserType.Admin)
            {
                ViewBag.SubjectsMenu = "class = active";
                ViewBag.QuestionLink = "color: #846c6c";

                List<QuestionsBank> list = new List<QuestionsBank>();
                if(manager!=null && manager.ListOfQuestionsBank()!=null)
                    list = manager.ListOfQuestionsBank();

                return View(list);
            }

            else
                return RedirectToAction("UnAuthorizeAccess", "Home", new { area = "" });
        }

        public ActionResult CreateQuestionsBank()
        {
            if (Config.CurrentUser == 0)
                return RedirectToAction("Login", "Account", new { area = "" });

            if (Config.User.UserType == (int)Enumerations.UserType.Super
               || Config.User.UserType == (int)Enumerations.UserType.Admin)
            {
                QuestionsBankViewModel model = new QuestionsBankViewModel();
                model.ListOfCurriculums = manager.dListOfCurriculums();
                model.ListOfGrades = Enumerable.Empty<SelectListItem>();
                model.ListOfSubjects = Enumerable.Empty<SelectListItem>();
                model.ListOfAnswers = manager.ListOfAnswers();
                model.ListOfQuestionLevel = manager.ListOfQuestionsLevel();

                return View(model);
            }

            else
                return RedirectToAction("UnAuthorizeAccess", "Home", new { area = "" });
        }

        [HttpPost]
        public ActionResult CreateQuestionsBank(QuestionsBankViewModel model)
        {
            if (Config.CurrentUser == 0)
                return RedirectToAction("Login", "Account", new { area = "" });

            //if (Config.User.UserType != (int)Enumerations.UserType.Super)
            //    return RedirectToAction("UnAuthorizeAccess", "Home", new { area = "" });

            if (ModelState.IsValid)
            {
            
                Int64 SubjectID = manager.AddQuestionsBank(model);
                if (SubjectID > 0)
                {
                    TempData["Message"] = "Add succesfully";
                    TempData["Status"] = 1;
                    return RedirectToAction("QuestionsBank");
                }
                else
                {
                    model.ListOfCurriculums = manager.dListOfCurriculums();
                    model.ListOfGrades = manager.dListOfGrades(model.CurriculumID);
                    model.ListOfSubjects = manager.dListOfSubjects(model.GradeID);
                    model.ListOfTopics = manager.dListOfTopics(model.SubjectID);
                    model.ListOfAnswers = manager.ListOfAnswers();
                    model.ListOfQuestionLevel = manager.ListOfQuestionsLevel();
                    ViewBag.Message = "Something went wrong!";
                    TempData["Status"] = 2;
                    ModelState.AddModelError("", "Something went wrong!");
                }
            }
            else
            {
                model.ListOfCurriculums = manager.dListOfCurriculums();
                model.ListOfGrades = manager.dListOfGrades(model.CurriculumID);
                model.ListOfSubjects = manager.dListOfSubjects(model.GradeID);
                model.ListOfTopics = manager.dListOfTopics(model.SubjectID);
                model.ListOfAnswers = manager.ListOfAnswers();
                model.ListOfQuestionLevel = manager.ListOfQuestionsLevel();
                ViewBag.Message = "Something went wrong!";
                TempData["Status"] = 2;
                ModelState.AddModelError("", "Please recheck your feilds");
            }
            return View(model);
        }


        [AllowAnonymous]
        public JsonResult GradesList(Int64 CurriculumID)
        {

            if (CurriculumID > 0)
            {
                IEnumerable<SelectListItem> ListOfGrades = manager.dListOfGrades(CurriculumID);
                return Json(
                     ListOfGrades, JsonRequestBehavior.AllowGet
                );
            }
            return Json(
                null,
                JsonRequestBehavior.AllowGet
            );
        }

        [AllowAnonymous]
        public JsonResult SubjectsList(Int64 GradeID)
        {

            if (GradeID > 0)
            {
                IEnumerable<SelectListItem> ListOfGrades = manager.dListOfSubjects(GradeID);
                return Json(
                     ListOfGrades, JsonRequestBehavior.AllowGet
                );
            }
            return Json(
                null,
                JsonRequestBehavior.AllowGet
            );
        }

        [AllowAnonymous]
        public JsonResult TopicsListDrop(Int64 SubjectID)
        {

            if (SubjectID > 0)
            {
                IEnumerable<SelectListItem> ListOfTopics = manager.dListOfTopics(SubjectID);
                return Json(
                     ListOfTopics, JsonRequestBehavior.AllowGet
                );
            }
            return Json(
                null,
                JsonRequestBehavior.AllowGet
            );
        }

    }
}