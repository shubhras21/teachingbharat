﻿using BLL.BusinessManagers;
using BLL.BusinessModels;
using BLL.Helpers;
using DAL.MasterEntity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Tutoring.Areas.Admin.Controllers
{
    public class SettingController : Controller
    {
        MyDbContext db = new MyDbContext();
        // GET: Admin/Setting
        public ActionResult UpdateSettings()
        {
            if (Config.CurrentUser == 0)
                return RedirectToAction("Login", "Account", new { area = "" });

            if (Config.User.UserType == (int)Enumerations.UserType.Super
               || Config.User.UserType == (int)Enumerations.UserType.Admin)
            {
                var update = db.MetaSettings.OrderByDescending(p => p.MetaID).FirstOrDefault();
                SettingsModel model = new SettingsModel();
                model.Description = update.Description;
                model.IsEnabled = update.IsEnabled;
                model.KeyWords = update.KeyWords;
                model.MetaID = update.MetaID;
                model.Title = update.Title;
                return View(model);
            }

            else
                return RedirectToAction("UnAuthorizeAccess", "Home", new { area = "" });
        }
        [HttpPost]
        public ActionResult UpdateSettings(SettingsModel model)
        {
            if (Config.CurrentUser == 0)
                return RedirectToAction("Login", "Account", new { area = "" });

            if (Config.User.UserType == (int)Enumerations.UserType.Super
               || Config.User.UserType == (int)Enumerations.UserType.Admin)
            {
                var update = db.MetaSettings.OrderByDescending(p => p.MetaID).FirstOrDefault();
                if (model.Description != "".Trim())
                {
                    update.Description = model.Description;
                }
                
                var val = Request.Form["optradio"];
                if (val == "Enable")
                {
                    update.IsEnabled = true;
                }
                else
                {
                    update.IsEnabled = false;
                }
                if (model.KeyWords != "".Trim())
                {
                    update.KeyWords = model.KeyWords;
                }
                if (model.Title != "".Trim())
                {
                    update.Title = model.Title;
                }
                db.SaveChanges();
                return RedirectToAction("Index", "Admin");
            }

            else
                return RedirectToAction("UnAuthorizeAccess", "Home", new { area = "" });


        }

    }
}