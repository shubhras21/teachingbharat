﻿using BLL.BusinessManagers;
using BLL.Helpers;
using DAL.MasterEntity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Tutoring.Areas.Admin.Controllers
{
    public class NotificationController : Controller
    {
        MyDbContext context = new MyDbContext();
        // GET: Admin/Notification
        public ActionResult Index()
        {
            if (Config.CurrentUser == 0)
                return RedirectToAction("Login", "Account", new { area = "" });

            if (Config.User.UserType == (int)Enumerations.UserType.Super
               || Config.User.UserType == (int)Enumerations.UserType.Admin)
            {

                var bannerslides = context.Notifications.ToList();
                int count = bannerslides.Count;
                var update = context.Notifications.Where(p => p.IsSeen != true).ToList();
                foreach (var item in update)
                {
                    item.IsSeen = true;
                }
                context.SaveChanges();
                return View(bannerslides);

            }

            else
                return RedirectToAction("UnAuthorizeAccess", "Home", new { area = "" });
            
        }
        [ChildActionOnly]
        public ActionResult Counter()
        {
            if (Config.CurrentUser == 0)
                return RedirectToAction("Login", "Account", new { area = "" });

            if (Config.User.UserType == (int)Enumerations.UserType.Super
               || Config.User.UserType == (int)Enumerations.UserType.Admin)
            {

                var bannerslides = NotificationManager.GetAllSlides();
                return View(bannerslides);

            }

            else
                return RedirectToAction("UnAuthorizeAccess", "Home", new { area = "" });

        }
        [ChildActionOnly]
        public ActionResult ShowList()
        {
            if (Config.CurrentUser == 0)
                return RedirectToAction("Login", "Account", new { area = "" });

            if (Config.User.UserType == (int)Enumerations.UserType.Super
               || Config.User.UserType == (int)Enumerations.UserType.Admin)
            {

                var bannerslides = NotificationManager.GetAllSlides();
                return View(bannerslides);

            }

            else
                return RedirectToAction("UnAuthorizeAccess", "Home", new { area = "" });

        }
    }
}