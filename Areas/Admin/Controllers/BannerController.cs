﻿using BLL.BusinessManagers;
using BLL.BusinessModels;
using BLL.Helpers;
using DAL.Entities;
using DAL.MasterEntity;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Tutoring.Areas.Admin.Controllers
{
    public class BannerController : Controller
    {
        MyDbContext db = new MyDbContext();

        // GET: Admin/Banner
        public ActionResult Index()
        {
            if (Config.CurrentUser == 0)
                return RedirectToAction("Login", "Account", new { area = "" });

            if (Config.User.UserType == (int)Enumerations.UserType.Super
               || Config.User.UserType == (int)Enumerations.UserType.Admin)
            {

                var bannerslides = BannerManager.GetAllSlides();
                return View(bannerslides);

            }

            else
                return RedirectToAction("UnAuthorizeAccess", "Home", new { area = "" });
            
        }

        public ActionResult AddSlide()
        {
            if (Config.CurrentUser == 0)
                return RedirectToAction("Login", "Account", new { area = "" });

            if (Config.User.UserType == (int)Enumerations.UserType.Super
               || Config.User.UserType == (int)Enumerations.UserType.Admin)
            {
                return View();
            }

            else
                return RedirectToAction("UnAuthorizeAccess", "Home", new { area = "" });
        }


        [HttpPost]
        public ActionResult AddSlide(BannerModel model)
        {
            if (Config.CurrentUser == 0)
                return RedirectToAction("Login", "Account", new { area = "" });

            if (Config.User.UserType == (int)Enumerations.UserType.Super
               || Config.User.UserType == (int)Enumerations.UserType.Admin)
            {
                var newlyCreatedBannerSlide = BannerManager.AddnewSlide(model);

                if (model.image != null)
                {
                    string pic = System.IO.Path.GetFileName(model.image.FileName);
                    string path = System.IO.Path.Combine(
                                           Server.MapPath("~/Content/BannerSlides"), model.ID.ToString() + ".jpg");

                    model.image.SaveAs(path);
                }

                return RedirectToAction("Index", "Banner");
            }

            else
                return RedirectToAction("UnAuthorizeAccess", "Home", new { area = "" });

            
        }
        public ActionResult UpdateStatus(bool check, int customerId)
        {
            if (Config.CurrentUser == 0)
                return RedirectToAction("Login", "Account", new { area = "" });

            if (Config.User.UserType == (int)Enumerations.UserType.Super
               || Config.User.UserType == (int)Enumerations.UserType.Admin)
            {
                var update = db.BannerSlides.Where(p => p.BannerSlidesID == customerId).FirstOrDefault();
                update.Status = check;
                db.SaveChanges();
                var bannerslides = BannerManager.GetAllSlides();

                return View(bannerslides);
            }

            else
                return RedirectToAction("UnAuthorizeAccess", "Home", new { area = "" });
            
        }

        public ActionResult UpdateSlide(int? slideID, string UserID)
        {
            if (Config.CurrentUser == 0)
                return RedirectToAction("Login", "Account", new { area = "" });

            if (Config.User.UserType == (int)Enumerations.UserType.Super
               || Config.User.UserType == (int)Enumerations.UserType.Admin)
            {
                string mm = UserID;
                var bannermodel = BannerManager.GetBannerSlideByID((int)slideID);

                return View(bannermodel);
            }

            else
                return RedirectToAction("UnAuthorizeAccess", "Home", new { area = "" });
            
        }

        [HttpPost]
        public ActionResult UpdateSlide(BannerModel model)
        {
            if (Config.CurrentUser == 0)
                return RedirectToAction("Login", "Account", new { area = "" });

            if (Config.User.UserType == (int)Enumerations.UserType.Super
               || Config.User.UserType == (int)Enumerations.UserType.Admin)
            {
                if (model != null && model.ID > 0)
                {
                    bool status = model.Status;
                    if (model.image != null)
                    {
                        var image = Server.MapPath("~/Content/BannerSlides/" + model.ID.ToString() + ".jpg");

                        if (System.IO.File.Exists(image))
                        {
                            System.IO.File.Delete(image);
                        }

                        string pic = System.IO.Path.GetFileName(model.image.FileName);
                        string path = System.IO.Path.Combine(
                                               Server.MapPath("~/Content/BannerSlides"), model.ID.ToString() + ".jpg");

                        model.image.SaveAs(path);

                    }

                    BannerManager.UpdateBannerSlide(model);
                }


                return RedirectToAction("Index", "Banner");
            }

            else
                return RedirectToAction("UnAuthorizeAccess", "Home", new { area = "" });
            
        }

        public ActionResult DeleteSlide(int slideID)
        {
            if (Config.CurrentUser == 0)
                return RedirectToAction("Login", "Account", new { area = "" });

            if (Config.User.UserType == (int)Enumerations.UserType.Super
               || Config.User.UserType == (int)Enumerations.UserType.Admin)
            {
                if (slideID > 0)
                {
                    var response = BannerManager.DeleteBannerSlide(slideID);
                    if (response == (int)Enumerations.ResponsHelpers.Deleted)
                    {
                        var image = Server.MapPath("~/Content/BannerSlides/" + slideID.ToString() + ".jpg");

                        if (System.IO.File.Exists(image))
                        {
                            System.IO.File.Delete(image);
                        }
                    }
                }

                return RedirectToAction("Index", "Banner");
            }

            else
                return RedirectToAction("UnAuthorizeAccess", "Home", new { area = "" });

            
        }
    }
}