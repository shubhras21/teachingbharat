﻿using BLL.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Tutoring.Areas.Admin.Controllers
{
    public class WithDrawsController : Controller
    {
        // GET: Admin/WithDraws
        public ActionResult Index()
        {
            if (Config.CurrentUser == 0)
                return RedirectToAction("Login", "Account", new { area = "" });

            if (Config.User.UserType == (int)Enumerations.UserType.Super
               || Config.User.UserType == (int)Enumerations.UserType.Admin)
            {
                return View();
            }

            else
                return RedirectToAction("UnAuthorizeAccess", "Home", new { area = "" });
        }
    }
}