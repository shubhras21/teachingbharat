﻿
using BLL.BusinessManagers;
using BLL.BusinessManagers.AdminManager;
using BLL.BusinessManagers.SessionsManager;
using BLL.BusinessManagers.StudentManager;
using BLL.BusinessManagers.TeacherManager;
using BLL.BusinessManagers.UserManager;
using BLL.BusinessModels.AdminModels;
using BLL.BusinessModels.TeacherModels;
using BLL.Helpers;
using DAL.Entities;
using DAL.MasterEntity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using static BLL.BusinessManagers.AdminManager.AdminManager;

namespace Tutoring.Areas.Admin.Controllers
{
    public class TeacherController : Controller
    {
        TeacherManager TeacherManager = new TeacherManager();
        UserActivities obj_Activity = null;
        UsersActivityManager ActivityMgr = new UsersActivityManager();
        MyDbContext context = new MyDbContext();

        // GET: Admin/Teacher
        public ActionResult Index()
        {
            if (Config.CurrentUser == 0)
                return RedirectToAction("Login", "Account", new { area = "" });

            if (Config.User.UserType == (int)Enumerations.UserType.Super
              || Config.User.UserType == (int)Enumerations.UserType.Admin)
            {
                return View();
            }

            else
                return RedirectToAction("UnAuthorizeAccess", "Home", new { area = "" });
        }

        public ActionResult ViewTeachers(int? flag)
        {
            if (Config.CurrentUser == 0)
                return RedirectToAction("Login", "Account", new { area = "" });

            if (Config.User.UserType == (int)Enumerations.UserType.Super
                || Config.User.UserType == (int)Enumerations.UserType.Admin)
            {
                var update = context.Notifications.Where(p => p.IsSeen != true).ToList();
                foreach (var item in update)
                {
                    long id = Convert.ToInt64(item.UserID);
                    var user = context.Users.Where(p => p.UserID == id).FirstOrDefault();
                    if (user.UserType == 3)
                    {
                        item.IsSeen = true;
                    }
                    
                }
                context.SaveChanges();
                ViewBag.TeachersMenu = "class = active";

            TeacherManager teacherMgr = new TeacherManager();
            AdminManager adminManager = new AdminManager();
            StudentManager studentMgr = new StudentManager();
            BLL.BusinessModels.TeacherModels.TeacherViewModel teacherModel = new BLL.BusinessModels.TeacherModels.TeacherViewModel();
          
            var teachersList = new List<BLL.BusinessModels.TeacherModels.TeacherViewModel>();

           
                if(flag == 2)
                {
                    ViewBag.totalTeachersRegistered = teacherMgr.GetTotalNoOfTeachersCount();
                    ViewBag.teachersRegisteredThisMonth = teacherMgr.GetTeachersRegisteredThisMonthCount();
                    ViewBag.teachersRegisteredToday = teacherMgr.GetTeachersRegisteredTodayCount();

                    if (Config.User.UserType == (int)Enumerations.UserType.Admin)
                    {
                        var currentUserPermissions = adminManager.GetPermissionIDsOfUserbyUserID(Config.CurrentUser);

                        ViewBag.currentUserPermissions = currentUserPermissions;

                    }

                    var studentsIds = TempData["studentIds"] as List<long>;
                    foreach (var item in studentsIds)
                    {
                        var userModel = teacherMgr.GetTeacherByUserID(item);
                        UserManager.ConvertUserViewModelToTeacherViewModel(userModel , ref teacherModel);
                        teachersList.Add(teacherModel);
                    }
                    return View(teachersList);
                }

                if(flag == 3)
                {
                    ViewBag.totalTeachersRegistered = teacherMgr.GetTotalNoOfTeachersCount();
                    ViewBag.teachersRegisteredThisMonth = teacherMgr.GetTeachersRegisteredThisMonthCount();
                    ViewBag.teachersRegisteredToday = teacherMgr.GetTeachersRegisteredTodayCount();
                    if (Config.User.UserType == (int)Enumerations.UserType.Admin)
                    {
                        var currentUserPermissions = adminManager.GetPermissionIDsOfUserbyUserID(Config.CurrentUser);
                        ViewBag.currentUserPermissions = currentUserPermissions;
                    }
                    var studentsIds = TempData["studentIds"] as List<long>;
                    foreach (var item in studentsIds)
                    {
                        var userModel = teacherMgr.GetTeacherByUserID(item);
                        UserManager.ConvertUserViewModelToTeacherViewModel(userModel, ref teacherModel);
                        teachersList.Add(teacherModel);
                    }
                    return View(teachersList);
                }

                if(flag == 4)
                {
                    ViewBag.totalTeachersRegistered = teacherMgr.GetTotalNoOfTeachersCount();
                    ViewBag.teachersRegisteredThisMonth = teacherMgr.GetTeachersRegisteredThisMonthCount();
                    ViewBag.teachersRegisteredToday = teacherMgr.GetTeachersRegisteredTodayCount();
                    if (Config.User.UserType == (int)Enumerations.UserType.Admin)
                    {
                        var currentUserPermissions = adminManager.GetPermissionIDsOfUserbyUserID(Config.CurrentUser);
                        ViewBag.currentUserPermissions = currentUserPermissions;
                    }
                    var studentsIds = TempData["studentIds"] as List<long>;
                    foreach (var item in studentsIds)
                    {
                        var userModel = teacherMgr.GetTeacherByUserID(item);
                        UserManager.ConvertUserViewModelToTeacherViewModel(userModel, ref teacherModel);
                        teachersList.Add(teacherModel);
                    }
                    return View(teachersList);
                }



                ViewBag.totalTeachersRegistered = teacherMgr.GetTotalNoOfTeachersCount();
                ViewBag.teachersRegisteredThisMonth = teacherMgr.GetTeachersRegisteredThisMonthCount();
                ViewBag.teachersRegisteredToday = teacherMgr.GetTeachersRegisteredTodayCount();

                var model = teacherMgr.ListofTeachers();

                if (Config.User.UserType == (int)Enumerations.UserType.Admin)
                {
                    var currentUserPermissions = adminManager.GetPermissionIDsOfUserbyUserID(Config.CurrentUser);

                    ViewBag.currentUserPermissions = currentUserPermissions;

                }

                return View(model);
            }
            else
            {
                return RedirectToAction("UnAuthorizeAccess", "Home", new { area = "" });
            }
        }

        public ActionResult BlockTeacher(Int64 userid)
        {
            if (Config.CurrentUser == 0)
                return RedirectToAction("Login", "Account", new { area = "" });

            if (Config.User.UserType == (int)Enumerations.UserType.Super
               || Config.User.UserType == (int)Enumerations.UserType.Admin)
            {

                var result = TeacherManager.BlockUser(userid);
                if (result == (int)Enumerations.ResponsHelpers.Block)
                {
                    ViewBag.Message = result;
                    
                }
                   
                return RedirectToAction("ViewTeachers");
            }

            else
                return RedirectToAction("UnAuthorizeAccess", "Home", new { area = "" });
        }

        public ActionResult ApproveTeacher(Int64 userid)
        {
            if (Config.CurrentUser == 0)
                return RedirectToAction("Login", "Account", new { area = "" });

            if (Config.User.UserType == (int)Enumerations.UserType.Super
                || Config.User.UserType == (int)Enumerations.UserType.Admin)
            {
                var result = TeacherManager.ActivateUser(userid);
                if (result == (int)Enumerations.ResponsHelpers.Block)
                    ViewBag.Message = result;

                obj_Activity = new UserActivities();
                obj_Activity.ActionPerformed = "Teacher got Appproved";
                obj_Activity.FKUserID = userid;
                obj_Activity.ActivitySource = (int)Enumerations.ActivitySource.BacksideActivity;
                ActivityMgr.AddActivity(obj_Activity);

                return RedirectToAction("ViewTeachers");
            }

            else
                return RedirectToAction("UnAuthorizeAccess", "Home", new { area = "" });

        }

        public ActionResult ViewDetails(int userid)
        {
            if (Config.CurrentUser == 0)
                return RedirectToAction("Login", "Account", new { area = "" });

            if (Config.User.UserType == (int)Enumerations.UserType.Super
               || Config.User.UserType == (int)Enumerations.UserType.Admin)
            {
                TeacherQualificationModel teacherModel = new TeacherQualificationModel();
                TeacherManager teacherManager = new TeacherManager();
                var usermodel = teacherManager.GetTeacherByUserID(userid);
                usermodel.teacherQualification = teacherManager.GetTeacherQualificationByUserID(userid);

                var teacherActivity = teacherManager.GetTeacherCompleteActivities(usermodel);
                usermodel.teacherCoursesList = TeacherManager.GetTeacherCourses(userid);

                return View(usermodel);
            }
            else
                return RedirectToAction("UnAuthorizeAccess", "Home", new { area = "" });
            
        }

        public ActionResult DeleteTeacher(int userid)
        {
            if (Config.CurrentUser == 0)
                return RedirectToAction("Login", "Account", new { area = "" });

            if (Config.User.UserType == (int)Enumerations.UserType.Super
               || Config.User.UserType == (int)Enumerations.UserType.Admin)
            {
                TeacherManager teacherManager = new TeacherManager();

                var username = UserManager.GetUserNameByID(userid);
                var status = teacherManager.DeleteTeacherByUserID(userid);
                if (status == (int)Enumerations.ResponsHelpers.Deleted)
                {
                    obj_Activity = new UserActivities();
                    obj_Activity.ActionPerformed = "Teacher with the name " + username + " has been Deleted";
                    obj_Activity.FKUserID = userid;
                    obj_Activity.ActivitySource = (int)Enumerations.ActivitySource.BacksideActivity;
                    ActivityMgr.AddActivity(obj_Activity);
                }

                return RedirectToAction("ViewTeachers", "Teacher");
            }
            else
                return RedirectToAction("UnAuthorizeAccess", "Home", new { area = "" });

            
        }

        public ActionResult GetTeacher(int userid)
        {
            if (Config.CurrentUser == 0)
                return RedirectToAction("Login", "Account", new { area = "" });

            if (Config.User.UserType == (int)Enumerations.UserType.Super
               || Config.User.UserType == (int)Enumerations.UserType.Admin)
            {
                TeacherManager teacherMgr = new TeacherManager();
                var teacherModel = teacherMgr.GetTeacherByUserID(userid);

                return View(teacherModel);
            }
            else
                return RedirectToAction("UnAuthorizeAccess", "Home", new { area = "" });

           
        }

        [HttpPost]
        public ActionResult UpdateTeacher (UserViewModel model)
        {
            if (Config.CurrentUser == 0)
                return RedirectToAction("Login", "Account", new { area = "" });

            if (Config.User.UserType == (int)Enumerations.UserType.Super
               || Config.User.UserType == (int)Enumerations.UserType.Admin)
            {
                TeacherManager teacherMgr = new TeacherManager();
                var status = teacherMgr.UpdateTeacher(model);
                if (status == (int)Enumerations.ResponsHelpers.Updated)
                {
                    obj_Activity = new UserActivities();
                    obj_Activity.ActionPerformed = "Teacher Updated";
                    obj_Activity.FKUserID = model.UserID;
                    obj_Activity.ActivitySource = (int)Enumerations.ActivitySource.BacksideActivity;
                    ActivityMgr.AddActivity(obj_Activity);
                }

                return RedirectToAction("ViewTeachers", "Teacher");
            }
            else
                return RedirectToAction("UnAuthorizeAccess", "Home", new { area = "" });

            
        }

        [HttpPost]
        public ActionResult ApplyFilter(string mainfilterRequest, string subfilterRequest)
        {
            if (Config.CurrentUser == 0)
                return RedirectToAction("Login", "Account", new { area = "" });

            if (Config.User.UserType == (int)Enumerations.UserType.Super
               || Config.User.UserType == (int)Enumerations.UserType.Admin)
            {
                TempData["mainfilterValue"] = null;
                TempData["subfilterValue"] = null;
                TempData["studentIds"] = null;

                SessionManager sessionMgr = new SessionManager();
                StudentManager studentMgr = new StudentManager();
                TeacherManager teacherMgr = new TeacherManager();


                if (mainfilterRequest == "FilterByGrade")
                {

                    var teachersList = teacherMgr.GetTeachersByGradeID(int.Parse(subfilterRequest));

                    TempData["studentIds"] = teachersList;
                    TempData["mainfilterValue"] = mainfilterRequest;
                    TempData["subfilterValue"] = subfilterRequest;

                    return RedirectToAction("ViewTeachers", new { flag = 2 });

                }
                if (mainfilterRequest == "FilterBySubject")
                {
                    var teachersList = teacherMgr.GetTeachersBySubjectID(int.Parse(subfilterRequest));

                    TempData["studentIds"] = teachersList;
                    TempData["mainfilterValue"] = mainfilterRequest;
                    TempData["subfilterValue"] = subfilterRequest;

                    return RedirectToAction("ViewTeachers", new { flag = 3 });

                }
                if (mainfilterRequest == "FilterByCity")
                {
                    var teachersList = teacherMgr.GetTeachersByCity(subfilterRequest);

                    TempData["studentIds"] = teachersList;
                    TempData["mainfilterValue"] = mainfilterRequest;
                    TempData["subfilterValue"] = subfilterRequest;

                    return RedirectToAction("ViewTeachers", new { flag = 4 });
                }
                if (mainfilterRequest == "FilterByStatus")
                {
                    var teachersList = teacherMgr.GetTeachersByStatus(int.Parse(subfilterRequest));

                    TempData["studentIds"] = teachersList;
                    TempData["mainfilterValue"] = mainfilterRequest;
                    TempData["subfilterValue"] = subfilterRequest;

                    return RedirectToAction("ViewTeachers", new { flag = 4 });
                }
                return View();
            }
            else
                return RedirectToAction("UnAuthorizeAccess", "Home", new { area = "" });
            
        }


        public ActionResult FilterMethod(string request)
        {
            if (Config.CurrentUser == 0)
                return RedirectToAction("Login", "Account", new { area = "" });

            if (Config.User.UserType == (int)Enumerations.UserType.Super
               || Config.User.UserType == (int)Enumerations.UserType.Admin)
            {
                TeacherManager teacherMgr = new TeacherManager();
                StudentManager studentMgr = new StudentManager();
                UserViewModel userModel = new UserViewModel();


                if (request == "FilterByGrade")
                {
                    // it will give all grades
                    var gradesList = studentMgr.GetGradesList();
                    var gradesListItems = userModel.GetGradesList(gradesList);
                    return Json(new { Success = "FilterByGrade", gradesList = gradesListItems });
                }
                if (request == "FilterBySubject")
                {
                    var subjectsList = studentMgr.GetSubjectsList();
                    var subjectsListItems = GenerateSubjectsDropdown(subjectsList);
                    return Json(new { Success = "FilterBySubject", subjectsList = subjectsListItems });

                }
                if (request == "FilterByCity")
                {
                    var citiesList = studentMgr.GetAllCities();
                    var citiesListItems = GenerateCitiesDropdown(citiesList);
                    return Json(new { Success = "FilterByCity", citiesList = citiesListItems });

                }

                return Json(new { Success = "true" });
            }
            else
                return RedirectToAction("UnAuthorizeAccess", "Home", new { area = "" });

            

        }

        public List<SelectListItem> GenerateSubjectsDropdown(List<Subjects> subjectsList)
        {
            StudentManager stdMgr = new StudentManager();

            List<SelectListItem> ListOfsubjcts = new List<SelectListItem>();
            ListOfsubjcts.Add(new SelectListItem { Value = "", Text = "Select Subject", Selected = false });
            foreach (var s in subjectsList)
            {
                var gradeModel = stdMgr.GetGradeByGradeID(s.FKGradeID);
                ListOfsubjcts.Add(new SelectListItem { Value = s.SubjectID.ToString(), Text = "Grade-" + gradeModel.GradeName + " " + s.SubjectName, Selected = false });
            }
            return ListOfsubjcts;
        }

        public List<SelectListItem> GenerateCitiesDropdown(List<string> citiesList)
        {
            StudentManager stdMgr = new StudentManager();

            List<SelectListItem> ListOfcity = new List<SelectListItem>();
            ListOfcity.Add(new SelectListItem { Value = "", Text = "Select City", Selected = false });
            foreach (var city in citiesList)
            {
                ListOfcity.Add(new SelectListItem { Value = city.ToString(), Text = city, Selected = false });
            }
            return ListOfcity;
        }
    }
}