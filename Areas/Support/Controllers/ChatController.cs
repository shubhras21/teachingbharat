﻿using BLL.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Tutoring.Areas.Support.Controllers
{
   
    public class ChatController : Controller
    {
        // GET: Support/Chat
       
        public ActionResult Index()
        {
            if (Config.User ==null || Config.User.UserID <= 0)
            {
                return RedirectToAction("Login","Account",  new { area = "", @returnUrl = Url.Action("Index","Chat", new { area = "Support" }) });
            }
            return View();
        }

    }
}