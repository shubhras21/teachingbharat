﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Tutoring.Areas.Parent.Controllers
{
    public class ParentController : Controller
    {
        // GET: Parent/Parent
        public ActionResult Index()
        {
            return View();
        }
    }
}