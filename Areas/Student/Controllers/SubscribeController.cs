﻿using BLL.BusinessManagers.AdminManager;
using BLL.BusinessModels;
using BLL.Helpers;
using DAL.Entities;
using DAL.MasterEntity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Tutoring.Areas.Student.Controllers
{
    public class SubscribeController : Controller
    {
        string msg = "";
        MyDbContext context = new MyDbContext();

        CurriculumsManagers cManager = new CurriculumsManagers();
        // GET: Student/Subscribe
        public ActionResult Index()
        {
            
            return View(ListSubRequests());
        }
        public List<Subjects> ListBySubjects(Int64 CurriculumID)
        {
            try
            {

                List<Subjects> list = new List<Subjects>();
                list = context.Subjects.Where(x => x.FKCurriculumID == CurriculumID && x.IsActive == true).ToList();
                if (list != null && list.Count > 0)
                {
                    list.ForEach(x => {
                        x.GradeName = context.Grades.Where(y => y.GradeID == x.FKGradeID).Select(y => y.GradeName).FirstOrDefault().ToString() + " - " + context.Subjects.Where(y => y.SubjectID == x.SubjectID).Select(y => y.SubjectName).FirstOrDefault();
                    });

                    return list;
                }
                else
                {
                    return null;
                }

            }
            catch (Exception)
            {
                throw;
            }
        }
        public List<Subjects> ListByAllSubjects()
        {
            try
            {

                List<Subjects> list = new List<Subjects>();
                list = context.Subjects.Where(x => x.IsActive == true).ToList();
                if (list != null && list.Count > 0)
                {
                    list.ForEach(x => {
                        x.GradeName = context.Grades.Where(y => y.GradeID == x.FKGradeID).Select(y => y.GradeName).FirstOrDefault().ToString() + " - " + context.Subjects.Where(y => y.SubjectID == x.SubjectID).Select(y => y.SubjectName).FirstOrDefault();
                    });

                    return list;
                }
                else
                {
                    return null;
                }

            }
            catch (Exception)
            {
                throw;
            }
        }
        public ActionResult Create()
        {


            StudentReqSubModel model = new StudentReqSubModel();
            model.ListofCurriculums = cManager.dListOfCurriculums();
            model.ListofSubjects = cManager.dListOfSubjects();
            ViewBag.ConfirmationMessage = TempData["ConfirmationMessage"];

            return View(model);
        }
        public ActionResult AddStudentReqSub(StudentReqSubModel model)
        {
            try
            {
                var obj = new DAL.Entities.StudentReqSub
                {
                    CreatedBy = Config.CurrentUser,
                    IsActive = true
                };
                obj.LookingFor = model.LookingFor;
                obj.StartDate = model.StartDate;
                obj.CurriculumId = model.CurriculumId;
                obj.SubjectId = model.SubjectId;
                obj.Remarks = model.Remarks;

                context.StudentReqSubs.Add(obj);
                context.SaveChanges();
                Int64 id = obj.Id;
                msg = "Your request submitted successfully.";


                StudentReqSubModel model2 = new StudentReqSubModel();
                model.ListofCurriculums = cManager.dListOfCurriculums();
                model.ListofSubjects = cManager.dListOfSubjects();
                TempData["ConfirmationMessage"]  = msg;

                return RedirectToAction("Create");
               
            }
            catch (Exception ex)
            {
                @ViewBag.ConfirmationMessage = "Unable to submit request. Please contact to administrator!!!";

                throw ex;
            }
        }
        public List<StudentReqSub> ListSubRequests()
        {
            try
            {

                List<StudentReqSub> list = new List<StudentReqSub>();
                list = context.StudentReqSubs.ToList();
                if (list != null && list.Count > 0)
                {
                    list.ForEach(x =>
                    {

                        x.Curriculum = context.Curriculums.Where(y => y.CurriculumID == x.CurriculumId).Select(y => new { Name = y.CurriculumName }).FirstOrDefault().Name.ToString();
                        x.Subject = context.Subjects.Where(y => y.SubjectID == x.SubjectId).Select(y => new { Name = y.SubjectName }).FirstOrDefault().Name.ToString();
                        

                    });

                    return list;
                }
                else
                {
                    return null;
                }

            }
            catch (Exception)
            {
                throw;
            }
        }
        public List<CoursesHeader> ListByCurriculumsAll()
        {
            try
            {

                List<CoursesHeader> list = new List<CoursesHeader>();
                list = context.CourseHeader.Where(x => x.IsActive == true).ToList();
                if (list != null && list.Count > 0)
                {
                    list.ForEach(x => {

                        x.CourseParticipants = context.CourseParticipants.Where(y => y.FKCourseID == x.CoursesHeaderID).ToList();
                        x.TeacherName = context.UsersProfile.Where(y => y.FKUserID == x.FKOfferedBY).Select(y => new { Name = y.FirstName + " " + y.LastName }).FirstOrDefault().Name.ToString();
                        x.GradeName = context.Grades.Where(y => y.GradeID == x.FKGradeID).Select(y => y.GradeName).FirstOrDefault().ToString() + " - " + context.Subjects.Where(y => y.SubjectID == x.FKSubjectID).Select(y => y.SubjectName).FirstOrDefault();
                        x.TeacherPic = Convert.ToString(context.UsersProfile.Where(y => y.FKUserID == x.FKOfferedBY).Select(y => y.ProfilePic).FirstOrDefault());

                    });

                    return list;
                }
                else
                {
                    return null;
                }

            }
            catch (Exception)
            {
                throw;
            }
        }

    }
}