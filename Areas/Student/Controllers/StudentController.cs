using BigBlueButtonSDK;
using BLL.BusinessManagers;
using BLL.BusinessManagers.AdminManager;
using BLL.BusinessManagers.SessionsManager;
using BLL.BusinessManagers.StudentManager;
using BLL.BusinessManagers.TeacherManager;
using BLL.BusinessManagers.UserManager;
using BLL.BusinessModels.AdminModels;
using BLL.BusinessModels.CalenderModels;
using BLL.BusinessModels.StudentModels;
using BLL.BusinessModels.TeacherModels;
using BLL.BusinessModels.UserModels;
using BLL.EmailTemplate;
using BLL.Helpers;
using DAL.Entities;
using DAL.MasterEntity;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Tutoring.Areas.Student.Controllers
{
      
    public class StudentController : Controller
    {

        //http://tutor.teachingbharat.com/bigbluebutton/api/create?meetingID=DAL.Entities.Sessions&name=Debugging&moderatorPW=123456&attendeePW=654321&checksum=1220f58e21878dc4608b95fca4a8e59484854754
        string BigBlueServer_URL = "http://tutor.teachingbharat.com/bigbluebutton/";
        // string BigBlueServer_URL = "https://tutor.teachingbharat.com/b/shu-fz4-2zt";
        string BigBlueServer_SecSalt = "afc6cab8ef4524b8b4638264ae369fd8";
        string BigBlueServer_Name = "Debugging";
        string BigBlueServer_UserName = "shubhras@yahoo.com";
        string BigBlueServer_Password = "123456";
        string BigBlueServer_AttendeePassword = "654321";


        UsersActivityManager ActivityMgr = null;
        AdminManager AdminMgr = null;
        UserActivities obj_Activity = null;
        MyDbContext db = new MyDbContext();

        JoinMeeting joinMeeting = new JoinMeeting();
        JoinMeetingRequest joinRequest = new JoinMeetingRequest();
        JoinMeetingResponse joinResponse = new JoinMeetingResponse();

        EndMeeting endMeeting = new EndMeeting();
        EndMeetingRequest endMeetingRequest = new EndMeetingRequest();

        EndMeetingResponse endMeetingResponse = new EndMeetingResponse();
        public void AddActivity(string ActivityBy, string Comments)
        {
            UserActivity users = new UserActivity();
            users.ActivityBy = ActivityBy;
            users.ActivityDateTime = DateTime.Now;
            users.Comments = Comments;
            users.UserID = Config.CurrentUser;
            users.IsSeen = false;
            db.UsersActivity.Add(users);
            db.SaveChanges();
        }


        // GET: Student/Student
        public ActionResult Index()
        {
            if (Config.CurrentUser == 0)
                return RedirectToAction("Login", "Account", new { area = "" });

            if (Config.User.UserType != (int)Enumerations.UserType.Student)
                return RedirectToAction("UnAuthorizeAccess", "Home", new { area = "" });
            
            int status;
            string message;
            int.TryParse(Convert.ToString(Request.Params["status"]), out status);
            message = Convert.ToString(Request.Params["message"]);   
                            
            ViewBag.Status = (TempData["Status"]!= null)?(int)TempData["Status"]:0;
            ViewBag.Message = (TempData["Message"] != null) ? (string)TempData["Message"] : null;
            
            UserBLL userBLL = new UserBLL();
            UsersProfile user = new UsersProfile();
            user = userBLL.GetUserProfile(Config.CurrentUser);
            SessionManager manager = new SessionManager();
            UsersActivityManager uManager = new UsersActivityManager();
            StudentDashboardViewModel model = new StudentDashboardViewModel();


            CourseOfferingsManager cManager = new CourseOfferingsManager();

            List<CourseParticipants> listCP = new List<CourseParticipants>();
            ViewBag.Subscription = cManager.StudentSubscriptions(Config.CurrentUser);

            List<UsersProfile> list = new List<UsersProfile>();



            list = manager.MyTeacher(Config.CurrentUser);
			if (list != null)
			{
				foreach (var item in list)
				{

					DateTime birthDate = Convert.ToDateTime(item.DateOfBirth);

					var year = birthDate.Year;
					var month = birthDate.Month;
					var day = birthDate.Day;

					DateTime NowDate = DateTime.Now.Date;

					var nowyear = NowDate.Year;
					var nowmonth = NowDate.Month;
					var nowday = NowDate.Day;
					if (day == nowday && month == nowmonth)
					{
						UserActivity users = new UserActivity();
						users.ActivityBy = "Student";
						users.ActivityDateTime = DateTime.Now;
						users.Comments = "Its your Teacher (" + item.FirstName + " " + item.LastName + ") Birthday today!";
						users.UserID = Config.CurrentUser;
						users.IsSeen = false;
						db.UsersActivity.Add(users);
					}
				}
				db.SaveChanges();
			}
            model.FirstName = user.FirstName;
            model.LastName = user.LastName;
            model.StateName = user.StateName;
            model.City = user.CityName;
            model.ContactNumber = user.ContactNumber;
            model.Email = Config.User.LoginEmail;
            model.ProfileImage = (!string.IsNullOrEmpty(user.ProfilePic)) ? user.ProfilePic : null;
            model.listOfSession = manager.StudentPendingSessions(Config.CurrentUser);
            model.listOfUpcomingSession = manager.StudentUpcomingSessions(Config.CurrentUser);
           
            model.listOfWeeklyReport = manager.StudentWeeklyReport(Config.CurrentUser);
            model.listOfUserActivities = uManager.LastFiveActivites(Config.CurrentUser);

            NotificationManager nManager = new NotificationManager();

            model.listOfMyNotifications = nManager.ListofUserNotifications(Config.CurrentUser);

            if (model.listOfMyNotifications != null && model.listOfMyNotifications.Count > 0)
            {
                model.listOfMyNotifications = model.listOfMyNotifications.Take(3).ToList();
            }

            AddActivity("student", "You seen dashboard");
            return View(model);

        }
        public ActionResult conncet_bbb()
        {
            
            FillDefaultJoinMeetingRequestAndResponse();
            joinMeeting.Request = joinRequest;
            
            JoinMeetingResponse joinResponse = joinRequest.Execute();
            joinMeeting.Response = joinResponse;

            joinResponse = joinRequest.Execute();

            if (joinResponse.Response.Length > 0) //if everything is ok
            {
                Json(new { url = joinRequest.RequestURL }, JsonRequestBehavior.AllowGet);
            }
            else //if something goes wrong
            {
                joinMeeting.Response = joinResponse;
            }
            return new RedirectResult(joinRequest.RequestURL);


        }
        //public void FillGetRecordsRequestAndResponse()
        //{
        //    SessionManager sManager = new SessionManager();
        //    TeacherManager manager = new TeacherManager();
        //    var list = manager.ListOfTeacherSessions(Config.CurrentUser);
        //    var eventObj = new Sessions();
        //    foreach (var item in list)
        //    {
        //        eventObj = sManager.GetSessionDetail(item.TrialSessionID);

        //    }
        //    //string ddss = Session["otp"].ToString();
        //    getRecordingsRequest.MeetingID = eventObj.ToString();
        //    getRecordingsRequest.ServerURL = "http://50.21.176.216/bigbluebutton/";
        //    getRecordingsRequest.SecuritySalt = "8227be0510f0931941f59bf42558f3d4";

        //    getRecordings.Request = getRecordingsRequest;
        //    getRecordings.Response = getRecordingsResponse;
        //}
        public ActionResult Cancel_BBB()
        {
            FillDefaultEndMeetingRequestAndResponse();
            endMeeting.Request = endMeetingRequest;
            endMeeting.Response = endMeetingResponse;
            endMeeting.Response = endMeetingRequest.Execute();
            return RedirectToAction("UpComingSessions", "Student", "Student");
        }
        private void FillDefaultJoinMeetingRequestAndResponse()
        {
            StudentManager manager = new StudentManager();
            SessionManager sManager = new SessionManager();
            var list = sManager.ListOfStudentSessions(Config.CurrentUser);
            var eventList = new List<FullCalender>();
            var eventObj = new Sessions();
            foreach (var item in list)
            {
                eventObj = sManager.GetSessionDetail(item.TrialSessionID);

            }

            joinRequest.MeetingID = eventObj.ToString();
            //joinRequest.MeetingID = "e0ae6dfb996a0e0ea85bd7a47359bfc994d646ee";
            joinRequest.FullName = Config.UsersProfile.FirstName;


            joinRequest.ServerURL = BigBlueServer_URL;
            joinRequest.SecuritySalt = BigBlueServer_SecSalt;
            joinRequest.Password = BigBlueServer_Password;
        }

        private void FillDefaultEndMeetingRequestAndResponse()
        {
            StudentManager manager = new StudentManager();
            SessionManager sManager = new SessionManager();
            var list = sManager.ListOfStudentSessions(Config.CurrentUser);
            var eventList = new List<FullCalender>();
            var eventObj = new Sessions();
            foreach (var item in list)
            {
                eventObj = sManager.GetSessionDetail(item.TrialSessionID);

            }

            endMeetingRequest.MeetingID = eventObj.ToString();
            endMeetingRequest.Password = BigBlueServer_Password;
            endMeetingRequest.ServerURL = BigBlueServer_URL;
            endMeetingRequest.SecuritySalt = BigBlueServer_SecSalt;
        }
        public ActionResult FindTeacher()
        {
            //if (Config.CurrentUser == 0)
            //    return RedirectToAction("Login", "Account", new { area = "" });

            //if (Config.User.UserType != (int)Enumerations.UserType.Student)
            //    return RedirectToAction("UnAuthorizeAccess", "Home", new { area = "" });

            FindTeacherViewModel model = new FindTeacherViewModel();
            StudentManager obj = new StudentManager();
            model = obj.GetDetails();
            AddActivity("student", "Teacher searched.");
            return View(model);

        }


        public ActionResult step_1()
        {
            if (Config.CurrentUser == 0)
                return RedirectToAction("Login", "Account", new { area = "" });

            if (Config.User.UserType != (int)Enumerations.UserType.Student)
                return RedirectToAction("UnAuthorizeAccess", "Home", new { area = "" });
            //AddActivity("student", "Teacher searched.");
            return View();

        }

        public ActionResult LoadOmm()
        {
            //if (Config.CurrentUser == 0)
            //    return RedirectToAction("Login", "Account", new { area = "" });

            //if (Config.User.UserType != (int)Enumerations.UserType.Student)
            //    return RedirectToAction("UnAuthorizeAccess", "Home", new { area = "" });

            return PartialView();
        }
        public PartialViewResult LoadCurriculums(string msg)
        {
           
            FindTeacherViewModel model = new FindTeacherViewModel();
            StudentManager obj = new StudentManager();
            model = obj.GetDetails();
            ViewBag.Message = msg;
            return PartialView(model);
        }

        public ActionResult LoadGrades(Int64? TargetId,string msg)
        {
            if (TargetId == null)
            {
                return RedirectToAction("LoadCurriculums", new { @msg = "Please select Curriculum" });
            }
            else
            {
                FindTeacherViewModel model = new FindTeacherViewModel();
                StudentManager obj = new StudentManager();
                dynamic a = TargetId.HasValue ? TargetId : 0;
                model = obj.GetGrades(a);
                model.CurriculumID = a;
                ViewBag.Message = msg;
                return PartialView(model);
            }        
        }


        public ActionResult LoadSubjects(Int64? GradeId, Int64 CurriculumID,string msg)
        {
            if (GradeId == null)
            {
                return RedirectToAction("LoadGrades", new { @msg = "Please select Grade", @TargetId= CurriculumID });
            }
            else
            {
            FindTeacherViewModel model = new FindTeacherViewModel();
            StudentManager obj = new StudentManager();
            dynamic a = GradeId.HasValue ? GradeId : 0;
            model = obj.GetSubjects(a);
            model.varGradeId = a;
            model.CurriculumID = CurriculumID;
            ViewBag.Message = msg;
            return PartialView(model);
            }
        }




        public ActionResult SubmitSubjects(Int64? SubjectId, Int64 GradeId, Int64 CurriculumID,string msg)
        {
            //if (Config.CurrentUser == 0)
            //    return RedirectToAction("Login", "Account", new { area = "" });

            //if (Config.User.UserType != (int)Enumerations.UserType.Student)
            //    return RedirectToAction("UnAuthorizeAccess", "Home", new { area = "" });

            if (SubjectId == null)
            {
                return RedirectToAction("LoadSubjects", new { @msg = "Please select Subject", @CurriculumID=CurriculumID, @GradeId=GradeId });
            }
            else
            {
                FindTeacherViewModel model = new FindTeacherViewModel();
                StudentManager obj = new StudentManager();
                dynamic a = SubjectId.HasValue ? SubjectId : 0;
                model = obj.GetSubjects(a);
                model.varSubjectId = a;
                model.CurriculumID = CurriculumID;
                model.varGradeId = GradeId;
                ViewBag.Message = msg;
                return RedirectToAction("ListOfTeacherss", new { @CurriculumID = CurriculumID, @GradeId = GradeId , @SubjectId=SubjectId });
            }
        }
       
        public ActionResult ListOfTeacherss(Int64 SubjectId, Int64 GradeId, Int64 CurriculumID)
        {

            //if (Config.CurrentUser == 0)
            //    return RedirectToAction("Login", "Account", new { area = "" });

            //if (Config.User.UserType != (int)Enumerations.UserType.Student)
            //    return RedirectToAction("UnAuthorizeAccess", "Home", new { area = "" });

            TempData["CurriculumID"] = CurriculumID;
            TempData["GradeId"] = GradeId;
            TempData["SubjectId"] = SubjectId;
            // string url = "http://10.90.1.162/Tutoring/Student/Student/ListOfTeachers";      
            //  string url = "http://localhost/Tutoring//Student/Student/ListOfTeachers";
            //     string url = "http://rudraa.org/Student/Student/ListOfTeachers";
            //  string url = "http://demo.teachingbharat.com/Student/Student/ListOfTeachers";
            string link = HttpContext.Request.Url.Scheme + "://" + HttpContext.Request.Url.Authority + Url.Action("ListOfTeachers", "Student");
            return JavaScript("window.location = '" + link + "'");
        }

        public ActionResult ListOfTeachers()
        {
            if (TempData["SubjectId"] == null || Convert.ToInt32(TempData["SubjectId"]) == 0)
            {
                return RedirectToAction("step_1");

            }


            //if (Config.CurrentUser == 0)
            //    return RedirectToAction("Login", "Account", new { area = "" });

            //if (Config.User.UserType != (int)Enumerations.UserType.Student)
            //    return RedirectToAction("UnAuthorizeAccess", "Home", new { area = "" });

            StudentManager obj = new StudentManager();
            Int64 SubjectId = (Int64)TempData["SubjectId"];
            TempData.Keep();
            Int64 GradeId = (Int64)TempData["GradeId"];
            TempData.Keep();
            Int64 CurriculumID = (Int64)TempData["CurriculumID"];
            TempData.Keep();
            var ListofTeacher = obj.ListOfRelevantTeachers(SubjectId, GradeId, CurriculumID);
            ViewBag.SubjectID = SubjectId;
            ViewBag.GradeID = GradeId;
            ViewBag.CurriculumID = CurriculumID;


            if (ListofTeacher!=null && ListofTeacher.Count>0)
            {
                

                if(ListofTeacher.Any(x=>x.User.IsOnline==true))
                {
                    // Online users exists
                    ListofTeacher = ListofTeacher.OrderByDescending(x => x.User.IsOnline).ToList();
                }
                else
                {

                    // No online user exist and I need to take users by last login date
                    ListofTeacher = ListofTeacher.OrderByDescending(x => x.User.LastLogin).ToList();

                }

            }


            return View(ListofTeacher);
        }


        [HttpGet]
        public ActionResult ChangeFilters()
        {

            CurriculumsManagers manager = new CurriculumsManagers();
            var model = new ChangeFiltersViewModel();
            model.ListOfCurriculums = manager.dListOfCurriculums();
            model.ListOfGrades = Enumerable.Empty<SelectListItem>();
            model.ListOfSubjects = Enumerable.Empty<SelectListItem>();         
            return PartialView(model);

        }
        
        [HttpPost]
        public ActionResult ChangeFilters(ChangeFiltersViewModel model)
        {
            try
            {
                if(model!=null)
                {
                    TempData["CurriculumID"] = model.CurriculumID;
                    TempData["GradeId"] = model.GradeID;
                    TempData["SubjectId"] = model.SubjectID;
                    return RedirectToAction("ListOfTeachers");
                }
                else
                {
                    return null;
                }
            }
            catch (Exception ex)
            {
                
                throw;
            }
           

        }

        public ActionResult StaticBookSessionss(Int64 TeacherID, Int64 CurriculumID, Int64 GradeID, Int64 SubjectID)
        {
            //if (Config.CurrentUser == 0)
            //    return RedirectToAction("Login", "Account", new { area = "" });

            //if (Config.User.UserType != (int)Enumerations.UserType.Student)
            //    return RedirectToAction("UnAuthorizeAccess", "Home", new { area = "" });

            BookSessionModel model = new BookSessionModel();
            model.CurriculumID = CurriculumID;
            model.GradeID = GradeID;
            model.SubjectID = SubjectID;
            model.TeacherID = TeacherID;
            SessionManager manager = new SessionManager();
            BookSessionModel obj = manager.SessionBookingHelper(model);
            TempData["BookSessionModel"] = obj;

            // Newly Added

            TempData.Keep();

            return RedirectToAction("StaticBookSessions");
        }

        public ActionResult StaticBookSessions()
        {

            string ReturnUrl = Request.Url.AbsoluteUri;

            if (Config.CurrentUser == 0)
                return RedirectToAction("Login", "Account", new { area = "", @ReturnUrl = ReturnUrl });

            if (Config.User.UserType != (int)Enumerations.UserType.Student)
                return RedirectToAction("UnAuthorizeAccess", "Home", new { area = "" });

            BookSessionModel obj = (BookSessionModel)TempData["BookSessionModel"];
            TempData.Keep();
            return View(obj);
        }

        [HttpPost]
        public ActionResult StaticBookSessions(BookSessionModel obj)
        {
            AdminMgr = new AdminManager();
            ActivityMgr = new UsersActivityManager();


            BookSessionModel objs = (BookSessionModel)TempData["BookSessionModel"];
            TempData.Keep();

            SessionManager manager = new SessionManager();
            obj.SessionType = (int)Enumerations.SessionType.One;
            
            Int64 ReturnID = manager.AddSession(obj);
            if(ReturnID>0)
            {
                // Add Activity of Sending Session Request
                obj_Activity = new UserActivities();
                obj_Activity.FKUserID = Config.CurrentUser;
               var usermodel = AdminMgr.GetAdminByUserID(obj_Activity.FKUserID);  // get user by id
                string name = "Someone";
                if(usermodel.UserID > 0)
                {
                  if(usermodel.UsersProfileViewModel != null)
                    {
                        if(usermodel.UsersProfileViewModel.UsersProfileID > 0)
                        {
                            name = usermodel.UsersProfileViewModel.FirstName + " " + usermodel.UsersProfileViewModel.LastName;
                        }
                    }
                }
                obj_Activity.ActionPerformed = name + " sent session request to " + obj.TeacherName;
                obj_Activity.ActivitySource = (int)Enumerations.ActivitySource.FrontsideActivity;
                Int64 ID = ActivityMgr.AddActivity(obj_Activity);
                UserNotifications noti = new UserNotifications();
                noti.ActionPerformed= " Student Request for session " ;
                noti.FKSessionID = ReturnID;
				noti.FKUserID = obj.TeacherID;

				noti.IsActive = true;
                noti.IsCourse = 0;
                noti.Time = DateTime.Now;
                noti.IsSeen = false;
                db.UserNotification.Add(noti);
                db.SaveChanges();

                TempData["Message"] = "Your session request send successfully.";
                TempData["Status"] = 2;
                AddActivity("student", "Session request send successfully.");
                return RedirectToAction("Index", new { @Status = 2, @Message = "Your session request send successfully." });
            }
            else
            {
                TempData["Message"] = "Something went wrong. Please try Later.";
                TempData["Status"] = 3;
                return RedirectToAction("Index", new { @Status = 3, @Message = "Something went wrong. Please try Later" });
            }

            return RedirectToAction("Index");
        }



        public ActionResult MySessions()
        {

            if (Config.CurrentUser == 0)
                return RedirectToAction("Login", "Account", new { area = "" });

            if (Config.User.UserType != (int)Enumerations.UserType.Student)
                return RedirectToAction("UnAuthorizeAccess", "Home", new { area = "" });

            SessionManager manager = new SessionManager();
            StudentDashboardViewModel model = new StudentDashboardViewModel();
            model.listOfSession = manager.StudentSessions(Config.CurrentUser);
            model.listOfSession = model.listOfSession.OrderByDescending(x => x.RequestDate).ToList();
            AddActivity("student", "My Sessions visited.");
            return View(model);

        }


        public ActionResult UpdateMySession()
        {

            if (Config.CurrentUser == 0)
                return RedirectToAction("Login", "Account", new { area = "" });

            if (Config.User.UserType != (int)Enumerations.UserType.Student)
                return RedirectToAction("UnAuthorizeAccess", "Home", new { area = "" });

            SessionManager manager = new SessionManager();

            MySessions mSessions = new MySessions();
            mSessions = manager.UpdatedStudentMySession(Config.CurrentUser);
            AddActivity("student", "My Sessions visited.");
            return View(mSessions);

        }

        public ActionResult UpComingSessions()
        {

            if (Config.CurrentUser == 0)
                return RedirectToAction("Login", "Account", new { area = "" });

            if (Config.User.UserType != (int)Enumerations.UserType.Student)
                return RedirectToAction("UnAuthorizeAccess", "Home", new { area = "" });

            SessionManager manager = new SessionManager();

            MySessions mSessions = new MySessions();
            mSessions = manager.UpdatedStudentMySession(Config.CurrentUser);

            return View(mSessions);

        }

        public ActionResult PendingSessions()
        {

            if (Config.CurrentUser == 0)
                return RedirectToAction("Login", "Account", new { area = "" });

            if (Config.User.UserType != (int)Enumerations.UserType.Student)
                return RedirectToAction("UnAuthorizeAccess", "Home", new { area = "" });

            int status;
            string message;
            int.TryParse(Convert.ToString(Request.Params["status"]), out status);
            message = Convert.ToString(Request.Params["message"]);
            ViewBag.Status = status;
            ViewBag.Message = message;

            SessionManager manager = new SessionManager();

            MySessions mSessions = new MySessions();
            mSessions = manager.UpdatedStudentMySession(Config.CurrentUser);

            return View(mSessions);

        }

        public ActionResult CanceledSessions()
        {

            if (Config.CurrentUser == 0)
                return RedirectToAction("Login", "Account", new { area = "" });

            if (Config.User.UserType != (int)Enumerations.UserType.Student)
                return RedirectToAction("UnAuthorizeAccess", "Home", new { area = "" });

            SessionManager manager = new SessionManager();

            MySessions mSessions = new MySessions();
            mSessions = manager.UpdatedStudentMySession(Config.CurrentUser);

            return View(mSessions);

        }
        public ActionResult PastSessions()
        {

            if (Config.CurrentUser == 0)
                return RedirectToAction("Login", "Account", new { area = "" });

            if (Config.User.UserType != (int)Enumerations.UserType.Student)
                return RedirectToAction("UnAuthorizeAccess", "Home", new { area = "" });

            SessionManager manager = new SessionManager();

            MySessions mSessions = new MySessions();
            mSessions = manager.UpdatedStudentMySession(Config.CurrentUser);

            return View(mSessions);

        }





        public ActionResult TeacherProfile(Int64 TeacherID)
        {
            if (Config.CurrentUser == 0)
                return RedirectToAction("Login", "Account", new { area = "" });

            if (Config.User.UserType != (int)Enumerations.UserType.Student)
                return RedirectToAction("UnAuthorizeAccess", "Home", new { area = "" });

            UserBLL userBLL = new UserBLL();
            Users u = new Users();
            u = userBLL.GetUserType(TeacherID);

            if(u==null)
            {
                return RedirectToAction("UnAuthorizeAccess", "Home", new { area = "" });
            }
            if (u.UserType == (int)Enumerations.UserType.Student || u.UserType == (int)Enumerations.UserType.Parent || u.UserType == (int)Enumerations.UserType.Admin || u.UserType == (int)Enumerations.UserType.Super)
            {
                return RedirectToAction("UnAuthorizeAccess", "Home", new { area = "" });
            }
            if (u.UserType!=(int)Enumerations.UserType.Teacher)
            {
                return RedirectToAction("UnAuthorizeAccess", "Home", new { area = "" });
            }
            UsersProfile user = new UsersProfile();
            user = userBLL.GetUserProfile(TeacherID);

            RatingsManager rManager = new RatingsManager();
            TeacherDashboardViewModel model = new TeacherDashboardViewModel();
            model.TeacherID = TeacherID;
            model.FirstName = user.FirstName;
            model.LastName = user.LastName;
            model.Experience = user.ExperienceName;
            model.StateName = user.StateName;
            model.City = user.CityName;
            model.ContactNumber = user.ContactNumber;
            model.UserProfile = user;
            TeacherQualification obj = new TeacherQualification();
            obj = userBLL.GetUserQualification(TeacherID);
            model.Qualification = obj.QualificationName;
            model.InstituteName = obj.UniversityName;
            model.listOfSubjects = userBLL.ListOfTeacherSubjects(TeacherID);

            model.RatingsModel = rManager.ListOfRatings(TeacherID);

            model.RatingsModel.ReviewsList = model.RatingsModel.ReviewsList.OrderByDescending(x => x.CreatedDate).Take(4).ToList();

            // For rating and review

            SessionManager manager = new SessionManager();
            List<UsersProfile> MyTeachers = new List<UsersProfile>();
            MyTeachers = manager.MyStudents(TeacherID);
            bool status = false;
            if (MyTeachers != null && MyTeachers.Count > 0)
            {
                status = MyTeachers.Any(x => x.FKUserID == Config.CurrentUser);
            }

            ViewBag.MyTeacherStatus = status;


            return View(model);
        }

        public ActionResult BookSession(Int64 TeacherID)
        {
            if (Config.CurrentUser == 0)
                return RedirectToAction("Login", "Account", new { area = "" });

            if (Config.User.UserType != (int)Enumerations.UserType.Student)
                return RedirectToAction("UnAuthorizeAccess", "Home", new { area = "" });

            BookSessionModel model = new BookSessionModel();
            model.TeacherID = TeacherID;
            SessionManager manager = new SessionManager();
            BookSessionModel obj = manager.BookSessionHelper(model);

            return View(obj);
        }

        [HttpPost]
        public ActionResult BookSession(BookSessionModel obj)
        {

            if (Config.CurrentUser == 0)
                return RedirectToAction("Login", "Account", new { area = "" });

            if (Config.User.UserType != (int)Enumerations.UserType.Student)
                return RedirectToAction("UnAuthorizeAccess", "Home", new { area = "" });

            SessionManager manager = new SessionManager();
            obj.SessionType = (int)Enumerations.SessionType.One;
            Int64 ReturnID = manager.AddSession(obj);
            if (ReturnID > 0)
            {
                TempData["Message"] = "Your session request send successfully.";
                TempData["Status"] = 2;
                return RedirectToAction("Index", new { @Status = 2, @Message = "Your session request send successfully." });
            }
            else
            {
                TempData["Message"] = "Something went wrong. Please try Later.";
                TempData["Status"] = 3;
                return RedirectToAction("Index", new { @Status = 3, @Message = "Something went wrong. Please try Later" });
            }
            return RedirectToAction("Index");

        }

        public ActionResult BookTrialSession(Int64 TeacherID)
        {

            if (Config.CurrentUser == 0)
                return RedirectToAction("Login", "Account", new { area = "" });

            if (Config.User.UserType != (int)Enumerations.UserType.Student)
                return RedirectToAction("UnAuthorizeAccess", "Home", new { area = "" });

            BookSessionModel model = new BookSessionModel();
            model.TeacherID = TeacherID;
            SessionManager manager = new SessionManager();
            BookSessionModel obj = manager.BookSessionHelper(model);

            return View(obj);
        }

        [HttpPost]
        public ActionResult BookTrialSession(BookSessionModel obj)
        {

            if (Config.CurrentUser == 0)
                return RedirectToAction("Login", "Account", new { area = "" });

            if (Config.User.UserType != (int)Enumerations.UserType.Student)
                return RedirectToAction("UnAuthorizeAccess", "Home", new { area = "" });

            SessionManager manager = new SessionManager();
            obj.SessionType = (int)Enumerations.SessionType.Trial;
            Int64 ReturnID = manager.AddSession(obj);
            if (ReturnID > 0)
            {
                TempData["Message"] = "Your session request send successfully.";
                TempData["Status"] = 2;
                return RedirectToAction("Index", new { @Status = 2, @Message = "Your session request send successfully." });
            }
            else
            {
                TempData["Message"] = "Something went wrong. Please try Later.";
                TempData["Status"] = 3;
                return RedirectToAction("Index", new { @Status = 3, @Message = "Something went wrong. Please try Later" });
            }
            return RedirectToAction("Index");

        }


        [AllowAnonymous]
        public JsonResult GradesList(int id)
        {
            MyDbContext context = new MyDbContext();

            if (id > 0)
            {
                return Json(
                   context.Grades.Where(x => x.FKCurriculumID == id && x.IsActive == true).Select(x => new { value = x.GradeID, text = x.GradeName }),
                    JsonRequestBehavior.AllowGet
                );
            }
            return Json(
                null,
                JsonRequestBehavior.AllowGet
            );
        }

        public JsonResult SubjectsList(int id)
        {
            MyDbContext context = new MyDbContext();

            if (id > 0)
            {
                return Json(
                  context.Subjects.Where(x => x.FKGradeID == id && x.IsActive == true).Select(x => new { value = x.SubjectID, text = x.SubjectName }),
                   JsonRequestBehavior.AllowGet
               );
            }
            return Json(
                null,
                JsonRequestBehavior.AllowGet
            );
        }

        [HttpGet]
        public JsonResult SerializeEvent()
        { 
            SessionManager sManager = new SessionManager();
            var list = sManager.ListOfStudentSessions(Config.CurrentUser);

            var eventList = new List<FullCalender>();

            // Add Sessions To Calender

            foreach (var item in list)
            {
                var eventObj = new Sessions();
                eventObj = sManager.GetSessionDetail(item.TrialSessionID);
                var eventObjs = new FullCalender();
                eventObjs.start = eventObj.TrialHappenDate.ToString();
                eventObjs.end = eventObj.TrialHappenDate.AddHours(1).ToString();
                eventObjs.title = eventObj.TopicName;
               // eventObjs.allDay = false;
                eventList.Add(eventObjs);
            }

            // Add Courses Schedule To Calender

            List<CourseSchedule> StudentCourses = new List<CourseSchedule>();
            CourseOfferingsManager cManager = new CourseOfferingsManager();
            StudentCourses = cManager.GetStudentCourseSchedule(Config.CurrentUser);
            if(StudentCourses!=null && StudentCourses.Count>0)
            {
                foreach (var item in StudentCourses)
                {
                    var eventObjs = new FullCalender();

                    eventObjs.start = item.CourseSessionDate.ToString();
                    eventObjs.end = item.CourseSessionDate.AddHours(1).ToString();
                    eventObjs.title = "Course Offering";

                    eventList.Add(eventObjs);

                }
            }

            return Json(eventList, JsonRequestBehavior.AllowGet);
        }

        public ActionResult MyCalender()
        {
            if (Config.CurrentUser == 0)
                return RedirectToAction("Login", "Account", new { area = "" });

            if (Config.User.UserType != (int)Enumerations.UserType.Student)
                return RedirectToAction("UnAuthorizeAccess", "Home", new { area = "" });

            UserBLL uBLL = new UserBLL();
            List<CalenderDateViewModel> list = new List<CalenderDateViewModel>();
            list = uBLL.StudentBookedSessionsTiming(Config.CurrentUser);
            AddActivity("student", "Calender seen.");
            return View(list);
        }

        public ActionResult JoinSessions()
        {
            if (Config.CurrentUser == 0)
                return RedirectToAction("Login", "Account", new { area = "" });

            if (Config.User.UserType != (int)Enumerations.UserType.Student)
                return RedirectToAction("UnAuthorizeAccess", "Home", new { area = "" });
            SessionManager manager = new SessionManager();

            Sessions obj = manager.StudentJoiningSession(Config.CurrentUser);

            return View(obj);
        }

        public ActionResult MyProfile()
        {
            if (Config.CurrentUser == 0)
                return RedirectToAction("Login", "Account", new { area = "" });

            if (Config.User.UserType != (int)Enumerations.UserType.Student)
                return RedirectToAction("UnAuthorizeAccess", "Home", new { area = "" });

            ViewBag.Status = (TempData["Status"] != null) ? (int)TempData["Status"] : 0;
            ViewBag.Message = (TempData["Message"] != null) ? (string)TempData["Message"] : null;

            UserBLL userBLL = new UserBLL();
            MyProfileViewModel GetMyProfile = userBLL.GetMyProfile(Config.CurrentUser);

            return View(GetMyProfile);
        }

        public ActionResult EditProfile()
        {
            if (Config.CurrentUser == 0)
                return RedirectToAction("Login", "Account", new { area = "" });

            if (Config.User.UserType != (int)Enumerations.UserType.Student)
                return RedirectToAction("UnAuthorizeAccess", "Home", new { area = "" });

            UserBLL userBLL = new UserBLL();
            EditProfileViewModel GetMyProfile = userBLL.EditMyProfile(Config.CurrentUser);
            return View(GetMyProfile);
        }

        public ActionResult ChangePassword()
        {
            if (Config.CurrentUser == 0)
                return RedirectToAction("Login", "Account", new { area = "" });

            if (Config.User.UserType != (int)Enumerations.UserType.Student)
                return RedirectToAction("UnAuthorizeAccess", "Home", new { area = "" });

            
            return View();
        }

        [HttpPost]
        public ActionResult ChangePassword(ChangePasswordViewModel model)
        {
            if (Config.CurrentUser == 0)
                return RedirectToAction("Login", "Account", new { area = "" });

            if (Config.User.UserType != (int)Enumerations.UserType.Student)
                return RedirectToAction("UnAuthorizeAccess", "Home", new { area = "" });

            UserBLL userBLL = new UserBLL();
            model.UserID = Config.CurrentUser;
            int status = userBLL.ChangePassword(model);
            if (status > 0)
            {
                TempData["Message"] = "Password Changed Succesfully";
                TempData["Status"] = 2;
                AddActivity("student", "Password Changed.");
                return RedirectToAction("MyProfile");
            }
            else
            {
                TempData["Message"] = "Something went wrong. Please try Later.";
                TempData["Status"] = 3;
                return RedirectToAction("MyProfile");
            }
            ModelState.AddModelError("", "Someting went wrong. Please try Later");

            return View();

        }
        [HttpPost]
        public ActionResult EditProfile(HttpPostedFileBase PostedFile,EditProfileViewModel obj)
        {
            if (Config.CurrentUser == 0)
                return RedirectToAction("Login", "Account", new { area = "" });

            if (Config.User.UserType != (int)Enumerations.UserType.Student)
                return RedirectToAction("UnAuthorizeAccess", "Home", new { area = "" });

            UserBLL userBLL = new UserBLL();
            obj.UserID = Config.CurrentUser;

            if (PostedFile != null && PostedFile.ContentLength > 0)
            {
                var extension = Path.GetExtension(PostedFile.FileName).ToLower();
                Guid id = Guid.NewGuid();
                string path = Server.MapPath("~/UserImages/" + id + extension);
                PostedFile.SaveAs(path);
                obj.ProfileImage = id.ToString() + extension;
            }
            else
            {
                obj.ProfileImage = null;
            }
            int status= userBLL.UpdateStudentProfile(obj);
            if (status > 0)
            {
                TempData["Message"] = "Profile updated succesfully";
                TempData["Status"] = 2;
                AddActivity("student", "Profile updated.");
                return RedirectToAction("MyProfile");
            }
            else
            {
                TempData["Message"] = "Something went wrong. Please try Later.";
                TempData["Status"] = 3;
                return RedirectToAction("MyProfile");
            }
            return RedirectToAction("MyProfile");
        }

        [AllowAnonymous]
        public JsonResult CheckOldPassword(string OldPassword)
        {
            MyDbContext context = new MyDbContext();
            return Json(context.Users.Any(lo => lo.Password == OldPassword && lo.UserID==Config.CurrentUser), JsonRequestBehavior.AllowGet);
        }

        public ActionResult SubmitSupport(string Subject, string Messages)
        {
            bool result = BLL.EmailTemplate.EmailTemplate.SupportEmail(Subject, Messages, Config.User.LoginEmail);
            if (result == true)
            {
                TempData["Message"] = "Your message to support send Succesfully";
                TempData["Status"] = 2;
            }
            else
            {
                TempData["Message"] = "Your message not delivered to support.";
                TempData["Status"] = 3;
            }
            return RedirectToAction("");
        }

        public ActionResult MyAccount()
        {
            if (Config.CurrentUser == 0)
                return RedirectToAction("Login", "Account", new { area = "" });

            if (Config.User.UserType != (int)Enumerations.UserType.Student)
                return RedirectToAction("UnAuthorizeAccess", "Home", new { area = "" });

            return View();
        }

        public ActionResult TryWhiteboard(string p)
        {
            if (Config.CurrentUser == 0)
                return RedirectToAction("Login", "Account", new { area = "" });

            if (Config.User.UserType != (int)Enumerations.UserType.Student && Config.User.UserType != (int)Enumerations.UserType.Teacher)
                return RedirectToAction("UnAuthorizeAccess", "Home", new { area = "" });

			ViewData["IsNewGroup"] = false;
			if (string.IsNullOrWhiteSpace(p))
			{


				Guid g = Guid.NewGuid();
				p = Convert.ToBase64String(g.ToByteArray());
				p = p.Replace("=", "");
				p = p.Replace("+", "");
				ViewData["IsNewGroup"] = true;
				ViewData["url"] = Request.Url.AbsoluteUri.ToString() + "?p=" + p;
			}
			else
			{
				ViewData["url"] = Request.Url.AbsoluteUri.ToString();
			}

			ViewData["GroupName"] = p;
			ViewBag.GroupName = p;

            AddActivity("student", "Whiteboard Visited.");
            return View();
		}


        public ActionResult MySubscriptions()
        {
            if (Config.CurrentUser == 0)
                return RedirectToAction("Login", "Account", new { area = "" });

            if (Config.User.UserType != (int)Enumerations.UserType.Student)
                return RedirectToAction("UnAuthorizeAccess", "Home", new { area = "" });

            return View("MyAccount");
        }
        public ActionResult MyCourses()
        {
            if (Config.CurrentUser == 0)
                return RedirectToAction("Login", "Account", new { area = "" });

            if (Config.User.UserType != (int)Enumerations.UserType.Student)
                return RedirectToAction("UnAuthorizeAccess", "Home", new { area = "" });

            return View("MyAccount");
        }
        public ActionResult MyTeachers()
        {
            if (Config.CurrentUser == 0)
                return RedirectToAction("Login", "Account", new { area = "" });

            if (Config.User.UserType != (int)Enumerations.UserType.Student)
                return RedirectToAction("UnAuthorizeAccess", "Home", new { area = "" });

            SessionManager manager = new SessionManager();
            List<UsersProfile> list = new List<UsersProfile>();
            //list = manager.MyTeacher(Config.CurrentUser); //Aman Edited
            list = manager.MyTeacherGetNotes(Config.CurrentUser);
            AddActivity("student", "My Teachers Visited.");
            return View(list);
        }


        public ActionResult FreeResources()
        {
            if (Config.CurrentUser == 0)
                return RedirectToAction("Login", "Account", new { area = "" });

            if (Config.User.UserType != (int)Enumerations.UserType.Student)
                return RedirectToAction("UnAuthorizeAccess", "Home", new { area = "" });

            return View("MyAccount");
        }

        public ActionResult TestAssignment()
        {
            if (Config.CurrentUser == 0)
                return RedirectToAction("Login", "Account", new { area = "" });

            if (Config.User.UserType != (int)Enumerations.UserType.Student)
                return RedirectToAction("UnAuthorizeAccess", "Home", new { area = "" });

            return View("MyAccount");
        }

        public ActionResult Notes()
        {
            if (Config.CurrentUser == 0)
                return RedirectToAction("Login", "Account", new { area = "" });

            if (Config.User.UserType != (int)Enumerations.UserType.Student)
                return RedirectToAction("UnAuthorizeAccess", "Home", new { area = "" });

            ViewBag.Status = (TempData["Status"] != null) ? (int)TempData["Status"] : 0;
            ViewBag.Message = (TempData["Message"] != null) ? (string)TempData["Message"] : null;


            NotesManager nManager = new NotesManager();
            List<Notes> list = new List<Notes>();
            list = nManager.StudentAssignedNotes(Config.CurrentUser);
            return View(list);

        }

        public ActionResult SystemCheck()
        {
            if (Config.CurrentUser == 0)
                return RedirectToAction("Login", "Account", new { area = "" });

            if (Config.User.UserType != (int)Enumerations.UserType.Student)
                return RedirectToAction("UnAuthorizeAccess", "Home", new { area = "" });

            return View();
        }
        public ActionResult Support()
        {
            if (Config.CurrentUser == 0)
                return RedirectToAction("Login", "Account", new { area = "" });

            if (Config.User.UserType != (int)Enumerations.UserType.Student)
                return RedirectToAction("UnAuthorizeAccess", "Home", new { area = "" });

            return View("SupportForm");
        }

        public ActionResult MonthlyTuitions()
        {
            if (Config.CurrentUser == 0)
                return RedirectToAction("Login", "Account", new { area = "" });

            if (Config.User.UserType != (int)Enumerations.UserType.Student)
                return RedirectToAction("UnAuthorizeAccess", "Home", new { area = "" });

            return View("MyAccount");
        }
        public ActionResult ExploreCourses()
        {
            if (Config.CurrentUser == 0)
                return RedirectToAction("Login", "Account", new { area = "" });

            if (Config.User.UserType != (int)Enumerations.UserType.Student)
                return RedirectToAction("UnAuthorizeAccess", "Home", new { area = "" });

            return View("MyAccount");
        }

        public ActionResult ClearDoubts()
        {
            if (Config.CurrentUser == 0)
                return RedirectToAction("Login", "Account", new { area = "" });

            if (Config.User.UserType != (int)Enumerations.UserType.Student)
                return RedirectToAction("UnAuthorizeAccess", "Home", new { area = "" });

            return View("MyAccount");
        }
        public ActionResult AllNotifications()
        {
            if (Config.CurrentUser == 0)
                return RedirectToAction("Login", "Account", new { area = "" });

            if (Config.User.UserType != (int)Enumerations.UserType.Student)
                return RedirectToAction("UnAuthorizeAccess", "Home", new { area = "" });

            return View("MyAccount");
        }
        public ActionResult AllActivities()
        {
            if (Config.CurrentUser == 0)
                return RedirectToAction("Login", "Account", new { area = "" });

            if (Config.User.UserType != (int)Enumerations.UserType.Student)
                return RedirectToAction("UnAuthorizeAccess", "Home", new { area = "" });

            UsersActivityManager uManager = new UsersActivityManager();
            List<UserActivities> list = new List<UserActivities>();
            list = uManager.ListOfUserActivities(Config.CurrentUser);
            return View(list);
        }

        public ActionResult SessionsScheduled()
        {
            if (Config.CurrentUser == 0)
                return RedirectToAction("Login", "Account", new { area = "" });

            if (Config.User.UserType != (int)Enumerations.UserType.Student)
                return RedirectToAction("UnAuthorizeAccess", "Home", new { area = "" });
            SessionManager manager = new SessionManager();
            List<Sessions> list = manager.StudentUpcomingSessions(Config.CurrentUser);
            return View(list);

        }

        public ActionResult SupportForm()
        {
            if (Config.CurrentUser == 0)
                return RedirectToAction("Login", "Account", new { area = "" });

            if (Config.User.UserType != (int)Enumerations.UserType.Student)
                return RedirectToAction("UnAuthorizeAccess", "Home", new { area = "" });

            return View();
        }

     


 

        public ActionResult SendLink(Int64 TAID, Int64 SID, string UserEmail)
        {
            if(!string.IsNullOrEmpty(UserEmail))
            {
                string URL = Config.WebsiteURL+"/Student/Student/ViewTestResult?TAID="+TAID+"&SID="+SID;

                var emailresult = BLL.EmailTemplate.EmailTemplate.SendResultEmail(UserEmail, URL);

                if (emailresult == true)
                {
                    TempData["Status"] = 2;
                    TempData["Message"] = "Email has been sent.";
                }
                else
                {
                    TempData["Status"] = 3;
                    TempData["Message"] = "Email Sending Failed.";          
                }

            }
            return RedirectToAction("ViewTestResult", new { @TAID = TAID, @SID = SID });
        }

        public ActionResult AllReviews(Int64 TID)
        {
            if (Config.CurrentUser == 0)
                return RedirectToAction("Login", "Account", new { area = "" });

            if (Config.User.UserType != (int)Enumerations.UserType.Student)
                return RedirectToAction("UnAuthorizeAccess", "Home", new { area = "" });

           
          
            SessionManager manager = new SessionManager();
            List<UsersProfile> MyTeachers = new List<UsersProfile>();
            MyTeachers = manager.MyStudents(TID);
            bool status = false;
            if(MyTeachers!=null && MyTeachers.Count>0)
            {
                status = MyTeachers.Any(x => x.FKUserID == Config.CurrentUser);
            }
          
            ViewBag.MyTeacherStatus = status;

            RatingsManager rManager = new RatingsManager();
            RatingsViewModel model = new RatingsViewModel();
            model = rManager.ListOfRatings(TID);
            model.ReviewsList = model.ReviewsList.OrderByDescending(x => x.CreatedDate).ToList();

            model.TeacherID = TID;

            ViewBag.Status = (TempData["Status"] != null) ? (int)TempData["Status"] : 0;
            ViewBag.Message = (TempData["Message"] != null) ? (string)TempData["Message"] : null;


            return View(model);

        }

        
        public ActionResult RateTeacher(Int64 id)
        {
            if (Config.CurrentUser == 0)
                return RedirectToAction("Login", "Account", new { area = "" });

            if (Config.User.UserType != (int)Enumerations.UserType.Student)
                return RedirectToAction("UnAuthorizeAccess", "Home", new { area = "" });

            RatingInsertionModel model = new RatingInsertionModel();
            model.TeacherID = id;
            model.ListofStars = new SelectList(new List<SelectListItem>
            {        
            new SelectListItem { Text = "5 Stars", Value = ((int)5).ToString()},
            new SelectListItem { Text = "4 Stars", Value = ((int)4).ToString()},
            new SelectListItem { Text = "3 Stars", Value = ((int)3).ToString()},
            new SelectListItem { Text = "2 Stars", Value = ((int)2).ToString()},
            new SelectListItem { Text = "1 Stars", Value = ((int)1).ToString()},
            }, "Value", "Text",0);

            return PartialView(model);

        }

        [HttpPost]
        public ActionResult RateTeacher(RatingInsertionModel model)
        {
            if (Config.CurrentUser == 0)
                return RedirectToAction("Login", "Account", new { area = "" });

            if (Config.User.UserType != (int)Enumerations.UserType.Student)
                return RedirectToAction("UnAuthorizeAccess", "Home", new { area = "" });

            model.StudentID = Config.CurrentUser;

            RatingsManager rManager = new RatingsManager();
            Int64 ID = rManager.AddRatings(model);

            if (ID>0)
            {
                TempData["Status"] = 2;
                TempData["Message"] = "Succesfully Rated.";
            }
            else
            {
                TempData["Status"] = 3;
                TempData["Message"] = "Something went wrong.";
            }

            return RedirectToAction("AllReviews", new { TID = model.TeacherID });

        }

        public ActionResult Subscriptions()
        {
            if (Config.CurrentUser == 0)
                return RedirectToAction("Login", "Account", new { area = "" });

            if (Config.User.UserType != (int)Enumerations.UserType.Student)
                return RedirectToAction("UnAuthorizeAccess", "Home", new { area = "" });

            CourseOfferingsManager cManager = new CourseOfferingsManager();

            List<CourseParticipants> list = new List<CourseParticipants>();
            list = cManager.StudentSubscriptions(Config.CurrentUser);

            return View(list); 
        }

        public ActionResult SubscriptionDetail(Int64 CourseID)
        {
            try
            {
                if (CourseID > 0)
                {

                    CourseOfferingsManager cManager = new CourseOfferingsManager();
                    CourseJoinDetails Course = new CourseJoinDetails();
                    Course = cManager.GetCourseJoinDetail(CourseID);
                    return View(Course);

                }
                else
                {
                    return RedirectToAction("Subscriptions");
                }

            }
            catch (Exception)
            {
                throw;
            }
           
        }

        public ActionResult DownloadNotes(Int64 id)
        {

            if (Config.CurrentUser == 0)
                return RedirectToAction("Login", "Account", new { area = "" });

            if (Config.User.UserType != (int)Enumerations.UserType.Student)
                return RedirectToAction("UnAuthorizeAccess", "Home", new { area = "" });

            try
            {

                if (id > 0)
                {

                    NotesManager nManager = new NotesManager();
                    var obj = nManager.GetNotes(id);

                    string filename = obj.FileName;
                    string filepath = AppDomain.CurrentDomain.BaseDirectory + "/TeacherNotes/" + obj.FileName;
                    byte[] filedata = System.IO.File.ReadAllBytes(filepath);
                    string contentType = MimeMapping.GetMimeMapping(filepath);

                    var cd = new System.Net.Mime.ContentDisposition
                    {
                        FileName = filename,
                        Inline = false,
                    };

                    Response.AppendHeader("Content-Disposition", cd.ToString());

                    bool status = nManager.UpdateDownloadCount(id);

                    if (status)
                        return File(filedata, contentType);
                    else
                        return null;

                }
                else
                {
                    return RedirectToAction("UnAuthorizeAccess", "Home", new { area = "" });
                }

            }
            catch (Exception ex)
            {

                throw;
            }

        }

        [HttpGet]
        public ActionResult EmailNotes(Int64 id)
        {
            try
            {
                ShareEmailViewModel model = new ShareEmailViewModel();
                model.NoteID = id;
                return PartialView(model);
            }
            catch (Exception ex)
            {
                throw;
            }
        }


        [HttpPost]
        public ActionResult EmailNotes(ShareEmailViewModel model)
        {
            try
            {
                NotesManager nManager = new NotesManager();
                var obj = nManager.GetNotes(model.NoteID);
                string filename = obj.FileName;
                string filepath = AppDomain.CurrentDomain.BaseDirectory + "/TeacherNotes/" + obj.FileName;

                string subject = Config.UsersProfile.FirstName + " Share Study notes with you";
                bool status = BLL.EmailTemplate.EmailTemplate.ShareNotes(model.Email, subject, filepath, model.Message, obj.FileName);

                if (status == true)
                {
                    TempData["Message"] = "Notes Shared Succesfully.";
                    TempData["Status"] = 2;
                    return RedirectToAction("Notes");
                }
                else
                {

                    TempData["Message"] = "Something went wrong.";
                    TempData["Status"] = 3;
                    return RedirectToAction("Notes");
                }

                return View();
            }
            catch (Exception ex)
            {
                throw;
            }
        }



        #region [Notifications Section]

        public ActionResult AllNotification()
        {

            if (Config.CurrentUser == 0)
                return RedirectToAction("Login", "Account", new { area = "" });

            if (Config.User.UserType != (int)Enumerations.UserType.Student)
                return RedirectToAction("UnAuthorizeAccess", "Home", new { area = "" });

            try
            {

                List<UserNotifications> list = new List<UserNotifications>();
                NotificationManager nManager = new NotificationManager();
                list = nManager.ListofUserNotifications(Config.CurrentUser);
                var update = db.UserNotification.Where(p => p.IsSeen != true).ToList();
                foreach (var item in update)
                {

                    var user = db.Users.Where(o => o.UserID == item.FKUserID).FirstOrDefault();
                    if (user.UserType == 4)
                    {
                        item.IsSeen = true;
                    }
                }
                db.SaveChanges();
                return View(list);
            }
            catch (Exception ex)
            {

                throw;
            }






        }



        #endregion


    }
}

