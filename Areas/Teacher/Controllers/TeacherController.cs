using BLL.BusinessModels.TeacherModels;
using BLL.Helpers;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using BLL.BusinessManagers.TeacherManager;
using DAL.Entities;
using BLL.BusinessManagers.UserManager;
using BLL.BusinessManagers.SessionsManager;
using BLL.BusinessModels.CalenderModels;
using BLL.EmailTemplate;
using BLL.BusinessModels.UserModels;
using DAL.MasterEntity;
using System.IO;
using BLL.BusinessManagers.AdminManager;
using BLL.BusinessManagers;
using BigBlueButtonSDK;
using BLL.BusinessManagers.StudentManager;
using BLL.BusinessModels.AdminModels;

namespace Tutoring.Areas.Teacher.Controllers
{
    public class TeacherController : Controller
    {
        //string BibBlueServer_URL = "http://50.21.176.216/bigbluebutton/";
        //string BibBlueServer_SecSalt = "8227be0510f0931941f59bf42558f3d4";
        //string BibBlueServer_Name = "Debugging";
        //string BibBlueServer_Password = "123456";
        //string BibBlueServer_AttendeePassword = "654321";


        //http://tutor.teachingbharat.com/bigbluebutton/api/create?meetingID=DAL.Entities.Sessions&name=Debugging&moderatorPW=123456&attendeePW=654321&checksum=1220f58e21878dc4608b95fca4a8e59484854754
        string BigBlueServer_URL = "http://tutor.teachingbharat.com/bigbluebutton/";
       // string BigBlueServer_URL = "https://tutor.teachingbharat.com/b/shu-fz4-2zt";
        string BigBlueServer_SecSalt = "afc6cab8ef4524b8b4638264ae369fd8";
        string BigBlueServer_Name = "Debugging";
        string BigBlueServer_UserName = "shubhras@yahoo.com";
        string BigBlueServer_Password = "123456";
        string BigBlueServer_AttendeePassword = "654321";
      


        CreateMeeting createMeeting = new CreateMeeting();
        CreateMeetingRequest createRequest = new CreateMeetingRequest();
        CreateMeetingResponse createResponse = new CreateMeetingResponse();

        JoinMeeting joinMeeting = new JoinMeeting();
        JoinMeetingRequest joinRequest = new JoinMeetingRequest();
        EndMeeting endMeeting = new EndMeeting();
        EndMeetingRequest endMeetingRequest = new EndMeetingRequest();
        EndMeetingResponse endMeetingResponse = new EndMeetingResponse();



        CurriculumsManagers manager = new CurriculumsManagers();
        UsersActivityManager ActivityMgr = new UsersActivityManager();
        UserActivities obj_Activity = null;
        MyDbContext db = new MyDbContext();

        // GET: Teacher/Teacher
        public ActionResult Index()
        {

            if (Config.CurrentUser == 0)
                return RedirectToAction("Login", "Account", new { area = "" });

            if (Config.User.UserType != (int)Enumerations.UserType.Teacher)
                return RedirectToAction("UnAuthorizeAccess", "Home", new { area = "" });

            SessionManager manager = new SessionManager();
            TeacherDashboardViewModel model = new TeacherDashboardViewModel();
            model.FirstName = Config.UsersProfile.FirstName;
            model.LastName = Config.UsersProfile.LastName;
            TeacherQualification obj = new TeacherQualification();
            UserBLL userBLL = new UserBLL();
            obj = userBLL.GetUserQualification(Config.CurrentUser);
            var user = userBLL.GetUserProfile(Config.CurrentUser);

            if (obj.QualificationName != null)
                model.Qualification = obj.QualificationName;
            else
                model.Qualification = null;


        

            model.InstituteName = obj.UniversityName;
            model.listOfSession = manager.TeacherPendingSessions(Config.CurrentUser);
            model.listOfUpcomingSession = manager.TeacherUpcomingSessions(Config.CurrentUser);
            model.listOfWeeklyReport= manager.StudentWeeklyReport(Config.CurrentUser);
            model.MyTotalEarnings = manager.TeacherTotalEarnings(Config.CurrentUser);
            model.MyStudentsCount = manager.TeacherTotalStudents(Config.CurrentUser);
            model.MySessionsCount = manager.TeacherSessionCount(Config.CurrentUser);
            model.listOfMyStudents = manager.MyStudents(Config.CurrentUser);


            NotificationManager nManager = new NotificationManager();
            model.listOfMyNotifications = new List<UserNotifications>();
            model.listOfMyNotifications = nManager.ListofUserNotifications(Config.CurrentUser);

            if (model.listOfMyNotifications != null && model.listOfMyNotifications.Count > 0)
            {
                model.listOfMyNotifications = model.listOfMyNotifications.Take(3).ToList();
            }

            ViewBag.Status = (TempData["Status"] != null) ? (int)TempData["Status"] : 0;
            ViewBag.Message = (TempData["Message"] != null) ? (string)TempData["Message"] : null;

            model.ProfileImage = user.ProfilePic;
            model.SessionCost = user.SessionCost;
         
            model.CourseCount = manager.TeacherCourseCount(Config.CurrentUser);
            return View(model);
        }

        public ActionResult MyCalender()
        {
            if (Config.CurrentUser == 0)
                return RedirectToAction("Login", "Account", new { area = "" });

            if (Config.User.UserType != (int)Enumerations.UserType.Teacher)
                return RedirectToAction("UnAuthorizeAccess", "Home", new { area = "" });

            UserBLL uBLL = new UserBLL();
            List<CalenderDateViewModel> list = new List<CalenderDateViewModel>();
            list = uBLL.TeacherBookedSessionsTiming(Config.CurrentUser);
            return View(list);
         
        }
        public ActionResult conncet_bbb()
        {

            FillDefaultCreateMeetingRequestAndResponse();
            createMeeting.Request = createRequest;
            createMeeting.Response = createResponse;
            createMeeting.Response = createRequest.Execute();

            FillDefaultJoinMeetingRequestAndResponse();
            joinMeeting.Request = joinRequest;



            JoinMeetingResponse joinResponse = joinRequest.Execute();
            joinMeeting.Response = joinResponse;

            joinResponse = joinRequest.Execute();
            if (joinResponse.Response.Length > 0) //if everything is ok
            {
                Json(new { url = joinRequest.RequestURL }, JsonRequestBehavior.AllowGet);
            }
            else //if something goes wrong
            {
                joinMeeting.Response = joinResponse;
            }
            return new RedirectResult(joinRequest.RequestURL);

        }
        public ActionResult conncet_bbb_student()
        {

            //FillDefaultCreateMeetingRequestAndResponse();
            //createMeeting.Request = createRequest;
            //createMeeting.Response = createResponse;
            //createMeeting.Response = createRequest.Execute();

            FillDefaultJoinMeetingRequestAndResponse();
            joinMeeting.Request = joinRequest;



            JoinMeetingResponse joinResponse = joinRequest.Execute();
            joinMeeting.Response = joinResponse;

            joinResponse = joinRequest.Execute();
            if (joinResponse.Response.Length > 0) //if everything is ok
            {
                Json(new { url = joinRequest.RequestURL }, JsonRequestBehavior.AllowGet);
            }
            else //if something goes wrong
            {
                joinMeeting.Response = joinResponse;
            }
            return new RedirectResult(joinRequest.RequestURL);

        }
        private void FillDefaultCreateMeetingRequestAndResponse()
        {
            SessionManager sManager = new SessionManager();
            TeacherManager manager = new TeacherManager();
            var list = manager.ListOfTeacherSessions(Config.CurrentUser);
            var eventObj = new Sessions();
            foreach (var item in list)
            {
                eventObj = sManager.GetSessionDetail(item.TrialSessionID);

            }
            //string ddss = Session["otp"].ToString();

            createRequest.ServerURL = BigBlueServer_URL;
            createRequest.SecuritySalt = BigBlueServer_SecSalt;// "8227be0510f0931941f59bf42558f3d4";
            createRequest.Name = BigBlueServer_Name;//"Debugging";
            createRequest.ModeratorPassword = BigBlueServer_Password;// "123456";
            createRequest.AttendeePassword = BigBlueServer_AttendeePassword;// "654321";
            //createRequest.AllowStartStopRecording = true;
            //createRequest.LogoutURL = "http://news.ridan.local/bigbluebutton";
            //createRequest.MeetingID = "e0ae6dfb996a0e0ea85bd7a47359bfc994d646ee";
            createRequest.MeetingID = eventObj.ToString();
            createResponse.Response = @"    <response> 
        <returncode>SUCCESS</returncode> 
        <meeting> 
            <meetingID>Test</meetingID> 
            <createTime>1308591802</createTime> 
            <attendeePW>ap</attendeePW> 
            <moderatorPW>mp</moderatorPW> 
            <hasBeenForciblyEnded>false</hasBeenForciblyEnded> 
            <messageKey>createSuccess</messageKey> 
            <message>Meeting has been create</message> 
        </meeting> 
    </response> ";


        }
        private void FillDefaultJoinMeetingRequestAndResponse()
        {
            SessionManager sManager = new SessionManager();
            TeacherManager manager = new TeacherManager();
            var list = manager.ListOfTeacherSessions(Config.CurrentUser);
            var eventObj = new Sessions();
            foreach (var item in list)
            {
                eventObj = sManager.GetSessionDetail(item.TrialSessionID);

            }
            //string ddss = Session["otp"].ToString();
            joinRequest.MeetingID = eventObj.ToString();
            //joinRequest.MeetingID = "e0ae6dfb996a0e0ea85bd7a47359bfc994d646ee";
            joinRequest.FullName = Config.UsersProfile.FirstName;

            //joinRequest.ServerURL = "http://50.21.176.216/bigbluebutton/";
            //joinRequest.SecuritySalt = "8227be0510f0931941f59bf42558f3d4";
            //joinRequest.Password = "123456";

            joinRequest.ServerURL = BigBlueServer_URL;
            //joinRequest.UserID = BigBlueServer_UserName;
            joinRequest.SecuritySalt = BigBlueServer_SecSalt;
            
            joinRequest.Password = BigBlueServer_Password;


        }
        public ActionResult Cancel_BBB()
        {
            FillDefaultEndMeetingRequestAndResponse();
            endMeeting.Request = endMeetingRequest;
            endMeeting.Response = endMeetingResponse;
            endMeeting.Response = endMeetingRequest.Execute();
            return RedirectToAction("UpComingSessions", "Teacher", "Teacher");
        }
        private void FillDefaultEndMeetingRequestAndResponse()
        {
            SessionManager sManager = new SessionManager();
            TeacherManager manager = new TeacherManager();
            var list = manager.ListOfTeacherSessions(Config.CurrentUser);
            var eventObj = new Sessions();
            foreach (var item in list)
            {
                eventObj = sManager.GetSessionDetail(item.TrialSessionID);

            }
            //string ddss = Session["otp"].ToString();
            endMeetingRequest.MeetingID = eventObj.ToString();
            endMeetingRequest.Password = BigBlueServer_Password;// "123456";
            endMeetingRequest.ServerURL = BigBlueServer_URL;// "http://50.21.176.216/bigbluebutton/";
            endMeetingRequest.SecuritySalt = BigBlueServer_SecSalt;// "8227be0510f0931941f59bf42558f3d4";
        }
        public ActionResult SetAvailability(string value)
        {
            if (Config.CurrentUser == 0)
                return RedirectToAction("Login", "Account", new { area = "" });

            if (Config.User.UserType != (int)Enumerations.UserType.Teacher)
                return RedirectToAction("UnAuthorizeAccess", "Home", new { area = "" });

           // ViewBag.ConfirmationMessage = value;

            return View();
        }

        public ActionResult TeacherSetAvailability(string value)
        {
            if (Config.CurrentUser == 0)
                return RedirectToAction("Login", "Account", new { area = "" });

            if (Config.User.UserType != (int)Enumerations.UserType.Teacher)
                return RedirectToAction("UnAuthorizeAccess", "Home", new { area = "" });

            //  TeacherManager TeacherManager = new TeacherManager();
            //  List<TeacherSetAvailabilityModel> list = TeacherManager.ListOfLayout();
            // List<TeacherSetAvailabilityModel> list = new List<TeacherSetAvailabilityModel>();
            // return View(list);

            UserBLL uBLL = new UserBLL();
            List<CalenderDateViewModel> list = new List<CalenderDateViewModel>();
            list = uBLL.TeacherBookedSessionsTiming(Config.CurrentUser);
            return View(list);
        }

        public ActionResult setTeacherAvailability(DateTime time)
        {
            TeacherManager TeacherManager = new TeacherManager();
            bool check = TeacherManager.CheckAvailability(Config.CurrentUser, time);
            if(check==true)
            {
                int id= TeacherManager.UpdateTeacherAvailability(Config.CurrentUser, time);
                if (id > 0)
                {
                    return Json(true, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(false, JsonRequestBehavior.AllowGet);
                }
            }
            else
            {
                TeachersAvailabilityViewModel model = new TeachersAvailabilityViewModel();
                model.IsActive = true;
                model.FKTeacherID = Config.CurrentUser;
                model.AvailableTime = time;
                int id = TeacherManager.SetAvailability(model);
                if (id > 0)
                {
                    return Json(true, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(false, JsonRequestBehavior.AllowGet);
                }
            }
            return null;
        }

        [HttpPost]
        public ActionResult SetAvailability(TeachersAvailabilityViewModel model)
        {
            if (Config.CurrentUser == 0)
                return RedirectToAction("Login", "Account", new { area = "" });

            if (Config.User.UserType != (int)Enumerations.UserType.Teacher)
                return RedirectToAction("UnAuthorizeAccess", "Home", new { area = "" });

            model.IsActive = true;
            model.FKTeacherID = Config.CurrentUser;
            if(ModelState.IsValid)
            { 
            TeacherManager TeacherManager = new TeacherManager();
            int id = TeacherManager.SetAvailability(model);

                //if (id == (int)Enumerations.ResponsHelpers.Alreadyexsists)
                //{
                //    ViewBag.Response = 1;
                //}
                //else
                //{
                //    ViewBag.Response = 0;
                //}
            string msg = MessagePrinter(id);
            return Content(msg,"text/html");
      
            }
            else
            {
                ModelState.AddModelError("","Date Time is Required");
            }
            return null;
        }        
               
        public string MessagePrinter(int response)
        {
            string message;
            if(response==(int)Enumerations.ResponsHelpers.Alreadyexsists)
            {
                message = "Record Already Exists";
            }
            else if (response == (int)Enumerations.ResponsHelpers.Block)
            {
                message = "User Blocked";
            }
            else if (response == (int)Enumerations.ResponsHelpers.Created)
            {
                message = "Record Saved Succesfully";
            }
            else if (response == (int)Enumerations.ResponsHelpers.Deleted)
            {
                message = "Record Deleted Succesfully";
            }
            else if (response == (int)Enumerations.ResponsHelpers.Doesnotexsists)
            {
                message = "Record Does not Exist";
            }
            else if (response == (int)Enumerations.ResponsHelpers.EmailSentFail)
            {
                message = "Email Sent Failure. Please try again";
            }
            else if (response == (int)Enumerations.ResponsHelpers.Error)
            {
                message = "Something went wrong. Please contact with Administrator";
            }
            else if (response == (int)Enumerations.ResponsHelpers.Updated)
            {
                message = "Record Updated Succesfully";
            }
            else 
            {
                message = "Something went wrong. Please contact with Administrator";
            }



            return message;
        }
        
        public ActionResult TeacherProfile()
        {
            if (Config.CurrentUser == 0)
                return RedirectToAction("Login", "Account", new { area = "" });

            if (Config.User.UserType != (int)Enumerations.UserType.Teacher)
                return RedirectToAction("UnAuthorizeAccess", "Home", new { area = "" });

            ViewBag.Status = (TempData["Status"] != null) ? (int)TempData["Status"] : 0;
            ViewBag.Message = (TempData["Message"] != null) ? (string)TempData["Message"] : null;

            UserBLL userBLL = new UserBLL();
            UsersProfile user = new UsersProfile();
            user = userBLL.GetUserProfile(Config.CurrentUser);

            TeacherDashboardViewModel model = new TeacherDashboardViewModel();

            model.FirstName = user.FirstName;
            model.LastName = user.LastName;
            model.Experience = user.ExperienceName;
            model.StateName = user.StateName;
            model.City = user.CityName;
            model.ContactNumber = user.ContactNumber;
            model.Email = Config.User.LoginEmail;
            TeacherQualification obj = new TeacherQualification();
          
            obj = userBLL.GetUserQualification(Config.CurrentUser);
            model.Qualification = obj.QualificationName;
            model.InstituteName = obj.UniversityName;
            
            model.listOfSubjects = userBLL.ListOfTeacherSubjects(Config.CurrentUser);
            model.listOfTeachersAvailability = userBLL.ListOfTeacherAvailability(Config.CurrentUser);
            model.listOfDates = userBLL.ListOfTeacherTimes(Config.CurrentUser);
            model.MyProfileViewModel= userBLL.GetMyProfile(Config.CurrentUser);
            model.ProfileImage = user.ProfilePic;
            model.SessionCost = user.SessionCost;

            RatingsManager rManager = new RatingsManager();
            model.RatingsModel = rManager.ListOfRatings(Config.CurrentUser);
            model.RatingsModel.ReviewsList = model.RatingsModel.ReviewsList.OrderByDescending(x => x.CreatedDate).Take(4).ToList();
            return View(model);
        }

        public ActionResult MySessions()
        {

            if (Config.CurrentUser == 0)
                return RedirectToAction("Login", "Account", new { area = "" });

            if (Config.User.UserType != (int)Enumerations.UserType.Teacher)
                return RedirectToAction("UnAuthorizeAccess", "Home", new { area = "" });

            int status;
            string message;
            int.TryParse(Convert.ToString(Request.Params["status"]), out status);
            message = Convert.ToString(Request.Params["message"]);
            ViewBag.Status = status;
            ViewBag.Message = message;

            SessionManager manager = new SessionManager();
            TeacherDashboardViewModel model = new TeacherDashboardViewModel();
            model.listOfSession = manager.TeacherSessions(Config.CurrentUser);
            model.listOfSession = model.listOfSession.OrderByDescending(x => x.RequestDate).ToList();

            return View(model);

        }

        public ActionResult UpdateMySession()
        {

            if (Config.CurrentUser == 0)
                return RedirectToAction("Login", "Account", new { area = "" });

            if (Config.User.UserType != (int)Enumerations.UserType.Teacher)
                return RedirectToAction("UnAuthorizeAccess", "Home", new { area = "" });

            SessionManager manager = new SessionManager();

            MySessions mSessions = new MySessions();
            mSessions = manager.UpdatedMySession(Config.CurrentUser);

            return View(mSessions);
            
        }

        public ActionResult UpComingSessions()
        {

            if (Config.CurrentUser == 0)
                return RedirectToAction("Login", "Account", new { area = "" });

            if (Config.User.UserType != (int)Enumerations.UserType.Teacher)
                return RedirectToAction("UnAuthorizeAccess", "Home", new { area = "" });

            SessionManager manager = new SessionManager();

            MySessions mSessions = new MySessions();
            mSessions = manager.UpdatedMySession(Config.CurrentUser);

            return View(mSessions);

        }

        public ActionResult PendingSessions()
        {

            if (Config.CurrentUser == 0)
                return RedirectToAction("Login", "Account", new { area = "" });

            if (Config.User.UserType != (int)Enumerations.UserType.Teacher)
                return RedirectToAction("UnAuthorizeAccess", "Home", new { area = "" });

            int status;
            string message;
            int.TryParse(Convert.ToString(Request.Params["status"]), out status);
            message = Convert.ToString(Request.Params["message"]);
            ViewBag.Status = status;
            ViewBag.Message = message;

            SessionManager manager = new SessionManager();

            MySessions mSessions = new MySessions();
            mSessions = manager.UpdatedMySession(Config.CurrentUser);

            return View(mSessions);

        }

        public ActionResult CanceledSessions()
        {

            if (Config.CurrentUser == 0)
                return RedirectToAction("Login", "Account", new { area = "" });

            if (Config.User.UserType != (int)Enumerations.UserType.Teacher)
                return RedirectToAction("UnAuthorizeAccess", "Home", new { area = "" });

            SessionManager manager = new SessionManager();

            MySessions mSessions = new MySessions();
            mSessions = manager.UpdatedMySession(Config.CurrentUser);

            return View(mSessions);

        }
        public ActionResult PastSessions()
        {

            if (Config.CurrentUser == 0)
                return RedirectToAction("Login", "Account", new { area = "" });

            if (Config.User.UserType != (int)Enumerations.UserType.Teacher)
                return RedirectToAction("UnAuthorizeAccess", "Home", new { area = "" });

            SessionManager manager = new SessionManager();

            MySessions mSessions = new MySessions();
            mSessions = manager.UpdatedMySession(Config.CurrentUser);

            return View(mSessions);

        }



        public ActionResult SessionModalWindow(Int64 id)
        {
            SessionManager manager = new SessionManager();
            Sessions obj = manager.GetSessionDetail(id);
            return PartialView(obj);
        }

        public ActionResult AcceptRequest(Int64 SessionID)
        {
            if (Config.CurrentUser == 0)
                return RedirectToAction("Login", "Account", new { area = "" });

            if (Config.User.UserType != (int)Enumerations.UserType.Teacher)
                return RedirectToAction("UnAuthorizeAccess", "Home", new { area = "" });

            SessionManager manager = new SessionManager();
            int status = manager.ChangeSessionRequestStatus(SessionID,(int)Enumerations.RequestStatus.Accepted);
            

            return RedirectToAction("MySessions", new { @Status = 2, @Message = "Request Status updated succesfully." });
        }

        public ActionResult RejectRequest(Int64 SessionID)
        {
            if (Config.CurrentUser == 0)
                return RedirectToAction("Login", "Account", new { area = "" });

            if (Config.User.UserType != (int)Enumerations.UserType.Teacher)
                return RedirectToAction("UnAuthorizeAccess", "Home", new { area = "" });

            SessionManager manager = new SessionManager();
            int status = manager.ChangeSessionRequestStatus(SessionID, (int)Enumerations.RequestStatus.Rejected);

            return RedirectToAction("MySessions", new { @Status = 2, @Message = "Request Status updated succesfully." });
        }

        [HttpGet]
        public JsonResult SerializeEvent()
        {
            TeacherManager manager = new TeacherManager();
            SessionManager sManager = new SessionManager();
            var list = manager.ListOfTeacherSessions(Config.CurrentUser);
            var eventList = new List<FullCalender>();
            foreach (var item in list)
            {
                var eventObj = new Sessions();
                eventObj = sManager.GetSessionDetail(item.TrialSessionID);
                var eventObjs = new FullCalender();
                eventObjs.start= eventObj.TrialHappenDate.ToString();
                eventObjs.end = eventObj.TrialHappenDate.AddHours(1).ToString();
                eventObjs.title = eventObj.TopicName;
             //   eventObjs.allDay = false;
               
                eventList.Add(eventObjs);
            }


            // Add Courses Schedule To Calender

            List<CourseSchedule> StudentCourses = new List<CourseSchedule>();
            CourseOfferingsManager cManager = new CourseOfferingsManager();
            StudentCourses = cManager.GetTeacherCourseSchedule(Config.CurrentUser);
            if (StudentCourses != null && StudentCourses.Count > 0)
            {
                foreach (var item in StudentCourses)
                {
                    var eventObjs = new FullCalender();

                    eventObjs.start = item.CourseSessionDate.ToString();
                    eventObjs.end = item.CourseSessionDate.AddHours(1).ToString();
                    eventObjs.title = "Course Offering";

                    eventList.Add(eventObjs);

                }
            }

            return Json(eventList, JsonRequestBehavior.AllowGet);
        }

        public ActionResult JoinSessions()
        {
            if (Config.CurrentUser == 0)
                return RedirectToAction("Login", "Account", new { area = "" });

            if (Config.User.UserType != (int)Enumerations.UserType.Teacher)
                return RedirectToAction("UnAuthorizeAccess", "Home", new { area = "" });
            SessionManager manager = new SessionManager();

            Sessions obj = manager.TeacherJoiningSession(Config.CurrentUser);

            return View(obj);
        }

        public ActionResult SetAvailabilityPartial(int? counts,DateTime? date)
        {

            if (Config.CurrentUser == 0)
                return RedirectToAction("Login", "Account", new { area = "" });

            if (Config.User.UserType != (int)Enumerations.UserType.Teacher)
                return RedirectToAction("UnAuthorizeAccess", "Home", new { area = "" });

            TeacherManager TeacherManager = new TeacherManager();
            CustomTimeZone time = new CustomTimeZone();
            
       
            if (Request.IsAjaxRequest())
            {
                //Not null condition
                int count = (counts.HasValue) ? counts.Value : 0;
                var dates = (date.HasValue) ? date : DateTime.Now;
                List<TeacherSetAvailabilityModel> list = TeacherManager.ListOfLayout1(count,(DateTime)dates);
                var check = list.First();
                DateTime test = check.RealTime.First().Date;
                var test1 = time.DateTimeNow().Date;

                if (test==test1)
                {
                    ViewBag.Previous = false;
                }
                else
                {
                    ViewBag.Previous = true;
                }
                return PartialView(list);
            }
           else
            {
                List<TeacherSetAvailabilityModel> list = TeacherManager.ListOfLayout();
                var check = list.First();
                DateTime test = check.RealTime.First().Date;
                var test1 = time.DateTimeNow().Date;

                if (test == test1)
                {
                    ViewBag.Previous = false;
                }
                else
                {
                    ViewBag.Previous = true;
                }
                return PartialView(list);
            }

            return PartialView();
        }

        public ActionResult SubmitSupport(string Subject, string Messages)
        {
            bool result = BLL.EmailTemplate.EmailTemplate.SupportEmail(Subject, Messages, Config.User.LoginEmail);
            if(result==true)
            {
                TempData["Message"] = "Your message to support send Succesfully";
                TempData["Status"] = 2;
            }
            else
            {
                TempData["Message"] = "Your message not delivered to support.";
                TempData["Status"] = 3;
            }
            return RedirectToAction("");
        }

        public ActionResult ChangePassword()
        {
            if (Config.CurrentUser == 0)
                return RedirectToAction("Login", "Account", new { area = "" });

            if (Config.User.UserType != (int)Enumerations.UserType.Teacher)
                return RedirectToAction("UnAuthorizeAccess", "Home", new { area = "" });

            return View();
        }

        [HttpPost]
        public ActionResult ChangePassword(TeachersChangePasswordViewModel model)
        {
            if (Config.CurrentUser == 0)
                return RedirectToAction("Login", "Account", new { area = "" });

            if (Config.User.UserType != (int)Enumerations.UserType.Teacher)
                return RedirectToAction("UnAuthorizeAccess", "Home", new { area = "" });

            UserBLL userBLL = new UserBLL();
            model.UserID = Config.CurrentUser;
            int status = userBLL.TeachersChangePassword(model);
            if (status > 0)
            {
                TempData["Message"] = "Password Changed Succesfully";
                TempData["Status"] = 2;
                return RedirectToAction("TeacherProfile");
            }
            else
            {
                TempData["Message"] = "Something went wrong. Please try Later.";
                TempData["Status"] = 3;
                return RedirectToAction("TeacherProfile");
            }
            ModelState.AddModelError("", "Someting went wrong. Please try Later");
            return View();

        }


        [HttpGet]
        public ActionResult EditProfile()
        {
            if (Config.CurrentUser == 0)
                return RedirectToAction("Login", "Account", new { area = "" });

            if (Config.User.UserType != (int)Enumerations.UserType.Teacher)
                return RedirectToAction("UnAuthorizeAccess", "Home", new { area = "" });

            UserBLL userBLL = new UserBLL();
            TeacherEditProfileViewModel GetMyProfile = userBLL.TeacherEditMyProfile(Config.CurrentUser);
            GetMyProfile.listOfSubjects = userBLL.ListOfTeacherSubjects(Config.CurrentUser);

            RatingsManager rManager = new RatingsManager();
            GetMyProfile.RatingsModel = rManager.ListOfRatings(Config.CurrentUser);
            GetMyProfile.RatingsModel.ReviewsList = GetMyProfile.RatingsModel.ReviewsList.OrderByDescending(x => x.CreatedDate).Take(4).ToList();


            return View(GetMyProfile);
        }

        [HttpPost]
        public ActionResult EditProfile(HttpPostedFileBase PostedFile, TeacherEditProfileViewModel obj)
        {
            if (Config.CurrentUser == 0)
                return RedirectToAction("Login", "Account", new { area = "" });

            if (Config.User.UserType != (int)Enumerations.UserType.Teacher)
                return RedirectToAction("UnAuthorizeAccess", "Home", new { area = "" });

            UserBLL userBLL = new UserBLL();
            obj.UserID = Config.CurrentUser;

            if (PostedFile != null && PostedFile.ContentLength > 0)
            {
                var extension = Path.GetExtension(PostedFile.FileName).ToLower();
                Guid id = Guid.NewGuid();
                string path = Server.MapPath("~/UserImages/" + id + extension);
                PostedFile.SaveAs(path);
                obj.ProfileImage = id.ToString() + extension;
            }
            else
            {
                obj.ProfileImage = null;
            }
            int status = userBLL.UpdateTeacherProfile(obj);
            if (status > 0)
            {
                TempData["Message"] = "Profile updated succesfully";
                TempData["Status"] = 2;
                return RedirectToAction("TeacherProfile");
            }
            else
            {
                TempData["Message"] = "Something went wrong. Please try Later.";
                TempData["Status"] = 3;
                return RedirectToAction("TeacherProfile");
            }
            return RedirectToAction("TeacherProfile");
        }

        [AllowAnonymous]
        public JsonResult CheckOldPassword(string OldPassword)
        {
            MyDbContext context = new MyDbContext();
            return Json(context.Users.Any(lo => lo.Password == OldPassword && lo.UserID == Config.CurrentUser), JsonRequestBehavior.AllowGet);
        }

        public ActionResult MyAccount()
        {
            if (Config.CurrentUser == 0)
                return RedirectToAction("Login", "Account", new { area = "" });

            if (Config.User.UserType != (int)Enumerations.UserType.Teacher)
                return RedirectToAction("UnAuthorizeAccess", "Home", new { area = "" });

            return View();
        }

        public ActionResult _ViewSubjects()
        {

            if (Config.CurrentUser == 0)
                return RedirectToAction("Login", "Account", new { area = "" });

            if (Config.User.UserType != (int)Enumerations.UserType.Teacher)
                return RedirectToAction("UnAuthorizeAccess", "Home", new { area = "" });

            UserBLL userBLL = new UserBLL();
            TeacherEditProfileViewModel GetMyProfile = userBLL.TeacherEditMyProfile(Config.CurrentUser);
            GetMyProfile.listOfSubjects = userBLL.ListOfTeacherSubjects(Config.CurrentUser);

            ViewBag.Status = (TempData["Status"] != null) ? (int)TempData["Status"] : 0;
            ViewBag.Message = (TempData["Message"] != null) ? (string)TempData["Message"] : null;

            return PartialView(GetMyProfile);
        }

        public ActionResult DeleteCourse(Int64 TeacherSubjectID)
        {
            if (Config.CurrentUser == 0)
                return RedirectToAction("Login", "Account", new { area = "" });

            if (Config.User.UserType != (int)Enumerations.UserType.Teacher)
                return RedirectToAction("UnAuthorizeAccess", "Home", new { area = "" });

            UserBLL uBLL = new UserBLL();
            int status = uBLL.RemoveTeacherSubject(TeacherSubjectID,Config.CurrentUser);
            if(status>0)
            {
                TempData["Message"] = "Subject removed succesfully";
                TempData["Status"] = 2;
            }
            else
            {
                TempData["Message"] = "Something went wrong.";
                TempData["Status"] = 3;
            }
            return RedirectToAction("_ViewSubjects");

        }

        public ActionResult _AddSubject()
        {
            var model = UserManager.SubjectRegisterhelpers();
            return PartialView(model);
        }
        [HttpPost]
        public ActionResult _AddSubject(AddSubjectViewModel model)
        {

            if (Config.CurrentUser == 0)
                return RedirectToAction("Login", "Account", new { area = "" });

            if (Config.User.UserType != (int)Enumerations.UserType.Teacher)
                return RedirectToAction("UnAuthorizeAccess", "Home", new { area = "" });

            UserBLL uBLL = new UserBLL();
            int status = uBLL.AddTeacherSubject(model, Config.CurrentUser);
            int a = 0;
            string b = null;
            if (status ==1)
            {
                a = 1;
                b = "Subject Added Succesfully";
            }
           else if (status == 2)
            {
                a = 0;
                b = "Subject Already Exists";
            }
            else
            {
                a = 0;
                b = "Something went wrong";
            }
           
            return JavaScript("TestMethod('"+ a + "','" + b + "')");
        }

        //[AllowAnonymous]
        //public JsonResult GradesList(int id)
        //{
        //    MyDbContext context = new MyDbContext();

        //    if (id > 0)
        //    {
        //        return Json(
        //           context.Grades.Where(x => x.FKCurriculumID == id && x.IsActive == true).Select(x => new { value = x.GradeID, text = x.GradeName }),
        //            JsonRequestBehavior.AllowGet
        //        );
        //    }
        //    return Json(
        //        null,
        //        JsonRequestBehavior.AllowGet
        //    );
        //}

        [AllowAnonymous]
        public JsonResult GradesListing(int id)
        {
            MyDbContext context = new MyDbContext();

            if (id > 0)
            {
                return Json(
                   context.Grades.Where(x => x.FKCurriculumID == id && x.IsActive == true).Select(x => new { value = x.GradeID, text = x.GradeName }),
                    JsonRequestBehavior.AllowGet
                );
            }
            return Json(
                null,
                JsonRequestBehavior.AllowGet
            );
        }
        public JsonResult SubjectsList(int id)
        {
            MyDbContext context = new MyDbContext();

            if (id > 0)
            {
                return Json(
                  context.Subjects.Where(x => x.FKGradeID == id && x.IsActive == true).Select(x => new { value = x.SubjectID, text = x.SubjectName }),
                   JsonRequestBehavior.AllowGet
               );
            }
            return Json(
                null,
                JsonRequestBehavior.AllowGet
            );
        }

        public ActionResult TryWhiteboard()
        {
            if (Config.CurrentUser == 0)
                return RedirectToAction("Login", "Account", new { area = "" });

            if (Config.User.UserType != (int)Enumerations.UserType.Teacher)
                return RedirectToAction("UnAuthorizeAccess", "Home", new { area = "" });

            return View();
        }

         public ActionResult MySubscriptions()
        {

            if (Config.CurrentUser == 0)
                return RedirectToAction("Login", "Account", new { area = "" });

            if (Config.User.UserType != (int)Enumerations.UserType.Teacher)
                return RedirectToAction("UnAuthorizeAccess", "Home", new { area = "" });

            CourseOfferingsManager cManager = new CourseOfferingsManager();

            List<CoursesHeader> list = new List<CoursesHeader>();
            list = cManager.TeacherSubscriptions(Config.CurrentUser);

            return View(list);

        }


        public ActionResult SubscriptionDetail(Int64 CourseID)
        {
            if (Config.CurrentUser == 0)
                return RedirectToAction("Login", "Account", new { area = "" });

            if (Config.User.UserType != (int)Enumerations.UserType.Teacher)
                return RedirectToAction("UnAuthorizeAccess", "Home", new { area = "" });

            try
            {
                if (CourseID > 0)
                {

                    CourseOfferingsManager cManager = new CourseOfferingsManager();
                    CourseJoinDetails Course = new CourseJoinDetails();
                    Course = cManager.GetTeacherCourseJoinDetail(CourseID);

                    return View(Course);

                }
                else
                {
                    return RedirectToAction("MySubscriptions");
                }

            }
            catch (Exception)
            {
                throw;
            }

        }
        public ActionResult MyCourses()
        {
            if (Config.CurrentUser == 0)
                return RedirectToAction("Login", "Account", new { area = "" });

            if (Config.User.UserType != (int)Enumerations.UserType.Teacher)
                return RedirectToAction("UnAuthorizeAccess", "Home", new { area = "" });

            return View("MyAccount");
        }
        public ActionResult MyStudents()
        {
            if (Config.CurrentUser == 0)
                return RedirectToAction("Login", "Account", new { area = "" });

            if (Config.User.UserType != (int)Enumerations.UserType.Teacher)
                return RedirectToAction("UnAuthorizeAccess", "Home", new { area = "" });

            SessionManager manager = new SessionManager();
            List<UsersProfile> list = new List<UsersProfile>();
            list = manager.MyStudents(Config.CurrentUser);
            return View(list);
           
        }
       
        public ActionResult MyStudents_Filtered(int? flag)
        {
            SessionManager manager = new SessionManager();
            List<UsersProfile> list = new List<UsersProfile>();
            list = manager.MyStudents(Config.CurrentUser);

           

            var studentsIds = TempData["studentIds"] as List<long>;
          
            List<UsersProfile> list_Filtered = new List<UsersProfile>();
          
            list_Filtered = (from s in list
                        where studentsIds.Contains(s.UsersProfileID)
                                               select s).ToList();

            return View("MyStudents",list_Filtered);

        }
        public List<SelectListItem> GenerateTeachersDropDown(List<UserViewModel> teachersList)
        {
            List<SelectListItem> ListOfTeachers = new List<SelectListItem>();
            ListOfTeachers.Add(new SelectListItem { Value = "", Text = "Select Teacher", Selected = false });
            foreach (var teacher in teachersList)
            {
                ListOfTeachers.Add(new SelectListItem { Value = teacher.UserID.ToString(), Text = teacher.UsersProfileViewModel.FirstName + " " + teacher.UsersProfileViewModel.LastName, Selected = false });
            }
            return ListOfTeachers;
        }
        public List<SelectListItem> GenerateSubjectsDropdown(List<Subjects> subjectsList)
        {
            StudentManager stdMgr = new StudentManager();

            List<SelectListItem> ListOfsubjcts = new List<SelectListItem>();
            ListOfsubjcts.Add(new SelectListItem { Value = "", Text = "Select Subject", Selected = false });
            foreach (var s in subjectsList)
            {
                var gradeModel = stdMgr.GetGradeByGradeID(s.FKGradeID);
                ListOfsubjcts.Add(new SelectListItem { Value = s.SubjectID.ToString(), Text = "Grade-" + gradeModel.GradeName + " " + s.SubjectName, Selected = false });
            }
            return ListOfsubjcts;
        }

        public List<SelectListItem> GenerateCitiesDropdown(List<string> citiesList)
        {
            StudentManager stdMgr = new StudentManager();

            List<SelectListItem> ListOfcity = new List<SelectListItem>();
            ListOfcity.Add(new SelectListItem { Value = "", Text = "Select City", Selected = false });
            foreach (var city in citiesList)
            {
                ListOfcity.Add(new SelectListItem { Value = city.ToString(), Text = city, Selected = false });
            }
            return ListOfcity;
        }
        public List<SelectListItem> GenerateGradeDropdown(List<string> gradeList)
        {
           
            SessionManager manager = new SessionManager();
            List<StudentGrades> list = new List<StudentGrades>();
            list =   manager.StudentGrades(Config.CurrentUser);


            List<SelectListItem> ListOfGrades = new List<SelectListItem>();
            ListOfGrades.Add(new SelectListItem { Value = "", Text = "Select Grade", Selected = false });
            foreach (var obj in list)
            {
                ListOfGrades.Add(new SelectListItem { Value = obj.StudentGradeID.ToString(), Text = obj.GradeName.ToString(), Selected = false });
            }
            return ListOfGrades;
        }
        public List<SelectListItem> GenerateStatusDropdown(List<string> citiesList)
        {
            //StudentManager stdMgr = new StudentManager();

            List<SelectListItem> ListOfcity = new List<SelectListItem>();
            ListOfcity.Add(new SelectListItem { Value = "", Text = "Select Status", Selected = false });
            foreach (var city in citiesList)
            {
                if (city == "1")
                {
                    ListOfcity.Add(new SelectListItem { Text = "IsDeleted", Value = city, Selected = false });
                }
                else if (city == "2")
                {
                    ListOfcity.Add(new SelectListItem { Text = "IsBlocked", Value = city, Selected = false });
                }
                else if (city == "3")
                {
                    ListOfcity.Add(new SelectListItem { Text = "IsActive", Value = city, Selected = false });
                }
                else if (city == "4")
                {
                    ListOfcity.Add(new SelectListItem { Text = "IsOnline", Value = city, Selected = false });
                }
                else if (city == "5")
                {
                    ListOfcity.Add(new SelectListItem { Text = "NotApproved", Value = city, Selected = false });
                }
                else if (city == "6")
                {
                    ListOfcity.Add(new SelectListItem { Text = "Approved", Value = city, Selected = false });
                }
                else if (city == "7")
                {
                    ListOfcity.Add(new SelectListItem { Text = "Pending", Value = city, Selected = false });
                }
                //ListOfcity.Add(new SelectListItem { Value = city.ToString(), Text = city, Selected = false });
            }
            return ListOfcity;
        }
        public ActionResult FilterMethod(string request)
        {
            if (Config.CurrentUser == 0)
                return RedirectToAction("Login", "Account", new { area = "" });

           
                TeacherManager teacherMgr = new TeacherManager();
                StudentManager studentMgr = new StudentManager();
                UserViewModel userModel = new UserViewModel();

                if (request == "FilterByTeacher")
                {
                    // it will give all teachers
                    var teachersList = teacherMgr.GetUsersByUserType((int)Enumerations.UserType.Teacher);
                    var teachersListItems = GenerateTeachersDropDown(teachersList);
                    return Json(new { Success = "FilterByTeacher", teachersList = teachersListItems });
                }
                if (request == "FilterByGrade")
                {
                    // it will give all grades
                    var gradesList = studentMgr.GetGradesList();
                   // var gradesListItems = userModel.GetGradesList(gradesList);
                    var gradesListItems = GenerateGradeDropdown(null);
                return Json(new { Success = "FilterByGrade", gradesList = gradesListItems });
                }
                if (request == "FilterBySubject")
                {
                    var subjectsList = studentMgr.GetSubjectsList();
                    var subjectsListItems = GenerateSubjectsDropdown(subjectsList);
                    return Json(new { Success = "FilterBySubject", subjectsList = subjectsListItems });

                }
                if (request == "FilterByCity")
                {
                    var citiesList = studentMgr.GetAllCities();
                    var citiesListItems = GenerateCitiesDropdown(citiesList);
                    return Json(new { Success = "FilterByCity", citiesList = citiesListItems });

                }
                if (request == "FilterByStatus")
                {
                    var citiesList = studentMgr.GetAllStatus();
                    var citiesListItems = GenerateStatusDropdown(citiesList);
                    return Json(new { Success = "FilterByStatus", citiesList = citiesListItems });

                }
                return Json(new { Success = "true" });
            
           

        }
        [HttpPost]
        public ActionResult ApplyFilter(string mainfilterRequest, string subfilterRequest)
        {

                if (Config.CurrentUser == 0)
                    return RedirectToAction("Login", "Account", new { area = "" });

                if (Config.User.UserType != (int)Enumerations.UserType.Teacher)
                    return RedirectToAction("UnAuthorizeAccess", "Home", new { area = "" });

                TempData["mainfilterValue"] = null;
                TempData["subfilterValue"] = null;
                TempData["studentIds"] = null;

                if (subfilterRequest.Trim().Length == 0)
                {
                    return RedirectToAction("MyStudents");
                }
                SessionManager sessionMgr = new SessionManager();
                StudentManager studentMgr = new StudentManager();

                if (mainfilterRequest == "FilterByTeacher")
                {
                    var studentsList = studentMgr.MyStudents(int.Parse(subfilterRequest));

                    TempData["studentIds"] = studentsList;
                    TempData["mainfilterValue"] = mainfilterRequest;
                    TempData["subfilterValue"] = subfilterRequest;
                
                    return RedirectToAction("MyStudents_Filtered", new { flag = 1 });

                }
                if (mainfilterRequest == "FilterByGrade")
                {
                    //var studentsList = studentMgr.GetUserProfileByGradeID(int.Parse(subfilterRequest));

                    TempData["studentIds"] = "0";
                    TempData["mainfilterValue"] = mainfilterRequest;
                    TempData["subfilterValue"] = subfilterRequest;

                SessionManager manager = new SessionManager();
                List<UsersProfile> list = new List<UsersProfile>();
                list = manager.MyStudents_ForGrade(Config.CurrentUser,Convert.ToInt32(subfilterRequest));

                return View("MyStudents", list);

                }
                if (mainfilterRequest == "FilterBySubject")
                {
                    var studentsList = studentMgr.GetStudentsBySubjectID(int.Parse(subfilterRequest));

                    TempData["studentIds"] = studentsList;
                    TempData["mainfilterValue"] = mainfilterRequest;
                    TempData["subfilterValue"] = subfilterRequest;

                    return RedirectToAction("MyStudents_Filtered", new { flag = 3 });

                }
                if (mainfilterRequest == "FilterByCity")
                {
                    var studentsList = studentMgr.GetStudentsByCity(subfilterRequest);

                    TempData["studentIds"] = "0";
                    TempData["mainfilterValue"] = mainfilterRequest;
                    TempData["subfilterValue"] = subfilterRequest;
                SessionManager manager = new SessionManager();
                List<UsersProfile> list = new List<UsersProfile>();
                list = manager.MyStudents_ForCity(Config.CurrentUser, subfilterRequest);

                return View("MyStudents", list);
                
                   
                }
                if (mainfilterRequest == "FilterByStatus")
                {
                    var studentsList = studentMgr.GetStudentsByStatus(int.Parse(subfilterRequest));

                    TempData["studentIds"] = studentsList;
                    TempData["mainfilterValue"] = mainfilterRequest;
                    TempData["subfilterValue"] = subfilterRequest;

                    return RedirectToAction("MyStudents_Filtered", new { flag = 4 });
                }

            return RedirectToAction("MyStudents");



        }

        public ActionResult MyStudentProfile(Int64 UserID)
        {
            if (Config.CurrentUser == 0)
                return RedirectToAction("Login", "Account", new { area = "" });

            if (Config.User.UserType != (int)Enumerations.UserType.Teacher)
                return RedirectToAction("UnAuthorizeAccess", "Home", new { area = "" });

            UserBLL userBLL = new UserBLL();
            MyProfileViewModel GetMyProfile = userBLL.GetMyProfile(UserID);

            return View(GetMyProfile);

        }

        public ActionResult TestAssignment()
        {
            if (Config.CurrentUser == 0)
                return RedirectToAction("Login", "Account", new { area = "" });

            if (Config.User.UserType != (int)Enumerations.UserType.Teacher)
                return RedirectToAction("UnAuthorizeAccess", "Home", new { area = "" });

            return View("MyAccount");
        }

        public ActionResult Notes()
        {
            if (Config.CurrentUser == 0)
                return RedirectToAction("Login", "Account", new { area = "" });

            if (Config.User.UserType != (int)Enumerations.UserType.Teacher)
                return RedirectToAction("UnAuthorizeAccess", "Home", new { area = "" });

            return View("MyAccount");
        }

        public ActionResult SystemCheck()
        {
            if (Config.CurrentUser == 0)
                return RedirectToAction("Login", "Account", new { area = "" });

            if (Config.User.UserType != (int)Enumerations.UserType.Teacher)
                return RedirectToAction("UnAuthorizeAccess", "Home", new { area = "" });

            return View();
        }
        public ActionResult Support()
        {
            if (Config.CurrentUser == 0)
                return RedirectToAction("Login", "Account", new { area = "" });

            if (Config.User.UserType != (int)Enumerations.UserType.Teacher)
                return RedirectToAction("UnAuthorizeAccess", "Home", new { area = "" });

            return View();
        }

        public ActionResult AllNotifications()
        {
            if (Config.CurrentUser == 0)
                return RedirectToAction("Login", "Account", new { area = "" });

            if (Config.User.UserType != (int)Enumerations.UserType.Teacher)
                return RedirectToAction("UnAuthorizeAccess", "Home", new { area = "" });

            return View();
        }

        public ActionResult SessionsScheduled()
        {
            if (Config.CurrentUser == 0)
                return RedirectToAction("Login", "Account", new { area = "" });

            if (Config.User.UserType != (int)Enumerations.UserType.Teacher)
                return RedirectToAction("UnAuthorizeAccess", "Home", new { area = "" });
            SessionManager manager = new SessionManager();
            List<Sessions> list = manager.TeacherUpcomingSessions(Config.CurrentUser);
            return View(list);

        }


 

        [AllowAnonymous]
        public JsonResult GradesList(Int64 CurriculumID)
        {

            if (CurriculumID > 0)
            {
                IEnumerable<SelectListItem> ListOfGrades = manager.dListOfGrades(CurriculumID);
                return Json(
                     ListOfGrades, JsonRequestBehavior.AllowGet
                );
            }
            return Json(
                null,
                JsonRequestBehavior.AllowGet
            );
        }

        [AllowAnonymous]
        public JsonResult dSubjectsList(Int64 GradeID)
        {

            if (GradeID > 0)
            {
                IEnumerable<SelectListItem> ListOfGrades = manager.dListOfTeacherSubjects(GradeID,Config.CurrentUser);
                return Json(
                     ListOfGrades, JsonRequestBehavior.AllowGet
                );
            }
            return Json(
                null,
                JsonRequestBehavior.AllowGet
            );
        }

        [AllowAnonymous]
        public JsonResult TopicsListDrop(Int64 SubjectID)
        {

            if (SubjectID > 0)
            {
                IEnumerable<SelectListItem> ListOfTopics = manager.dListOfSubjectTopics(SubjectID);
                return Json(
                     ListOfTopics, JsonRequestBehavior.AllowGet
                );
            }
            return Json(
                null,
                JsonRequestBehavior.AllowGet
            );
        }


        [AllowAnonymous]
        
        public JsonResult DateTimeValidation(DateTime Date, DateTime Time)
        {
            if(Date!=null && Time!=null)
            {
                DateTime a = Date;
                TimeSpan b = Time.TimeOfDay;
                DateTime merge = a.Add(b);

                CustomTimeZone czone = new CustomTimeZone();
                DateTime CurrentTime = czone.DateTimeNow();
                if (merge > CurrentTime )
                {
                    return Json(new { success = true }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new { success = false }, JsonRequestBehavior.AllowGet);
                }

            }
          
            return Json(new { success= false },JsonRequestBehavior.AllowGet);
        }



        public ActionResult AllReviews()
        {
            if (Config.CurrentUser == 0)
                return RedirectToAction("Login", "Account", new { area = "" });

            if (Config.User.UserType != (int)Enumerations.UserType.Teacher)
                return RedirectToAction("UnAuthorizeAccess", "Home", new { area = "" });


            RatingsManager rManager = new RatingsManager();
            RatingsViewModel model = new RatingsViewModel();
            model = rManager.ListOfRatings(Config.CurrentUser);
            model.ReviewsList = model.ReviewsList.OrderByDescending(x => x.CreatedDate).ToList();
            model.TeacherID = Config.CurrentUser;

            return View(model);

        }


        // Course and Offerings Section  

        [HttpGet]
        public ActionResult AddCourse()
        {
            if (Config.CurrentUser == 0)
                return RedirectToAction("Login", "Account", new { area = "" });

            if (Config.User.UserType != (int)Enumerations.UserType.Teacher)
                return RedirectToAction("UnAuthorizeAccess", "Home", new { area = "" });



            AddCourseViewModel model = new AddCourseViewModel();

            CurriculumsManagers cManager = new CurriculumsManagers();
            model.ListofTeacherSubjects = cManager.dListOfTeacherSubjects(Config.CurrentUser);
            model.ListofavailableTimes = cManager.ListOfAvailableTimes();
            model.ListofDays = cManager.ListOfDays();

            if(model.ListofavailableTimes != null && model.ListofavailableTimes.Count>0)
            {
                model.ListofavailableTimes = model.ListofavailableTimes.OrderBy(x => x.TimeSlot).ToList();
            }

            return View(model);
        }

        [HttpPost]
        public ActionResult AddCourse(AddCourseViewModel model)
        {
            if (Config.CurrentUser == 0)
                return RedirectToAction("Login", "Account", new { area = "" });

            if (Config.User.UserType != (int)Enumerations.UserType.Teacher)
                return RedirectToAction("UnAuthorizeAccess", "Home", new { area = "" });


            try
            {

                CourseOfferingsManager coManager = new CourseOfferingsManager();
                model.FKOfferedBy = Config.CurrentUser;
                Int64 ID = coManager.AddCourse(model);
                if (ID >0)
                {
                    TempData["Message"] = "Course succesfully.";
                    TempData["Status"] = 2;
                    return RedirectToAction("CoursesList");
                }
                else
                {
                    TempData["Message"] = "Something went wrong.";
                    TempData["Status"] = 3;
                    return RedirectToAction("CoursesList");
                }
            }
            catch (Exception)
            {

                throw;
            }

            return View();
        }
 
        public ActionResult CoursesList()
        {
            if (Config.CurrentUser == 0)
                return RedirectToAction("Login", "Account", new { area = "" });

            if (Config.User.UserType != (int)Enumerations.UserType.Teacher)
                return RedirectToAction("UnAuthorizeAccess", "Home", new { area = "" });

            ViewBag.Status = (TempData["Status"] != null) ? (int)TempData["Status"] : 0;
            ViewBag.Message = (TempData["Message"] != null) ? (string)TempData["Message"] : null;


            return View();
        }

        public ActionResult _CoursesList()
        {
            if (Config.CurrentUser == 0)
                return RedirectToAction("Login", "Account", new { area = "" });

            if (Config.User.UserType != (int)Enumerations.UserType.Teacher)
                return RedirectToAction("UnAuthorizeAccess", "Home", new { area = "" });



            CourseOfferingsManager cManager = new CourseOfferingsManager();

            List<CoursesHeader> list = new List<CoursesHeader>();

            list = cManager.TeacherCourses(Config.CurrentUser);

            return PartialView(list);

        }


        public ActionResult CourseDetail(Int64 CourseID)
        {
            if (Config.CurrentUser == 0)
                return RedirectToAction("Login", "Account", new { area = "" });

            if (Config.User.UserType != (int)Enumerations.UserType.Teacher)
                return RedirectToAction("UnAuthorizeAccess", "Home", new { area = "" });



            try
            {
                if (CourseID > 0)
                {

                    CourseOfferingsManager cManager = new CourseOfferingsManager();
                    CoursesHeader Course = new CoursesHeader();
                    Course = cManager.GetCourseDetail(CourseID);

                    return View(Course);

                }
                else
                {
                    return RedirectToAction("Index");
                }

            }
            catch (Exception)
            {

                throw;
            }
        }


        #region [Free Resources Section]


        public ActionResult FreeResources()
        {

            if (Config.CurrentUser == 0)
                return RedirectToAction("Login", "Account", new { area = "" });

            if (Config.User.UserType != (int)Enumerations.UserType.Teacher)
                return RedirectToAction("UnAuthorizeAccess", "Home", new { area = "" });

            ViewBag.Status = (TempData["Status"] != null) ? (int)TempData["Status"] : 0;
            ViewBag.Message = (TempData["Message"] != null) ? (string)TempData["Message"] : null;

           

            FreeResourcesManager fManager = new FreeResourcesManager();

            List<FreeResources> list = new List<FreeResources>();

            list = fManager.TeacherResources(Config.CurrentUser);


            return View(list);

        }


        public ActionResult _FreeResources()
        {

            if (Config.CurrentUser == 0)
                return RedirectToAction("Login", "Account", new { area = "" });

            if (Config.User.UserType != (int)Enumerations.UserType.Teacher)
                return RedirectToAction("UnAuthorizeAccess", "Home", new { area = "" });

        
            FreeResourcesManager fManager = new FreeResourcesManager();

            List<FreeResources> list = new List<FreeResources>();

            list = fManager.TeacherResources(Config.CurrentUser);

            return PartialView(list);

        }

        public ActionResult _LoadSubjectResource(Int64 sID)
        {

            if (Config.CurrentUser == 0)
                return RedirectToAction("Login", "Account", new { area = "" });

            if (Config.User.UserType != (int)Enumerations.UserType.Teacher)
                return RedirectToAction("UnAuthorizeAccess", "Home", new { area = "" });


            FreeResourcesManager fManager = new FreeResourcesManager();

            List<FreeResources> list = new List<FreeResources>();

            list = fManager.TeacherResourcesBySubject(Config.CurrentUser, sID);

            return PartialView(list);

        }

        [HttpGet]
        public ActionResult AddResource()
        {
            if (Config.CurrentUser == 0)
                return RedirectToAction("Login", "Account", new { area = "" });

            if (Config.User.UserType != (int)Enumerations.UserType.Teacher)
                return RedirectToAction("UnAuthorizeAccess", "Home", new { area = "" });


            AddResourceViewModel model = new AddResourceViewModel();
            CurriculumsManagers cManager = new CurriculumsManagers();
            model.ListofTeacherSubjects = cManager.dListOfTeacherSubjects(Config.CurrentUser);
            model.ListofSubjectTopics = Enumerable.Empty<SelectListItem>();

            return PartialView(model);
        }

        [HttpPost]
        public ActionResult AddResource(AddResourceViewModel model)
        {

            if (Config.CurrentUser == 0)
                return RedirectToAction("Login", "Account", new { area = "" });

            if (Config.User.UserType != (int)Enumerations.UserType.Teacher)
                return RedirectToAction("UnAuthorizeAccess", "Home", new { area = "" });

            model.FKOfferedBy = Config.CurrentUser;

            FreeResourcesManager fManager = new FreeResourcesManager();
            Int64 ID=fManager.AddResource(model);

            if (ID > 0)
            {
                TempData["Message"] = "Resource Added succesfully.";
                TempData["Status"] = 2;
                return RedirectToAction("FreeResources");
            }
            else
            {
                TempData["Message"] = "Something went wrong.";
                TempData["Status"] = 3;
                return RedirectToAction("FreeResources");
            }

            return View();
        }


        [HttpGet]
        public ActionResult EditResource(Int64 ResourceID)
        {
            if (Config.CurrentUser == 0)
                return RedirectToAction("Login", "Account", new { area = "" });

            if (Config.User.UserType != (int)Enumerations.UserType.Teacher)
                return RedirectToAction("UnAuthorizeAccess", "Home", new { area = "" });


            FreeResourcesManager fManager = new FreeResourcesManager();
            FreeResources obj = new FreeResources();

            obj = fManager.GetFreeResource(ResourceID);
            if (obj != null)
            {

                AddResourceViewModel model = new AddResourceViewModel();
                CurriculumsManagers cManager = new CurriculumsManagers();

                model.ListofTeacherSubjects = cManager.dListOfTeacherSubjects(Config.CurrentUser);
                model.ListofSubjectTopics = cManager.dListOfSubjectTopics(obj.FKSubjectID);

                model.ResourceTitle = obj.ResourceTitle;
                model.ResourceID = obj.FreeResourcesID;
                model.ResourceDescription = obj.VideoDescription;
                model.FKTopicID = obj.FKTopicID;
                model.FKSubjectID = obj.FKSubjectID;
                model.ResourceYear = obj.Year;
                model.VideoURL = obj.VideoURL;
                model.TopicOptional = obj.OptionalTopic;

                return PartialView(model);


            }
            else
            {
                return RedirectToAction("UnAuthorizeAccess", "Home", new { area = "" });
            }

           
        }

        [HttpPost]
        public ActionResult EditResource(AddResourceViewModel model)
        {

            if (Config.CurrentUser == 0)
                return RedirectToAction("Login", "Account", new { area = "" });

            if (Config.User.UserType != (int)Enumerations.UserType.Teacher)
                return RedirectToAction("UnAuthorizeAccess", "Home", new { area = "" });

            model.FKOfferedBy = Config.CurrentUser;

            FreeResourcesManager fManager = new FreeResourcesManager();
            bool status =  fManager.UpdateResource(model);

            if (status ==true)
            {
                TempData["Message"] = "Resource Updated succesfully.";
                TempData["Status"] = 2;
                return RedirectToAction("ResourceDetail", new { @ResourceID=model.ResourceID });
            }
            else
            {
                TempData["Message"] = "Something went wrong.";
                TempData["Status"] = 3;
                return RedirectToAction("ResourceDetail", new { @ResourceID = model.ResourceID });
            }

            return View();
        }


        public ActionResult ResourceDetail(Int64 ResourceID)
        {

            if (Config.CurrentUser == 0)
                return RedirectToAction("Login", "Account", new { area = "" });

            if (Config.User.UserType != (int)Enumerations.UserType.Teacher)
                return RedirectToAction("UnAuthorizeAccess", "Home", new { area = "" });
            try
            {
                FreeResourcesManager fManager = new FreeResourcesManager();
                FreeResources obj = new FreeResources();

                obj = fManager.GetFreeResource(ResourceID);
                if (obj != null)
                {
                    ViewBag.Status = (TempData["Status"] != null) ? (int)TempData["Status"] : 0;
                    ViewBag.Message = (TempData["Message"] != null) ? (string)TempData["Message"] : null;

                    return View(obj);

                }
                else
                {
                    return RedirectToAction("UnAuthorizeAccess", "Home", new { area = "" });
                }
            }
            catch (Exception)
            {

                throw;
            }

        }

        public ActionResult DeleteResouce(Int64 ResourceID)
        {
            if (Config.CurrentUser == 0)
                return RedirectToAction("Login", "Account", new { area = "" });

            if (Config.User.UserType != (int)Enumerations.UserType.Teacher)
                return RedirectToAction("UnAuthorizeAccess", "Home", new { area = "" });
            try
            {
                FreeResourcesManager fManager = new FreeResourcesManager();

                bool status = fManager.DeleteResource(ResourceID);

                if (status == true)
                {
                    TempData["Message"] = "Resource Deleted succesfully.";
                    TempData["Status"] = 2;
                    return RedirectToAction("FreeResources");
                }
                else
                {
                    TempData["Message"] = "Something went wrong.";
                    TempData["Status"] = 3;
                    return RedirectToAction("FreeResources");
                }

            }
            catch(Exception ex)
            {
                throw;
            }
        }


        #endregion


        #region[Notes Section]

        public ActionResult Folders()
        {

            if (Config.CurrentUser == 0)
                return RedirectToAction("Login", "Account", new { area = "" });

            if (Config.User.UserType != (int)Enumerations.UserType.Teacher)
                return RedirectToAction("UnAuthorizeAccess", "Home", new { area = "" });

            ViewBag.Status = (TempData["Status"] != null) ? (int)TempData["Status"] : 0;
            ViewBag.Message = (TempData["Message"] != null) ? (string)TempData["Message"] : null;


            NotesManager nManager = new NotesManager();
            List<Folders> list = new List<Folders>();
            list = nManager.ListOfFolder(Config.CurrentUser);
            return View(list); 

        }

        [HttpGet]
        public ActionResult AddFolder()
        {
            if (Config.CurrentUser == 0)
                return RedirectToAction("Login", "Account", new { area = "" });

            if (Config.User.UserType != (int)Enumerations.UserType.Teacher)
                return RedirectToAction("UnAuthorizeAccess", "Home", new { area = "" });

            AddFolderViewModel model = new AddFolderViewModel();

            return View(model);
        }

        [HttpPost]
        public ActionResult AddFolder(AddFolderViewModel model)
        {
            if (Config.CurrentUser == 0)
                return RedirectToAction("Login", "Account", new { area = "" });

            if (Config.User.UserType != (int)Enumerations.UserType.Teacher)
                return RedirectToAction("UnAuthorizeAccess", "Home", new { area = "" });


            NotesManager nManager = new NotesManager();

            model.FKCreatedBy = Config.CurrentUser;
            Int64 id = nManager.AddFolder(model);

            if (id>0)
            {
                TempData["Message"] = "Folder Created Succesfully.";
                TempData["Status"] = 2;
                return RedirectToAction("Folders");
            }
            else
            {
                TempData["Message"] = "Something went wrong.";
                TempData["Status"] = 3;
                return RedirectToAction("Folders");
            }

           
        }

        [HttpGet]
        public ActionResult AddNotes(Int64 id)
        {

            if (Config.CurrentUser == 0)
                return RedirectToAction("Login", "Account", new { area = "" });

            if (Config.User.UserType != (int)Enumerations.UserType.Teacher)
                return RedirectToAction("UnAuthorizeAccess", "Home", new { area = "" });


            AddNotesViewModel model = new AddNotesViewModel();
            CurriculumsManagers cManager = new CurriculumsManagers();
            model.ListofTeacherSubjects = cManager.dListOfTeacherSubjects(Config.CurrentUser);
            model.ListofSubjectTopics = Enumerable.Empty<SelectListItem>();
            model.FolderID = id;
            return PartialView(model);

        }

        [HttpPost]
        public ActionResult AddNotes(AddNotesViewModel model, HttpPostedFileBase FileBase)
        {
            if (Config.CurrentUser == 0)
                return RedirectToAction("Login", "Account", new { area = "" });

            if (Config.User.UserType != (int)Enumerations.UserType.Teacher)
                return RedirectToAction("UnAuthorizeAccess", "Home", new { area = "" });


            NotesManager nManager = new NotesManager();

            model.FKOfferedBy = Config.CurrentUser;

            if (FileBase != null && FileBase.ContentLength > 0)
            {
                var extension = Path.GetExtension(FileBase.FileName).ToLower();
                Guid gID = Guid.NewGuid();
                string path = Server.MapPath("~/TeacherNotes/" + gID + extension);
                FileBase.SaveAs(path);
                model.FileName= gID.ToString() + extension;
            }
            else
            {
                model.FileName = null;
            }

            Int64 id = nManager.AddNotes(model);

            if (id > 0)
            {
                TempData["Message"] = "Note Added Succesfully.";
                TempData["Status"] = 2;
                return RedirectToAction("GetNotes",new { @id=model.FolderID});
            }
            else
            {
                TempData["Message"] = "Something went wrong.";
                TempData["Status"] = 3;
                return RedirectToAction("GetNotes", new { @id = model.FolderID });
            }

        }

        public ActionResult GetNotes(Int64 id)
        {

            if (Config.CurrentUser == 0)
                return RedirectToAction("Login", "Account", new { area = "" });

            if (Config.User.UserType != (int)Enumerations.UserType.Teacher)
                return RedirectToAction("UnAuthorizeAccess", "Home", new { area = "" });


            ViewBag.Status = (TempData["Status"] != null) ? (int)TempData["Status"] : 0;
            ViewBag.Message = (TempData["Message"] != null) ? (string)TempData["Message"] : null;

            NotesManager nManager = new NotesManager();

            ViewBag.FolderName = nManager.GetFolder(id).FolderName;
            ViewBag.FolderID = id;
          
            List<Notes> list = new List<Notes>();
            list = nManager.ListOfNotes(Config.CurrentUser,id);
            return View(list);


        }

        [HttpGet]
        public ActionResult EditNotes(Int64 id)
        {

            if (Config.CurrentUser == 0)
                return RedirectToAction("Login", "Account", new { area = "" });

            if (Config.User.UserType != (int)Enumerations.UserType.Teacher)
                return RedirectToAction("UnAuthorizeAccess", "Home", new { area = "" });

            try
            {
                if (id > 0)
                {
                    NotesManager nManager = new NotesManager();
                    var obj = nManager.GetNotes(id);

                    if(obj!=null)
                    {
                        AddNotesViewModel model = new AddNotesViewModel();
                        CurriculumsManagers cManager = new CurriculumsManagers();

                        model.ListofTeacherSubjects = cManager.dListOfTeacherSubjects(Config.CurrentUser);
                        model.ListofSubjectTopics = cManager.dListOfTopics(obj.FKSubjectID);
                        model.FileName = obj.FileName;
                        model.FKSubjectID = obj.FKSubjectID;
                        model.FKTopicID = obj.FKTopicID;
                        model.FolderID = obj.FKFolderID;
                        model.NoteID = obj.NoteID;
                        model.NotesTitle = obj.NotesName;
                        model.NotesDescription = obj.TopicDescription;
                        model.TopicOptional = obj.TopicOptional;
                        return PartialView(model);

                    }
                    else
                    {
                        return RedirectToAction("UnAuthorizeAccess", "Home", new { area = "" });
                    }
                  
                }
                else
                {
                    return RedirectToAction("UnAuthorizeAccess", "Home", new { area = "" });
                }
            }
            catch (Exception ex)
            {

                throw;
            }

        }


        [HttpPost]
        public ActionResult EditNotes(AddNotesViewModel model, HttpPostedFileBase FileBase)
        {

            if (Config.CurrentUser == 0)
                return RedirectToAction("Login", "Account", new { area = "" });

            if (Config.User.UserType != (int)Enumerations.UserType.Teacher)
                return RedirectToAction("UnAuthorizeAccess", "Home", new { area = "" });

            try
            {

                if (FileBase != null && FileBase.ContentLength > 0)
                {
                    var extension = Path.GetExtension(FileBase.FileName).ToLower();
                    Guid gID = Guid.NewGuid();
                    string path = Server.MapPath("~/TeacherNotes/" + gID + extension);
                    FileBase.SaveAs(path);
                    model.FileName = gID.ToString() + extension;
                }
                else
                {
                    model.FileName = null;
                }

                NotesManager nManager = new NotesManager();
                bool status = nManager.EditNotes(model);

                if (status)
                {
                    TempData["Message"] = "Notes Updated Succesfully.";
                    TempData["Status"] = 2;
                    return RedirectToAction("GetNotes", new { @id = model.FolderID });
                }
                else
                {
                    TempData["Message"] = "Something went wrong.";
                    TempData["Status"] = 3;
                    return RedirectToAction("GetNotes", new { @id = model.FolderID });
                }

                return View();

            }
            catch (Exception ex)
            {

                throw;
            }

        }


        public ActionResult DeleteNotes(Int64 id)
        {

            if (Config.CurrentUser == 0)
                return RedirectToAction("Login", "Account", new { area = "" });

            if (Config.User.UserType != (int)Enumerations.UserType.Teacher)
                return RedirectToAction("UnAuthorizeAccess", "Home", new { area = "" });

            try
            {

                if(id>0)
                {

                    NotesManager nManager = new NotesManager();
                    var obj = nManager.GetNotes(id);
                    bool sID = nManager.DeleteNotes(id) ;
                
                    if (sID==true)
                    {                      
                        TempData["Message"] = "Note Deleted Succesfully.";
                        TempData["Status"] = 2;
                        return RedirectToAction("GetNotes", new { @id = obj.FKFolderID });
                    }
                    else
                    {
        
                        TempData["Message"] = "Something went wrong.";
                        TempData["Status"] = 3;
                        return RedirectToAction("GetNotes", new { @id = obj.FKFolderID });
                    }

                }
                else
                {
                    return RedirectToAction("UnAuthorizeAccess", "Home", new { area = "" });
                }
               
            }
            catch (Exception ex)
            {

                throw;
            }

        }


        public ActionResult DownloadNotes(Int64 id)
        {

            if (Config.CurrentUser == 0)
                return RedirectToAction("Login", "Account", new { area = "" });

            if (Config.User.UserType != (int)Enumerations.UserType.Teacher)
                return RedirectToAction("UnAuthorizeAccess", "Home", new { area = "" });

            try
            {

                if (id > 0)
                {

                    NotesManager nManager = new NotesManager();
                    var obj = nManager.GetNotes(id);

                    string filename = obj.FileName;
                    string filepath = AppDomain.CurrentDomain.BaseDirectory+ "/TeacherNotes/" + obj.FileName;
                    byte[] filedata = System.IO.File.ReadAllBytes(filepath);
                    string contentType = MimeMapping.GetMimeMapping(filepath);

                    var cd = new System.Net.Mime.ContentDisposition
                    {
                        FileName = filename,
                        Inline = false,
                    };
                    string value = String.Format("inline;filename={0}",cd.ToString());
                    Response.AppendHeader("Content-Disposition", cd.ToString());

                    bool status = nManager.UpdateDownloadCount(id);

                    if (status)
                       // return File(contentType.Attachment.Data, MimeExtensionHelper.GetMimeType(result.Attachment.FileName), result.Attachment.FileName);
                    return File(filedata, contentType);
                    else
                        return null;

                }
                else
                {
                    return RedirectToAction("UnAuthorizeAccess", "Home", new { area = "" });
                }

            }
            catch (Exception ex)
            {

                throw;
            }

        }


        [HttpGet]
        public ActionResult EmailNotes(Int64 id)
        {
            try
            {
                ShareEmailViewModel model = new ShareEmailViewModel();
                model.NoteID = id;
                return PartialView(model);
            }
            catch (Exception ex)
            {
                throw;
            }
        }


        [HttpPost]
        public ActionResult EmailNotes(ShareEmailViewModel model)
        {
            try
            {
                NotesManager nManager = new NotesManager();
                var obj = nManager.GetNotes(model.NoteID);
                string filename = obj.FileName;
                string filepath = AppDomain.CurrentDomain.BaseDirectory + "/TeacherNotes/" + obj.FileName;

                string subject = Config.UsersProfile.FirstName + " Share Study notes with you";
                bool status= BLL.EmailTemplate.EmailTemplate.ShareNotes(model.Email, subject, filepath, model.Message, obj.FileName);

                if (status == true)
                {
                    TempData["Message"] = "Notes Shared Succesfully.";
                    TempData["Status"] = 2;
                    return RedirectToAction("GetNotes", new { @id = obj.FKFolderID });
                }
                else
                {

                    TempData["Message"] = "Something went wrong.";
                    TempData["Status"] = 3;
                    return RedirectToAction("GetNotes", new { @id = obj.FKFolderID });
                }

                return View();
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        #endregion


        #region [Notifications Section]

        public ActionResult AllNotification()
        {

            if (Config.CurrentUser == 0)
                return RedirectToAction("Login", "Account", new { area = "" });

            if (Config.User.UserType != (int)Enumerations.UserType.Teacher)
                return RedirectToAction("UnAuthorizeAccess", "Home", new { area = "" });

            try
            {

                List<UserNotifications> list = new List<UserNotifications>();
                NotificationManager nManager = new NotificationManager();
                list = nManager.ListofUserNotifications(Config.CurrentUser);
                var update = db.UserNotification.Where(p => p.IsSeen != true).ToList();
                foreach(var item in update)
                {

                    var user = db.Users.Where(o => o.UserID == item.FKUserID).FirstOrDefault();
                    if (user.UserType == 3)
                    {
                        item.IsSeen = true;
                    }
                }db.SaveChanges();
                return View(list);
            }
            catch (Exception ex)
            {

                throw;
            }



           


        }

        //[ChildActionOnly]
        public PartialViewResult LoadNotification()
        {
            try
            {
                //NotificationManager nManager = new NotificationManager();
                //List<UserNotifications> list = new List<UserNotifications>();
                //list = nManager.ListofUserNotifications(Config.CurrentUser);

                //if (list != null && list.Count > 0)
                //{
                //    list = list.Take(3).ToList();
                //}
                //else
                //{
                //    list = null;
                //}

                //return PartialView(list);
                return PartialView();
            }
            catch (Exception ex)
            {

                throw;
            }
        }


        #endregion

















        //public ActionResult TestDetail(Int64 TestID)
        //{
        //    if (Config.CurrentUser == 0)
        //        return RedirectToAction("Login", "Account", new { area = "" });

        //    if (Config.User.UserType != (int)Enumerations.UserType.Teacher)
        //        return RedirectToAction("UnAuthorizeAccess", "Home", new { area = "" });


        //    if (TestID > 0)
        //    {
        //        QuestionPaperViewModel model = manager.TestQuestionPaper(TestID);
        //        if (model.UserID == Config.CurrentUser)
        //        {
        //            return View(model);
        //        }
        //        else
        //        {
        //            return RedirectToAction("UnAuthorizeAccess", "Home", new { area = "" });
        //        }
        //    }
        //    else
        //    {

        //    }
        //    return View();

        //}

    }
}

